 $(document).ready(function() {
	$("#form-novo-caderno").validate({
		rules: {
			nome: {
				required: true,
			},
			limite: {
				range: [1, 5000]
			}
		},
		messages: {
			nome: {
				required: "Nome do Caderno é obrigatório",
			},
			limite: {
				range: "A quantidade de questões a serem adicionadas deve ser um número entre 1 e 5000.",
			}
			
		}
	});
	
	$("#form-caderno-existente").validate({
		rules: {
			limite: {
				range: [1, 5000]
			}
		},
		messages: {
			limite: {
				range: "A quantidade de questões a serem adicionadas deve ser um número entre 1 e 5000.",
			}
			
		}
	});
});