function carrega_modal_edicao_caderno(caderno_id) {
	$('#modal-editar-caderno .panel-body .panel-conteudo').hide();
	
	$.get('/questoes/main/xhr_get_caderno/'+caderno_id, function(data) {
		$('#id_caderno').val(caderno_id);
		$('#nome_caderno').val(data.cad_nome);
		// $('#categorias').val(data.categorias);		
		
		if(data.cad_comercializavel == 1) {
			$('#comercializavel').prop('checked',true);
		}
		else {
			$('#comercializavel').prop('checked',false);
		}

		if(data.cad_coaching == 1) {
			$('#coaching').prop('checked',true);
		}
		else {
			$('#coaching').prop('checked',false);
		}
		
		$('#modal-editar-caderno .panel-body .panel-carregando').hide();
		$('#modal-editar-caderno .panel-body .panel-conteudo').show();

		atualizar_categorias();
	}, 'json');
} 

function carrega_modal_exclusao_caderno(caderno_id) {
	$('#item_id').val(caderno_id);
} 

function carrega_modal_zerar_estatisticas(caderno_id) {
	$('#zer_est_item_id').val(caderno_id);
} 

function carrega_modal_categorizacao_caderno(caderno_id) {
	$('#modal-categorizar-caderno .modal-body .panel-conteudo').hide();
	$('.hidden-caderno-id').val(caderno_id);
	
	$.get('/questoes/main/xhr_get_categorias_de_caderno/' + caderno_id, function(data) {
		$('#categorias_ids').val(data).trigger('change');
		$('#modal-categorizar-caderno .modal-body .panel-carregando').hide();
		$('#modal-categorizar-caderno .modal-body .panel-conteudo').show();
	}, 'json');
}

function carrega_modal_categorizacao_cadernos_selecionados() {
	caderno_id = $('.caderno-checkbox:checked').map(function () { return this.value} ).get().join('-')

	$('.hidden-caderno-id').val(caderno_id);
	$('.hidden-massa').val(1);
	$('#categorias_ids').val('').trigger('change');
	$('#modal-categorizar-caderno .modal-body .panel-carregando').hide();
	$('#modal-categorizar-caderno .modal-body .panel-conteudo').show();
}

$(document).ready(function() {
	$("#form-editar-caderno").validate({
		rules: {
			cad_nome: {
				required: true,
			}
		},
		messages: {
			cad_nome: {
				required: "Nome do Caderno é obrigatório",
			}
		}
	});
	
	$("#categorias_ids").select2();
	
	$("#search-cadernos").change( function () {
		/*var filter = $(this).val(); // get the value of the input, which we filter on
		if (filter) {
			$(".cadernos-tabela").find(".nome_caderno:not(:Contains(" + filter + "))").parent().hide();
			$(".cadernos-tabela").find(".nome_caderno:Contains(" + filter + ")").parent().show();
		} else {
			$(".cadernos-tabela").find("tr").show();
		}*/
	}).keyup( function () {
		// fire the above change event after every letter
		$(this).change();
	});
	
	// Case-Insensitive Contains
	$.expr[':'].Contains = function(a,i,m){
	    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
	};


});