function selecionar_range() 
{
    $('#aluno').val($('#aluno_id').val());
    $('#inicio').val($("#daterange").data('daterangepicker').startDate.format('YYYY-MM-DD'));
    $('#fim').val($("#daterange").data('daterangepicker').endDate.format('YYYY-MM-DD'));
}

$(function() {

  $("#daterange").daterangepicker({
   	"locale": 
   	{
          "format": "DD/MM/YYYY",
          "separator": " - ",
          "applyLabel": "Filtrar",
          "cancelLabel": "Cancelar",
          "fromLabel": "De",
          "toLabel": "Até",
          "customRangeLabel": "Personalizado",
          "weekLabel": "S",
          "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
          "monthNames": ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
          "firstDay": 1,
      },
      "startDate": $('#inicio').val(),
  	  "endDate": $('#fim').val(),
      "alwaysShowCalendars": true,
      ranges: {
         'Hoje': [moment(), moment()],
         'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
         'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
         'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
         'Este Mês': [moment().startOf('month'), moment().endOf('month')],
         'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
         'Este Ano': [moment().startOf('year'), moment().endOf('year')]
      }
  });

  $('#daterange').on('apply.daterangepicker', function(ev, picker) {
  	selecionar_range();
  		$('#filtro').submit();
  });

  $('#filtro-submit-btn').click(function() {
  	selecionar_range();
  	$('#filtro-submit').val(1);
  	$('#minha-visao-submit').val(0);
  	$('#filtro').submit();
  });

  $('#minha-visao-submit-btn').click(function() {
  	selecionar_range();
  	$('#filtro-submit').val(0);
  	$('#minha-visao-submit').val(1);
  	$('#filtro').submit();
  });


  $('#fb-por-banca').select2({
    placeholder: "Selecione uma Banca",
  });
  
  $('#fb-por-banca').on('change', function () {
      selecionar_range();
      $('#bancas_ids').val($('#fb-por-banca').val());
      $('#filtro').submit();
  });
  
  $('#filtro-titulo').on('blur', function () {
      selecionar_range();
      $('#sim_nome').val($('#filtro-titulo').val());
      $('#filtro').submit();
  });
  
  $('#filtro-titulo').on('keyup', function (e) {
	if(e.keyCode == 13) {
		$(this).blur();
	}
  });

  $('#fb-por-disciplina').select2({
      placeholder: "Selecione uma disciplina",
      allowClear: true
  });

  $('#fb-por-disciplina').on('change', function () {
      selecionar_range();
      $('#disciplinas_ids').val($('#fb-por-disciplina').val());
      $('#filtro').submit();
  });

  $('#fb-por-assunto').select2({
      placeholder: "Selecione um assunto",
      allowClear: true
  });

  $('#fb-por-assunto').on('change', function () {
      selecionar_range();
      $('#assuntos_ids').val($('#fb-por-assunto').val());
      $('#filtro').submit();
  });

});