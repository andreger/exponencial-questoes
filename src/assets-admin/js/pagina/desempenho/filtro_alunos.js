$(function() {
	$('.check-aluno').change(function(){

		var filtro_aluno = '';
		
		$('input[name="filtro_aluno[]"]:checked').each(function() {
			filtro_aluno += this.value + ',';
		});

		if(filtro_aluno){
			filtro_aluno = filtro_aluno.substring(0, filtro_aluno.length-1);
		}
		
		$("#aluno_id option").each(function() {
			if($(this).val()){
				$(this).remove();
			}else{
				$(this).text('Carregando alunos...');
			}
		});

		$('#aluno_id').select2({
			placeholder: "Carregando alunos..."
		});

		$('#filtro_aluno').val(filtro_aluno);
				
		$.ajax({
			url: "/questoes/main/get_ajax_main_alunos_acompanhamento/0",
			method: "POST",
			cache: false,
			data: { 
				filtro_aluno: filtro_aluno
			}
		})
		.done(function( msg ) {
			$("#select-alunos").html(msg);
			$('#aluno_id').select2({
				placeholder: "Selecione um aluno..."
			});
		});

	});

	$('#aluno_id').select2({
		placeholder: "Selecione um aluno..."
	});

});