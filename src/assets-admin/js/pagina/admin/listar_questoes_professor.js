$(document).ready(function() {
    $(".multiselect").select2({	
		    placeholder: "Selecione..."
    });

    $("#limpar-filtros-btn").click(function(e) {
        e.preventDefault();
        $("#frm-filtros input[type='text']").val("");
        $(".multiselect").val(null).trigger('change');
        $("#sumit-btn").click();
    });
    
    $(".toggle-filtros-btn").click(function(e) {
        e.preventDefault();
        $("#filtro").toggle();
        localStorage.setItem('questoes.admin.listar_provas_professores.exibir_filtro', $("#filtro").is(':visible'));
    });
    
    var exibir_filtro = localStorage.getItem('questoes.admin.listar_provas_professores.exibir_filtro');

    if(exibir_filtro == "true") {
        $("#filtro").show();
    }
});