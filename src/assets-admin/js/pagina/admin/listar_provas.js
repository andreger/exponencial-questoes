$(document).ready(function() {
    $(".multiselect").select2({	
		    placeholder: "Selecione..."
    });

    $(".toggle-filtros-btn").click(function(e) {
        e.preventDefault();
        $("#filtro").toggle();
        localStorage.setItem('questoes.admin.listar_provas.exibir_filtro', $("#filtro").is(':visible'));
    });

    $("#limpar-filtros-btn").click(function(e) {
        e.preventDefault();
        $("#frm-filtros input[type='text']").val("");
        $(".multiselect").val(null).trigger('change');
        $("#sumit-btn").click();
    });

    var exibir_filtro = localStorage.getItem('questoes.admin.listar_provas.exibir_filtro');

    if(exibir_filtro == "true") {
        $("#filtro").show();
    }
});