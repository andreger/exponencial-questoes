$(document).ready(function() { 
	
	 $('#disciplina').change(function () {
		var sim_id = $(this).data('sim-id');
		var query_string = $(this).data('busca-query-string');
		url = "/questoes/admin/revisar_questoes/" + sim_id + "/" + $('#disciplina option:selected').val();
		url = url + query_string;
     	location.href = url	
 	 });
	 
	 $('#botao-sim').click(function (event) {
		 $('#acao-questoes').submit();
	 });
	 
	 $('#salvar').click(function (event) {
		 var selecionadas = $('.questao-opcao:checked').length;
		 var permitidas = $(this).data('permitidas')
		 if(selecionadas > permitidas) {
			 var msg = "<div class='alert alert-danger alert-dismissable'> " +
			 	"<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>" +
			 	"Você está tentando adicionar " + selecionadas + " questões, mas este simulado só suporta mais " + permitidas + "." +
			 	"</div>";
			 $('#msgbox').html(msg);
		 }
		 else {
			 $('#acao-questoes').submit();
		 }
	 });
 
});