$(document).ready(function() {
	$('.botao-designar-aluno').click(function() {
		var id = $(this).data('id');
		$.get('/questoes/xhr/get_usuarios_designados/' + id, function(data) {
			$('#usuarios_designados-' + id).html(data);
		});
	});
});

function cancelar_designacao(siu_id, sim_id) {
	var confirm = window.confirm('Deseja cancelar esta designação?');
	
	if(confirm == true) {
		$('#siu-'+siu_id).hide();
		
		$.get('/questoes/xhr/cancelar_designacao/' + siu_id, function(data) {
			$.get('/questoes/xhr/get_usuarios_designados/' + sim_id, function(data) {
				$('#usuarios_designados-' + sim_id).html(data);
			});
		});
	}
}