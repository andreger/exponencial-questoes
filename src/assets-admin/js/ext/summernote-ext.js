function sendFile(file, editor) {
    var data = new FormData();
    data.append("file", file);
    
    $.ajax({
        data: data,
        type: "POST",
        url: "/questoes/admin/xhr_upload_imagem",
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
            editor.summernote('insertImage', url);
        }
    });
}

$.extend($.summernote.options, {
    defaultFontName: 'open sans',
    fontNames: [
        'Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Helvetica', 'Impact', 'open sans', 'Times New Roman', 'Verdana'
    ]
});

