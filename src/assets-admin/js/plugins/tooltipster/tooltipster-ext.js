function gerar_tooltipster(){
    
    $('.tooltipster, .tooltipster-top').tooltipster({
      maxWidth: 300,
      theme: 'tooltipster-shadow',
      interactive: true,
    });
    
    $('.tooltipster-right').tooltipster({
      maxWidth: 300,
      theme: 'tooltipster-shadow',
      side: 'right',
      interactive: true,
    });
    
    $('.tooltipster-left').tooltipster({
      maxWidth: 300,
      theme: 'tooltipster-shadow',
      side: 'left',
      interactive: true,
    });

    $('.tooltipster-bottom').tooltipster({
      maxWidth: 300,
      theme: 'tooltipster-shadow',
      side: 'bottom',
      interactive: true,
    });
    
}

$(function() {
    gerar_tooltipster();
});