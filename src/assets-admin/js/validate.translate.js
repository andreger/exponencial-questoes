jQuery.extend(jQuery.validator.messages, {
    required: "Este campo é necessário.",
    remote: "Por favor, corrija este campo.",
    email: "Por favor, insira um e-mail válido.",
    url: "Por favor, insira uma URL válida.",
    date: "Por favor, insira uma data válida.",
    dateISO: "Por favor, insira uma data válida (ISO).",
    number: "Por favor, insira um número válido.",
    digits: "Por favor, insira somente dígitos.",
    creditcard: "Por favor, insira um numero de cartão de crédito válido.",
    equalTo: "Por favor, insira o mesmo valor novamente.",
    accept: "Por favor, insira um valor com extensão válida.",
    maxlength: jQuery.validator.format("Por favor, não entre mais que {0} caracteres."),
    minlength: jQuery.validator.format("Por favor, insira, pelo menos, {0} caracteres."),
    rangelength: jQuery.validator.format("Por favor, insira um valor que tenha entre {0} e {1} caracteres."),
    range: jQuery.validator.format("Por favor, insira um valor entre {0} e {1}."),
    max: jQuery.validator.format("Por favor, insira um valor menor ou igual a {0}."),
    min: jQuery.validator.format("Por favor, insira um valor maior ou igual a {0}.")
});