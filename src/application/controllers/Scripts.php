<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Scripts extends CI_Controller {
	
	public function __construct() 
	{
		parent::__construct ();
				
		$this->load->model ( 'disciplina_model' );
		$this->load->model ( 'banca_model' );
		$this->load->model ( 'orgao_model' );
		$this->load->model ( 'cargo_model' );
		$this->load->model ( 'assunto_model' );
		$this->load->model ( 'prova_model' );
		$this->load->model ( 'questao_model' );
		$this->load->model ( 'area_formacao_model' );
		$this->load->model ( 'area_atuacao_model' );
		$this->load->model ( 'comentario_model' );
		$this->load->model ( 'simulado_model' );
		
		$this->output->enable_profiler(TRUE);
	}

	public function atualizar_provas()
	{
		set_time_limit(360);

		$reader = ReaderFactory::create(Type::XLSX); // for XLSX files

		$filePath = $_SERVER['DOCUMENT_ROOT'] . '/questoes/assets-admin/xls/areas.xlsx';
		$reader->open($filePath);

		echo "Trocando banca 160<br>";
		$this->prova_model->trocar_banca(160, 128);

		echo "Trocando banca 208<br>";
		$this->prova_model->trocar_banca(208, 165);

		foreach ($reader->getSheetIterator() as $sheet) {

		    foreach ($sheet->getRowIterator() as $row) {

		    	$prova_id = $row[0];

		    	if($prova_id == 'ID_PROVA') continue;
		    	if($prova_id == 'ID_BANCA') exit;

		    	$escolaridade_id = $row[8];
		    	$area_antiga = $row[10];
		    	$area_nova = $row[13];

				echo "Atualizando prova {$prova_id}<br>"; 
		    	$this->prova_model->atualizar_escolaridade($prova_id, $escolaridade_id);
		    	$this->prova_model->excluir_prova_area_atuacao($prova_id, $area_antiga);
		    	$this->prova_model->adicionar_area_atuacao($prova_id, $area_nova);
		    }

		    break;
		}

		$reader->close();
	}

	public function atualizar_comentarios()
	{
		$pulo = 100;

		$p = $_GET['p'] ? $_GET['p'] : 0;

		if($p == 0) {
			$this->comentario_model->resetar_destaque($comentario);
		}

		$_SESSION['sc_com_id'] = isset($_SESSION['sc_com_id']) ? $_SESSION['sc_com_id'] + $pulo : 1;

		echo $p . " de " . 	$this->comentario_model->contar_todos();

		$comentarios = $this->comentario_model->listar_a_partir($p, $pulo);
		// $comentarios = $this->comentario_model->listar_a_partir(574, 1);


		foreach ($comentarios as $comentario) {
			$this->comentario_model->atualizar_destaque($comentario);
		}


		$p = $p + $pulo;
		echo "<script>location.href='/questoes/scripts/atualizar_comentarios?p={$p}';</script>";
	}
	
	public function resetar_tipo_questao()
	{
		$this->questao_model->atualizar_todos_tipos_como_nulo();
	}
	
	public function update_url_reduzida()
	{
		$this->load->helper('google');
		
		$questoes = $this->questao_model->listar_questoes_sem_url_reduzida(150);
		
		foreach ($questoes as $questao) {
			$url = get_shorten_url(get_questao_url($questao['que_id']));

			echo "Processando questão {$questao['que_id']}...";
			if($url) {
				$this->questao_model->atualizar_url_reduzida($questao['que_id'], $url);
				echo "URL: {$url} <br>";
			}
			else {
				echo "Não processado <br>";
			}
		}
	}
	
	public function update_url_reduzida_simulados()
	{
		$this->load->helper('google');
		
		$this->db->from('simulados_questoes sm');
		$this->db->join('questoes q', 'sm.que_id = q.que_id');
		$this->db->where('q.que_url_reduzida', null);	
		$query = $this->db->get();
		
		$questoes = $query->result_array();
		
		echo "Questões a serem tratadas: " . count($questoes). "<br><br>";
		
		foreach ($questoes as $questao) {
			$url = get_shorten_url(get_questao_url($questao['que_id']));
			$this->questao_model->atualizar_url_reduzida($questao['que_id'], $url);
			echo "Processando questão {$questao['que_id']}... URL: {$url} <br>";
		}
	}
	
	public function update_tipo_questao() {
		$questoes = $this->questao_model->listar_questoes_com_tipo_nulo();
		foreach ($questoes as $questao) {
			$opcoes = $this->questao_model->contar_opcoes($questao['que_id']);
			
			if($opcoes == 5){
				$tipo = MULTIPLA_ESCOLHA_5;
			}elseif($opcoes == 0){
				$tipo = CERTO_ERRADO;
			}else{
				$tipo = MULTIPLA_ESCOLHA;
			}
			
			$this->questao_model->atualizar_tipo($questao['que_id'], $tipo);	
		}
		
		$todas = $this->questao_model->contar_todas();
		$nulas = $this->questao_model->contar_questoes_com_tipo_nulo();
		$data['concluidas'] = $todas - $nulas; 
		$data['todas'] = $todas;
		$data['percentual'] = get_percentual($data['concluidas'], $todas);
		
		$this->load->view('scripts/update_tipo_questao', $data);
	}
	
	public function update_disciplina_de_questao($primeiro_id = 0)
	{
		$questoes = $this->questao_model->listar_questoes_com_disciplina_nula($primeiro_id);

		foreach ($questoes as $questao) {
			if($questao['disciplina_id']) {
				$this->questao_model->atualizar_disciplina($questao['que_id'], $questao['disciplina_id']);
			}
			
			$proximo_id = $questao['que_id'];
		}
		
		$todas = $this->questao_model->contar_todas();
		$nulas = $this->questao_model->contar_questoes_com_disciplina_nula($primeiro_id);
		$data['concluidas'] = $todas - $nulas;
		$data['todas'] = $todas;
		$data['percentual'] = get_percentual($data['concluidas'], $todas);
		$data['url'] = 'http://' . $_SERVER['HTTP_HOST'] . '/questoes/scripts/update_disciplina_de_questao/' . $proximo_id;
		
		$this->load->view('scripts/update_disciplina_de_questao', $data);
	}
	
	# incluir nsae01.casimages.net		
	public function copiar_imagens()
	{
		$this->load->model('Questao_model');
		
		$this->db->like('que_enunciado', 's3.amazonaws.com');
		$this->db->from('questoes');
		$total_questoes = $this->db->count_all_results();
		echo "Ainda há {$total_questoes} questoes com imagens";
		
		$this->db->like('que_enunciado', 's3.amazonaws.com');
		$this->db->from('questoes');
		$this->db->limit(10);
		$query = $this->db->get();
		
		$result = $query->result_array();
		

		foreach ($result as $item) {
			$html = str_get_html($item['que_enunciado']);
			
			$i = 0;
			$elementos = $html->find('img');
			foreach ($elementos as $elemento) {
				$i = $i + 1;
				$qcon_nome_imagem = $elemento->src;

				$qcon_nome_imagem_a = explode('/', $qcon_nome_imagem);
				$qcon_nome_imagem_a[count($qcon_nome_imagem_a) - 1] = urlencode($qcon_nome_imagem_a[count($qcon_nome_imagem_a) - 1]);

				$qcon_nome_imagem = implode('/', $qcon_nome_imagem_a);
				$qcon_nome_imagem = str_replace("%25", "%", $qcon_nome_imagem);
				$qcon_nome_imagem = str_replace("%5C%22", "", $qcon_nome_imagem);
				$qcon_nome_imagem = str_replace("\\\"", "", $qcon_nome_imagem);
				
				# incluir nsae01.casimages.net				
				if(strpos($qcon_nome_imagem,'s3.amazonaws.com') === false) continue;
				
				$expo_relativo = "/questoes/uploads/imagens/{$item['que_id']}_{$i}.jpg";
				$expo_absoluto = $_SERVER['DOCUMENT_ROOT'] . $expo_relativo;
				
				copy($qcon_nome_imagem, $expo_absoluto);
				self::remover_se_for_invalido($expo_absoluto);
				
				$novo_enunciado = str_replace("\\\"", '"', $item['que_enunciado']);
				$novo_enunciado = str_replace($qcon_nome_imagem, $expo_relativo, $novo_enunciado);
				
				$dados = array(
						'qim_original_url' => $qcon_nome_imagem,
						'qim_url' => $expo_relativo,
						'qim_tratada' => 0,
						'que_id' => $item['que_id']
				);
				
				$this->Questao_model->salvar_questao_imagem($dados);
				$this->Questao_model->atualizar_enunciado($item['que_id'], $novo_enunciado);
			}
		}
// 		$this->load->view('buscador/refresh');
	}
	
	public function copiar_imagens_texto_base()
	{		
		$this->load->model('Questao_model');
	
		$this->db->like('que_texto_base', 's3.amazonaws.com');
		$this->db->from('questoes');
		$total_questoes = $this->db->count_all_results();
		echo "Ainda há {$total_questoes} textos-bases com imagens";
	
		$this->db->like('que_texto_base', 's3.amazonaws.com');
		$this->db->from('questoes');
		$this->db->limit(10);
		$query = $this->db->get();
	
		$result = $query->result_array();
	
	
		foreach ($result as $item) {
			$html = str_get_html($item['que_texto_base']);
				
			$i = 0;
			$elementos = $html->find('img');
			foreach ($elementos as $elemento) {
				$i = $i + 1;
				$qcon_nome_imagem = $elemento->src;
				$qcon_nome_imagem = stripslashes($qcon_nome_imagem);
				$qcon_nome_imagem = str_replace('"', "", $qcon_nome_imagem);
				$qcon_nome_imagem = str_replace("'", "", $qcon_nome_imagem);

				$qcon_nome_imagem_a = explode('/', $qcon_nome_imagem);
				$qcon_nome_imagem_a[count($qcon_nome_imagem_a) - 1] = urlencode($qcon_nome_imagem_a[count($qcon_nome_imagem_a) - 1]);

				$qcon_nome_imagem = implode('/', $qcon_nome_imagem_a);
				$qcon_nome_imagem = str_replace("%25", "%", $qcon_nome_imagem);
				$qcon_nome_imagem = str_replace("%5C%22", "", $qcon_nome_imagem);
				$qcon_nome_imagem = str_replace("\\\"", "", $qcon_nome_imagem);
				
				if(strpos($qcon_nome_imagem,'s3.amazonaws.com') === false) continue;
	
				$expo_relativo = "/questoes/uploads/imagens/{$item['que_id']}_{$i}_tb.jpg";
				$expo_absoluto = $_SERVER['DOCUMENT_ROOT'] . $expo_relativo;
	
// 				echo "<br>" . stripslashes($qcon_nome_imagem) . "<br>";
				echo $expo_absoluto . "<br>";
				
				copy($qcon_nome_imagem, $expo_absoluto);
				self::remover_se_for_invalido($expo_absoluto);
	
				$novo_texto_base = str_replace("\\\"", '"', $item['que_texto_base']);
				$novo_texto_base = str_replace($qcon_nome_imagem, $expo_relativo, $novo_texto_base);
	
				$dados = array(
						'qim_original_url' => $qcon_nome_imagem,
						'qim_url' => $expo_relativo,
						'qim_tratada' => 0,
						'que_id' => $item['que_id']
				);
	
				$this->Questao_model->salvar_questao_imagem($dados);
				$this->Questao_model->atualizar_texto_base($item['que_id'], $novo_texto_base);
			}
		}
		// 		$this->load->view('buscador/refresh');
	}
	
	public function copiar_imagens_opcoes()
	{
		$this->load->model('Questao_model');
	
		$this->db->like('qop_texto', 's3.amazonaws.com');
		$this->db->from('questoes_opcoes');
		$total_questoes = $this->db->count_all_results();
		echo "Ainda ha {$total_questoes} opcoes de questoes com imagens";
	
		$this->db->like('qop_texto', 's3.amazonaws.com');
		$this->db->from('questoes_opcoes');
		$this->db->limit(10);
		$query = $this->db->get();
	
		$result = $query->result_array();
	
	
		foreach ($result as $item) {
			$html = str_get_html($item['qop_texto']);
				
			$i = 0;
			$elementos = $html->find('img');
			foreach ($elementos as $elemento) {
				$i = $i + 1;
				$qcon_nome_imagem = $elemento->src;

				$qcon_nome_imagem_a = explode('/', $qcon_nome_imagem);
				$qcon_nome_imagem_a[count($qcon_nome_imagem_a) - 1] = urlencode($qcon_nome_imagem_a[count($qcon_nome_imagem_a) - 1]);

				$qcon_nome_imagem = implode('/', $qcon_nome_imagem_a);
				$qcon_nome_imagem = str_replace("%25", "%", $qcon_nome_imagem);
				$qcon_nome_imagem = str_replace("%5C%22", "", $qcon_nome_imagem);
				$qcon_nome_imagem = str_replace("\\\"", "", $qcon_nome_imagem);

				if(strpos($qcon_nome_imagem,'s3.amazonaws.com') === false) continue;
	
				$expo_relativo = "/questoes/uploads/imagens/{$item['que_id']}_{$item['qop_id']}_{$i}.jpg";
				$expo_absoluto = $_SERVER['DOCUMENT_ROOT'] . $expo_relativo;
	
				copy($qcon_nome_imagem, $expo_absoluto);
				self::remover_se_for_invalido($expo_absoluto);
	
				$novo_texto = str_replace("\\\"", '"', $item['qop_texto']);
				$novo_texto = str_replace($qcon_nome_imagem, $expo_relativo, $novo_texto);
	
				// echo "<br>";
				// echo $qcon_nome_imagem . "<br>";
				// echo $expo_relativo . "<br>";
				// echo $novo_texto;exit;

				$dados = array(
						'qim_original_url' => $qcon_nome_imagem,
						'qim_url' => $expo_relativo,
						'qim_tratada' => 0,
						'que_id' => $item['que_id']
				);
	
				$this->Questao_model->salvar_questao_imagem($dados);
				$this->Questao_model->atualizar_texto_opcao($item['qop_id'], $novo_texto);
			}
		}
// 		$this->load->view('buscador/refresh');
	}
	
	public function corrigir_imagens_com_espaco()
	{
		$this->db->like('qim_original_url', ' ');
		$query = $this->db->get('questoes_imagens');
		
		$imagens = $query->result_array();
		foreach ($imagens as $imagem) {
			$nova_url = str_replace(" ","%20", $imagem['qim_original_url']);
			copy($nova_url, $_SERVER['DOCUMENT_ROOT'] . $imagem['qim_url']);
			
			$this->db->where('qim_id', $imagem['qim_id']);
			$this->db->set('qim_original_url', $nova_url);
			$this->db->update('questoes_imagens');
		}
	}
	
	private function remover_se_for_invalido($caminho)
	{
		if(!getimagesize($caminho)) {
			unlink($caminho);
		}
	}
	
	/**********************************************************************
	 * Lista todas as disciplinas que não possuem o assunto "Sem assunto" e
	 * adiciona o "Sem assunto" para essas disciplinas
	 **********************************************************************/
	public function criar_assunto_sem_assunto()
	{
		$disciplinas = $this->disciplina_model->listar_sem_assunto();
		
		foreach ($disciplinas as $disciplina) {
			$assunto = array(
				'ass_nome' => SEM_ASSUNTO,
				'dis_id' => $disciplina['dis_id']
			);
			
			$this->assunto_model->salvar($assunto);
		}
	}
	
	/**********************************************************************
	 * Lista todas as questoes que não possuem assuntos e adiciona o 
	 * "Sem assunto" para essas questoes
	 **********************************************************************/
	public function atualiza_questoes_sem_assunto()
	{
		$this->db->from('questoes q');
		$this->db->join('questoes_assuntos qa', 'q.que_id = qa.que_id', 'left');
		$this->db->where('ass_id', null);
		$num_questoes = $this->db->count_all_results();
		
		echo "Existem {$num_questoes} questoes sem assuntos...<br>";
		
		$this->db->select('q.que_id, q.dis_id, qa.ass_id');
		$this->db->from('questoes q');
		$this->db->join('questoes_assuntos qa', 'q.que_id = qa.que_id', 'left');
		$this->db->where('ass_id', null);
		$this->db->limit(200);
		$query = $this->db->get();
		
		$questoes = $query->result_array();
		
		foreach ($questoes as $questao) {
			$assunto = $this->assunto_model->get_by_nome_e_disciplina(SEM_ASSUNTO, $questao['dis_id']);

			if($assunto) {
				$this->questao_model->salvar_assunto_em_questao($assunto, $questao);
			}
		}
		
		$this->load->view('buscador/refresh');
	}
	
	/**********************************************************************
	 * Lista todas as questoes que possuem assuntos e possuem o "Sem assunto".
	 * Além disso remove o "Sem assunto dessas questões"
	 **********************************************************************/
	public function atualiza_questoes_com_assunto()
	{
		$this->db->select('q.que_id');
		$this->db->from('questoes q');
		$this->db->join('questoes_assuntos qa', 'q.que_id = qa.que_id');
		$this->db->join('assuntos a', 'qa.ass_id = a.ass_id');
		$this->db->where('ass_nome', "(Sem assunto)");

		$query = $this->db->get();
		$resultado = $query->result_array();
	
		$total = count($resultado);
	
		$i = 1;
		foreach ($resultado as $item) {
			set_time_limit(360);

			echo "Processando $i de $total<br>";

			$this->db->where('que_id', $item['que_id']);
			$this->db->from('questoes_assuntos');
			$query = $this->db->get();

			$qas = $query->result_array();

			if(count($qas) > 1) {
				$assuntos_ids = [];
				foreach ($qas as $qa) {
					array_push($assuntos_ids, $qa['ass_id']);
				}

				$this->questao_model->atualizar_assuntos($item['que_id'], $assuntos_ids);
			}

			$i++;
		}
			
		$this->load->view('buscador/refresh');
	}
	
	
	public function update_p_br_p_summernote()
	{
		$pbrp = "<p><br></p>";
		
		$this->db->where('que_texto_base', $pbrp);
		$this->db->set('que_texto_base', '');
		$this->db->update('questoes');
		
		$this->db->where('que_enunciado', $pbrp);
		$this->db->set('que_enunciado', '');
		$this->db->update('questoes');
		
		$this->db->where('qop_texto', $pbrp);
		$this->db->set('qop_texto', '');
		$this->db->update('questoes_opcoes');
	}
	
	public function update_resposta_zerada()
	{
		$this->db->where('que_resposta', 0);
		$this->db->set('que_resposta', null);
		$this->db->update('questoes');
	}
	
	public function atualizar_acentos()
	{
		header ('Content-type: text/html; charset=UTF-8');
		
		$acentos = array(
			"&Agrave;" 	=> "À",
			"&Aacute;" 	=> "Á",
			"&Acirc;" 	=> "Â",
			"&Atilde;" 	=> "Ã",
			"&Auml;" 	=> "Ä",
			"&Aring;" 	=> "Å",
			"&AElig;" 	=> "Æ",
			"&Ccedil;" 	=> "Ç",
			"&Egrave;" 	=> "È",
			"&Eacute;" 	=> "É",
			"&Ecirc;" 	=> "Ê",
			"&Euml;" 	=> "Ë",
			"&Igrave;" 	=> "Ì",
			"&Iacute;" 	=> "Í",
			"&Icirc;" 	=> "Î",
			"&Iuml;" 	=> "Ï",
			"&ETH;" 	=> "Ð",
			"&Ntilde;" 	=> "Ñ",
			"&Ograve;" 	=> "Ò",
			"&Oacute;" 	=> "Ó",
			"&Ocirc;" 	=> "Ô",
			"&Otilde;" 	=> "Õ",
			"&Ouml;"	=> "Ö",
			"&Oslash;" 	=> "Ø",
			"&OElig;" 	=> "Œ",
			"&Scaron;" 	=> "Š",
			"&Ugrave;" 	=> "Ù",
			"&Uacute;" 	=> "Ú",
			"&Ucirc;" 	=> "Û",
			"&Uuml;" 	=> "Ü",
			"&Yacute;" 	=> "Ý",
			"&THORN;" 	=> "Þ",
			"&szlig;" 	=> "ß",
			"&agrave;"	=> "à",
			"&aacute;" 	=> "á",
			"&acirc;" 	=> "â",
			"&atilde;" 	=> "ã",
			"&auml;" 	=> "ä",
			"&aring;" 	=> "å",
			"&aelig;" 	=> "æ",
			"&ccedil;" 	=> "ç",
			"&egrave;" 	=> "è",
			"&eacute;" 	=> "é",
			"&ecirc;" 	=> "ê",
			"&euml;" 	=> "ë",
			"&igrave;" 	=> "ì",
			"&iacute;" 	=> "í",
			"&icirc;" 	=> "î",
			"&iuml;" 	=> "ï",
			"&eth;" 	=> "ð",
			"&ntilde;" 	=> "ñ",
			"&ograve;" 	=> "ò",
			"&oacute;" 	=> "ó",
			"&ocirc;" 	=> "ô",
			"&otilde;" 	=> "õ",
			"&ouml;" 	=> "ö",
			"&oslash;" 	=> "ø",
			"&oelig;" 	=> "œ",
			"&scaron;" 	=> "š",
			"&ugrave;" 	=> "ù",
			"&uacute;" 	=> "ú",
			"&ucirc;" 	=> "û",
			"&uuml;" 	=> "ü",
			"&yacute;" 	=> "ý",
			"&yuml;" 	=> "ÿ",
			"&thorn;" 	=> "þ",
		);
		
		foreach ($acentos as $termo => $acento) {
			set_time_limit(360);
			echo "Termo {$termo} em ENUNCIADO... <br>";
			$this->questao_model->atualizar_termos('questoes', 'que_enunciado', $termo, $acento);
			echo "Termo {$termo} em TEXTO BASE... <br>";
			$this->questao_model->atualizar_termos('questoes', 'que_texto_base', $termo, $acento);
			echo "Termo {$termo} em OPÇÕES... <br>";
			$this->questao_model->atualizar_termos('questoes_opcoes', 'qop_texto', $termo, $acento);
			
			
			/*$questoes = $this->questao_model->buscar_termo('que_texto_base', $termo);
			
			echo "Textos-bases com {$termo}: " . count($questoes) . "<br>";
			
			foreach ($questoes as $questao) {
				$novo = str_replace($termo, $acento, $questao['que_texto_base']);
				$this->questao_model->atualizar_texto_base($questao['que_id'], $novo);
			}
			
			$questoes = $this->questao_model->buscar_termo('que_enunciado', $termo);
			
			echo "Enunciados com {$termo}: " . count($questoes) . "<br>";
			
			foreach ($questoes as $questao) {
				$novo = str_replace($termo, $acento, $questao['que_enunciado']);
				$this->questao_model->atualizar_enunciado($questao['que_id'], $novo);
			}
			
			$opcoes = $this->questao_model->buscar_termo_opcoes('qop_texto', $termo);
			
			echo "Opções com {$termo}: " . count($opcoes) . "<br>";
			
			foreach ($opcoes as $opcao) {
				$novo = str_replace($termo, $acento, $opcao['qop_texto']);
				$this->questao_model->atualizar_texto_opcao($opcao['qop_id'], $novo);
			}
			*/			
		}
	}

	public function atualizar_questoes_inativas()
	{
		$questoes = $this->questao_model->listar_questoes_ativas_imagens_nao_tratadas();

		foreach ($questoes as $questao) {
			set_time_limit(30);			
			$this->questao_model->marcar_questao_como_inativa($questao['que_id']);
		}
	}

	public function atualizar_campo_busca()
	{
		$this->load->helper('questao');

		$questoes = $this->questao_model->listar_questoes_sem_campo_busca();

		if($questoes) {
			foreach ($questoes as $questao) {
				set_time_limit(30);

				$this->questao_model->atualizar_campo_busca($questao['que_id']);
			}

			$this->load->view('buscador/refresh');
		}
		else {
			echo "Todas as questões já foram atualizadas";
		}
	}

	public function atualizar_imagens_erradas()
	{
		$imagens = $this->questao_model->listar_imagens_erradas();

		if($imagens) {
			foreach($imagens as $imagem) {
				set_time_limit(30);

				$questao = $this->questao_model->get_by_id($imagem['que_id']);
				$html = str_get_html(stripcslashes($questao['que_texto_base']));

				$ia = explode('_', $imagem['qim_url']);

				$elemento = $html->find('img', $ia[1] - 1);

				if($elemento) {
					if($elemento->src && $elemento->alt) {
						$nome_original = str_replace("Imagem", $elemento->alt, $imagem['qim_original_url']);
						$nome_original = str_replace(" ", "%20", $nome_original);

						copy($nome_original, $_SERVER['DOCUMENT_ROOT'] . $imagem['qim_url']);
						$novo_texto_base = str_replace($elemento->src, $imagem['qim_url'], $questao['que_texto_base']);

						$this->questao_model->atualizar_texto_base($imagem['que_id'], $novo_texto_base);
					}
					else {
						$nome_original = $imagem['qim_original_url'] . '*';	
					}
				}
				else {
					$nome_original = $imagem['qim_original_url'] . '*';
				}

				$this->questao_model->atualizar_questao_imagem($imagem['qim_id'], array('qim_original_url' => $nome_original) );
			}

			$this->load->view('buscador/refresh');
		}
		else {
			echo "Todas as imagens foram atualizadas";
		}


	}

	public function atualizar_respostas_cadernos_simulados()
	{
		$respostas = $this->questao_model->listar_respostas_usuarios_cadernos();

		foreach ($respostas as $resposta) {
			set_time_limit(30);
			
			$this->questao_model->salvar_resposta_usuario($resposta['usu_id'], $resposta['que_id'], $resposta['qre_selecao']);
		}


		$respostas = $this->questao_model->listar_respostas_usuarios_simulados();

		foreach ($respostas as $resposta) {
			set_time_limit(30);

			$this->questao_model->salvar_resposta_usuario($resposta['usu_id'], $resposta['que_id'], $resposta['qre_selecao']);
		}

	}

	public function atualizar_status_questoes_imagens_tratadas()
	{
		$imagens = $this->questao_model->listar_imagens_tratadas_de_questoes_inativas();

		foreach($imagens as $imagem) {
			// echo "Imagem {$imagem['qim_id']}... ";

			set_time_limit(30);
			// conta imagens não tratadas
			$num = $this->questao_model->contar_imagens_de_questao($imagem['que_id']);

			if($num == 0) {
				$this->questao_model->marcar_questao_como_ativa($imagem['que_id']);
			}

		}

	}

	public function rebuscar_imagens($pagina = 0)
	{
		$imagens = $this->questao_model->listar_imagens_nao_tratadas('imagem*', 10, $pagina);
		foreach ($imagens as $imagem) {
			set_time_limit(120);

			$a = explode('/', $imagem['qim_original_url']);

			$a[count($a) - 1] = urlencode($a[count($a) - 1]);
			$a = str_replace("%25", "%", $a);

			$a = implode('/', $a);

			echo $a . "<br>";
			copy($a, $_SERVER['DOCUMENT_ROOT'] . $imagem['qim_url']);	
		}

		$pagina++;
		echo "<script>location.href = '/questoes/scripts/rebuscar_imagens/{$pagina}'; </script>)";		
	}

	public function atualizar_questoes_disciplinas_excluidas_simulados()
	{
		$simulados = $this->simulado_model->listar();

		foreach ($simulados as $simulado) {
			$this->simulado_model->remover_questoes_disciplinas_excluidas($simulado['sim_id']);
		}
	}

	/******************************************************************************
	 * Ativar todas as questões sem imagem que possuem resposta e estão inativas
	 * https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1760
	 *****************************************************************************/


	public function desmarcar_sem_imagem_questoes_inativas()
	{
		$this->questao_model->desmarcar_sem_imagem_questoes_inativas();
		echo "Script finalizado";
		exit;
	}

	/**
	 * Marca as questões sem imagens
	 * 
	 * CRONJOB:
	 * 
	 * * /10   0-6,22-23	*	*	*
	 * 
	 * wget -O /dev/null 
	 * 
	 * https://www.exponencialconcursos.com.br/questoes/scripts/marcar_questoes_sem_imagens
	 * 
	 */

	public function marcar_questoes_sem_imagens()
	{
		// $num = $this->questao_model->contar_questoes_inativas_nao_marcadas();

		// echo "Questoes inativas nao marcadas: $num";

		$questoes = $this->questao_model->listar_questoes_inativas_nao_marcadas();

		if($questoes) {
			echo "Aguarde... Executando script...";
		}
		else {
			echo "Script finalizado";
			exit;
		}

		foreach ($questoes as $questao) {

			$this->questao_model->marcar_sem_imagem($questao['que_id'], 1);	

			// se não tem resposta marca como se tivesse imagem
			// if(!$questao['que_resposta']) {
			// 	$this->questao_model->marcar_sem_imagem($questao['que_id'], 0);
			// }

			// marca zero se texto base tiver imagem
			if(strpos($questao['que_texto_base'], 'img') !== FALSE) {
				$this->questao_model->marcar_sem_imagem($questao['que_id'], 0);	
			}

			// marca zero se enunciado tiver imagem
			if(strpos($questao['que_enunciado'], 'img') !== FALSE) {
				$this->questao_model->marcar_sem_imagem($questao['que_id'], 0);	
			}

			$opcoes = $this->questao_model->listar_opcoes($questao['que_id']);

			if($opcoes) {
				foreach ($opcoes as $opcao) {
					// marca zero se tiver imagem em uma das opcoes
					if(strpos($opcao['qop_texto'], 'img') !== FALSE) {
						$this->questao_model->marcar_sem_imagem($questao['que_id'], 0);
						break;
					}
				}

			}

		}

		$this->load->view('buscador/refresh');
	}

	/**
	 * Ativa as questões sem imagens
	 * 
	 * CRONJOB:
	 * 
	 * 4	4	*	*	*	
	 * 
	 * wget -O /dev/null 
	 * 
	 * https://www.exponencialconcursos.com.br/questoes/scripts/ativar_questoes_sem_imagens
	 * 
	 */

	public function ativar_questoes_sem_imagens()
	{
		$num = $this->questao_model->contar_questoes_inativas_sem_imagem();

		echo "Questoes inativas sem imagem: $num";

		$questoes = $this->questao_model->listar_questoes_inativas_sem_imagem(1000);

		foreach ($questoes as $questao) {

			$this->questao_model->atualizar_ativo_inativo($questao['que_id'], 1);	
		}

		$this->load->view('buscador/refresh');
	}


	public function iniciar_questoes_provas()
	{
		$restantes = $this->questao_model->contar_questoes_provas_nao_inicializadas();

		if($restantes) {
			echo "Script em execução. Restando $restantes questão(ões)...";
			$questoes_provas = $this->questao_model->buscar_questoes_provas_nao_inicializadas();
			$this->questao_model->salvar_questoes_provas($questoes_provas);				
		}
		else {
			echo "Script encerrado com sucesso.";
		}

		$this->load->view('buscador/refresh');

	}

	/**
	 * Corrige os assuntos incorretos em questões
	 * 
	 * @since k2
	 * 
	 */

	public function corrigir_questoes_assuntos()
	{
		$this->load->helper('questao');

		self::criar_assunto_sem_assunto();

		// Lista as questões que possuem assuntos diferentes da disciplina da questão
		if($questoes = $this->questao_model->listar_questoes_com_assuntos_incorretos()) {

			foreach ($questoes as $questao) {
				$antes = get_assuntos_questao_str($questao['que_id']);

				// remove os assuntos diferentes da disciplina da questão	
				$this->questao_model->remover_assunto_de_questao($questao['ass_id'], $questao['que_id']);

				// verifica se possui outro assunto
				if($this->questao_model->contar_assuntos_questao($questao['que_id']) == 0) {
					
					// recupera o "sem assunto" da disciplina
					$sem_assunto = $this->assunto_model->get_by_nome_e_disciplina(SEM_ASSUNTO, $questao['q_dis_id']);

					// adiciona o "sem assunto" na questão
					if($sem_assunto) {
						$this->questao_model->salvar_assunto_em_questao(['ass_id' => $sem_assunto['ass_id']], ['que_id' => $questao['que_id']]);
					}
				}

				$depois = get_assuntos_questao_str($questao['que_id']);

				$this->questao_model->salvar_questao_assunto_corretor([
					'que_id' => $questao['que_id'],
					'qac_antes' => $antes,
					'qac_depois' => $depois,
					'qac_data_hora' => get_data_hora_agora()
				]);
			}
		}		

			// Lista as questões que não possuem nenhum assunto ativo
		if($questoes = $this->questao_model->listar_questoes_com_nenhum_assunto_ativo()) {

			foreach ($questoes as $questao) {

				echo "Questao: {$questao['que_id']}<br>";

				$antes = get_assuntos_questao_str($questao['que_id']);

				echo "Antes: {$antes}<br>";

				// remove todos os assuntos da questão	
				$this->questao_model->remover_todos_assuntos_de_questao($questao['que_id']);

				// recupera o "sem assunto" da disciplina
				$sem_assunto = $this->assunto_model->get_by_nome_e_disciplina(SEM_ASSUNTO, $questao['dis_id']);

				echo "Sem Assunto: {$sem_assunto['ass_id']}<br>";

				// adiciona o "sem assunto" na questão
				if($sem_assunto) {
					$this->questao_model->salvar_assunto_em_questao(['ass_id' => $sem_assunto['ass_id']], ['que_id' => $questao['que_id']]);
				}

				$depois = get_assuntos_questao_str($questao['que_id']);

				echo "Antes: {$depois}<br>";

				$this->questao_model->salvar_questao_assunto_corretor([
					'que_id' => $questao['que_id'],
					'qac_antes' => $antes,
					'qac_depois' => $depois,
					'qac_data_hora' => get_data_hora_agora()
				]);
			}		
		}

		// $this->load->view('buscador/refresh');
	}

}