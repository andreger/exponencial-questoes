<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

KLoader::helper("AcessoGrupoHelper");

class Cadernos extends CI_Controller {
	

	public function __construct() {
		parent::__construct ();

		if(is_area_desativada(array(PAINEL_SQ_GERAL))){
			redirecionar_erro_500();
		}

		session_start();

		$this->load->model ( 'caderno_model' );
		$this->load->model ( 'questao_model' );

		$this->load->helper('caderno');
		$this->load->helper('questao');
		$this->load->helper('aluno');

		//$this->output->enable_profiler(true);
		
		init_profiler();
	}
	
	public function embaralhar_questoes($caderno_id){
		$redirect_url = get_meus_cadernos_url();

		if(isset($_GET['ref']) && $_GET['ref']) {
			$redirect_url = $_GET['ref'];
		}

		$this->caderno_model->embaralhar_questoes($caderno_id);
		set_mensagem(SUCESSO, "Questões embaralhadas com sucesso.");
		redirect($redirect_url);
	}

	/**
	 * Redireciona para uma questão aleatória de um caderno
	 * 
	 * @since L1
	 * 
	 * @param int $caderno_id ID do caderno
	 * @param int $total_questoes Total de questões no caderno, esse valor muda caso o caderno esteja com filtro
	 * 
	 */
	public function ir_para_questao_aleatoria($caderno_id, $total_questoes){

		$query_string = http_build_query($_GET);
		if($query_string){
			$query_string = "?" . $query_string;
		}

		$aleatoria = mt_rand(0, $total_questoes -1);
		redirect(get_resolver_caderno_url($caderno_id, $aleatoria) . $query_string);
	}


	public function alterar_questoes_por_pagina($questoes_por_pagina, $caderno_id, $offset = 1)
	{
		$_SESSION['qpp'] = $questoes_por_pagina;
		$novo_offset =  get_novo_offset($questoes_por_pagina, $offset);

		$sufixo = $novo_offset ? $novo_offset : NULL;
		
		$query_string = http_build_query($_GET);
		if($query_string)
		{
			$query_string = "?" . $query_string;
		}

		redirect(get_resolver_caderno_url($caderno_id, $sufixo) . $query_string);
	}

	/**
	 * Altera o modo de visualizar a resolução de caderno e redireciona para a tela de resolver caderno
	 * 
	 * @since L1
	 * 
	 * @param string $modo Tipo de visulização das questões: MODO_QUESTOES_XXX
	 * @param int $caderno_id ID do caderno que está sendo resolvido
	 * @param int $offset Offset atual da visualização de questões
	 * 
	 */
	public function alterar_modo_resolver_questoes($modo, $caderno_id, $offset = 0)
	{
		$qqp = $_SESSION['qpp'];

		//Se o modo não mudar não mudo nada
		if($_SESSION['mrq'] != $modo){
			
			$_SESSION['mrq'] = $modo;

			if($_SESSION['mrq'] == MODO_QUESTOES_COCKPIT){
				$qqp = 1;//Cockpit é de um em um
			}else{
				$qqp = 10;//Padrão
			}
		}

		self::alterar_questoes_por_pagina($qqp, $caderno_id, $offset);
	}

	public function acesso_meus_cadernos()
	{
		$_SESSION['meus_cadernos_filtro'] = array();
		self::limpar_categoria();
	}

	public function limpar_categoria()
	{
		$_SESSION['categoria_filtro'] = null;
		redirect("questoes/cadernos/meus_cadernos");
	}
	
	public function meus_cadernos($categoria_id = 0, $offset = null)
	{	
		$conf_usuario = get_config_aluno();
		
		$data['check_acertos'] = $conf_usuario  ?  $conf_usuario['mostrar_acertos'] : 1; 
		$data['check_filtros'] = $conf_usuario  ?  $conf_usuario['exibir_filtros'] : 1; 
		$data['check_historico'] = $conf_usuario  ?  $conf_usuario['manter_historico_marcacao_cadernos'] : 1; 
		$data['check_url_cad'] = $conf_usuario  ?  $conf_usuario['exibir_url_imprimir_caderno'] : 1;  
		
		if(is_area_desativada(array(PAINEL_SQ_CADERNOS))){
			redirecionar_erro_500();
		}

		if($offset == null){
			$offset = 0;
		}
		
		if ($categoria_id != 0) 
		{
			$_SESSION['categoria_filtro'] = $categoria_id;
		}
		elseif ($_SESSION['categoria_filtro'] && $categoria_id == 0) 
		{
			$categoria_id = $_SESSION['categoria_filtro'];
		}	
		// tem_acesso(array(
		// 		ADMINISTRADOR, PROFESSOR, CONSULTOR, ASSINANTE_SQ, REVISOR
		// ), ACESSO_NEGADO_SQ);
		tem_acesso(array(USUARIO_LOGADO), ACESSO_NEGADO_SQ);

		zerar_respostas_marcadas();

		$data['include_chosen'] = true;
		$data['include_validade'] = true;
		$data['include_c3'] = true;
	
		if($this->input->get('limpar')){
			$filtro = array(
				'filtro_nome' => null,
				'filtro_legenda' => null,
				'filtro_ordenacao' => null
			);
			$_SESSION['categoria_filtro'] = null;
			redirect ( get_meus_cadernos_url() );
		}
		else if($this->input->get('filtrar')){
			$filtro = array(
				'filtro_nome' => $this->input->get('texto'),
				'filtro_legenda' => $this->input->get('legenda'),
				'filtro_ordenacao' => $this->input->get('ordem')
			);
		}
		
		$data['categorias'] = $this->caderno_model->listar_categorias(get_current_user_id());
		$data['categoria_id'] = $_SESSION['categoria_filtro'] ?: $categoria_id;
		$data['categorias_combo'] = get_categoria_multiselect($this->caderno_model->listar_categorias(get_current_user_id()));
		$data['ordenacao_combo'] = get_meus_cadernos_ordenacao_combo();
		$total = $this->caderno_model->contar_cadernos(get_current_user_id(), $categoria_id, $filtro);
		$data['cadernos'] = $this->caderno_model->listar_cadernos(get_current_user_id(), $categoria_id, $offset, CADERNOS_POR_PAGINA, $filtro);
		
		$incluir_inativas = AcessoGrupoHelper::cadernos_questoes_inativas();
		
		foreach ($data['cadernos'] as &$caderno) {
		    $caderno['questoes_certas'] = $caderno['cad_qtd_acertos'];
		    $caderno['questoes_erradas'] = $caderno['cad_qtd_erros'];
		    $caderno['total_questoes'] = $caderno['cad_qtd_questoes_usuario'];
			$caderno['total_questoes_respondidas'] = $caderno['questoes_certas'] + $caderno['questoes_erradas'];
			$caderno['total_questoes_nao_respondidas'] = $caderno['total_questoes'] - $caderno['total_questoes_respondidas'];
		}
		
		$query_string = "";

		if($categoria_id){
			$query_string = "/{$categoria_id}/";
		}else{
			$query_string = "/0/";
		}

		$url = get_meus_cadernos_url() . $query_string;

		$config = pagination_config($url, $total, CADERNOS_POR_PAGINA, 4);
		$config['reuse_query_string'] = TRUE;
		$this->pagination->initialize($config);
		$links = $this->pagination->create_links();
		$data['links'] = $links;
		$data['search_filter'] = $filtro['filtro_nome'];
		$data['filtro_legenda'] = $filtro['filtro_legenda'];
		$data['filtro_ordenacao'] = $filtro['filtro_ordenacao'];

		// compartilha e-mail de compartilhamento
		self::enviar_email();
		//print_r($data['categorias']); 
		
		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view('principal/cadernos/meus_cadernos', $data);
		$this->load->view(get_main_footer_view_url(), $data);
	}

	private function enviar_email()
	{
		if($this->input->post('enviar_email')) {
			$emails = explode(',', $this->input->post('emails'));
			$titulo = $this->input->post('titulo');
			$mensagem = $this->input->post('mensagem');
			$mensagem_padrao = $this->input->post('mensagem_padrao');

			if(strpos($mensagem, $mensagem_padrao) === FALSE) {
				$mensagem .= "<br><br>" . $mensagem_padrao;
			}

			foreach ($emails as $email) {
				//enviar_email($email, $titulo, $mensagem, null, null);
			}

			set_mensagem(SUCESSO, "E-mails enviados com sucesso.");
		}
	}
	
	public function excluir_questao_de_caderno($caderno_id, $questao_id, $pagina = null)
	{
		if(!is_dono_do_caderno($caderno_id)) exit;

		$this->caderno_model->excluir_questao_de_caderno($caderno_id, $questao_id);
		
		set_mensagem(SUCESSO, "Questão removida do caderno com sucesso.");
		redirect(get_resolver_caderno_url($caderno_id, $pagina));
	}
	
	function get_ajax_cadernos_alunos_acompanhamento_url($pagina = PAGE_CADERNOS){
		
		$filtro_aluno = $this->input->post('filtro_aluno');

		if($pagina == PAGE_CADERNOS){
			$alunos = get_ajax_main_alunos_acompanhamento($filtro_aluno, FALSE);
			echo form_multiselect("usu_ids[]", $alunos, '', "id='usu_ids' class='multiselect'");
		}
	
	}

	public function designar_caderno($input)
	{	
		$cadernos_ids = explode("-", $input);
		
		if($cadernos_ids) {
			foreach ($cadernos_ids as $caderno_id) {
					
				$caderno = $this->caderno_model->get_caderno($caderno_id);
				unset($caderno['cad_id']);
				unset($caderno['cad_ultimo_acesso']);
				unset($caderno['cad_tempo_gasto']);
				$caderno['cad_data_criacao'] = date("Y-m-d H:i:s");
				$caderno['usu_designador_id'] = get_current_user_id();
			
				$questoes = $this->questao_model->listar_por_caderno($caderno_id);
				
				switch (true){
					case ( is_array($this->input->post('usu_ids')) && is_array($this->input->post('prof_ids')) ):	
						$usuarios_id = array_merge($this->input->post('usu_ids'), $this->input->post('prof_ids') );
						break;
					case ( !is_array($this->input->post('usu_ids')) && is_array($this->input->post('prof_ids')) ):
						$usuarios_id = $this->input->post('prof_ids');
						break;
					case ( is_array($this->input->post('usu_ids')) && !is_array($this->input->post('prof_ids')) ):
						$usuarios_id = $this->input->post('usu_ids');
						break;
					case ( !is_array($this->input->post('usu_ids')) && !is_array($this->input->post('prof_ids')) ):

						set_mensagem(ERRO,'Nenhuma designação foi feita. Necessário selecionar alunos e/ou professores');
						redirect("questoes/cadernos/meus_cadernos/{$_SESSION['categoria_filtro']}");
						return false;
						break;
				}	

				$designador = get_usuario_array($caderno['usu_designador_id']);
				
				foreach ($usuarios_id as $usu_id){
					$usuario = get_usuario_array($usu_id);

					$caderno['usu_id'] = $usu_id;
					$cad_id = $this->caderno_model->adicionar_caderno($caderno);
						
					$this->caderno_model->adicionar_questoes_em_caderno($cad_id, $questoes);
						
					$existe_cat_coach = $this->caderno_model->existe_categoria_em_usuario($usu_id, 'Coaching Exponencial');
					if(! $existe_cat_coach ){
						$categoria = array( 'cat_nome' => 'Coaching Exponencial',
								'usu_id'	=> $usu_id
						);
						$cat_id[0] = $this->caderno_model->adicionar_categoria($categoria);
					}
					else{
						$cat_id[0] = $existe_cat_coach;
					}
					
					// salva no caderno original os usuários que estão sendo designados
					$this->caderno_model->salvar_usuario_designado($caderno_id, $usu_id);
						
					$this->caderno_model->adicionar_categorias_em_caderno($cad_id, $cat_id);
					// set_mensagem(SUCESSO, "Caderno designado com sucesso.");

					$titulo = "Coaching Exponencial Concursos - {$caderno['cad_nome']}";
					$mensagem = get_template_email ( 'aviso-caderno-designado.php', array (
							'designador' => $designador['nome_completo'],
							'url' => get_resolver_caderno_url($caderno['sim_id']),
							'nome' => $usuario['nome_completo'],
							'caderno' => $caderno['cad_nome'],
					) );
					//enviar_email ( $usuario['email'], $titulo, $mensagem, null, null, null, true);
					//enviar_email ( $designador['email'], $titulo, $mensagem, null, null, null, true);
				}

			}

			set_mensagem(SUCESSO,'Caderno(s) designado(s) com sucesso');
		}
		else {
			set_mensagem(ERRO,'Necessário selecionar pelo menos um caderno');	
		}
		
		redirect("questoes/cadernos/meus_cadernos/{$_SESSION['categoria_filtro']}");
	}
	
	public function modal_designar_caderno($caderno_id, $edicao_massa = FALSE) {
		$data['caderno_id'] = $caderno_id;
		$data['edicao_massa'] = $edicao_massa;
		$data['usuario_logado'] = get_usuario_array(get_current_user_id());
		$data['alunos'] = get_alunos_acompanhamento(NULL, FALSE);
		$data['professores'] = get_professores_multiselect(get_professores(FALSE));
		
		echo $this->load->view('modal/designar_caderno', $data, FALSE);
	}

	public function modal_filtros_caderno($caderno_id) {
		$caderno = $this->caderno_model->get_caderno($caderno_id);
		
		$data['caderno_id'] = $caderno_id;

		$data['filtros'] = formatar_filtro_caderno($caderno['cad_filtros']);
		echo $this->load->view('xhr/filtros_caderno', $data, FALSE);
	}

	public function modal_compartilhar_caderno($caderno_id) 
	{
		
		$this->load->view('modal/modal_compartilhar_caderno');
	}
	
	public function publico($id)
	{
	    
		/*if(!tem_acesso([USUARIO_LOGADO]))
		{
			header("Location: " . home_url() . "/cadastro-login/?ref={$_SERVER['REQUEST_URI']}");
			exit;
		}*/
		
		$caderno_id = decode_id_compartilhado($id);

		$caderno = $this->caderno_model->get_caderno_compartilhado($caderno_id, get_current_user_id());
		
		if($caderno) {
			$novo_caderno_id = $caderno['cad_id'];
		}
		else {
			$caderno_existe = $this->caderno_model->get_caderno($caderno_id);
			if($caderno_existe && !$caderno_existe['cad_coaching'])
			{
				$novo_caderno_id = $this->caderno_model->adicionar_caderno_compartilhado($caderno_id, get_current_user_id());
			}
			else
			{
				set_mensagem(AVISO,"Desculpe, o caderno foi excluído pelo usuário que o compartilhou. Caso seja relacionado a um produto do Exponencial, entre em contato conosco para ajudarmos a solucionar o problema'. <a href='/fale-conosco' class='alert-link'>Fale conosco</a>");
				redirect(get_meus_cadernos_url());
			}	
		}

		//if(tem_acesso(array(/*ADMINISTRADOR, PROFESSOR, CONSULTOR, ASSINANTE_SQ, REVISOR*/ USUARIO_LOGADO ))) {
			redirect(get_resolver_caderno_url($novo_caderno_id, NULL, TRUE));	
		//}
		//else {
		//	redirect(get_meus_cadernos_url());
		//}
			
	}

	public function coaching($id)
	{
	    
	    if(!tem_acesso([USUARIO_LOGADO]))
	    {
    	    header("Location: " . home_url() . "/cadastro-login/?ref={$_SERVER['REQUEST_URI']}");
    	    exit;
	    }

		if(!AcessoGrupoHelper::caderno_coaching_acessar())
		{
			set_mensagem(AVISO,"Desculpe, você não tem acesso a esse tipo de conteúdo. Caso seja relacionado a um produto do Exponencial, entre em contato conosco para ajudarmos a solucionar o problema'. <a href='/fale-conosco' class='alert-link'>Fale conosco</a>");
			redirect(get_meus_cadernos_url());
		}
		else
		{
			$caderno_id = decode_id_compartilhado($id);

			$caderno = $this->caderno_model->get_caderno_coaching($caderno_id, get_current_user_id());

			if($caderno)
			{
				$novo_caderno_id = $caderno['cad_id'];
			}
			else
			{
				$caderno_existe = $this->caderno_model->get_caderno($caderno_id);
				if($caderno_existe && $caderno_existe['cad_coaching'])
				{
					$novo_caderno_id = $this->caderno_model->adicionar_caderno_compartilhado($caderno_id, get_current_user_id(), TRUE);
				}
				else
				{
					set_mensagem(AVISO,"Desculpe, o caderno foi excluído pelo usuário que o compartilhou. Caso seja relacionado a um produto do Exponencial, entre em contato conosco para ajudarmos a solucionar o problema'. <a href='/fale-conosco' class='alert-link'>Fale conosco</a>");
					redirect(get_meus_cadernos_url());
				}
			}

			redirect(get_resolver_caderno_url($novo_caderno_id, NULL, TRUE));
		}
	}

	public function gerar_novo($caderno_id)
	{
		$caderno = $this->caderno_model->get_caderno($caderno_id);

		//$_SESSION['filtro'] = $caderno['cad_filtros'];

		$filtro = unserialize($caderno['cad_filtros']);

		if($filtro && !empty($filtro))
		{
			$filtro['buscar'] = 'Buscar!';//Força a busca no resolver questões
		}

		$query_string = get_query_string($filtro);

		redirect(get_resolver_questoes_url() . $query_string);
	}

	public function modal_info_caderno($caderno_id) {
		$caderno = $this->caderno_model->get_caderno($caderno_id);
		$caderno['questoes_certas'] = $this->caderno_model->get_total_resolvidas($caderno['cad_id'], get_current_user_id(), CERTO);
		$caderno['questoes_erradas'] = $this->caderno_model->get_total_resolvidas($caderno['cad_id'], get_current_user_id(), ERRADO);
		$caderno['total_questoes_anuladas'] = $this->caderno_model->get_total_questoes_anuladas($caderno['cad_id']);
		$caderno['total_questoes'] = $this->caderno_model->get_total_questoes($caderno['cad_id']);
		$caderno['total_questoes_respondidas'] = $caderno['questoes_certas'] + $caderno['questoes_erradas'];
		$caderno['total_questoes_nao_respondidas'] = $caderno['total_questoes'] - $caderno['total_questoes_respondidas'];
		$caderno['tempo_gasto'] = tempo_gasto($caderno['cad_tempo_gasto']);
		$caderno['tempo_medio_questao'] = $caderno['total_questoes'] == 0 ? "00:00:00" : tempo_gasto( round($caderno['cad_tempo_gasto'] / $caderno['total_questoes']) );
		$caderno['tempo_medio_questao_resolvida'] = $caderno['total_questoes_respondidas'] == 0 ? "00:00:00" :tempo_gasto( round($caderno['cad_tempo_gasto'] / $caderno['total_questoes_respondidas']) );

		$data['caderno'] = $caderno;

		echo $this->load->view('modal/info_caderno', $data, TRUE);
	}
	
	public function modal_info_caderno_exponencial($caderno_id) {
	    $produtos = $this->caderno_model->listar_produtos_de_caderno($caderno_id);
	   
	    $data = [];
	    if(count($produtos) == 0) {
	        $data['html'] = "";
	    }
	    else {
	        $wc_product = wc_get_product($produtos[0]['produto_id']);
	        $data['html'] = $wc_product->post->post_content;
	    }
	    
	    $data["html"] = $data["html"] ?: "Nenhuma informação a ser exibida.";
	    
	    echo $this->load->view('modal/info_caderno_exponencial', $data, TRUE);
	}

	public function modal_indice($caderno_id, $possui_filtro = FALSE) 
	{
	    $incluir_inativas = AcessoGrupoHelper::cadernos_questoes_inativas();
	    
	    $assuntos = $this->caderno_model->buscar_assuntos($caderno_id, $incluir_inativas);
	    $disciplinas = $this->caderno_model->contar_questoes_por_disciplinas($caderno_id, $incluir_inativas);
	    $total = $this->caderno_model->contar_questoes($caderno_id, $incluir_inativas);
	    
	    $indice = [];
	    foreach($assuntos as $assunto) {
			//completar_indice($indice, $assunto);
			completar_indice($indice, $assunto, $caderno_id, $incluir_inativas);
	    }
	    
	    foreach ($disciplinas as &$disciplina) {
	        $disciplina["assuntos"] = get_arvore_indice($indice, null, $disciplina["dis_id"]);
	    }
	    
	    $data = [];
	    $data["disciplinas"] = $disciplinas;
		$data["total"] = $total;
		$data['possui_filtro'] = $possui_filtro;
		
		if(isset($_GET['ass_ids'])){
			//$string_filtro = $_SESSION['filtro'];
			//$filtro = unserialize($string_filtro);
			$data['assuntos_selecionados'] = $_GET['ass_ids'];
		}

	    $this->load->view("modal/indice_caderno_tabela", $data);
	}

	public function excluir_selecionados()
	{
		$ids = $this->input->post('ids');

		if($ids) {

			$is_ok = TRUE;

			foreach ($ids as $id){
				if($this->caderno_model->is_caderno_associado_a_produto($id)){
					$caderno = $this->caderno_model->get_caderno($id);
					set_mensagem(ERRO, 'Não foi possível excluir o caderno ' . $caderno['cad_nome'] . ', pois o mesmo está associado a um produto!');
					$is_ok = FALSE;
					break;
				}
			}

			if($is_ok){

				foreach ($ids as $id){
					$caderno = array('cad_id' => $id);

					if(pode_excluir_caderno($caderno)) {
						$this->caderno_model->excluir_caderno($id);
					}

				}

				set_mensagem(SUCESSO,'Cadernos excluídos com sucesso');
			}
		}

		else {
			set_mensagem(ERRO,'É necessário selecionar pelo menos um caderno');
		}

		redirect("questoes/cadernos/meus_cadernos");
	}

	public function exponencial($offset = null){
		
		//$this->output->enable_profiler(TRUE);

		if(is_area_desativada(array(PAINEL_SQ_CADERNOS))){
			redirecionar_erro_500();
		}
		
		//tem_acesso(array(USUARIO_LOGADO), ACESSO_NEGADO_SQ);

		if(is_null($offset)){
			$offset = 0;
			$_SESSION['cadernos_filtro'] = array();
		}

		require_once(APPPATH.'libraries/HTMLPurifier.php');
		$config = HTMLPurifier_Config::createDefault();
		$data['purifier'] = new HTMLPurifier($config);

		if($this->input->post('limpar')){
			$_SESSION['cadernos_filtro'] = array(
				'filtro_nome' => null,
				'filtro_busca' => TODAS,
				'filtro_ordem' => null
			);
			redirect ( get_cadernos_exponencial_url() );
		}else if($this->input->post('filtrar')){
			$offset = 0;
			$_SESSION['cadernos_filtro'] = array(
				'filtro_nome' => $this->input->post('search_filter'),
				'filtro_busca' => $this->input->post('radio_caderno'),
				'filtro_ordem' => $this->input->post('ordenacao')
			);
		}

		$filtro = $_SESSION['cadernos_filtro'];

		$usuario_id = get_current_user_id();

		if($filtro['filtro_busca'] != QUE_ADQUIRI && $filtro['filtro_busca'] != QUE_NAO_ADQUIRI && $filtro['filtro_busca'] != GRATUITOS ) {
            $cacheKey = implode(":", ["expo", "sq", "caderno-expo", serialize($filtro), $offset]);
            $cacheValue = get_memcached($cacheKey);

            if(!$cacheValue) {
                $cacheValue = $this->caderno_model->listar($usuario_id, $offset, CADERNOS_POR_PAGINA_WIDGET, $filtro);
                set_memcached($cacheKey, $cacheValue, time() + (3600 * 24) );
            }

            $cadernos = $cacheValue;
        }
		else {
            $cadernos = $this->caderno_model->listar($usuario_id, $offset, CADERNOS_POR_PAGINA_WIDGET, $filtro);
        }
		
		$total = $this->caderno_model->contar($usuario_id, $filtro);

		foreach ($cadernos as &$caderno) {
			//$caderno['comprou_produto'] = $usuario_id ? $this->caderno_model->is_comprou_caderno($caderno['cad_ref_id'], $usuario_id) : false;
            /* @todo Desativado, pois é uma rotina cara e aparenta estar com bug. Todas as consultas desse looping são as mesmas. */
            $caderno['comprou_produto'] = false;
		}

		$pag_config = pagination_config(get_cadernos_exponencial_url(), $total, CADERNOS_POR_PAGINA_WIDGET);
		$this->pagination->initialize($pag_config);
		$links = $this->pagination->create_links();
		
		$data['links'] = $links;
		$data['total'] = $total;

		$data['cadernos'] = $cadernos;
		$data['search_filter'] = $filtro['filtro_nome'];
		$data['radio_caderno'] = $filtro['filtro_busca'];
		$data['ordenacao_selecionada'] = $filtro['filtro_ordem'];

		if(is_null($data['radio_caderno'])){
			$data['radio_caderno'] = TODAS;
		}

		$data['ordenacoes'] = get_ordenacao_multiselect();

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view(get_cadernos_view_url(), $data);
		$this->load->view(get_main_footer_view_url(), $data);

	}
	
	/**
	 * Ação para duplicação de cadernos na listagem presente em SQ-MECAD
	 *
	 * @since L1
	 *
	 * @param int $caderno_id Id do caderno
	 */ 
	
	public function duplicar($caderno_id) 
	{
	    /* === restrições de acesso === */  
	    if(!AcessoGrupoHelper::cadernos_duplicar()) {
	        wp_redirect(ACESSO_NEGADO);
	        exit;
	    }
	    
	    try
	    {
    	    /* === tratamento dos parâmetros === */
	        if(!(filter_var($caderno_id, FILTER_VALIDATE_INT))) {
	            throw new InvalidArgumentException("Argumento 'caderno_id' precisa ser inteiro");
	        }
    	    
    	    /* === fluxo natural === */
            $this->caderno_model->duplicar_caderno($caderno_id, get_current_user_id(), false, " (cópia)");
            
            set_mensagem(SUCESSO,"Caderno duplicado com sucesso.");
	    }
	    catch(Exception $e) 
	    {
	        set_mensagem(ERRO,"Desculpe, ocorreu um erro ao duplicar o caderno. Entre em contato conosco para ajudarmos a solucionar o problema'. <a href='/fale-conosco' class='alert-link'>Fale conosco</a>");  
	        log_erro("Erro ao duplicar o produto", $e, ["caderno_id" => $caderno_id]);
	    }
	    finally {
	        /* === redirecionamento === */
	        redirect(get_meus_cadernos_url());
	    }
   
	}
	
	/**
	 * Ação para zerar estatísticas de cadernos na listagem presente em SQ-MECAD
	 *
	 * @since L1
	 */
	
	public function zerar_estatisticas()
	{
	    try
	    {
	        /* === tratamento dos parâmetros === */
    	    $caderno_id = $this->input->post("zer_est_item_id");
    	    
    	    if(!(filter_var($caderno_id, FILTER_VALIDATE_INT))) {
    	        throw new InvalidArgumentException("Argumento 'caderno_id' precisa ser inteiro");
    	    }
    	    
    	    /* === fluxo natural === */
    	    $this->caderno_model->zerar_estatisticas($caderno_id, get_current_user_id());
    	    
    	    set_mensagem(SUCESSO,"As estatatísticas do caderno foram zeradas com sucesso.");
	    }
	    catch(Exception $e)
	    {
	        set_mensagem(ERRO,"Desculpe, ocorreu um erro ao zerar as estatísticas do caderno. Entre em contato conosco para ajudarmos a solucionar o problema'. <a href='/fale-conosco' class='alert-link'>Fale conosco</a>");
	        log_erro("Erro ao zerar estatísticas do produto", $e, ["caderno_id" => $caderno_id]);
	    }
	    finally {
    	    /* === redirecionamento === */
    	    redirect(get_meus_cadernos_url());
	    }
	}

	/**
	 * Atualiza o caderno
	 * 
	 * @since L1
	 * 
	 * @param int $caderno_id ID do caderno a ser atualizado
	 */
	public function atualizar($caderno_id){

		$this->load->helper('simulado');

		$caderno = $this->caderno_model->get_caderno($caderno_id);

		if(!pode_editar_caderno($caderno))
		{
		    redirecionar_para_acesso_negado();
		}
		
		if(!$caderno_id || !$caderno){
			redirect(get_meus_cadernos_url());
		}

		$data['caderno'] = $caderno;

		init_combos($data, TRUE, TRUE);

		$questoes = $this->caderno_model->listar_questoes_ids($caderno_id);
		
		if($this->input->post("avancar")){	

			$filtro_novo = array();

			if($this->input->post('status_ids'))
			{
				$filtro_novo['status_ids'] = $this->input->post('status_ids');
			}
			if ($this->input->post('filtro_favoritas')) {
				$filtro_novo['favoritas'] = $this->input->post('filtro_favoritas');
			}
			if ($this->input->post('data_inicio')) {
				$filtro_novo['data_inicio'] = $this->input->post('data_inicio');
			}
			if ($this->input->post('data_fim')) {
				$filtro_novo['data_fim'] = $this->input->post('data_fim');
			}
			
			adicionar_filtro($filtro_novo, $data, $this);
			
			$data['apenas_assunto_pai'] = $this->input->post("apenas_assunto_pai");
			if(!$data['apenas_assunto_pai'])
			{
				adicionar_assuntos_filhos($filtro_novo);
			}

			$data['qtd_questoes'] = $this->input->post("qtd_questoes");			

			$aux = $this->questao_model->get_questoes_ids_por_filtro($filtro_novo, null);
			$questoes_encontradas = array();
			foreach($aux as $questao){
				array_push($questoes_encontradas, $questao['que_id']);
			}

			$questoes_fora_do_filtro = array_diff($questoes, $questoes_encontradas);

			$data['qtd_questoes_atual'] = count($questoes);
			$data['qtd_questoes_filtro'] = count($questoes_encontradas);
			$data['qtd_questoes_fora_filtro'] = count($questoes_fora_do_filtro);

			$data['questoes_fora_do_filtro'] = $questoes_fora_do_filtro;
			$data['is_info_caderno'] = TRUE;

			$data['filtro_novo'] = base64_encode(serialize($filtro_novo));

			$data['questoes'] = $this->questao_model->get_questoes_completas_para_listagem($questoes);

		}elseif($this->input->post("voltar")){

			$filtro_novo = unserialize(base64_decode($this->input->post("filtro_novo")));

			$posts = get_array_posts();
			$filtro = array();
			foreach ($posts as $post => $selecionados) {
				if($filtro_novo[$post]){
					$filtro[$selecionados] = $filtro_novo[$post];
				}
			}

			$data = array_merge($data, $filtro);

			$data['qtd_questoes'] = $this->input->post("qtd_questoes");

		}elseif($this->input->post("atualizar")){
			
			$filtro_novo = unserialize(base64_decode($this->input->post("filtro_novo")));

			$aux = $this->questao_model->get_questoes_ids_por_filtro($filtro_novo, null);
			$questoes_encontradas = array();
			foreach($aux as $questao){
				array_push($questoes_encontradas, $questao['que_id']);
			}
			
			$questoes_fora_do_filtro = array_diff($questoes, $questoes_encontradas);
			
			//Atualiza o número de questões
			$qtd_questoes = $this->input->post("qtd_questoes");
			$this->caderno_model->atualizar_quantidade_questoes($caderno_id, $qtd_questoes);

			//Atualiza o filtro associado
			$this->caderno_model->atualizar_filtros($caderno_id, serialize($filtro_novo), FALSE);

			//Remove questões (se necessário)
			if(!$this->input->post("check_manter_atuais")){
				
				$this->caderno_model->excluir_questoes_de_caderno($caderno_id);

			}else{

				//Verifica se é necessário excluir questões específicas
				if($this->input->post("check_fora_filtro")){
					$ficaram = count($questoes) - count($questoes_fora_do_filtro);
				}else{
					$ficaram = count($questoes);
				}
	
				if($ficaram >= 0){
						
					if($ficaram > $qtd_questoes){
						$qtd_remover = $ficaram - $qtd_questoes;
					}
	
				}

				//Remove as questões que não atendem o novo filtro
				if($this->input->post("check_fora_filtro")){

					$questoes_fora_do_filtro = array_diff($questoes, $questoes_encontradas);

					$this->caderno_model->excluir_questoes_de_caderno($caderno_id, $questoes_fora_do_filtro);

				}

				//Tem que remover algumas questões do caderno atual
				if($qtd_remover){

					if($this->input->post("radio_exclusao") == CADERNOS_QUESTOES_ALEATORIAS){
						$questoes_excluir = $this->caderno_model->listar_questoes_ids($caderno_id, TRUE, $qtd_remover);
					}elseif($this->input->post("radio_exclusao") == CADERNOS_QUESTOES_ESCOLHIDAS){
						$questoes_excluir = $this->input->post("que_ids");
						$ainda_faltam = $qtd_remover - count($questoes_excluir);
					}

					$this->caderno_model->excluir_questoes_de_caderno($caderno_id, $questoes_excluir);

					if($ainda_faltam > 0){//Se não informaram a quantidade mínima remove o restante aleatoriamente
						$questoes_excluir = $this->caderno_model->listar_questoes_ids($caderno_id, TRUE, $ainda_faltam);
						$this->caderno_model->excluir_questoes_de_caderno($caderno_id, $questoes_excluir);
					}

				}
			}

			//Associa novas questões aleatoriamente ao caderno seguindo os filtros escolhidos
			$total_questoes = $this->caderno_model->contar_questoes($caderno_id, true);
			$qtd_questoes_novas = $qtd_questoes - $total_questoes;
			
			if($qtd_questoes_novas > 0){

				//Pega "aleatoriamente" as mais novas...
				rsort($questoes_encontradas);
				$questoes_encontradas = array_slice($questoes_encontradas, 0, $qtd_questoes_novas);
				
				//embaralha a escolha "aleatória"
				$keys = array_keys($questoes_encontradas);
				shuffle($keys);
				$random = array();
				foreach ($keys as $key){
					$random[$key] = $questoes_encontradas[$key]; 
				}
				
				$this->caderno_model->adicionar_questoes_ids_em_caderno($caderno_id, $random);
			}

			set_mensagem(SUCESSO, "Caderno atualizado com sucesso.");
			redirect(get_meus_cadernos_url());
		

		}else{

			$filtros_a = unserialize($caderno['cad_filtros']);

			$posts = get_array_posts();
			$filtro = array();
			foreach ($posts as $post => $selecionados) {
				if($filtros_a[$post]){
					$filtro[$selecionados] = $filtros_a[$post];
				}
			}

			if($caderno['cad_qtd_questoes']){
				$data['qtd_questoes'] = $caderno['cad_qtd_questoes'];
			}else{
				$data['qtd_questoes'] = count($questoes);
			}

			if($filtro['disciplinas_selecionadas']) {
				$data['assuntos'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplinas($filtro['disciplinas_selecionadas']));
			}

			$data = array_merge($data, $filtro);
			
		}


		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view('cadernos/atualizar/atualizar_caderno', $data);
		$this->load->view(get_main_footer_view_url(), $data);

	}

	/**
	 * Redireciona para a versão do usuário logado do caderno demo. Caso o caderno ainda não exista ele é criado.
	 * 
	 * @since L1
	 * 
	 * @param int $caderno_id Id do caderno demo
	 */
	public function caderno_demo($caderno_id, $produto_id)
	{
		KLoader::helper("ProdutoHelper");

		redirecionar_se_nao_estiver_logado('/questoes/main/caderno_demo/' . $caderno_id . '/' . $produto_id);

		// se o produto está expirado, redireciona
		if(is_produto_expirado($produto_id)){
			wp_redirect(get_site_url() . '/cursos-por-concurso/?expirado=1', 301);
			exit;
		}

		$caderno_to_redirect = null;
		
		$caderno_ref = $this->caderno_model->get_caderno($caderno_id);

		if($caderno_ref && ProdutoHelper::is_caderno_aula_demo_produto($caderno_id, $produto_id)){
			
			$caderno_demo = $this->caderno_model->get_caderno_demo($caderno_id, get_current_user_id());
			print_r("PRimeiro cadDemo: ");
			print_r($caderno_demo);
			//Se não tem caderno demo então tenta criar
			if(!$caderno_demo){
				print_r("Vai criar com base no cadRef: ");
				print_r($caderno_ref);
				$this->caderno_model->criar_caderno_demo($caderno_ref);
				$caderno_demo = $this->caderno_model->get_caderno_demo($caderno_id, get_current_user_id());
			}

			$caderno_to_redirect = $caderno_demo;
		}

		if($caderno_to_redirect){
			redirect(get_resolver_caderno_url($caderno_to_redirect['cad_id'], NULL, TRUE));
		}else{
			redirecionar_para_acesso_negado();
		}

	}

	/**
	 * Incrementa o tempo gasto em um caderno e troca de página
	 * 
	 * @since L1
	 * 
	 * @param int $caderno_id ID do caderno
	 * @param int $segundos tempo gasto em segundos que será incrementado
	 * 
	 */
	public function trocar_pagina($caderno_id, $segundos)
	{
		if($caderno_id && $segundos)
		{
			$this->caderno_model->incrementar_tempo_gasto($caderno_id, $segundos);
		}
		
		redirect( $_GET['redirect'] );
	}

}