<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Sitemaps extends CI_Controller {
	
	public function __construct() 
	{
		parent::__construct ();

		$this->load->model ( 'questao_model' );
		$this->load->helper ( 'questao' );
		// $this->output->enable_profiler(TRUE);
	
}	
	public function questoes($offset = 0)
	{
		ini_set('memory_limit', '-1');

		$questoes = $this->questao_model->listar_todas(true, 5000, $offset);

		$links = array();

		foreach ($questoes as $questao) {
			set_time_limit(30);

			array_push($links, array(
				'loc' => get_questao_seo_url($questao),
				'changefreq' => 'monthly',
				'priority' => '0.5'
			));
		}

		ob_clean(); 
		$this->load->view('sitemaps/sitemap', array('links' => $links));
	}
}