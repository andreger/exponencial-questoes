<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

class Migrate extends CI_Controller
{
    public function index()
    {
        $this->load->library('migration');

        $version = $this->input->get("version");

        if($version)
        {
            echo "Usando Versão: {$version}<br>";
        }
        else
        {
            echo "Usando Versão do 'migration_version'<br>";
        }

        if ( $version )
        {
            $result = $this->migration->version($version);
        }
        else
        {
            $result = $this->migration->current();
        }

        if($result === FALSE )
        {
            show_error($this->migration->error_string());
        }
        else
        {
            echo "Migration successful to {$result}!";    
        }
    }
}