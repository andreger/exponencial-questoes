<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

/**
 * Controlar as requisições ajax relacionadas a Questões
 * 
 * @since L1
 * 
 * @author João Paulo Ferreira <jpaulofms@gmail.com>
 */
class Questoes_xhr extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct ();
    }

	/**
	 * Carrega o modal de anotações de uma questão
	 */
	public function anotacoes_questao($questao_id)
	{
		if($questao_id)
		{
			$this->load->model('questao_model');
			$data['anotacoes'] = $this->questao_model->listar_questoes_anotacoes($questao_id, get_current_user_id());
			$data['cockpit_questao_id'] = $questao_id;

			echo $this->load->view('questoes/barra_tarefas/anotacoes/modal_dialog', $data, true);
		}
	}

	public function buscar_ids_questoes()
	{
		KLoader::helper("AcessoGrupoHelper");
		AcessoGrupoHelper::copiar_ids_questoes(ACESSO_NEGADO);

		$this->load->model('questao_model');
		$this->load->helper('questao');
		$this->load->helper('filtro');

		$filtro = array();
		$data = array();
		
		adicionar_todos_filtros($filtro, $data, $this, FALSE);

		$filtro['cad_id'] = $this->input->get("cad_id");

		$result = $this->questao_model->get_questoes_ids_por_filtro($filtro, null);
		
		$questoes_ids = array();
		foreach($result as $questao)
		{
			array_push($questoes_ids, $questao['que_id']);
		}
		echo implode(",", $questoes_ids);
	}

    /**
     * Carrega a view de edição rápida de uma questão
     * 
     * @since L1
     * 
     * @param int Squestao_id ID da questão a ser editada
     * 
     */	
	public function carregar_edicao_rapida($questao_id) {

		$data['include_jquery_chained'] = true;
		
		$this->load->model('disciplina_model');
		$this->load->model('simulado_model');
		$this->load->model('questao_model');
		$this->load->model('assunto_model');
		$this->load->model('comentario_model');

		KLoader::helper("AcessoGrupoHelper");
		
		$disciplinas = $this->disciplina_model->listar_todas();
		
		$data['is_associada_simulado_ativo'] = $this->simulado_model->is_questao_associada_simulado_ativo($questao_id);
		$data['questao'] = $this->questao_model->get_by_id($questao_id);
		$data['disciplinas'] = get_disciplina_combo($disciplinas);

		$data['assuntos'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplina($data['questao']['dis_id']), true);
		$data['assuntos_selecionados'] = get_assuntos_ids($this->questao_model->get_assuntos_questao ( $questao_id ) );
		
		$data['dificuldades'] = get_dificuldade_questao_combo();
		$data['statuses'] = get_status_questao_combo();
		$data['situacoes'] = get_situacoes_combo();

		$data['professores'] = get_professores_combo(get_professores(FALSE));

		$data['comentario_video'] = $this->comentario_model->get_comentario_video_por_questao($questao_id);
		
		echo $this->load->view('questoes/barra_superior/edicao_rapida', $data, true);
	}

	/**
	 * Valida e salva a edição rápida de uma questão, caso aconteça algum erro de validação retorna o erro
	 * 
	 * @since L1
	 * 
	 */
	public function edicao_rapida_questao() {

		$this->load->model('assunto_model');
		$this->load->model('questao_model');
		$this->load->model('disciplina_model');
		$this->load->model('comentario_model');
		
		$this->load->helper('questao');

		KLoader::helper("VimeoHelper");

		$erro = "";

		//Ou informa os dois ou nenhum
		if($this->input->post('cvi_user_id') || $this->input->post('cvi_url')){

			if(!$this->input->post('cvi_user_id')){
				$erro .= "O professor do Comentário em Vídeo é obrigatório.<br/>";
			}

			if(!$this->input->post('cvi_url')){
				$erro .= "A URL do Comentário em Vídeo é obrigatória.<br/>";
			}elseif(!VimeoHelper::is_vimeo_url($this->input->post('cvi_url'))){
				$erro .= "A URL do Vimeo informada não é válida.<br/>";
			}

		}
		
		if($erro){

			echo ui_get_alerta($erro, ALERTA_ERRO);

		}else{

			$this->questao_model->atualizacao_rapida(array(
				'que_id'	=> $this->input->post('que_id'),
				'dis_id'	=> $this->input->post('dis_id'),
				'ass_ids'	=> $this->input->post('ass_ids'),
				'que_dificuldade'	=> $this->input->post('que_dificuldade'),
				'que_resposta' => $this->input->post('que_resposta'),
				'que_status'	=> $this->input->post('que_status'),
				'que_ativo'	=> $this->input->post('que_ativo')
			));

			$comentario_video = $this->comentario_model->get_comentario_video_por_questao($this->input->post('que_id'));
			$comentario_video['usu_id'] = $this->input->post('cvi_user_id');
			$comentario_video['cvi_url'] = $this->input->post('cvi_url');
			$comentario_video['que_id'] = $this->input->post('que_id');

			//Só salva/atualiza se tiver url informada, a validação já deve ter sido feita
			if($comentario_video['cvi_url']){

				if($comentario_video['cvi_id']){
					$this->comentario_model->atualizar_comentario_video($comentario_video);
				}else{
					$this->comentario_model->salvar_comentario_video($comentario_video);
				}

			}elseif($comentario_video['cvi_id']){//Se não tem url tem que excluir caso já possua alguma informação no banco de dados
				$this->comentario_model->excluir_comentario_video($comentario_video);
			}

			$this->questao_model->atualizar_destaque_questao($this->input->post('que_id'));

			//Invalida o cache de questões após cada alteração em questões
			memcached_remover_cache_questoes();
			
			echo "OK";
		}
	}
	
	public function marcar_desmarcar_favorito($questao_id, $tipo = QUESTAO_FAVORITA) 
	{
		$this->load->model('questao_model');
		$this->questao_model->marcar_desmarcar_favorita($questao_id, get_current_user_id(), $tipo);
	}

	/**
	 * Remove uma anotação de uma questão para um usuário
	 * 
	 * @since L1
	 * 
	 * @return HTML contendo a listagem de anotações atualizadas
	 */
	public function remover_questao_anotacao(){

		$this->load->model('questao_model');

		if($qan_id = $this->input->post('id')){
			
			$anotacao = $this->questao_model->get_questao_anotacao($qan_id);

			if($anotacao 
				&& $anotacao['usu_id'] == get_current_user_id()
				&& $anotacao['que_id'] == $this->input->post('que_id')){
					$this->questao_model->remover_questao_anotacao($qan_id);
			}
		}
		
		$anotacoes = $this->questao_model->listar_questoes_anotacoes($anotacao['que_id'], get_current_user_id());
		$data['anotacoes'] = $anotacoes;

		echo $this->load->view('questoes/barra_tarefas/anotacoes/anotacoes', $data, true);
	}

	/**
	 * Salva ou edita uma anotação de uma questão para um usuário
	 * 
	 * @since L1
	 * 
	 * @return HTML contento a listagem de anotações atualizadas
	 */
	public function salvar_questao_anotacao(){

		$this->load->model('questao_model');

		if($qan_id = $this->input->post('id')){
			$anotacao = $this->questao_model->get_questao_anotacao($qan_id);
		}

		$texto_anotacao = htmlspecialchars($this->input->post('qan_anotacao'));
		if($anotacao && $anotacao['usu_id'] == get_current_user_id()){
			$this->questao_model->alterar_questao_anotacao($qan_id, $texto_anotacao);
		}else{
			$anotacao = array(
				'usu_id' => get_current_user_id(),
				'que_id' => $this->input->post('que_id'),
				'qan_anotacao' => $texto_anotacao,
			);
			$this->questao_model->salvar_questao_anotacao($anotacao);
		}

		$anotacoes = $this->questao_model->listar_questoes_anotacoes($anotacao['que_id'], get_current_user_id());
		$data['anotacoes'] = $anotacoes;

		echo $this->load->view('questoes/barra_tarefas/anotacoes/anotacoes', $data, true);

	}

}