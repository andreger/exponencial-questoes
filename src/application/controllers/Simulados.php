<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

KLoader::helper("AcessoGrupoHelper");

class Simulados extends CI_Controller {
	

	public function __construct() 
	{
		parent::__construct ();

		session_start();

		$this->load->model('banca_model');
		$this->load->model('orgao_model');
		$this->load->model('simulado_model');

		$this->load->helper('aluno');
		$this->load->helper('questao');
		$this->load->helper('simulado');
		
		init_profiler();
	}
	
	public function meus($offset = null)
	{
	    $conf_usuario = get_config_aluno();
	    
	    $data['check_acertos'] = $conf_usuario  ?  $conf_usuario['mostrar_acertos'] : 1;
	    $data['check_filtros'] = $conf_usuario  ?  $conf_usuario['exibir_filtros'] : 1;
	    $data['check_historico'] = $conf_usuario  ?  $conf_usuario['manter_historico_marcacao_cadernos'] : 1;
	    
	    if(is_area_desativada(array(PAINEL_SQ_SIMULADOS))){
	        redirecionar_erro_500();
	    }
	    
	    if($offset == null){
	        $offset = 0;
	        $_SESSION['meus_simulados_filtro'] = array();
	    }
	    
	    $this->session->set_userdata(ULTIMA_URL, $_SERVER['REQUEST_URI']);
	    
	    $data['include_icheck'] = true;
	    $data['include_chosen'] = true;
	    $data['include_main_meus_simulados'] = true;
	    $data['include_validade'] = true;
	    $data['include_daterangepicker'] = true;
	    $data['include_basictable'] = true;
	    $data['check_simulado'] = array();
	    $data['ordenacoes'] = get_ordenacao_multiselect();
	    
	    require_once(APPPATH.'libraries/HTMLPurifier.php');
	    $config = HTMLPurifier_Config::createDefault();
	    $data['purifier'] = new HTMLPurifier($config);
	    
	    $data['bancas'] = get_banca_multiselect($this->banca_model->listar_todas());
	    $data['orgaos'] = get_orgao_multiselect($this->orgao_model->listar_todas());
	    
	    $data['inicio'] = converter_para_ddmmyyyy(date('Y-m-d', strtotime('-1 year')));
	    $data['fim'] = converter_para_ddmmyyyy(date('Y-m-d'));
	    
	    if($this->input->post('limpar')){
	        $_SESSION['meus_simulados_filtro'] = array(
	            'filtro_nome' => null,
	            'filtro_bancas' => null,
	            'filtro_instituicoes' => null,
	            'filtro_busca' => TODAS,
				'filtro_ordem' => null,
				'filtro_produto' => null
	        );
	        redirect ( get_meus_simulados_url(). '/' );
	    }else if($this->input->post('filtrar')){
	        $_SESSION['meus_simulados_filtro'] = array(
	            'filtro_nome' => $this->input->post('search_filter'),
	            'filtro_bancas' => $this->input->post('ban_id'),
	            'filtro_instituicoes' => $this->input->post('org_id'),
	            'filtro_busca' => $this->input->post('check_simulado'),
	            'filtro_ordem' => $this->input->post('ordenacao')
	        );
	    }
	    
	    /* === Tratamento do filtro via GET === */
	    //if($_GET["sn"]) {
	    //    $_SESSION['meus_simulados_filtro']['filtro_nome'] = urldecode($_GET["sn"]);
		//}
		if($_GET["p"]) {
			$_SESSION['meus_simulados_filtro']['filtro_produto'] = $_GET["p"];
			$data['produto_id'] = $_GET["p"];
		}
	    
	    $filtro = $_SESSION['meus_simulados_filtro'];
	    
	    $simulados = $this->simulado_model->listar($offset, SIMULADOS_POR_PAGINA, TRUE, $filtro);
	    $total = $this->simulado_model->contar(TRUE, $filtro);
	    foreach ($simulados as &$simulado) {
	        $simulado['total_questoes'] = $this->simulado_model->get_total_questoes($simulado['sim_id']);
	    }
	    
	    $simulados_ranking = $this->simulado_model->listar_simulados_ranking(get_current_user_id());
	    foreach ($simulados_ranking as &$simulado) {
	        // $simulados_ranking['aproveitamento'] = $this->simulado_model->get_aproveitamento($simulado['sim_id'], get_current_user_id());
	        $simulado['nota'] = $this->simulado_model->get_nota(get_current_user_id(), $simulado['sim_id']);
	        $simulado['aprovado'] = $this->simulado_model->is_aprovado(get_current_user_id(), $simulado['sim_id']);
	    }
	    
	    $pag_config = pagination_config(get_meus_simulados_url(), $total, SIMULADOS_POR_PAGINA);
	    $this->pagination->initialize($pag_config);
	    $links = $this->pagination->create_links();
	    
	    $data['links'] = $links;
	    $data['total'] = $total;
	    
	    $data['simulados'] = $simulados;
	    $data['simulados_ranking'] = $simulados_ranking;
	    
	    $data['search_filter'] = $filtro['filtro_nome'];
	    $data['banca_selecionada'] = $filtro['filtro_bancas'];
	    $data['orgao_selecionado'] = $filtro['filtro_instituicoes'];
	    $data['check_simulado'] = $filtro['filtro_busca'];
	    $data['ordenacao_selecionada'] = $filtro['filtro_ordem'];
	    
	    if(is_null($data['check_simulado'])){
	        $data['check_simulado'] = TODAS;
	    }
	    
	    $this->load->view(get_main_header_view_url(), $data);
	    $this->load->view(get_meus_simulados_view_url(), $data);
	    $this->load->view(get_main_footer_view_url(), $data);
	}

	/**
	 * Altera o modo de visualizar a resolução de simulado e redireciona para a tela de resolver simulado
	 * 
	 * @since L1
	 * 
	 * @param $modo Tipo de visulização das questões: MODO_QUESTOES_XXX
	 * @param $simulado_id ID do simulado que está sendo resolvido
	 * 
	 */
	public function alterar_modo_resolver_questoes($modo, $simulado_id, $exibe_resultado = FALSE)
	{	
		if($_SESSION['mrq'] != $modo)
		{
			$_SESSION['mrq'] = $modo;

			if($_SESSION['mrq'] == MODO_QUESTOES_COCKPIT)
			{
				$_SESSION['qpp'] = 1;
			}
			else
			{
				$_SESSION['qpp'] = 10;
			}
		}
		
		$query_string = http_build_query($_GET);
		if($query_string)
		{
			$query_string = "?" . $query_string;
		}

		if($exibe_resultado)
		{
			$query_string = "/0/1" . $query_string;
		}

		redirect(get_resolver_simulado_url($simulado_id, false) . $query_string);
	}

	/**
	 * Redireciona para uma questão aleatória de um simulado
	 * 
	 * @since L1
	 * 
	 * @param $simulado_id ID do simulado
	 * @param $total_questoes Total de questões no simulado
	 * 
	 */
	public function ir_para_questao_aleatoria($simulado_id, $total_questoes, $exibe_resultado = FALSE, $tipo_resultado = NULL){
		$aleatoria = mt_rand(0, $total_questoes -1);
		$sufixo = "";
		if($exibe_resultado){
			$sufixo = "/1";
		}
		if($tipo_resultado)
		{
			$sufixo = ($sufixo?:"/0")."/{$tipo_resultado}";
		}
		redirect(get_resolver_simulado_url($simulado_id, false, $aleatoria) . $sufixo);
	}

}