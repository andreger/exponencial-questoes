<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

class Relatorios extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		session_start();

		date_default_timezone_set('America/Sao_Paulo');
		tem_acesso(array(ADMINISTRADOR, PROFESSOR), ACESSO_NEGADO);
		set_time_limit(120);

		if(is_ambiente_desenvolvimento()) {
			$this->output->enable_profiler(TRUE);
		}
	}

	public function questoes_comentadas_resumo($professor_id = 0, $inicio = null, $fim = null, $exportar = 0)
	{
		if(!is_administrador() && !is_coordenador_sq() && $professor_id != get_current_user_id()) {
			redirecionar_para_acesso_negado();
		}

		$this->load->model('Relatorios_model');
		$this->load->helper(array('math', 'relatorio'));

		$data['questoes_comentadas'] = array();
		$data['totais_pagseguro'] = array();

		$inicio = is_null($inicio) ? "2016-01" : $inicio;

		if(is_null($fim)) {
			if(is_administrador() || is_coordenador_sq()) {
				$fim = date('Y-m');
			}
			else {
				$fim = date('Y-m', strtotime('-1 month'));
			}
		}

		for($i = date('Y'); $i >= 2016; $i--) {
			for($j = 12; $j >= 1; $j--) {
				if($i == date('Y') && $j > date('m')) continue;

				$mes = $i . '-' . ($j > 9 ? $j : '0' . $j);

				if($mes < $inicio) { break 2; }
				if($mes > $fim) { continue; }

				$periodos = $this->Relatorios_model->get_questoes_comentadas($j, $i, $professor_id);
				$periodos[0]['mes'] = $j;
				$periodos[0]['ano'] = $i;
				$periodos[0]['mes_ano'] = "$j/$i";
				$periodos[0]['posicao'] = $this->Relatorios_model->get_posicao($j, $i, $professor_id);

				array_push($data['questoes_comentadas'], $periodos[0]);

				$data['totais_pagseguro'][$j][$i] = $this->Relatorios_model->get_totais_pagseguro($j, $i);
				$data['total_coments'][$j][$i] = $this->Relatorios_model->contar_questoes_comentadas_por_usuario($j, $i);
			}
		}

		$dados = explode('-', $inicio);
		$dados_f = explode('-', $fim);

		$data['mes_ano'] = (int)$dados[1] . '/' . $dados[0];
		$data['mes_ano_f'] = (int)$dados_f[1] . '/' . $dados_f[0];
		$data['professor_id'] = $professor_id;

		$data['combo_mes_ano'] = get_meses_combo();
		$data['ultima_atualizacao'] = converter_para_ddmmyyyy($this->Relatorios_model->get_ultima_atualizacao());
		$data['professor'] = $professor_id ? get_usuario_array($professor_id) : null;
		$data['combo_professores'] = get_professores_combo_options();

		$data['num_participantes'] = $this->Relatorios_model->get_num_participantes();
		$data['html_tabela'] = $this->load->view('relatorios/questoes_comentadas_tabela_professor', $data, TRUE);

		if($exportar){
			exportar_excel($data['html_tabela']);
		}
		else{
			$this->load->view('relatorios/questoes_comentadas_professor', $data);
		}
	}

	public function questoes_comentadas($mes = FALSE, $ano = FALSE)
	{
		if(!tem_acesso(array(ADMINISTRADOR, COORDERNADOR_SQ, PROFESSOR))) {
			redirecionar_para_acesso_negado();
		}

		$this->load->model('Relatorios_model');
		$this->load->helper(array('math', 'relatorio'));

		if(!$mes || !$ano){
			if(is_administrador() || is_coordenador_sq()) {
				$mes = date("m");
				$ano = date("Y");
			}
			else {
				$mes = date("m", strtotime('-1 month'));
				$ano = date("Y", strtotime('-1 month'));
			}
		}

		if($this->input->post('mes-ano')){
			$dados = explode("-", $this->input->post('mes-ano'));
			$dados_f = explode("-", $this->input->post('mes-ano-f'));
			$inicio = $dados[1] . '-' . ($dados[0] > 9 ? $dados[0] : '0' . $dados[0]);
			$fim = $dados_f[1] . '-' . ($dados_f[0] > 9 ? $dados_f[0] : '0' . $dados_f[0]);
			$professor_id = $this->input->post('professor_id');

			$exportar = $this->input->post('exportar-excel') ? 1 : 0;
			if(!is_administrador() && !is_coordenador_sq() && is_professor()) {
				$professor_id = get_current_user_id();
				redirect("/questoes/relatorios/questoes_comentadas_resumo/$professor_id/$inicio/$fim/$exportar");
			}
			else {
				if($dados != $dados_f || $professor_id != 0) {
					redirect("/questoes/relatorios/questoes_comentadas_resumo/$professor_id/$inicio/$fim/$exportar");
				}
			}

			$mes = $dados[0];
			$ano = $dados[1];
			$mes_f = $dados[0];
			$ano_f = $dados[1];
		}
		else {

			$mes_f = $mes;
			$ano_f = $ano;

			if(!is_administrador() && !is_coordenador_sq() && is_professor()) {
				$professor_id = get_current_user_id();
				redirect("/questoes/relatorios/questoes_comentadas_resumo/$professor_id");
			}
		}

		$user_id = false;
		if(is_professor() && !is_administrador() && !is_coordenador_sq()){
			$user_id = get_current_user_id();
		}

		$data['ultima_atualizacao'] = converter_para_ddmmyyyy($this->Relatorios_model->get_ultima_atualizacao());
		$data['combo_mes_ano'] = get_meses_combo();
		$data['questoes_comentadas'] = $this->Relatorios_model->get_questoes_comentadas($mes, $ano, $user_id);
		$data['mes_ano'] = $mes."/".$ano;
		$data['mes_ano_f'] = $mes_f."/".$ano_f;
		$data['min_pagamento'] = get_minimo_valor_acumulado_por_data($ano . " - " . ($mes < 10 ? "0" . $mes : $mes) - "-01", VMA_SQ);

		$data['totais_pagseguro'] = $this->Relatorios_model->get_totais_pagseguro($mes, $ano);

		// echo "<pre>";
		// print_r($data['totais_pagseguro']);exit;

		$primeiro_dia_mes = date("Y-m-d", strtotime($ano."-".$mes."-01"));
		$ultimo_dia_mes = date('Y-m-t', strtotime($primeiro_dia_mes));
		
		$tem_dados_travados = (strtotime($ultimo_dia_mes) > strtotime(REL_QUESTOES_COMENTADAS_DATA_FIXADA));
		
		if($tem_dados_travados)
		{
			$data['total_coments_fixo'] = $this->Relatorios_model->get_questoes_comentadas_travado();
			$data['total_comments_pos_travamento'] = $this->Relatorios_model->contar_questoes_comentadas_por_usuario($mes, $ano, FALSE, TRUE);
			$data['data_travamento'] = date("d/m/Y", strtotime(REL_QUESTOES_COMENTADAS_DATA_FIXADA));
		}
		else
		{
			$data['total_coments_fixo'] = null;
		}

		$data['combo_professores'] = get_professores_combo_options();

		$qc_temp = $data['questoes_comentadas'];
		$data['questoes_comentadas'] = array();

		foreach ($qc_temp as $qc) {
			$usuario = get_usuario_array($qc['user_id']);
			$index = slugify($usuario['nome_completo']);
			if($tem_dados_travados)
			{
				$qc['qct_total'] = $this->Relatorios_model->get_questoes_comentadas_travado($qc['user_id']);
			}
			else
			{
				$qc['qct_total'] = null;
			}
			$data['questoes_comentadas'][$index] = $qc;
		}
		ksort($data['questoes_comentadas'], SORT_LOCALE_STRING);

		$total_coments = 0;
		if($data['questoes_comentadas']){
			foreach ($data['questoes_comentadas'] as $coments){
				$total_coments += $coments['qcp_qtde_comentada'];
			}
			$data['total_coments'] = $total_coments;
		}
		
		$data['tem_dados_travados'] = $tem_dados_travados;

		$data['num_participantes'] = $this->Relatorios_model->get_num_participantes();
		$data['html_tabela'] = $this->load->view('relatorios/questoes_comentadas_tabela', $data, TRUE);

		if( $this->input->post('exportar-excel') ){
			exportar_excel($data['html_tabela']);
		}
		else{
			$this->load->view('relatorios/questoes_comentadas', $data);
		}

		if($this->input->post('reprocessar')) {
			$mes = $mes < 10 ? "0" . $mes : $mes;
			$r = urlencode("/questoes/relatorios/questoes_comentadas/$mes/$ano");
			redirect("/questoes/cronjobs/processar_pedidos/$ano/$mes?r=$r");
		}
	}

	public function questoes_comentadas_diff()
	{
		if(!tem_acesso(array(ADMINISTRADOR))) {
			redirecionar_para_acesso_negado();
		}

		$this->load->model('Comentario_model');
		$this->load->model('Relatorios_model');

		$questoes_comentadas = $this->Relatorios_model->get_questoes_comentadas(date('m'), date('Y'));

		$html = "<table border='0' cellspacing='0' cellpadding='0' style='margin-top: 20px' class='table table-bordered table-striped'>";
		$html .= "<tr clas='font-10 bg-blue text-white line-height-1-5'>
					<th>ID</th>
					<th>Nome</th>
					<th>Total Relatório</th>
					<th>Total Base</th>
					<th>Diferença</th>
				</tr>";

		foreach ($questoes_comentadas as $item) {
			$total = $this->Comentario_model->get_total_comentarios_por_usuario($item['user_id']);
			$usuario = get_usuario_array($item['user_id']);
			$diferenca = $total - $item['qcp_qtde_comentada'];

			$diff = $diferenca ?: "";

			$html .= "<tr>";
			$html .= "<td>{$item['user_id']}</td>";
			$html .= "<td>{$usuario['nome_completo']}</td>";
			$html .= "<td>{$item['qcp_qtde_comentada']}</td>";
			$html .= "<td>{$total}</td>";
			$html .= "<td>{$diff}</td>";
			$html .= "</tr>";
		}

		$html .= "</table>";

		echo $html;
	}

 	public function assinaturas($offset = 0) {

		 $this->load->helper('relatorio');
		 KLoader::model("AssinaturaModel");

 		if(!tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) {
			redirecionar_para_acesso_negado();
		}

		$expiracao = $_SESSION['rel_assinaturas']['expiracao'];
		$filtro = $_SESSION['rel_assinaturas']['filtro'];

		if($_POST['filtrar'])
		{
			$expiracao = isset($_POST['data_expiracao']) && $_POST['data_expiracao'] ? converter_para_yyyymmdd($_POST['data_expiracao']) : null;
			$filtro =  isset($_POST['filtro']) ? $_POST['filtro'] : null;

			$_SESSION['rel_assinaturas'] = array('expiracao' => $expiracao, 'filtro' => $filtro);
		}


 		$somente_ativos = (!$filtro && !$expiracao) || ($filtro == FILTRO_ASSINATURAS_SOMENTE_ATIVOS) ? true : false;
 		$somente_expirados = (!$filtro && !$expiracao) || ($filtro == FILTRO_ASSINATURAS_SOMENTE_EXPIRADOS) ? true : false;

		if(isset($_POST['filtro']) && $_POST['filtro'] == FILTRO_ASSINATURAS_A_EXPIRAR_EM_30_DIAS) {
			$expiracao = date('Y-m-d', strtotime("+ 30 days"));
		}

		if($this->input->post("exportar")){
			$limite = null;
		}else{
			$limite = LIMITE_RELATORIO_ASSINATURAS;
		}

		if($assinaturas = AssinaturaModel::listar_alunos_assinaturas($expiracao, $somente_ativos, $somente_expirados, $offset, $limite)) {

			$html = "<table border='0' cellspacing='0' cellpadding='0' style='margin-top: 20px' class='table table-bordered table-striped'>
						<thead>
							<tr class='text-center font-10 bg-blue text-white line-height-1-5'>
								<th>Nome</th>
								<th>E-mail</th>
								<th>Primeira Assinatura</th>
								<th>Última Expiração</th>
								<th>Quant. Renovações</th>
								<th>Tempo Assinatura</th>
								<th>Status</th>
								<th>Pagamento</th>
							</tr>
						</thead>";

			foreach ($assinaturas as $item) {
				$data_inicio = converter_para_ddmmyyyy($item->data_inicio);
				$data_fim = converter_para_ddmmyyyy($item->data_fim);

				if(hoje_yyyymmdd() <= $item->data_fim) {
					$status = "Ativo";

					$data_f = new DateTime($item->data_fim);
					$data_i = new DateTime(hoje_yyyymmdd());
					$intervalo = $data_f->diff($data_i);

					/**
					 * Tempo de assinatura: tempo desde que assinou até quando
					 * expirou. Se ainda não expirou, tempo até o dia atual.
					 * Informar dado em meses.
					 *
					 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/310
					 */

					$tempo = floor(decimal($item->tempo  - ($intervalo->days / 30)));

					// Alguns pedidos de teste possuiam dados inválidos de tempo de assinatura.
					if($tempo < 1) {
						$tempo = 1;
					}
				}
				else {
					$status = "Expirado";
					$tempo = $item->tempo > 1 ? $item->tempo : 1;
				}

				if($item->preco > 0){
					$pagamento = "Pago";
				}else{
					$pagamento = "Grátis";
				}

				$html .= "<tr>";
				$html .= "<td>{$item->display_name}</td>";
				$html .= "<td>{$item->user_email}</td>";
				$html .= "<td>{$data_inicio}</td>";
				$html .= "<td>{$data_fim}</td>";
				$html .= "<td>{$item->qtde}</td>";
				$html .= "<td>{$tempo}</td>";
				$html .= "<td>{$status}</td>";
				$html .= "<td>{$pagamento}</td>";
				$html .= "</tr>";
			}

			$html .= "</table>";

		}
		else {
			$html = "Não foram encontrados dados para essa busca.";
		}

		if($_POST['exportar']){
			exportar_excel($html);
		}
		else {

			$total = AssinaturaModel::contar_alunos_assinaturas($expiracao, $somente_ativos);

			$pag_config = pagination_config("/questoes/relatorios/assinaturas", $total, LIMITE_RELATORIO_ASSINATURAS);
			$this->pagination->initialize($pag_config);
			$links = $this->pagination->create_links();

			if(count($assinaturas)) {
				if($total == 1) {
					$data['total'] = "1 aluno encontrado";

				} else {
					$inicio = $offset + 1;
					$fim = count($assinaturas) + $offset;
					$data['total'] = "{$total} alunos encontrados (exibindo $inicio - $fim)";

				}
			} else {
				$data['total'] = "";
			}

			$data['links'] = $links;

			$data['tabela'] = $html;
			$data['expiracao'] = $expiracao;

			if(isset($_SESSION['rel_assinaturas']['filtro'])) {
				$data['filtro_selecionado'] = $_SESSION['rel_assinaturas']['filtro'];
			}
			else {
				if(!isset($_SESSION['rel_assinaturas']['expiracao'])) {
					// filtro padrão (Somente Ativos)
					$data['filtro_selecionado'] = FILTRO_ASSINATURAS_SOMENTE_ATIVOS;
				}
			}

			$this->load->view('relatorios/assinaturas', $data);
		}
	 }

	public function avaliacoes_comentarios($pagina = null){

		if(!tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR))) {
			redirecionar_para_acesso_negado();
		}

		$this->load->helper('relatorio');
		$this->load->helper('link');
		$this->load->model('Relatorios_model');
		$this->load->library('pagination');

		$data['include_mask'] = true;

		$data['media_avaliacao_comparador_combo'] = get_media_avaliacao_comparador_combo();
		$data['avaliacao_comentario_ordem_combo'] = get_avaliacao_comentario_ordem_combo();
		$data['avaliacao_comentario_sentido_combo'] = get_avaliacao_comentario_sentido_combo();
		$data['combo_professores'] = get_professores_combo_options();

		$is_busca_sessao = TRUE;
		if(is_null($pagina) || $this->input->post('submit')){
			$pagina = 0;
			unset($_SESSION['rel_avaliacoes_comentarios_filtro']);
			$is_busca_sessao = FALSE;
		}

		if($is_busca_sessao && isset($_SESSION['rel_avaliacoes_comentarios_filtro'])){

			$valores = $_SESSION['rel_avaliacoes_comentarios_filtro'];
			$_SESSION['rel_avaliacoes_comentarios_filtro']['offset'] = $pagina;

			$professor_id = $valores['professor_id'];
			$media_avaliacao = $valores['media_avaliacao'];
			$media_avaliacao_comparador = $valores['media_avaliacao_comparador'];
			$quantidade_avaliacoes = $valores['quantidade_avaliacoes'];
			$ordem = $valores['ordem'];
			$ordem_dir = $valores['ordem_dir'];

		}else{

			if(!is_administrador() && !is_coordenador_sq() && is_professor()) {
				$professor_id = get_current_user_id();
			}else{
				$professor_id = $this->input->post('professor_id');
			}
			$media_avaliacao = $this->input->post('media_avaliacao');
			$media_avaliacao_comparador = $this->input->post('media_avaliacao_comparador');
			$quantidade_avaliacoes = $this->input->post('quantidade_avaliacoes');
			$ordem = $this->input->post('ordem');

			if(empty($ordem)){
				$ordem = 'que.que_id';
			}

			$ordem_dir = $this->input->post('ordem_dir');
			if(empty($ordem_dir)){
				$ordem_dir = 'ASC';
			}

			$_SESSION['rel_avaliacoes_comentarios_filtro'] = array(
				'professor_id' => $professor_id,
				'media_avaliacao' => $media_avaliacao,
				'media_avaliacao_comparador' => $media_avaliacao_comparador,
				'quantidade_avaliacoes' => $quantidade_avaliacoes,
				'ordem' => $ordem,
				'ordem_dir' => $ordem_dir,
				'offset' => 0
			);

		}

		$result = $this->Relatorios_model->get_avaliacao_comentario_questao(true, $professor_id, $media_avaliacao, $media_avaliacao_comparador, $quantidade_avaliacoes, null);

		if($result){
			if($result['qtd'] > 0){
				$media = number_format($result['media']/$result['qtd'], 2, ',', '');
				$media_simples = number_format($result['media_simples']/$result['qtd'], 2, ',', '');
			}else{
				$media = 0;
				$media_simples = 0;
			}
			$data['total_avaliacoes_media'] = $media;
			$data['total_avaliacoes_media_simples'] = $media_simples;
			$data['total_avaliacoes'] = $result['qtd'];
		}

		$result = $this->Relatorios_model->get_avaliacao_comentario_questao(false, $professor_id, $media_avaliacao, $media_avaliacao_comparador, $quantidade_avaliacoes, $ordem." ".$ordem_dir, $pagina, 30);

		if($result){

			$html = "<table border='0' cellspacing='0' cellpadding='0'  class='mt-3 table table-bordered table-striped'>";
			$html .= "<thead><tr class='font-10 bg-blue text-white line-height-1-5'>
						<th>Id da Questão</th>
						<th>Professor</th>
						<th>Quantidade de avaliações</th>
						<th>Média das avaliações</th>
						<th>Quantidade de avaliações válidas</th>
						<th>Média das avaliações válidas</th>
					</tr></thead>";

			foreach ($result as $item) {

				$url = get_resolver_questoes_url(TIPO_QUESTAO, $item['que_id']);

				$html .= "<tr>";
				$html .= "<td class='text-center'><a href='{$url}'>{$item['que_id']}</a></td>";
				$html .= "<td>{$data['combo_professores'][$item['user_id']]}</td>";
				$html .= "<td class='text-center'>{$item['qtd']}</td>";
				$html .= "<td class='text-center'>".number_format($item['media_simples'], 2, ',','')."</td>";
				$html .= "<td class='text-center'>{$item['qtd_media']}</td>";
				$html .= "<td class='text-center'>".number_format($item['media'], 2, ',','')."</td>";
				$html .= "</tr>";
			}

			$html .= "</table>";
			$html_tabela = $html;
		}else{
			$html_tabela = "Nenhum resultado encontrado";
		}

		$config['base_url'] = get_rel_avaliacoes_comentarios_url();
		$config['total_rows'] = $data['total_avaliacoes'];
		$config['per_page'] = 30;
		$config['attributes'] = array('class' => 'mr-1 btn u-btn-blue  paginacao-rel');
		$config['cur_tag_open'] = '<span class="mr-1 btn u-btn-white paginacao-select">';
		$config['cur_tag_close'] = '</span>';
		$config["first_url"] = get_rel_avaliacoes_comentarios_url()."/0";
		$config['last_link'] = '>>';
		$config['first_link'] = '<<';

		$this->pagination->initialize($config);

		$links = $this->pagination->create_links();

		$data['html_tabela'] = $html_tabela;
		$data['links'] = $links;
		$data['professor_id'] = $professor_id;
		$data['media_avaliacao'] = $media_avaliacao;
		$data['media_avaliacao_comparador'] = $media_avaliacao_comparador;
		$data['quantidade_avaliacoes'] = $quantidade_avaliacoes;
		$data['ordem'] = $ordem;
		$data['ordem_dir'] = $ordem_dir;
		$data['pagina'] = $pagina;

		// $this->load->view(get_main_header_view_url(), $data);
		$this->load->view('/relatorios/avaliacoes_comentarios', $data);
		// $this->load->view(get_main_footer_view_url(), $data);

	}

}