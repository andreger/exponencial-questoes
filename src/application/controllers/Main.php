<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

KLoader::helper("AcessoGrupoHelper");

class Main extends CI_Controller {
	public function __construct() {
	    
		parent::__construct ();

		if(is_area_desativada(array(PAINEL_SQ_GERAL))){
			redirecionar_erro_500();
		}

		session_start();

		$this->load->model ( 'disciplina_model' );
		$this->load->model ( 'banca_model' );
		$this->load->model ( 'orgao_model' );
		$this->load->model ( 'cargo_model' );
		$this->load->model ( 'assunto_model' );
		$this->load->model ( 'prova_model' );
		$this->load->model ( 'questao_model' );

		$this->load->model ( 'area_formacao_model' );
		$this->load->model ( 'area_atuacao_model' );
		$this->load->model ( 'simulado_model' );
		$this->load->model ( 'comentario_model' );
		$this->load->model ( 'caderno_model' );
		$this->load->model ( 'configuracao_usuario_model' );

		$this->load->helper('questao');
		$this->load->helper('simulado');
		$this->load->helper('prova');
		$this->load->helper('aluno');
		$this->load->helper('filtro');

		init_profiler();
	}

	public function index()
	{
		redirect(get_resolver_questoes_url());
	}

	public function assine()
	{
		$this->load->view('main/assine');
	}

	public function check_default($post_string) {
		return $post_string == OPCAO_ZERO ? FALSE : TRUE;
	}

	public function imprimir_gabarito($simulado_id) {
		$questoes = $this->questao_model->listar_por_simulado($simulado_id, LIMIT_LISTAR_QUESTOES);

		$data['gabarito'] = get_gabarito($questoes, NUM_COLUNAS_GABARITO);
		$data['simulado'] = $this->simulado_model->get_by_id($simulado_id);

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view('main/imprimir_gabarito', $data);
		$this->load->view(get_main_footer_view_url(), $data);
	}

	public function imprimir_cartao_resposta($simulado_id)
	{
		$questoes = $this->questao_model->listar_por_simulado($simulado_id, LIMIT_LISTAR_QUESTOES);

		$linha = 0;
		$linhas = array();

		for($i = 0; $i < count($questoes); $i++) {

			switch ($i % 3) {
				case 0 : $mod = 'esq'; break;
				case 1 : $mod = 'cen'; break;
				case 2 : $mod = 'dir'; break;
			}

			$linhas[$linha]['num_' . $mod] = $i + 1;

			if($questoes[$i]['que_tipo'] == MULTIPLA_ESCOLHA) {
				$num_opcoes = $this->questao_model->contar_opcoes($questoes[$i]['que_id']);
				if($num_opcoes > 5) {
					$num_opcoes = 5;
				}

				$opcoes = array(1 => 'a', 2 => 'b', 3 => 'c', 4=> 'd', 5 => 'e');

				$linhas[$linha]['opcoes_' . $mod] = "";
				for($j = 1; $j <= $num_opcoes; $j++) {
					$linhas[$linha]['opcoes_' . $mod] .= " (" . $opcoes[$j] . ")";
				}

			}
			else {
				$linhas[$linha]['opcoes_' . $mod] = "(C) (E)";
			}

			if($i % 3 == 2) {
				$linha++;
			}
		}

		$data['linhas'] = $linhas;
		$data['simulado'] = $this->simulado_model->get_by_id($simulado_id);

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view('main/imprimir_cartao_resposta', $data);
		$this->load->view(get_main_footer_view_url(), $data);
	}

	public function get_main_ass_id_ajax() {
		$string = '';
		$first = 1;
		$array = array();
		$disciplinas_ids = $this->input->get('dis_ids');
		foreach ($disciplinas_ids as $disciplina_id) {
			$assuntos = $this->assunto_model->listar_por_disciplina($disciplina_id);
			foreach ($assuntos as $assunto) {
				if (! in_array ( $assunto, $array )) {
					array_push($array, $assunto);
					if($first == 1)
						$string = '[ [ "' . $assunto['ass_id'] . '", "' . $assunto['ass_nome'] . '" ]';
					else
						$string .= ', [ "' . $assunto['ass_id'] . '", "' . $assunto['ass_nome'] . '" ]';

					$first = 0;
				}
			}
		}
		$string .= ' ]';
		echo $string;
	}

	public function get_ajax_main_responder_url()
	{
		$usuario_id = get_id_usuario_atual();

		$questao_id = $this->input->post("id");
		$caderno_id = $this->input->post("caderno_id") ? $this->input->post("caderno_id") : null;
		$simulado_id = $this->input->post("simulado_id") ? $this->input->post("simulado_id") : null;
		$selecao = $this->input->post("selecao");
		$resposta = $this->questao_model->get_resposta($questao_id);
		$questao = $this->questao_model->get_by_id($questao_id);

		if(!tem_acesso(array(ADMINISTRADOR, PROFESSOR, CONSULTOR, REVISOR, ASSINANTE_SQ))) {
			$num_respondidas = $this->questao_model->contar_questoes_respondidas($usuario_id);
			//log_sq('debug', "Nao assinante usuario id: {$usuario_id} respondeu {$num_respondidas} das ". get_max_respondidas_nao_assinante() . " permitidas");

			if(is_null($caderno_id) && is_null($simulado_id) && $num_respondidas >= get_max_respondidas_nao_assinante()) {
				//log_sq('debug', "Nao assinante usuario id : {$usuario_id} nao pode mais responder questoes hoje");
				echo "<script>$('#link-modal-assine-sq').click()</script>";
				exit;
			}

		}

		if($resposta == $selecao) {
			$mensagem = "<span class='resposta-mensagem resposta-certa'><img src='/wp-content/themes/academy/images/mensagem-namosca.png'> Parabéns, você acertou!</span>";
			$resultado = CERTO;
		} else {
			$txt_resposta = $resposta ? "A resposta certa é <strong>" . get_letra_opcao_questao($resposta, $questao['que_tipo']) ."</strong>" : null;

			$mensagem = "<span class='resposta-mensagem resposta-errada'><img src='/wp-content/themes/academy/images/mensagem-errada.png'> Resposta errada! $txt_resposta</span>";
			$resultado = ERRADO;
		}
		$this->questao_model->salvar_resposta_usuario($usuario_id, $questao_id, $selecao, $simulado_id, $caderno_id);

		//Se for resposta de caderno de questões deve-se atualizar os totais de acertos e erros do caderno
		if($caderno_id){
			$incluir_inativas = AcessoGrupoHelper::cadernos_questoes_inativas();
			$this->caderno_model->atualizar_totais($caderno_id, $usuario_id, $incluir_inativas);
		}

		echo $mensagem;
	}

	public function resolver_simulado($sim_id, $pagina = 0, $revisar_simulado = FALSE, $tipo_resultado = SIMULADO_QUESTAO_TODAS)
	{
		$this->load->helper('cookie');

		$conf_usuario = get_config_aluno();
		$data['check_acertos'] = $conf_usuario  ?  $conf_usuario['mostrar_acertos'] : 1;
		$data['check_filtros'] = $conf_usuario  ?  $conf_usuario['exibir_filtros'] : 1;
		$data['check_historico'] = $conf_usuario  ?  $conf_usuario['manter_historico_marcacao_cadernos'] : 1;
		$data['check_assuntos'] = $conf_usuario ? $conf_usuario['exibir_assuntos'] : 0;
		$data['cabecalho_ordem'] = $conf_usuario ? $conf_usuario['ordem_cabecalho'] : "ID,Ano,Bancas,Órgãos,Provas,Disciplina,Assuntos";

		if(is_area_desativada(array(PAINEL_SQ_SIMULADOS))){
			redirecionar_erro_500();
		}

		$this->load->helper('simulado');
		$this->load->helper('ranking');

		$has_simulado = tem_acesso_ao_simulado($sim_id, get_current_user_id());
		if(!$has_simulado) redirecionar_para_acesso_negado();

		$data['exibir_resultado'] = false;

		$data['tem_filtro_questoes'] = false;

		$data['simulado'] = $this->simulado_model->get_by_id($sim_id);

		if($_POST['exibir_resultado'] || $revisar_simulado || $this->input->post('tipo_resultado_filtrar')) {

			$simulado_usuario = $this->simulado_model->get_usuario_simulado($sim_id, get_current_user_id());

			$data['exibir_resultado'] = true;

			// Exibição de resultado após finalização de simulado
			if($_POST['exibir_resultado']) {
				$data['exibir_info_resposta'] = true;

				//$data['respostas'] = isset($_POST['opcao_questao']) ? $_POST['opcao_questao'] : [];
				//Mudou para o cookie por causa do cockpit que só exibe uma questão por vez, na visão normal todas as questões já eram exibidas
				$data['respostas'] = json_decode(stripslashes(get_cookie('resp_sim_' . $sim_id)), true);
				$this->simulado_model->resolver_simulado($sim_id, $data['respostas']);
				$tempo = $this->input->post('tempo');

				// grava nota das disciplinas
				$data['nota'] = $this->simulado_model->salvar_nota(FALSE, get_current_user_id(), $sim_id);

				// grava duração e hora da resolução
				$this->simulado_model->finalizar_simulado($simulado_usuario['siu_id'], $tempo);

				//busca os dados do ranking
				$data['ranking'] = self::get_ranking_por_usuario($sim_id, get_current_user_id());

				delete_cookie('resp_sim_' . $sim_id);
				delete_cookie('cron_sim_' . $sim_id);
			}

			// Exibição de resultado após clicar em Revisar simulado
			else {
				$data['exibir_info_resposta'] = $simulado_usuario['siu_exibir_detalhes'];

				$data['respostas'] = $this->simulado_model->listar_questoes_respondidas_respostas($sim_id, get_current_user_id());

				$data['nota'] = $this->simulado_model->get_nota(get_current_user_id(), $sim_id);

				$tempo = $simulado_usuario['siu_tempo_gasto'];

				//busca os dados do ranking
				$data['ranking'] = self::get_ranking_por_usuario($sim_id, get_current_user_id());
			}

			$resultado_simulado = $this->simulado_model->get_resultado_por_simulado(get_current_user_id(), $sim_id);

			$data['acertos'] = $resultado_simulado['sre_acertos'];
			$data['aprovado'] = $this->simulado_model->is_aprovado(get_current_user_id(), $sim_id);
			$data['posicao'] = $this->simulado_model->get_posicao_ranking($sim_id, get_current_user_id());

			$tempo_por_questao = isset($data['respostas']) && count($data['respostas']) > 0 ? $tempo / count($data['respostas']) / 60 : 0;
			$data['tempo_por_questao'] = $tempo_por_questao < 1 ? "< 1 min" : decimal($tempo_por_questao, 'min');
			$data['tempo_total'] = gmdate("H:i:s", $tempo);

			if( $this->input->post('tipo_resultado') )
			{
				$tipo_resultado = $this->input->post('tipo_resultado');
			}

			if(!$tipo_resultado) {
			    $tipo_resultado = SIMULADO_QUESTAO_TODAS;//SIMULADO_QUESTAO_NAO_RESOLVIDAS;
			} else {
				$data['tem_filtro_questoes'] = true;
			}

			$data['tipo_resultado'] = $tipo_resultado;

			$data['detalhes'] = [];
			$disciplinas = $this->simulado_model->listar_disciplinas_simulado($sim_id, 'sd.sid_ordem', 'asc');
			foreach($disciplinas as $item) {
				$resultado = $this->simulado_model->get_resultado_disciplina($sim_id, $item['dis_id'], get_current_user_id());

				$detalhe = [
					'nome' => $item['dis_nome'],
					'qtde' => $item['qtde'],
					'acertos' => $resultado['sre_acertos'],
					'nota' => $resultado['sre_resultado']
				];

				//Se erros subtraem os acertos
				if(($data['simulado']['sim_pontos_negativos'] && $data['simulado']['sim_peso_erros']) || $data['ranking']['ran_peso_erros'] ){
					if($data['simulado']['sim_peso_erros'])
					{
						$erros = ($resultado['sre_acertos'] - $resultado['sre_resultado'])/$data['simulado']['sim_peso_erros'];
					}
					else
					{
						$erros = (($resultado['sre_acertos'] * get_peso_utilizado($data['ranking']['ran_id'], $item['dis_id'])) - $resultado['sre_resultado'])/$data['ranking']['ran_peso_erros'];
					}
					$detalhe['erros'] = $erros;
					$detalhe['branco'] = $item['qtde'] - $resultado['sre_acertos'] - $erros;
				}

				array_push($data['detalhes'], $detalhe);
			}

		}
		else {
			if($page == 0) {
				$this->simulado_model->zerar_tempo_gasto($sim_id, get_current_user_id());
			}
		}

		$data['is_resolver_simulado'] = $revisar_simulado ? false : true;
		$data['simulado_id'] = $sim_id;

		if(!isset($_SESSION['mrq']) || $_SESSION['mrq'] == MODO_QUESTOES_COCKPIT){
			$data['limite'] = 1;
			$data['pagina'] = $pagina;
		}
		
		$data['total'] = $this->simulado_model->contar_questoes_simulado($sim_id, $tipo_resultado);

		if($data['total'] > 0) {
			$data['questoes'] = $this->questao_model->listar_por_simulado($sim_id, $data['limite']?:null, $data['pagina']?:null, null, $tipo_resultado);

			foreach ($data['questoes'] as &$questao) {
				$questao['total_comentarios'] = $this->comentario_model->contar_comentarios($questao['que_id']);
			}

		} else {
			$data['erro'] ='Não foram encontradas questões nesse simulado.';
		}
		
		$data['is_exibe_modal_resultado'] = isset($_POST['exibir_resultado']);
		
		if(!isset($_SESSION['mrq']) || $_SESSION['mrq'] == MODO_QUESTOES_COCKPIT)
		{
			$data['is_usa_cockpit'] = true;
			$data['questao'] = $data['questoes'][0];
			$data['cockpit_questao_id'] =$data['questao']['que_id'];
			$data['anotacoes'] = $this->questao_model->listar_questoes_anotacoes($data['questao']['que_id'], get_current_user_id());
			$data['links'] = $this->load->view('questoes/rodape/paginacao_cockpit', 
				[
					'url' => get_resolver_simulado_url($sim_id, false),
					'pagina_atual' => $pagina,
					'total' => $data['total'],
					'simulado_id' => $sim_id,
					'is_exibir_resultado' => $data['exibir_resultado'],
					'tipo_resultado' => $tipo_resultado
				], TRUE
			);
		}

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view(get_resolver_simulado_view_url(), $data);
		$this->load->view(get_main_footer_view_url(), $data);
	}

	public function resolver_caderno($caderno_id, $page = 0, $tipo = TODAS)
	{
		$conf_usuario = get_config_aluno();
		$data['check_acertos'] = $conf_usuario  ?  $conf_usuario['mostrar_acertos'] : 1;
		$data['check_filtros'] = $conf_usuario  ?  $conf_usuario['exibir_filtros'] : 1;
		$data['check_historico'] = $conf_usuario  ?  $conf_usuario['manter_historico_marcacao_cadernos'] : 1;
		$data['check_url_cad'] = $conf_usuario  ?  $conf_usuario['exibir_url_imprimir_caderno'] : 1;
		$data['check_assuntos'] = $conf_usuario ? $conf_usuario['exibir_assuntos'] : 0;
		$data['cabecalho_ordem'] = $conf_usuario ? $conf_usuario['ordem_cabecalho'] : "ID,Ano,Bancas,Órgãos,Provas,Disciplina,Assuntos";

		if(is_area_desativada(array(PAINEL_SQ_CADERNOS))){
			redirecionar_erro_500();
		}

		$incluir_inativas = tem_acesso(array(ADMINISTRADOR, PROFESSOR, REVISOR));

		$this->load->helper('caderno');

		$data['limite'] = $_SESSION['qpp'] ?: 10;

		zerar_filtro();
		zerar_respostas_marcadas();

		//Caso do caderno público: se é um caderno compartilhado, o usuário do caderno é zero e é há usuário logado então redireciona para o caderno público resolver
		$caderno = $this->caderno_model->get_caderno($caderno_id);
		if( $caderno['cad_compartilhado'] && !$caderno['usu_id'] && get_current_user_id() )
		{
			$caderno_compartilhado_url = get_caderno_compartilhado_url( $caderno['cad_ref_id'] );
			redirect( $caderno_compartilhado_url );
			exit;
		}

		// se não for dono do caderno redireciona
		if(!is_dono_do_caderno($caderno_id) && !tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) {
			$redirecionar = ACESSO_NEGADO_SQ;
			header("Location: $redirecionar");
			exit;
		}

		// se caderno for comprado não precisa ser assinante
		if(!is_dono_do_caderno($caderno_id) && !is_caderno_comprado($caderno_id) && !tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) {

		    tem_acesso(array(
					ADMINISTRADOR, PROFESSOR, CONSULTOR, ASSINANTE_SQ, REVISOR
			), ACESSO_NEGADO_SQ);


		}

		// se for caderno de pedido (oriundo de uma aula) então depende do produto comprado ainda estar disponível
		$caderno_pedido = $this->caderno_model->get_caderno_pedido($caderno_id);
		if($caderno['cad_comprado'] && $caderno_pedido['cpe_prd_id']){
			if(is_produto_indisponivel($caderno_pedido['cpe_prd_id'], $caderno_pedido['cpe_ped_id'])){
				$redirecionar = ACESSO_NEGADO_SQ;
				header("Location: $redirecionar");
				exit;
			}
		}

		//Se for um caderno de coaching e não tiver mais acesso de aluno de coaching então redireciona e exibe um aviso
		if($caderno['cad_compartilhado'] && $caderno['cad_coaching'] && !AcessoGrupoHelper::caderno_coaching_acessar())
		{
			set_mensagem(AVISO,"Desculpe, esse caderno está disponível apenas para alunos de Coaching. Caso seja relacionado a um produto do Exponencial, entre em contato conosco para ajudarmos a solucionar o problema'. <a href='/fale-conosco' class='alert-link'>Fale conosco</a>");
			redirect(get_meus_cadernos_url());
		}

		$data['filtro_favoritas'] = $this->input->get('filtro_favoritas')?: NULL;
		$data['filtro_url'] = get_resolver_caderno_url($caderno_id);
		$data['tipo'] = null;
		init_combos($data, TRUE, TRUE);

		$this->caderno_model->salvar_acesso($caderno_id);

		$data['caderno_id'] = $caderno_id;
		$data['caderno'] = $caderno;
		$data["is_caderno"] = true;
		$data["is_caderno_comprado"] = is_caderno_comprado($caderno_id);
		$data['status'] = get_status_questao_combo();
		$data["questoes_por_pagina_combo"] = get_questoes_por_pagina_array();

		$filtro = array();

		if($this->input->get('buscar')) {

			if($this->input->get('status_ids'))
			{
				$filtro['status_ids'] = $this->input->get('status_ids');
			}
			if ($this->input->get('filtro_favoritas')) {
				$filtro['favoritas'] = $this->input->get('filtro_favoritas');
			}
			if ($this->input->get('data_inicio')) {
				$filtro['data_inicio'] = $this->input->get('data_inicio');
			}
			if ($this->input->get('data_fim')) {
				$filtro['data_fim'] = $this->input->get('data_fim');
			}
			if ($this->input->get('busca')) {
				$filtro['busca'] = $this->input->get('busca');
			}

			$filtro['cad_id'] = $caderno_id;

			if($_GET['ass_ids'] && !is_array($_GET['ass_ids'])){
				$_GET['ass_ids'] = explode(',', $_GET['ass_ids']);
			}

			adicionar_filtro($filtro, $data, $this, FALSE);
			
			$data['apenas_assunto_pai'] = $this->input->get("apenas_assunto_pai");
			if(!$data['apenas_assunto_pai'])
			{
				adicionar_assuntos_filhos($filtro);
			}

			if($filtro['dis_ids']) {
				if(!is_array($filtro['dis_ids'])){
					$filtro['dis_ids'] = explode(',', $filtro['dis_ids']);
				}
				$data['assuntos'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplinas($filtro['dis_ids']));
			}


			//$string_filtro = serialize($filtro);
			//$_SESSION['filtro'] =  $string_filtro;
		}

		else {

			//$string_filtro = $_SESSION['filtro'];
			//$filtro = unserialize($string_filtro);
			$filtro['cad_id'] = $caderno_id;

			//if($filtro['dis_ids']) {
			//	$data['assuntos'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplinas($filtro['dis_ids']));
			//}

			preencher_formulario_filtro($filtro, $data);
		}

		$data['filtro_favoritas'] = $filtro['favoritas'] ?: NULL;
		$data['busca'] = $filtro['busca']?: NULL;
		$data['data_inicio'] = $filtro['data_inicio']?: NULL;
		$data['data_fim'] = $filtro['data_fim']?: NULL;

		$data['limite'] = isset($_SESSION['qpp']) ? $_SESSION['qpp'] : 10;
		if(!isset($_SESSION['mrq']) || $_SESSION['mrq'] == MODO_QUESTOES_COCKPIT){
			$data['limite'] = 1;
		}
		$total = $this->questao_model->get_total_resultado( $filtro, $incluir_inativas );
		$config = pagination_config(get_resolver_caderno_url($caderno_id), $total, $data['limite'], 4);
		$config['reuse_query_string'] = TRUE;
        $this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();
		$data['filtro'] = $filtro;

		$data['has_filtro_caderno'] = count($filtro) > 1;

		if($total > 0) {
			if($this->input->get('remove_questoes_caderno') == 'S') {
				$questoes = $this->questao_model->get_ids_questoes( $filtro );

				$arr_questao_id = array();
				foreach ($questoes as $q) {
					$arr_questao_id[] = $q['que_id'];
				}
				$total = $this->questao_model->remove_questoes_caderno($caderno_id, $arr_questao_id);
				$data['successo'] = "Todas as $total questões filtradas foram removidas do caderno. É necessário limpar o filtro.</a>";
				$data['questoes'] = array();
				$total = 0;
			} else {
				$data['questoes'] = $this->questao_model->listar_com_opcoes_e_assuntos($filtro, $data['limite'], $page, $incluir_inativas);

				foreach ($data['questoes'] as &$questao) {
					$questao['total_comentarios'] = $this->comentario_model->contar_comentarios($questao['que_id']);
				}

			}


		} else {
			$data['erro'] = "Não foram encontradas questões nesse caderno. <a href='/questoes/main/resolver_por_filtro/{$caderno_id}'> Deseja filtrar em toda a base?</a>";
		}

		$data['page'] = $page;
		$data['total'] = $total;

		$data['is_filtro_bloqueado'] = is_area_desativada(array(PAINEL_SQ_FILTRO_QUESTOES));
		$data['is_usa_barra_tec'] = TRUE;

		$query_string = http_build_query($_GET);
		$query_string = str_replace("zf=1", "", $query_string);
		if($query_string){
			$query_string = "?" . $query_string;
		}
		$data['query_string'] = $query_string;
		
		if(!isset($_SESSION['mrq']) || $_SESSION['mrq'] == MODO_QUESTOES_COCKPIT){
			$data['is_usa_cockpit'] = true;
			$data['questao'] = $data['questoes'][0];
			$data['cockpit_questao_id'] =$data['questao']['que_id'];
			$data['anotacoes'] = $this->questao_model->listar_questoes_anotacoes($data['questao']['que_id'], get_current_user_id());
			$data['links'] = $this->load->view('questoes/rodape/paginacao_cockpit', 
				[
					'url' => get_resolver_caderno_url($caderno_id),
					'pagina_atual' => $page,
					'total' => $total,
					'caderno_id' => $caderno_id,
					'query_string' => $query_string
				], TRUE
			);
		}

		$data['filtros_selecionados'] = get_filtros_selecionados($filtro);
		$data['has_filtro'] = count($data['filtros_selecionados']) > 0 && !nenhum_filtro_aplicado($filtro);

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view(get_resolver_questoes_view_url(), $data);
		$this->load->view(get_main_footer_view_url(), $data);
	}

	public function alterar_questoes_por_pagina($questoes_por_pagina, $offset, $disciplina_id = NULL, $url = NULL)
	{
		$_SESSION['qpp'] = $questoes_por_pagina;
		$novo_offset =  get_novo_offset($questoes_por_pagina, $offset);
		$sufixo = $novo_offset ? "/{$novo_offset}" : "";

		$filtro = $_SERVER["QUERY_STRING"];
		if($filtro){
			$filtro = "?".$filtro;
		}

		if($disciplina_id)
		{
			$url = get_resolver_favoritas_url( $disciplina_id );
		}
		elseif($url)
		{
			$url = urldecode( urldecode( $url ) );
		}
		else
		{
			$url = get_resolver_questoes_url();
		}
		redirect($url . $sufixo . $filtro);
	}

	/**
	 * Altera o modo de visualizar a resolução de questões e redireciona para a tela de resolver questões
	 * 
	 * @since L1
	 * 
	 * @param int $modo Tipo de visulização das questões: MODO_QUESTOES_XXX
	 * @param int $offset Offset atual da visualização de questões
	 * 
	 */
	public function alterar_modo_resolver_questoes($modo, $offset = 0, $disciplina_id = NULL, $url = NULL)
	{
		$qqp = $_SESSION['qpp'];

		//Se o modo não mudar não mudo nada
		if($_SESSION['mrq'] != $modo){
			
			$_SESSION['mrq'] = $modo;

			if($_SESSION['mrq'] == MODO_QUESTOES_COCKPIT){
				$qqp = 1;//Cockpit é de um em um
			}else{
				$qqp = 10;//Padrão
			}
		}

		self::alterar_questoes_por_pagina($qqp, $offset, $disciplina_id, $url);
	}

	public function resolver()
	{
		//$_SESSION['filtro'] = null;
		redirect(get_resolver_questoes_url());
	}

	public function resolver_questao($id)
	{
		self::resolver_questoes(TIPO_QUESTAO, $id, TODAS, TRUE);
	}

	public function resolver_por_filtro($caderno_id)
	{
		$caderno = $this->caderno_model->get_caderno($caderno_id);
		$_SESSION['filtro'] = $caderno['cad_filtro'];
		redirect(get_resolver_questoes_url());
	}

	public function resolver_por_disciplina($disciplina_id)
	{
		$_SESSION['filtro'] = serialize(array('dis_ids' => array($disciplina_id)));
		redirect(get_resolver_questoes_url());
	}


	public function resolver_questoes($tipo = null, $id = null, $filtro_questoes = NULL, $novo_link = FALSE)
	{
	    $data = [];

	    $incluir_inativas = tem_acesso(array(ADMINISTRADOR, PROFESSOR, REVISOR));

		$conf_usuario = get_config_aluno();
		$data['check_acertos'] = $conf_usuario ? $conf_usuario['mostrar_acertos'] : 1;
		$data['check_filtros'] = $conf_usuario ? $conf_usuario['exibir_filtros'] : 1;
		$data['check_historico'] = $conf_usuario ? $conf_usuario['manter_historico_marcacao_cadernos'] : 1;
		$data['check_assuntos'] = $conf_usuario ? $conf_usuario['exibir_assuntos'] : 1;
		$data['cabecalho_ordem'] = $conf_usuario ? $conf_usuario['ordem_cabecalho'] : "ID,Ano,Bancas,Órgãos,Provas,Disciplina,Assuntos";
		//$this->load->helper('link');

		if($tipo == TIPO_QUESTAO && $id && !$novo_link) {
			$questao = $this->questao_model->get_by_id($id, true);
			redirect(get_questao_seo_url($questao, 'location', 301));
		}

		zerar_filtro();

		$this->session->set_userdata(ULTIMA_URL, $_SERVER['REQUEST_URI']);

		$filtro = array();

		if($filtro_questoes) {
			$filtro['filtro_incluir'] = $filtro_questoes;
		}

		$url = get_resolver_questoes_url();
		$uri = "";
		$segmento = 3;

		if(!is_null($tipo) && !is_null($id)) {
			if($tipo == TIPO_ASSUNTO) {
				$filtro['ass_ids'] = array($id);
				$segmento = 6;
				$url = "/questoes/main/resolver_questoes/{$tipo}/{$id}/{$filtro_questoes}";
				$uri = "{$tipo}/{$id}/{$filtro_questoes}";
			}
			elseif ($tipo == TIPO_DISCIPLINA) {
				$filtro['dis_ids'] = array($id);
				$segmento = 6;
				$url = "/questoes/main/resolver_questoes/{$tipo}/{$id}/{$filtro_questoes}";
				$uri = "{$tipo}/{$id}/{$filtro_questoes}";
			}
			elseif ($tipo == TIPO_BANCA) {
				$filtro['ban_ids'] = array($id);
				$segmento = 6;
				$url = "/questoes/main/resolver_questoes/{$tipo}/{$id}/{$filtro_questoes}";
				$uri = "{$tipo}/{$id}/{$filtro_questoes}";
			}
			elseif ($tipo == TIPO_PROVA) {
				$filtro['pro_ids'] = array($id);
				$segmento = 6;
				$url = "/questoes/main/resolver_questoes/{$tipo}/{$id}/{$filtro_questoes}";
				$uri = "{$tipo}/{$id}/{$filtro_questoes}";
			}
			elseif ($tipo == TIPO_BANCA_DISCIPLINA) {
				$ids = explode("-", $id);

				$filtro['ban_ids'] = array($ids[0]);
				$filtro['dis_ids'] = array($ids[1]);
				$segmento = 6;
				$url = "/questoes/main/resolver_questoes/{$tipo}/{$id}/{$filtro_questoes}";
				$uri = "{$tipo}/{$id}/{$filtro_questoes}";
			}
			elseif ($tipo == TIPO_SIMULADO) {
				$filtro['sim_ids'] = array($id);
			}
			elseif ($tipo == TIPO_QUESTAO) {
				$filtro['que_ids'] = array($id);
				$data['is_tipo_questao'] = true;
			}
			elseif ($tipo == FAVORITAS) {
				$filtro['dis_ids'] = array($id);
				$filtro["filtro_incluir"] = FAVORITAS;
				$data['is_favoritas'] = $id;
				$segmento = 4;
				$url = "/questoes/main/resolver_favoritas/{$id}";
			}

			//$_SESSION['filtro'] =  serialize($filtro);
		}

		$data['filtro_favoritas'] = $this->input->get('filtro_favoritas')?: NULL;

		if($data['is_favoritas']){
			$data['filtro_url'] = get_resolver_favoritas_url($id);
		}else{
			$data['filtro_url'] = get_resolver_questoes_url();
		}
		$data['tipo'] = $tipo;
		init_combos($data, TRUE, TRUE);

		if($this->input->get('buscar')) {
			
			adicionar_todos_filtros($filtro, $data, $this, FALSE);

			/*if($filtro['dis_ids']) {
				$data['assuntos'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplinas($filtro['dis_ids']));
			}*/

			//$_SESSION['filtro'] =  serialize($filtro);

		}/* elseif(isset($_SESSION['filtro'])) {

			$filtro = unserialize($_SESSION['filtro']);

			if($filtro['dis_ids']) {
				$data['assuntos'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplinas($filtro['dis_ids']));
			}
			if($filtro['favoritas'])
			{
				$data['filtro_favoritas'] = $filtro['favoritas'];
			}

			preencher_formulario_filtro($filtro, $data);
		}*/
		$data['busca'] =  $filtro['busca']?: NULL;
		$data['data_inicio'] = $filtro['data_inicio']?: NULL;
		$data['data_fim'] = $filtro['data_fim']?: NULL;
		$data['filtro_favoritas'] = $filtro['favoritas'] ?: null;
		$data['status'] = get_status_questao_combo();
		$data['categorias_cadernos'] = get_categoria_multiselect($this->caderno_model->listar_categorias(get_current_user_id()));
		
		$data['limite'] = isset($_SESSION['qpp']) ? $_SESSION['qpp'] : 10;
		if(!isset($_SESSION['mrq']) || $_SESSION['mrq'] == MODO_QUESTOES_COCKPIT){
			$data['limite'] = 1;
		}

		$data['filtros_selecionados'] = get_filtros_selecionados($filtro);
		$data['has_filtro'] = count($data['filtros_selecionados']) > 0 && !nenhum_filtro_aplicado($filtro);
		
//		if(nenhum_filtro_aplicado($filtro)) {
//			$total = get_transient(TRANSIENT_NUM_QUESTOES);
//
//			if(!$total) {
//				$total = $this->questao_model->get_total_ativas_com_respostas();
//				set_transient(TRANSIENT_NUM_QUESTOES, $total, DAY_IN_SECONDS);
//			}
//		}
//		else {
//			$total = $this->questao_model->get_total_resultado( $filtro, $has_filtro_cache );
//		}

        $cacheKeyCount = implode(":", [ "expo", "sq" , "count", serialize($filtro), (int)$incluir_inativas ]);
        $cacheValueCount = get_memcached($cacheKeyCount);

        if(!$cacheValueCount) {
            $cacheValueCount = $this->questao_model->get_total_resultado( $filtro, $incluir_inativas );
            set_memcached($cacheKeyCount, $cacheValueCount, time() + (3600));
        }

        $total = $cacheValueCount;

		$config = pagination_config($url, $total, $data['limite'], $segmento);
		$config['reuse_query_string'] = TRUE;
        $this->pagination->initialize($config);

        $page = ($this->uri->segment($segmento)) ? $this->uri->segment($segmento) : 0;

        $data["offset"] = $page;
		$data["links"] = $this->pagination->create_links();
		$data["questoes_por_pagina_combo"] = get_questoes_por_pagina_array();
		$data['filtro'] = $filtro;

		if($total > 0) {
			$data['total'] = $total;
			$filtro['order_num_comentarios'] = TRUE;

//			$data['questoes'] = $this->questao_model->listar_com_opcoes_e_assuntos($filtro, $data['limite'], $page, $has_filtro_cache);

            $cacheKeyQuestoes = implode(":", [ "expo", "sq" , "questoes", serialize($filtro), $data['limite'] , $page, (int)$incluir_inativas ]);
            $cacheValueQuestoes = get_memcached($cacheKeyQuestoes);

            if(!$cacheValueQuestoes) {
                $cacheValueQuestoes = $this->questao_model->listar_com_opcoes_e_assuntos($filtro, $data['limite'], $page, $incluir_inativas);
                set_memcached($cacheKeyQuestoes, $cacheValueQuestoes, time() + (3600));
            }

            $data['questoes'] = $cacheValueQuestoes;


			foreach ($data['questoes'] as &$questao) {
				$questao['total_comentarios'] = $this->comentario_model->contar_comentarios($questao['que_id']);
			}
		} else {
			$data['erro'] ='Não foram encontradas questões com este filtro.';
		}

		$data['is_filtro_bloqueado'] = is_area_desativada(array(PAINEL_SQ_FILTRO_QUESTOES));

		$query_string = http_build_query($_GET);
		$query_string = str_replace("zf=1", "", $query_string);
		if($query_string){
			$query_string = "?" . $query_string;
		}
		$data['query_string'] = $query_string;
		
		if(!isset($_SESSION['mrq']) || $_SESSION['mrq'] == MODO_QUESTOES_COCKPIT)
		{
			$data['is_usa_cockpit'] = true;
			$data['questao'] = $data['questoes'][0];
			$data['cockpit_questao_id'] =$data['questao']['que_id'];
			$data['anotacoes'] = $this->questao_model->listar_questoes_anotacoes($data['questao']['que_id'], get_current_user_id());
			$data['links'] = $this->load->view('questoes/rodape/paginacao_cockpit', 
				[
					'url' => $url,
					'pagina_atual' => $page,
					'total' => $total,
					'query_string' => $query_string,
					'uri' => $uri
				], TRUE
			);
		}

		if($tipo == TIPO_QUESTAO) {
		    $data['title'] = get_questao_titulo($data['questoes'][0]);
		}


		$data['url'] = $url;

	    $this->load->view(get_main_header_view_url(), $data);
	    $this->load->view(get_resolver_questoes_view_url(), $data);
	    $this->load->view(get_main_footer_view_url(), $data);
	}

	public function resolver_favoritas($disciplina_id)
	{
		self::resolver_questoes(FAVORITAS, $disciplina_id);
	}

	private function get_pagination_config($base_url, $total_rows, $per_page, $url_segment = 3)
	{
		$config = array();
		$config["base_url"] = $base_url;
		$config["total_rows"] = $total_rows;
		$config["per_page"] = $per_page;
		$config['uri_segment'] = $url_segment;
		$config['num_links'] = 3;
		$config['full_tag_open'] = '<div class="btn-group btn-pagination">';
		$config['full_tag_close'] = '</div>';
		$config['first_link'] = 'Primeiro';
		$config['first_tag_open'] = '<div class="btn btn-white">';
		$config['first_tag_close'] = '</div>';
		$config['last_link'] = 'Último';
		$config['last_tag_open'] = '<div class="btn btn-white">';
		$config['last_tag_close'] = '</div>';
		$config['next_link'] = '<i class="fa fa-chevron-right"></i>';
		$config['next_tag_open'] = '<div class="btn btn-white">';
		$config['next_tag_close'] = '</div>';
		$config['prev_link'] = '<i class="fa fa-chevron-left"></i>';
		$config['prev_tag_open'] = '<div class="btn btn-white">';
		$config['prev_tag_close'] = '</div>';
		$config['num_tag_open'] = '<div class="btn btn-white">';
		$config['num_tag_close'] = '</div>';
		$config['cur_tag_open'] = '<div class="btn btn-white  active">';
		$config['cur_tag_close'] = '</div>';

		return $config;
	}

	public function imprimir_questoes_html() {
// 		tem_acesso(array(
// 			ADMINISTRADOR, PROFESSOR, CONSULTOR, ASSINANTE_SQ, REVISOR
// 		), ACESSO_NEGADO_SQ);


		$filtro = array();
		$limit = LIMIT_IMPRIMIR_QUESTOES;
		/*if(isset($_SESSION['filtro'])) {
			$string_filtro = $_SESSION['filtro'];
			$filtro = unserialize($string_filtro);
		} else {
			$filtro['filtro_incluir'] = TODAS;
		}*/

		$filtro = array();
		adicionar_todos_filtros( $filtro, $data, $this, FALSE );

		$questoes = $this->questao_model->listar_por_filtros( $filtro, $limit, null, true );

		if(count($questoes) > 0) {
			$data['nome'] = "";
			$data['gabarito'] = get_gabarito($questoes, NUM_COLUNAS_GABARITO);
			$data['conteudo'] = imprimir_questoes_html($questoes);
			$this->load->view('main/imprimir_questoes_html', $data);

		} else {
			$data['erro'] ='Não foram encontradas questões com este filtro.';
		}

	}

	public function ajax_notificar_erro_questao() {
		$notificacao['que_id'] = $this->input->post("id");
		$notificacao['qet_id'] = $this->input->post("qet_id");
		$notificacao['qer_justificativa'] = $this->input->post("qer_justificativa");
		$notificacao['usu_id'] = get_id_usuario_atual();

		if ($this->input->post("qet_id") == OPCAO_ZERO) {
			echo "É necessário escolher uma notificação.";
		} else {
			$this->questao_model->salvar_notificacao($notificacao);
			echo "Notificação enviada com sucesso!";
		}
	}

	public function get_ajax_main_form_notificacao_url() {
		$questao = $this->questao_model->get_by_id($this->input->post("id"));
		$tipos_erro = get_tipo_erro_combo($this->questao_model->listar_tipos_erros_questao());

		$mensagem = get_botao_resposta("Notificação enviada com sucesso. Muito obrigado.", "resposta-enviada.png", "resposta-enviada");

		echo
		"<div id='msg-{$questao['que_id']}' style='margin: 20px; color: red;'></div>"
		.form_dropdown('qet_id', $tipos_erro, "", 'id="notificacao-'.$questao['que_id'].'" class="form-control m-b"').
		"<div class='form-group'>
			<label class='col-sm-2 control-label'>Justificativa (opcional)</label>
			<div class='col-sm-10'>
				<textarea name='qer_justificativa' id='justificativa-{$questao['que_id']}'></textarea>
			</div>
		</div>
		<div>
			<a href='#' data-id='{$questao['que_id']}' class='btn btn-primary notificar'><i class='fa fa-check'></i> Enviar</a>
		</div>
		<script>
		$('.notificar').click(function (event) {
					event.preventDefault();

					var id = $(this).data('id');
					var qet_id = $('#notificacao-' + id).val();
					var qer_justificativa = $('#justificativa-' + id).val();

					if(!qet_id) {
						$('#msg-'+id).html('O tipo da mensagem precisa ser especificado');
					}
					else if(!qer_justificativa) {
						$('#msg-'+id).html('A justificativa precisa ser preenchida');
					}
					else {
						$( '#notificar-' + id + ' .panel-body' ).html(\"" . $mensagem ."\");
						$.ajax({
							url: '" . get_ajax_main_notificar_erro_questao_url() . "',
							method: 'POST',
							cache: false,
							data: { id: id, qet_id : qet_id, qer_justificativa : qer_justificativa }
						});
					}
				});
		</script>";
	}

	public function get_ajax_main_comentarios_questao_url($simulado_id = null) {

		$questao_id = $this->input->post("id");

		$comentarios_alunos = $this->comentario_model->listar_por_questao($questao_id, false);
		$comentarios_professores = $this->comentario_model->listar_por_questao($questao_id, true);
		$comentarios_avaliacoes = $this->comentario_model->listar_comentarios_avaliacoes($questao_id, get_current_user_id());

		$data['simulado_id'] = $simulado_id;
		$data['professor_pode_comentar'] = AcessoGrupoHelper::professor_pode_comentar($questao_id);

		$data['questao_id'] = $questao_id;
		$data['comentarios_alunos'] = $comentarios_alunos;
		$data['comentarios_professores'] = $comentarios_professores;
		$data['comentarios_avaliacoes'] = $comentarios_avaliacoes;

		$this->load->view('questoes/barra_inferior/comentarios', $data);
	}

	public function get_ajax_main_avaliar_comentario_url(){

		$comentario_id = $this->input->post("id");
		$usuario_id = $this->input->post("usuId");
		$avaliacao = $this->input->post("avaliacao");

		$media = $this->comentario_model->salvar_comentario_avaliacao($comentario_id, $usuario_id, $avaliacao);

		echo number_format($media, 2, ',', '')." Média Geral";

	}

	public function adicionar_comentario($comentario_id = null) {

		$comentario['com_texto'] = $this->input->post("texto");

		$is_edition = !is_null($comentario_id);

		$user_id = null;
		if($is_edition){
			$comentario_db = $this->comentario_model->get_by_id($comentario_id);
			$user_id = $comentario_db['user_id'];
		}else{
			$user_id = $this->input->post("usuId");
		}

		$current_user_id = get_current_user_id();

		if(!$current_user_id){
			echo "erro:Pedimos desculpas, mas sua sessão encerrou. É necessário fazer novo login para comentar uma questão. <a data-toggle='modal' data-target='#login-modal' href='#'>Clique aqui!</a>";
			return;
		}

		if($user_id != $current_user_id && !tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))){
			echo "erro:Pedimos desculpas, mas sua sessão encerrou. É necessário fazer novo login para comentar uma questão. <a data-toggle='modal' data-target='#login-modal' href='#'>Clique aqui!</a>";
			return;
		}

		if( !(strlen(strip_tags($comentario['com_texto'])) >= (is_professor($user_id) ? 50 : 1))){
			echo("erro:".(is_professor($user_id) ? "O comentário deve ter, no mínimo, 50 caracteres!" : "É necessário digitar um comentário para enviar!"));
			return;
		}

		if($is_edition) {

			$comentario['com_data_edicao'] = date('Y-m-d h:i:s');
			$comentario['com_id'] = $comentario_id;
			$comentario['que_id'] = $this->input->post("id");
			$comentario['user_id'] = $user_id;

			$this->comentario_model->atualizar($comentario);
			$this->questao_model->atualizar_destaque_questao($comentario['que_id']);

			/**
			 * Removido provisoriamente por pedido do Lucas em 12/01/19 por conta de limite excedido de envio de e-mails
			 */

			// self::notificar_professor_sobre_comentario($comentario_id, TRUE);

			echo get_botao_resposta("Comentário atualizado com sucesso!", "resposta-comentario.png", "resposta-comentario");

		} else {

			$comentario['que_id'] = $this->input->post("id");
			$comentario['user_id'] = $this->input->post("usuId");
			$comentario['com_data_criacao'] = date('Y-m-d h:i:s');

			$comentario_id = $this->comentario_model->salvar($comentario);
			$comentario['com_id'] = $comentario_id;

			$total = $this->comentario_model->contar_comentarios($comentario['que_id']);

			$this->questao_model->atualizar_numero_comentarios($comentario['que_id'], $total);
			$this->questao_model->atualizar_destaque_questao($comentario['que_id']);


			/**
			 * Removido provisoriamente por pedido do Lucas em 12/01/19 por conta de limite excedido de envio de e-mails
			 */

			// self::notificar_professor_sobre_comentario($comentario_id);

			//Quem comenta uma questão passa a acompanhá-la automaticamente
			if(!$this->questao_model->is_favorita($comentario['que_id'], $comentario['user_id'], QUESTAO_ACOMPANHADA)){
				$this->questao_model->marcar_desmarcar_favorita($comentario['que_id'], $comentario['user_id'], QUESTAO_ACOMPANHADA);
			}

			//Notifica usuários que acompanham a questão
			self::notificar_novo_comentario($comentario);

			echo get_botao_resposta("Comentário salvo com sucesso!", "resposta-comentario.png", "resposta-comentario");
		}

		//Invalida o cache de questões após cada alteração em questões
		memcached_remover_cache_questoes();

	}

	public function get_ajax_main_estatisticas_questao() {

		$this->load->helper('grafico');

		$questao_id = $this->input->post("id");

		$questao = $this->questao_model->get_by_id($questao_id);

		$acertos = $this->questao_model->contar_questao_resposta($questao_id, true, null, null, TRUE);
		$erros = $this->questao_model->contar_questao_resposta($questao_id, false, null, null, TRUE);

		$alternativas = $this->questao_model->contar_questao_resposta_alternativas($questao_id, null, null, TRUE);

		if($acertos > 0 || $erros > 0){

			$que_respostas = grafico_questao_resposta($acertos, $erros);
			$que_alternativas = grafico_questao_alternativas($alternativas, $questao['que_tipo']);

			$data['questao_id'] = $questao_id;
			$data['que_respostas'] = $que_respostas;
			$data['total_respostas'] = $acertos + $erros;
			$data['que_alternativas'] = $que_alternativas;

			$this->load->view("main/estatisticas_questao", $data);

		}else{
			echo "Essa questão ainda não possui respostas.";
		}
	}

	public function get_ajax_main_questao_dificuldade($questao_id){
		echo get_questao_dificuldade_url($questao_id);
	}

	public function get_ajax_main_questao_ver_dificuldade($questao_id){
		echo get_questao_ver_dificuldade_url($questao_id);
	}

	private function notificar_professor_sobre_comentario($comentario_id, $edicao = FALSE)
	{

		return TRUE;

		$comentario = $this->comentario_model->get_by_id($comentario_id);
		$destaque = $this->comentario_model->get_destaque($comentario['que_id']);
		$questao = $this->questao_model->get_by_id($comentario['que_id'], TRUE);
		$aluno = get_usuario_array($comentario['user_id']);
		$adm_email = new stdClass;
		$adm_email->user_email = 'leonardo.coelho@exponencialconcursos.com.br';
		$email_prof = new stdClass;
		$usuario = get_usuario_array($destaque['user_id']);

		// verifica se o professor é diferente do adm
		if($usuario['email'] != $adm_email->user_email)
		{
			$email_prof->user_email = $usuario['email'];
		}

		$titulo = "Questão comentada por aluno";
		$titulo .= $edicao ? " (Edição)" : "";

		$mensagem = get_template_email ('questao_comentada.php', array (
			'questao_id' => $comentario['que_id'],
			'disciplina' => $questao['dis_nome'],
			'nome_aluno' => $aluno['nome_completo'],
			'email' => $aluno['email'],
			'url' => get_questao_seo_url($questao)
		) );

		$coodenadores_sq = listar_coordenadores_sq_ativos();
		array_unshift($coodenadores_sq, $adm_email);
		$email_prof->user_email ? array_unshift($coodenadores_sq, $email_prof) : '';


		echo $coodenadores_sq->user_email;
		foreach ($coodenadores_sq as $coodenador_sq)
		{
			//enviar_email($coodenador_sq->user_email, $titulo, $mensagem, NULL, NULL, NULL, false);
		}

	}

	public function excluir_comentario($comentario_id) {

		$comentario = $this->comentario_model->get_by_id($comentario_id);
		$questao_id = $comentario['que_id'];

		$this->comentario_model->excluir($comentario_id);

		$total = $this->comentario_model->contar_comentarios($questao_id);

		$this->questao_model->atualizar_numero_comentarios($questao_id, $total);

		//Invalida o cache de questões após cada alteração em questões
		memcached_remover_cache_questoes();

		echo "Comentário excluido com sucesso!";
	}

	function get_ajax_main_alunos_acompanhamento($pagina = PAGE_MEU_DESEMPENHO){
		$filtro_aluno = $this->input->post('filtro_aluno');

		if($pagina == PAGE_MEU_DESEMPENHO){
			$alunos = get_ajax_main_alunos_acompanhamento($filtro_aluno);

			echo form_dropdown('aluno_id', $alunos, $aluno_selecionado ? $aluno_selecionado: null, "id='aluno_id' class='form-control m-b'");
		}elseif($pagina == PAGE_SIMULADOS){
			$alunos = get_ajax_main_alunos_acompanhamento($filtro_aluno, FALSE);
			echo form_multiselect("usu_ids[]", $alunos, $aluno_selecionado ? $aluno_selecionado: null, "id='usu_ids' class='multiselect'");
		}
    }

	public function main_resultado_detalhado_url()
	{
		$simulado_id = $this->input->post("id");
		// $resultados = $this->simulado_model->get_resultados_detalhados($simulado_id, get_current_user_id());

		$data['resultados'] = $this->simulado_model->listar_respostas($simulado_id, get_current_user_id());

		$data['simulado'] = $this->simulado_model->get_by_id($simulado_id);
		$filtro['sim_ids'] = array($simulado_id);

		$data['questoes'] = $this->questao_model->listar_por_simulado($simulado_id);
		$data['total_questoes'] = $this->simulado_model->get_total_questoes($simulado_id);
		$data['simulado_id'] = $simulado_id;
		$data['opcoes_label'] = array(1 => 'a)', 2 => 'b)', 3 => 'c)', 4=> 'd)', 5 => 'e)');

		echo $this->load->view('modal/resultado_simulado_detalhado', $data, TRUE);
	}

	public function imprimir_meus_simulados_html($simulado_id, $tipo_resultado = SIMULADO_QUESTAO_TODAS) {
		$has_simulado = tem_acesso_ao_simulado($simulado_id, get_current_user_id());
		if(!$has_simulado) redirecionar_para_acesso_negado();

		$simulado = $this->simulado_model->get_by_id($simulado_id);

		$cargo = $this->cargo_model->get_by_id($simulado['car_id']);

		// $questoes = $this->questao_model->listar_por_simulado($simulado_id, LIMIT_LISTAR_QUESTOES);
		$questoes = $this->questao_model->listar_por_simulado($simulado_id, LIMIT_LISTAR_QUESTOES, null, null, $tipo_resultado);
		$questoes_validas = array();

		if(count($questoes) > 0) {
			foreach ($questoes as &$questao) {
				$questao['assuntos'] = $this->questao_model->get_assuntos_questao($questao['que_id']);
				if($questao['que_tipo'] == MULTIPLA_ESCOLHA) {
					$questao['opcoes'] = $this->questao_model->listar_opcoes($questao['que_id']);
					array_push($questoes_validas, $questao);
				} else {
					array_push($questoes_validas, $questao);
				}
			}
			$data['conteudo'] = imprimir_questoes_html($questoes_validas, $simulado, $cargo, null, false);
            $data['gabarito'] = get_gabarito($questoes_validas, NUM_COLUNAS_GABARITO);
            $data['comentarios'] = imprimir_comentarios($questoes_validas);
			$this->load->view('main/imprimir_questoes_html', $data);
		} else {
			$data['erro'] ='Não foram encontradas questões com este filtro.';
		}
	}


	public function imprimir_resultado_simulado($simulado_id, $tipo_resultado = SIMULADO_QUESTAO_TODAS) {
		tem_acesso(array(
				ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, CONSULTOR, ASSINANTE_SQ, REVISOR
		), ACESSO_NEGADO_SQ);

		$simulado = $this->simulado_model->get_by_id($simulado_id);
		$cargo = $this->cargo_model->get_by_id($simulado['car_id']);

		$questoes = $this->questao_model->listar_por_simulado($simulado_id, null, null, null, $tipo_resultado);
		$questoes_validas = array();

		if(count($questoes) > 0) {
			foreach ($questoes as &$questao) {
				$questao['assuntos'] = $this->questao_model->get_assuntos_questao($questao['que_id']);
				if($questao['que_tipo'] == MULTIPLA_ESCOLHA) {
					$questao['opcoes'] = $this->questao_model->listar_opcoes($questao['que_id']);
					array_push($questoes_validas, $questao);
				} else {
					array_push($questoes_validas, $questao);
				}
			}

			$disciplinas = $this->simulado_model->listar_disciplinas_simulado($simulado_id, 'sd.sid_ordem', 'asc');
			$respostas = $this->simulado_model->listar_respostas($simulado_id, get_current_user_id());

// 			$data['detalhes'] = get_resultado_simulado_array($disciplinas, $data['resultados']);
			$data['conteudo'] = imprimir_resultado_simulado($questoes_validas, $simulado, $respostas);
			$this->load->view('main/imprimir_questoes_html', $data);
		} else {
			$data['erro'] ='Não foram encontradas questões com este filtro.';
		}
	}

	public function imprimir_meus_cadernos_html($caderno_id) {
		// tem_acesso(array(
		// 		ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, CONSULTOR, ASSINANTE_SQ, REVISOR
		// ), ACESSO_NEGADO_SQ);

		$this->load->helper('caderno');

		// se não for dono do caderno redireciona
		if(!is_dono_do_caderno($caderno_id) && !tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) {
			$redirecionar = ACESSO_NEGADO_SQ;
			header("Location: $redirecionar");
			exit;
		}

		// se caderno for comprado não precisa ser assinante
		if(!is_dono_do_caderno($caderno_id) && !is_caderno_comprado($caderno_id) && !tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) {
			tem_acesso(array(
					ADMINISTRADOR, PROFESSOR, CONSULTOR, ASSINANTE_SQ, REVISOR
			), ACESSO_NEGADO_SQ);
		}

		$caderno = $this->caderno_model->get_caderno($caderno_id);
		
		$filtro = array();
		
		$filtro['cad_id'] = $caderno_id;
		
		if($_GET['ass_ids'] && !is_array($_GET['ass_ids'])){
			$_GET['ass_ids'] = explode(',', $_GET['ass_ids']);
		}

		adicionar_todos_filtros( $filtro, $data, $this, FALSE );

		if($filtro['dis_ids']) {
			if(!is_array($filtro['dis_ids'])){
				$filtro['dis_ids'] = explode(',', $filtro['dis_ids']);
			}
		}

		$questoes = $this->questao_model->listar_por_filtros( $filtro, LIMIT_LISTAR_QUESTOES, null, true );

		$questoes_validas = array();

		if(count($questoes) > 0) {
			foreach ($questoes as &$questao) {
				$questao['assuntos'] = $this->questao_model->get_assuntos_questao($questao['que_id']);
				if($questao['que_tipo'] == MULTIPLA_ESCOLHA) {
					$questao['opcoes'] = $this->questao_model->listar_opcoes($questao['que_id']);
					array_push($questoes_validas, $questao);
				} else {
					array_push($questoes_validas, $questao);
				}
			}

			$data['nome'] = $caderno['cad_nome'];
			$data['gabarito'] = get_gabarito($questoes, NUM_COLUNAS_GABARITO);
			$data['conteudo'] = imprimir_questoes_html($questoes_validas, null, null, $caderno);
			$data['comentarios'] = imprimir_comentarios($questoes);
			$this->load->view('main/imprimir_questoes_html', $data);

		} else {
			$data['erro'] ='Não foram encontradas questões com este filtro.';
		}
	}

	/**
	 * TODO: Ver duplicidade com Xhr->simulado_nota($simulado_id)
	 */
	public function salvar_resultado($simulado_id) {

		$disciplinas_acertos = [];

		foreach ($this->input->post() as $key => $input) {
			if(!strcmp($key, 'enviar') == 0) {
				$post_name = explode("_", $key);

				if($post_name[1] == 'acertos'){
					$disciplinas_acertos[$post_name[2]] = $input;
				}elseif($post_name[1] == 'erros'){
					$disciplinas_erros[$post_name[2]] = $input;
				}
			}
		}

		if(isset($disciplinas_erros)){
			echo $this->simulado_model->salvar_nota(FALSE, get_current_user_id(), $simulado_id, $disciplinas_acertos, $disciplinas_erros);
		}else{
			echo $this->simulado_model->salvar_nota(FALSE, get_current_user_id(), $simulado_id, $disciplinas_acertos);
		}

		$simulado_usuario = $this->simulado_model->get_usuario_simulado($simulado_id, get_current_user_id());

		$this->simulado_model->finalizar_simulado($simulado_usuario['siu_id'], null, false);

		redirect ( get_meus_simulados_url(). '/' );
	}

	public function salvar_resultado_detalhado($simulado_id) {

		foreach ($this->input->post('opcao_questao') as $questao_id => $selecao) {
			$resposta = $this->questao_model->get_resposta($questao_id);

			if($selecao) {
				$resultado = ($selecao == $resposta) ? CERTO : ERRADO;
				$this->questao_model->salvar_resposta_usuario(get_current_user_id(), $questao_id, $selecao, $simulado_id);
			}
		}

		$this->simulado_model->salvar_nota(FALSE, get_current_user_id(), $simulado_id);

		$simulado_usuario = $this->simulado_model->get_usuario_simulado($simulado_id, get_current_user_id());
		$this->simulado_model->finalizar_simulado($simulado_usuario['siu_id'], 0);

		redirect(get_meus_simulados_url(). '/');
	}

	function ajax_get_assuntos($disciplinas) {
		$disciplinas_ids = explode('-', $disciplinas);

		/** [JOULE][J2] UPGRADE - Definir ordem de assuntos da Taxonomia
		 * 	https://podio.com/profinfnetedubr/lslc/apps/upgrades/items/334
		 *
		 *	A listagem de assuntos deve levar em consideração a prioridade
		 */
		$assuntos = $this->assunto_model->listar_por_disciplinas($disciplinas_ids, true, 'ass_prioridade asc, ass_nome asc');
		$assuntos = to_select2_format( get_assunto_multiselect( $assuntos, TRUE ) );

		echo json_encode($assuntos);
	}

	/**
	 * Retorna o total de pontos e posição de um usuário para um ranking ligado a um simulado ou um simulado sem ranking
	 *
	 * @see Main::ranking()
	 *
	 * @since L1
	 * 
	 * @param int $simulado_id ID do simulado que possui ou não um ranking
	 * @param int $user_id ID do usuário a verificar a pontuação e posição para o ranking
	 * 
	 * @return array Array contendo o 'total_ranking', 'posicao_ranking', 'resultado_ranking' e 'total_{$simulado_id}'
	 */
	public function get_ranking_por_usuario($simulado_id, $user_id){

		$this->load->model("ranking_model");

		$r = $this->ranking_model->get_ranking_por_simulado($simulado_id);

		if($r) {
			$ranking = $this->simulado_model->get_ranking_by_id($r['ran_id'], NULL, LIMIT_RANKING, FALSE, TRUE);
		}
		else {
			$ranking = $this->simulado_model->get_ranking($simulado_id, NULL, LIMIT_RANKING, FALSE, TRUE);
		}

		$result = array();
		$posicao_ranking = 0;
		$total_pontos_possivel = NULL;
		$is_media_ponderada = FALSE;

		foreach($ranking as &$item) {

			// recupera a entidade ranking, se presente
			$def_ranking = $this->ranking_model->get_ranking_por_simulado($simulado_id);
			$def_ranking_id = isset($def_ranking['ran_id']) ? $def_ranking['ran_id'] : null;

			if($def_ranking_id) {

				$result['ran_peso_erros'] = $def_ranking['ran_peso_erros'];
				$result['ran_id'] = $def_ranking_id;

				$soma_ranking = 0;
				// lista as provas (entidades simulados) envolvidos no ranking
				$def_simulados = $this->ranking_model->listar_simulados($def_ranking_id);

				foreach($def_simulados as $def_simulado) {

					$soma_simulado = 0;

					// lista grupos das provas
					$def_grupos = $this->ranking_model->listar_grupos_de_simulado($def_simulado['ran_id'], $def_simulado['sim_id']);

					foreach ($def_grupos as $def_grupo) {

						$def_disciplinas = $this->ranking_model->listar_disciplinas_em_grupo($def_grupo['ran_id'], $def_grupo['rag_id']);

						$resultado_grupo = $this->simulado_model->get_resultado_por_grupo($item['usu_id'], $def_simulado['sim_id'], $def_grupo['rag_id']);

						$soma_ranking += $resultado_grupo['sre_resultado'];
						$soma_simulado += $resultado_grupo['sre_resultado'];

					}

					// lista disciplinas sem grupos
					$def_disciplinas = $this->ranking_model->listar_disciplinas_sem_grupo_de_simulado($def_ranking_id, $def_simulado['sim_id']);

					foreach($def_disciplinas as $def_disciplina) {
						$resultado = $this->simulado_model->get_resultado_disciplina($def_simulado['sim_id'], $def_disciplina['dis_id'], $item['usu_id']);

						$soma_ranking += $resultado['sre_resultado'];
						$soma_simulado += $resultado['sre_resultado'];

					}

					$result['total_'.$def_simulado['sim_id']] = $soma_simulado;

				}


			}

			// simulado sem ranking
			else {

				$disciplinas = $this->simulado_model->listar_disciplinas_por_ordem($simulado_id);

				$soma_ranking = 0;

				foreach ($disciplinas as $disciplina) {

					$resultado = $this->simulado_model->get_resultado_disciplina($simulado_id, $disciplina['dis_id'], $item['usu_id']);

					$soma_ranking += $resultado['sre_resultado'];

				}
			}

			//Resgata os valores somente uma vez
			if($total_pontos_possivel === NULL)
			{
				$total_pontos_possivel = 0;
				if($def_ranking_id)
				{
					$is_media_ponderada = $def_ranking['ran_media_ponderada'];
					if($is_media_ponderada)
					{
						$total_pontos_possivel = $this->ranking_model->get_maximo_pontos($def_ranking_id);
					}
				}
				else
				{
					$simulado = $this->simulado_model->get_by_id($simulado_id);
					$is_media_ponderada = $simulado['sim_media_ponderada'];
					if($is_media_ponderada)
					{
						$total_pontos_possivel = $this->simulado_model->get_total_questoes($simulado_id);
					}
				}

			}

			//$result['total_ranking'] = $soma_ranking;
			$result['total_ranking'] = $is_media_ponderada ? round( ( $soma_ranking / $total_pontos_possivel) * 10, 2 ) : $soma_ranking;

			$result['posicao_ranking'] = ++$posicao_ranking;

			if($item['usu_id'] == $user_id){
				break;
			}

		}

		if($r) {
			$result['resultado_ranking'] = $this->simulado_model->is_aprovado_por_ranking($user_id, $r['ran_id'], $result['total_ranking'], $r['ran_corte']) ? "Aprovado" : "Reprovado";
		}else{
			$result['resultado_ranking'] = $this->simulado_model->is_aprovado($user_id, $simulado_id) ? "Aprovado" : "Reprovado";
		}

		return $result;

	}

	/**
	 * @see Main::get_ranking_por_usuario()
	 */
	function ranking($simulado_id, $somente_aprovados = 0, $offset = 0, $exportar = 0)
	{
 		tem_acesso(array(
 				/*ADMINISTRADOR, PROFESSOR, CONSULTOR, ASSINANTE_SQ, REVISOR*/USUARIO_LOGADO
 		), ACESSO_NEGADO_SQ);

		$this->load->model('ranking_model');
		$this->load->helper('simulado');
		$this->load->helper('ranking');

		if(is_null($offset)){
			$offset = 0;
		}

		$data['include_floatthead'] = true;

		$data['somente_aprovados'] = $somente_aprovados;
		$data['ranking_usuario'] = $this->simulado_model->get_posicao_ranking($simulado_id, get_current_user_id(), $somente_aprovados);

		$has_simulado = tem_acesso_ao_simulado($simulado_id, get_current_user_id());
		if(!$has_simulado) redirecionar_para_acesso_negado();

		$r = $this->ranking_model->get_ranking_por_simulado($simulado_id);

		if($r) {
			$ranking = $this->simulado_model->get_ranking_by_id($r['ran_id'], $offset, LIMIT_RANKING, $somente_aprovados, $exportar);
		}
		else {
			$ranking = $this->simulado_model->get_ranking($simulado_id, $offset, LIMIT_RANKING, $somente_aprovados, $exportar);
		}

		$total_pontos_possivel = NULL;
		$is_media_ponderada = FALSE;

		foreach($ranking as &$item) {
			$colunas = [];

			// recupera a entidade ranking, se presente
			$def_ranking = $this->ranking_model->get_ranking_por_simulado($simulado_id);
			$def_ranking_id = isset($def_ranking['ran_id']) ? $def_ranking['ran_id'] : null;

			if($def_ranking_id) {

				$simulados_disciplinas = array();

				$soma_ranking = 0;
				// lista as provas (entidades simulados) envolvidos no ranking
				$def_simulados = $this->ranking_model->listar_simulados($def_ranking_id);

				foreach($def_simulados as $def_simulado) {

					// echo "Sim id: " .$def_simulado['sim_id'] . "<br>";

					$soma_simulado = 0;

					// lista grupos das provas
					// $def_grupos = $this->ranking_model->listar_grupos($def_simulado['ran_id']);
					$def_grupos = $this->ranking_model->listar_grupos_de_simulado($def_simulado['ran_id'], $def_simulado['sim_id']);

					foreach ($def_grupos as $def_grupo) {

						$def_disciplinas = $this->ranking_model->listar_disciplinas_em_grupo($def_grupo['ran_id'], $def_grupo['rag_id']);

						// lista disciplinas dos grupos
						foreach($def_disciplinas as $def_disciplina) {

							$resultado = $this->simulado_model->get_resultado_disciplina($def_simulado['sim_id'], $def_disciplina['dis_id'], $item['usu_id']);

							array_push($colunas, [
								'nome' => $def_disciplina['dis_nome'],
								'valor' => $resultado['sre_resultado'],
								'destaque' => false
							]);

							array_push($simulados_disciplinas, [
								'dis_id' => $def_disciplina['dis_id'],
								'sim_id' => $def_simulado['sim_id']
							]);
						}

						$resultado_grupo = $this->simulado_model->get_resultado_por_grupo($item['usu_id'], $def_simulado['sim_id'], $def_grupo['rag_id']);

						$soma_ranking += $resultado_grupo['sre_resultado'];
						$soma_simulado += $resultado_grupo['sre_resultado'];

						array_push($colunas, [
							'nome' => $def_grupo['rag_nome'],
							'valor' => $resultado_grupo['sre_resultado'],
							'destaque' => true
						]);

					}

					// lista disciplinas sem grupos
					// $def_disciplinas = $this->ranking_model->listar_disciplinas_sem_grupo($def_grupo['ran_id']);
					$def_disciplinas = $this->ranking_model->listar_disciplinas_sem_grupo_de_simulado($def_ranking_id, $def_simulado['sim_id']);

					foreach($def_disciplinas as $def_disciplina) {
						$resultado = $this->simulado_model->get_resultado_disciplina($def_simulado['sim_id'], $def_disciplina['dis_id'], $item['usu_id']);

						$soma_ranking += $resultado['sre_resultado'];
						$soma_simulado += $resultado['sre_resultado'];

						array_push($colunas, [
							'nome' => $def_disciplina['dis_nome'],
							'valor' => $resultado['sre_resultado'],
							'destaque' => false
						]);

						array_push($simulados_disciplinas, [
							'dis_id' => $def_disciplina['dis_id'],
							'sim_id' => $def_simulado['sim_id']
						]);
					}

					if(count($def_simulados) > 1) {
						array_push($colunas, [
							'nome' => $def_simulado['sim_nome'],
							'valor' => $soma_simulado,
							'destaque' => true
						]);
					}

				}


			}

			// simulado sem ranking
			else {

				$disciplinas = $this->simulado_model->listar_disciplinas_por_ordem($simulado_id);

				$soma_ranking = 0;

				foreach ($disciplinas as $disciplina) {

					$resultado = $this->simulado_model->get_resultado_disciplina($simulado_id, $disciplina['dis_id'], $item['usu_id']);
					$e_disciplina = $this->disciplina_model->get_by_id($disciplina['dis_id']);

					$soma_ranking += $resultado['sre_resultado'];

					array_push($colunas, [
						'nome' => $e_disciplina['dis_nome'],
						'valor' => $resultado['sre_resultado'],
					]);

				}
			}

			//Resgata os valores somente uma vez
			if($total_pontos_possivel === NULL)
			{
				$total_pontos_possivel = 0;
				if($def_ranking_id)
				{
					$is_media_ponderada = $def_ranking['ran_media_ponderada'];
					if($is_media_ponderada)
					{
						$total_pontos_possivel = $this->ranking_model->get_maximo_pontos($def_ranking_id);
					}
				}
				else
				{
					$simulado = $this->simulado_model->get_by_id($simulado_id);
					$is_media_ponderada = $simulado['sim_media_ponderada'];
					if($is_media_ponderada)
					{
						$total_pontos_possivel = $this->simulado_model->get_total_questoes($simulado_id);
					}
				}

			}

			array_push($colunas, [
				'nome' => 'Nota Final',
				'valor' => $is_media_ponderada ? round( ( $soma_ranking / $total_pontos_possivel) * 10, 2 ) : $soma_ranking,
				'destaque' => true
			]);

			$item['total_ranking'] = $soma_ranking;

			// print_r($colunas);

			if($exportar){
				$item['colunas_exportar'] = $colunas;
				$item['colunas'] = array_slice($colunas, $offset, LIMIT_RANKING);
			}else{
				$item['colunas'] = $colunas;
			}

		}

		$segmento = 5;
		$url = get_ranking_simulado_url($simulado_id, $somente_aprovados);

		if($r) {
			$total = $this->simulado_model->count_ranking_by_id($r['ran_id'], $somente_aprovados);
		}
		else {
			$total = $this->simulado_model->count_ranking($simulado_id, $somente_aprovados);
		}

		$config = pagination_config($url, $total, LIMIT_RANKING, $segmento);
        $this->pagination->initialize($config);

		if($r) {
			$tab_disciplinas = $simulados_disciplinas;
		}else{
			$tab_disciplinas = $this->simulado_model->listar_disciplinas_por_ordem($simulado_id);
		}

		$data['media_disciplinas'] = [];

		foreach ($tab_disciplinas as $tab_disciplina) {
			if($r) {
				$aux_simulado_id = $tab_disciplina['sim_id'];
			}else{
				$aux_simulado_id = $simulado_id;
			}
			$media = $this->simulado_model->get_media_resultado_disciplina($aux_simulado_id, $tab_disciplina['dis_id']);
			$maximo = $this->simulado_model->get_pontuacao_maxima_disciplina($aux_simulado_id, $tab_disciplina['dis_id']);

			$disciplina = $this->disciplina_model->get_by_id($tab_disciplina['dis_id']);

		 	$percentual = porcentagem($media / $maximo * 100);

			$minha_nota = null;
			$meu_percentual = null;
			$estilo = null;

			if($data['ranking_usuario']) {
				$meu_resultado = $this->simulado_model->get_resultado_disciplina($aux_simulado_id, $tab_disciplina['dis_id'], get_current_user_id());
		 		$minha_nota = $meu_resultado['sre_resultado'];
				$meu_percentual = porcentagem($minha_nota / $maximo * 100);

		 		if($minha_nota >= ($media * 1.2)) {
		 			$estilo = "ranking-acima-media";
		 		}
		 		elseif($minha_nota <= ($media * 0.8)) {
		 			$estilo = "ranking-abaixo-media";
		 		}


		 	}

		 	array_push($data['media_disciplinas'], [
		 		'dis_nome' => $disciplina['dis_nome'],
		 		'media' => decimal($media),
		 		'percentual' => $percentual,
		 		'minha_nota' => decimal($minha_nota),
		 		'meu_percentual' => $meu_percentual,
		 		'estilo' => $estilo
		 	]);
		}

		// print_r($data['media_disciplinas']);

		$data['qtde_5'] = round($total * 0.05) > 0 ? round($total * 0.05) : 1;
		$data['qtde_10'] = round($total * 0.1) > 0 ? round($total * 0.1) : 1;;
		$data['qtde_20'] = round($total * 0.2) > 0 ? round($total * 0.2) : 1;;

		$nota_5 = $this->simulado_model->get_media_ranking($simulado_id, $data['qtde_5']);
		$nota_10 = $this->simulado_model->get_media_ranking($simulado_id, $data['qtde_10']);
		$nota_20 = $this->simulado_model->get_media_ranking($simulado_id, $data['qtde_20']);
		$maxima = $this->simulado_model->get_pontuacao_maxima($simulado_id);

		$data['nota_5'] = decimal($nota_5);
		$data['nota_10'] = decimal($nota_10);
		$data['nota_20'] = decimal($nota_20);

		$data['percentual_5'] = porcentagem($nota_5 / $maxima * 100);
		$data['percentual_10'] = porcentagem($nota_10 / $maxima * 100);
		$data['percentual_20'] = porcentagem($nota_20 / $maxima * 100);

		$data['percentual_a_5'] = $nota_5 / $maxima * 100;
		$data['percentual_a_10'] = $nota_10 / $maxima * 100;
		$data['percentual_a_20'] = $nota_20 / $maxima * 100;

        $data["links"] = $this->pagination->create_links();
        $data['offset'] = $offset;
		$data['ranking'] = $ranking;
		$data['simulado'] = $this->simulado_model->get_by_id($simulado_id);
		$data['qtde_questoes'] = $this->simulado_model->get_total_questoes($simulado_id);

		$data['total'] = $total;




		foreach ($data['ranking'] as &$item) {

			if($r) {
				$item['sim_usu_data_resolucao'] = $this->ranking_model->get_data_resolucao($r['ran_id'], $item['usu_id']);
				$item['aprovado'] = $this->simulado_model->is_aprovado_por_ranking($item['usu_id'], $r['ran_id'], $item['total_ranking'], $r['ran_corte']) ? "Aprovado" : "Reprovado";
			}else{
				$item['aprovado'] = $this->simulado_model->is_aprovado($item['usu_id'], $simulado_id) ? "Aprovado" : "Reprovado";
			}

		}



		// if($ranking_usuario && ($ranking_usuario['posicao'] > LIMIT_RANKING)) {
		// 	array_push($data['ranking'], $ranking_usuario);
		// }

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view(get_ranking_view_url(), $data);
		if($exportar){
			$this->load->view(get_ranking_excel_view_url(), $data);
		}
		$this->load->view(get_main_footer_view_url(), $data);
	}

	function adicionar_novo_caderno()
	{
		if($this->input->post()) {
			$data_caderno = array(
				'cad_nome' => $this->input->post('nome'),
				'cad_data_criacao' => get_data_hora_agora(),
				'usu_id' => get_current_user_id(),
				'cad_comercializavel' => FALSE,
				'cad_coaching' => FALSE
			);

			if(tem_acesso([PROFESSOR]) && $this->input->post('comercializavel')){
				$data_caderno['cad_comercializavel'] = $this->input->post('comercializavel');
			}

			if(tem_acesso([PARCEIRO_COACHING]) && $this->input->post('coaching')){
				$data_caderno['cad_coaching'] = $this->input->post('coaching');
			}

			$caderno_id = $this->caderno_model->adicionar_caderno($data_caderno);

			$filtro_caderno = base64_decode($this->input->post("filtro_caderno"));

			$this->caderno_model->atualizar_filtros($caderno_id, $filtro_caderno);

			$limite = $this->input->post('limite');
			$categorias_ids = $this->input->post('categorias_ids');
			$novas_categorias = $this->input->post('novas_categorias');
			$cat_ids = $categorias_ids?:array();
			if($novas_categorias)
			{
				$novas_categorias_a = explode(',', $novas_categorias);
				foreach ($novas_categorias_a as $item)
				{
					$id = $this->caderno_model->adicionar_categoria(array(
						'usu_id' => get_current_user_id(),
						'cat_nome' => $item
					));
					array_push($cat_ids, $id);
				}
			}

			self::adicionar_categorias_em_caderno($caderno_id, $cat_ids, FALSE, FALSE);
			self::adicionar_questoes_ao_caderno($caderno_id, isset($limite) ? $limite : NULL);

		}
	}

	function adicionar_caderno_existente()
	{
		if($this->input->post())
		{
			$caderno_id = $this->input->post('caderno_id');
			$limite = $this->input->post('limite');
			$categorias_ids = $this->input->post('categorias_ids');
			$novas_categorias = $this->input->post('novas_categorias');

			$cat_ids = $categorias_ids?:array();
			if($novas_categorias)
			{
				$novas_categorias_a = explode(',', $novas_categorias);
				foreach ($novas_categorias_a as $item)
				{
					$id = $this->caderno_model->adicionar_categoria(array(
						'usu_id' => get_current_user_id(),
						'cat_nome' => $item
					));
					array_push($cat_ids, $id);
				}
			}

			$filtro_caderno = base64_decode($this->input->post("filtro_caderno"));

			$this->caderno_model->atualizar_filtros($caderno_id, $filtro_caderno);
			
			self::adicionar_categorias_em_caderno($caderno_id, $cat_ids, FALSE, FALSE);
			self::adicionar_questoes_ao_caderno($caderno_id, isset($limite) ? $limite : NULL, $filtro_caderno);
		}
	}

	private function adicionar_questoes_ao_caderno($caderno_id, $limite = null, $filtro = null)
	{
		$maximo = $limite ? $limite : MAX_QUESTOES_ADICIONADAS_CADERNO;
		
		$caderno = $this->caderno_model->get_caderno($caderno_id);
		if($caderno){
			$filtro = unserialize($filtro?:$caderno['cad_filtros']);

			if($filtro != TODAS) {
				$incluir_inativas = tem_acesso(array(ADMINISTRADOR, PROFESSOR, REVISOR));
				$questoes = $this->questao_model->listar_por_filtros($filtro, MAX_QUESTOES_ADICIONADAS_CADERNO, null, false, $incluir_inativas);
				$this->caderno_model->adicionar_questoes_em_caderno($caderno_id, $questoes, $maximo);
			}

			set_mensagem(SUCESSO,'Questões adicionadas ao caderno');
		}else{
			set_mensagem(ERRO,'Caderno não encontrado');
		}
		redirect(get_resolver_questoes_url() . "?" . $_SERVER['QUERY_STRING'] );
	}

	// @TODO: Migrar operações de cadernos para o controller cadernos

	function editar_caderno()
	{
		if($this->input->post()) {
			$data['cad_id'] = $this->input->post('cad_id');
			$data['cad_nome'] = $this->input->post('cad_nome');
			$data['cad_comercializavel'] = $this->input->post('cad_comercializavel');
			$data['cad_coaching'] = $this->input->post('cad_coaching');

			$categorias_ids = $this->input->post('categorias_ids');
			$novas_categorias = $this->input->post('novas_categorias');

			if($novas_categorias) {
				$novas_categorias_a = explode(',', $novas_categorias);

				foreach ($novas_categorias_a as $item) {
					$id = $this->caderno_model->adicionar_categoria(array(
						'usu_id' => get_current_user_id(),
						'cat_nome' => $item
					));

					array_push($categorias_ids, $id);
				}
			}

			self::adicionar_categorias_em_caderno($data['cad_id'], $categorias_ids, TRUE, FALSE);

			$this->caderno_model->atualizar_caderno($data);
			set_mensagem(SUCESSO, 'Caderno atualizado com sucesso!');
		}
		redirect(get_meus_cadernos_url());
	}

	function editar_categoria()
	{
		if($this->input->post()) {
			$data['cat_id'] = $this->input->post('cat_id');
			$data['cat_nome'] = $this->input->post('cat_nome');

			$this->caderno_model->atualizar_categoria($data);
			set_mensagem(SUCESSO, 'Categoria atualizado com sucesso!');
		}
		redirect(get_meus_cadernos_url());
	}

	function excluir_caderno() {
		$this->load->helper('caderno');

		if($this->input->post()) {
			$caderno_id  = $this->input->post('item_id');
			$caderno = $this->caderno_model->get_caderno($caderno_id);

			// if(is_compartilhado($caderno)) {
			// 	$this->caderno_model->excluir_caderno_compartilhado($caderno_id, get_current_user_id());
			// }
			// else {

			if($this->caderno_model->is_caderno_associado_a_produto($caderno_id)) {
				set_mensagem(ERRO, 'Não foi possível excluir o caderno selecionado, pois o mesmo está associado a um produto!');
			}
			else {
				$this->caderno_model->excluir_caderno($caderno_id);
				set_mensagem(SUCESSO, 'Caderno excluído com sucesso!');
			}

		}
		redirect(get_meus_cadernos_url());
	}

	function excluir_caderno_compartilhado() {
		if($this->input->post()) {
			$this->caderno_model->excluir_caderno_compartilhado($this->input->post('item_id'), get_current_user_id());
			set_mensagem(SUCESSO, 'Caderno excluído com sucesso!');
		}
		redirect(get_meus_cadernos_url());
	}

	function excluir_categoria() {
		if($this->input->post()) {
			$this->caderno_model->excluir_categoria($this->input->post('del_cat_item_id'));
			set_mensagem(SUCESSO, 'Categoria excluída com sucesso!');
		}
		redirect(get_meus_cadernos_url());
	}

	function xhr_get_caderno($caderno_id)
	{
		$caderno = $this->caderno_model->get_caderno($caderno_id);

		if($caderno) {
			$caderno['categorias'] = $this->caderno_model->listar_categorias_ids_de_caderno($caderno_id);
		}
		echo json_encode($caderno);
	}

	function xhr_get_categoria($categoria_id)
	{
		$categoria = $this->caderno_model->get_categoria($categoria_id);
		echo json_encode($categoria);
	}

	function xhr_get_categorias_de_caderno($caderno_id)
	{
		$categorias = $this->caderno_model->listar_categorias_ids_de_caderno($caderno_id);

		$categorias_array = array();
		foreach ($categorias as $categoria) {
			array_push($categorias_array, $categoria['cat_id']);
		}

		echo json_encode($categorias_array);
	}
		
	function xhr_incrementar_tempo_gasto_caderno($caderno_id, $segundos = null)
	{
		if($segundos)
		{
			$this->caderno_model->incrementar_tempo_gasto($caderno_id, $segundos);
		}
		else
		{
			$this->caderno_model->incrementar_tempo_gasto($caderno_id);
		}
	}

	function xhr_incrementar_tempo_gasto_simulado($simulado_id, $segundos = null)
	{
		if($segundos)
		{
			$this->simulado_model->incrementar_tempo_gasto($simulado_id, get_current_user_id(), $segundos);
		}
		else
		{
			$this->simulado_model->incrementar_tempo_gasto($simulado_id, get_current_user_id());
		}
	}

	function adicionar_nova_categoria()
	{
		if($this->input->post()) {
			$data = array(
					'cat_nome' => $this->input->post('nome'),
					'usu_id' => get_current_user_id()
			);
			$categoria_id = $this->caderno_model->adicionar_categoria($data);
			//$excluir_atuais = $this->input->post('massa') ? FALSE : TRUE;
			$excluir_atuais = FALSE;
			$cadernos_ids = explode("-", $this->input->post('caderno_id'));
			self::adicionar_categorias_em_cadernos($cadernos_ids, array($categoria_id), $excluir_atuais);
		}
	}

	function adicionar_categoria_existente()
	{
		if($this->input->post()) {
			$categorias_ids = $this->input->post('categorias_ids');
			$cadernos_ids = explode("-", $this->input->post('caderno_id'));
			$excluir_atuais = $this->input->post('massa') ? FALSE : TRUE;

			self::adicionar_categorias_em_cadernos($cadernos_ids, $categorias_ids, $excluir_atuais);
		}
	}

	function favoritas()
	{
		tem_acesso(array(
				/*ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, CONSULTOR, ASSINANTE_SQ, REVISOR*/ USUARIO_LOGADO
		), ACESSO_NEGADO_SQ);

		$data['include_chosen'] = true;
		$data['include_validade'] = true;

		$data['disciplinas'] = $this->questao_model->listar_disciplinas_favoritas(get_current_user_id());
		foreach ($data['disciplinas'] as &$disciplina) {
			$disciplina['questoes_certas'] = $this->questao_model->get_total_resolvidas_favoritas(get_current_user_id(), $disciplina['dis_id'], CERTO);
			$disciplina['questoes_erradas'] = $this->questao_model->get_total_resolvidas_favoritas(get_current_user_id(), $disciplina['dis_id'], ERRADO);
			$disciplina['total_questoes'] = $this->questao_model->get_total_questoes_favoritas(get_current_user_id(), $disciplina['dis_id']);
			$disciplina['total_questoes_respondidas'] = $disciplina['questoes_certas'] + $disciplina['questoes_erradas'];
			$disciplina['total_questoes_nao_respondidas'] = $disciplina['total_questoes'] - $disciplina['total_questoes_respondidas'];
		}

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view(get_favoritas_view_url(), $data);
		$this->load->view(get_main_footer_view_url(), $data);
	}


	private function adicionar_categorias_em_caderno($caderno_id, $categorias_ids, $excluir_atuais = TRUE, $redirecionar = TRUE)
	{
		$this->caderno_model->adicionar_categorias_em_caderno($caderno_id, $categorias_ids, $excluir_atuais);

		if($redirecionar) {
			set_mensagem(SUCESSO,'Categoria(s) adicionada(s) ao caderno');
			redirect(get_meus_cadernos_url());
		}
	}

	private function adicionar_categorias_em_cadernos($cadernos_id, $categorias_ids, $excluir_atuais)
	{
		$this->load->helper('caderno');

		$cadernos_id = array_remove_blank($cadernos_id);

		if($cadernos_id) {

			if($categorias_ids) {

				$this->load->helper('caderno');

				foreach ($cadernos_id as $caderno_id) {
					$caderno = array('cad_id' => $caderno_id);

					if(pode_categorizar_caderno($caderno)) {
						$this->caderno_model->adicionar_categorias_em_caderno($caderno_id, $categorias_ids, $excluir_atuais);
					}
				}

				set_mensagem(SUCESSO,'Categoria(s) adicionada(s) aos cadernos');

			}
			else {
				set_mensagem(ERRO,'É necessário escolher pelo menos uma categoria');
			}

		}
		else {
			set_mensagem(ERRO,'É necessário selecionar pelo menos um caderno');
		}

		redirect(get_meus_cadernos_url());
	}

	public function remover_filtro($campo, $valor, $url64)
	{
		// define qual filtro é: resolver questões, listar questões ou revisar questões
		$url = base64_decode($url64);
		$filtro_key = get_filtro_key($url);

		if(isset($_SESSION[$filtro_key])) {

			$filtro = unserialize($_SESSION[$filtro_key]);

			if(isset($filtro[$campo])) {

				// Campos que possuem valores múltiplos
				if(is_array($filtro[$campo])) {

					if (($key = array_search($valor, $filtro[$campo])) !== false) {
		    			unset($filtro[$campo][$key]);

		    			if(!$filtro[$campo]) {
		    				unset($filtro[$campo]);
		    			}
					}

				}
				// Campos que possuem valores únicos
				else {
					unset($filtro[$campo]);
				}

			}

			$_SESSION[$filtro_key] = serialize($filtro);
		}

		redirect($url);
	}

	public function get_ajax_main_remover_formatacao_alternativas(){

		AcessoGrupoHelper::limpar_formatacao(ACESSO_NEGADO);

		$questao_id = $this->input->post("id");

		$questao = $this->questao_model->get_by_id($questao_id);

		if(!$questao){
			echo "Questão inválida!";
			return;
		}

		/*$historico_base = array(
			'que_id' => $questao_id,
			'usu_id' => get_current_user_id(),
			'qhi_data' => date('Y-m-d H:i:s')
		);*/

		$new_texto_base = sanitizar($questao['que_texto_base']);
		$this->questao_model->atualizar_texto_base($questao_id, $new_texto_base);

		//Histórico do texto base
		/*$historico = $historico_base;
		$historico['qhi_campo'] = HISTORICO_TEXTO_BASE;
		$historico['qhi_valor_antigo'] = $questao['que_texto_base'];
		$historico['qhi_valor_novo'] = $new_texto_base;
		$this->questao_model->salvar_historico($historico);*/
		
		//Do enunciado só é removido negrito, itálico, sublinhado, tachado e cor de fundo
		$enunciado = $questao['que_enunciado'];	
		$enunciado = str_replace("font-style: italic;","", $enunciado);
		$enunciado = str_replace("font-style: oblique;","", $enunciado);
		$enunciado = str_replace("font-weight: bold;","", $enunciado);
		$enunciado = str_replace("font-weight: bolder;","", $enunciado);
		$enunciado = str_replace("font-weight:","fontt-weight:", $enunciado);//devido à grande quantidade de possibilidades de valor apenas 'estragamos' a propriedade
		$enunciado = str_replace("text-decoration: overline;","", $enunciado);
		$enunciado = str_replace("text-decoration: line-through;","", $enunciado);
		$enunciado = str_replace("text-decoration: underline;","", $enunciado);
		$enunciado = str_replace("text-decoration-line: overline;","", $enunciado);
		$enunciado = str_replace("text-decoration-line: line-through;","", $enunciado);
		$enunciado = str_replace("text-decoration-line: underline;","", $enunciado);
		$enunciado = str_replace("text-decoration-line:","textt-decoration-line:", $enunciado);
		$enunciado = str_replace("text-decoration:","textt-decoration:", $enunciado);//devido à grande quantidade de possibilidades de valor apenas 'estragamos' a propriedade
		$enunciado = str_replace("background-color:","backgroundd-color:", $enunciado);//devido à grande quantidade de possibilidades de valor apenas 'estragamos' a propriedade
		$enunciado = str_replace("bgcolor:","bggcolor:", $enunciado);//devido à grande quantidade de possibilidades de valor apenas 'estragamos' a propriedade
		$enunciado = str_replace("<b>","", $enunciado);
		$enunciado = str_replace("</b>","", $enunciado);
		$enunciado = str_replace("<i>","", $enunciado);
		$enunciado = str_replace("</i>","", $enunciado);
		$enunciado = str_replace("<em>","", $enunciado);
		$enunciado = str_replace("</em>","", $enunciado);
		$enunciado = str_replace("<strong>","", $enunciado);
		$enunciado = str_replace("</strong>","", $enunciado);
		$enunciado = str_replace("<u>","", $enunciado);
		$enunciado = str_replace("</u>","", $enunciado);
		$enunciado = str_replace("<s>","", $enunciado);
		$enunciado = str_replace("</s>","", $enunciado);
		$enunciado = str_replace("<strike>","", $enunciado);
		$enunciado = str_replace("</strike>","", $enunciado);
		$enunciado = str_replace("<del>","", $enunciado);
		$enunciado = str_replace("</del>","", $enunciado);
		$enunciado = str_replace("<ins>","", $enunciado);
		$enunciado = str_replace("</ins>","", $enunciado);

		$this->questao_model->atualizar_enunciado($questao_id, $enunciado);
		
		//Histórico do enunciado
		/*$historico = $historico_base;
		$historico['qhi_campo'] = HISTORICO_ENUNCIADO;
		$historico['qhi_valor_antigo'] = $questao['que_enunciado'];
		$historico['qhi_valor_novo'] = $enunciado;
		$this->questao_model->salvar_historico($historico);*/

		$opcoes = $this->questao_model->listar_opcoes($questao_id, FALSE);

		foreach($opcoes as $op){

			$new_op_texto = sanitizar($op['qop_texto']);
			$this->questao_model->alterar_opcao_texto($op['qop_id'], $new_op_texto);
			
			//Histórico das alternativas
			/*$historico = $historico_base;
			$historico['qhi_campo'] = HISTORICO_ALTERNATIVAS;
			$historico['qhi_valor_antigo'] = get_letra_opcao_questao($op['qop_ordem']) . ") " . $op['qop_texto'];
			$historico['qhi_valor_novo'] = get_letra_opcao_questao($op['qop_ordem']) . ") " . $new_op_texto;
			$this->questao_model->salvar_historico($historico);*/
		}

		//Histórico de ação de limpeza de formatação de alternativas
		$historico = array(
			'que_id' => $questao_id,
			'usu_id' => get_current_user_id(),
			'qhi_data' => date('Y-m-d H:i:s'),
			'qhi_campo' => HISTORICO_ALTERNATIVAS,
			'qhi_valor_antigo' => '',
			'qhi_valor_novo' => 'Removeu formatação de enunciado, dados da questão e alternativas'
		);
		$this->questao_model->salvar_historico($historico);

		echo "A formatação das alternativas foi removida com sucesso!";
	}

	/**
	 * Redireciona para uma questão aleatória de um simulado
	 * 
	 * @since L1
	 * 
	 * @param int $total_questoes Total de questões no simulado, esse valor muda caso o simulado esteja com filtro
	 * @param string $uri URI da busca atual
	 * 
	 */
	public function ir_para_questao_aleatoria($total_questoes, $uri = null, $is_favoritas = null){

		$query_string = http_build_query($_GET);
		if($query_string){
			$query_string = "?" . $query_string;
		}

		$aleatoria = mt_rand(0, $total_questoes -1);

		if($is_favoritas)
		{
			$url = get_resolver_favoritas_url($is_favoritas);
		}
		else
		{
			$url = get_resolver_questoes_url();
			if($uri)
			{
				$uri = urldecode( urldecode( $uri ) );
				$url .= "/$uri";
			}
		}

		redirect("{$url}/{$aleatoria}" . $query_string);
	}

	private function notificar_novo_comentario($comentario, $enviar_email = FALSE){
		
		KLoader::helper("UrlHelper");
		$this->load->model('notificacao_model');

		$usuarios = $this->questao_model->listar_usuarios_acompanhando_questao($comentario['que_id'], $comentario['user_id']);

		if($enviar_email == TRUE)
		{
			$comentador = get_usuario_array($comentario['user_id']);
			$comentador_nome_exibicao = get_usuario_nome_exibicao($comentador['id']);
			$is_professor = is_professor($comentario['user_id'], true);
			$comentador_url = site_url(UrlHelper::get_professor_url($comentador['slug']));
			$questao_url = get_questao_url($comentario['que_id']);
			
			$questao = $this->questao_model->get_by_id($comentario['que_id']);
			$disciplina = $this->disciplina_model->get_by_id($questao['dis_id']);
		}

		foreach($usuarios as $usuario){

			//Salva uma notificação para o usuário
			$notificacao = array(
					'com_id' => $comentario['com_id'],
					'not_remetente_id' => $comentario['user_id'],
					'not_destinatario_id' => $usuario['user_id'],
			);

			$this->notificacao_model->salvar($notificacao);

			if($enviar_email == TRUE)
			{
				//Se o usuário tiver um email cadastrado, envia um e-mail de notificação
				if(!$usuario['email']) continue;

				$mensagem = get_template_email('notificacao-comentario-questao.php', array(
					'nome' => $usuario['nome'],
					'questao_id' => $comentario['que_id'],
					'disciplina' => $disciplina['dis_nome'],
					'is_professor' => $is_professor,
					'comentario_usuario_url' => $comentador_url, 
					'comentario_usuario' => $comentador_nome_exibicao,
					'comentario' => $comentario['com_texto'],
					'questao_link' => $questao_url
				));
				
				//enviar_email($usuario['email'], 'Exponencial Concursos - Novo Comentário', $mensagem);
			}
		}
	}

	/**
	 * Redireciona a URL antiga de Meus Simulados para a atual
	 * 
	 * @since M4
	 */
	public function meus_simulados(){
		redirect( get_meus_simulados_url() );
	}

}
