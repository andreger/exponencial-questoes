<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sandbox extends CI_Controller {

	public  function __construct() {
		parent::__construct();
		
		#$this->output->enable_profiler(true);
		#ini_set('display_errors', 1);
		#ini_set('display_startup_errors', 1);
		#error_reporting(E_ALL);
	}
	
	public function shorten()
	{
		$this->load->helper('google');
		
		echo get_shorten_url("http://www.google.com.br");
	}
	
	public function valid()
	{
		$file = $_SERVER['DOCUMENT_ROOT'] . "/questoes/uploads/imagens/aaa.jpg";
		if(getimagesize($file)) {
			echo "imagem";
		}
		else {
			unlink($file);
			echo "não imagem";
		}
	}
	
	public function criar_usuario()
	{
		$id = time();
		$email = "teste{$id}@gmail.com";
		$senha = "123456";
		$nome = "Teste {$id}"; 
		
		criar_usuario($email, $senha, $nome);
		
		echo "Criado usuário: $email com senha $senha";
	}

	public function travar_quantidade_questoes_comentadas()
	{
		$this->load->model('Relatorios_model');
		$this->Relatorios_model->output->enable_profiler(TRUE);
		$this->Relatorios_model->travar_questoes_comentadas();

		echo "A quantidade de questões comentadas por professor foi salva.";
	}

	public function gerar_prefixo_arquivo_provas($prova_id = null)
	{
		$this->load->model("prova_model");
		$this->load->helper("prova");

		if($prova_id)
		{
			$prova['pro_id'] = $prova_id;
			$provas = array($prova);
		}
		else
		{
			$provas = $this->prova_model->listar_todas();
		}

		foreach($provas as $prova)
		{
			$this->prova_model->gerar_nome_arquivo_prova( $prova['pro_id'] );
		}

		echo "Foram gerados os prefixos dos arquivos para " . count($provas) . " provas";
	}

	public function remover_cache_questoes()
	{
		echo "iniciou<br>";
		
		tem_acesso([ADMINISTRADOR], ACESSO_NEGADO);

		echo "vai chamar 'memcached_remover_cache_questoes'...<br>";
		memcached_remover_cache_questoes();
		echo "chamou...<br>";
		echo "<br> Cache de questões removido com sucesso!";
	}

}