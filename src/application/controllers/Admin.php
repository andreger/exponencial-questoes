<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

KLoader::helper("AcessoGrupoHelper");

class Admin extends CI_Controller {

	public function __construct() {

		parent::__construct ();

		if(is_area_desativada(array(PAINEL_SQ_GERAL, PAINEL_SQ_ADMINISTRACAO))){
			redirecionar_erro_500();
		}

		session_start();

		$this->load->model ( 'configuracao_model' );
		$this->load->model ( 'disciplina_model' );
		$this->load->model ( 'banca_model' );
		$this->load->model ( 'orgao_model' );
		$this->load->model ( 'cargo_model' );
		$this->load->model ( 'assunto_model' );
		$this->load->model ( 'prova_model' );
		$this->load->model ( 'questao_model' );
		$this->load->model ( 'area_formacao_model' );
		$this->load->model ( 'area_atuacao_model' );
		$this->load->model ( 'simulado_model' );
		$this->load->model ( 'comentario_model');
		$this->load->model ( 'caderno_model');
		$this->load->model ( 'ranking_model' );

		$this->load->helper('questao');
		$this->load->helper('caderno');
		$this->load->helper('filtro');
		$this->load->helper('aluno');

		$this->load->library('parser');

		init_profiler();

	}

	function check_default($post_string) {
		return $post_string == OPCAO_ZERO ? FALSE : TRUE;
	}

	function check_comentario($string) {

		$string = str_replace (array("\r\n", "\n", "\r"), ' ', $text);

		if($string == "<p><br></p>" || $string == "") {
			return true;
		}
		if($this->input->post("user_id") > 0) {
			return true;
		}

		return false;
	}

	function check_opcao($que_tipo){
		return false;
	}

	function check_vimeo($url){
		if($url){
			KLoader::helper("VimeoHelper");
			return VimeoHelper::is_vimeo_url($url);
		}else{
			return true;
		}
	}

	public function index()
	{
		redirect(get_listar_simulados_url());
	}

	/**
	 * DISCIPLINA
	 */

	/**
	 * Lista as disciplinas existentes
	 */

	public function listar_disciplinas()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$data['include_data_tables'] = true;
		$disciplinas = $this->disciplina_model->listar_todas(FALSE, FALSE);

		$data['thead_array'] = array('Nome', 'Status', 'Ações');
		$data['tbody_array'] = get_tabela_disciplinas($disciplinas);
		$data['tfoot_array'] = $data['thead_array'];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_disciplina_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Salva e edita disciplina
	 */
	public function editar_disciplina($disciplina_id = null) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		if ($this->input->post ( 'salvar' )) {

			$this->form_validation->set_error_delimiters ( '<span>', '</span>' );
			$this->form_validation->set_rules ( 'dis_nome', 'Nome', "required|max_length[100]");
			$this->form_validation->set_message ( 'required', '%s é obrigatório.' );
			$this->form_validation->set_message ( 'max_length', '%s não pode passar de 30 caracteres.' );

			if ($this->form_validation->run () === true) {
				$disciplina['dis_nome'] = $this->input->post ( 'dis_nome' );
				$disciplina['dis_ativo'] = $this->input->post ( 'dis_ativo' );

				if (!empty ( $disciplina_id )) {
					$atualizar = FALSE;
					$original = $this->disciplina_model->get_by_id($disciplina_id);
					$disciplina ['dis_id'] = $disciplina_id;

					if(($disciplina['dis_ativo']) == INATIVO) {
						$simulados = $this->disciplina_model->listar_simulados_atrelados($disciplina_id);

						foreach ($simulados as $simulado) {
							$simulado_id = $simulado['sim_id'];

							/****************************************************************************
							 * Permitir inativar disciplinas
							 * https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1898
							 ***************************************************************************/
							/**
							 * EC-116, Podio 2905: Foi solicitado deixar o simulado inconsistente somente manualmente
							 */
							//$this->simulado_model->atualizar_status($simulado_id, INCONSISTENTE);

							$this->simulado_model->remover_disciplina($simulado_id, $disciplina_id);
							$this->simulado_model->remover_revisores_disciplina($simulado_id, $disciplina_id);
							$this->simulado_model->remover_questoes_disciplinas($simulado_id, $disciplina_id);
						}

					}

					if($original['dis_nome'] == $disciplina['dis_nome']) {
						$atualizar = TRUE;
					}
					else {
						if($this->disciplina_model->get_by_nome($disciplina['dis_nome'])) {
							set_mensagem_flash( 'erro', 'Já existe uma disciplina com esse nome');
						}
						else {
							$atualizar = TRUE;
						}

					}

					if($atualizar) {
						$this->disciplina_model->atualizar ( $disciplina );
						set_mensagem_flash( 'sucesso', 'Disciplina atualizada com sucesso!' );
					}

				}
				else {
					$dis_id = $this->disciplina_model->salvar ( $disciplina );

					$assunto = array(
							ass_nome => SEM_ASSUNTO,
							dis_id => $dis_id
					);
					$this->assunto_model->salvar($assunto);

					set_mensagem_flash ( 'sucesso', 'Disciplina inserida com sucesso!' );
				}
				redirect ( get_listar_disciplinas_url () . '/' );
			}
		}



		$data['statuses'] = get_situacoes_combo(null);
		$data['disciplina'] = array();

		if(!is_null($disciplina_id)) {
			$data['disciplina'] = $this->disciplina_model->get_by_id($disciplina_id);
			$data['simulados_atrelados'] = $this->disciplina_model->listar_simulados_atrelados($disciplina_id);
		}

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_editar_disciplina_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Exclui disciplina
	 */
	public function excluir_disciplina($disciplina_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$this->disciplina_model->excluir ( $disciplina_id );
		$this->session->set_flashdata ( 'sucesso', 'Disciplina excluída com sucesso!' );
		redirect ( get_listar_disciplinas_url () . '/');
	}

	/**
	 * Exibe a árvore de disciplinas
	 */
	public function arvore_disciplinas() {
		tem_acesso(array(
				ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, PROFESSOR, CONSULTOR, REVISOR
		), ACESSO_NEGADO);

		$data ['include_jstree'] = true;

		$disciplinas = $this->disciplina_model->listar_todas();

		foreach ($disciplinas as &$disciplina) {
			$disciplina['filhos'] = $this->assunto_model->get_arvore($disciplina['dis_id']);
		}

		// define o item raiz abstrato com filhos disciplinas
		$data['arvore']['filhos'] = $disciplinas;

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_arvore_disciplinas_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * BANCA
	 */

	/**
	 * Lista as bancas existentes
	 */
	public function listar_bancas()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$data['include_data_tables'] = true;
		$bancas = $this->banca_model->listar_todas();

		$data['thead_array'] = array('Nome Abreviado', 'Nome Completo', 'Ações');
		$data['tbody_array'] = get_tabela_bancas($bancas);
		$data['tfoot_array'] = $data['thead_array'];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_banca_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Salva e edita banca
	 */

	public function editar_banca($banca_id = null)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		if($this->input->post('salvar')) {
			$original = $this->banca_model->get_by_nome($this->input->post('ban_nome'));

			$this->form_validation->set_error_delimiters('<span>', '</span>');
			$this->form_validation->set_rules('ban_nome', 'Nome Abreviado', "required|max_length[100]");
			$this->form_validation->set_rules('ban_nome_completo', 'Nome Completo', 'max_length[100]');
			$this->form_validation->set_message('required', '%s é obrigatório.');
			$this->form_validation->set_message('max_length', '%s não pode passar de 100 caracteres.');
			$this->form_validation->set_message('edit_unique', '%s já existe no cadastro.');

			if ($this->form_validation->run() === true)
			{
				$data['ban_nome'] = $this->input->post('ban_nome');
				$data['ban_nome_completo'] = $this->input->post('ban_nome_completo');
				if(!empty($banca_id)) {
					// caso nome original não tenha sido mudado não envia o nome da banca para atualizar
					if($data['ban_nome'] == $original['ban_nome']) {
						unset($data['ban_nome']);
						$data['ban_id'] = $banca_id;
						$this->banca_model->atualizar($data);
						set_mensagem_flash('sucesso', 'Banca atualizada com sucesso!');
					}
					else {
						if($this->banca_model->get_by_nome($data['ban_nome'])) {
							set_mensagem_flash('erro', 'Já existe banca com esse nome!');
						}
						else {
							$data['ban_id'] = $banca_id;
							$this->banca_model->atualizar($data);
							set_mensagem_flash('sucesso', 'Banca atualizada com sucesso!');
						}
					}

				} else {
					$this->banca_model->salvar ( $data );
					$this->session->set_flashdata ( 'sucesso', 'Banca inserida com sucesso!' );
				}
				redirect ( get_listar_bancas_url () . '/' );
			}
		}

		$data['banca'] = array();
		if(!is_null($banca_id))
			$data['banca'] = $this->banca_model->get_by_id($banca_id);

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_editar_banca_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Exclui banca
	 */
	public function excluir_banca($banca_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->banca_model->excluir ( $banca_id );
		$this->session->set_flashdata ( 'sucesso', 'Banca excluída com sucesso!' );
		redirect ( get_listar_bancas_url () . '/');
	}

	/**
	 * ÓRGÃO
	 */

	/**
	 * Lista os órgãos existentes
	 */
	public function listar_orgaos()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$data['include_data_tables'] = true;
		$orgaos = $this->orgao_model->listar_todas();

		$data['thead_array'] = array('Nome Abreviado', 'Nome Completo', 'Ações');
		$data['tbody_array'] = get_tabela_orgaos($orgaos);
		$data['tfoot_array'] = $data['thead_array'];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_orgao_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Salva e edita órgão
	 */
	public function editar_orgao($orgao_id = null)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		if($this->input->post('salvar')) {
			$original = $this->orgao_model->get_by_nome($this->input->post('org_nome'));
			$regra_unique = count($original) > 0 ? "|edit_unique[orgaos.org_nome.{$original['org_nome']}]" : "";

			$this->form_validation->set_error_delimiters('<span>', '</span>');
			$this->form_validation->set_rules('org_nome', 'Nome Abreviado', 'required|max_length[100]');
			$this->form_validation->set_rules('org_nome_completo', 'Nome Completo', 'max_length[100]');
			$this->form_validation->set_message('required', '%s é obrigatório.');
			$this->form_validation->set_message('max_length', '%s não pode passar de 100 caracteres.');
			$this->form_validation->set_message('edit_unique', '%s já existe no cadastro.');

			if ($this->form_validation->run() === true)
			{
				$data['org_nome'] = $this->input->post('org_nome');
				$data['org_nome_completo'] = $this->input->post('org_nome_completo');
				if(!empty($orgao_id)) {
					$data['org_id'] = $orgao_id;
					$this->orgao_model->atualizar($data);
					$this->session->set_flashdata('sucesso', 'Instituição atualizada com sucesso!');
				} else {
					$orgao = $this->orgao_model->get_by_nome($data ['org_nome']);
					$this->orgao_model->salvar($data);
					$this->session->set_flashdata('sucesso', 'Instituição inserida com sucesso!');
				}
				redirect ( get_listar_orgaos_url () . '/' );
			}
		}

		$data['orgao'] = array();
		if(!is_null($orgao_id))
			$data['orgao'] = $this->orgao_model->get_by_id($orgao_id);

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_editar_orgao_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Exclui orgao
	 */

	public function excluir_orgao($orgao_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$this->orgao_model->excluir($orgao_id);
		$this->session->set_flashdata('sucesso', 'Instituição excluída com sucesso!');
		redirect(get_listar_orgaos_url() . '/');
	}

	/**
	 * CARGO
	 */

	/**
	 * Lista os cargos existentes
	 */
	public function listar_cargos()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$data['include_data_tables'] = true;
		$cargos = $this->cargo_model->listar_todas();

		$data['thead_array'] = array('Nome', 'Ações');
		$data['tbody_array'] = get_tabela_cargos($cargos);
		$data['tfoot_array'] = $data['thead_array'];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_cargo_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Salva e edita cargo
	 */
	public function editar_cargo($cargo_id = null) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		if ($this->input->post ( 'salvar' )) {
			$this->form_validation->set_error_delimiters ( '<span>', '</span>' );
			$this->form_validation->set_rules ( 'car_nome', 'Nome', 'required|max_length[50]' );
			$this->form_validation->set_message ( 'required', '%s é obrigatório.' );
			$this->form_validation->set_message ( 'max_length', '%s não pode passar de 50 caracteres.' );

			if ($this->form_validation->run () === true) {
				$data ['car_nome'] = $this->input->post ( 'car_nome' );
				if (! empty ( $cargo_id )) {
					$data ['car_id'] = $cargo_id;
					$this->cargo_model->atualizar ( $data );
					$this->session->set_flashdata ( 'sucesso', 'Cargo atualizado com sucesso!' );
				} else {
					$cargo = $this->cargo_model->get_by_nome($data ['car_nome']);
					if(!count($cargo) > 0) {
						$this->cargo_model->salvar ( $data );
						$this->session->set_flashdata ( 'sucesso', 'Cargo inserido com sucesso!' );
					} else {
						$this->session->set_flashdata ( 'sucesso', 'Cargo já existe!' );
					}
				}
				redirect ( get_listar_cargos_url () . '/' );
			}
		}

		$data['cargo'] = array();
		if(!is_null($cargo_id))
			$data['cargo'] = $this->cargo_model->get_by_id($cargo_id);

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_editar_cargo_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Exclui cargo
	 */
	public function excluir_cargo($cargo_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$this->cargo_model->excluir ( $cargo_id );
		$this->session->set_flashdata ( 'sucesso', 'Cargo excluído com sucesso!' );
		redirect ( get_listar_cargos_url () . '/');
	}

	/**
	 * ASSUNTO
	 */

	/**
	 * Lista as assuntos existentes
	 */
	public function listar_assuntos($offset = 0)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$data['include_data_tables'] = true;

		if($this->input->post('filtrar')) {
			$_SESSION['q_assunto'] = $this->input->post('q');
			$_SESSION['st_a_assunto'] = $this->input->post('st_a');
			$_SESSION['st_i_assunto'] = $this->input->post('st_i');
			$offset = 0;
		}

		$palavra = isset($_SESSION['q_assunto']) ? $_SESSION['q_assunto'] : null;
		$ativo = isset($_SESSION['st_a_assunto']) ? $_SESSION['st_a_assunto'] : null;
		$inativo = isset($_SESSION['st_i_assunto']) ? $_SESSION['st_i_assunto'] : null;

		$assuntos = $this->assunto_model->buscar($offset, $palavra, $ativo, $inativo);
		foreach ($assuntos as &$assunto) {
			if($assunto['ass_ativo'] == NAO) {
				$assunto['ass_nome'] = $assunto['ass_nome'] . " (?)";
			}
		}

		$total = $this->assunto_model->contar($palavra, $ativo, $inativo);

		$config = pagination_config(get_listar_assuntos_url(), $total, ASSUNTOS_POR_PAGINA);
		$this->pagination->initialize($config);

		$data["links"] = $this->pagination->create_links();

		$data['thead_array'] = array('Disciplina','Assunto Pai', 'Nome', 'Status', 'Ações');
		$data['tbody_array'] = get_tabela_assuntos($assuntos);
		$data['tfoot_array'] = $data['thead_array'];


		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_assunto_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Salva e edita assunto
	 */
	public function editar_assunto($assunto_id = null)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		if($this->input->post('salvar')) {
			$this->form_validation->set_error_delimiters('<span>', '</span>');
			$this->form_validation->set_rules('ass_nome', 'Nome', "required|max_length[200]");
			$this->form_validation->set_rules('dis_id', 'Disciplina', 'required|callback_check_default');
			$this->form_validation->set_message('required', '%s é obrigatório.');
			$this->form_validation->set_message('max_length', '%s não pode passar de 200 caracteres.');
			$this->form_validation->set_message('check_default', 'Você precisa escolher uma disciplina.');
			$this->form_validation->set_message('edit_unique', '%s já existe no cadastro.');

			if ($this->form_validation->run() === true)
			{
				$data['ass_nome'] = stripslashes($this->input->post('ass_nome'));
				$data['dis_id'] = $this->input->post('dis_id');
				$data['ass_ativo'] = $this->input->post('ass_ativo');

				if($this->input->post('ass_pai_id') > 0) {
					$data['ass_pai_id'] = $this->input->post('ass_pai_id');
				}
				else {
					$data['ass_pai_id'] = null;
				}

				if($assunto_id && $assunto_id == $data['ass_pai_id']) {
					set_mensagem_flash('error', 'Assunto não pode ser assunto-pai dele mesmo!');
				}

				else {
					if(!empty($assunto_id)) {
						$data['ass_id'] = $assunto_id;
						$this->assunto_model->atualizar($data);
						set_mensagem_flash('sucesso', 'Assunto atualizado com sucesso!');
					} else {
						$assunto = $this->assunto_model->get_by_nome($data ['ass_nome']);
						$this->assunto_model->salvar ( $data );
						set_mensagem_flash('sucesso', 'Assunto inserido com sucesso!');
					}
					redirect ( get_listar_assuntos_url () . '/' );
				}
			}
		}

		$data['include_jquery_chained'] = true;
		$data['include_admin_editar_assunto'] = true;

		$data['disciplina_selecionada'] = OPCAO_ZERO;
		$data['assunto_selecionado'] = OPCAO_ZERO;
		$data['assuntos_pai'] = array(OPCAO_ZERO => "Primeiro selecione a disciplina...");
		$data['assunto'] = array();
		$data['disciplinas'] = get_disciplina_combo($this->disciplina_model->listar_todas(false, false), " (?)");
		$data['statuses'] = get_situacoes_combo(null, 'o');

		if(!is_null($assunto_id)) {
			$data ['assunto'] = $this->assunto_model->get_by_id ( $assunto_id );
			//$data ['assuntos_pai'] = get_assunto_combo ( $this->assunto_model->listar_por_disciplina ( $data ['assunto'] ['dis_id'], NAO ), false, true );
			$data ['disciplina_selecionada'] = $data ['assunto'] ['dis_id'];
			if (! is_null ( $data ['assunto'] ['ass_pai_id'] ))
				$data ['assunto_selecionado'] = $data ['assunto'] ['ass_pai_id'];
		}

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_editar_assunto_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Exclui assunto
	 */
	public function excluir_assunto($assunto_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		//$_SESSION['q_assunto'] = '';

		$this->assunto_model->excluir ( $assunto_id );
		set_mensagem_flash('sucesso', 'Assunto excluído com sucesso!');
		//$this->session->set_flashdata ( 'sucesso', 'Assunto excluído com sucesso!' );
		redirect ( get_listar_assuntos_url () . '/');
	}

	/**
	 * Json para assunto pai
	 */
	public function get_ass_id_ajax($destacar_assunto_folha = false, $remover_sem_assunto = false, $param = 'dis_id', $somente_ativos = NAO, $selecionado_ass_id = 0) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, PROFESSOR, REVISOR
		), ACESSO_NEGADO);

		$disciplina_id = $this->input->get($param);
		$assuntos = $this->assunto_model->listar_por_disciplina($disciplina_id, ($somente_ativos == SIM) ? TRUE : FALSE);
		$string = '[ [ "", "Selecione um assunto..." ]';
		foreach ($assuntos as $assunto) {
			if($remover_sem_assunto) {
				if($assunto['ass_nome'] == SEM_ASSUNTO) continue;
			}

            // Os assuntos cadastrados com aspas duplas ("") estavam inválidando o json gerado,
            // resultando em não carregamento de "Select's" nos formulários
            // Este código remove as aspas duplas e altera por aspas simples
            $assunto['ass_nome'] = str_replace("\"", "'", $assunto['ass_nome']);

			if($assunto['ass_ativo'] == NAO){
				$assunto['ass_nome'] = $assunto['ass_nome'] . " (?)";
			}

			if($destacar_assunto_folha) {
				$possui_filhos = $this->assunto_model->possui_filhos($assunto['ass_id']);



				if(!$possui_filhos) {
					$string .= ', [ "' . $assunto['ass_id'] . '", "' . $assunto['ass_nome'] . ' *" ]';
				}
				else {
					$string .= ', [ "' . $assunto['ass_id'] . '", "' . $assunto['ass_nome'] . '" ]';
				}
			}
			else {
				$string .= ', [ "' . $assunto['ass_id'] . '", "' . $assunto['ass_nome'] . '" ]';
			}
		}

		if($selecionado_ass_id)
		{
			$string .= ', [ "selected", "' . $selecionado_ass_id . '" ]';
		}

		$string .= ' ]';
		echo $string;
	}

	public function conversao_assuntos()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		if($this->input->post('nova_conversao')) {

			$this->form_validation->set_error_delimiters('<span>', '</span>');
			$this->form_validation->set_rules('dis_antigo', 'Disciplina Antiga', "required");
			$this->form_validation->set_rules('dis_novo', 'Disciplina Nova', "required");
			$this->form_validation->set_rules('ass_novo', 'Assunto Novo', "required");

			$this->form_validation->set_message('required', 'Campo %s é obrigatório.');

			$is_conversao_default = $this->input->post("conversao_default");

			if($is_conversao_default){
				$disciplina = $this->input->post('dis_antigo');
				$this->form_validation->set_rules('ass_antigo', 'Assunto Antigo', "is_null", array("is_null" => "O %s deve ser vazio na conversão default."));
				$this->form_validation->set_rules('inativar_assunto', 'Inativar Assunto', "empty", array("empty" => "O Assunto Antigo não pode ser inativado na conversão default."));
				$this->form_validation->set_rules('dis_antigo', 'Disciplina Antiga', 'required|is_unique[assuntos_conversao.dis_id]');
				$this->form_validation->set_message('is_unique', 'Já existe uma conversão default para a disciplina informada.');
			}else{
				$this->form_validation->set_rules('ass_antigo', 'Assunto Antigo', "required");
			}

			if ($this->form_validation->run() === true) {

				$ass_antigo = $this->input->post('ass_antigo');
				$ass_novo = $this->input->post('ass_novo');

				$this->assunto_model->adicionar_conversao($ass_antigo, $ass_novo, $disciplina);

				if($this->input->post('inativar_assunto') && !$is_conversao_default) {
					$this->assunto_model->atualizar(array('ass_id' => $ass_antigo, 'ass_ativo' => NAO));
				}

				if($is_conversao_default){
					$disciplina_nova = $this->input->post('dis_novo');
					$msg_afetados = self::verificar_simulados_afetados_pela_conversao_assunto(null, FALSE, $disciplina_nova, TRUE, $disciplina);
				}else{
					$msg_afetados = self::verificar_simulados_afetados_pela_conversao_assunto($ass_antigo, FALSE);
				}

				set_mensagem_flash('sucesso', 'Conversão salva com sucesso.' . $msg_afetados);

			}else{
				$dis_antigo = $this->input->post('dis_antigo');
				if($dis_antigo){
					$data['assuntos_antigos'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplina($dis_antigo, FALSE));
				}
				$dis_novo = $this->input->post('dis_novo');
				if($dis_novo){
					$data['assuntos_novos'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplina($dis_novo, FALSE));
				}
			}
		}

		if($this->input->post('filtrar'))
		{
			$_SESSION['disciplinas_selecionadas'] = $this->input->post('disciplinas_ids');
			$data['disciplinas_selecionadas_ids'] = $_SESSION['disciplinas_selecionadas'];
		}
		elseif ($_SESSION['disciplinas_selecionadas'])
		{
			$data['disciplinas_selecionadas_ids'] = $_SESSION['disciplinas_selecionadas'];
		}
		else
		{
			$data['disciplinas_selecionadas_ids'] = "";
		}

		$data['include_jquery_chained'] = true;
		$data ['include_chosen'] = true;

		$conversoes = $this->assunto_model->listar_conversoes($data['disciplinas_selecionadas_ids'], $data['disciplinas_selecionadas_ids']);

		$data['conversoes'] = $conversoes;
		$todas_disciplinas = $this->disciplina_model->listar_todas(FALSE, FALSE);
		$data['disciplinas'] = get_disciplina_combo($todas_disciplinas, " (?)");
		$data['disciplinas_multi'] = get_disciplina_multiselect($todas_disciplinas, true, " (?)");
		$data['assuntos'] = array();

		$data['thead_array'] = array('Disciplina Antiga', 'Default?','Assunto Antigo', 'Disciplina Nova', 'Assunto Novo', 'Data Criação', 'Ações');
		$data['tbody_array'] = get_tabela_conversoes($conversoes);
		$data['tfoot_array'] = $data['thead_array'];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/conversao_assuntos', $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function valida_conversao_default($disciplina){
		$conversao_default = $this->assunto_model->get_conversao_default($disciplina);
		if($conversao_default){
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * Define a ordem como os assuntos serão exibidos na árvore de assuntos de uma disciplina
	 *
	 * [JOULE][J2] UPGRADE - Definir ordem de assuntos da Taxonomia
	 *
	 * @see https://podio.com/profinfnetedubr/lslc/apps/upgrades/items/334
	 *
	 * @since 10.1.0
	 */

	public function ordem_taxonomia($disciplina_id = null)
	{
		// controle de acesso
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		// se disciplina já foi selecionada exibe árvore
		if($disciplina_id) {
			$data['assuntos'] =  $this->assunto_model->listar_raizes($disciplina_id, 'ass_prioridade, ass_nome');
		}

		// prepara variáveis para template
		$data['include_nestable'] = true;
		$data['disciplinas'] = get_disciplina_combo($this->disciplina_model->listar_todas(), false);
		$data['disciplina_selecionada'] = $disciplina_id;
		$data['caixa_azul'] = ui_get_alerta("<p>Os assuntos estão ordenados da maior prioridade para a menor.</p><p>Para alterar uma posição basta arrastar o assunto desejado para cima ou para baixo.</p><p>O sistema salvará automaticamente todas as alterações.</p>", ALERTA_INFO);
		// renderiza a página
		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/ordem_taxonomia_combo', $data);

		if($disciplina_id) {
			$this->parser->parse('admin/ordem_taxonomia_arvore', $data);
		}

		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function rodar_conversao($assunto_id, $default = null)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		if($default){
			self::rodar_conversao_default($default);
		}else{

			$conversao = $this->assunto_model->get_conversao($assunto_id);

			$antigo = $this->assunto_model->get_by_id($conversao['ass_antigo']);
			$novo = $this->assunto_model->get_by_id($conversao['ass_novo']);

			// lista e organiza simulados e questões afetadas
			$msg_afetados = self::verificar_simulados_afetados_pela_conversao_assunto($assunto_id, TRUE, $novo['dis_id']);

			$questoes_assuntos = $this->questao_model->listar_por_assunto($assunto_id);
			$questoes_convertidas = count($questoes_assuntos);
			// atualiza os assuntos
			foreach ($questoes_assuntos as $questao_assunto) {

				$is_salva_assunto = TRUE;

				if($antigo['dis_id'] != $novo['dis_id']) {
					$this->questao_model->remover_todos_assuntos_de_questao($questao_assunto['que_id']);
				}else{
					$this->questao_model->remover_assunto_de_questao($assunto_id, $questao_assunto['que_id']);
					if($novo['ass_nome'] == SEM_ASSUNTO && $this->questao_model->contar_assuntos_questao($questao_assunto['que_id']) > 0){
						$is_salva_assunto = FALSE;
					}
				}
				if($is_salva_assunto == TRUE){
					$this->questao_model->salvar_assunto_em_questao($novo, array('que_id' => $questao_assunto['que_id']));
				}
			}

			// atualiza a disciplina
			if($antigo['dis_id'] != $novo['dis_id'])
			{
				$questoes_assuntos = $this->questao_model->listar_por_assunto($conversao['ass_novo']);

				foreach ($questoes_assuntos as $questao_assunto)
				{
					$this->questao_model->atualizar_disciplina($questao_assunto['que_id'], $novo['dis_id']);
				}
			}

			if ($questoes_convertidas == 0)
			{
				set_mensagem_flash('sucesso', "Conversão foi executada com sucesso - Nenhuma questão convertida." . $msg_afetados);
			}
			elseif ($questoes_convertidas == 1)
			{
				set_mensagem_flash('sucesso', "Conversão foi executada com sucesso - {$questoes_convertidas} questão convertida." . $msg_afetados);
			}
			else
			{
			set_mensagem_flash('sucesso', "Conversão foi executada com sucesso - {$questoes_convertidas} questões convertidas." . $msg_afetados);
			}
		}

		redirect(get_conversao_assuntos_url());
	}

	private function rodar_conversao_default($disciplina_id){

		$conversao = $this->assunto_model->get_conversao_default($disciplina_id);
		if($conversao){

			$disciplina = $this->disciplina_model->get_by_id($disciplina_id);

			if($disciplina['dis_ativo'] == SIM){
				set_mensagem_flash('erro', 'A disciplina de origem está ativa e a conversão não será executada.');
				return;
			}

			$novo = $this->assunto_model->get_by_id($conversao['ass_novo']);

			$msg_afetados = self::verificar_simulados_afetados_pela_conversao_assunto(null, TRUE, $novo['dis_id'], TRUE, $disciplina_id);

			$questoes = $this->questao_model->listar_questoes_sem_conversao($disciplina_id);

			foreach($questoes as $questao){

				$this->questao_model->remover_todos_assuntos_de_questao($questao['que_id']);

				$this->questao_model->salvar_assunto_em_questao($novo, array('que_id' => $questao['que_id']));

			}

			$questoes_assuntos = $this->questao_model->listar_por_assunto($conversao['ass_novo']);

			foreach ($questoes_assuntos as $questao_assunto) {
				$this->questao_model->atualizar_disciplina($questao_assunto['que_id'], $novo['dis_id']);
			}

			set_mensagem_flash('sucesso', 'Conversão foi executada com sucesso - {$teste} questões convertidas.' . $msg_afetados);

		}else{
			set_mensagem_flash('erro', 'A conversão default informada não existe.');
		}


	}

	public function verificar_simulados_afetados_pela_conversao_assunto($assunto_antigo, $executar_acoes, $disciplina_nova = null, $is_default = FALSE, $disciplina_antiga = null){

		// lista e organiza simulados e questões afetadas
		$msg_afetados = "";

		if($is_default == TRUE){
			$afetadas = $this->questao_model->listar_questoes_afetadas_conversao_default($disciplina_antiga);
		}else{
			$afetadas = $this->assunto_model->listar_questoes_afetadas_conversao($assunto_antigo);
		}

		$simulados_afetados = array_group_by($afetadas, "sim_id");

		if($simulados_afetados) {

			$msg_afetados .= "<br><strong>ATENÇÃO!</strong> Os seguintes simulados ";
			if($executar_acoes == TRUE){
				$msg_afetados .= "foram";
			}else{
				$msg_afetados .= "serão";
			}
			$msg_afetados .= " afetados por esta conversão:<br>";

			$todas_questoes = [];
			foreach ($simulados_afetados as $simulado_id => $simulados_questoes) {

				if($executar_acoes == TRUE){
					/**
					 * EC-116, Podio 2905: Foi solicitado deixar o simulado inconsistente somente manualmente
					 */
					//self::tornar_simulado_inconsistente($simulado_id, FALSE);
				}

				$simulado = $this->simulado_model->get_by_id($simulado_id);

				$questoes = [];
				foreach ($simulados_questoes as $simulado_questao) {
					array_push($questoes, $simulado_questao['que_id']);
					array_push($todas_questoes, $simulado_questao['que_id']);

					//Se a disciplina nova não existe no simulado então deve-se remover do simulado
					if($executar_acoes == TRUE && $disciplina_nova){
						$disciplina_nova_no_simulado = $this->simulado_model->get_disciplinas_simulado($simulado_id, $disciplina_nova);
						if(!$disciplina_nova_no_simulado){
							$this->simulado_model->excluir_questao_de_simulado($simulado_id, $simulado_questao['que_id']);
						}

					}
				}

				$msg_afetados .= "<br><strong>". $simulado['sim_nome'] . ":</strong> " . implode(", ", $questoes);
			}

			$msg_afetados .= "<br><br><strong>Todas as questões:</strong> " . implode(", ", $todas_questoes);
		}

		return $msg_afetados;

	}

	public function excluir_conversao($assunto_antigo_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->assunto_model->excluir_conversao($assunto_antigo_id);

		$this->session->set_flashdata ( 'sucesso', 'Conversão excluída com sucesso!' );
		redirect ( get_conversao_assuntos_url() . '/');
	}

	/**
	 * PROVA
	 */

	/**
	 * Lista as provas existentes
	 *
	 * @param int $offset Offset da página a ser renderizada
	 */

	public function listar_provas($offset = 0)
	{
		KLoader::helper("FiltroHelper");

		// regras de acesso
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		// inclusão de bibliotecas js
		$data['include_data_tables'] = true;
		$data['include_chosen'] = true;

		// Inicialização da sessão
		if(!$_SESSION['frm_listar_questoes']) {
			$_SESSION['frm_listar_questoes'] = [
				'q_prova' => null,
				'q_anos' => null,
				'q_orgaos' => null,
				'q_bancas' => null,
				'q_areas_atuacao' => null
			];
		}

		// Ação pós clique no botão Filtrar
		if($this->input->post('filtrar')) {
			$_SESSION['frm_listar_questoes']['q_prova'] = trim($this->input->post('q'));
			$_SESSION['frm_listar_questoes']['q_anos'] = $this->input->post('q_anos');
			$_SESSION['frm_listar_questoes']['q_orgaos'] = $this->input->post('q_orgaos');
			$_SESSION['frm_listar_questoes']['q_bancas'] = $this->input->post('q_bancas');
			$_SESSION['frm_listar_questoes']['q_areas_atuacao'] = $this->input->post('q_areas_atuacao');
			$offset = 0;
		}

		// Montagem dos parâmetros da busca
		$filtros = [
			'nome' => $_SESSION['frm_listar_questoes']['q_prova'],
			'anos' => $_SESSION['frm_listar_questoes']['q_anos'],
			'orgaos' => $_SESSION['frm_listar_questoes']['q_orgaos'],
			'bancas' => $_SESSION['frm_listar_questoes']['q_bancas'],
			'areas_atuacao' => $_SESSION['frm_listar_questoes']['q_areas_atuacao'],
		];

		// Chamada das buscas
		$total = $this->prova_model->contar($filtros);
		$provas = $this->prova_model->buscar($offset, $filtros);
		
		foreach ($provas as &$prova) {
		    $prova["areas_atuacao"] = $this->prova_model->get_areas_atuacao_prova_str($prova["pro_id"]);
		}

		// Montagem da paginação
		$config = pagination_config(get_listar_provas_url(), $total, PROVAS_POR_PAGINA);
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();

		// Montagem dos dados da tabela de resultado
		$data['filtros'] = $filtros;
		$data['total'] = numero_inteiro($total);
		$data['tem_filtro_aplicado'] = FiltroHelper::tem_filtro_aplicado($filtros);
		$data['thead_array'] = array('Nome','Ano', 'Instituição', 'Banca', 'Áreas de Atuação', 'Qtde de Questões', 'Ações');
		$data['tbody_array'] = get_tabela_provas($provas);
		$data['tfoot_array'] = $data['thead_array'];

		// Montagem dos combos para o filtro
		$data["combo_anos"] = get_ano_multiselect($this->prova_model->listar_anos());
		$data["combo_orgaos"] = get_orgao_multiselect($this->orgao_model->listar_todas());
		$data["combo_bancas"] = get_banca_multiselect($this->banca_model->listar_todas());
		$data["combo_areas_atuacao"] = get_area_atuacao_multiselect($this->area_atuacao_model->listar_todas());

		// Chamada das views
		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_prova_view_url (), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Duplica prova
	 *
	 * @author Ivan Duarte <ivan.duarte@keydea.com.br>
	 * @param int $prova_id numero id da prova
	 * @since K5
	 */
	public function duplicar_prova($prova_id)
	{
		// regras de acesso
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$prova = $this->prova_model->get_by_id ($prova_id);
		$prova['car_ids'] = get_cargos_ids ( $this->prova_model->get_cargos_prova ( $prova_id ) );
		$prova['arf_ids'] = get_areas_formacao_ids($this->prova_model->get_areas_formacao_prova ( $prova_id ));
		$prova['ara_ids'] = get_areas_atuacao_ids($this->prova_model->get_areas_atuacao_prova ( $prova_id ));
		$filtros = array('pro_ids' => array('0' => $prova_id));
		$questoes = $this->questao_model->listar_por_filtros($filtros);
		$prova['pro_id'] = NULL;
		$prova['pro_nome'] = $prova['pro_nome']." (Cópia)";
		$id_prova = $this->prova_model->salvar ($prova);
		$nova_prova = $this->prova_model->get_by_id ($id_prova);
		if($questoes)
		{
			foreach ($questoes as $questao)
			{
				$this->questao_model->salvar_prova_em_questao($nova_prova, $questao);
			}
		}
		set_mensagem(SUCESSO, "Prova duplicada com sucesso.");
		redirect ( get_editar_prova_url (array('pro_id' => $id_prova)));
	}

	/**
	 * Salva e edita prova
	 */
	public function editar_prova($prova_id = null) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->load->helper("prova");

		if ($this->input->post ( 'salvar' )) {
			$this->form_validation->set_error_delimiters ( '<span>', '</span>' );
			$this->form_validation->set_rules ( 'pro_nome', 'Nome', 'required|max_length[100]' );
			$this->form_validation->set_rules ( 'pro_ano', 'Ano', 'required' );
			$this->form_validation->set_rules ( 'pro_tipo', 'Modalidade', 'required' );
			$this->form_validation->set_rules ( 'org_id', 'Órgão', 'required|callback_check_default' );
			$this->form_validation->set_rules ( 'ban_id', 'Banca', 'required|callback_check_default' );
			$this->form_validation->set_message ( 'required', '%s é obrigatório.' );
			$this->form_validation->set_message ( 'max_length', '%s não pode passar de 100 caracteres.' );
			$this->form_validation->set_message ( 'check_default', 'Você precisa escolher uma opção' );
			$this->form_validation->set_message('edit_unique', '%s já existe no cadastro.');

			if ($this->form_validation->run () === true) {
				$data ['pro_nome'] = $this->input->post ( 'pro_nome' );
				$data ['pro_ano'] = $this->input->post ( 'pro_ano' );
				$data ['pro_tipo'] = $this->input->post ( 'pro_tipo' );
				$data ['org_id'] = $this->input->post ( 'org_id' );
				$data ['ban_id'] = $this->input->post ( 'ban_id' );

				if (count ( $this->input->post ( 'car_ids' ) ) > 0)
					$data ['car_ids'] = $this->input->post ( 'car_ids' );

				if (count ( $this->input->post ( 'arf_ids' ) ) > 0)
					$data ['arf_ids'] = $this->input->post ( 'arf_ids' );

				if (count ( $this->input->post ( 'ara_ids' ) ) > 0)
					$data ['ara_ids'] = $this->input->post ( 'ara_ids' );

				if ($this->input->post ( 'esc_id' ) > 0)
					$data ['esc_id'] = $this->input->post ( 'esc_id' );

				if (! empty ( $prova_id )) {
					$data ['pro_id'] = $prova_id;
					$this->prova_model->atualizar ( $data );
					set_mensagem(SUCESSO, 'Prova atualizada com sucesso!' );
				} else {
					$prova = $this->prova_model->get_by_nome($data ['ass_nome']);
					$prova_id = $this->prova_model->salvar ( $data );
					set_mensagem(SUCESSO, 'Prova inserida com sucesso!' );
				}

				//Atualiza o ano das questões associadas na tabela de questoes
				$this->questao_model->atualizar_ano_questoes($prova_id, null);

				redirect ( get_editar_prova_url (array('pro_id' => $prova_id)) . '/' );
			}
			else {
				set_mensagem(ERRO, 'Erro ao tentar salvar os dados da prova!');
			}
		}

		if ($this->input->post ( 'enviarProva' )) {
			$prova = $this->prova_model->get_by_id ( $prova_id );
			$nome_prova = get_nome_arquivo_prova( "prova", $prova );
			if(!upload_pdf($nome_prova, 'provas','pdf_prova', $this))
			{
				redirect ( get_editar_prova_url (array('pro_id' => $prova_id)) . '/' );
			}
		}

		if ($this->input->post ( 'enviarGabarito' )) {
			$prova = $this->prova_model->get_by_id ( $prova_id );
			$nome_gabarito = get_nome_arquivo_prova( "gabarito", $prova );
			if(!upload_pdf($nome_gabarito, 'gabaritos','pdf_gabarito', $this))
			{
				redirect ( get_editar_prova_url (array('pro_id' => $prova_id)) . '/' );
			}
		}

		if ($this->input->post ( 'enviarEdital' )) {
			$prova = $this->prova_model->get_by_id ( $prova_id );
			$nome_edital = get_nome_arquivo_prova( "edital", $prova );
			if(!upload_pdf($nome_edital, 'editais','pdf_edital', $this))
			{
				redirect ( get_editar_prova_url (array('pro_id' => $prova_id)) . '/' );
			}
		}

		$data ['include_chosen'] = true;
		$data ['include_admin_editar_prova'] = true;
		$data ['banca_selecionada'] = OPCAO_ZERO;
		$data ['orgao_selecionado'] = OPCAO_ZERO;
		$data ['escolaridade_selecionada'] = OPCAO_ZERO;
		$data ['cargos_selecionados'] = array ();
		$data ['formacoes_selecionadas'] = array ();
		$data ['atuacoes_selecionadas'] = array ();
		$data ['prova'] = array ();
		$data ['modalidades'] = get_modalidades_combo ();
		$data ['bancas'] = get_banca_combo ( $this->banca_model->listar_todas () );
		$data ['orgaos'] = get_orgao_combo ( $this->orgao_model->listar_todas () );
		$data ['cargos'] = get_cargo_multiselect ( $this->cargo_model->listar_todas () );
		$data ['areas_formacao'] = get_area_formacao_multiselect ( $this->area_formacao_model->listar_todas () );
		$data ['areas_atuacao'] = get_area_atuacao_multiselect ( $this->area_atuacao_model->listar_todas () );
		$data ['escolaridades'] = get_escolaridade_combo ();

		if (! is_null ( $prova_id )) {

			$data ['prova'] = $this->prova_model->get_by_id ( $prova_id );
			$data ['banca_selecionada'] = $data ['prova'] ['ban_id'];
			$data ['orgao_selecionado'] = $data ['prova'] ['org_id'];
			$data ['modalidade_selecionada'] = $data ['prova'] ['pro_tipo'];
			$data ['cargos_selecionados'] = get_cargos_ids ( $this->prova_model->get_cargos_prova ( $prova_id ) );
			$data ['formacoes_selecionadas'] = get_areas_formacao_ids($this->prova_model->get_areas_formacao_prova ( $prova_id ));
			$data ['atuacoes_selecionadas'] = get_areas_atuacao_ids($this->prova_model->get_areas_atuacao_prova ( $prova_id ));
			$data ['has_prova_pdf'] = $this->prova_model->has_prova_pdf ( $data ['prova'] );
			$data ['has_gabarito_pdf'] = $this->prova_model->has_gabarito_pdf ( $data ['prova'] );
			$data ['has_edital_pdf'] = $this->prova_model->has_edital_pdf ( $data ['prova'] );
			if (! is_null ( $data ['prova'] ['esc_id'] ))
				$data ['escolaridade_selecionada'] = $data ['prova'] ['esc_id'];
		}
		else {
			$data['prova'] = array(
				'pro_nome' => $this->input->post('pro_nome') ? $this->input->post('pro_nome') : '',
				'pro_ano' => $this->input->post('pro_ano') ? $this->input->post('pro_ano') : '',
				'pro_tipo' => $this->input->post('pro_tipo') ? $this->input->post('pro_tipo') : ''
			);
			$data['banca_selecionada'] = $this->input->post('ban_id') ?  $this->input->post('ban_id') : null;
			$data['orgao_selecionado'] = $this->input->post('org_id') ?  $this->input->post('org_id') : null;
			$data['modalidade_selecionada'] = $this->input->post('pro_tipo') ?  $this->input->post('pro_tipo') : null;
			$data['cargos_selecionados'] = $this->input->post('car_ids') ?  $this->input->post('car_ids') : null;
			$data['escolaridade_selecionada'] = $this->input->post('esc_id') ? $this->input->post('esc_id') : null;
			$data['formacoes_selecionadas'] = $this->input->post('arf_ids') ? $this->input->post('arf_ids') : null;
			$data['atuacoes_selecionadas'] = $this->input->post('ara_ids') ? $this->input->post('ara_ids') : null;
		}

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_editar_prova_view_url (), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Exclui prova
	 */
	public function excluir_prova($prova_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->prova_model->excluir ( $prova_id );
		$this->session->set_flashdata ( 'sucesso', 'Prova excluída com sucesso!' );
		redirect ( get_listar_provas_url () . '/');
	}

	public function excluir_prova_pdf($prova_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->load->helper("prova");

		unlink(get_prova_pdf_url( get_nome_arquivo_prova ( "prova", $prova_id ) ) );
		redirect ( get_editar_prova_url (array('pro_id' => $prova_id)) . '/' );
	}

	public function excluir_gabarito_pdf($prova_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->load->helper("prova");

		unlink(get_gabarito_pdf_url(  get_nome_arquivo_prova ( "gabarito", $prova_id )  ));
		redirect ( get_editar_prova_url (array('pro_id' => $prova_id)) . '/' );
	}

	public function excluir_edital_pdf($prova_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->load->helper("prova");

		unlink(get_edital_pdf_url(  get_nome_arquivo_prova ( "edital", $prova_id )  ));
		redirect ( get_editar_prova_url (array('pro_id' => $prova_id)) . '/' );
	}

	/**
	 * QUESTÃO
	 */

	/**
	 * Lista as questoes existentes
	 */
	public function listar_questoes() {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$filtro = array();
		$data['include_chosen'] = true;
		$data['include_admin_listar_questoes'] = true;
		$data['include_data_tables'] = true;
		$data['include_icheck'] = true;
		$data['include_datepicker'] = true;
		$data['include_mask'] = true;
		$data['include_jstree'] = true;

		$data['status'] = get_status_questao_combo();
		$data['palavra_chave'] = '';
		$data['que_qcon_id'] = '';
		init_combos($data, FALSE);

		if($this->input->get('buscar')) {
			if($this->input->get('status_ids'))
			{
				$filtro['status_ids'] = $this->input->get('status_ids');
			}
			if ($this->input->post('filtro_favoritas')) {
				$filtro['favoritas'] = $this->input->get('filtro_favoritas');
			}
			if ($this->input->get('data_inicio')) {
				$filtro['data_inicio'] = $this->input->get('data_inicio');
			}
			if ($this->input->get('data_fim')) {
				$filtro['data_fim'] = $this->input->get('data_fim');
			}
			if ($this->input->get('busca')) {
				$filtro['busca'] = $this->input->get('busca');
			}

			adicionar_filtro($filtro, $data, $this, FALSE);

			$data['apenas_assunto_pai'] = $this->input->get("apenas_assunto_pai");
			if(!$data['apenas_assunto_pai'])
			{
				adicionar_assuntos_filhos($filtro);
			}

			//$_SESSION['filtro_listar'] = serialize($filtro);
		}

		if($filtro['dis_ids']) {
			$data['assuntos'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplinas($filtro['dis_ids']));
		}

		$data['busca'] = $filtro['busca'] ?: null;
		$data['bancas_selecionadas'] = $filtro['ban_ids'] ?: null;
		$data['orgaos_selecionados'] = $filtro['org_ids'] ?: null;
		$data['cargos_selecionados'] = $filtro['car_ids'] ?: null;
		$data['anos_selecionados'] = $filtro['pro_anos'] ?: null;
		$data['escolaridades_selecionadas'] = $filtro['esc_ids'] ?: null;
		$data['formacoes_selecionadas'] = $filtro['arf_ids'] ?: null;
		$data['disciplinas_selecionadas'] = $filtro['dis_ids'] ?: null;
		$data['assuntos_selecionados'] = $filtro['ass_ids'] ?: null;
		$data['atuacoes_selecionadas'] = $filtro['ara_ids'] ?: null;
		$data['tipos_selecionados'] = $filtro['que_tipos'] ?: null;
		$data['dificuldades_selecionadas'] = $filtro['que_dificuldades'] ?: null;
		$data['simulados_selecionados'] = $filtro['sim_ids'] ?: null;
		$data['statuses_selecionadas'] = $filtro['que_statuses'] ?: null;
		$data['status_selecionados'] = $filtro['status_ids'] ?: null;
		$data['filtro_favoritas'] = $filtro['favoritas'] ?: null;
		$data['data_inicio'] = $filtro['data_inicio']?: NULL;
		$data['data_fim'] = $filtro['data_fim']?: NULL;

		$data['array_multiselect'] = array(
				array('dis_ids', $data['disciplinas'], $data['disciplinas_selecionadas']),
				array('ass_ids', $data['assuntos'], $data['assuntos_selecionados']),
				array('ban_ids', $data['bancas'], $data['bancas_selecionadas']),
				array('org_ids', $data['orgaos'], $data['orgaos_selecionados']),
				array('car_ids', $data['cargos'], $data['cargos_selecionados']),
				array('pro_anos', $data['anos'], $data['anos_selecionados']),
				array('esc_ids', $data['escolaridades'], $data['escolaridades_selecionadas']),
				array('arf_ids', $data['formacoes'], $data['formacoes_selecionadas']),
				array('ara_ids', $data['atuacoes'], $data['atuacoes_selecionadas']),
				array('que_statuses', $data['statuses'], $data['statuses_selecionadas']),
				array('que_tipos', $data['tipos'], $data['tipos_selecionados']),
				array('que_dificuldades', $data['dificuldades'], $data['dificuldades_selecionadas']),
				array('sim_ids', $data['simulados'], $data['simulados_selecionados']),
				array('status_ids', $data['status'], $data['status_selecionados']),
		);

		preencher_formulario_filtro($filtro, $data);

		// $filtro['incluir_disciplinas_inativas'] = 1; // Inclui questoes com disciplinas inativas
		$total = $this->questao_model->get_total_resultado($filtro, true);
		$data['filtro'] = $filtro;

		if($total > 0) {
			$questoes = $this->questao_model->listar_por_filtros($filtro, LIMIT_LISTAR_QUESTOES, null, false, true);

			$data['total'] = $total;
			$data['thead_array'] = array('Enunciado', 'Status', 'Ações');
			$data['tbody_array'] = get_tabela_questoes($questoes);
			$data['tfoot_array'] = $data['thead_array'];
		} else {
			$data['erro'] ='Não foram encontradas questões com este filtro.';
		}

		$query_string = http_build_query($_GET);
		if($query_string){
			$query_string = "?" . $query_string;
		}
		$data['query_string'] = $query_string;

// 		echo "<pre>";
// 		print_r($data);

		$data['filtros_selecionados'] = get_filtros_selecionados($filtro);
		$data['has_filtro'] = count($data['filtros_selecionados']) > 0 && !nenhum_filtro_aplicado($filtro);

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view ( get_listar_questoes_view_url(), $data );
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Salva e edita questão
	 */
	public function editar_questao($questao_id = null) {

		AcessoGrupoHelper::editar_questao($questao_id, ACESSO_NEGADO);

		$is_professor = !tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, REVISOR));
		$usuario_logado = get_usuario(get_current_user_id());

		if ($this->input->post ( 'salvar' )) {
			$this->form_validation->set_error_delimiters ( '<span>', '</span>' );
			$this->form_validation->set_rules ( 'que_enunciado', 'Enunciado', 'required' );
			$this->form_validation->set_rules ( 'dis_id', 'Disciplina', 'required|callback_check_default' );
			$this->form_validation->set_rules ( 'com_texto', 'Comentário', 'callback_check_comentario' );
			//$this->form_validation->set_rules ( 'que_procedencia', 'Procedência', 'required' );
			$this->form_validation->set_message ( 'required', '%s é obrigatório.' );
			$this->form_validation->set_message ( 'max_length', '%s não pode passar de 200 caracteres.' );
			$this->form_validation->set_message ( 'check_default', 'Você precisa escolher uma %s' );
			$this->form_validation->set_message ( 'check_comentario', 'Você precisa escolher um professor.' );
// 			if(is_null)$this->form_validation->set_rules ( 'que_resposta', 'Resposta', 'required|callback_check_default' );

			if( $this->input->post ( 'cvi_url' ) || $this->input->post ( 'cvi_user_id' ) ){
				$this->form_validation->set_rules ( 'cvi_url', 'Url do Comentário em Vídeo', 'required|callback_check_vimeo',
					array('required' => 'A URL do vídeo é obrigatória.', 'check_vimeo' => 'A URL informada não é válida.') );
				$this->form_validation->set_rules ( 'cvi_user_id', 'Professor do Comentário em Vídeo', 'required' );
				//$this->form_validation->set_message ( 'check_vimeo', 'A URL informada não é válida.' );
			}

			if($this->input->post ( 'que_tipo' ) == MULTIPLA_ESCOLHA || $this->input->post ( 'que_tipo' ) == MULTIPLA_ESCOLHA_5) {

				$opcoes = array(
						$this->input->post ( 'qop_01' ),
						$this->input->post ( 'qop_02' ),
						$this->input->post ( 'qop_03' ),
						$this->input->post ( 'qop_04' )
				);

				if($this->input->post ( 'que_tipo' ) == MULTIPLA_ESCOLHA_5){
					array_push($opcoes, $this->input->post ( 'qop_05' ));
				}

				$count = 0;
				foreach ($opcoes as $opcao){
					if(strlen(trim(sanitizar($opcao)))) {
						$count++;
					}
				}

				if($count != 5 && $this->input->post ( 'que_tipo' ) == MULTIPLA_ESCOLHA_5){
					$this->form_validation->set_rules ( 'que_tipo', 'Tipo', 'callback_check_opcao', array('check_opcao' => 'Para esse tipo de questão é necessário preencher as 5 opções.') );

				}elseif($count < 2){
					$this->form_validation->set_rules ( 'que_tipo', 'Tipo', 'callback_check_opcao', array('check_opcao' => 'Para esse tipo de questão é necessário ter, pelo menos, 2 opções preenchidas.') );

				}

			}

			if ($this->form_validation->run () === true) {
				$data['que_enunciado'] = $this->input->post ( 'que_enunciado' );
				$data['que_texto_base'] = $this->input->post ( 'que_texto_base' );
				$data['que_resposta'] = $this->input->post ( 'que_resposta' );
				$data['pro_ids'] = $this->input->post ( 'pro_ids' );
				$data['dis_id'] = $this->input->post ( 'dis_id' );
				$data['que_tipo'] = $this->input->post ( 'que_tipo' );
				$data['que_status'] = $this->input->post ( 'que_status' );
				$data['que_procedencia'] = $this->input->post( 'que_procedencia' );
				
				// Nas questão por professores esse campo não aparece
				if(null !== $this->input->post ( 'que_ativo' )) {
				    $data['que_ativo'] = $this->input->post ( 'que_ativo' );
				}

				if(empty($data ['que_procedencia'])){
					$data ['que_procedencia'] = null;
				}

				if($data['que_status'] != CANCELADA && !$data['que_resposta']) {
					set_mensagem_flash('erro', 'Resposta precisa ser preenchida');
				}
				else {
					$ass_ids = $this->input->post ( 'ass_ids' );

					if (count ( $ass_ids ) > 0)
						$data ['ass_ids'] = $ass_ids;

					if ($this->input->post ( 'que_dificuldade' ) > 0)
						$data ['que_dificuldade'] = $this->input->post ( 'que_dificuldade' );


					$data['opcoes'] = $opcoes;
					if (! empty ( $questao_id )) {
						$data ['que_id'] = $questao_id;
						$this->questao_model->atualizar ( $data );
						set_mensagem_flash('sucesso', 'Questão atualizada com sucesso!');
					} else {

						if($is_professor){
							$data['que_aprovacao'] = QUESTAO_EM_APROVACAO;
							$data['que_ativo'] = ATIVO;
						}else{
							$data['que_aprovacao'] = QUESTAO_APROVADA;
						}

						$data['usu_id'] = $usuario_logado->ID;

						$questao_id = $this->questao_model->salvar ( $data );
						set_mensagem_flash('sucesso', 'Questão inserida com sucesso!');
					}

					$com_texto = trim($this->input->post("com_texto"));
					if($com_texto != "" && $com_texto !="<p><br></p>") {
						$comentario = array(
							'com_texto' => $com_texto,
							'user_id' => $this->input->post("user_id"),
							'que_id' => $questao_id,
							'com_data_criacao' => date("Y-m-d H:i:s")
						);
						$this->comentario_model->salvar($comentario);

						$total = $this->comentario_model->contar_comentarios($questao_id);

						$this->questao_model->atualizar_numero_comentarios($questao_id, $total);
					}


					$data ['cvi_url'] = $this->input->post ( 'cvi_url' );
					$data ['cvi_user_id'] = $this->input->post( 'cvi_user_id' );

					$comentario_video = $this->comentario_model->get_comentario_video_por_questao($questao_id);

					$comentario_video['usu_id'] = $data ['cvi_user_id'];
					$comentario_video['cvi_url'] = $data ['cvi_url'];
					$comentario_video['que_id'] = $questao_id;

					//Só salva/atualiza se tiver url informada, a validação já deve ter sido feita
					if($comentario_video['cvi_url']){

						if($comentario_video['cvi_id']){
							$this->comentario_model->atualizar_comentario_video($comentario_video);
						}else{
							$this->comentario_model->salvar_comentario_video($comentario_video);
						}

					}elseif($comentario_video['cvi_id']){//Se não tem url tem que excluir caso já possua alguma informação no banco de dados
						$this->comentario_model->excluir_comentario_video($comentario_video);
					}
					
					$this->questao_model->atualizar_destaque_questao($questao_id);

					$questao = $this->questao_model->get_by_id($questao_id);
					
					//Invalida o cache de questões após cada alteração em questões
					memcached_remover_cache_questoes();

					redirect ( get_editar_questao_url ($questao) );
				}
			}
			else {
				set_mensagem_flash('erro', 'Um ou mais campos obrigatórios não foram preenchidos.');
			}
		}elseif ($this->input->post ( 'replicar' )) {

			$questao_replicar_id = $this->input->post('que_id');

			$questao_replicar = $this->questao_model->get_by_id($questao_replicar_id);

			$data['que_texto_base'] = $questao_replicar['que_texto_base'];
			$data['que_status'] = $questao_replicar['que_status'];
			$data['que_tipo'] = $questao_replicar['que_tipo'];
			$data['dis_id'] = $questao_replicar['dis_id'];
			
			/**
			 * Status removido a pedido do tícket B2836
			 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2836
			 */ 
			//$data['status_selecionado'] = $questao_replicar['que_status'];
			$data['situacao_selecionada'] = $questao_replicar['que_ativo'];
			$data['procedencia_selecionada'] = $questao_replicar['que_procedencia'];
			$data['tipo_selecionado'] = $questao_replicar['que_tipo'];
			$data['provas_selecionadas'] = get_provas_ids ( $this->questao_model->get_provas_questao ( $questao_replicar_id ) );

			
			if(!is_null($data['dis_id'] ))
			{
				$dis_id = $data['dis_id'];
				$data['assuntos'] = get_assunto_multiselect ( $this->assunto_model->listar_por_disciplina ( $dis_id ), TRUE );
				$data['disciplina_selecionada'] = $data['dis_id'];
			}

		}else{
			$data['tipo_selecionado'] = MULTIPLA_ESCOLHA_5;
			$data['status_selecionado'] = NORMAL;
			$data['provas_selecionadas'] = [];
			$data['disciplina_selecionada'] = OPCAO_ZERO;
			$data['assuntos'] = [];
		}

		$data['include_validade'] = true;
		$data['include_summernote'] = true;
		$data['include_chosen'] = true;
		$data['include_admin_editar_questao'] = true;
		$data['include_jquery_chained'] = true;
		$data['include_cropper'] = true;
		$data['include_jstree'] = true;
		$data['include_jquery_ui'] = true;
		$data['include_datepicker'] = true;

		$data['dificuldade_selecionada'] = OPCAO_ZERO;
		$data['assuntos_selecionados'] = [];
		$data['resposta_selecionada'] = OPCAO_ZERO;
		$data['questao'] = array ();
		$data['provas'] = get_prova_combo( $this->prova_model->listar_todas () );
		$data['tipos'] = get_tipo_questao_multiselect();
		$data['disciplinas'] = get_disciplina_combo($this->disciplina_model->listar_todas());
		$data['dificuldades'] = get_dificuldade_questao_combo();
		$data['statuses'] = get_status_questao_combo();
		$data['situacoes'] = get_situacoes_combo();
		$data['procedencias'] = get_procedencia_questao_combo();

		if(!$is_professor){
			$data['professores'] = get_professores_combo(get_professores(FALSE));
		}else{
			$data['professores'] = get_professores_combo(array($usuario_logado));
		}

		if (! is_null ( $questao_id )) {
			$data['professor_que_comentou'] = $this->comentario_model->get_professor_que_comentou($questao_id);
			$data['questao'] = $this->questao_model->get_by_id ( $questao_id );
			$data['destaque'] = $this->comentario_model->get_destaque($questao_id);
			$data['resposta_selecionada'] = $data ['questao'] ['que_resposta'];
			$data['dificuldade_selecionada'] = $data ['questao'] ['que_dificuldade'];
			$data['tipo_selecionado'] = $data['questao'] ['que_tipo'];
			$data['provas_selecionadas'] = get_provas_ids ( $this->questao_model->get_provas_questao ( $questao_id ) );

			$data['status_selecionado'] = $data ['questao'] ['que_status'];
			$data['situacao_selecionada'] = $data ['questao'] ['que_ativo'];
			$data['procedencia_selecionada'] = $data ['questao'] ['que_procedencia'];

			$comentario_video = $this->comentario_model->get_comentario_video_por_questao($questao_id);

			$data ['cvi_url'] = $comentario_video['cvi_url'];
			$data ['cvi_user_id'] = $comentario_video['usu_id'];

			if(!is_null($data ['questao'] ['dis_id'] ))
			{

				if(isset($_POST['dis_id'])) {
					$dis_id = $_POST['dis_id'];
				}
				else {
					$dis_id = $data ['questao'] ['dis_id'];
				}

				$data['assuntos'] = get_assunto_multiselect ( $this->assunto_model->listar_por_disciplina ( $dis_id ), TRUE );
				$data['disciplina_selecionada'] = $data ['questao'] ['dis_id'];
				$data['assuntos_selecionados'] = get_assuntos_ids ( $this->questao_model->get_assuntos_questao ( $questao_id ) );
			}

			$questoes_opcoes = $this->questao_model->listar_opcoes($questao_id);
			$count = 1;
			foreach ($questoes_opcoes as $opcao) {
				$data['qop_0' . $count] = $opcao['qop_texto'];
				$count++;
			}
		}
		$data['respostas'] = get_resposta_combo($data ['tipo_selecionado']);
		$data['com_texto'] = $this->input->post ( 'com_texto' );
		$data['imagens_nao_tratadas'] = $this->questao_model->get_questao_imagens($questao_id);

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view ( get_editar_questao_view_url (), $data );
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function excluir_comentario_questao($questao_id, $comentario_id)
	{
		$this->comentario_model->excluir($comentario_id);

		$total = $this->comentario_model->contar_comentarios($questao_id);

		$this->questao_model->atualizar_numero_comentarios($questao_id, $total);
		$this->questao_model->atualizar_destaque_questao($questao_id);

		// verifica existe outro comentário de professor na questão
		$mensagem = "";
		if($this->comentario_model->get_destaque($questao_id)) {
			$mensagem = "<br><strong>Atenção: </strong> Outro comentário de professor foi encontrado na questão.";
		}

		//Invalida o cache de questões após cada alteração em questões
		memcached_remover_cache_questoes();

		// retorna para tela de edição
		set_mensagem_flash("sucesso", "Comentário de questão removido com sucesso. $mensagem");
		redirect(get_editar_questao_url($questao_id));
	}

	public function historico_questao($questao_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$data['include_data_tables'] = true;
		$data['data_table_order'] = '[[ 0, "desc" ]]';
		$data['data_table_length'] = 50;
		$data['questao'] = $this->questao_model->get_by_id($questao_id);

		$historicos = $this->questao_model->listar_historicos($questao_id);

		$data['thead_array'] = array('Data','Usuário', 'Campo', 'Antigo Valor', 'Novo Valor');
		$data['tbody_array'] = get_tabela_historico($historicos);
		$data['tfoot_array'] = $data['thead_array'];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view ( get_historico_questao_view_url (), $data );
		$this->load->view(get_admin_footer_view_url(), $data);
	}
	/**
	 * Contagem de Questões
	 */
	public function contagem_questoes()
	{
		if($this->input->post()) {

			$linha = $this->input->post('linha');
			$coluna = $this->input->post('coluna');

			$data['matriz'] = $this->questao_model->contar_questoes($linha, $coluna, array('314','315'), NULL);
		}

		$data['entidades_options'] = get_entidades_questao_combo();

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_contagem_questoes_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}


	/**
	 * Exclui questão
	 */
	public function excluir_questao($questao_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$this->questao_model->excluir ( $questao_id );
		$this->session->set_flashdata ( 'sucesso', 'questao excluída com sucesso!' );

		//Invalida o cache de questões após cada alteração em questões
		memcached_remover_cache_questoes();

		redirect ( get_listar_questoes_url() . '/');
	}

	public function get_modal_adicionar_questao($questao_id) {
		$questao = $this->questao_model->get_by_id($questao_id);
		$simulados = get_simulado_combo($this->simulado_model->listar());
		echo
			"<div class='modal-header'>
				<button type='button' class='close' data-dismiss='modal'>&times;</button>
				<h4 class='modal-title'>Adicionar Questão a Simulado</h4>
			</div>
			<div class='modal-body'>".
				form_open(get_adicionar_questao_url($questao), 'role="form" class=""').
					"<div class='form-group'>".
						form_dropdown('sim_id', $simulados, "", 'id="sim_id" class="form-control m-b"').
					"</div>
					<div class=''>".
						get_admin_botao_submit("Adicionar", "adicionar").
					"</div>".
				form_close().
			"</div>";
	}

	public function adicionar_questao($questao_id) {
		if ($this->input->post( 'adicionar' )) {
			$this->form_validation->set_error_delimiters( '<span>', '</span>' );
			$this->form_validation->set_rules( 'sim_id', 'Simulado', 'required|callback_check_default' );
			if ($this->form_validation->run() === true) {
				$questao = $this->questao_model->get_by_id($questao_id);
				$simulado_id = $this->input->post( 'sim_id' );
				$ordem = $this->simulado_model->get_proxima_ordem_questao($simulado_id);
				$this->simulado_model->salvar_questoes_em_simulado($simulado_id, array($questao), $ordem);
				$this->session->set_flashdata ( 'sucesso', 'questao incluída com sucesso!' );
			}
		}
		redirect ( get_listar_questoes_url() . '/');
	}

	public function contar_questoes()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$data['nome'] = "contagem-de-questoes";

		if($this->input->post('gerar')) {
			$this->form_validation->set_error_delimiters ('<span>', '</span>');
			$this->form_validation->set_rules('linha', 'Grandeza da Linha', "greater_than[0]");
			$this->form_validation->set_rules('coluna', 'Grandeza da Coluna', "greater_than[0]");
			$this->form_validation->set_rules('nome', 'Nome do Arquivo', "required");
			$this->form_validation->set_message('required', 'Esse campo é obrigatório.');
			$this->form_validation->set_message('greater_than', 'É necessário selecionar uma opção nesse campo.');
			$this->form_validation->set_message('matches', 'A Grandeza da Coluna não pode ser igual à Grandeza da Linha.');

			$data['nome'] = $this->input->post('nome') ?: NULL;
			$data['linha_selecionada'] = $this->input->post('linha') ?: NULL;
			$data['coluna_selecionada'] = $this->input->post('coluna') ?: NULL;

			$data['filtro'] = [
				'dis_ids' => $this->input->post('dis_ids'),
				'ban_ids' => $this->input->post('ban_ids'),
				'pro_anos' => $this->input->post('pro_anos'),
				'esc_ids' => $this->input->post('esc_ids'),
				'ara_ids' => $this->input->post('ara_ids'),
				'que_statuses' => $this->input->post('que_statuses'),
				'que_tipos' => $this->input->post('que_tipos')
			];

			if($this->form_validation->run() === TRUE) {
				$linhas = self::get_grandeza_valores($data['linha_selecionada']);
				$colunas = self::get_grandeza_valores($data['coluna_selecionada']);

				$linha_campo = self::get_grandeza_campo($data['linha_selecionada']);
				$coluna_campo = self::get_grandeza_campo($data['coluna_selecionada']);

				$data['filtro'] = array_filter($data['filtro']);

				// preenche primeira celula (0, 0) com string vazia
				$tabela[0][0] = "";

				// preenche cabecalho de colunas
				$j = 1;
				foreach ($colunas as $coluna_key => $coluna_valor) {
					if($coluna_key <= 0) continue;

					$tabela[0][$j] = $coluna_valor;
					$j++;
				}

				// preenche cabecalho de linhas
				$i = 1;
				foreach ($linhas as $linha_key => $linha_valor) {
					if($linha_key <= 0) continue;

					$tabela[$i][0] = $linha_valor;
					$i++;
				}

				// preenche conteudo da tabela
				$i = 1;
				foreach ($linhas as $linha_key => $linha_valor) {
					if($linha_key <= 0) continue;

					$j = 1;
					foreach ($colunas as $coluna_key => $coluna_valor) {

						set_time_limit(360);

						if($coluna_key <= 0) continue;

						$tabela[$i][$j] = $this->questao_model->contar_questoes($linha_campo, $linha_key, $coluna_campo, $coluna_key, $data['filtro']);

						$j++;
					}

					$i++;
				}

				exportar_xls($tabela, $data['nome']);
			}
		}

		$data['include_chosen'] = TRUE;

		init_combos($data);

		$data['linhas_combo'] = get_grandezas_combo("");
		$data['colunas_combo'] = get_grandezas_combo("");

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_contar_questoes_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	private function get_grandeza_valores($grandeza)
	{
		switch ($grandeza) {
			case CAMPO_DISCIPLINA:
				return get_disciplina_multiselect($this->disciplina_model->listar_todas());
				break;
			case CAMPO_BANCA:
				return get_banca_multiselect($this->banca_model->listar_todas());
				break;
			case CAMPO_ANO:
				return get_ano_multiselect($this->prova_model->listar_anos());
				break;
			case CAMPO_ESCOLARIDADE:
				return get_escolaridade_multiselect();
				break;
			case CAMPO_AREA_ATUACAO:
				return get_area_atuacao_multiselect($this->area_atuacao_model->listar_todas());
				break;
			case CAMPO_STATUS:
				return get_status_questao_multiselect();
				break;
			case CAMPO_TIPO:
				return get_tipo_questao_multiselect();
				break;
			case CAMPO_COMENTARIOS_PROFESSORES:
				return get_professores_multiselect(get_professores());
				break;
			case CAMPO_GABARITO:
				return [COM_RESPOSTA => 'Com gabarito', SEM_RESPOSTA => 'Sem gabarito'];
				break;
			case CAMPO_SITUACAO:
				return get_situacoes_combo();
				break;
		}
	}

	private function get_grandeza_campo($grandeza)
	{
		switch ($grandeza) {
			case CAMPO_DISCIPLINA: return 'd.dis_id'; break;
			case CAMPO_BANCA: return 'b.ban_id'; break;
			case CAMPO_ANO:	return 'p.pro_ano'; break;
			case CAMPO_ESCOLARIDADE: return 'p.esc_id';
			case CAMPO_AREA_ATUACAO: return 'pa.ara_id'; break;
			case CAMPO_STATUS: return 'q.que_status'; break;
			case CAMPO_TIPO: return 'q.que_tipo'; break;
			case CAMPO_COMENTARIOS_PROFESSORES: return 'c.user_id'; break;
			case CAMPO_GABARITO: return 'q.que_resposta'; break;
			case CAMPO_SITUACAO: return 'q.que_ativo'; break;
		}
	}


	public function get_que_resposta_ajax() {
		$tipo = $this->input->get('que_tipo');
		$respostas = get_resposta_combo($tipo);
		$string = '';
		$first = 1;
		foreach($respostas as $key => $value) {
			if($first == 1)
				$string = '[ [ "' . $key . '", "' . $value . '" ]';
			else
				$string .= ', [ "' . $key . '", "' . $value . '" ]';

			$first = 0;
		}
		$string .= ' ]';
		echo $string;
	}

	/**
	 * ÁREA DE ATUAÇÃO
	 */

	/**
	 * Lista as areas de formação existentes
	 */

	public function listar_areas_formacao()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$data['include_data_tables'] = true;
		$areas_formacao = $this->area_formacao_model->listar_todas();

		$data['thead_array'] = array('Nome', 'Ações');
		$data['tbody_array'] = get_tabela_areas_formacao($areas_formacao);
		$data['tfoot_array'] = $data['thead_array'];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_areas_formacao_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Salva e edita área de formação
	 */
	public function editar_area_formacao($area_formacao_id = null) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		if ($this->input->post ( 'salvar' )) {
			$original = $this->area_formacao_model->get_by_nome($this->input->post('arf_nome'));
			$regra_unique = count($original) > 0 ? "|edit_unique[areas_formacao.arf_nome.{$original['arf_nome']}]" : "";

			$this->form_validation->set_error_delimiters ( '<span>', '</span>' );
			$this->form_validation->set_rules ( 'arf_nome', 'Nome', "required|max_length[50]$regra_unique" );
			$this->form_validation->set_message ( 'required', '%s é obrigatório.' );
			$this->form_validation->set_message ( 'max_length', '%s não pode passar de 50 caracteres.' );
			$this->form_validation->set_message('edit_unique', '%s já existe no cadastro.');

			if ($this->form_validation->run () === true) {
				$data ['arf_nome'] = $this->input->post ( 'arf_nome' );
				if (! empty ( $area_formacao_id )) {
					$data ['arf_id'] = $area_formacao_id;
					$this->area_formacao_model->atualizar ( $data );
					$this->session->set_flashdata ( 'sucesso', 'Área de formação atualizada com sucesso!' );
				} else {
					$area = $this->area_formacao_model->get_by_nome($data ['arf_nome']);
					$this->area_formacao_model->salvar ( $data );
					$this->session->set_flashdata ( 'sucesso', 'Área de formação inserida com sucesso!' );
				}
				redirect ( get_listar_areas_formacao_url () . '/' );
			}
		}


		$data['area_formacao'] = array();
		if(!is_null($area_formacao_id))
			$data['area_formacao'] = $this->area_formacao_model->get_by_id($area_formacao_id);

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_editar_area_formacao_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Exclui área de formação
	 */
	public function excluir_area_formacao($area_formacao_id) {
		tem_acesso(array(
				ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$this->area_formacao_model->excluir ( $area_formacao_id );
		$this->session->set_flashdata ( 'sucesso', 'Área de formação excluída com sucesso!' );
		redirect ( get_listar_areas_formacao_url () . '/');
	}

	/**
	 * ÁREA DE ATUAÇÃO
	 */


	/**
	 * Lista as areas de atuação existentes
	 */

	public function listar_areas_atuacao()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$data['include_data_tables'] = true;
		$areas_atuacao = $this->area_atuacao_model->listar_todas();

		$data['thead_array'] = array('Nome', 'Ações');
		$data['tbody_array'] = get_tabela_areas_atuacao($areas_atuacao);
		$data['tfoot_array'] = $data['thead_array'];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_areas_atuacao_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Salva e edita área de atuação
	 */
	public function editar_area_atuacao($area_atuacao_id = null) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		if ($this->input->post ( 'salvar' )) {
			$original = $this->area_atuacao_model->get_by_nome($this->input->post('ara_nome'));
			$regra_unique = count($original) > 0 ? "|edit_unique[areas_atuacao.ara_nome.{$original['ara_nome']}]" : "";

			$this->form_validation->set_error_delimiters ( '<span>', '</span>' );
			$this->form_validation->set_rules ( 'ara_nome', 'Nome', "required|max_length[100]$regra_unique" );
			$this->form_validation->set_message ( 'required', '%s é obrigatório.' );
			$this->form_validation->set_message ( 'max_length', '%s não pode passar de 100 caracteres.' );
			$this->form_validation->set_message('edit_unique', '%s já existe no cadastro.');

			if ($this->form_validation->run () === true) {
				$data ['ara_nome'] = $this->input->post ( 'ara_nome' );
				if (! empty ( $area_atuacao_id )) {
					$data ['ara_id'] = $area_atuacao_id;
					$this->area_atuacao_model->atualizar ( $data );
					$this->session->set_flashdata ( 'sucesso', 'Área de atuação atualizada com sucesso!' );
				} else {
					$area = $this->area_atuacao_model->get_by_nome($data ['ara_nome']);
						$this->area_atuacao_model->salvar ( $data );
						$this->session->set_flashdata ( 'sucesso', 'Área de atuação inserida com sucesso!' );
				}
				redirect ( get_listar_areas_atuacao_url () . '/' );
			}
		}

		$data['area_atuacao'] = array();
		if(!is_null($area_atuacao_id))
			$data['area_atuacao'] = $this->area_atuacao_model->get_by_id($area_atuacao_id);

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_editar_area_atuacao_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Exclui área de atuação
	 */
	public function excluir_area_atuacao($area_atuacao_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$this->area_atuacao_model->excluir ( $area_atuacao_id );
		$this->session->set_flashdata ( 'sucesso', 'Área de atuação excluída com sucesso!' );
		redirect ( get_listar_areas_atuacao_url () . '/');
	}

	/**
	 * 	SIMULADO
	 */

	public function listar_simulados($is_busca_sessao = null)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR, PROFESSOR
		), ACESSO_NEGADO);

		$this->load->helper('simulado');

		$usuario_logado = get_usuario_array(get_current_user_id());

		$data['include_jquery_form'] = true;
		$data['include_admin_listar_simulados'] = true;
		$data['include_chosen'] = true;
		$data['include_datepicker'] = true;
		$data['include_validade'] = true;
		$data['check_simulado'] = array();

		$data['disciplinas'] = get_disciplina_multiselect($this->disciplina_model->listar_todas());
		$data['professores'] = get_professores_multiselect(get_professores(FALSE));
		$data['bancas'] = get_banca_multiselect($this->banca_model->listar_todas());
		$data['orgaos'] = get_orgao_multiselect($this->orgao_model->listar_todas());
		//$data['alunos'] = get_alunos_multiselect(get_alunos_ativos_podio($usuario_logado['nome_completo']));
		$data['alunos'] = get_ajax_main_alunos_acompanhamento($filtro_aluno, FALSE);

		$data['filtro_aluno'] = array();
		$data['ordenacoes'] = get_ordenacao_simulado_multiselect();

		$data['aluno_selecionado'] = NULL;

		if(is_selenium()) {
			$selenium = get_usuario_by_email('selenium@exponencialconcursos.com.br');
			$data['alunos'][$selenium->ID] = "Selenium";
			$data['aluno_selecionado'] = $selenium->ID;
		}

		if($this->input->post('filtrar')) {

			$data['texto'] = $this->input->post('search_filter');
			$data['banca_selecionada'] = $this->input->post('ban_ids');
			$data['orgao_selecionado'] = $this->input->post('org_ids');
			$data['disciplina_selecionada'] = $this->input->post('dis_ids');
			$data['professor_selecionado'] = $this->input->post('prof_ids');
			$data['situacao_selecionada'] = $this->input->post('check_situacao');
			$data['status_selecionado'] = $this->input->post('check_simulado');
			$data['prazos_selecionados'] = $this->input->post('check_prazo');
			$data['ordenacao_selecionada'] = $this->input->post('ordenacao');

			$result = $this->simulado_model->listar_por_filtro_com_total($this->input->post());

			$data['total'] = array_pop($result);
			$data['simulados'] = $result;

			$_SESSION['listar_simulado_filtro'] = array(
				'search_filter' => $data['texto'],
				'ban_ids' => $data['banca_selecionada'],
				'org_ids' => $data['orgao_selecionado'],
				'dis_ids' => $data['disciplina_selecionada'],
				'prof_ids' => $data['professor_selecionado'],
				'check_situacao' => $data['situacao_selecionada'],
				'check_simulado' => $data['status_selecionado'],
				'check_prazo' => $data['prazos_selecionados'],
				'ordenacao' => $data['ordenacao_selecionada'],
				'offset' => 0
			);

		}else if(isset($_SESSION['listar_simulado_filtro']) && !is_null($is_busca_sessao) && $is_busca_sessao >= 0){

			$valores = $_SESSION['listar_simulado_filtro'];
			$_SESSION['listar_simulado_filtro']['offset'] = $is_busca_sessao;

			$data['texto'] = $valores['search_filter'];
			$data['banca_selecionada'] = $valores['ban_ids'];
			$data['orgao_selecionado'] = $valores['org_ids'];
			$data['disciplina_selecionada'] = $valores['dis_ids'];
			$data['professor_selecionado'] = $valores['prof_ids'];
			$data['situacao_selecionada'] = $valores['check_situacao'];
			$data['status_selecionado'] = $valores['check_simulado'];
			$data['prazos_selecionados'] = $valores['check_prazo'];
			$data['ordenacao_selecionada'] = $valores['ordenacao'];

			$result = $this->simulado_model->listar_por_filtro_com_total($valores, $is_busca_sessao);

			$data['total'] = array_pop($result);
			$data['simulados'] = $result;

		}else {

			if(is_null($is_busca_sessao) || $is_busca_sessao < 0){
				$is_busca_sessao = 0;
			}

			$data['texto'] = "";
			$data['banca_selecionada'] = OPCAO_ZERO;
			$data['orgao_selecionado'] = OPCAO_ZERO;
			$data['disciplina_selecionada'] = OPCAO_ZERO;
			$data['professor_selecionado'] = OPCAO_ZERO;
			$data['situacao_selecionada'] = NULL;
			$data['status_selecionado'] = NULL;
			$data['prazos_selecionados'] = array();
			$data['ordenacao_selecionada'] = PRIORIDADE;

			/**
			 * [JOULE] Ordenação por prioridade deve ser default
			 *
			 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2081
			 */

			$result = $this->simulado_model->listar_por_filtro_com_total(['ordenacao' => PRIORIDADE], $is_busca_sessao);

			$data['total'] = array_pop($result);
			$data['simulados'] = $result;


			unset($_SESSION['listar_simulado_filtro']);
		}

		if($data['simulados']) {
			foreach ($data['simulados'] as &$simulado) {
				$simulado['total_questoes'] = $this->simulado_model->get_total_questoes($simulado['sim_id']);
			}
		}

		if(is_null($is_busca_sessao)){
			$is_busca_sessao = 0;
		}

		if(!$data['prazos_selecionados']){
			$data['prazos_selecionados'] = array();
		}

		$config = pagination_config(get_listar_simulados_url(), $data['total'], SIMULADOS_POR_PAGINA);
		$config["first_url"] = get_listar_simulados_url()."/0";
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_simulados_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function mudar_status_simulado($simulado_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$simulado = $this->simulado_model->get_by_id($simulado_id);
		$status = ATIVO;
		if($simulado['sim_status'] == ATIVO)
			$status = INATIVO;
		$this->simulado_model->mudar_status($simulado, $status);
		redirect ( get_listar_simulados_url () . '/' );
	}

	public function designar_alunos($simulado_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR, PROFESSOR
		), ACESSO_NEGADO);


		$usuarios_id = $this->input->post ( 'usu_ids' );
		$data_limite = converter_para_yyyymmdd($this->input->post ( 'data_conclusao' ));

		$this->simulado_model->designar_usuarios($simulado_id, $usuarios_id, $data_limite);

		set_mensagem(SUCESSO, 'Usuarios designados ao simulado com sucesso!');
		redirect ( get_listar_simulados_url () );
	}

	public function duplicar_simulado($simulado_id, $opcao_duplicacao)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$simulado = $this->simulado_model->get_by_id($simulado_id);
		unset($simulado['sim_id']);
		$simulado['sim_nome'] = $simulado['sim_nome'] . " (cópia)";
		if($simulado['sim_status'] == 1){
			$simulado['sim_status'] = 2;
		}

		$novo_simulado_id = $this->simulado_model->salvar($simulado);

		$exclusoes = $this->simulado_model->get_exclusoes_simulado($simulado_id);
		foreach ($exclusoes as $exclusao){
			unset($exclusao['sie_id']);
			$exclusao['sim_id'] = $novo_simulado_id;
			$this->simulado_model->salvar_simulado_exclusao($exclusao);
		}

		$disciplinas = $this->simulado_model->get_dados_simulados_disciplinas($simulado_id);
		foreach ($disciplinas as $disciplina){
			$disciplina['sim_id'] = $novo_simulado_id;
			$this->simulado_model->salvar_simulado_disciplina($disciplina);
		}

		$assuntos = $this->simulado_model->get_dados_simulados_assuntos($simulado_id);
		$cont = 0;
		foreach ($assuntos as $assunto){
			$assunto['sim_id'] = $novo_simulado_id;
			$this->simulado_model->salvar_simulado_assunto($assunto);
		}

		$revisores = $this->simulado_model->listar_revisores($simulado_id);
		foreach ($revisores as $revisor) {
			$this->simulado_model->adicionar_revisor($novo_simulado_id, $revisor['dis_id'], $revisor['usu_id']);
		}

		$simulados_associados_ids = array();
		$associados = $this->simulado_model->listar_simulados_associados($simulado_id);

		foreach ($associados as $associado) {
			array_push($simulados_associados_ids, $associado['sim_associado_id']);
		}
		if($simulados_associados_ids) {
			$this->simulado_model->atualizar_simulados_associados($novo_simulado_id, $simulados_associados_ids);
		}

		if($opcao_duplicacao == 2){
			$questoes = $this->simulado_model->get_questoes_simulado($simulado_id);
			foreach ($questoes as $questao){
				$questao['sim_id'] = $novo_simulado_id;
				$this->simulado_model->salvar_simulado_questao($questao);
			}
		}

		$this->simulado_model->copia_imagem_simulado($simulado_id, $novo_simulado_id);
		$this->simulado_model->copia_pdf_simulado($simulado_id, $novo_simulado_id);
		set_mensagem(SUCESSO, 'Simulado duplicado com sucesso!');
		redirect(get_listar_simulados_url(TRUE));
	}

	public function atualizar_notas($simulado_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$ranking = $this->simulado_model->get_ranking($simulado_id, 0, null);

		$i = 0;
		$total = count($ranking);

		log_sq('info', "Atualizando notas do simulado: {$simulado_id}");
		echo "Atualizando notas do simulado: {$simulado_id}<br>";

		foreach ($ranking as $item) {
			$i++;
			set_time_limit(180);
			log_sq('info', "Atualizando usuario $i de $total (ID: {$item['usu_id']})");
			echo "Atualizando usuario $i de $total (ID: {$item['usu_id']} - SIU: {$item['siu_id']})<br>";

			$this->simulado_model->salvar_nota(FALSE, $item['usu_id'], $simulado_id);
		}

		// log_sq('info', "Fim da atualizacao de notas do simulado: {$simulado_id}");

		// set_mensagem(SUCESSO, 'Notas atualizadas com sucesso!');
		// redirect(get_listar_simulados_url());
	}

	public function modal_comentarios_professores_caderno($caderno_id){
		echo self::modal_comentarios_professores(null, $caderno_id);
	}

	public function modal_comentarios_professores_simulado($simulado_id){
		echo self::modal_comentarios_professores($simulado_id, null);
	}

	private function modal_comentarios_professores($simulado_id, $caderno_id){

		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ
		), ACESSO_NEGADO);

		$this->load->helper('tabela');

		if(!is_null($simulado_id)){
			$comentarios_professores = $this->comentario_model->contar_comentarios_simulado_por_professor($simulado_id);
			$total_sem_comentarios = $this->questao_model->contar_questoes_simulado_sem_comentario_professor($simulado_id);
			$total_questoes = $this->simulado_model->contar_questoes_simulado($simulado_id);
		}

		if(!is_null($caderno_id)){
			$comentarios_professores = $this->comentario_model->contar_comentarios_caderno_por_professor($caderno_id);
			$total_sem_comentarios = $this->questao_model->contar_questoes_caderno_sem_comentario_professor($caderno_id);
			$total_questoes = $this->caderno_model->get_total_questoes($caderno_id);
		}

		$total_percentual = 0;
		foreach($comentarios_professores as &$comentario_professor){
			$comentario_professor['percentual'] = arredondar_numero(($comentario_professor['total']/$total_questoes)*100, 3) ;
			$total_percentual += $comentario_professor['percentual'];
		}

		$sem_comentarios_perc = arredondar_numero(($total_sem_comentarios/$total_questoes)*100, 3);
		$total_percentual += $sem_comentarios_perc;
		array_push($comentarios_professores, array('nome' => 'Não comentadas', 'total' => $total_sem_comentarios, 'percentual' => $sem_comentarios_perc));

		$thead_array = array('Professores', 'Quantidade de questões', '% do total');
		$tbody_array = get_tabela_comentarios_professores($comentarios_professores);
		$tfoot_array = array('Totais', $total_questoes, porcentagem($total_percentual));

		$tabela = get_simple_table($thead_array, $tbody_array, $tfoot_array, '', '');

		$echo =
		"<div class='modal-header'>
			<div class='row resultado-header'>
				<h3 class='pull-left'>Questões comentadas por professor</h3>
			</div>
		</div>
		<div class='modal-body'>{$tabela}</div>
		<div class='modal-footer'>
			<button type='button' class='btn btn-white pull-right' data-dismiss='modal'>Fechar</button>
		</div>
		";

		return $echo;
	}

	public function modal_duplicar_simulados($simulado_id) {
		$this->load->helper('simulado');

		$echo = "<div class='modal-header'>
		<div class='row resultado-header'>
		<h3 class='pull-left'>Duplicar Simulado</h3>
		</div>
		</div>
		<div class='modal-body'>
					<p>Escolha a forma de duplicação do simulado:</p>


			</div>
			<div class='modal-footer'>
				<img src='".get_tema_image_url("loader.gif")."' style='display:none' id='loader'>
				<a href='".get_duplicar_simulados_url($simulado_id, 2)."' role='button' class='btn btn-primary btn-duplicar'>Copiando Questões</a>
				<a href='".get_duplicar_simulados_url($simulado_id, 1)."' role='button' class='btn btn-primary btn-duplicar pull-right'>Sem Questões</a>
				<button type='button' class='btn btn-white pull-left' data-dismiss='modal'>Fechar</button>

			</div></div>
		<script>
		$('.btn-duplicar').click(function (event) {
			$('#loader').show();
		});

		</script>";

		echo $echo;
	}

	public function editar_simulado($simulado_id = null, $passo = 1){

		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->load->helper('simulado');

		$is_salvar = $this->input->post ( 'simulado_passo' ) || $this->input->post ( 'simulado_salvar' ) ;

		if ($passo == 1 ) {

			if($is_salvar || $this->input->post ( 'img_submit' ) ||  $this->input->post ( 'pdf_submit' ) == "submit"){

				$data = [
				    'sim_nome' => $this->input->post('sim_nome'),
				    'org_id' => $this->input->post('org_id'),
				    'car_id' => $this->input->post('car_id'),
				    'ban_id' => $this->input->post('ban_id'),
				    'sim_status' => $this->input->post('sim_status'),
				    'sim_sortear_questoes' => $this->input->post('sim_sortear_questoes') ?: 0,
				    'sim_pontos_negativos' => $this->input->post('sim_pontos_negativos') ?: 0,
				    'sim_peso_erros' => $this->input->post('sim_peso_erros') ? numero_americano( $this->input->post('sim_peso_erros') ) : null,
				    'sim_data_entrega' => converter_para_yyyymmdd($this->input->post('sim_data_entrega')),
				    'sim_url_edital' => $this->input->post('sim_url_edital') ?: null,
					//'tipos_selecionados' => $this->input->post('tipo_id')
					'sim_media_ponderada' => $this->input->post('sim_media_ponderada') ?:0,
			    ];

				if ( empty ( $simulado_id )) {
					$simulado = $this->simulado_model->get_by_nome($data ['sim_nome']);
					if(!count($simulado) > 0) {
						$simulado_id = $this->simulado_model->salvar ( $data );
					} else {
						$this->session->set_flashdata ( 'img_simulado', 'Já existe um simulado com este nome!' );
						redirect ( get_editar_simulado_url () .'//'.$passo);
					}
				}
				else {
					$data['sim_id'] = $simulado_id;
					$this->simulado_model->atualizar_dados_basicos($data);
				}

				//Tipo de questão
				$this->simulado_model->remover_exclusoes_simulado($simulado_id, 'que_tipo');
				$exclusao_tipo_questao = array(
					'sim_id' => $simulado_id,
					'sie_campo' => 'que_tipo',
					'sie_valor' => $this->input->post('tipo_id')
				);
				$this->simulado_model->salvar_simulado_exclusao($exclusao_tipo_questao);

				//Simulados associados
				$this->simulado_model->atualizar_simulados_associados($simulado_id, $this->input->post('simulados_associados'));

				if($this->input->post ( 'img_submit' ) == "submit") {
					if(!upload_img("img_simulado_$simulado_id", 'simulados','img_simulado', $this)){
						redirect ( get_editar_simulado_url (array('sim_id' => $simulado_id), $passo) );
					}
				}

				elseif($this->input->post ( 'pdf_submit' ) == "submit") {
					if(!upload_pdf("pdf_simulado_$simulado_id", 'simulados','pdf_simulado', $this)){
						redirect ( get_editar_simulado_url (array('sim_id' => $simulado_id), $passo) );
					}
				}

				elseif($this->input->post ( 'simulado_passo' )){
					//Para carregar a próxima tela
					$passo++;
					$is_salvar = false;
				}else{
					redirect ( get_listar_simulados_url(TRUE) );
				}

			}

			if($passo == 1){

				$data ['orgao_selecionado'] = OPCAO_ZERO;
				$data ['cargo_selecionado'] = OPCAO_ZERO;
				$data ['banca_selecionado'] = OPCAO_ZERO;
				$data ['status_selecionado'] = OPCAO_ZERO;
				$data ['bancas_combo'] = get_banca_combo ( $this->banca_model->listar_todas () );
				$data ['orgaos_combo'] = get_orgao_combo ( $this->orgao_model->listar_todas () );
				$data ['cargos_combo'] = get_cargo_combo ( $this->cargo_model->listar_todas () );
				$data ['status_combo'] = get_status_combo();
				$data ['simulados_combo'] = get_simulado_multiselect( $this->simulado_model->listar_simulados_exceto($simulado_id) );
				$data ['tipos_selecionados'] = OPCAO_ZERO;
				$data ['tipos'] = get_tipo_questao_multiselect();


				if (!is_null ( $simulado_id ) && $passo == 1) {
					$data ['simulado'] = $this->simulado_model->get_by_id ( $simulado_id );
					$data ['cargo_selecionado'] = $data ['simulado'] ['car_id'];
					$data ['orgao_selecionado'] = $data ['simulado'] ['org_id'];
					$data ['banca_selecionado'] = $data ['simulado'] ['ban_id'];
					$data ['status_selecionado'] = $data ['simulado'] ['sim_status'];
					$data ['has_simulado_img'] = $this->simulado_model->has_simulado_img ( $simulado_id );
					$data ['has_simulado_pdf'] = $this->simulado_model->has_simulado_pdf ( $simulado_id );
					$data ['tipos_selecionados'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'que_tipo'), 'sie_valor') ?: null;
					$data ['simulados_associados_selecionados'] = get_coluna($this->simulado_model->listar_simulados_associados($simulado_id), 'sim_associado_id') ?: null;
				}

				$data['array_combo'] = array(
					array('Orgão', 'org_id', $data['orgaos_combo'], $data['orgao_selecionado']),
					array('Cargos', 'car_id', $data['cargos_combo'], $data['cargo_selecionado']),
					array('Banca', 'ban_id', $data['bancas_combo'], $data['banca_selecionado']),
					array('Status', 'sim_status', $data['status_combo'], $data['status_selecionado']),
					array('Tipo de Questão', 'tipo_id', $data['tipos'], $data['tipos_selecionados'])
				);

			}

		}

		if ($passo == 2) {

			$input_names = get_array_input_names();
			$input_labels = get_array_input_labels();
			$campos_tabela = get_array_table_fields();

			if($is_salvar){

				foreach($input_names as $key => $nome) {
					if ($this->input->post ( $nome ) > 0) {
						$exclusoes [$campos_tabela[$key]] = $this->input->post ( $nome );
					}
				}

				//Atualiza as exclusões ignorando o campo 'que_tipo' que está no primeiro passo
				$this->simulado_model->atualizar_exclusoes_simulado($simulado_id, $exclusoes, 'que_tipo');

				if($this->input->post ( 'simulado_passo' )){
					//Para carregar a próxima tela
					$passo++;
					$is_salvar = false;
				}else{
					redirect ( get_listar_simulados_url(TRUE) );
				}
			}

			if($passo == 2){

				$data ['simulado'] = $this->simulado_model->get_by_id ( $simulado_id );

				$data ['bancas_selecionadas'] = array ();
				$data ['orgaos_selecionados'] = array ();
				$data ['cargos_selecionados'] = array ();
				$data ['anos_selecionados'] = array();
				$data ['escolaridades_selecionadas'] = array ();
				$data ['atuacoes_selecionadas'] = array ();
				$data ['dificuldades_selecionadas'] = array();
				$data ['formacoes_selecionadas'] = array ();
				$data ['tipos_selecionados'] = array ();
				$data ['bancas'] = get_banca_multiselect( $this->banca_model->listar_todas () );
				$data ['orgaos'] = get_orgao_multiselect( $this->orgao_model->listar_todas () );
				$data ['cargos'] = get_cargo_multiselect( $this->cargo_model->listar_todas () );
				$data ['anos'] = get_ano_multiselect($this->prova_model->listar_anos());
				$data ['escolaridades'] = get_escolaridade_multiselect();
				$data ['areas_atuacao'] = get_area_atuacao_multiselect ( $this->area_atuacao_model->listar_todas () );
				$data ['dificuldades'] = get_dificuldade_questao_multiselect();
				$data ['areas_formacao'] = get_area_formacao_multiselect ( $this->area_formacao_model->listar_todas () );
				$data ['tipos'] = get_tipo_questao_multiselect();
				$data ['status'] = get_status_questao_combo();
				$data ['assuntos_selecionados'] = "";
				$data ['sim_excluir_questoes'] = get_excluir_questoes_simulado();

				$data ['bancas_selecionadas'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'ban_id'), 'sie_valor') ?: null;
				$data ['orgaos_selecionados'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'org_id'), 'sie_valor') ?: null;
				$data ['cargos_selecionados'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'car_id'), 'sie_valor') ?: null;
				$data ['anos_selecionados'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'pro_ano'), 'sie_valor') ?: null;
				$data ['escolaridades_selecionadas'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'esc_id'), 'sie_valor') ?: null;
				$data ['atuacoes_selecionadas'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'ara_id'), 'sie_valor') ?: null;
				$data ['dificuldades_selecionadas'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'que_dificuldade'), 'sie_valor') ?: null;
				$data ['formacoes_selecionadas'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'arf_id'), 'sie_valor') ?: null;
				$data ['simulados_associados_selecionados'] = get_coluna($this->simulado_model->listar_simulados_associados($simulado_id), 'sim_associado_id') ?: null;
				$data ['status_selecionados'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'que_status'), 'sie_valor') ?: null;
				$data ['sim_excluir_questoes_selecionados'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'sim_excluir_questoes'), 'sie_valor') ?: null;

				$data['array_multiselect'] = array(
						array($input_names[0], $data['bancas'], $data['bancas_selecionadas']),
						array($input_names[1], $data['orgaos'], $data['orgaos_selecionados']),
						array($input_names[2], $data['cargos'], $data['cargos_selecionados']),
						array($input_names[3], $data['anos'], $data['anos_selecionados']),
						array($input_names[4], $data['escolaridades'], $data['escolaridades_selecionadas']),
						array($input_names[5], $data['areas_atuacao'], $data['atuacoes_selecionadas']),
						array($input_names[6], $data['dificuldades'], $data['dificuldades_selecionadas']),
						array($input_names[7], $data['areas_formacao'], $data['formacoes_selecionadas']),
						array($input_names[9], $data['status'], $data['status_selecionados']),
						array($input_names[14], $data['sim_excluir_questoes'], $data['sim_excluir_questoes_selecionados']),
				);
			}

		}

		if ($passo == 3) {

			if($is_salvar){

				$disciplinas_selecionadas = array();
				foreach ($this->input->post('dis_check') as $value) {
					$disciplinas_selecionadas[$value] = $this->input->post("dis_input_{$value}");
				}

				$data ['disciplinas_selecionadas'] = $disciplinas_selecionadas;

				$this->simulado_model->atualizar_disciplinas_simulado($simulado_id, $disciplinas_selecionadas);

				if($this->input->post ( 'simulado_passo' )){
					//Para carregar a próxima tela
					$passo++;
					$is_salvar = false;
				}else{
					redirect ( get_listar_simulados_url(TRUE) );
				}
			}

			if($passo == 3){

				$disciplinas = $this->disciplina_model->listar_todas(true);
				$total_questoes_disciplina = 0;

				$i = 10000;
				$disciplinas_ordenadas = array();
				foreach ($disciplinas as $disciplina) {
					$i = $i + 1;
					$index = $i;

					$disciplina['dis_check'] = "dis_check_{$disciplina['dis_id']}";
					$disciplina['selected'] = false;
					$disciplina['dis_input'] = "dis_input_{$disciplina['dis_id']}";
					$disciplina['valor_input'] = 10;
					$disciplina_salva = $this->simulado_model->get_disciplinas_simulado($simulado_id, $disciplina['dis_id']);
					if(count($disciplina_salva) > 0) {
						$ordem = $this->simulado_model->get_ordem_de_disciplina($simulado_id, $disciplina['dis_id']);
						if($ordem) {
							$index = $ordem;
						}

						$disciplina['selected'] = true;
						$disciplina['valor_input'] = $disciplina_salva[0]['qtde'];
						$total_questoes_disciplina += $disciplina['valor_input'];
					}

					$disciplinas_ordenadas[$index] = $disciplina;
				}

				$data['total_questoes_disciplina'] = $total_questoes_disciplina;
				ksort($disciplinas_ordenadas);
				$data['disciplinas'] = $disciplinas_ordenadas;
				$data ['simulado'] = $this->simulado_model->get_by_id ( $simulado_id );

				self::adicionar_filtro_simulado($simulado_id);

			}
		}

		if ($passo == 4) {

			if($is_salvar){

				$assuntos_selecionados = explode(",", $this->input->post ( 'assuntos_selecionados' ));
				$disciplina_nova_ordem = explode(";", $this->input->post ('disciplinas_ordem'));
				$this->simulado_model->atualizar_assuntos_simulado($simulado_id, $assuntos_selecionados);
				$this->simulado_model->atualizar_ordem_disciplinas($simulado_id, $disciplina_nova_ordem);

				if($this->input->post ( 'simulado_passo' )){
					//Para carregar a próxima tela
					$passo++;
					$is_salvar = false;
				}else{
					redirect ( get_listar_simulados_url(TRUE) );
				}
			}

			if($passo == 4){

				$i = 10000;
				$disciplinas_ordenadas = array();
				$disciplinas = $this->simulado_model->get_disciplinas_simulado($simulado_id);
				$total_questoes_disciplina = 0;
				foreach ($disciplinas as $disciplina) {
					$i = $i + 1;
					$index = $i;

					$ordem = $this->simulado_model->get_ordem_de_disciplina($simulado_id, $disciplina['dis_id']);
					if($ordem) {
						$index = $ordem;
					}

					$assuntos_ids = get_coluna($this->simulado_model->get_assuntos_simulado($simulado_id), 'ass_id');
					$data['assuntos_selecionados'] = implode(",", $assuntos_ids);

					$disciplinas_ordenadas[$index] = $disciplina;

					$total_questoes_disciplina += $disciplina['qtde'];
				}

				self::adicionar_filtro_simulado($simulado_id);
				$simulado_ass_id = self::get_simulado_ass_id($disciplinas_ordenadas);

				$data['simulado_ass_id'] = $simulado_ass_id;

				$data['total_questoes_disciplina'] = $total_questoes_disciplina;
				$assuntos_ids = get_coluna($this->simulado_model->get_assuntos_simulado($simulado_id), 'ass_id');
				$data['assuntos_selecionados'] = implode(",", $assuntos_ids);
				$disciplinas_ordem = $this->simulado_model->listar_disciplinas_por_ordem($simulado_id);
				$data ['disciplinas_ordem'] = get_disciplinas_ordem_str($disciplinas_ordem);
				$data ['simulado'] = $this->simulado_model->get_by_id ( $simulado_id );

			}

		}

		if ($passo == 5) {

			if($is_salvar){

				$revisores = array();
				foreach ($this->input->post('revisores_ids') as $disciplina_id => $revisores_ids) {
					if($disciplina_id == 0) continue;

					foreach ($revisores_ids as $revisor_id) {
						array_push($revisores, array(
							'disciplina_id' => $disciplina_id,
							'usuario_id' => $revisor_id
						));
					}
				}

				$sim_revisao_ativada = $this->input->post ( 'sim_revisao_ativada' );

				if(empty($sim_revisao_ativada)){
					$sim_revisao_ativada = '0';
				}

				$this->simulado_model->atualizar_revisores($simulado_id, $revisores);
				$this->simulado_model->atualizar_status_revisao($simulado_id, $sim_revisao_ativada);

				if($this->input->post ( 'simulado_passo' )){
					//Para carregar a próxima tela
					$passo++;
					$is_salvar = false;
				}else{
					redirect ( get_listar_simulados_url(TRUE) );
				}
			}

			if($passo == 5){
				$data['simulado_revisores'] = self::get_simulado_revisores_html($simulado_id);
				$data ['simulado'] = $this->simulado_model->get_by_id ( $simulado_id );
			}

		}

		if ($passo == 6) {

			if($is_salvar){

				$prd_id = $this->input->post ( 'prd_id' );
				$descricao = $this->input->post ( 'sim_descricao' );

				$this->simulado_model->atualizar_dados_produto($simulado_id, $prd_id, $descricao);

				if($this->input->post ( 'simulado_passo' )){
					//Para carregar a próxima tela
					$passo++;
					$is_salvar = false;
				}
			}

			if($passo == 6){

				$data ['produto_selecionado'] = 0;
				$data ['produtos'] = get_produto_combo ( get_todos_produtos_e_coaching_posts('any'), TRUE);
				$data ['simulado'] = $this->simulado_model->get_by_id ( $simulado_id );

				if(!is_null($data ['simulado']['prd_id'])){
					$data ['produto_selecionado'] = $data ['simulado']['prd_id'];
				}

			}else{
				self::init_rotulos($simulado_id);
				redirect ( get_listar_simulados_url(TRUE) );
			}

		}

		$data['passo'] = $passo;
		$data['simulado_id'] = $simulado_id;

		$data ['include_dropzone'] = true;
		$data ['include_steps'] = true;
		$data ['include_chosen'] = true;
		$data ['include_validade'] = true;
		$data ['include_jstree'] = true;
		$data ['include_summernote'] = true;
		$data ['include_admin_editar_simulado'] = true;
		$data ['include_jquery_ui'] = true;
		//$data ['include_mask'] = true;
		$data ['include_mask_plugin'] = true;
		$data ['include_datepicker'] = true;

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_editar_simulado_view_url($passo), $data);
		$this->load->view(get_admin_footer_view_url(), $data);

	}

	/**
	 * Define a ordem como os simulados inconsistentes serão exibidas na ordenação
	 * por prioridade
	 *
	 * [JOULE][J1] UPGRADE - Controle de prioridade dos simulados
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/340
	 *
	 * @since 10.0.0
	 */

	public function prioridade_simulados()
	{
		// Controle de acesso
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		// recupera informações dos modelos
		$m_simulados = $this->simulado_model->listar_por_status(INCONSISTENTE, 's.sim_prioridade asc, s.sim_data_inconsistencia desc, s.sim_id desc');

		// prepara variáveis para template
		$data['include_nestable'] = true;
		$data['simulados'] = $m_simulados;
		$data['caixa_azul'] = ui_get_alerta("<p>Os simulados estão ordenados da maior prioridade para a menor.</p><p>Para alterar uma posição basta arrastar o simulado desejado para cima ou para baixo.</p><p>O sistema salvará automaticamente todas as alterações.</p>", ALERTA_INFO);
		// renderiza a página
		$this->load->view('admin/header', $data);
		$this->parser->parse('admin/simulados/prioridade_simulados', $data);
		$this->load->view('admin/footer', $data);
	}

	public function editar_revisores($simulado_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		if ($this->input->post('salvar')) {

			$revisores = array();

			foreach ($this->input->post('revisores_ids') as $disciplina_id => $revisores_ids) {
				if($disciplina_id == 0) continue;

				foreach ($revisores_ids as $revisor_id) {
					array_push($revisores, array(
						'disciplina_id' => $disciplina_id,
						'usuario_id' => $revisor_id
					));
				}
			}
			set_mensagem(SUCESSO, 'Revisores editados com sucesso!');
			$this->simulado_model->atualizar_revisores($simulado_id, $revisores);
			redirect(get_listar_simulados_url(TRUE));
		}
		$name = $this->simulado_model->get_by_id($simulado_id);

		$data['simulado_nome'] = $name['sim_nome'];
		$data ['include_chosen'] = true;

		$data['simulado_id'] = $simulado_id;
		$data['revisores_ids'] = $this->simulado_model->listar_revisores_agrupados_por_disciplina($simulado_id, true);
		$data['professores'] = get_professores_multiselect(get_professores(FALSE));
		$data['disciplinas'] = $this->simulado_model->listar_disciplinas_simulado($simulado_id);

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/editar_revisores', $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function excluir_simulado_img($simulado_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		excluir_arquivo("img_simulado_{$simulado_id}", 'simulados');
		redirect ( get_editar_simulado_url (array('sim_id' => $simulado_id)) . '/' );
	}

	public function excluir_simulado_pdf($simulado_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		excluir_arquivo("pdf_simulado_{$simulado_id}", 'simulados');
		redirect ( get_editar_simulado_url (array('sim_id' => $simulado_id)) . '/' );
	}

	public function get_simulado_ass_id($disciplinas_simulado){

		$filtro = unserialize($_SESSION['simulado_filtro']);

		$string = "<ul id='sortable'>";
		$key = 0;
		foreach ($disciplinas_simulado as $disciplina) {
			set_time_limit(60);

			$id = $disciplina['dis_id'];
			$num_questoes = $disciplina['qtde'];

			$arvore_assuntos = gerar_arvore_array($this->assunto_model->listar_por_disciplina($id));
			$string .=
				'<li class="sortable-item" id="'. $id .'"><div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_'. $key . '">' .
									$disciplina['dis_nome'] . ' (' . $num_questoes . ')' .
									'<i class="icon-angle-down"></i>
								</a>
								<div class="text-right">
									<a class="btn blue todos">Todos</a>
									<a class="btn red nenhum">Nenhum</a>
								</div>
							</h4>
						</div>
						<div id="collapse_'. $key . '" class="panel-collapse collapse">
							<div class="panel-body">';
			if(count($arvore_assuntos) > 0) {
				$string .= 	'<div id="tree'. $key . '" class="jstree-open arvores">';
				foreach ($arvore_assuntos as $assunto) {
// 					$total_questoes = $this->assunto_model->get_total_questoes($assunto['ass_id']);
					$filtro['ass_ids'] = array($assunto['ass_id']);
					$total_questoes = $this->questao_model->get_total_resultado( $filtro );

					$string .=
								'<ul>
									<li id="'. $assunto['ass_id'] . '" class="jstree-open">
										<b>' . $assunto['ass_nome'] .'</b> - ('. $total_questoes .')';
					if(count($assunto['filhos']) > 0) {
						$string .= self::imprimir_filhos($assunto['filhos']);
					}
					$string .=		'</li>
								</ul>';
				}

				$string .= 	'</div>';
			} else {
				$string .= '<span>Não foram encontrados assuntos relacionados a esta disciplina</span>';
			}
			$string .= '</div>
					</div>
				</div></li>';
			$key++;
		}

		$string .= '</ul>';
		$string .= "<script>
						$('#sortable').sortable({
							update: function(event, ui) {

	             				var order = [];

					            $('.sortable-item').each( function(e) {
									order.push( $(this).attr('id'));
	              					var positions = order.join(';');
									$('#disciplinas_ordem').val(positions);
									console.log(positions);
								});
							}
						});
					</script>";
		return $string;

	}

	public function get_simulado_ass_id_ajax() {
// 		$this->simulado_model->listar_disciplina_por_ordem();

		$filtro = unserialize($_SESSION['simulado_filtro']);

		$ids_disciplinas = $_REQUEST['ids_disciplinas'];
		$qtde_questoes = $_REQUEST['qtde_questoes'];

		$string = "<ul id='sortable'>";
		foreach ($ids_disciplinas as $key => $id) {
			set_time_limit(60);

			$disciplina = $this->disciplina_model->get_by_id($id);
			$num_questoes = $qtde_questoes[$key];

			$arvore_assuntos = gerar_arvore_array($this->assunto_model->listar_por_disciplina($id));
			$string .=
				'<li class="sortable-item" id="'. $id .'"><div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_'. $key . '">' .
									$disciplina['dis_nome'] . ' (' . $num_questoes . ')' .
									'<i class="icon-angle-down"></i>
								</a>
								<div class="text-right">
									<a class="btn blue todos">Todos</a>
									<a class="btn red nenhum">Nenhum</a>
								</div>
							</h4>
						</div>
						<div id="collapse_'. $key . '" class="panel-collapse collapse">
							<div class="panel-body">';
			if(count($arvore_assuntos) > 0) {
				$string .= 	'<div id="tree'. $key . '" class="jstree-open arvores">';
				foreach ($arvore_assuntos as $assunto) {
// 					$total_questoes = $this->assunto_model->get_total_questoes($assunto['ass_id']);
					$filtro['ass_ids'] = array($assunto['ass_id']);
					$total_questoes = $this->questao_model->get_total_resultado( $filtro );

					$string .=
								'<ul>
									<li id="'. $assunto['ass_id'] . '" class="jstree-open">
										<b>' . $assunto['ass_nome'] .'</b> - ('. $total_questoes .')';
					if(count($assunto['filhos']) > 0) {
						$string .= self::imprimir_filhos($assunto['filhos']);
					}
					$string .=		'</li>
								</ul>';
				}

				$string .= 	'</div>';
			} else {
				$string .= '<span>Não foram encontrados assuntos relacionados a esta disciplina</span>';
			}
			$string .= '</div>
					</div>
				</div></li>';
		}

		$string .= '</ul>';
		$string .= "<script>
						$('#sortable').sortable({
							update: function(event, ui) {

	             				var order = [];

					            $('.sortable-item').each( function(e) {
									order.push( $(this).attr('id'));
	              					var positions = order.join(';');
									$('#disciplinas_ordem').val(positions);
									console.log(positions);
								});
							}
						});
					</script>";
		echo $string;
	}

	public function get_simulado_revisores_html($simulado_id){

		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$data['simulado_id'] = $simulado_id;
		$data['revisores_ids'] = $this->simulado_model->listar_revisores_agrupados_por_disciplina($simulado_id, true);
		$data['professores'] = get_professores_multiselect(get_professores(FALSE));
		$data['disciplinas'] = $this->simulado_model->get_disciplinas_simulado($simulado_id);

		return $this->load->view('admin/simulado_revisores', $data, TRUE);

	}

	public function simulado_revisores($simulado_id = NULL)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$data['simulado_id'] = $simulado_id;
		$data['revisores_ids'] = $this->simulado_model->listar_revisores_agrupados_por_disciplina($simulado_id, true);
		$data['professores'] = get_professores_multiselect(get_professores(FALSE));
		$data['disciplinas'] = $this->disciplina_model->listar_por_ids($this->input->post('ids_disciplinas'));
		echo $this->load->view('admin/simulado_revisores', $data, TRUE);
	}

	private function imprimir_filhos($filhos)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$string ="";
		if(count($filhos) > 0) {
			$string .= '<ul>';
			foreach($filhos as $assunto) {
				$string .= '<li id="'. $assunto['ass_id'] . '" class="jstree-open">
						<b>' . $assunto['ass_nome'] .'</b>';
				if(count($assunto['filhos']) > 0) {
					$string .= self::imprimir_filhos($assunto['filhos']);
				}
				$string .=	'</li>';
			}
			$string .= '</ul>';
		}
		return $string;
	}

	public function revisar_questoes($simulado_id, $disciplina_id = null)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR, PROFESSOR, CONSULTOR
		), ACESSO_NEGADO);

		$data['include_summernote'] = true;
		$data['include_chosen'] = true;
		$data['include_jstree'] = true;
// 		$data['include_admin_listar_questoes'] = true;
		$data['include_stars'] = true;
		$data['include_c3'] = true;
		$data['include_icheck'] = true;
		$data['include_datepicker'] = true;
		$data['include_mask'] = true;

		$this->load->helper('questao');
		$this->load->helper('prova');
		$this->load->helper('simulado');

		$data['disciplina_selecionada'] = $disciplina_id;
		$disciplinas_simulado = $this->simulado_model->listar_disciplinas_simulado($simulado_id);
		foreach($disciplinas_simulado as &$disciplina){
			$rotulos = $this->simulado_model->get_rotulos_disciplina($simulado_id, $disciplina['dis_id']);
			if($rotulos && strcmp($rotulos, $disciplina['dis_nome']) != 0){
				$disciplina["dis_nome"] = $disciplina['dis_nome']." (".$rotulos.")";
			}
		}
		$data['disciplina_combo'] = get_disciplina_simulado_combo($disciplinas_simulado, $simulado_id);

		$data['filtro_favoritas'] = $this->input->get('filtro_favoritas')?: NULL;
		$data['status'] = get_status_questao_combo();
		$data['palavra_chave'] = '';
		$data['que_qcon_id'] = '';

		init_combos($data, TRUE, TRUE);

		if(!is_null($disciplina_id)) {
			if(!pode_revisar_disciplina_simulado($simulado_id, $disciplina_id)) {
				redirecionar_para_acesso_negado();
			}

			//O filtro é necessário mesmo não sendo realizada a busca, por isso sempre é montado
			$filtro = array();
			if($this->input->get('status_ids'))
			{
				$filtro['status_ids'] = $this->input->get('status_ids');
			}
			if ($this->input->get('filtro_favoritas')) {
				$filtro['favoritas'] = $this->input->get('filtro_favoritas');
			}
			if ($this->input->get('data_inicio')) {
				$filtro['data_inicio'] = $this->input->get('data_inicio');
			}
			if ($this->input->get('data_fim')) {
				$filtro['data_fim'] = $this->input->get('data_fim');
			}

			adicionar_filtro($filtro, $data, $this, FALSE);


			$ignoradas = $this->simulado_model->listar_questoes_ignoradas($simulado_id);
			if($ignoradas) {
				$filtro['ignoradas'] = $ignoradas; //$this->simulado_model->listar_questoes_ignoradas($simulado_id);
			}

// 				preencher_formulario_filtro($filtro, $data);

			$data['apenas_assunto_pai'] = $this->input->get("apenas_assunto_pai");
			if(!$data['apenas_assunto_pai'])
			{
				adicionar_assuntos_filhos($filtro);
			}
			$filtro['dis_ids'] = array($disciplina_id);

			//$_SESSION['filtro_revisar'] = serialize($filtro);

			$filtro ['sim_excluir_questoes'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'sim_excluir_questoes'), 'sie_valor');

			$filtro_sessao = $filtro;

			if($this->input->get('buscar')) {
				$data['questoes'] = $this->questao_model->listar_com_opcoes_e_assuntos($filtro);
				$data['total'] = $this->questao_model->get_total_resultado( $filtro );
				$data['is_resultado'] = true;
			}

			else {
				$filtro = array(
						'sim_ids' => array($simulado_id),
						'dis_ids' => array($disciplina_id)
				);

				$data['questoes'] = $this->questao_model->listar_por_simulado($simulado_id, null, null, $disciplina_id);

				$data['questoes_combo'] = get_questoes_relacionadas_combo($data['questoes']);
			}

			//if(isset($_SESSION['filtro_revisar'])) {
			if($this->input->get('buscar')) {
				//$filtro_sessao = unserialize($_SESSION['filtro_revisar']);

				if($filtro_sessao['dis_ids']) {
					$data['assuntos'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplinas($filtro['dis_ids']));
				}
				if($filtro_sessao['favoritas'])
				{
				    $data['filtro_favoritas'] = $filtro['favoritas'];
				}
			}else{
				//Só vou carregar o filtro padrão se não tiver nenhuma busca diferente
				$carrega_filtro_simulado = empty($_GET);//TRUE;
			}

			$data['assuntos'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplina($disciplina_id));

			$data['busca'] = $filtro_sessao['busca'] ?: null;
			$data['bancas_selecionadas'] = $filtro_sessao['ban_ids'] ?: null;
			$data['orgaos_selecionados'] = $filtro_sessao['org_ids'] ?: null;
			$data['cargos_selecionados'] = $filtro_sessao['car_ids'] ?: null;
			$data['anos_selecionados'] = $filtro_sessao['pro_anos'] ?: null;
			$data['escolaridades_selecionadas'] = $filtro_sessao['esc_ids'] ?: null;
			$data['assuntos_selecionados'] = $filtro_sessao['ass_ids'] ?: null;
			$data['formacoes_selecionadas'] = $filtro_sessao['arf_ids'] ?: null;
			$data['atuacoes_selecionadas'] = $filtro_sessao['ara_ids'] ?: null;
			$data['tipos_selecionados'] = $filtro_sessao['que_tipos'] ?: null;
			$data['dificuldades_selecionadas'] = $filtro_sessao['que_dificuldades'] ?: null;
			$data['simulados_selecionados'] = $filtro_sessao['sim_ids'] ?: null;
			$data['statuses_selecionadas'] = $filtro_sessao['que_statuses'] ?: null;
			$data['data_inicio'] = $filtro_sessao['data_inicio']?: null;
			$data['data_fim'] = $filtro_sessao['data_fim']?: null;

			if($carrega_filtro_simulado == TRUE){
				$data ['bancas_selecionadas'] = $data ['bancas_selecionadas'] ?: get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'ban_id'), 'sie_valor') ?: null;
				$data ['orgaos_selecionados'] = $data ['orgaos_selecionados'] ?: get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'org_id'), 'sie_valor') ?: null;
				$data ['cargos_selecionados'] = $data ['cargos_selecionados'] ?: get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'car_id'), 'sie_valor') ?: null;
				$data ['anos_selecionados'] = $data ['anos_selecionados'] ?: get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'pro_ano'), 'sie_valor') ?: null;
				$data ['escolaridades_selecionadas'] = $data ['escolaridades_selecionadas'] ?: get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'esc_id'), 'sie_valor') ?: null;
				$data ['atuacoes_selecionadas'] = $data ['atuacoes_selecionadas'] ?: get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'ara_id'), 'sie_valor') ?: null;
				$data ['dificuldades_selecionadas'] = $data ['dificuldades_selecionadas'] ?: get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'que_dificuldade'), 'sie_valor') ?: null;
				$data ['formacoes_selecionadas'] = $data ['formacoes_selecionadas'] ?: get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'arf_id'), 'sie_valor') ?: null;
				$data ['tipos_selecionados'] = $data ['tipos_selecionados'] ?: get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'que_tipo'), 'sie_valor') ?: null;
				$data ['simulados_associados_selecionados'] = $data ['simulados_associados_selecionados'] ?: get_coluna($this->simulado_model->listar_simulados_associados($simulado_id), 'sim_associado_id') ?: null;
				$data ['status_selecionados'] = $data ['status_selecionados'] ?: get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'que_status'), 'sie_valor') ?: null;
				$data ['sim_excluir_questoes_selecionados'] = get_coluna($this->simulado_model->get_exclusoes_simulado($simulado_id, 'sim_excluir_questoes'), 'sie_valor');
			}

			$data['array_multiselect'] = array(
					array('ban_ids', $data['bancas'], $data['bancas_selecionadas']),
					array('org_ids', $data['orgaos'], $data['orgaos_selecionados']),
					array('car_ids', $data['cargos'], $data['cargos_selecionados']),
					array('pro_anos', $data['anos'], $data['anos_selecionados']),
					array('esc_ids', $data['escolaridades'], $data['escolaridades_selecionadas']),
					// 				array('dis_ids', $data['disciplinas'], $data['disciplinas_selecionadas']),
					array('ass_ids', $data['assuntos'], $data['assuntos_selecionados']),
					array('arf_ids', $data['formacoes'], $data['formacoes_selecionadas']),
					array('ara_ids', $data['atuacoes'], $data['atuacoes_selecionadas']),
					array('que_statuses', $data['statuses'], $data['statuses_selecionadas']),
					array('que_tipos', $data['tipos'], $data['tipos_selecionados'], true),
					array('que_dificuldades', $data['dificuldades'], $data['dificuldades_selecionadas']),
					array('sim_ids', $data['simulados'], $data['simulados_selecionados'])

			);

			preencher_formulario_filtro($filtro_sessao, $data);

// 			if(!$data['is_resultado']) {
				foreach ($data['questoes'] as &$questao) {
					$questao['total_comentarios'] = count($this->comentario_model->listar_por_questao($questao['que_id']));
				}
// 			}

			$data['max_questoes'] = $this->simulado_model->get_maximo_questoes_disciplina($simulado_id, $disciplina_id);
			$data['atual_questoes'] = $this->simulado_model->get_atual_questoes_disciplina($simulado_id, $disciplina_id);
			$data['permitidas_questoes'] = $data['max_questoes'] - $data['atual_questoes'];
			$data['pode_adicionar'] = $data['atual_questoes'] < $data['max_questoes'] ? true : false;
		}
		else {
			//unset($_SESSION['filtro_revisar']);
		}

		$data['filtro'] = $filtro_sessao;
		$data['is_revisar'] = true;
		$data['disciplina_id'] = $disciplina_id;
		$data['simulado_id'] = $simulado_id;
		$data['simulado'] = $this->simulado_model->get_by_id($simulado_id);

		$data['filtros_selecionados'] = get_filtros_selecionados($filtro, array('que_tipos'));
		$data['has_filtro'] = count($data['filtros_selecionados']) > 0 && !nenhum_filtro_aplicado($filtro);

		$data['busca_query_string'] = get_query_string($_GET, 'buscar');

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_revisar_questoes_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function excluir_questoes_de_simulado($simulado_id, $disciplina_id)
	{
		tem_acesso(array(
				ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR, PROFESSOR, CONSULTOR
		), ACESSO_NEGADO);


		$questoes_ids = $this->input->post('que_ids');

		if($questoes_ids) {
			foreach ($questoes_ids as $questao_id) {
				$this->simulado_model->excluir_questao_de_simulado($simulado_id, $questao_id);
			}

			$this->simulado_model->checar_consistencia($simulado_id);

			set_mensagem(SUCESSO, 'Questões removidas do simulado com sucesso!');
		}
		else {
			set_mensagem(ERRO, 'É necessário selecionar pelo menos uma questão!');
		}

		$query_string = get_query_string($_GET, 'buscar');

		redirect(get_revisar_questoes_url($simulado_id, $disciplina_id) . $query_string);
	}

	public function adicionar_questoes_ao_simulado($simulado_id, $disciplina_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR, PROFESSOR, CONSULTOR
		), ACESSO_NEGADO);

		$questoes_ids = $this->input->post('que_ids');

		if($questoes_ids) {
			$max_questoes = $this->simulado_model->get_maximo_questoes_disciplina($simulado_id, $disciplina_id);
			$atual_questoes = $this->simulado_model->get_atual_questoes_disciplina($simulado_id, $disciplina_id);

			if( ($atual_questoes + count($questoes_ids)) > $max_questoes) {
				$tentando = count($questoes_ids) == 1 ? "1 questão" : count($questoes_ids) . " questões";
				$permitido = $max_questoes - $atual_questoes;

				set_mensagem(ERRO, "Você está tentando adicionar {$tentando}, mas este simulado só suporta mais {$permitido}.");
// 				redirect(get_adicionar_questoes_ao_simulado_url($simulado_id, $disciplina_id));
			}
			else {
				foreach ($questoes_ids as $questao_id) {
					$this->simulado_model->adicionar_questao_ao_simulado($simulado_id, $questao_id);
				}

				$this->simulado_model->checar_consistencia($simulado_id);

				set_mensagem(SUCESSO, 'Questões adicionadas ao simulado com sucesso!');
			}
		}
		else {
			set_mensagem(ERRO, 'É necessário selecionar pelo menos uma questão!');
		}

		$query_string = get_query_string($_GET, 'buscar');

		redirect(get_revisar_questoes_url($simulado_id, $disciplina_id) . $query_string);
	}

	public function inativar_revisao($simulado_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->simulado_model->atualizar_status_revisao($simulado_id, 0);
		set_mensagem(SUCESSO, 'Revisão inativada com sucesso!');
		redirect(get_listar_simulados_url(TRUE));
	}

	public function ativar_revisao($simulado_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->simulado_model->atualizar_status_revisao($simulado_id, ATIVO);
		set_mensagem(SUCESSO, 'Revisão ativada com sucesso!');
		redirect(get_listar_simulados_url(TRUE));
	}

	public function alterar_status_simulado($simulado_id){

		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$simulado = $this->simulado_model->get_by_id($simulado_id);

		if($simulado['sim_status'] == ATIVO){
			self::tornar_simulado_inconsistente($simulado_id);
		}else if($simulado['sim_status'] == INCONSISTENTE){
			self::inativar_simulado($simulado_id);
		}else if($simulado['sim_status'] == INATIVO){
			self::ativar_simulado($simulado_id);
		}

	}

	public function tornar_simulado_inconsistente($simulado_id, $is_redireciona = TRUE){
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->simulado_model->atualizar_status($simulado_id, INCONSISTENTE);
		if($is_redireciona == TRUE){
			set_mensagem(SUCESSO, 'Simulado foi tornado inconsistente com sucesso!');
			redirect(get_listar_simulados_url(TRUE));
		}
	}

	public function inativar_simulado($simulado_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->simulado_model->atualizar_status($simulado_id, INATIVO);
		set_mensagem(SUCESSO, 'Simulado inativado com sucesso!');
		redirect(get_listar_simulados_url(TRUE));
	}

	public function ativar_simulado($simulado_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		if($this->simulado_model->is_simulado_consistente($simulado_id)) {

			$simulado = $this->simulado_model->get_by_id( $simulado_id );
			
			//Só ativa o simulado se ele possuir um produto válido
			$produto = wc_get_product($simulado['prd_id']);
			if($produto && $produto->post->post_status == 'publish')
			{
				$this->simulado_model->atualizar_status($simulado_id, ATIVO);
				set_mensagem(SUCESSO, 'Simulado ativado com sucesso!');
			}
			else
			{
				set_mensagem(ERRO, 'Não é possível ativar este simulado por não possuir um produto válido ou publicado');	
			}

		}
		else {
			set_mensagem(ERRO, 'Não é possível ativar este simulado por possuir menos questões que o previsto');
		}
		redirect(get_listar_simulados_url(TRUE));
	}

	public function marcar_todas_como_tratadas()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		if($this->input->post('submit-tratar-todas')) {
			$ids = $this->input->post('imagens_a_tratar');

			if($ids) {
				foreach ($ids as $id) {
					$this->questao_model->marcar_questao_imagem_como_tratada($id);
					self::marcar_questao_como_ativa($id);
				}
				set_mensagem(SUCESSO, 'Questões marcadas como tratadas com sucesso!');
			}
			else {
				set_mensagem(ERRO, 'O filtro atual não contém imagens.');
			}

		}

	}

	public function listar_imagens_nao_tratadas($offset = 0)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		self::marcar_todas_como_tratadas();

		if($this->input->post('submit') && $this->input->post('que_id')) {
			$questao_id = $this->input->post('que_id');

			$data['total'] = $this->questao_model->contar_imagens_de_questao($questao_id);
			$data['imagens'] = $this->questao_model->get_questao_imagens($questao_id);

			$data['questao_id'] = $questao_id;
		}
		else {
			$data['total'] = $this->questao_model->contar_questao_imagens();

			$limit = 20;

			$config = pagination_config(get_listar_imagens_nao_tratadas_url(), $data['total'] , $limit);
	        $this->pagination->initialize($config);
	        $data["offset"] = $offset;
			$data["links"] = $this->pagination->create_links();

			$data['imagens'] = $this->questao_model->listar_questao_imagens(false, $limit, $offset);
		}

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_imagens_nao_tratadas_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function listar_imagens_tratadas()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$data['include_daterangepicker'] = true;

		$inicio = $this->input->post('inicio') ? $this->input->post('inicio') : date('Y-m-d', strtotime('-1 month'));
		$fim = $this->input->post('fim') ? $this->input->post('fim') : date('Y-m-d');
		$usuario_id = $this->input->post('usuario_id') ? $this->input->post('usuario_id') : null;

		$data['inicio'] = converter_para_ddmmyyyy($inicio);
		$data['fim'] = converter_para_ddmmyyyy($fim);

		$data['imagens'] = $this->questao_model->listar_todas_imagens_tratadas($inicio, $fim, $usuario_id);
		$data['usuarios'] = get_usuarios_combo($this->questao_model->listar_usuarios_tratadores());
		$data['usuario_id'] = $usuario_id;

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_imagens_tratadas_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function marcar_imagem_como_tratada($imagem_id, $offset)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$this->questao_model->marcar_questao_imagem_como_tratada($imagem_id);
		self::marcar_questao_como_ativa($imagem_id);

		redirect(get_listar_imagens_nao_tratadas_url($offset));
	}

	public function marcar_questao_como_ativa($imagem_id)
	{
		$imagem = $this->questao_model->get_questao_imagem($imagem_id);

		// conta imagens não tratadas
		if($this->questao_model->contar_imagens_de_questao($imagem['que_id']) == 0) {
			$this->questao_model->marcar_questao_como_ativa($imagem['que_id']);
			
			//Invalida o cache de questões após cada alteração em questões
			memcached_remover_cache_questoes();
		}

	}

	public function confirmar_tratar_imagem($imagem_id, $offset = null)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$data['imagem'] = $this->questao_model->get_questao_imagem($imagem_id);

		$pre_imagem = explode("/", $data['imagem']['qim_url']);
		$pre_imagem[count($pre_imagem) - 1] = "pre_" . $pre_imagem[count($pre_imagem) - 1];
		$pre_imagem = implode("/", $pre_imagem);
		$data['pre_imagem'] = $pre_imagem;

		if($this->input->post()) 
		{
			unlink( $_SERVER['DOCUMENT_ROOT'] . $data['imagem']['qim_url'] );
			rename( $_SERVER['DOCUMENT_ROOT'] . $pre_imagem, $_SERVER['DOCUMENT_ROOT'] . $data['imagem']['qim_url'] );

			$this->questao_model->marcar_questao_imagem_como_tratada($imagem_id);
			self::marcar_questao_como_ativa($imagem_id);

			redirect(get_listar_imagens_nao_tratadas_url($offset));

		}

		$data['offset'] = $offset;

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/questoes/imagens/confirmar_tratar_imagem', $data);
		$this->load->view(get_admin_footer_view_url(), $data);

	}

	public function tratar_imagem($imagem_id, $offset = null)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$data['include_cropper'] = true;
		$data['imagem'] = $this->questao_model->get_questao_imagem($imagem_id);
		
		if($this->input->post()) {

			$this->load->library('image_lib');

			$pre_imagem = explode("/", $data['imagem']['qim_url']);
			$pre_imagem[count($pre_imagem) - 1] = "pre_" . $pre_imagem[count($pre_imagem) - 1];
			$pre_imagem = implode("/", $pre_imagem);

			$config['image_library'] = 'gd2';
			$config['source_image'] = $_SERVER['DOCUMENT_ROOT'] . $data['imagem']['qim_url'];
			$config['new_image'] = $_SERVER['DOCUMENT_ROOT'] . $pre_imagem;
			$config['x_axis'] = $this->input->post('x');
			$config['y_axis'] = $this->input->post('y');
			$config['maintain_ratio'] = FALSE;
			$config['width'] = $this->input->post('width');
			$config['height'] = $this->input->post('height');
			
			$this->image_lib->initialize($config);
			//Forcei aqui abaixo porque não estava fazendo o crop
			$this->image_lib->width = $this->input->post('width');
			$this->image_lib->height = $this->input->post('height');
			
			if (!$this->image_lib->crop()){
				set_mensagem_flash( ERRO, 'Erro: ' . $this->image_lib->display_errors('', '') );
				$this->image_lib->clear();
			}
			else {
				$this->image_lib->clear();
				redirect( get_confirmar_tratar_imagem_url( $imagem_id, $offset ) );
			}
		}

		$data['offset'] = $offset;

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/questoes/imagens/tratar_imagem', $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function listar_produtos($categoria_id = null)
	{
		tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING), ACESSO_NEGADO);

		$data['include_chosen'] = true;
		$data['include_validade'] = true;

		$data['cadernos_produtos'] = $this->caderno_model->listar_cadernos_produtos($categoria_id);
		foreach ($data['cadernos_produtos'] as &$caderno) {
			$caderno['questoes_certas'] = $this->caderno_model->get_total_resolvidas($caderno['cad_id'], get_current_user_id(), CERTO);
			$caderno['questoes_erradas'] = $this->caderno_model->get_total_resolvidas($caderno['cad_id'], get_current_user_id(), ERRADO);
			$caderno['total_questoes'] = $this->caderno_model->get_total_questoes($caderno['cad_id']);
			$caderno['total_questoes_respondidas'] = $caderno['questoes_certas'] + $caderno['questoes_erradas'];
			$caderno['total_questoes_nao_respondidas'] = $caderno['total_questoes'] - $caderno['total_questoes_respondidas'];
		}

		$data['categorias'] = $this->caderno_model->listar_categorias_cp();
		$data['categorias_cp_combo'] = get_categoria_cp_multiselect($this->caderno_model->listar_categorias_cp());

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_cadernos_produtos_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function editar_produto($caderno_produto_id = null) {
		tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING), ACESSO_NEGADO);

		if ($this->input->post ( 'salvar' )) {
			$this->form_validation->set_error_delimiters ( '<span>', '</span>' );
			$this->form_validation->set_rules ( 'cad_id', 'Nome', 'required' );
			$this->form_validation->set_rules ( 'produto_id', 'Nome', 'required' );
			$this->form_validation->set_message ( 'required', '%s é obrigatório.' );

			if ($this->form_validation->run () === true) {
				$data ['cad_id'] = $this->input->post('cad_id');
				$data ['produto_id'] = $this->input->post('produto_id');

				if(is_null($caderno_produto_id)) {
					$this->caderno_model->salvar_caderno_produto($data);
					set_mensagem_flash( 'sucesso', 'Produto cadastrado com sucesso!' );
				}
				else {
					$data['cpr_id'] = $caderno_produto_id;
					$this->caderno_model->atualizar_caderno_produto($data);
					set_mensagem_flash( 'sucesso', 'Produto atualizado com sucesso!' );
				}

				redirect ( get_listar_cadernos_produtos_url() );
			}
		}

		$data['foi_vendido'] = 0;
		if(!is_null($caderno_produto_id)) {
			$data['caderno_produto'] = $this->caderno_model->get_caderno_produto($caderno_produto_id);

			$data['foi_vendido'] = contar_usuarios_que_compraram($data['caderno_produto']['produto_id']);
		}

		$data['cadernos_combo'] = get_caderno_combo($this->caderno_model->listar_cadernos_comercializaveis(), "Selecione um caderno...");
		$data['produtos_combo'] = get_produto_combo(get_todos_produtos_e_coaching_posts());

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_editar_caderno_produto_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function inativar_caderno_produto($produto_caderno_id)
	{
		tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING), ACESSO_NEGADO);

		$this->caderno_model->inativar_caderno_produto($produto_caderno_id);

		set_mensagem_flash( 'sucesso', 'Produto inativado com sucesso!' );
		redirect(get_listar_cadernos_produtos_url());
	}

	public function ativar_caderno_produto($produto_caderno_id)
	{
		tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING), ACESSO_NEGADO);

		$this->caderno_model->ativar_caderno_produto($produto_caderno_id);

		set_mensagem_flash( 'sucesso', 'Produto ativado com sucesso!' );
		redirect(get_listar_cadernos_produtos_url());
	}

	public function editar_categoria_produto()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, REVISOR
		), ACESSO_NEGADO);

		if($this->input->post()) {
			$data['ccp_id'] = $this->input->post('ccp_id');
			$data['ccp_nome'] = $this->input->post('ccp_nome');

			$this->caderno_model->atualizar_categoria_produto($data);
			set_mensagem_flash(SUCESSO, 'Categoria atualizado com sucesso!');
		}
		redirect(get_listar_cadernos_produtos_url());
	}

	function adicionar_nova_categoria_cp()
	{
		tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING), ACESSO_NEGADO);

		if($this->input->post()) {
			$data = array(
				'ccp_nome' => $this->input->post('nome'),
			);
			$categoria_id = $this->caderno_model->adicionar_categoria_cp($data);
			self::adicionar_categorias_cp_em_produto($this->input->post('caderno_produto_id'), array($categoria_id));
		}
	}

	private function adicionar_categorias_cp_em_produto($caderno_produto_id, $categorias_ids)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->caderno_model->adicionar_categorias_cp_em_produto($caderno_produto_id, $categorias_ids);

		set_mensagem_flash(SUCESSO,'Categoria(s) adicionada(s) ao produto');
		redirect(get_listar_cadernos_produtos_url());
	}

	public function adicionar_categoria_cp_existente()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		if($this->input->post()) {
			$produto_id = $this->input->post('caderno_produto_id');
			$categorias_ids = $this->input->post('categorias_ids');
			self::adicionar_categorias_cp_em_produto($produto_id, $categorias_ids);
		}
	}

	function excluir_categoria_produto() {

		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		if($this->input->post()) {
			$this->caderno_model->excluir_categoria_produto($this->input->post('del_cat_item_id'));
			set_mensagem_flash(SUCESSO, 'Categoria excluída com sucesso!');
		}
		redirect(get_listar_cadernos_produtos_url());
	}

	public function xhr_tratar_imagem()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$imagem_id = $this->input->post('imagem_id');
		$data['imagem'] = $this->questao_model->get_questao_imagem($imagem_id);

		$this->load->library('image_lib');

		$config['image_library'] = 'gd2';
		$config['source_image'] = $_SERVER['DOCUMENT_ROOT'] . $data['imagem']['qim_url'];
		$config['x_axis'] = $this->input->post('x');
		$config['y_axis'] = $this->input->post('y');
		$config['maintain_ratio'] = FALSE;
		$config['width'] = $this->input->post('width');
		$config['height'] = $this->input->post('height');

		$this->image_lib->initialize($config);
		if (!$this->image_lib->crop()){
			echo $this->image_lib->display_errors();
		}
		else {
			$this->questao_model->marcar_questao_imagem_como_tratada($imagem_id);
			self::marcar_questao_como_ativa($imagem_id);
		}
		$this->image_lib->clear();
	}

	public function apagar_filtro_simulado()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$_SESSION['simulado_filtro'] = null;
	}

	public function adicionar_filtro_simulado($simulado_id = null)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$filtro = array();
		$data = array();
		if(is_null($simulado_id)){
			adicionar_filtro($filtro, $data, $this);
		}else{
			self::adicionar_filtro_from_db($filtro, $simulado_id);
		}

		$_SESSION['simulado_filtro'] = serialize($filtro);
	}

	private function adicionar_filtro_from_db(&$filtro, $simulado_id){

		$exclusoes = $this->simulado_model->get_exclusoes_simulado($simulado_id);

		foreach($exclusoes as $exclusao){

			if(!$filtro[$exclusao['sie_campo']]){
				$filtro[$exclusao['sie_campo']] = array();
			}

			array_push($filtro[$exclusao['sie_campo']], $exclusao['sie_valor']);

		}

		$filtro['excluir_sim_id'] = $simulado_id;

	}

	public function contar_questoes_por_filtro_e_disciplina($disciplina_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$filtro = unserialize($_SESSION['simulado_filtro']);
		$filtro['dis_ids'] = array($disciplina_id);

		echo $this->questao_model->get_total_resultado( $filtro );
	}

	/***********************************************************************
	 *
	 * Ranking
	 *
	 ***********************************************************************/

	public function listar_rankings()
	{
		tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR), ACESSO_NEGADO);

		$data['include_data_tables'] = true;
		$rankings = $this->ranking_model->listar_todos();

		$data['thead_array'] = array('Nome', 'Ações');
		$data['tbody_array'] = get_tabela_rankings($rankings);
		$data['tfoot_array'] = $data['thead_array'];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_rankings_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Exclui ranking
	 */
	public function excluir_ranking($ranking_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$this->ranking_model->excluir ( $ranking_id );
		$this->session->set_flashdata ( 'sucesso', 'Ranking excluído com sucesso!' );
		redirect ( get_listar_rankings_url () . '/');
	}

	public function editar_ranking($id = null, $pagina = 1)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->load->helper('combo_helper');

		$data['include_chosen'] = true;
		$data['include_jquery_ui'] = true;
		$data['include_mask_plugin'] = true;

		$data['id'] = $id;
		$data['pagina'] = $pagina;

		if($id) {
			$data['ranking'] = $this->ranking_model->get_by_id($id);
		}

		switch ($pagina) {

			case 1: {

				/*****************************************************************
				 * Passo 1
				 *****************************************************************/
				if($this->input->post()) {

					$this->form_validation->set_error_delimiters('<span>', '</span>');
					$this->form_validation->set_rules('ran_nome', 'Nome', "required|max_length[200]");
					$this->form_validation->set_rules('simulados_ids[]', 'Campo Simulados', "required");
					$this->form_validation->set_message('required', '%s é obrigatório.');
					$this->form_validation->set_message('max_length', '%s não pode passar de 200 caracteres.');
					$this->form_validation->set_message('edit_unique', '%s já existe no cadastro.');

					$ran_nome = $this->input->post('ran_nome');
					$sim_ids = $this->input->post('simulados_ids');
					$ran_media_ponderada = $this->input->post('ran_media_ponderada') ?: 0;
					$ranking = array('ran_id' => $id, 'ran_nome' => $ran_nome, 'ran_media_ponderada' => $ran_media_ponderada);

					if ($this->form_validation->run() === true)
					{
						$ranking_id = $this->ranking_model->salvar_ranking($ranking);

						$this->ranking_model->remover_diferenca_ranking_simulados($ranking_id, $sim_ids);
						$this->ranking_model->salvar_ranking_simulados($ranking_id, $sim_ids);
						redirect(get_editar_ranking_url($ranking_id, 2));
					}
				}

				$data['simulados'] = get_simulado_multiselect($this->ranking_model->listar_simulados_disponiveis());
				$selecionados = $id ? $this->ranking_model->listar_simulados($id) : array();

				if($selecionados) {
					$data['simulados_selecionados'] = array();

					foreach ($selecionados as $selecionado) {
						$data['simulados'][$selecionado['sim_id']] = $selecionado['sim_nome'];
						$data['simulados_selecionados'][] = $selecionado['sim_id'];
					}
				}

				break;
			}

			case 2: {

				/*****************************************************************
				 * Passo 2
				 *****************************************************************/


				$this->form_validation->set_error_delimiters('<span>', '</span>');
				$this->form_validation->set_rules('rag_nome', 'Nome do Grupo', "required|max_length[200]");
				$this->form_validation->set_message('required', '%s é obrigatório.');
				$this->form_validation->set_message('max_length', '%s não pode passar de 200 caracteres.');
				$this->form_validation->set_message('edit_unique', '%s já existe no cadastro.');


				if($this->input->post()) {
					$rag_nome = $this->input->post('rag_nome');
					$grupo = array('ran_id' => $id, 'rag_nome' => $rag_nome);

					if ($this->form_validation->run() === true) {
						$this->ranking_model->salvar_grupo($grupo);
					}
				}

				$data['grupos'] = $this->ranking_model->listar_grupos($id);
				$data['thead_array'] = array('Nome do Grupo', 'Ações');
				$data['tbody_array'] = get_tabela_grupos($data['grupos']);
				$data['tfoot_array'] = $data['thead_array'];

				break;
			}

			case 3: {

				/*****************************************************************
				 * Passo 3
				 *****************************************************************/

				if($this->input->post()) {

					$grupos = $this->input->post('grupos');

					if($grupos) {

						foreach ($grupos as $chave => $dis_ids) {

							if($chave == 'undefined') {
								$chave = null;
							}

							$dis_ids = str_replace("\\", "",$dis_ids);
							$dis_ids_a = json_decode($dis_ids);

							if($dis_ids_a) {
								$this->ranking_model->remover_diferenca_disciplina_ranking_grupo($id, $chave, $dis_ids_a);

								foreach ($dis_ids_a as $dis_id) {

									if(!$this->ranking_model->get_disciplina_ranking($id, $chave, $dis_id)) {
										$this->ranking_model->salvar_disciplina_ranking($id, $chave, $dis_id);
									}

								}
							}

						}

					}

					redirect(get_editar_ranking_url($id, 4));
				}

				$grupos = $this->ranking_model->listar_grupos($id);
				$simulados = $this->ranking_model->listar_simulados($id);

				$sem_grupos = array();
				$com_grupos = array();

				foreach ($grupos as $grupo) {
					$com_grupos[$grupo['rag_id']] = array('nome' => $grupo['rag_nome'], 'itens' => array());
				}

				foreach ($simulados as $simulado) {
					$disciplinas = $this->simulado_model->listar_disciplinas_por_ordem($simulado['sim_id']);

					foreach ($disciplinas as $disciplina) {

						$achou_grupo = false;

						$item = $this->disciplina_model->get_by_id($disciplina['dis_id']);

						foreach ($grupos as $grupo) {

							if($this->ranking_model->get_disciplina_ranking($id, $grupo['rag_id'], $disciplina['dis_id'])) {
								$achou_grupo = true;

								if(!in_array($item, $com_grupos[$grupo['rag_id']]['itens'])) {
									array_push($com_grupos[$grupo['rag_id']]['itens'], $item);
									break;
								}

							}
						}

						if(!$achou_grupo) {

							if(!in_array($item, $sem_grupos)) {
								array_push($sem_grupos, $item);
							}
						}

					}

				}

				$data['sem_grupos'] = $sem_grupos;
				$data['com_grupos'] = $com_grupos;
				break;
			}

			case 4: {

			/*****************************************************************
			 * Passo
			 *****************************************************************/

				if($this->input->post()) {

					$ran_corte = $this->input->post("ran_corte") ? numero_americano($this->input->post("ran_corte")) : null;
					$ran_peso_erros = $this->input->post("ran_peso_erros") ? numero_americano($this->input->post("ran_peso_erros")) : null;
					$this->ranking_model->alterar_corte_peso_erro_ranking($id, $ran_corte, $ran_peso_erros);

					$simulados = $this->input->post('simulados');

					foreach ($simulados as $sim_id => $simulado) {

						$simulado_ranking = array(
							'ran_id' => $id,
							'sim_id' => $sim_id,
							'ras_peso' => $simulado['peso'] ? numero_americano($simulado['peso']) : 1,
							'ras_corte' => $simulado['corte'] ? numero_americano($simulado['corte']) : 0
						);

						$this->ranking_model->atualizar_simulado_ranking($simulado_ranking);
					}

					$grupos = $this->input->post('grupos');

					foreach ($grupos as $rag_id => $grupo) {
						$grupo_ranking = array(
							'ran_id' => $id,
							'rag_id' => $rag_id,
							'rag_peso' => $grupo['peso'] ? numero_americano($grupo['peso']) : 1,
							'rag_corte' => $grupo['corte'] ? numero_americano($grupo['corte']) : 0
						);

						$this->ranking_model->atualizar_grupo_ranking($grupo_ranking);
					}

					$disciplinas = $this->input->post('disciplinas');

					foreach ($disciplinas as $dis_id => $disciplina) {
						$tem_default = !$disciplina['rag_id'] || $disciplina['peso'] || $disciplina['corte'];
						$disciplina_ranking = array(
							'ran_id' => $id,
							'rag_id' => $disciplina['rag_id'] ?: null,
							'dis_id' => $dis_id,
							'rad_peso' => $disciplina['peso'] ? numero_americano($disciplina['peso']) : ( !$tem_default ? null : 1 ),
							'rad_corte' => $disciplina['corte'] ? numero_americano($disciplina['corte']) : ( !$tem_default ? null : 0 )
						);

						$this->ranking_model->atualizar_disciplina_ranking($disciplina_ranking);
					}
					set_mensagem_flash('sucesso', 'Ranking atualizado com sucesso!');
					redirect(get_listar_rankings_url());

				}

				$data['ranking'] = $this->ranking_model->get_by_id($id);
				$data['simulados'] = $this->ranking_model->listar_simulados($id);
				$data['grupos'] = $this->ranking_model->listar_grupos($id);
				//$data['disciplinas'] = $this->ranking_model->listar_disciplinas_sem_grupo($id);
				$data['disciplinas'] = $this->ranking_model->listar_disciplinas_por_ranking($id);

				break;
			}
		}

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_editar_ranking_view_url($pagina), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	/**
	 * Exclui grupo
	 */
	public function excluir_grupo($grupo_id) {
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$grupo = $this->ranking_model->get_grupo($grupo_id);

		$this->ranking_model->excluir_grupo($grupo_id);
		$this->session->set_flashdata ('sucesso', 'Grupo excluído com sucesso!' );
		redirect ( get_editar_ranking_url ($grupo['ran_id'], 2) );
	}

	/**
	 * Salva e edita grupo
	 */

	public function editar_grupo($id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		if($this->input->post('salvar')) {
			$this->form_validation->set_error_delimiters('<span>', '</span>');
			$this->form_validation->set_rules('rag_nome', 'Nome', "required|max_length[200]");
			$this->form_validation->set_message('required', '%s é obrigatório.');
			$this->form_validation->set_message('max_length', '%s não pode passar de 200 caracteres.');

			if ($this->form_validation->run() === true)
			{
				$grupo = array(
					'rag_id' => $id,
					'rag_nome' => $this->input->post('rag_nome'),
				);

				$this->ranking_model->salvar_grupo ( $grupo );
				$this->session->set_flashdata ( 'sucesso', 'Grupo atualizado com sucesso!' );

				$grupo = $this->ranking_model->get_grupo($id);
				redirect ( get_editar_ranking_url ($grupo['ran_id'], 2) );
			}
		}

		$data['grupo'] = $this->ranking_model->get_grupo($id);

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/editar_grupo', $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function listar_questoes_repetidas()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$data['include_data_tables'] = true;

		$status = null;
		$questoes_repetidas = $this->questao_model->listar_questoes_repetidas($status);

		$data['thead_array'] = array('Questão Master', 'Questão Repetida', 'Ações');
		$data['tbody_array'] = get_tabela_questoes_repetidas($questoes_repetidas);
		$data['tfoot_array'] = $data['thead_array'];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/listar_questoes_repetidas', $data);
		$this->load->view(get_admin_footer_view_url(), $data);

	}

	public function comparar_questoes($master_id, $repetida_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$this->load->helper('simulado');

		if($this->input->post()) {

			$master = $this->input->post('master');
			$repetida = $this->input->post('repetida');
			$ignorar = $this->input->post('ignorar');

			$master_id = $this->input->post('master_id');
			$repetida_id = $this->input->post('repetida_id');
			$inativar = $this->input->post('inativar');

			if($ignorar) {

				$this->questao_model->ignorar_questao_repetida($master_id, $repetida_id, get_current_user_id(), get_data_hora_agora());

				set_mensagem_flash( 'sucesso', 'Caso ignorado com sucesso' );
			}
			else {
				$inverter_master = isset($repetida) && $repetida ? true : false;

				$this->questao_model->atualizar_questao_repetida($master_id, $repetida_id, $inativar, get_current_user_id(), get_data_hora_agora(), $inverter_master);

				/**
				 * Associar questão master a prova das slave
				 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2326
				 */

				$_master = $master_id;
				$_repetida = $repetida_id;

				if($inverter_master) {
					$_master = $repetida_id;
					$_repetida = $master_id;
				}

				if($repetidas_provas = $this->questao_model->listar_questao_prova_por_questao_id($_repetida)) {

					foreach ($repetidas_provas as $repetida_prova) {

						$repetida_prova_id = $repetida_prova['pro_id'];

						if(!$this->questao_model->get_questao_prova($_master, $repetida_prova_id)) {
							$this->questao_model->salvar_questao_prova($_master, $repetida_prova_id);
						}
					}

				}

				set_mensagem_flash( 'sucesso', 'Comparação resolvida com sucesso' );
			}

			redirect('/questoes/admin/listar_questoes_repetidas');
		}

		$master = $this->questao_model->get_by_id($master_id, true);
		$master_opcoes = $this->questao_model->listar_opcoes($master_id);
		$data['master'] = get_questao_para_comparacao($master, $master_opcoes);
		$data['comentarios_alunos_master'] = $this->comentario_model->listar_por_questao($master_id, false);
		$data['comentarios_professores_master'] = $this->comentario_model->listar_por_questao($master_id, true);

		$repetida = $this->questao_model->get_by_id($repetida_id, true);
		$repetida_opcoes = $this->questao_model->listar_opcoes($repetida_id);
		$data['repetida'] = get_questao_para_comparacao($repetida, $repetida_opcoes);
		$data['comentarios_alunos_repetida'] = $this->comentario_model->listar_por_questao($repetida_id, false);
		$data['comentarios_professores_repetida'] = $this->comentario_model->listar_por_questao($repetida_id, true);

		$data['comparacao'] = get_questoes_comparacao($master, $master_opcoes, $repetida, $repetida_opcoes);

		$data['master_id'] = $master_id;
		$data['repetida_id'] = $repetida_id;

		$data['include_chosen'] = true;
		$data ['include_jstree'] = true;

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/comparar_questoes', $data);
		$this->load->view(get_admin_footer_view_url(), $data);

	}

	public function editar_configs_questoes_repetidas()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$data['include_icheck'] = true;
		$data['include_chosen'] = true;

		if($this->input->post()) {
			$percentual_similaridade = $this->input->post('config_percentual_similaridade') ?: 0;
			$base_texto = $this->input->post('config_base_texto') ?: NAO;
			$enunciado = $this->input->post('config_enunciado') ?: NAO;
			$ignorar_html = $this->input->post('config_ignorar_html') ?: NAO;
			$mesma_disciplina = $this->input->post('config_mesma_disciplina') ?: NAO;
			$mesma_banca = $this->input->post('config_mesma_banca') ?: NAO;
			$disciplinas = $this->input->post('config_disciplinas') ? implode(",", $this->input->post('config_disciplinas')) : "";
			$anos = $this->input->post('config_anos') ? implode(",", $this->input->post('config_anos')) : "";
			$qtde_questoes = $this->input->post('config_qtde_questoes') ?: 0;

			$this->configuracao_model->set('qr_percentual_similaridade', $percentual_similaridade);
			$this->configuracao_model->set('qr_base_texto', $base_texto);
			$this->configuracao_model->set('qr_enunciado', $enunciado);
			$this->configuracao_model->set('qr_ignorar_html', $ignorar_html);
			$this->configuracao_model->set('qr_mesma_disciplina', $mesma_disciplina);
			$this->configuracao_model->set('qr_mesma_banca', $mesma_banca);
			$this->configuracao_model->set('qr_disciplinas', $disciplinas);
			$this->configuracao_model->set('qr_anos', $anos);
			$this->configuracao_model->set('qr_qtde_questoes', $qtde_questoes);

			set_mensagem_flash( 'sucesso', 'Configurações gravadas com sucesso' );
		}

		$data['disciplinas'] = get_disciplina_combo($this->disciplina_model->listar_todas());
		$data['anos'] = get_ano_multiselect($this->prova_model->listar_anos());

		$data['config_percentual_similaridade'] = $this->configuracao_model->get('qr_percentual_similaridade') ?: 100;
		$data['config_base_texto'] = $this->configuracao_model->get('qr_base_texto') ?: SIM;
		$data['config_enunciado'] = $this->configuracao_model->get('qr_enunciado') ?: SIM;
		$data['config_ignorar_html'] = $this->configuracao_model->get('qr_ignorar_html') ?: NAO;
		$data['config_mesma_disciplina'] = $this->configuracao_model->get('qr_mesma_disciplina') ?: SIM;
		$data['config_mesma_banca'] = $this->configuracao_model->get('qr_mesma_banca') ?: SIM;
		$data['config_qtde_questoes'] = $this->configuracao_model->get('qr_qtde_questoes') ?: 100;
		$data['config_disciplinas'] = $this->configuracao_model->get('qr_disciplinas') ? explode(",", $this->configuracao_model->get('qr_disciplinas')) : [];
		$data['config_anos'] = $this->configuracao_model->get('qr_anos') ? explode(",", $this->configuracao_model->get('qr_anos')) : [];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/editar_configs_questoes_repetidas', $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function questoes_repetidas_execucoes()
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$execucoes = $this->questao_model->listar_questoes_repetidas_execucoes();

		$data['thead_array'] = array('ID', 'Data/hora', 'Similaridade', 'Texto Base', 'Enunciado', 'Ignorar HTML', 'Mesma Disciplina', 'Mesma Banca', 'Disciplinas', 'Anos', 'Tempo Gasto (seg)', 'Executadas', 'Repetidas');
		$data['tbody_array'] = get_tabela_questoes_repetidas_execucoes($execucoes);
		$data['tfoot_array'] = $data['thead_array'];

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/questoes_repetidas_execucoes', $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function questoes_repetidas_execucoes_detalhes($execucao_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$data['execucao_id'] = $execucao_id;

		$data['questoes_executadas'] = $this->questao_model->listar_questoes_repetidas_executadas($execucao_id);
		$data['questoes_repetidas']= $this->questao_model->listar_questoes_repetidas_executadas($execucao_id, true);

		$data['num_questoes_executadas'] = count($data['questoes_executadas']);
		$data['num_questoes_repetidas'] = count($data['questoes_repetidas']);

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/questoes_repetidas_execucoes_detalhes', $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function rotulos_materias($simulado_id)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		$data['include_jquery_ui'] = true;

		if($_POST['salvar']) {
			$rotulos = $_POST['rotulos'];

			foreach ($rotulos as $rotulo_id => $questoes) {

				$questoes = str_replace("\\", "", $questoes);
				$questoes_a = json_decode($questoes);

				if($questoes_a) {

					foreach ($questoes_a as $questao_id) {
						$this->simulado_model->atualizar_rotulo_em_questao($simulado_id, $questao_id, $rotulo_id);
					}

				}

			}

			set_mensagem_flash('sucesso', 'Rótulos salvos com suceso!');
			redirect(get_rotulos_materias_simulado_url($simulado_id));
		}

		self::init_rotulos($simulado_id);

		$data['rotulos'] = $this->simulado_model->listar_rotulos($simulado_id);
		foreach ($data['rotulos'] as &$rotulo) {
			if($questoes = $this->simulado_model->listar_questoes_por_rotulo($rotulo['sro_id'])) {
				$rotulo['questoes'] = [];

				foreach ($questoes as $questao) {
					array_push($rotulo['questoes'], $questao['que_id']);
				}
			}
		}

		$data['simulado_id'] = $simulado_id;

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/rotulos_materias', $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function editar_rotulo($simulado_id, $rotulo_id = null)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);

		if($this->input->post('salvar')) {
			$this->form_validation->set_error_delimiters('<span>', '</span>');
			$this->form_validation->set_rules('sro_nome', 'Nome', "required|max_length[100]");
			$this->form_validation->set_message('required', '%s é obrigatório.');
			$this->form_validation->set_message('max_length', '%s não pode passar de 100 caracteres.');

			if ($this->form_validation->run() === true)
			{
				$dados = [
					'sro_nome' => $this->input->post('sro_nome')
				];

				if($rotulo_id) {
					$dados['sro_id'] = $rotulo_id;
				}
				else {
					$dados['sro_id'] = null;
					$dados['sro_ordem'] = $this->simulado_model->get_proxima_ordem_rotulo($simulado_id);
					$dados['sim_id'] = $simulado_id;
				}

				$this->simulado_model->salvar_rotulo($dados);
				set_mensagem_flash('sucesso', 'Rótulo salvo com suceso!');

				redirect(get_rotulos_materias_simulado_url($simulado_id));
			}
		}

		$data['simulado_id'] = $simulado_id;
		$data['rotulo'] = $this->simulado_model->get_rotulo($rotulo_id);

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view('admin/editar_rotulo', $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	public function excluir_rotulo($simulado_id, $rotulo_id)
	{
		if($this->simulado_model->listar_questoes_por_rotulo($rotulo_id)) {
			set_mensagem_flash('erro', 'Existem questões associadas a esse rótulo');
		}
		else {
			$this->simulado_model->excluir_rotulo($rotulo_id);
			set_mensagem_flash('sucesso', 'Rótulo excluído com sucesso');
		}

		redirect(get_rotulos_materias_simulado_url($simulado_id));
	}

	public function correcao_assuntos($offset = 0)
	{
		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR
		), ACESSO_NEGADO);


		$data['include_data_tables'] = true;

		if($this->input->post('filtrar')) {
			$_SESSION['q_qac'] = $this->input->post('q');
			$offset = 0;
		}

		$palavra = isset($_SESSION['q_qac']) ? $_SESSION['q_qac'] : null;

		$qacs = $this->questao_model->listar_questoes_assuntos_corretor($offset, $palavra);

		$total = $this->questao_model->contar_questoes_assuntos_corretor($palavra);

		$config = pagination_config(get_correcao_assuntos_url(), $total, QUESTOES_ASSUNTOS_CORRETOR_POR_PAGINA);
		$this->pagination->initialize($config);

		$data["links"] = $this->pagination->create_links();

		$data['thead_array'] = array('Questão ID','Assuntos Antes', 'Assuntos Depois', 'Data/Hora');
		$data['tbody_array'] = get_tabela_qac($qacs);
		$data['tfoot_array'] = $data['thead_array'];


		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_correcao_assuntos_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);
	}

	private function init_rotulos($simulado_id)
	{
		if(!$this->simulado_model->listar_rotulos($simulado_id)) {
			$this->simulado_model->criar_rotulos_iniciais($simulado_id);
		}
	}

	public function listar_questoes_professor($offset = 0)
	{

		tem_acesso(array(
			ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR, PROFESSOR
		), ACESSO_NEGADO);

		KLoader::helper("FiltroHelper");

		$data['include_chosen'] = true;
		$data['include_jstree'] = true;
		$data['include_vimeo'] = true;

		// Inicialização da sessão
		if(!$_SESSION['frm_listar_questoes_professores']) {
		    $_SESSION['frm_listar_questoes_professores'] = [
				'q_disciplinas' => null,
				'q_assuntos' => null,
		        'q_professores' => null,
		        'q_aprovacao' => null,
		        'q_procedencia' => null
		    ];
		}

		// Ação pós clique no botão Filtrar
		if($this->input->post('filtrar')) {
			$_SESSION['frm_listar_questoes_professores']['q_disciplinas'] = $this->input->post('q_disciplinas');
			$_SESSION['frm_listar_questoes_professores']['q_assuntos'] = $this->input->post('q_assuntos');
		    $_SESSION['frm_listar_questoes_professores']['q_professores'] = $this->input->post('q_professores');
		    $_SESSION['frm_listar_questoes_professores']['q_aprovacao'] = $this->input->post('q_aprovacao');
		    $_SESSION['frm_listar_questoes_professores']['q_procedencia'] = $this->input->post('q_procedencia');
		    $offset = 0;
		}

		// Montagem dos parâmetros da busca
		$filtros = [
			'disciplinas' => $_SESSION['frm_listar_questoes_professores']['q_disciplinas'],
			'assuntos' => $_SESSION['frm_listar_questoes_professores']['q_assuntos'],
		    'professores' => $_SESSION['frm_listar_questoes_professores']['q_professores'],
		    'aprovacao' => $_SESSION['frm_listar_questoes_professores']['q_aprovacao'],
		    'procedencia' => $_SESSION['frm_listar_questoes_professores']['q_procedencia'],
		];

		// Administrador e coordenador acessam todas as questões
		if(!tem_acesso([ADMINISTRADOR, COORDENADOR])) {
		    $filtros['professores'] = [get_current_user_id()];
		}

		adicionar_assuntos_filhos($filtros, 'assuntos');

		// Chamada das buscas
		$questoes = $this->questao_model->listar_questoes_professor($filtros, QUESTOES_PROFESSOR_POR_PAGINA, $offset);
		$total = $this->questao_model->contar_questoes_professor($filtros);

		// Montagem da paginação
		$config = pagination_config('/questoes/admin/listar_questoes_professor', $total, QUESTOES_PROFESSOR_POR_PAGINA);
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();

		// Montagem dos dados da tabela de resultado
		$data['filtros'] = $filtros;
		$data['total'] = numero_inteiro($total);

		$data['questoes'] = $questoes;
		$data['disciplinas_combo'] = get_disciplina_multiselect($this->disciplina_model->listar_todas());
		if($filtros['disciplinas']){
			$data['assuntos_combo'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplinas($filtros['disciplinas']));
		}
		$data['professores_combo'] = get_professores_multiselect(get_professores( FALSE ));
		$data['aprovacao_combo'] = get_aprovacao_multiselect();
		$data['procedencia_combo'] = get_procedencia_multiselect();

		$this->load->view(get_admin_header_view_url(), $data);
		$this->load->view(get_listar_questoes_professor_view_url(), $data);
		$this->load->view(get_admin_footer_view_url(), $data);

	}

	public function aprovar_questao($questao_id){

		tem_acesso(array(
			ADMINISTRADOR
		), ACESSO_NEGADO);

		$questao = $this->questao_model->get_by_id($questao_id);

		if($questao['que_aprovacao'] == QUESTAO_EM_APROVACAO){
			$this->questao_model->aprovar_reprovar_questao($questao_id, TRUE);
			$this->questao_model->atualizar_ativo_inativo($questao_id, ATIVO);

			//Invalida o cache de questões após cada alteração em questões
			memcached_remover_cache_questoes();

			set_mensagem_flash( 'sucesso', 'Questão aprovada com sucesso!' );
		}else{
			set_mensagem_flash( 'erro', 'Essa questão não está em aprovação.' );
		}

		redirect(get_listar_questoes_professor_url());

	}

	public function reprovar_questao($questao_id){

		tem_acesso(array(
			ADMINISTRADOR
		), ACESSO_NEGADO);

		$questao = $this->questao_model->get_by_id($questao_id);

		if($questao['que_aprovacao'] == QUESTAO_EM_APROVACAO){
			$this->questao_model->aprovar_reprovar_questao($questao_id, false);
			$this->questao_model->atualizar_ativo_inativo($questao_id, INATIVO);

			//Invalida o cache de questões após cada alteração em questões
			memcached_remover_cache_questoes();

			set_mensagem_flash( 'sucesso', 'Questão reprovada com sucesso!' );
		}else{
			set_mensagem_flash( 'erro', 'Essa questão não está em aprovação.' );
		}

		redirect(get_listar_questoes_professor_url());

	}

	/**
	 * Aprova ou rejeita todas as questöes selecionadas
	 */

	public function aprovar_rejeitar_questoes_selecionadas()
	{

	    tem_acesso([ADMINISTRADOR, COORDENADOR], ACESSO_NEGADO);

	    $tipo = $this->input->post("hid-tipo");

	    if($questoes_ids = $this->input->post("que_ids")) {

	        foreach ($questoes_ids as $questao_id) {

	            if($tipo == QUESTAO_APROVADA) {
	                $this->questao_model->aprovar_reprovar_questao($questao_id, true);
	                $this->questao_model->atualizar_ativo_inativo($questao_id, ATIVO);

	                set_mensagem_flash( 'sucesso', 'Questões aprovadas com sucesso!' );
	            }
	            elseif($tipo == QUESTAO_REPROVADA) {
	                $this->questao_model->aprovar_reprovar_questao($questao_id, false);
	                $this->questao_model->atualizar_ativo_inativo($questao_id, INATIVO);

	                set_mensagem_flash( 'sucesso', 'Questões rejeitadas com sucesso!' );
	            }
    	    }
	    }

	    redirect(get_listar_questoes_professor_url());

	}



	/***********************************************************************
	 *
	 * XHR
	 *
	 ***********************************************************************/

	public function xhr_upload_imagem() {
		if ($_FILES['file']['name']) {
			if (!$_FILES['file']['error']) {
				$name = time();
				$ext = explode('.', $_FILES['file']['name']);
				$filename = $name . '.' . $ext[count($ext)-1];
				$destination = $_SERVER['DOCUMENT_ROOT'] .'/questoes/uploads/imagens/' . $filename; //change this directory
				$location = $_FILES["file"]["tmp_name"];
				move_uploaded_file($location, $destination);
				echo '/questoes/uploads/imagens/' . $filename;//change this URL
			}
			else
			{
				echo  $message = 'Erro ao subir a imagem:  '.$_FILES['file']['error'];
			}
		}
	}

	public function xhr_atualizar_questao_relacionada($questao_id, $questao_relacionada_id)
	{
		$this->questao_model->atualizar_questao_relacionada($questao_id, $questao_relacionada_id);
	}

	public function xhr_get_categorias_de_caderno_produto($produto_id)
	{
		$categorias = $this->caderno_model->listar_categorias_ids_de_produto($produto_id);

		$categorias_array = array();
		foreach ($categorias as $categoria) {
			array_push($categorias_array, $categoria['ccp_id']);
		}

		echo json_encode($categorias_array);
	}

	function xhr_get_categoria_produto($categoria_id)
	{
		$categoria = $this->caderno_model->get_categoria_produto($categoria_id);
		echo json_encode($categoria);
	}

	function logado()
	{
		echo get_current_user_id();
	}

}
