<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

/**
 * Controlar as requisições ajax relacionadas a Cadernos
 * 
 * @since L1
 * 
 * @author João Paulo Ferreira <jpaulofms@gmail.com>
 */
class Cadernos_xhr extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct ();
    }



	/**
	 * Adiciona uma questão a um caderno existente e associa a suas categorias selecionadas ou criadas
	 * e escreve uma mensagem de sucesso.
	 * 
	 * @since L1
	 * 
	 */
	public function adicionar_caderno_existente()
	{			
		$this->load->model("caderno_model");
		
		$questao_id = $this->input->post("que_id");
		$caderno_id = $this->input->post("cad_id");
		
		$categorias_ids = $this->input->post('categorias_ids');
		$novas_categorias = $this->input->post('novas_categorias');
		$cat_ids = $categorias_ids?:array();
		if($novas_categorias) {
			$novas_categorias_a = explode(',', $novas_categorias);
			foreach ($novas_categorias_a as $item) {
				$id = $this->caderno_model->adicionar_categoria(array(
					'usu_id' => get_current_user_id(),
					'cat_nome' => $item
				));
				array_push($cat_ids, $id);
			} 
		}

		self::adicionar_categorias_em_caderno($caderno_id, $cat_ids, FALSE, FALSE);
		self::adicionar_questao_ao_caderno($caderno_id, $questao_id);
		
		echo get_botao_resposta("Questão adicionada ao caderno com sucesso.", "resposta-adicionada.png", "resposta-adicionada");
	}

	/**
	 * Adiciona categorias em um caderno
	 * 
	 * @since L1
	 * 
	 * @param int $caderno_id ID do caderno
	 * @param int $categorias_ids IDs das categorias
	 * @param bool $excluir_atuais TRUE se for para excluir todas as questões atuais
	 * @param bool $redirecionar TRUE se for para redirecionar para a página Meus Cadernos
	 */
	private function adicionar_categorias_em_caderno($caderno_id, $categorias_ids, $excluir_atuais = TRUE, $redirecionar = TRUE)
	{	
		$this->load->model("caderno_model");

		$this->caderno_model->adicionar_categorias_em_caderno($caderno_id, $categorias_ids, $excluir_atuais);
	
		if($redirecionar) {
			set_mensagem(SUCESSO,'Categoria(s) adicionada(s) ao caderno');
			redirect(get_meus_cadernos_url());
		}
	}

	/**
	 * Adiciona uma questão ao caderno informado e adiciona filtro de questão avulsa
	 * 
	 * @since L1
	 * 
	 * @param int $caderno_id ID do caderno
	 * @param int #questao_id ID da questão
	 */
	private function adicionar_questao_ao_caderno($caderno_id, $questao_id)
	{
		$this->load->model("caderno_model");

		$this->caderno_model->adicionar_questao_em_caderno($caderno_id, $questao_id);
		$this->caderno_model->atualizar_filtro_questao_avulsa($caderno_id, $questao_id);
	}

	/**
	 * Adiciona uma questão a um caderno novo e suas categorias selecionadas/criadas
	 * e escreve a mensagem de sucesso.
	 * 
	 * @since L1
	 * 
	 */
	public function adicionar_novo_caderno()
	{
		$this->load->model("caderno_model");

		$questao_id = $this->input->post("que_id");
		
		$comercializavel = FALSE;
		if(tem_acesso([PROFESSOR]))
		{
			$comercializavel = $this->input->post('comercializavel')?:0;
		}

		$coaching = FALSE;
		if(tem_acesso([PARCEIRO_COACHING]))
		{
			$coaching = $this->input->post('coaching')?:0;
		}

		$data_caderno = array(
				'cad_nome' => $this->input->post('nome_caderno'),
				'cad_data_criacao' => get_data_hora_agora(),
				'usu_id' => get_current_user_id(),
				'cad_comercializavel' => $comercializavel,
				'cad_coaching' => $coaching
		);
		
		$caderno_id = $this->caderno_model->adicionar_caderno($data_caderno);
		
		$categorias_ids = $this->input->post('categorias_ids');
		$novas_categorias = $this->input->post('novas_categorias');
		$cat_ids = $categorias_ids?:array();
		if($novas_categorias) {
			$novas_categorias_a = explode(',', $novas_categorias);
			foreach ($novas_categorias_a as $item) {
				$id = $this->caderno_model->adicionar_categoria(array(
					'usu_id' => get_current_user_id(),
					'cat_nome' => $item
				));
				array_push($cat_ids, $id);
			} 
		}

		self::adicionar_categorias_em_caderno($caderno_id, $cat_ids, FALSE, FALSE);
		self::adicionar_questao_ao_caderno($caderno_id, $questao_id);
		
		echo get_botao_resposta("Questão adicionada ao caderno com sucesso.", "resposta-adicionada.png", "resposta-adicionada");
	}

	
	/**
	 * Carrega o componente de adicionar uma questão selecionada a um caderno
	 * 
	 * @since L1
	 * 
	 */
	public function get_adicionar_caderno_questao()
	{
		$this->load->model("caderno_model");
		$data['questao_id'] = $this->input->post("id");
		$data['cadernos'] = get_caderno_combo($this->caderno_model->listar_cadernos(get_current_user_id()));
		$data['categorias_cadernos'] = get_categoria_multiselect($this->caderno_model->listar_categorias(get_current_user_id()));

		//$this->load->view('main/form_adicionar_caderno', $data);

		$data['is_modal'] = FALSE;
		$data['is_ajax'] = TRUE;
		
		$this->load->view(get_questoes_barra_tarefas_adicionar_a_caderno_view_url(), $data);
	}

    /**
     * Retorna uma string com os IDs das categorias de um caderno, sepadas por vírgula
     * 
     * @since L1
     * 
     * @param int Scaderno_id ID do caderno
     * 
     */
	public function listar_categorias_caderno($caderno_id) 
	{
		$this->load->model('caderno_model');
		$ids = array();

		$categorias = $this->caderno_model->listar_categorias_ids_de_caderno($caderno_id);
		foreach ($categorias as $categoria) {
			array_push($ids, trim($categoria['cat_id']));
		}

		echo implode(',', $ids);
	}
}