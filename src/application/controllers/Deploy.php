<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

class Deploy extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function execute($milestone = null)
    {
        if($milestone) {
            $class_methods = get_class_methods('Deploy');
            
            $scripts = [];
            foreach ($class_methods as $item) {
                if(starts_with($item, $milestone)) {
                    array_push($scripts, $item);
                }
            }

            asort($scripts);

            foreach ($scripts as $item) {
                set_time_limit(360);

                echo "<br>Executando script: {$item}...<br>";

                self::$item();

                echo "<br>--------------------------------<br>";
            }
        }
        else {
            echo "Erro: Milestone precisa ser especificado";
        }
    }

    public function run()
    {
        self::k1_20180504111600_atualizar_campos_produto_busca();
    }

    private function k1_20180504111600_atualizar_campos_produto_busca()
    {
        $posts = get_todos_produtos_posts(false, 'any');

        $i = 1;
        foreach ($posts as $item) {
            set_time_limit(300);

            echo $i . " - " . $item->ID . "<br>";
            atualizar_campo_produto_busca($item->ID);
            $i++;
        }

        $page_check = get_page_by_path('pesquisa');
        $new_page = array(
            'post_type' => 'page',
            'post_title' => 'Pesquisa',
            'post_name' => 'pesquisa',
            'post_content' => '',
            'post_status' => 'publish',
            'post_author' => 1,
        );

        if(!isset($page_check->ID)){
            $new_page_id = wp_insert_post($new_page);
        }
    }

    private function k1_20180504111800_atualizar_provas_em_questao_master()
    {
        $this->load->model('questao_model');

        $questoes_repetidas_resolvidas = $this->questao_model->listar_questoes_repetidas(STATUS_COMPARACAO_RESOLVIDA);

        echo "Encontradas " . count($questoes_repetidas_resolvidas) . " questões repetidas resolvidas.<br>";

        foreach ($questoes_repetidas_resolvidas as $item) {
            $_master = $item['que_id_master'];
            $_repetida = $item['que_id_repetida'];

            if($repetidas_provas = $this->questao_model->listar_questao_prova_por_questao_id($_repetida)) {

                foreach ($repetidas_provas as $repetida_prova) {
                    
                    $repetida_prova_id = $repetida_prova['pro_id'];
            
                    if(!$this->questao_model->get_questao_prova($_master, $repetida_prova_id)) {
                        $this->questao_model->salvar_questao_prova($_master, $repetida_prova_id);
                    }
                }

            }
        }
    }

    private function k1_20180511150100_adicionar_paginas_gratis()
    {
        $paginas = [
            ['titulo' => 'Cursos Grátis', 'slug' => 'cursos-gratis'], 
            ['titulo' => 'Simulados Grátis', 'slug' => 'simulados-gratis'], 
            ['titulo' => 'Mapas Mentais Grátis', 'slug' => 'mapas-mentais-gratis'], 
            ['titulo' => 'Questões Comentadas Grátis', 'slug' => 'questoes-comentadas-gratis'], 
            ['titulo' => 'Audiobooks Grátis', 'slug' => 'audiobooks-gratis'], 
        ];

        foreach ($paginas as $item) {
            
            $page_check = get_page_by_path($item['slug']);
            $new_page = array(
                'post_type' => 'page',
                'post_title' => $item['titulo'],
                'post_name' => $item['slug'],
                'post_content' => '',
                'post_status' => 'publish',
                'post_author' => 1,
            );

            if(!isset($page_check->ID)){
                $new_page_id = wp_insert_post($new_page);
            }

        }

        echo "Removendo depoimentos...<br>";
        wp_delete_post(3584);

        $menu_items = wp_get_nav_menu_items('Main Menu');
        foreach ($menu_items as $item) {
            if($item->post_title == "MATERIAL GRÁTIS") {
                echo "Menu já foi alterado...<br>";
                return;
            }
        }

        $menu = wp_get_nav_menu_object('Main Menu');
        $menu_id = (int) $menu->term_id;

        $data =  array(
            'menu-item-title' => 'MATERIAL GRÁTIS',
            'menu-item-classes' => 'menu-gratis',
            'menu-item-url' => "#", 
            'menu-item-parent-id' => 0,
            'menu-item-position'  => 5,
            'menu-item-object' => 'custom',
            'menu-item-type'      => 'custom',
            'menu-item-status'    => 'publish'
          );

        $mg_id = wp_update_nav_menu_item($menu_id, 0, $data);

        $subs = [
            ['position' => 6, 'slug' => 'cursos-gratis', 'titulo' => 'Cursos Grátis'], 
            ['position' => 7, 'slug' => 'simulados-gratis', 'titulo' => 'Simulados Grátis'], 
            ['position' => 8, 'slug' => 'mapas-mentais-gratis', 'titulo' => 'Mapas Mentais Grátis'], 
            ['position' => 9, 'slug' => 'questoes-comentadas-gratis', 'titulo' => 'Questões Comentadas Grátis'], 
            ['position' => 10, 'slug' => 'audiobooks-gratis', 'titulo' => 'Audiobooks Grátis'],
        ];

        foreach ($subs as $item) {
            $pagina = get_page_by_path($item['slug']);

            $data =  array(
                'menu-item-object-id' => $pagina->ID,
                'menu-item-parent-id' => $mg_id,
                'menu-item-title' => $item['titulo'],
                'menu-item-classes' => 'sub-menu-gratis',
                'menu-item-position'  => $item['position'],
                'menu-item-object' => 'page',
                'menu-item-type'      => 'post_type',
                'menu-item-status'    => 'publish'
            );

            wp_update_nav_menu_item($menu_id, 0, $data);
        }

    }

    private function k1_20180529223900_criar_pagina_mailchimp() 
    {

        $page_check = get_page_by_path('mailchimp');
        $new_page = array(
            'post_type' => 'page',
            'post_title' => 'Mailchimp',
            'post_name' => 'mailchimp',
            'post_content' => '[expo_mailchimp mostrar_areas=1 area="Área Fiscal"]',
            'post_status' => 'publish',
            'post_author' => 1,
        );

        if(!isset($page_check->ID)){
            $new_page_id = wp_insert_post($new_page);
        }
    
    }

    private function k1_20180530202700_criar_rotulos() 
    {
        $this->load->model('simulado_model');
        
        $simulados = $this->simulado_model->listar();

        foreach ($simulados as $simulado) {
            $simulado_id = $simulado['sim_id'];
            
            if(!$this->simulado_model->listar_rotulos($simulado_id)) {
                echo "Criando rótulos para simulado: $simulado_id...<br>";
                $this->simulado_model->criar_rotulos_iniciais($simulado_id);
            }

        }    
    }

    private function k2_20180628175800_corrigir_conversoes() 
    {
        $this->load->model('assunto_model');
        $this->load->model('questao_model');
        

        $conversoes = $this->assunto_model->listar_conversoes(NULL, NULL, FALSE);

        foreach ($conversoes as $conversao) {
            set_time_limit(300);
            $antigo = $this->assunto_model->get_by_id($conversao['ass_antigo']);
            $novo = $this->assunto_model->get_by_id($conversao['ass_novo']);

            // atualiza a disciplina
            if($antigo['dis_id'] != $novo['dis_id']) {
                echo "Processando conversão: Assunto - {$antigo['ass_id']} - {$antigo['ass_nome']}...<br>";

                $questoes_assuntos = $this->questao_model->listar_por_assunto($conversao['ass_novo']);

                foreach ($questoes_assuntos as $questao_assunto) {
                    $this->questao_model->atualizar_disciplina($questao_assunto['que_id'], $novo['dis_id']);
                }
            }
        }

    }
}