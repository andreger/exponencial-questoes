<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buscador extends CI_Controller {

	public  function __construct() {
		parent::__construct();
		$this->load->helper('prova');
		$this->load->helper('qcon');
		$this->load->model('area_atuacao_model');
		$this->load->model('area_formacao_model');
		$this->load->model('assunto_model');
		$this->load->model('banca_model');
		$this->load->model('configuracao_model');
		$this->load->model('disciplina_model');
		$this->load->model('orgao_model');
		$this->load->model('prova_model');
		$this->load->model('questao_model');
		$this->load->model('buscador_model');
// 		$this->output->enable_profiler(true);

// 		ini_set('output_buffering', 'off');
// 		// Turn off PHP output compression
// 		ini_set('zlib.output_compression', false);
		 
// 		//Flush (send) the output buffer and turn off output buffering
// 		//ob_end_flush();
// 		while (@ob_end_flush());
		 
// 		// Implicitly flush the buffer(s)
// 		ini_set('implicit_flush', true);
// 		ob_implicit_flush(true);
		
// 		//prevent apache from buffering it for deflate/gzip
// 		header("Content-type: text/plain");
// 		header('Cache-Control: no-cache'); // recommended to prevent caching of event data.
	}
	
	public function qconcursosr($inicio, $fim)
	{
		for ($pagina = $inicio; $pagina <= $fim; $pagina++) {
			$url = 'https://www.qconcursos.com/questoes-de-concursos/questoes?order=questao_aplicada_em+desc&page=' . $pagina;
			$html = file_get_contents($url);
			file_put_contents(get_buscador_questoes_dir(). $pagina . ".html", $html);
		}
	}
	
	public function qconcursosa()
	{
		$a = array(
				72712);
		
		
		foreach ($a as $pagina) {
			$url = 'https://www.qconcursos.com/questoes-de-concursos/questoes?order=questao_aplicada_em+desc&page=' . $pagina;
			$html = file_get_contents($url);
			file_put_contents(get_buscador_questoes_dir(). $pagina . ".html", $html);
		}
	}
	
	public function qconcursosp($ultima_pagina = 40000, $configuracao_qcon_proxima_pagina = 'qconp_proxima_pagina') {
		$pagina = $this->configuracao_model->get_valor($configuracao_qcon_proxima_pagina, true);
			
		if($pagina > $ultima_pagina) exit;
			
		// 			$url = "https://www.qconcursos.com/questoes-de-concursos/questoes/search?utf8=%E2%9C%93&q=&orgao=&banca=&ano=&cargo=&escolaridade=&modalidade=&disciplina=" . $disciplina_qcon_id . "&assunto=&area_atuacao=&area_formacao=&nivel_dificuldade=&periodo_de=&periodo_ate=&possui_gabarito_comentado_texto_e_video=&possui_comentarios_gerais=&possui_comentarios=&possui_anotacoes=&sem_dos_meus_cadernos=&sem_anuladas=&sem_desatualizadas=&sem_anuladas_impressao=&sem_desatualizadas_impressao=&prova=&caderno_id=&migalha=&data_comentario_texto=&data=&r=&rc=&re=&nao_resolvidas=&page=" . $pagina;
		$url = 'https://www.qconcursos.com/questoes-de-concursos/questoes?order=questao_aplicada_em+desc&page=' . $pagina;
			
		$html = file_get_html($url);
		$html = file_get_contents($url);
		file_put_contents(get_buscador_questoes_dir(). $pagina . ".html", $html);
		
		$configuracao = array('cfg_nome' => $configuracao_qcon_proxima_pagina, 'cfg_valor' => $pagina + 1);
		$this->configuracao_model->atualizar($configuracao);
	}
	
	public function qconcursos($ultima_pagina = 83909, $configuracao_qcon_proxima_pagina = 'parse0_10') 
	{
		echo "Script iniciado <br><br>";
		
		for($z = 0; $z < 30; $z++) {
			echo "z = " . $z . "<br>";
			echo "Iniciando leitura de pagina<br>";
			ob_flush();
			flush();
			
			$pagina = $this->configuracao_model->get_valor($configuracao_qcon_proxima_pagina, true);
				
			echo "Pagina a ser lida: $pagina<br>";
			ob_flush();
			flush();
		
			if($pagina > $ultima_pagina) exit;
				
			// 			$url = "https://www.qconcursos.com/questoes-de-concursos/questoes/search?utf8=%E2%9C%93&q=&orgao=&banca=&ano=&cargo=&escolaridade=&modalidade=&disciplina=" . $disciplina_qcon_id . "&assunto=&area_atuacao=&area_formacao=&nivel_dificuldade=&periodo_de=&periodo_ate=&possui_gabarito_comentado_texto_e_video=&possui_comentarios_gerais=&possui_comentarios=&possui_anotacoes=&sem_dos_meus_cadernos=&sem_anuladas=&sem_desatualizadas=&sem_anuladas_impressao=&sem_desatualizadas_impressao=&prova=&caderno_id=&migalha=&data_comentario_texto=&data=&r=&rc=&re=&nao_resolvidas=&page=" . $pagina;
			$url = get_buscador_questoes_dir() . $pagina . ".html";
				
			echo "Abrindo url: {$url}<br>";
			ob_flush();
			flush();
			
			$html = file_get_html($url);
			
			for($i = 0; $i < 5; $i++) {
				$elemento = $html->find('.uma-questao-1', $i);
				
				if(!$elemento) continue;
		
				$qcon_id =  substr(trim($elemento->find('.questao-numero', 0)->plaintext),1);
				
				echo "Lendo questao {$qcon_id}<br>";
				ob_flush();
				flush();
				
				$nome_disciplina = trim($elemento->find('.disciplina-assunto div a', 0)->plaintext);
				
				$questoes_meta = $elemento->find('.uma-questao-topo-linha2');
				if(!$questoes_meta) continue;
				
				$ano = trim($elemento->find('.uma-questao-topo-linha2 div', 0)->find('text',1)->plaintext);
				$nome_banca = trim($elemento->find('.uma-questao-topo-linha2 div', 1)->find('text',1)->plaintext);
				$nome_orgao = trim($elemento->find('.uma-questao-topo-linha2 div', 2)->find('text',1)->plaintext);
				$nome_prova = trim($elemento->find('.uma-questao-topo-linha2 div', 3)->find('a',0)->plaintext);
				$qcon_url = trim($elemento->find('.uma-questao-topo-linha2 div', 3)->find('a',0)->href);
		
				$disciplina = $this->disciplina_model->get_by_nome($nome_disciplina, true);
				$banca = $this->banca_model->get_by_nome($nome_banca, true);
				$orgao = $this->orgao_model->get_by_nome($nome_orgao, true);
				$prova = $this->prova_model->get($nome_prova, $ano, $banca['ban_id'], $orgao['org_id'], $qcon_url, true);
		
				$escolaridade_id = self::get_escolaridade_prova($prova['pro_id']);
				$this->prova_model->atualizar_escolaridade($prova['pro_id'], $escolaridade_id);
				
				echo "Salvando pdfs<br>";
				ob_flush();
				flush();
				
				self::salvar_pdfs_prova($prova['pro_id']);
		
				$enunciado = $elemento->find('.enunciado', 0)->innertext;
				$opcoes_array = $elemento->find('.questoes-texto');
		
				echo "Lendo opcoes<br>";
				ob_flush();
				flush();
				
				$questoes_opcoes = array();
				foreach ($opcoes_array as $opcao) {
					array_push($questoes_opcoes, $opcao->innertext);
				}
				
				echo "Lendo status<br>";
				ob_flush();
				flush();
		
				$status = self::get_status($elemento);
		
				echo "Salvando questao<br>";
				ob_flush();
				flush();
				
				if(is_null($enunciado)) {
					echo "Enunciado nulo... Pulando questao<br>";
					ob_flush();
					flush();
					continue;
				}
				
				$questao = $this->questao_model->get(array(
						'que_qcon_id' 	=> $qcon_id,
						'que_enunciado' => $enunciado,
						'pro_id' => $prova['pro_id'],
						'que_tipo' => get_questao_tipo($questoes_opcoes),
						'dis_id' => $disciplina['dis_id'],
						'que_status' => $status,
						'opcoes' => $questoes_opcoes) , true);
		
				$assuntos_questao = $elemento->find('.disciplina-assunto span[id^=primeiros_tres_assuntos_] a');
				
				$num_assuntos = count($assuntos_questao);
				echo "Lendo assuntos ($num_assuntos)<br>";
				ob_flush();
				flush();
				
				foreach ($assuntos_questao as $assunto_nome) {
						
						
					$assunto_nome = trim($assunto_nome->innertext);
						
					// 					echo $assunto_nome;
						
					$assunto = $this->assunto_model->get(array(
							'ass_nome'	=> $assunto_nome,
							'dis_id'	=> $disciplina['dis_id']
					), true);
					
					echo "Salvando assunto: {$assunto_nome}<br>";
					ob_flush();
					flush();
					
					$this->questao_model->salvar_assunto_em_questao($assunto, $questao);
				}
			}
			
			echo "Atualizando configuracoes<br>";
			ob_flush();
			flush();
				
			$configuracao = array('cfg_nome' => $configuracao_qcon_proxima_pagina, 'cfg_valor' => $pagina + 1);
			$this->configuracao_model->atualizar($configuracao);
			
			echo "Pagina encerrada<br><br>********************************************<br><br>";
			ob_flush();
			flush();
		}
		
		echo "Script encerrado<br><br>";
	}
	
	
// 	public function qconcursos($disciplina_qcon_id = 1, $ultima_pagina = 76932) {
// 		if(is_null($disciplina_qcon_id)) 
// 		{
// 			$this->load->view('buscador/qconcurso');
// 		} 
// 		else 
// 		{
// // 			$configuracao_qcon_proxima_pagina = 'qcon_proxima_pagina_' . $disciplina_qcon_id;
// 			$configuracao_qcon_proxima_pagina = 'qcon_proxima_pagina';
			
// 			$pagina = $this->configuracao_model->get_valor($configuracao_qcon_proxima_pagina, true); 
			
// 			if($pagina > $ultima_pagina) exit;
			
// // 			$url = "https://www.qconcursos.com/questoes-de-concursos/questoes/search?utf8=%E2%9C%93&q=&orgao=&banca=&ano=&cargo=&escolaridade=&modalidade=&disciplina=" . $disciplina_qcon_id . "&assunto=&area_atuacao=&area_formacao=&nivel_dificuldade=&periodo_de=&periodo_ate=&possui_gabarito_comentado_texto_e_video=&possui_comentarios_gerais=&possui_comentarios=&possui_anotacoes=&sem_dos_meus_cadernos=&sem_anuladas=&sem_desatualizadas=&sem_anuladas_impressao=&sem_desatualizadas_impressao=&prova=&caderno_id=&migalha=&data_comentario_texto=&data=&r=&rc=&re=&nao_resolvidas=&page=" . $pagina;
// 			$url = 'https://www.qconcursos.com/questoes-de-concursos/questoes?order=questao_aplicada_em+desc&page=' . $pagina;
			
// 			$html = file_get_html($url);
			
// 			for($i = 0; $i < 5; $i++) {
// 				$elemento = $html->find('.uma-questao-1', $i);
				
// 				$qcon_id =  substr(trim($elemento->find('.questao-numero', 0)->plaintext),1); echo "<br>". $qcon_id;
// 				$nome_disciplina = trim($elemento->find('.disciplina-assunto div a', 0)->plaintext);
// 				$ano = trim($elemento->find('.uma-questao-topo-linha2 div', 0)->find('text',1)->plaintext);
// 				$nome_banca = trim($elemento->find('.uma-questao-topo-linha2 div', 1)->find('text',1)->plaintext);
// 				$nome_orgao = trim($elemento->find('.uma-questao-topo-linha2 div', 2)->find('text',1)->plaintext);
// 				$nome_prova = trim($elemento->find('.uma-questao-topo-linha2 div', 3)->find('a',0)->plaintext);
// 				$qcon_url = trim($elemento->find('.uma-questao-topo-linha2 div', 3)->find('a',0)->href);
				
// 				$disciplina = $this->disciplina_model->get_by_nome($nome_disciplina, true);
// 				$banca = $this->banca_model->get_by_nome($nome_banca, true);
// 				$orgao = $this->orgao_model->get_by_nome($nome_orgao, true);
// 				$prova = $this->prova_model->get($nome_prova, $ano, $banca['ban_id'], $orgao['org_id'], $qcon_url, true);

// 				$escolaridade_id = self::get_escolaridade_prova($prova['pro_id']);
// 				$this->prova_model->atualizar_escolaridade($prova['pro_id'], $escolaridade_id);
// 				self::salvar_pdfs_prova($prova['pro_id']);				
				
// 				$enunciado = $elemento->find('.enunciado', 0)->innertext;
// 				$opcoes_array = $elemento->find('.questoes-texto');
				
// 				$questoes_opcoes = array();
// 				foreach ($opcoes_array as $opcao) {
// 					array_push($questoes_opcoes, $opcao->innertext);
// 				}
				
// 				$status = self::get_status($elemento);
				
// 				$questao = $this->questao_model->get(array(
// 						'que_qcon_id' 	=> $qcon_id,
// 						'que_enunciado' => $enunciado,
// 						'pro_id' => $prova['pro_id'],
// 						'que_tipo' => get_questao_tipo($questoes_opcoes),
// 						'dis_id' => $disciplina['dis_id'],
// 						'que_status' => $status,
// 						'opcoes' => $questoes_opcoes) , true);
				
// 				$assuntos_questao = $elemento->find('.disciplina-assunto span[id^=primeiros_tres_assuntos_] a');
// 				foreach ($assuntos_questao as $assunto_nome) {
					
					
// 					$assunto_nome = trim($assunto_nome->innertext);
					
// // 					echo $assunto_nome;
					
// 					$assunto = $this->assunto_model->get(array(
// 							'ass_nome'	=> $assunto_nome,
// 							'dis_id'	=> $disciplina['dis_id']
// 					), true);
// 					$this->questao_model->salvar_assunto_em_questao($assunto, $questao);					
// 				}
// 			}
			
// 			$configuracao = array('cfg_nome' => $configuracao_qcon_proxima_pagina, 'cfg_valor' => $pagina + 1);
// 			$this->configuracao_model->atualizar($configuracao);
			
// // 			sleep(30);
// // 			$this->load->view('buscador/refresh');
// 		}
// 	}
	
	private function get_status($elemento)
	{
		if(count($elemento->find('.questao-desatualizada')) > 0) {
			return DESATUALIZADA;
		}
		if(count($elemento->find('.questao-anulada')) > 0) {
			return CANCELADA;
		}
		return NORMAL;
	}
	
	private function get_conteudo_prova_qcon($prova_id)
	{
		$arquivo = get_buscador_provas_dir() . $prova_id . '.html';
		
		if(file_exists($arquivo)) {
			return file_get_html($arquivo);
		}
		else {
			$prova = $this->prova_model->get_by_id($prova_id);
			$url = "http://www.qconcursos.com" . $prova['pro_qcon_url'];
			
			$html = file_get_contents($url);
			file_put_contents($arquivo, $html);
			
			return file_get_html($arquivo);
		}
	}
	
	public function salvar_usuario_buscador()
	{
		$dados = array(
				'bus_email' => $this->input->post('email'),
				'bus_senha' => $this->input->post('senha')
			);
			
		$this->db->insert('buscador_usuarios', $dados);
	}
	
// 	private function get_conteudo_prova_qcon($prova_id)
// 	{
// 		$prova = $this->prova_model->get_by_id($prova_id);
// 		$url = "http://www.qconcursos.com" . $prova['pro_qcon_url'];
		
// 		return file_get_html($url);
// 	}
	
	
	private function get_escolaridade_prova($prova_id)
	{
		$html = self::get_conteudo_prova_qcon($prova_id);
		
		$elements = $html->find('.categogoria-txt-link .link-laranja');
		foreach ($elements as $element) {
			$url = $element->href;
			if(strpos($url, 'escolaridade_hidden') === false) continue;
			
			return substr($url, -1);
		}
	}
	
	public function salvar_pdfs_prova($prova_id) 
	{
		$html = self::get_conteudo_prova_qcon($prova_id);
		
		$arquivo = get_arquivo_edital($prova_id);
		if(!file_exists($arquivo)) {
			$url = $html->find('li[class=baixar-seta] a',2)->href;
			copy($url, $arquivo);
		}
		
		$arquivo = get_arquivo_gabarito($prova_id);
		if(!file_exists($arquivo)) {
			$url = $html->find('li[class=baixar-seta] a',1)->href;
			copy($url, $arquivo);
		}
		
		$arquivo = get_arquivo_prova($prova_id);
		if(!file_exists($arquivo)) {
			$url = $html->find('li[class=baixar-seta] a',0)->href;
			copy($url, $arquivo);
		}
		
	}
	
	public function abrir_edital($prova_id)
	{
		$arquivo = get_arquivo_edital($prova_id);
		if(file_exists($arquivo)) {
			self::abrir_pdf($arquivo);
		}
		else {
			$html = self::get_conteudo_prova_qcon($prova_id);
			$url = $html->find('li[class=baixar-seta] a',2)->href;
			copy($url, $arquivo);
		
			self::abrir_pdf($arquivo);
		}
	} 
	
	public function abrir_gabarito($prova_id)
	{
		$arquivo = get_arquivo_gabarito($prova_id);
		if(file_exists($arquivo)) {
			self::abrir_pdf($arquivo);
		}
		else {
			$html = self::get_conteudo_prova_qcon($prova_id);
			$url = $html->find('li[class=baixar-seta] a',1)->href;
			copy($url, $arquivo);
	
			self::abrir_pdf($arquivo);
		}
	}
	
	public function abrir_prova($prova_id)
	{
		$arquivo = get_arquivo_prova($prova_id);
		if(file_exists($arquivo)) {
			self::abrir_pdf($arquivo);
		}
		else {
			$html = self::get_conteudo_prova_qcon($prova_id);
			$url = $html->find('li[class=baixar-seta] a',0)->href;
			copy($url, $arquivo);
	
			self::abrir_pdf($arquivo);
		}
	}
	
	public function abrir_pdf($arquivo) 
	{
		$fp= fopen($arquivo, "r");
	
		header("Cache-Control: maxage=1");
		header("Pragma: public");
		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename=".$arquivo."");
		header("Content-Description: PHP Generated Data");
		header("Content-Transfer-Encoding: binary");
		header('Content-Length:' .filesize($arquivo));
		ob_clean();

		flush();
		
		while (!feof($fp)){
			$buff = fread($fp,1024);
			print $buff;
		}
		exit;
	}
	
	
	
	
	public function init_cargos() 
	{
		$this->load->model('cargo_model');
		
		$json = '[{"id":7159,"value":"2\u00ba Tenente M\u00e9dico PM Estagi\u00e1rio"},{"id":310,"value":"Administrador"},{"id":1152,"value":"Administrador de banco de dados"},{"id":5090,"value":"Administrador de Dados"},{"id":2386,"value":"Administrador de Edif\u00edcios"},{"id":2628,"value":"Administrador de redes"},{"id":138,"value":"Advogado"},{"id":8833,"value":"Advogado da uni\u00e3o"},{"id":7403,"value":"Agente"},{"id":8627,"value":"Agente - Arrecada\u00e7\u00e3o e Tributos"},{"id":398,"value":"Agente Administrativo"},{"id":8862,"value":"Agente Apoio T\u00e9cnico"},{"id":315,"value":"Agente Censit\u00e1rio"},{"id":1465,"value":"Agente comunit\u00e1rio de sa\u00fade"},{"id":6958,"value":"Agente da Fiscaliza\u00e7\u00e3o Financeira"},{"id":527,"value":"Agente da Fiscaliza\u00e7\u00e3o Financeira - Inform\u00e1tica - Suporte de Web"},{"id":8257,"value":"Agente de a\u00e7\u00e3o social"},{"id":2181,"value":"Agente de Apoio - Administrativo"},{"id":8152,"value":"Agente de Apoio - Manuten\u00e7\u00e3o e Suporte de Inform\u00e1tica"},{"id":2179,"value":"Agente de Apoio Operacional"},{"id":2180,"value":"Agente de Apoio Socioeducativo"},{"id":6068,"value":"Agente de Atividades Administrativas"},{"id":1466,"value":"Agente de Combate a Endemias"},{"id":661,"value":"Agente de Correios - Atendente Comercial"},{"id":662,"value":"Agente de Correios - Carteiro"},{"id":663,"value":"Agente de Correios - Operador de Triagem e Transbordo"},{"id":566,"value":"Agente de Defensoria - Analista de Sistemas"},{"id":568,"value":"Agente de Defensoria - Arquiteto"},{"id":569,"value":"Agente de Defensoria - Assistente Social"},{"id":570,"value":"Agente de Defensoria - Cientista Social"},{"id":575,"value":"Agente de Defensoria - Comunica\u00e7\u00e3o Social"},{"id":572,"value":"Agente de Defensoria - Desenhista Industrial"},{"id":7271,"value":"Agente de Defensoria - Engenheiro Civil"},{"id":7272,"value":"Agente de Defensoria - Engenheiro de Redes"},{"id":7273,"value":"Agente de Defensoria - Engenheiro El\u00e9trico"},{"id":577,"value":"Agente de Defensoria - Psic\u00f3logo"},{"id":3405,"value":"Agente de Defesa Civil"},{"id":2965,"value":"Agente de Endemias"},{"id":6682,"value":"Agente de Escolta e Vigil\u00e2ncia Penitenci\u00e1rio"},{"id":6640,"value":"Agente de Fazenda"},{"id":4529,"value":"Agente de Fiscaliza\u00e7\u00e3o"},{"id":6977,"value":"Agente de Fiscaliza\u00e7\u00e3o Financeira"},{"id":695,"value":"Agente de Fiscaliza\u00e7\u00e3o Judici\u00e1ria"},{"id":323,"value":"Agente de intelig\u00eancia"},{"id":884,"value":"Agente de Investiga\u00e7\u00e3o e Agente de Pol\u00edcia"},{"id":2053,"value":"Agente de Manuten\u00e7\u00e3o"},{"id":8844,"value":"Agente de Opera\u00e7\u00e3o e Fiscaliza\u00e7\u00e3o de Transporte e Tr\u00e2nsito"},{"id":7410,"value":"Agente de Orienta\u00e7\u00e3o e Fiscaliza\u00e7\u00e3o"},{"id":8552,"value":"Agente de Pesquisas e Mapeamento"},{"id":456,"value":"Agente de Pol\u00edcia"},{"id":411,"value":"Agente de pol\u00edcia federal"},{"id":7546,"value":"Agente de Promotoria \u2013 Assessoria"},{"id":7120,"value":"Agente de Prote\u00e7\u00e3o"},{"id":6961,"value":"Agente de Prote\u00e7\u00e3o e Defesa do Consumidor"},{"id":1429,"value":"Agente de seguran\u00e7a"},{"id":8608,"value":"Agente de Seguran\u00e7a Judici\u00e1ria - Motorista"},{"id":1479,"value":"Agente de tr\u00e2nsito"},{"id":2969,"value":"Agente de Tributos"},{"id":1191,"value":"Agente de Vigil\u00e2ncia"},{"id":2182,"value":"Agente Educacional"},{"id":331,"value":"Agente Estadual Agropecu\u00e1rio"},{"id":636,"value":"Agente Executivo"},{"id":168,"value":"Agente Federal da Pol\u00edcia Federal"},{"id":1930,"value":"Agente Fiscal"},{"id":807,"value":"Agente Fiscal de Rendas"},{"id":808,"value":"Agente Fiscal de Rendas - Gest\u00e3o Tribut\u00e1ria"},{"id":809,"value":"Agente Fiscal de Rendas - Tecnologia da Informa\u00e7\u00e3o"},{"id":642,"value":"Agente Fiscal de Tributos Estaduais"},{"id":8675,"value":"Agente Fiscal do Tesouro do Estado"},{"id":8503,"value":"Agente Governamental"},{"id":803,"value":"Agente Legislativo de Servi\u00e7os T\u00e9cnicos e Administrativos"},{"id":805,"value":"Agente Legislativo de Servi\u00e7os T\u00e9cnicos e Administrativos - Processamento de Dados"},{"id":1926,"value":"Agente Operacional"},{"id":763,"value":"Agente Penitenci\u00e1rio"},{"id":8599,"value":"Agente Previdenci\u00e1rio"},{"id":8544,"value":"Agente P\u00fablico"},{"id":7463,"value":"Agente T\u00e9cnico - Administrador"},{"id":7551,"value":"Agente T\u00e9cnico - Arquiteto"},{"id":7560,"value":"Agente T\u00e9cnico - Estat\u00edstico"},{"id":3756,"value":"Agente T\u00e9cnico - Jur\u00eddico"},{"id":834,"value":"Agente T\u00e9cnico Administrativo"},{"id":4550,"value":"Agente t\u00e9cnico de intelig\u00eancia"},{"id":795,"value":"Agente T\u00e9cnico Legislativo Especializado"},{"id":800,"value":"Agente T\u00e9cnico Legislativo Especializado - Admin  e Arquitetura de Dados"},{"id":802,"value":"Agente T\u00e9cnico Legislativo Especializado - An\u00e1lise de Infraestrutura de Redes"},{"id":790,"value":"Agente T\u00e9cnico Legislativo Especializado - Arquitetura"},{"id":797,"value":"Agente T\u00e9cnico Legislativo Especializado - Direito"},{"id":798,"value":"Agente T\u00e9cnico Legislativo Especializado - Direito (Finan\u00e7as e Or\u00e7amento)"},{"id":785,"value":"Agente T\u00e9cnico Legislativo Especializado - Economia-Administra\u00e7\u00e3o-Ci\u00eancias Cont\u00e1beis"},{"id":787,"value":"Agente T\u00e9cnico Legislativo Especializado - Enfermagem"},{"id":793,"value":"Agente T\u00e9cnico Legislativo Especializado - Gest\u00e3o de Projetos"},{"id":794,"value":"Agente T\u00e9cnico Legislativo Especializado - Hist\u00f3ria"},{"id":792,"value":"Agente T\u00e9cnico Legislativo Especializado - Pedagogia"},{"id":791,"value":"Agente T\u00e9cnico Legislativo Especializado - Psicologia"},{"id":801,"value":"Agente T\u00e9cnico Legislativo Especializado - Seguran\u00e7a de Redes"},{"id":799,"value":"Agente T\u00e9cnico Legislativo Especializado - Tecnologia da Informa\u00e7\u00e3o"},{"id":1988,"value":"Ajudante"},{"id":7081,"value":"Ajudante de Carga e Descarga"},{"id":7082,"value":"Ajudante de Motorista"},{"id":8883,"value":"Aluno - Curso T\u00e9cnico - ETAM"},{"id":6921,"value":"Aluno - EsFCEx"},{"id":6920,"value":"Aluno - EsPCEx"},{"id":6924,"value":"Aluno - EsSEx - CFO"},{"id":8880,"value":"Aluno - IME"},{"id":7817,"value":"Aluno Oficial - CFO"},{"id":679,"value":"Analista"},{"id":6917,"value":"Analista - Administra\u00e7\u00e3o de Sistemas"},{"id":7927,"value":"Analista - Administra\u00e7\u00e3o Escolar"},{"id":8851,"value":"Analista - Administrador"},{"id":870,"value":"Analista - Advocacia"},{"id":671,"value":"Analista - An\u00e1lise de Sistemas"},{"id":8548,"value":"Analista - An\u00e1lise e Desenvolvimento de Aplica\u00e7\u00f5es"},{"id":7709,"value":"Analista - An\u00e1lise e Desenvolvimento de Sistemas"},{"id":2240,"value":"Analista - Antropologia"},{"id":2241,"value":"Analista - Arqueologia"},{"id":1433,"value":"Analista - Arquitetura"},{"id":2242,"value":"Analista - Arquivologia"},{"id":1955,"value":"Analista - Assist\u00eancia Social"},{"id":4268,"value":"Analista - Assuntos Jur\u00eddicos"},{"id":851,"value":"Analista - Biblioteconomia"},{"id":2243,"value":"Analista - Biologia"},{"id":1434,"value":"Analista - Ci\u00eancias Sociais"},{"id":2245,"value":"Analista - Cl\u00ednica M\u00e9dica"},{"id":852,"value":"Analista - Comunica\u00e7\u00e3o Social"},{"id":512,"value":"Analista - Contabilidade"},{"id":2220,"value":"Analista - Controle Interno"},{"id":1008,"value":"Analista - Desenho Instrucional"},{"id":853,"value":"Analista - Desenvolvimento de Sistemas"},{"id":7929,"value":"Analista - Designer Institucional"},{"id":4780,"value":"Analista - Direito"},{"id":672,"value":"Analista - Economia"},{"id":7912,"value":"Analista - Educa\u00e7\u00e3o"},{"id":2405,"value":"Analista - Educa\u00e7\u00e3o Ambiental"},{"id":3653,"value":"Analista - Enfermagem"},{"id":3654,"value":"Analista - Engenharia Ambiental"},{"id":8886,"value":"Analista - Engenharia Civil"},{"id":6070,"value":"Analista - Engenharia de Produ\u00e7\u00e3o Civil"},{"id":8122,"value":"Analista - Engenharia de Telecomunica\u00e7\u00f5es"},{"id":6071,"value":"Analista - Engenharia Eletr\u00f4nica"},{"id":855,"value":"Analista - Engenharia em Seguran\u00e7a do Trabalho"},{"id":2225,"value":"Analista - Engenharia Mec\u00e2nica"},{"id":4557,"value":"Analista - Engenharia Qu\u00edmica"},{"id":4559,"value":"Analista - Estat\u00edstica"},{"id":1996,"value":"Analista - Finan\u00e7as"},{"id":8107,"value":"Analista - Finan\u00e7as e Controle"},{"id":3656,"value":"Analista - Fisioterapia"},{"id":3657,"value":"Analista - Geografia"},{"id":7714,"value":"Analista - Gest\u00e3o e An\u00e1lise Processual"},{"id":8648,"value":"Analista - Gest\u00e3o em Metrologia e Qualidade Industrial - Direito"},{"id":8651,"value":"Analista - Gest\u00e3o em Metrologia e Qualidade Industrial - Sistemas Informatizados"},{"id":858,"value":"Analista - Gest\u00e3o Financeira"},{"id":8106,"value":"Analista - Gest\u00e3o P\u00fablica"},{"id":5401,"value":"Analista - Hist\u00f3ria"},{"id":7315,"value":"Analista - Inform\u00e1tica"},{"id":7713,"value":"Analista - Infraestrutura e Log\u00edstica"},{"id":673,"value":"Analista - Jornalismo"},{"id":2270,"value":"Analista - Letras"},{"id":5369,"value":"Analista - Neg\u00f3cios"},{"id":861,"value":"Analista - Neg\u00f3cios em Tecnologia da Informa\u00e7\u00e3o"},{"id":8108,"value":"Analista - Oceanografia"},{"id":2226,"value":"Analista - Or\u00e7amento"},{"id":7930,"value":"Analista - Or\u00e7amento e Finan\u00e7as"},{"id":3660,"value":"Analista - Pedagogia"},{"id":6781,"value":"Analista - Planejamento e Or\u00e7amento"},{"id":7711,"value":"Analista - Pol\u00edtica Econ\u00f4mica e Monet\u00e1ria"},{"id":2218,"value":"Analista - Processual"},{"id":8383,"value":"Analista - Programador"},{"id":676,"value":"Analista - Psicologia"},{"id":7317,"value":"Analista - Psiquiatria"},{"id":677,"value":"Analista - Publicidade"},{"id":2341,"value":"Analista - Rela\u00e7\u00f5es P\u00fablicas"},{"id":1009,"value":"Analista - Servi\u00e7o Social"},{"id":7710,"value":"Analista - Suporte \u00e0 Infraestrutura de Tecnologia da Informa\u00e7\u00e3o"},{"id":1999,"value":"Analista - Suporte de Inform\u00e1tica"},{"id":8109,"value":"Analista - Suporte e Infraestrutura"},{"id":8549,"value":"Analista - Suporte Operacional"},{"id":865,"value":"Analista - Suporte T\u00e9cnico"},{"id":513,"value":"Analista - Tecnologia da Informa\u00e7\u00e3o"},{"id":8123,"value":"Analista - Terapia Ocupacional"},{"id":6,"value":"Analista Administrativo"},{"id":344,"value":"Analista Administrativo - An\u00e1lise de Neg\u00f3cios"},{"id":7121,"value":"Analista Administrativo - \u00c1rea 1"},{"id":7137,"value":"Analista Administrativo - \u00c1rea 2"},{"id":7138,"value":"Analista Administrativo - \u00c1rea 3"},{"id":7139,"value":"Analista Administrativo - \u00c1rea 4"},{"id":7140,"value":"Analista Administrativo - \u00c1rea 5"},{"id":2278,"value":"Analista Administrativo - \u00c1rea Administrativa"},{"id":8086,"value":"Analista Administrativo - Arquitetura"},{"id":300,"value":"Analista Administrativo - Arquivologia"},{"id":301,"value":"Analista Administrativo - Biblioteconomia"},{"id":5333,"value":"Analista Administrativo - Ci\u00eancias da Computa\u00e7\u00e3o"},{"id":4205,"value":"Analista Administrativo - Ci\u00eancias Humanas ou Sociais"},{"id":302,"value":"Analista Administrativo - Comunica\u00e7\u00e3o Social"},{"id":36,"value":"Analista Administrativo - Contabilidade"},{"id":35,"value":"Analista Administrativo - Direito"},{"id":3418,"value":"Analista Administrativo - Economia"},{"id":1738,"value":"Analista Administrativo - Estat\u00edstica"},{"id":34,"value":"Analista Administrativo - Inform\u00e1tica"},{"id":7513,"value":"Analista Administrativo - Infraestrutura de TI"},{"id":3419,"value":"Analista Administrativo - Jornalismo"},{"id":339,"value":"Analista Administrativo - Tecnologia da Informa\u00e7\u00e3o"},{"id":727,"value":"Analista Ambiental"},{"id":2452,"value":"Analista Ambiental - Engenheiro Ambiental"},{"id":139,"value":"Analista Ambiental - Ocean\u00f3grafo"},{"id":657,"value":"Analista Banc\u00e1rio"},{"id":6621,"value":"Analista Cont\u00e1bil"},{"id":7755,"value":"Analista da Pol\u00edcia Civil - Ci\u00eancias Cont\u00e1beis"},{"id":7757,"value":"Analista da Pol\u00edcia Civil - Direito"},{"id":7758,"value":"Analista da Pol\u00edcia Civil - Enfermagem"},{"id":7771,"value":"Analista da Pol\u00edcia Civil - Estat\u00edstica"},{"id":7770,"value":"Analista da Pol\u00edcia Civil - Tecnologia da Informa\u00e7\u00e3o"},{"id":691,"value":"Analista de Atividades do Meio Ambiente"},{"id":5277,"value":"Analista de banco de dados"},{"id":1913,"value":"Analista de Com\u00e9rcio Exterior"},{"id":8314,"value":"Analista de Compras"},{"id":6964,"value":"Analista de Controle"},{"id":6967,"value":"Analista de Controle Externo"},{"id":181,"value":"Analista de Controle Externo - Auditoria de Obras P\u00fablicas"},{"id":248,"value":"Analista de Controle Externo - Auditoria Governamental"},{"id":241,"value":"Analista de Controle Externo - Ci\u00eancias Cont\u00e1beis"},{"id":643,"value":"Analista de Controle Externo - Comum a todos"},{"id":6965,"value":"Analista de Controle Externo - Controle Externo"},{"id":6951,"value":"Analista de Controle Externo - Coordenadoria de Engenharia"},{"id":6952,"value":"Analista de Controle Externo - Coordenadoria de Inform\u00e1tica"},{"id":6953,"value":"Analista de Controle Externo - Coordenadoria Jur\u00eddica"},{"id":6954,"value":"Analista de Controle Externo - Coordenadorias T\u00e9cnicas"},{"id":243,"value":"Analista de Controle Externo - Direito"},{"id":6966,"value":"Analista de Controle Externo - Engenharia"},{"id":419,"value":"Analista de Controle Externo - Gest\u00e3o do Conhecimento"},{"id":258,"value":"Analista de Controle Externo - Inform\u00e1tica - Processamento de Dados"},{"id":850,"value":"Analista de Controle Externo - Inspe\u00e7\u00e3o de Obras P\u00fablicas"},{"id":849,"value":"Analista de Controle Externo - Inspe\u00e7\u00e3o Governamental"},{"id":5365,"value":"Analista de Controle Externo - Meio Ambiente"},{"id":420,"value":"Analista de Controle Externo - Or\u00e7amento e Finan\u00e7as"},{"id":2751,"value":"Analista de Controle Externo - Planejamento e Gest\u00e3o"},{"id":246,"value":"Analista de Controle Externo - Processamentos de Dados"},{"id":183,"value":"Analista de Controle Externo - Tecnologia da Informa\u00e7\u00e3o"},{"id":4555,"value":"Analista de Controle Interno"},{"id":597,"value":"Analista de Controle Interno \u2013 Finan\u00e7as P\u00fablicas"},{"id":598,"value":"Analista de Controle Interno \u2013 Tecnologia da Informa\u00e7\u00e3o"},{"id":6817,"value":"Analista de Correios - Analista de Sistemas - Desenvolvimento de Sistemas"},{"id":6818,"value":"Analista de Correios - Analista de Sistemas - Produ\u00e7\u00e3o"},{"id":6819,"value":"Analista de Correios - Analista de Sistemas - Suporte de Sistemas"},{"id":6820,"value":"Analista de Correios - Arquiteto"},{"id":6821,"value":"Analista de Correios - Arquivologia"},{"id":667,"value":"Analista de Correios - Assistente Social"},{"id":6822,"value":"Analista de Correios - Bibliotec\u00e1rio"},{"id":6823,"value":"Analista de Correios - Com\u00e9rcio Exterior"},{"id":6824,"value":"Analista de Correios - Comunica\u00e7\u00e3o Social - Publicidade e Propaganda"},{"id":6825,"value":"Analista de Correios - Comunica\u00e7\u00e3o Social - Rela\u00e7\u00f5es P\u00fablicas"},{"id":6827,"value":"Analista de Correios - Desenho Industrial"},{"id":6828,"value":"Analista de Correios - Designer Gr\u00e1fico"},{"id":6829,"value":"Analista de Correios - Economista"},{"id":6834,"value":"Analista de Correios - Engenheiro - Engenharia Eletr\u00f4nica"},{"id":6835,"value":"Analista de Correios - Engenheiro - Engenharia Mec\u00e2nica"},{"id":6836,"value":"Analista de Correios - Estat\u00edstico"},{"id":6837,"value":"Analista de Correios - Hist\u00f3ria"},{"id":6838,"value":"Analista de Correios - Jornalismo"},{"id":6839,"value":"Analista de Correios - Letras"},{"id":6840,"value":"Analista de Correios - Muse\u00f3logo"},{"id":6841,"value":"Analista de Correios - Pedagogo"},{"id":668,"value":"Analista de Correios - Psic\u00f3logo"},{"id":6791,"value":"Analista de Desenvolvimento"},{"id":7170,"value":"Analista de Documenta\u00e7\u00e3o"},{"id":2221,"value":"Analista de Documenta\u00e7\u00e3o - Arquivologia"},{"id":2222,"value":"Analista de Documenta\u00e7\u00e3o - Biblioteconomia"},{"id":2223,"value":"Analista de Documenta\u00e7\u00e3o - Comunica\u00e7\u00e3o Social"},{"id":2224,"value":"Analista de Documenta\u00e7\u00e3o - Estat\u00edstica"},{"id":451,"value":"Analista de Finan\u00e7as e Controle"},{"id":63,"value":"Analista de Finan\u00e7as e Controle - \u00c1rea - Auditoria e Fiscaliza\u00e7\u00e3o"},{"id":62,"value":"Analista de Finan\u00e7as e Controle - \u00c1rea - Correi\u00e7\u00e3o"},{"id":13,"value":"Analista de Finan\u00e7as e Controle - Comum a todos"},{"id":7248,"value":"Analista de Finan\u00e7as e Controle - Cont\u00e1bil-Financeira"},{"id":7249,"value":"Analista de Finan\u00e7as e Controle - Desenvolvimento Institucional"},{"id":7250,"value":"Analista de Finan\u00e7as e Controle - Econ\u00f4mico-Financeira"},{"id":61,"value":"Analista de Finan\u00e7as e Controle - Tecnologia da Informa\u00e7\u00e3o"},{"id":7409,"value":"Analista de Fiscaliza\u00e7\u00e3o"},{"id":8674,"value":"Analista de Geom\u00e1tica - Engenharia da Computa\u00e7\u00e3o"},{"id":2483,"value":"Analista de Gest\u00e3o - Analista de Sistemas"},{"id":8876,"value":"Analista de Gest\u00e3o - Biblioteconomia"},{"id":8527,"value":"Analista de Gest\u00e3o - Contabilidade"},{"id":2487,"value":"Analista de Gest\u00e3o - Economista"},{"id":8528,"value":"Analista de Gest\u00e3o - Servi\u00e7o Social"},{"id":8529,"value":"Analista de Gest\u00e3o - Sistemas"},{"id":8881,"value":"Analista de Gest\u00e3o Administrativa"},{"id":1588,"value":"Analista de Gest\u00e3o Administrativa - Assistente Social"},{"id":8768,"value":"Analista de Gest\u00e3o Corporativa"},{"id":3477,"value":"Analista de Gest\u00e3o Corporativa - Analista de TI"},{"id":3478,"value":"Analista de Gest\u00e3o Corporativa - Comunica\u00e7\u00e3o Social"},{"id":4801,"value":"Analista de Gest\u00e3o Corporativa - Finan\u00e7as e Or\u00e7amento"},{"id":4803,"value":"Analista de Gest\u00e3o Corporativa - Tecnologia da Informa\u00e7\u00e3o"},{"id":85,"value":"Analista de Gest\u00e3o Corporativa J\u00fanior - \u00c1rea Tecnologia da Informa\u00e7\u00e3o"},{"id":7171,"value":"Analista de Gest\u00e3o e Tr\u00e2nsito"},{"id":6494,"value":"Analista de Gest\u00e3o em Sa\u00fade - Gest\u00e3o de Tecnologia da Informa\u00e7\u00e3o"},{"id":7126,"value":"Analista de Gest\u00e3o Pleno"},{"id":7172,"value":"Analista de Identifica\u00e7\u00e3o Civil"},{"id":8709,"value":"Analista de Informa\u00e7\u00e3o"},{"id":1941,"value":"Analista de Inform\u00e1tica"},{"id":2353,"value":"Analista de Inform\u00e1tica - Desenvolvimento de Sistemas"},{"id":2354,"value":"Analista de Inform\u00e1tica - Suporte T\u00e9cnico"},{"id":725,"value":"Analista de Infraestrutura"},{"id":7098,"value":"Analista de Log\u00edstica Pleno"},{"id":7185,"value":"Analista de Marketing"},{"id":764,"value":"Analista de Mercado de Capitais"},{"id":578,"value":"Analista de N\u00edvel Superior - Desenvolvimento de Sistemas"},{"id":6689,"value":"Analista de Opera\u00e7\u00f5es"},{"id":8767,"value":"Analista de Pesquisa Energ\u00e9tica"},{"id":4805,"value":"Analista de Pesquisa Energ\u00e9tica - G\u00e1s e Bioenergia"},{"id":4811,"value":"Analista de Pesquisa Energ\u00e9tica - Petr\u00f3leo - Abastecimento"},{"id":4812,"value":"Analista de Pesquisa Energ\u00e9tica - Petr\u00f3leo - Explora\u00e7\u00e3o"},{"id":4815,"value":"Analista de Pesquisa Energ\u00e9tica - Recursos Energ\u00e9ticos"},{"id":4816,"value":"Analista de Pesquisa Energ\u00e9tica - Transmiss\u00e3o de Energia"},{"id":7226,"value":"Analista de Planejamento - Arquitetura"},{"id":1562,"value":"Analista de Planejamento - Arquivologia"},{"id":1544,"value":"Analista de Planejamento - Assuntos Educacionais"},{"id":1564,"value":"Analista de Planejamento - Biblioteconomia"},{"id":1552,"value":"Analista de Planejamento - Ci\u00eancias Cont\u00e1beis"},{"id":7227,"value":"Analista de Planejamento - Comunica\u00e7\u00e3o Social"},{"id":7232,"value":"Analista de Planejamento - Desenvolvimento e Manuten\u00e7\u00e3o de Sistemas"},{"id":7228,"value":"Analista de Planejamento - Direito"},{"id":7229,"value":"Analista de Planejamento - Engenharia de Seguran\u00e7a do Trabalho"},{"id":7230,"value":"Analista de Planejamento - Engenharia Mec\u00e2nica"},{"id":7231,"value":"Analista de Planejamento - Estatistica"},{"id":1555,"value":"Analista de Planejamento - Gest\u00e3o em Pesquisa"},{"id":7234,"value":"Analista de Planejamento - Gest\u00e3o Financeira"},{"id":1568,"value":"Analista de Planejamento - Historia"},{"id":7233,"value":"Analista de Planejamento - Infraestrutura em TI"},{"id":1569,"value":"Analista de Planejamento - Jornalismo"},{"id":1570,"value":"Analista de Planejamento - Pedagogia"},{"id":688,"value":"Analista de Planejamento e Or\u00e7amento"},{"id":2755,"value":"Analista de planejamento e or\u00e7amento - apo"},{"id":584,"value":"Analista de Planejamento e Or\u00e7amento - Planejamento e Or\u00e7amento"},{"id":585,"value":"Analista de Planejamento e Or\u00e7amento - Tecnologia da Informa\u00e7\u00e3o"},{"id":8866,"value":"Analista de Pol\u00edticas P\u00fablicas - Sistemas"},{"id":5091,"value":"Analista de Processos"},{"id":684,"value":"Analista de Processos Organizacionais - An\u00e1lise de Sistemas"},{"id":682,"value":"Analista de Processos Organizacionais - Contabilidade"},{"id":681,"value":"Analista de Processos Organizacionais - Direito"},{"id":683,"value":"Analista de Processos Organizacionais - Economia"},{"id":1033,"value":"Analista de Processos Organizacionais - Engenharia"},{"id":7518,"value":"Analista de Procuradoria - \u00c1rea de Apoio Calculista"},{"id":7519,"value":"Analista de Procuradoria - \u00c1rea de Apoio Jur\u00eddico"},{"id":6984,"value":"Analista de Projetos - Agronomia"},{"id":6985,"value":"Analista de Projetos - Econ\u00f4mico-Financeira"},{"id":6986,"value":"Analista de Projetos - Engenharia"},{"id":6987,"value":"Analista de Projetos - Jur\u00eddica"},{"id":8326,"value":"Analista de Promotoria - Assistente Jur\u00eddico"},{"id":5115,"value":"Analista de Promotoria I"},{"id":8380,"value":"Analista de Promotoria II"},{"id":6960,"value":"Analista de Prote\u00e7\u00e3o e Defesa do Consumidor"},{"id":5394,"value":"Analista de Recursos Humanos"},{"id":6182,"value":"Analista de Recursos Humanos J\u00fanior"},{"id":89,"value":"Analista de redes e comunica\u00e7\u00e3o de dados"},{"id":902,"value":"Analista de Rela\u00e7\u00f5es P\u00fablicas"},{"id":472,"value":"Analista de Saneamento - Analista de Tecnologia da Informa\u00e7\u00e3o - Desenvolvimento"},{"id":7154,"value":"Analista de Sa\u00fade"},{"id":2230,"value":"Analista de Sa\u00fade - Enfermagem"},{"id":2234,"value":"Analista de Sa\u00fade - Servi\u00e7o Social"},{"id":2629,"value":"Analista de Seguran\u00e7a"},{"id":5092,"value":"Analista de Sistema Operacional"},{"id":2,"value":"Analista de Sistemas"},{"id":2247,"value":"Analista de Sistemas - Administra\u00e7\u00e3o de Dados"},{"id":6847,"value":"Analista de Sistemas - Arquitetura de Software"},{"id":1539,"value":"Analista de Sistemas - Desenvolvimento de Aplica\u00e7\u00f5es"},{"id":3535,"value":"Analista de Sistemas - Desenvolvimento de Sistemas"},{"id":6848,"value":"Analista de Sistemas - Desenvolvimento e Manuten\u00e7\u00e3o"},{"id":6849,"value":"Analista de Sistemas - Gest\u00e3o de TI"},{"id":6851,"value":"Analista de Sistemas - Seguran\u00e7a da Informa\u00e7\u00e3o"},{"id":1540,"value":"Analista de Sistemas - Suporte"},{"id":2256,"value":"Analista de Sistemas - Suporte Basis SAP R3"},{"id":5039,"value":"Analista de Sistemas J\u00fanior"},{"id":143,"value":"Analista de Sistemas J\u00fanior - Engenharia de Software"},{"id":145,"value":"Analista de Sistemas J\u00fanior - Infra-Estrutura"},{"id":144,"value":"Analista de Sistemas J\u00fanior - Processos de Neg\u00f3cios"},{"id":92,"value":"Analista de Sistemas Pleno  -  Engenharia de Software"},{"id":91,"value":"Analista de Sistemas Pleno  -  Infra-estrutura"},{"id":90,"value":"Analista de Sistemas Pleno  -  Processos"},{"id":1153,"value":"Analista de Suporte"},{"id":899,"value":"Analista de Suporte de Sistemas"},{"id":4346,"value":"Analista de Suprimento J\u00fanior"},{"id":8744,"value":"Analista de Tecnologia - Obras"},{"id":1496,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o"},{"id":3714,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o - An\u00e1lise de Informa\u00e7\u00f5es"},{"id":4720,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o - Contabilidade"},{"id":7105,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o - Desenvolvimento"},{"id":3716,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o - Desenvolvimento de Sistemas"},{"id":986,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o - Engenharia de Software"},{"id":3717,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o - Redes"},{"id":8611,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o - Seguran\u00e7a da Informa\u00e7\u00e3o"},{"id":987,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o - Suporte T\u00e9cnico"},{"id":7189,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o - Telecomunica\u00e7\u00f5es"},{"id":7190,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o - Teste e Qualidade"},{"id":7191,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o - Web Design"},{"id":7853,"value":"Analista de Tecnologia da Informa\u00e7\u00e3o e Comunica\u00e7\u00e3o I"},{"id":2630,"value":"Analista de Teste de Qualidade"},{"id":2454,"value":"Analista de TI - Administra\u00e7\u00e3o de Dados"},{"id":2455,"value":"Analista de TI - Analista de Suporte"},{"id":3040,"value":"Analista de TI - Analista de Telecomunica\u00e7\u00f5es"},{"id":3034,"value":"Analista de TI - Desenvolvimento de Sistemas"},{"id":2458,"value":"Analista de TI - Sistemas"},{"id":7706,"value":"Analista de TI - Suporte"},{"id":4349,"value":"Analista de TI J\u00fanior"},{"id":2018,"value":"Analista de TIC - Infraestrutura"},{"id":2030,"value":"Analista de TIC - Seguran\u00e7a da Informa\u00e7\u00e3o"},{"id":1299,"value":"Analista de Tr\u00e2nsito"},{"id":1302,"value":"Analista de Tr\u00e2nsito - Assessor Jur\u00eddico"},{"id":641,"value":"Analista do Banco Central"},{"id":530,"value":"Analista do Banco Central - \u00c1rea 1"},{"id":531,"value":"Analista do Banco Central - \u00c1rea 2"},{"id":532,"value":"Analista do Banco Central - \u00c1rea 3"},{"id":534,"value":"Analista do Banco Central - \u00c1rea 5"},{"id":535,"value":"Analista do Banco Central - \u00c1rea 6"},{"id":1708,"value":"Analista do Minist\u00e9rio P\u00fablico"},{"id":715,"value":"Analista do Minist\u00e9rio P\u00fablico - Especialidade Direito"},{"id":713,"value":"Analista do Minist\u00e9rio P\u00fablico \u2013 Especialidade An\u00e1lise de Sistemas"},{"id":714,"value":"Analista do Minist\u00e9rio P\u00fablico \u2013 Especialidade Contabilidade"},{"id":716,"value":"Analista do Minist\u00e9rio P\u00fablico \u2013 Especialidade Servi\u00e7o Social"},{"id":232,"value":"Analista do Seguro Social"},{"id":217,"value":"Analista do Seguro Social - Arquitetura"},{"id":218,"value":"Analista do Seguro Social - Arquivologia"},{"id":219,"value":"Analista do Seguro Social - Biblioteconomia"},{"id":1169,"value":"Analista do Seguro Social - Ci\u00eancia Atuariais"},{"id":220,"value":"Analista do Seguro Social - Ci\u00eancia da Computa\u00e7\u00e3o"},{"id":222,"value":"Analista do Seguro Social - Comunica\u00e7\u00e3o Social"},{"id":223,"value":"Analista do Seguro Social - Direito"},{"id":225,"value":"Analista do Seguro Social - Engenharia de Seguran\u00e7a do Trabalho"},{"id":226,"value":"Analista do Seguro Social - Engenharia de Telecomunica\u00e7\u00f5es"},{"id":228,"value":"Analista do Seguro Social - Engenharia Mec\u00e2nica"},{"id":229,"value":"Analista do Seguro Social - Estat\u00edstica"},{"id":230,"value":"Analista do Seguro Social - Pedagogia"},{"id":231,"value":"Analista do Seguro Social - Psicologia"},{"id":233,"value":"Analista do Seguro Social - Terapia Ocupacional"},{"id":7379,"value":"Analista Econ\u00f4mico - Financeiro"},{"id":3932,"value":"Analista Educacional"},{"id":1197,"value":"Analista em C&T J\u00fanior - Comunica\u00e7\u00e3o Social - Jornalismo"},{"id":1198,"value":"Analista em C&T J\u00fanior - Comunica\u00e7\u00e3o Social - Rela\u00e7\u00f5es P\u00fablicas"},{"id":1199,"value":"Analista em C&T J\u00fanior - Direito - Legisla\u00e7\u00e3o P\u00fablica em Sa\u00fade"},{"id":1200,"value":"Analista em C&T J\u00fanior - Engenharia - Cl\u00ednica"},{"id":1201,"value":"Analista em C&T J\u00fanior - Engenharia - Infraestrutura"},{"id":1202,"value":"Analista em C&T J\u00fanior - Gest\u00e3o de Recursos Humanos"},{"id":1203,"value":"Analista em C&T J\u00fanior - Gest\u00e3o P\u00fablica"},{"id":4987,"value":"Analista em C&T Pleno 1-I"},{"id":3705,"value":"Analista em Ci\u00eancia e Tecnologia - Ci\u00eancias Cont\u00e1beis"},{"id":5282,"value":"Analista em Ci\u00eancia e Tecnologia J\u00fanior - Geral"},{"id":7465,"value":"Analista em Ci\u00eancia e Tecnologia Pleno"},{"id":7042,"value":"Analista em Comunica\u00e7\u00e3o e Processamento de Dados"},{"id":7474,"value":"Analista em Geoci\u00eancias"},{"id":7630,"value":"Analista em Geoci\u00eancias - Biblioteconomia"},{"id":7632,"value":"Analista em Geoci\u00eancias - Contabilidade"},{"id":7628,"value":"Analista em Geoci\u00eancias - Direito"},{"id":7633,"value":"Analista em Geoci\u00eancias - Economia"},{"id":7636,"value":"Analista em Geoci\u00eancias - Oceanografia"},{"id":7637,"value":"Analista em Geoci\u00eancias - Qu\u00edmica"},{"id":7638,"value":"Analista em Geoci\u00eancias - Sistemas"},{"id":1678,"value":"Analista em Gest\u00e3o Administrativa"},{"id":7122,"value":"Analista em Gest\u00e3o Especializado"},{"id":7180,"value":"Analista em Infraestrutura de Transportes"},{"id":827,"value":"Analista em Planejamento, Or\u00e7amento e Finan\u00e7as P\u00fablicas"},{"id":7589,"value":"Analista Executivo"},{"id":7243,"value":"Analista Executivo - Direito"},{"id":6707,"value":"Analista Executivo em Metrologia e Qualidade - Avalia\u00e7\u00e3o da Conformidade"},{"id":730,"value":"Analista Executivo em Metrologia e Qualidade - Comunica\u00e7\u00e3o Social-Jornalismo"},{"id":732,"value":"Analista Executivo em Metrologia e Qualidade - Desenvolvimento de Sistemas"},{"id":6708,"value":"Analista Executivo em Metrologia e Qualidade - Gest\u00e3o P\u00fablica"},{"id":738,"value":"Analista Executivo em Metrologia e Qualidade - Redes"},{"id":1676,"value":"Analista financeiro (institui\u00e7\u00f5es financeiras)"},{"id":4355,"value":"Analista Financeiro Pleno"},{"id":845,"value":"Analista Judici\u00e1rio"},{"id":5330,"value":"Analista Judici\u00e1rio - An\u00e1lise de Sistemas"},{"id":5311,"value":"Analista Judici\u00e1rio - An\u00e1lise de Sistemas - Desenvolvimento"},{"id":8242,"value":"Analista Judici\u00e1rio - An\u00e1lise de Sistemas de Informa\u00e7\u00e3o"},{"id":6813,"value":"Analista Judici\u00e1rio - An\u00e1lise de Suporte"},{"id":276,"value":"Analista Judici\u00e1rio - Analista de Sistemas Suporte"},{"id":8838,"value":"Analista Judici\u00e1rio - \u00c1rea Administrativa"},{"id":43,"value":"Analista Judici\u00e1rio - \u00c1rea Judici\u00e1ria"},{"id":44,"value":"Analista Judici\u00e1rio - \u00c1rea Judici\u00e1ria - Execu\u00e7\u00e3o de Mandados"},{"id":190,"value":"Analista Judici\u00e1rio - Arquitetura"},{"id":185,"value":"Analista Judici\u00e1rio - Arquivologia"},{"id":130,"value":"Analista Judici\u00e1rio - Assist\u00eancia Social"},{"id":7165,"value":"Analista Judici\u00e1rio - Bibliotec\u00e1rio"},{"id":65,"value":"Analista Judici\u00e1rio - Biblioteconomia"},{"id":6760,"value":"Analista Judici\u00e1rio - Cl\u00ednica M\u00e9dica"},{"id":630,"value":"Analista Judici\u00e1rio - Comunica\u00e7\u00e3o Social"},{"id":64,"value":"Analista Judici\u00e1rio - Contabilidade"},{"id":6687,"value":"Analista Judici\u00e1rio - Direito"},{"id":187,"value":"Analista Judici\u00e1rio - Economia"},{"id":6843,"value":"Analista Judici\u00e1rio - Educa\u00e7\u00e3o F\u00edsica"},{"id":194,"value":"Analista Judici\u00e1rio - Enfermagem"},{"id":6784,"value":"Analista Judici\u00e1rio - Engenharia"},{"id":235,"value":"Analista Judici\u00e1rio - Engenharia de Seguran\u00e7a do Trabalho"},{"id":202,"value":"Analista Judici\u00e1rio - Engenharia Mec\u00e2nica"},{"id":560,"value":"Analista Judici\u00e1rio - Escriv\u00e3o Judicial"},{"id":186,"value":"Analista Judici\u00e1rio - Estat\u00edstica"},{"id":2311,"value":"Analista Judici\u00e1rio - Execu\u00e7\u00e3o de Mandados"},{"id":6814,"value":"Analista Judici\u00e1rio - Execu\u00e7\u00e3o Penal"},{"id":197,"value":"Analista Judici\u00e1rio - Fisioterapia"},{"id":279,"value":"Analista Judici\u00e1rio - Fonoaudi\u00f3logo"},{"id":2310,"value":"Analista Judici\u00e1rio - Inform\u00e1tica"},{"id":280,"value":"Analista Judici\u00e1rio - Jornalista"},{"id":6936,"value":"Analista Judici\u00e1rio - Judici\u00e1ria e Administrativa"},{"id":2272,"value":"Analista Judici\u00e1rio - Letras"},{"id":8846,"value":"Analista Judici\u00e1rio - Medicina"},{"id":214,"value":"Analista Judici\u00e1rio - Nutri\u00e7\u00e3o"},{"id":67,"value":"Analista Judici\u00e1rio - Odontologia"},{"id":133,"value":"Analista Judici\u00e1rio - Oficial de Justi\u00e7a"},{"id":6815,"value":"Analista Judici\u00e1rio - Oficial de Justi\u00e7a Avaliador"},{"id":193,"value":"Analista Judici\u00e1rio - Pedagogia"},{"id":75,"value":"Analista Judici\u00e1rio - Psicologia"},{"id":615,"value":"Analista Judici\u00e1rio - Psicologia do Trabalho"},{"id":6844,"value":"Analista Judici\u00e1rio - Publicidade e Propaganda"},{"id":7627,"value":"Analista Judici\u00e1rio - Qualquer \u00c1rea de Forma\u00e7\u00e3o"},{"id":6845,"value":"Analista Judici\u00e1rio - R\u00e1dio e TV"},{"id":3649,"value":"Analista Judici\u00e1rio - Rela\u00e7\u00f5es P\u00fablicas"},{"id":6763,"value":"Analista Judici\u00e1rio - Revisor de Texto"},{"id":191,"value":"Analista Judici\u00e1rio - Servi\u00e7o Social"},{"id":8244,"value":"Analista Judici\u00e1rio - Suporte em Tecnologia da Informa\u00e7\u00e3o"},{"id":68,"value":"Analista Judici\u00e1rio - Taquigrafia"},{"id":177,"value":"Analista Judici\u00e1rio - Tecnologia da Informa\u00e7\u00e3o"},{"id":7622,"value":"Analista Judici\u00e1rio-Oficial de Justi\u00e7a Avaliador e Leiloeiro"},{"id":1497,"value":"Analista Jur\u00eddico"},{"id":6672,"value":"Analista Jur\u00eddico - Analista de Sistemas"},{"id":6673,"value":"Analista Jur\u00eddico - Contabilidade"},{"id":6674,"value":"Analista Jur\u00eddico - Direito e Legisla\u00e7\u00e3o"},{"id":7006,"value":"Analista Legislativo"},{"id":8777,"value":"Analista Legislativo - Administra\u00e7\u00e3o de Servidores"},{"id":7977,"value":"Analista Legislativo - Analista de Sistemas"},{"id":7978,"value":"Analista Legislativo - Arquitetura"},{"id":8710,"value":"Analista Legislativo - Comunica\u00e7\u00e3o Social"},{"id":8659,"value":"Analista Legislativo - Contabilidade"},{"id":8779,"value":"Analista Legislativo - Desenvolvimento e Manuten\u00e7\u00e3o de Programas"},{"id":8776,"value":"Analista Legislativo - Direito"},{"id":8660,"value":"Analista Legislativo - Infraestrutura"},{"id":8872,"value":"Analista Legislativo - Pesquisador Legislativo"},{"id":894,"value":"Analista Legislativo - Processo Legislativo"},{"id":8661,"value":"Analista Legislativo - Sistemas"},{"id":7525,"value":"Analista Ministerial - An\u00e1lise e Desenvolvimento de Sistemas"},{"id":6937,"value":"Analista Ministerial - \u00c1rea Administrativa"},{"id":6981,"value":"Analista Ministerial - \u00c1rea Jur\u00eddica"},{"id":6944,"value":"Analista Ministerial - \u00c1rea Processual"},{"id":6938,"value":"Analista Ministerial - Arquitetura"},{"id":7112,"value":"Analista Ministerial - Biblioteconomia"},{"id":6982,"value":"Analista Ministerial - Ci\u00eancias Cont\u00e1beis"},{"id":8378,"value":"Analista Ministerial - Ci\u00eancias da Computa\u00e7\u00e3o"},{"id":6939,"value":"Analista Ministerial - Controle Interno"},{"id":7113,"value":"Analista Ministerial - Direito"},{"id":6940,"value":"Analista Ministerial - Documenta\u00e7\u00e3o"},{"id":7114,"value":"Analista Ministerial - Economia"},{"id":6942,"value":"Analista Ministerial - Inform\u00e1tica"},{"id":6943,"value":"Analista Ministerial - Or\u00e7amento"},{"id":6992,"value":"Analista Ministerial - Psicologia"},{"id":7531,"value":"Analista Ministerial - Seguran\u00e7a da Informa\u00e7\u00e3o"},{"id":6945,"value":"Analista Ministerial - Servi\u00e7o Social"},{"id":7115,"value":"Analista Ministerial - Tecnologia da Informa\u00e7\u00e3o"},{"id":7532,"value":"Analista Ministerial - Teste e Qualidade de Software"},{"id":8771,"value":"Analista Organizacional - Ci\u00eancias Cont\u00e1beis"},{"id":8770,"value":"Analista Organizacional - Ci\u00eancias Jur\u00eddicas"},{"id":2046,"value":"Analista Organizacional - Servi\u00e7o Social"},{"id":2235,"value":"Analista Pericial"},{"id":8705,"value":"Analista Portu\u00e1rio - Biblioteconomia"},{"id":7856,"value":"Analista Previdenci\u00e1rio"},{"id":3645,"value":"Analista Previdenci\u00e1rio - Contabilidade"},{"id":8908,"value":"Analista P\u00fablico de Gest\u00e3o"},{"id":6789,"value":"Analista Sociocultural - Pedagogia"},{"id":1942,"value":"Analista Suporte Gest\u00e3o"},{"id":806,"value":"Analista T\u00e9cnico"},{"id":888,"value":"Analista T\u00e9cnico - Administrativo"},{"id":2492,"value":"Analista T\u00e9cnico - Arquiteto"},{"id":5124,"value":"Analista T\u00e9cnico - Auditoria"},{"id":5128,"value":"Analista T\u00e9cnico - Gest\u00e3o de Pessoas"},{"id":635,"value":"Analista T\u00e9cnico - Tecnologia da Informa\u00e7\u00e3o"},{"id":7483,"value":"Analista T\u00e9cnico Administrativo - Ci\u00eancia Jur\u00eddicas"},{"id":8175,"value":"Analista T\u00e9cnico de Controle Externo - Auditoria Governamental"},{"id":8174,"value":"Analista T\u00e9cnico de Controle Externo - Minist\u00e9rio P\u00fablico"},{"id":7177,"value":"Analista T\u00e9cnico de Gest\u00e3o Ambiental - Classe IV"},{"id":7962,"value":"Analista T\u00e9cnico em Gest\u00e3o de Registro Mercantil - Analista de Inform\u00e1tica"},{"id":7961,"value":"Analista T\u00e9cnico em Gest\u00e3o de Registro Mercantil - Analista T\u00e9cnico Administrativo"},{"id":2136,"value":"Analista T\u00e9cnico \u2013 Assistente Social"},{"id":2145,"value":"Analista T\u00e9cnico \u2013 Psic\u00f3logo"},{"id":101,"value":"Analista Treinee - An\u00e1lise de Sistemas"},{"id":104,"value":"Analista Treinee - Ci\u00eancias Cont\u00e1beis"},{"id":103,"value":"Analista Treinee - Ci\u00eancias da Computa\u00e7\u00e3o"},{"id":107,"value":"Analista Treinee - Economia"},{"id":110,"value":"Analista Treinee - Matem\u00e1tica"},{"id":106,"value":"Analista Treinee - Psicologia"},{"id":369,"value":"Analista Tribut\u00e1rio da Receita Federal"},{"id":8510,"value":"Aprendiz - Marinheiro"},{"id":2208,"value":"Arque\u00f3logo"},{"id":8898,"value":"Arquiteto"},{"id":6852,"value":"Arquiteto - Planejamento F\u00edsico de Aeroportos"},{"id":2631,"value":"Arquiteto de Sistemas"},{"id":7726,"value":"Arquiteto urbanista"},{"id":1193,"value":"Arquivista"},{"id":614,"value":"Arquivologista"},{"id":3404,"value":"Arte - Educador"},{"id":8714,"value":"Artes C\u00eanicas - Instrutor Circense"},{"id":6925,"value":"Aspirante"},{"id":542,"value":"Assessor - Contabilidade"},{"id":543,"value":"Assessor - Direito"},{"id":8610,"value":"Assessor - Tecnologia da Informa\u00e7\u00e3o"},{"id":8456,"value":"Assessor Administrativo"},{"id":7168,"value":"Assessor de Estabelecimento Penal"},{"id":977,"value":"Assessor Jur\u00eddico"},{"id":6651,"value":"Assessor T\u00e9cnico - Contabilidade"},{"id":6652,"value":"Assessor T\u00e9cnico - Economia"},{"id":553,"value":"Assessor T\u00e9cnico de Inform\u00e1tica"},{"id":554,"value":"Assessor T\u00e9cnico Jur\u00eddico"},{"id":1630,"value":"Assessor T\u00e9cnico Legislativo"},{"id":1962,"value":"Assistente"},{"id":5468,"value":"Assistente - Apoio administrativo"},{"id":1924,"value":"Assistente - Contabilidade"},{"id":8890,"value":"Assistente - Educa\u00e7\u00e3o b\u00e1sica"},{"id":8649,"value":"Assistente - Gest\u00e3o em Metrologia e Qualidade Industrial"},{"id":1963,"value":"Assistente - Inform\u00e1tica"},{"id":6581,"value":"Assistente - Tecnologia"},{"id":303,"value":"Assistente Administrativo"},{"id":4770,"value":"Assistente Administrativo - Financeiro"},{"id":6186,"value":"Assistente de Administra\u00e7\u00e3o"},{"id":1510,"value":"Assistente de Alunos"},{"id":18,"value":"Assistente de Chancelaria"},{"id":7123,"value":"Assistente de Defensoria P\u00fablica"},{"id":6816,"value":"Assistente de Fiscaliza\u00e7\u00e3o"},{"id":8852,"value":"Assistente de Gabinete"},{"id":5661,"value":"Assistente de Gest\u00e3o"},{"id":1621,"value":"Assistente de Gest\u00e3o Administrativa"},{"id":3043,"value":"Assistente de Hardware"},{"id":7794,"value":"Assistente de Inform\u00e1tica"},{"id":4294,"value":"Assistente de Laboratorio"},{"id":8750,"value":"Assistente de Manuten\u00e7\u00e3o - El\u00e9trica"},{"id":2409,"value":"Assistente de Patrim\u00f4nio"},{"id":6507,"value":"Assistente de Procuradoria"},{"id":6187,"value":"Assistente de Recursos Humanos"},{"id":8805,"value":"Assistente de Saneamento - Assistente de Gest\u00e3o"},{"id":8514,"value":"Assistente de Suporte Acad\u00eamico"},{"id":5663,"value":"Assistente de Tecnologia da Informa\u00e7\u00e3o"},{"id":7730,"value":"Assistente em Administra\u00e7\u00e3o"},{"id":1282,"value":"Assistente Em C&T 1 \u2013 Apoio T\u00e9cnicoadministrativo"},{"id":5015,"value":"Assistente em C&T 3-I"},{"id":137,"value":"Assistente em Ci\u00eancia e Tecnologia"},{"id":2759,"value":"Assistente Executivo"},{"id":8859,"value":"Assistente Executivo em Metrologia e Qualidade - Administra\u00e7\u00e3o"},{"id":8861,"value":"Assistente Executivo em Metrologia e Qualidade - Inform\u00e1tica"},{"id":2412,"value":"Assistente Financeiro"},{"id":7614,"value":"Assistente Judici\u00e1rio-Web Designer"},{"id":839,"value":"Assistente Jur\u00eddico"},{"id":8684,"value":"Assistente Jur\u00eddico - Advocacia"},{"id":558,"value":"Assistente Legislativo"},{"id":116,"value":"Assistente Legislativo  -  Manuten\u00e7\u00e3o em Inform\u00e1tica"},{"id":8875,"value":"Assistente Legislativo - Operador T\u00e9cnico"},{"id":6000,"value":"Assistente Operacional"},{"id":8773,"value":"Assistente Organizacional - \u00c1rea Administrativa"},{"id":6225,"value":"Assistente Previdenci\u00e1rio"},{"id":7603,"value":"Assistente Securit\u00e1rio"},{"id":400,"value":"Assistente social"},{"id":669,"value":"Assistente T\u00e9cnico"},{"id":8579,"value":"Assistente T\u00e9cnico - Administra\u00e7\u00e3o e Finan\u00e7as"},{"id":6664,"value":"Assistente T\u00e9cnico - Administrativa"},{"id":8587,"value":"Assistente T\u00e9cnico - Inform\u00e1tica"},{"id":2942,"value":"Assistente T\u00e9cnico - Seguran\u00e7a do Trabalho"},{"id":5637,"value":"Assistente T\u00e9cnico - T\u00e9cnico em Sa\u00fade"},{"id":359,"value":"Assistente T\u00e9cnico Administrativo"},{"id":7916,"value":"Assistente T\u00e9cnico de Identifica\u00e7\u00e3o Civil"},{"id":7620,"value":"Assistente T\u00e9cnico Judici\u00e1rio"},{"id":2150,"value":"Assistente T\u00e9cnico \u2013 T\u00e9cnico em Seguran\u00e7a do Trabalho"},{"id":1419,"value":"Atendente"},{"id":8219,"value":"Atendente Administrativo"},{"id":1075,"value":"Atendente Consult\u00f3rio Dent\u00e1rio"},{"id":8607,"value":"Atendente de Educa\u00e7\u00e3o Infantil"},{"id":7806,"value":"Atendente de Necrot\u00e9rio Policial"},{"id":6802,"value":"Atendente de Reintegra\u00e7\u00e3o Social"},{"id":19,"value":"Atendente Judici\u00e1rio"},{"id":1154,"value":"Atendente Social"},{"id":7588,"value":"Atividade de Complexidade Gerencial"},{"id":7545,"value":"Atividade de Complexidade Intelectual"},{"id":7946,"value":"Atividade T\u00e9c. de Complexidade Intelectual - Administra\u00e7\u00e3o - Ci\u00eancias Cont\u00e1beis"},{"id":7952,"value":"Atividade T\u00e9c. de Suporte - Direito"},{"id":7544,"value":"Atividade T\u00e9cnica de Suporte"},{"id":7543,"value":"Atividade T\u00e9cnica de Suporte - Direito"},{"id":948,"value":"Auditor"},{"id":5117,"value":"Auditor - Ci\u00eancias Cont\u00e1beis"},{"id":5119,"value":"Auditor - Direito"},{"id":696,"value":"Auditor da Receita do Estado"},{"id":951,"value":"Auditor de Contas P\u00fablicas"},{"id":6990,"value":"Auditor de Controle Externo"},{"id":7306,"value":"Auditor de Controle Externo - Direito"},{"id":6973,"value":"Auditor de Controle Externo - Inform\u00e1tica"},{"id":6969,"value":"Auditor de Controle Externo - Jur\u00eddica"},{"id":5834,"value":"Auditor de Controle Interno"},{"id":590,"value":"Auditor do Estado \u2013 Direito"},{"id":593,"value":"Auditor do Estado \u2013 Tecnologia da Informa\u00e7\u00e3o"},{"id":843,"value":"Auditor do Tesouro Municipal"},{"id":842,"value":"Auditor do Tesouro Municipal - Tecnologia da Informa\u00e7\u00e3o"},{"id":27,"value":"Auditor do Tribunal de Contas"},{"id":8170,"value":"Auditor Federal de Controle Externo"},{"id":6933,"value":"Auditor Federal de Controle Externo - Auditoria de Obras P\u00fablicas"},{"id":6932,"value":"Auditor Federal de Controle Externo - Auditoria Governamental"},{"id":6934,"value":"Auditor Federal de Controle Externo - Psicologia"},{"id":2750,"value":"Auditor Federal de Controle Externo - Tecnologia da Informa\u00e7\u00e3o"},{"id":8262,"value":"Auditor Fiscal"},{"id":1034,"value":"Auditor Fiscal da Receita Estadual"},{"id":20,"value":"Auditor Fiscal da Receita Federal"},{"id":93,"value":"Auditor Fiscal da Receita Federal - \u00c1rea Tecnologia da Informa\u00e7\u00e3o"},{"id":94,"value":"Auditor Fiscal da Receita Federal - \u00c1rea Tribut\u00e1ria e Aduaneira"},{"id":6701,"value":"Auditor Fiscal de Atividades Urbanas - Controle Ambiental"},{"id":6700,"value":"Auditor Fiscal de Atividades Urbanas - Transportes"},{"id":4258,"value":"Auditor Fiscal do Munic\u00edpio"},{"id":844,"value":"Auditor Fiscal do Tesouro Estadual"},{"id":12,"value":"Auditor Fiscal do Trabalho"},{"id":6913,"value":"Auditor P\u00fablico Externo"},{"id":6643,"value":"Auditor Tribut\u00e1rio"},{"id":694,"value":"Auxiliar"},{"id":365,"value":"Auxiliar administrativo"},{"id":528,"value":"Auxiliar da Fiscaliza\u00e7\u00e3o Financeira"},{"id":770,"value":"Auxiliar da Fiscaliza\u00e7\u00e3o Financeira II"},{"id":6191,"value":"Auxiliar de Administra\u00e7\u00e3o"},{"id":1965,"value":"Auxiliar de Atividades Pedag\u00f3gicas"},{"id":5890,"value":"Auxiliar de Autopsia"},{"id":1511,"value":"Auxiliar de Biblioteca"},{"id":8053,"value":"Auxiliar de Comunica\u00e7\u00e3o"},{"id":5305,"value":"Auxiliar de Creche"},{"id":7459,"value":"Auxiliar de Desenvolvimento"},{"id":7182,"value":"Auxiliar de Documenta\u00e7\u00e3o"},{"id":1085,"value":"Auxiliar de Enfermagem"},{"id":3666,"value":"Auxiliar de Farm\u00e1cia"},{"id":7915,"value":"Auxiliar de Fiscaliza\u00e7\u00e3o"},{"id":6979,"value":"Auxiliar de Fiscaliza\u00e7\u00e3o Financeira"},{"id":3521,"value":"Auxiliar de Gest\u00e3o"},{"id":4646,"value":"Auxiliar de Inform\u00e1tica"},{"id":7472,"value":"Auxiliar de Manuten\u00e7\u00e3o"},{"id":4947,"value":"Auxiliar de Necr\u00f3psia"},{"id":8902,"value":"Auxiliar de Oficiais da Pol\u00edcia Militar"},{"id":8119,"value":"Auxiliar de Papiloscopista Policial"},{"id":6748,"value":"Auxiliar de Per\u00edcia M\u00e9dico-legal"},{"id":6510,"value":"Auxiliar de Procuradoria"},{"id":5427,"value":"Auxiliar de Saneamento"},{"id":3007,"value":"Auxiliar de Sa\u00fade"},{"id":2512,"value":"Auxiliar de Secretaria"},{"id":1966,"value":"Auxiliar de Servi\u00e7os"},{"id":6703,"value":"Auxiliar de Servi\u00e7os Administrativos"},{"id":366,"value":"Auxiliar de servi\u00e7os gerais"},{"id":8173,"value":"Auxiliar de Veterin\u00e1ria e Zootecnia"},{"id":189,"value":"Auxiliar Judici\u00e1rio"},{"id":9,"value":"Auxiliar Judici\u00e1rio - \u00c1rea Administrativa"},{"id":8238,"value":"Auxiliar M\u00e9dico Legal"},{"id":8327,"value":"Auxiliar Motorista"},{"id":3502,"value":"Auxiliar Operacional"},{"id":3233,"value":"Auxiliar Operacional de Servi\u00e7os Diversos"},{"id":8816,"value":"Auxiliar Pericial"},{"id":1674,"value":"Auxiliar T\u00e9cnico"},{"id":7778,"value":"Auxiliar T\u00e9cnico - Administra\u00e7\u00e3o"},{"id":1756,"value":"Auxiliar T\u00e9cnico - Biblioteca"},{"id":8446,"value":"Auxiliar T\u00e9cnico de Educa\u00e7\u00e3o"},{"id":98,"value":"Auxiliar T\u00e9cnico de Inform\u00e1tica"},{"id":7007,"value":"Bibliotec\u00e1rio"},{"id":355,"value":"Bibliotec\u00e1rio Documentalista"},{"id":5221,"value":"Bibliotec\u00e1rio J\u00fanior"},{"id":1088,"value":"Biblioteconomista"},{"id":545,"value":"Bi\u00f3logo"},{"id":1089,"value":"Biom\u00e9dico"},{"id":3545,"value":"Bombeiro"},{"id":7419,"value":"Cabo"},{"id":7563,"value":"Cabo - T\u00e9cnico em Contabilidade"},{"id":7576,"value":"Cabo - T\u00e9cnico em Metalurgia"},{"id":7578,"value":"Cabo - T\u00e9cnico em Motores"},{"id":7581,"value":"Cabo - T\u00e9cnico em Processamento de Dados"},{"id":7583,"value":"Cabo - T\u00e9cnico em Qu\u00edmica"},{"id":8825,"value":"Cabo - T\u00e9cnico em Radiologia M\u00e9dica"},{"id":8708,"value":"Cadete"},{"id":7174,"value":"Cadete da Pol\u00edcia Militar"},{"id":8747,"value":"Capel\u00e3o Militar"},{"id":6922,"value":"Capel\u00e3o Militar Cat\u00f3lico"},{"id":6923,"value":"Capel\u00e3o Militar Evang\u00e9lico"},{"id":2377,"value":"Cargos de N\u00edvel M\u00e9dio"},{"id":2378,"value":"Cargos de N\u00edvel Superior"},{"id":3659,"value":"Cirurgi\u00e3o-dentista"},{"id":4132,"value":"Cirurgi\u00e3o-Dentista - Endodontia"},{"id":1080,"value":"Comiss\u00e1rio da Inf\u00e2ncia e da Juventude"},{"id":1684,"value":"Comiss\u00e1rio de Pol\u00edcia"},{"id":8513,"value":"Comprador Pleno"},{"id":8276,"value":"Comunica\u00e7\u00e3o Social"},{"id":23,"value":"Comunica\u00e7\u00e3o Social (Bacharelado)"},{"id":142,"value":"Comunica\u00e7\u00e3o Social - Rela\u00e7\u00f5es P\u00fablicas"},{"id":2261,"value":"Comunicador Social - Jornalismo"},{"id":8449,"value":"Conciliador C\u00edvil"},{"id":8450,"value":"Conciliador Criminal"},{"id":8895,"value":"Condutor de Ve\u00edculo de Emerg\u00eancia Terrestre"},{"id":6194,"value":"Conferente"},{"id":8065,"value":"Conselheiro Tutelar"},{"id":7407,"value":"Consultor"},{"id":900,"value":"Consultor de Or\u00e7amento"},{"id":7311,"value":"Consultor Legislativo"},{"id":8843,"value":"Consultor T\u00e9cnico Legislativo - Inform\u00e1tica"},{"id":24,"value":"Contador"},{"id":2974,"value":"Cont\u00ednuo"},{"id":7136,"value":"Controlador de Tr\u00e1fego A\u00e9reo C\u00f3digo"},{"id":1767,"value":"Controlador Interno"},{"id":1702,"value":"Cozinheiro"},{"id":7036,"value":"Cuidador"},{"id":175,"value":"Defensor P\u00fablico"},{"id":169,"value":"Delegado de Pol\u00edcia"},{"id":360,"value":"Desenhista"},{"id":1435,"value":"Desenhista Industrial"},{"id":2632,"value":"Desenvolvedor"},{"id":6699,"value":"Designer"},{"id":1707,"value":"Designer Gr\u00e1fico"},{"id":1933,"value":"Diagramador"},{"id":361,"value":"Digitador"},{"id":171,"value":"Diplomata"},{"id":6780,"value":"Diplomata - Bolsa-pr\u00eamio de voca\u00e7\u00e3o para a Diplomacia"},{"id":7615,"value":"Direito"},{"id":7156,"value":"Diretor Adjunto de Unidade Escolar"},{"id":4479,"value":"Diretor de Imagem"},{"id":2646,"value":"Diretor de Programa"},{"id":7155,"value":"Diretor de Unidade Escolar"},{"id":2271,"value":"Distribuidor"},{"id":8605,"value":"Documenta\u00e7\u00e3o"},{"id":2633,"value":"Documentador"},{"id":25,"value":"Economista"},{"id":1891,"value":"Economista J\u00fanior"},{"id":4410,"value":"Educador Infantil"},{"id":3406,"value":"Educador Social"},{"id":991,"value":"Eletricista"},{"id":3540,"value":"Eletricista de Manuten\u00e7\u00e3o"},{"id":1768,"value":"Eletrot\u00e9cnico"},{"id":7322,"value":"Enfermagem"},{"id":587,"value":"Enfermeiro"},{"id":3324,"value":"Enfermeiro - Enfermagem Cir\u00fargica"},{"id":8157,"value":"Enfermeiro - Geral"},{"id":8088,"value":"Enfermeiro - Hemodin\u00e2mica"},{"id":3289,"value":"Enfermeiro - Nefrologia"},{"id":4279,"value":"Enfermeiro - Oncologia"},{"id":4138,"value":"Enfermeiro - Sa\u00fade da Fam\u00edlia"},{"id":3293,"value":"Enfermeiro - Terapia Intensiva"},{"id":3265,"value":"Enfermeiro - Urg\u00eancia e Emerg\u00eancia - SAMU"},{"id":1098,"value":"Enfermeiro do Trabalho"},{"id":921,"value":"Enfermeiro do Trabalho J\u00fanior"},{"id":7748,"value":"Enfermeiro Fiscal"},{"id":8279,"value":"Engenharia Ambiental"},{"id":8278,"value":"Engenharia de Agrimensura"},{"id":8447,"value":"Engenharia de S\u00e1ude P\u00fablica"},{"id":312,"value":"Engenharia Eletr\u00f4nico"},{"id":8298,"value":"Engenharia Florestal"},{"id":8302,"value":"Engenharia Mecanica"},{"id":8286,"value":"Engenharia Qu\u00edmica"},{"id":640,"value":"Engenheiro"},{"id":631,"value":"Engenheiro - Analista Judici\u00e1rio"},{"id":2947,"value":"Engenheiro - Telecomunica\u00e7\u00f5es"},{"id":3164,"value":"Engenheiro A - Analista de Meio Ambiente"},{"id":2763,"value":"Engenheiro agrimensor"},{"id":993,"value":"Engenheiro agr\u00f4nomo"},{"id":1027,"value":"Engenheiro Ambiental"},{"id":7618,"value":"Engenheiro ambiental"},{"id":994,"value":"Engenheiro cart\u00f3grafo"},{"id":29,"value":"Engenheiro civil"},{"id":2211,"value":"Engenheiro da Computa\u00e7\u00e3o"},{"id":6786,"value":"Engenheiro de controle de qualidade"},{"id":7917,"value":"Engenheiro de controle e automa\u00e7\u00e3o"},{"id":8835,"value":"Engenheiro de Equipamento J\u00fanior - El\u00e9trica"},{"id":5096,"value":"Engenheiro de Equipamento J\u00fanior - Eletr\u00f4nica"},{"id":6782,"value":"Engenheiro de Equipamento J\u00fanior - Inspe\u00e7\u00e3o"},{"id":1427,"value":"Engenheiro de Equipamento J\u00fanior - Mec\u00e2nica"},{"id":8506,"value":"Engenheiro de G\u00e1s Natural J\u00fanior"},{"id":6858,"value":"Engenheiro de Infraestrutura Aeron\u00e1utica"},{"id":922,"value":"Engenheiro de Petr\u00f3leo J\u00fanior"},{"id":923,"value":"Engenheiro de Processamento J\u00fanior"},{"id":875,"value":"Engenheiro de produ\u00e7\u00e3o"},{"id":5375,"value":"Engenheiro de Produ\u00e7\u00e3o J\u00fanior"},{"id":6136,"value":"Engenheiro de redes de comunica\u00e7\u00e3o"},{"id":311,"value":"Engenheiro de Seguran\u00e7a"},{"id":580,"value":"Engenheiro de seguran\u00e7a do trabalho"},{"id":4387,"value":"Engenheiro de Seguran\u00e7a J\u00fanior"},{"id":2264,"value":"Engenheiro de Telecomunica\u00e7\u00f5es"},{"id":4416,"value":"Engenheiro do Trabalho"},{"id":1100,"value":"Engenheiro eletricista"},{"id":337,"value":"Engenheiro eletr\u00f4nico"},{"id":1028,"value":"Engenheiro florestal"},{"id":7522,"value":"Engenheiro J\u00fanior"},{"id":4069,"value":"Engenheiro J\u00fanior - Civil"},{"id":4071,"value":"Engenheiro J\u00fanior - Mec\u00e2nica"},{"id":4073,"value":"Engenheiro J\u00fanior - Seguran\u00e7a"},{"id":313,"value":"Engenheiro mec\u00e2nico"},{"id":1029,"value":"Engenheiro mecatr\u00f4nico"},{"id":1915,"value":"Engenheiro naval"},{"id":876,"value":"Engenheiro Qu\u00edmico"},{"id":1030,"value":"Engenheiro Sanitarista"},{"id":6644,"value":"Escrevente T\u00e9cnico Judici\u00e1rio"},{"id":127,"value":"Escritur\u00e1rio"},{"id":410,"value":"Escriv\u00e3o da Pol\u00edcia Federal"},{"id":2920,"value":"Escriv\u00e3o de Pol\u00edcia"},{"id":1035,"value":"Escriv\u00e3o de Pol\u00edcia Civil"},{"id":7479,"value":"Especialista"},{"id":838,"value":"Especialista - Administrativo e Financeiro"},{"id":2568,"value":"Especialista - Contabilidade"},{"id":8907,"value":"Especialista em Controladoria P\u00fablica"},{"id":8810,"value":"Especialista em Educa\u00e7\u00e3o - Supervis\u00e3o Pedag\u00f3gica"},{"id":7130,"value":"Especialista em Financiamento e Execu\u00e7\u00e3o de Programas e Projetos Educacionais"},{"id":7167,"value":"Especialista em Geologia e Geof\u00edsica"},{"id":7254,"value":"Especialista em Gest\u00e3o de Telecomunica\u00e7\u00f5es - Administrativo"},{"id":7261,"value":"Especialista em Gest\u00e3o de Telecomunica\u00e7\u00f5es - Analista de TI"},{"id":7255,"value":"Especialista em Gest\u00e3o de Telecomunica\u00e7\u00f5es - Auditoria"},{"id":7256,"value":"Especialista em Gest\u00e3o de Telecomunica\u00e7\u00f5es - Comercial"},{"id":7264,"value":"Especialista em Gest\u00e3o de Telecomunica\u00e7\u00f5es - Engenheiro de Telecomunica\u00e7\u00f5es"},{"id":7257,"value":"Especialista em Gest\u00e3o de Telecomunica\u00e7\u00f5es - Estat\u00edstica"},{"id":7258,"value":"Especialista em Gest\u00e3o de Telecomunica\u00e7\u00f5es - Finan\u00e7as"},{"id":7260,"value":"Especialista em Gest\u00e3o de Telecomunica\u00e7\u00f5es - Psicologia"},{"id":450,"value":"Especialista em Pol\u00edticas P\u00fablicas e Gest\u00e3o Governamental"},{"id":8454,"value":"Especialista em Previd\u00eancia Social"},{"id":8662,"value":"Especialista em Previd\u00eancia Social - Gest\u00e3o de Tecnologia da Informa\u00e7\u00e3o"},{"id":3485,"value":"Especialista em Produ\u00e7\u00e3o de Hemoderivados - An\u00e1lises Cl\u00ednicas"},{"id":3487,"value":"Especialista em Produ\u00e7\u00e3o de Hemoderivados - Engenheiro Eletr\u00f4nico"},{"id":3489,"value":"Especialista em Produ\u00e7\u00e3o de Hemoderivados - Engenheiro Mec\u00e2nico"},{"id":3490,"value":"Especialista em Produ\u00e7\u00e3o de Hemoderivados - Engenheiro Qu\u00edmico"},{"id":7413,"value":"Especialista em Psicologia"},{"id":38,"value":"Especialista em Regula\u00e7\u00e3o"},{"id":296,"value":"Especialista em Regula\u00e7\u00e3o - Comunica\u00e7\u00e3o Social"},{"id":40,"value":"Especialista em Regula\u00e7\u00e3o - Direito"},{"id":285,"value":"Especialista em Regula\u00e7\u00e3o - Economia"},{"id":287,"value":"Especialista em Regula\u00e7\u00e3o - Engenharia"},{"id":349,"value":"Especialista em Regula\u00e7\u00e3o - Engenharia Ambiental ou Biologia"},{"id":350,"value":"Especialista em Regula\u00e7\u00e3o - Engenharia Naval ou Engenharia Mec\u00e2nica"},{"id":347,"value":"Especialista em Regula\u00e7\u00e3o - Estat\u00edstica"},{"id":39,"value":"Especialista em Regula\u00e7\u00e3o - Inform\u00e1tica"},{"id":3447,"value":"Especialista em Regula\u00e7\u00e3o - Qualquer \u00c1rea de Forma\u00e7\u00e3o"},{"id":7600,"value":"Especialista em Regula\u00e7\u00e3o Atividade Cinematogr\u00e1fica e Audiovisual - \u00c1rea 1"},{"id":7601,"value":"Especialista em Regula\u00e7\u00e3o Atividade Cinematogr\u00e1fica e Audiovisual - \u00c1rea 2"},{"id":7602,"value":"Especialista em Regula\u00e7\u00e3o Atividade Cinematogr\u00e1fica e Audiovisual - \u00c1rea 3"},{"id":7152,"value":"Especialista em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil"},{"id":7141,"value":"Especialista em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil - \u00c1rea 1"},{"id":7142,"value":"Especialista em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil - \u00c1rea 2"},{"id":7143,"value":"Especialista em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil - \u00c1rea 3"},{"id":7144,"value":"Especialista em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil - \u00c1rea 4"},{"id":7145,"value":"Especialista em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil - \u00c1rea 5"},{"id":7146,"value":"Especialista em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil - \u00c1rea 6"},{"id":7147,"value":"Especialista em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil - \u00c1rea 7"},{"id":7466,"value":"Especialista em Regula\u00e7\u00e3o de Sa\u00fade Suplementar"},{"id":7361,"value":"Especialista em Regula\u00e7\u00e3o de Servi\u00e7os de Transportes Terrestres"},{"id":7044,"value":"Especialista em Sa\u00fade - Assistente Social"},{"id":5796,"value":"Especialista em Sa\u00fade - Medicina Veterin\u00e1ria"},{"id":8616,"value":"Especialista Portu\u00e1rio - Contratos, Compras e Licita\u00e7\u00f5es"},{"id":7734,"value":"Est\u00e1gio - Direito"},{"id":7587,"value":"Estat\u00edstica"},{"id":401,"value":"Estat\u00edstico"},{"id":6949,"value":"Exame de Ordem"},{"id":6859,"value":"Exame de Ordem Unificado"},{"id":6919,"value":"Exame Nacional do Ensino M\u00e9dio"},{"id":835,"value":"Executivo P\u00fablico"},{"id":889,"value":"Farmac\u00eautico"},{"id":8897,"value":"Farmac\u00eautico (auditoria e regula\u00e7\u00e3o)"},{"id":1102,"value":"Farmac\u00eautico analista cl\u00ednico (bioqu\u00edmico)"},{"id":5439,"value":"Farmac\u00eautico hospitalar e cl\u00ednico"},{"id":3294,"value":"Farmac\u00eautico industrial"},{"id":6201,"value":"Fiscal"},{"id":697,"value":"Fiscal da Receita Estadual"},{"id":638,"value":"Fiscal de Rendas"},{"id":1499,"value":"Fiscal de Servi\u00e7o P\u00fablico"},{"id":1158,"value":"Fiscal de Tributos"},{"id":3508,"value":"Fiscal do Meio Ambiente"},{"id":325,"value":"Fiscal Estadual Agropecu\u00e1rio - Biologia"},{"id":328,"value":"Fiscal Estadual Agropecu\u00e1rio - Engenharia de Pesca"},{"id":329,"value":"Fiscal Estadual Agropecu\u00e1rio - Medicina Veterin\u00e1ria"},{"id":2513,"value":"Fiscal Municipal"},{"id":1470,"value":"Fiscal Sanit\u00e1rio"},{"id":1103,"value":"F\u00edsico"},{"id":8091,"value":"F\u00edsico - F\u00edsica M\u00e9dica - Radiodiagn\u00f3stico"},{"id":8090,"value":"F\u00edsico - F\u00edsica M\u00e9dica - Radioterapia"},{"id":1104,"value":"Fisioterapeuta"},{"id":4599,"value":"Fisioterapeuta - Terapia Intensiva"},{"id":892,"value":"Fonoaudi\u00f3logo"},{"id":5892,"value":"Fot\u00f3grafo Criminal\u00edstico"},{"id":7239,"value":"Fuzileiro Naval"},{"id":1188,"value":"Gari"},{"id":5382,"value":"Geof\u00edsico J\u00fanior"},{"id":924,"value":"Geof\u00edsico J\u00fanior - F\u00edsica"},{"id":925,"value":"Geof\u00edsico J\u00fanior - Geologia"},{"id":877,"value":"Ge\u00f3grafo"},{"id":1709,"value":"Ge\u00f3logo"},{"id":5093,"value":"Gerente de Projetos"},{"id":2634,"value":"Gerente de Seguran\u00e7a"},{"id":2635,"value":"Gerente de Suporte"},{"id":5370,"value":"Gerente de Telecomunica\u00e7\u00f5es"},{"id":8293,"value":"Gest\u00e3o P\u00fablica"},{"id":7473,"value":"Gestor"},{"id":6918,"value":"Gestor de Atividade Jornal\u00edstica"},{"id":8715,"value":"Gestor de Transportes e Obras - Administra\u00e7\u00e3o"},{"id":8716,"value":"Gestor de Transportes e Obras - Direito"},{"id":8504,"value":"Gestor Governamental"},{"id":1631,"value":"Gestor P\u00fablico"},{"id":8064,"value":"Guarda Civil Metropolitano"},{"id":840,"value":"Guarda Municipal"},{"id":1916,"value":"Guarda Portu\u00e1rio"},{"id":3610,"value":"Histori\u00f3grafo"},{"id":1297,"value":"INCA 2010 - Conhecimentos B\u00e1sicos \u2013 PARTE I (nivel m\u00e9dio)"},{"id":1298,"value":"INCA 2010 - Conhecimentos B\u00e1sicos \u2013 PARTE I (nivel superior)"},{"id":6766,"value":"Inspetor"},{"id":6974,"value":"Inspetor de Pol\u00edcia"},{"id":2124,"value":"Inspetor de Seguran\u00e7a"},{"id":4506,"value":"Instrumentador Cir\u00fargico"},{"id":6168,"value":"Instrutor de Inform\u00e1tica"},{"id":1682,"value":"Investigador"},{"id":1683,"value":"Investigador de Pol\u00edcia"},{"id":878,"value":"Jornalista"},{"id":426,"value":"Juiz de direito"},{"id":6996,"value":"Juiz do trabalho"},{"id":6997,"value":"Juiz federal"},{"id":8865,"value":"Julgador Administrativo Tribut\u00e1rio do Tesouro Estadual"},{"id":5429,"value":"Leiturista"},{"id":179,"value":"M\u00e9dico"},{"id":3533,"value":"M\u00e9dico - Angiorradiologia e Cirurgia Endovascular"},{"id":4405,"value":"M\u00e9dico - Cancerologia Cir\u00fargica"},{"id":3271,"value":"M\u00e9dico - Cirurgia Card\u00edaca"},{"id":2196,"value":"M\u00e9dico - Cirurgia Oncoginecol\u00f3gica"},{"id":1112,"value":"M\u00e9dico - Cl\u00ednica M\u00e9dica"},{"id":3388,"value":"M\u00e9dico - Diagn\u00f3stico por Imagem"},{"id":1460,"value":"M\u00e9dico - Ecocardiografia"},{"id":3239,"value":"M\u00e9dico - Endoscopia Digestiva"},{"id":3240,"value":"M\u00e9dico - Endoscopia Respirat\u00f3ria"},{"id":3241,"value":"M\u00e9dico - Ergometria"},{"id":1320,"value":"M\u00e9dico - Gastroenterologia"},{"id":4413,"value":"M\u00e9dico - Hemodin\u00e2mica e Cardiologia Intervencionista"},{"id":3244,"value":"M\u00e9dico - Hemoterapia"},{"id":1328,"value":"M\u00e9dico - Internista (Clinica M\u00e9dica)"},{"id":2201,"value":"M\u00e9dico - Mamografia"},{"id":1332,"value":"M\u00e9dico - Neonatologia"},{"id":1333,"value":"M\u00e9dico - Neurocirurgia"},{"id":3060,"value":"M\u00e9dico - Nutrologia"},{"id":1339,"value":"M\u00e9dico - Patologia Cl\u00ednica - Medicina Laboratorial"},{"id":1125,"value":"M\u00e9dico - Patologista"},{"id":1704,"value":"M\u00e9dico - Pneumotisiologia"},{"id":2203,"value":"M\u00e9dico - Radiologia Intervencionista e Angiorradiologia"},{"id":1344,"value":"M\u00e9dico - Radioterapia"},{"id":2998,"value":"M\u00e9dico - Urgentista"},{"id":1484,"value":"M\u00e9dico - Veterin\u00e1ria"},{"id":3234,"value":"M\u00e9dico alergista e imunologista"},{"id":1309,"value":"M\u00e9dico anestesista"},{"id":2244,"value":"M\u00e9dico cardiologista"},{"id":1312,"value":"M\u00e9dico cirurgi\u00e3o geral"},{"id":1314,"value":"M\u00e9dico cirurgi\u00e3o pl\u00e1stico"},{"id":7305,"value":"M\u00e9dico cl\u00ednico geral"},{"id":203,"value":"M\u00e9dico dermatologista"},{"id":208,"value":"M\u00e9dico do trabalho"},{"id":8837,"value":"M\u00e9dico do Trabalho J\u00fanior"},{"id":204,"value":"M\u00e9dico endocrinologista"},{"id":206,"value":"M\u00e9dico geriatra"},{"id":2363,"value":"M\u00e9dico ginecologista"},{"id":1115,"value":"M\u00e9dico ginecologista e obstetra"},{"id":4741,"value":"M\u00e9dico hematologista"},{"id":1116,"value":"M\u00e9dico infectologista"},{"id":1117,"value":"M\u00e9dico intensivista"},{"id":2921,"value":"M\u00e9dico legista"},{"id":1331,"value":"M\u00e9dico nefrologista"},{"id":209,"value":"M\u00e9dico neurologista"},{"id":210,"value":"M\u00e9dico oftalmologista"},{"id":1764,"value":"M\u00e9dico oncologista"},{"id":211,"value":"M\u00e9dico ortopedista e traumatologista"},{"id":1340,"value":"M\u00e9dico pediatra"},{"id":3329,"value":"M\u00e9dico Perito"},{"id":1457,"value":"M\u00e9dico pneumologista"},{"id":192,"value":"M\u00e9dico psiquiatra"},{"id":1126,"value":"M\u00e9dico radiologista"},{"id":1345,"value":"M\u00e9dico reumatologista"},{"id":1127,"value":"M\u00e9dico ultrassonografista"},{"id":1346,"value":"M\u00e9dico urologista"},{"id":7408,"value":"M\u00e9dico veterin\u00e1rio"},{"id":2967,"value":"Merendeira"},{"id":8657,"value":"Mestre de Cerim\u00f4nias"},{"id":3577,"value":"Mestre de Obras"},{"id":1031,"value":"Meteorologista"},{"id":4267,"value":"Monitor"},{"id":1485,"value":"Monitor de Creche"},{"id":367,"value":"Motorista"},{"id":7193,"value":"Motorista Auxiliar"},{"id":5267,"value":"Motorista de Ambul\u00e2ncia"},{"id":1700,"value":"Motorista de Caminh\u00e3o"},{"id":6403,"value":"Motorista III"},{"id":1507,"value":"Muse\u00f3logo"},{"id":7321,"value":"M\u00fasico - Soldado"},{"id":7119,"value":"N\u00edvel M\u00e9dio"},{"id":7060,"value":"N\u00edvel Superior"},{"id":890,"value":"Nutricionista"},{"id":7515,"value":"Nutricionista J\u00fanior"},{"id":7324,"value":"Odontologia"},{"id":1128,"value":"Odont\u00f3logo"},{"id":2164,"value":"Odont\u00f3logo - Cirurgi\u00e3o Buco Maxilo"},{"id":2165,"value":"Odont\u00f3logo - Cirurgi\u00e3o Dentista Cl\u00ednico"},{"id":8164,"value":"Odont\u00f3logo - Dent\u00edstica"},{"id":8165,"value":"Odont\u00f3logo - Estomatologia"},{"id":8166,"value":"Odont\u00f3logo - Implantodontia"},{"id":2169,"value":"Odont\u00f3logo - Odontopediatria"},{"id":8167,"value":"Odont\u00f3logo - Ortodontia"},{"id":2170,"value":"Odont\u00f3logo - Periodontia"},{"id":2173,"value":"Odont\u00f3logo - Protesista"},{"id":4414,"value":"Odont\u00f3logo - Radiologia"},{"id":2805,"value":"Odont\u00f3logo Especialista - Pr\u00f3tese"},{"id":1129,"value":"Odontopediatra"},{"id":6867,"value":"Oficial - Contabilidade"},{"id":1187,"value":"Oficial Administrativo"},{"id":7914,"value":"Oficial Bombeiro Militar"},{"id":6645,"value":"Oficial Bombeiro Militar Combatente"},{"id":6950,"value":"Oficial Bombeiro Militar Complementar"},{"id":6770,"value":"Oficial Combatente da Pol\u00edcia Militar"},{"id":8905,"value":"Oficial da Marinha Mercante"},{"id":6962,"value":"Oficial da Pol\u00edcia Militar"},{"id":8882,"value":"Oficial de Apoio"},{"id":1079,"value":"Oficial de Apoio Judicial"},{"id":425,"value":"Oficial de Cart\u00f3rio"},{"id":718,"value":"Oficial de Chancelaria"},{"id":7959,"value":"Oficial de Controle Externo"},{"id":174,"value":"Oficial de Defensoria P\u00fablica"},{"id":1711,"value":"Oficial de Dilig\u00eancia"},{"id":6473,"value":"Oficial de Fazenda"},{"id":322,"value":"Oficial de Intelig\u00eancia"},{"id":281,"value":"Oficial de Justi\u00e7a"},{"id":5438,"value":"Oficial de Manuten\u00e7\u00e3o"},{"id":7520,"value":"Oficial de Produ\u00e7\u00e3o"},{"id":8117,"value":"Oficial de Sa\u00fade"},{"id":2813,"value":"Oficial de Sa\u00fade - Cirurgia Pedi\u00e1trica"},{"id":2815,"value":"Oficial de Sa\u00fade - Cirurgia Tor\u00e1cica"},{"id":2816,"value":"Oficial de Sa\u00fade - Cirurgia Vascular"},{"id":2817,"value":"Oficial de Sa\u00fade - Cl\u00ednica M\u00e9dica de Emerg\u00eancia"},{"id":2820,"value":"Oficial de Sa\u00fade - Endodontia"},{"id":2821,"value":"Oficial de Sa\u00fade - Endoscopia Digestiva"},{"id":2822,"value":"Oficial de Sa\u00fade - Enfermagem Geral"},{"id":2823,"value":"Oficial de Sa\u00fade - Farm\u00e1cia Bioqu\u00edmica"},{"id":2824,"value":"Oficial de Sa\u00fade - Farm\u00e1cia Hospitalar"},{"id":2825,"value":"Oficial de Sa\u00fade - Farm\u00e1cia Industrial"},{"id":2826,"value":"Oficial de Sa\u00fade - Fisiatria"},{"id":2827,"value":"Oficial de Sa\u00fade - Fisioterapia Cardiopulmonar"},{"id":2828,"value":"Oficial de Sa\u00fade - Fisioterapia Neurol\u00f3gica"},{"id":2829,"value":"Oficial de Sa\u00fade - Fisioterapia Pedi\u00e1trica Neonatal"},{"id":2830,"value":"Oficial de Sa\u00fade - Fisioterapia Traumato-ortop\u00e9dica"},{"id":2831,"value":"Oficial de Sa\u00fade - Fonoaudiologia"},{"id":2832,"value":"Oficial de Sa\u00fade - Gastroenterologia"},{"id":2835,"value":"Oficial de Sa\u00fade - Hemodin\u00e2mica"},{"id":2836,"value":"Oficial de Sa\u00fade - Hemoterapia"},{"id":2838,"value":"Oficial de Sa\u00fade - Mastologia"},{"id":2840,"value":"Oficial de Sa\u00fade - Neurocirurgia"},{"id":2843,"value":"Oficial de Sa\u00fade - Nutri\u00e7\u00e3o"},{"id":2845,"value":"Oficial de Sa\u00fade - Odonto Cl\u00ednica Geral"},{"id":2848,"value":"Oficial de Sa\u00fade - Ortodontia"},{"id":2851,"value":"Oficial de Sa\u00fade - Patologia Cl\u00ednica"},{"id":2852,"value":"Oficial de Sa\u00fade - Pediatria"},{"id":2854,"value":"Oficial de Sa\u00fade - Pr\u00f3tese Dental"},{"id":2855,"value":"Oficial de Sa\u00fade - Psicologia"},{"id":2859,"value":"Oficial de Sa\u00fade - Servi\u00e7o Social"},{"id":2863,"value":"Oficial de Sa\u00fade - Veterin\u00e1ria Caninos"},{"id":2864,"value":"Oficial de Sa\u00fade - Veterin\u00e1ria Equ\u00ednos"},{"id":7186,"value":"Oficial de Transportes"},{"id":7163,"value":"Oficial Engenheiro"},{"id":2749,"value":"Oficial Engenheiro - Engenharia da Computa\u00e7\u00e3o"},{"id":2742,"value":"Oficial Engenheiro - Engenharia de Telecomunica\u00e7\u00f5es"},{"id":2744,"value":"Oficial Engenheiro - Engenharia Eletr\u00f4nica"},{"id":2745,"value":"Oficial Engenheiro - Engenharia Mec\u00e2nica"},{"id":2747,"value":"Oficial Engenheiro - Engenharia Qu\u00edmica"},{"id":6688,"value":"Oficial Escrevente"},{"id":689,"value":"Oficial Judici\u00e1rio"},{"id":7902,"value":"Oficial Judici\u00e1rio - Oficial de Justi\u00e7a"},{"id":8656,"value":"Oficial Legislativo"},{"id":2865,"value":"Oficial Pedagogo - Pedagogia"},{"id":4533,"value":"Oficial T\u00e9cnico de Intelig\u00eancia - \u00c1rea de Administra\u00e7\u00e3o"},{"id":4540,"value":"Oficial T\u00e9cnico de Intelig\u00eancia - \u00c1rea de Criptoan\u00e1lise \u2013 Estat\u00edstica"},{"id":4541,"value":"Oficial T\u00e9cnico de Intelig\u00eancia - \u00c1rea de Desenvolvimento e Manuten\u00e7\u00e3o de Sistemas"},{"id":4542,"value":"Oficial T\u00e9cnico de Intelig\u00eancia - \u00c1rea de Direito"},{"id":4536,"value":"Oficial T\u00e9cnico de Intelig\u00eancia \u2013 \u00c1rea de Arquivologia"},{"id":4538,"value":"Oficial T\u00e9cnico de Intelig\u00eancia \u2013 \u00c1rea de Comunica\u00e7\u00e3o Social \u2013 Jornalismo"},{"id":4539,"value":"Oficial T\u00e9cnico de Intelig\u00eancia \u2013 \u00c1rea de Comunica\u00e7\u00e3o Social \u2013 Publicidade e Propaganda"},{"id":4543,"value":"Oficial T\u00e9cnico de Intelig\u00eancia \u2013 \u00c1rea de Educa\u00e7\u00e3o F\u00edsica"},{"id":4546,"value":"Oficial T\u00e9cnico de Intelig\u00eancia \u2013 \u00c1rea de Pedagogia"},{"id":4547,"value":"Oficial T\u00e9cnico de Intelig\u00eancia \u2013 \u00c1rea de Psicologia"},{"id":4548,"value":"Oficial T\u00e9cnico de Intelig\u00eancia \u2013 \u00c1rea de Servi\u00e7o Social"},{"id":7162,"value":"Oficial Tempor\u00e1rio"},{"id":2580,"value":"Oficial Tempor\u00e1rio - Administra\u00e7\u00e3o"},{"id":2581,"value":"Oficial Tempor\u00e1rio - An\u00e1lise de Sistemas"},{"id":2585,"value":"Oficial Tempor\u00e1rio - Economia"},{"id":2595,"value":"Oficial Tempor\u00e1rio - Estat\u00edstica"},{"id":2598,"value":"Oficial Tempor\u00e1rio - Jornalismo"},{"id":2623,"value":"Oficial Tempor\u00e1rio - Psicologia Cl\u00ednica"},{"id":2624,"value":"Oficial Tempor\u00e1rio - Psicologia Educacional"},{"id":2625,"value":"Oficial Tempor\u00e1rio - Psicologia Organizacional do Trabalho"},{"id":2604,"value":"Oficial Tempor\u00e1rio - Servi\u00e7o Social"},{"id":2605,"value":"Oficial Tempor\u00e1rio - Servi\u00e7os Jur\u00eddicos"},{"id":8602,"value":"Operacional Administrativo"},{"id":4422,"value":"Operador"},{"id":700,"value":"Operador de Computador"},{"id":6408,"value":"Operador de ETE IV"},{"id":1474,"value":"Operador de M\u00e1quina Pesada"},{"id":1572,"value":"Operador de M\u00e1quinas"},{"id":4489,"value":"Operador de R\u00e1dio"},{"id":8676,"value":"Operador Metroferrovi\u00e1rio Junior"},{"id":5435,"value":"Or\u00e7amentista"},{"id":8290,"value":"Papiloscopista"},{"id":170,"value":"Papiloscopista da Pol\u00edcia Federal"},{"id":885,"value":"Papiloscopista e T\u00e9cnico em Per\u00edcia"},{"id":5888,"value":"Papiloscopista Policial"},{"id":7325,"value":"Pedagogia"},{"id":1025,"value":"Pedagogo"},{"id":5889,"value":"Perito Criminal"},{"id":8289,"value":"Perito criminal"},{"id":5318,"value":"Perito Criminal - An\u00e1lise de Sistemas"},{"id":2923,"value":"Perito Criminal - Biologia"},{"id":372,"value":"Perito Criminal - Biol\u00f3gicas"},{"id":5319,"value":"Perito Criminal - Biomedicina"},{"id":2924,"value":"Perito Criminal - Ci\u00eancias Cont\u00e1beis"},{"id":7919,"value":"Perito Criminal - Ci\u00eancias da Computa\u00e7\u00e3o"},{"id":7857,"value":"Perito Criminal - Contabilidade"},{"id":6105,"value":"Perito Criminal - Engenharia Ambiental"},{"id":5321,"value":"Perito Criminal - Engenharia da Computa\u00e7\u00e3o"},{"id":377,"value":"Perito Criminal - Engenharia Mec\u00e2nica"},{"id":2928,"value":"Perito Criminal - Farmac\u00eautico"},{"id":382,"value":"Perito Criminal - F\u00edsica"},{"id":384,"value":"Perito Criminal - Geologia"},{"id":6103,"value":"Perito Criminal - Inform\u00e1tica"},{"id":5328,"value":"Perito Criminal - Odontologia"},{"id":6111,"value":"Perito Criminal - Qu\u00edmica"},{"id":388,"value":"Perito Criminal - Veterin\u00e1ria"},{"id":6749,"value":"Perito Criminal Especial"},{"id":7512,"value":"Perito Criminal Federal"},{"id":33,"value":"Perito Criminal Federal - Inform\u00e1tica"},{"id":6750,"value":"Perito em Telecomunica\u00e7\u00e3o"},{"id":7004,"value":"Perito Legista"},{"id":3647,"value":"Perito M\u00e9dico"},{"id":7302,"value":"Perito M\u00e9dico Legal"},{"id":6976,"value":"Perito M\u00e9dico Previdenci\u00e1rio"},{"id":6751,"value":"Perito Papilosc\u00f3pico"},{"id":7300,"value":"Perito Papiloscopista"},{"id":8780,"value":"Perito Qu\u00edmico Forense - Qu\u00edmica"},{"id":7477,"value":"Pesquisador"},{"id":4891,"value":"Pesquisador - Arquitetura"},{"id":4302,"value":"Pesquisador - Ci\u00eancia da Computa\u00e7\u00e3o"},{"id":7235,"value":"Pesquisador - Eletricidade"},{"id":7236,"value":"Pesquisador - Mec\u00e2nica"},{"id":6728,"value":"Pesquisador - Metrologia Legal"},{"id":6731,"value":"Pesquisador - Psicologia"},{"id":7237,"value":"Pesquisador - Telecomunica\u00e7\u00f5es, Computa\u00e7\u00e3o e Eletr\u00f4nica"},{"id":7475,"value":"Pesquisador em Geoci\u00eancias"},{"id":6711,"value":"Pesquisador Tecnologista - Acredita\u00e7\u00e3o"},{"id":742,"value":"Pesquisador Tecnologista em Metrologia e Qualidade - Ci\u00eancias Econ\u00f4micas"},{"id":744,"value":"Pesquisador Tecnologista em Metrologia e Qualidade - Engenharia de Produ\u00e7\u00e3o"},{"id":8856,"value":"Pesquisador Tecnologista em Metrologia e Qualidade - Engenharia Qu\u00edmica"},{"id":749,"value":"Pesquisador Tecnologista em Metrologia e Qualidade - Inform\u00e1tica Aplicada \u00e0 Metrologia Legal"},{"id":755,"value":"Pesquisador Tecnologista em Metrologia e Qualidade - Tecnologia e Inova\u00e7\u00e3o"},{"id":898,"value":"Policial Legislativo Federal"},{"id":8718,"value":"Policial Militar"},{"id":586,"value":"Policial Rodovi\u00e1rio Federal - M\u00e9dio"},{"id":8891,"value":"Policial Rodovi\u00e1rio Federal - Superior"},{"id":7384,"value":"Primeiro Tenente - Administra\u00e7\u00e3o"},{"id":7400,"value":"Primeiro Tenente - Biblioteconomia"},{"id":8817,"value":"Primeiro Tenente - Cirurgi\u00f5es-Dentistas"},{"id":7389,"value":"Primeiro Tenente - Direito"},{"id":8818,"value":"Primeiro Tenente - Enfermagem"},{"id":7391,"value":"Primeiro Tenente - Estat\u00edstica"},{"id":8820,"value":"Primeiro Tenente - Fisioterapia"},{"id":8821,"value":"Primeiro Tenente - Fonoaudiologia"},{"id":7393,"value":"Primeiro Tenente - Inform\u00e1tica"},{"id":8823,"value":"Primeiro Tenente - M\u00e9dico"},{"id":8822,"value":"Primeiro Tenente - Nutri\u00e7\u00e3o"},{"id":7397,"value":"Primeiro Tenente - Psicologia"},{"id":6927,"value":"Primeiro tenente de pol\u00edcia militar"},{"id":321,"value":"Procurador"},{"id":7590,"value":"Procurador Aut\u00e1rquico"},{"id":8831,"value":"Procurador da Fazenda Nacional"},{"id":8830,"value":"Procurador da Rep\u00fablica"},{"id":8834,"value":"Procurador do Estado"},{"id":8848,"value":"Procurador do trabalho"},{"id":769,"value":"Procurador Especial de Contas"},{"id":8832,"value":"Procurador Federal"},{"id":3581,"value":"Procurador Jur\u00eddico"},{"id":8455,"value":"Procurador Legislativo"},{"id":1770,"value":"Procurador Municipal"},{"id":1508,"value":"Produtor Cultural"},{"id":912,"value":"Produtor de Desenvolvimento de Conte\u00fados Jornal\u00edsticos para Internet"},{"id":1168,"value":"Professor"},{"id":1422,"value":"Professor - 1 ao 5 Ano Ensino Fundamental"},{"id":1513,"value":"Professor - Administra\u00e7\u00e3o"},{"id":2709,"value":"Professor - Agronomia"},{"id":2367,"value":"Professor - Arquitetura"},{"id":1012,"value":"Professor - Artes"},{"id":2334,"value":"Professor - Artes C\u00eanicas"},{"id":2335,"value":"Professor - Artes Pl\u00e1sticas"},{"id":4940,"value":"Professor - Artes Visuais"},{"id":4128,"value":"Professor - Atividades"},{"id":2319,"value":"Professor - Automa\u00e7\u00e3o"},{"id":317,"value":"Professor - Biologia"},{"id":2320,"value":"Professor - Bioqu\u00edmica"},{"id":1020,"value":"Professor - Ci\u00eancias"},{"id":6499,"value":"Professor - Ci\u00eancias Naturais"},{"id":2285,"value":"Professor - Ci\u00eancias Sociais"},{"id":2286,"value":"Professor - Contabilidade"},{"id":2287,"value":"Professor - Controle Automa\u00e7\u00e3o Instrumenta\u00e7\u00e3o Industrial"},{"id":2336,"value":"Professor - Dan\u00e7a"},{"id":1515,"value":"Professor - Direito"},{"id":3311,"value":"Professor - Educa\u00e7\u00e3o Especial"},{"id":1013,"value":"Professor - Educa\u00e7\u00e3o F\u00edsica"},{"id":1010,"value":"Professor - Educa\u00e7\u00e3o Infantil"},{"id":2289,"value":"Professor - Eletr\u00f4nica"},{"id":1516,"value":"Professor - Eletrot\u00e9cnica"},{"id":1514,"value":"Professor - Enfermagem"},{"id":2290,"value":"Professor - Engenharia Agr\u00edcola"},{"id":2291,"value":"Professor - Engenharia Mec\u00e2nica"},{"id":5368,"value":"Professor - Ensino B\u00e1sico"},{"id":3400,"value":"Professor - Ensino Religioso"},{"id":1626,"value":"Professor - Filosofia"},{"id":319,"value":"Professor - F\u00edsica"},{"id":4577,"value":"Professor - Fisioterapia"},{"id":1014,"value":"Professor - Geografia"},{"id":2323,"value":"Professor - Gest\u00e3o"},{"id":1015,"value":"Professor - Hist\u00f3ria"},{"id":1532,"value":"Professor - Inform\u00e1tica"},{"id":1016,"value":"Professor - Ingl\u00eas"},{"id":1720,"value":"Professor - L\u00edngua Espanhola"},{"id":6483,"value":"Professor - L\u00edngua Francesa"},{"id":7048,"value":"Professor - L\u00edngua Inglesa"},{"id":1017,"value":"Professor - L\u00edngua Portuguesa"},{"id":2712,"value":"Professor - Log\u00edstica"},{"id":318,"value":"Professor - Matem\u00e1tica"},{"id":2307,"value":"Professor - Mec\u00e2nica"},{"id":2010,"value":"Professor - Meio Ambiente"},{"id":2325,"value":"Professor - Moda"},{"id":8121,"value":"Professor - M\u00fasica"},{"id":2011,"value":"Professor - Nutri\u00e7\u00e3o"},{"id":2545,"value":"Professor - Pedagogia"},{"id":8062,"value":"Professor - Portugu\u00eas"},{"id":2339,"value":"Professor - Psicologia"},{"id":320,"value":"Professor - Qu\u00edmica"},{"id":2303,"value":"Professor - Secretariado"},{"id":2013,"value":"Professor - Seguran\u00e7a do Trabalho"},{"id":2515,"value":"Professor - S\u00e9ries Iniciais"},{"id":2014,"value":"Professor - Sociologia"},{"id":5143,"value":"Professor - Teatro"},{"id":2329,"value":"Professor - Tecnologia da Informa\u00e7\u00e3o"},{"id":4292,"value":"Professor - Topografia"},{"id":2015,"value":"Professor - Zootecnia"},{"id":1347,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 Arte"},{"id":1348,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 Biologia"},{"id":1349,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 Educa\u00e7\u00e3o F\u00edsica"},{"id":1350,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 Espanhol"},{"id":1352,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 F\u00edsica"},{"id":1353,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 Geografia"},{"id":1354,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 Hist\u00f3ria"},{"id":1355,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 Ingl\u00eas"},{"id":1356,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 L\u00edngua Portuguesa"},{"id":1357,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 Matem\u00e1tica"},{"id":1358,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 Qu\u00edmica"},{"id":1359,"value":"Professor B \u2014 Ensino Fundamental e M\u00e9dio \u2014 Sociologia"},{"id":8377,"value":"Professor de Educa\u00e7\u00e3o B\u00e1sica"},{"id":1721,"value":"Professor de Educa\u00e7\u00e3o B\u00e1sica dos anos iniciais"},{"id":1181,"value":"Professor de Educa\u00e7\u00e3o F\u00edsica"},{"id":1476,"value":"Professor de Ensino Fundamental"},{"id":5540,"value":"Professor de Ensino Fundamental - S\u00e9ries Iniciais"},{"id":1019,"value":"Professor de M\u00fasica"},{"id":8232,"value":"Professor I"},{"id":2903,"value":"Professor N\u00edvel 3 - Educa\u00e7\u00e3o F\u00edsica"},{"id":1360,"value":"Professor P \u2014 Pedagogo"},{"id":7921,"value":"Professor Pleno I"},{"id":60,"value":"Profissional B\u00e1sico  -  Administra\u00e7\u00e3o"},{"id":58,"value":"Profissional B\u00e1sico  -  An\u00e1lise de Sistemas - Desenvolvimento"},{"id":59,"value":"Profissional B\u00e1sico  -  An\u00e1lise de Sistemas - Suporte"},{"id":57,"value":"Profissional B\u00e1sico  -  Arquitetura"},{"id":55,"value":"Profissional B\u00e1sico  -  Ci\u00eancias Cont\u00e1beis"},{"id":56,"value":"Profissional B\u00e1sico  -  Comunica\u00e7\u00e3o Social"},{"id":54,"value":"Profissional B\u00e1sico  -  Economia"},{"id":4817,"value":"Profissional B\u00e1sico - Arquivologia"},{"id":538,"value":"Profissional B\u00e1sico - Biblioteconomia"},{"id":4818,"value":"Profissional B\u00e1sico - Direito"},{"id":7198,"value":"Profissional B\u00e1sico - Engenharia"},{"id":539,"value":"Profissional B\u00e1sico - Psicologia"},{"id":1895,"value":"Profissional de Comunica\u00e7\u00e3o J\u00fanior"},{"id":5386,"value":"Profissional de Comunica\u00e7\u00e3o J\u00fanior - Jornalismo"},{"id":5387,"value":"Profissional de Comunica\u00e7\u00e3o J\u00fanior - Rela\u00e7\u00f5es P\u00fablicas"},{"id":1132,"value":"Profissional de Educa\u00e7\u00e3o F\u00edsica"},{"id":3112,"value":"Profissional de Enfermagem - T\u00e9cnico em Enfermagem"},{"id":5228,"value":"Profissional de Meio Ambiente J\u00fanior"},{"id":7471,"value":"Profissional de N\u00edvel M\u00e9dio"},{"id":7671,"value":"Profissional de N\u00edvel M\u00e9dio Suporte"},{"id":7668,"value":"Profissional de N\u00edvel M\u00e9dio Suporte - Eletricista"},{"id":2900,"value":"Profissional de N\u00edvel Superior"},{"id":7646,"value":"Profissional de N\u00edvel Superior - Arquivologia"},{"id":7647,"value":"Profissional de N\u00edvel Superior - Ci\u00eancias Cont\u00e1beis"},{"id":7080,"value":"Profissional de Vendas"},{"id":959,"value":"Profissional J\u00fanior - Administra\u00e7\u00e3o"},{"id":7088,"value":"Profissional J\u00fanior - An\u00e1lise de Infraestrutura"},{"id":7089,"value":"Profissional J\u00fanior - An\u00e1lise de Sistemas"},{"id":7084,"value":"Profissional J\u00fanior - Auditoria"},{"id":963,"value":"Profissional J\u00fanior - Ci\u00eancias Cont\u00e1beis"},{"id":7085,"value":"Profissional J\u00fanior - Ci\u00eancias Econ\u00f4micas"},{"id":965,"value":"Profissional J\u00fanior - Comunica\u00e7\u00e3o Social - Rela\u00e7\u00f5es P\u00fablicas"},{"id":7090,"value":"Profissional J\u00fanior - Desenvolvimento de Aplica\u00e7\u00f5es"},{"id":956,"value":"Profissional J\u00fanior - Direito"},{"id":8849,"value":"Profissional J\u00fanior - Engenharia"},{"id":972,"value":"Profissional J\u00fanior - Engenharia Mec\u00e2nica"},{"id":5293,"value":"Profissional J\u00fanior - Servi\u00e7o Social"},{"id":96,"value":"Programador de computador"},{"id":2647,"value":"Programador Visual"},{"id":176,"value":"Promotor de Justi\u00e7a"},{"id":7197,"value":"Promotor de Justi\u00e7a Militar"},{"id":7326,"value":"Psicologia"},{"id":549,"value":"Psic\u00f3logo"},{"id":1133,"value":"Psic\u00f3logo Cl\u00ednico"},{"id":2340,"value":"Psic\u00f3logo Escolar"},{"id":1134,"value":"Psic\u00f3logo Hospitalar"},{"id":1081,"value":"Psic\u00f3logo Judicial"},{"id":7217,"value":"Psic\u00f3logo J\u00fanior"},{"id":1135,"value":"Psic\u00f3logo Organizacional"},{"id":1718,"value":"Psicopedagogo"},{"id":314,"value":"Qu\u00edmico"},{"id":5162,"value":"Qu\u00edmico de Petr\u00f3leo J\u00fanior"},{"id":316,"value":"Recenseador"},{"id":1136,"value":"Recepcionista"},{"id":3343,"value":"Redator"},{"id":1896,"value":"Redator J\u00fanior - Bilingue"},{"id":507,"value":"Regulador de Servi\u00e7os P\u00fablicos"},{"id":1003,"value":"Rela\u00e7\u00f5es P\u00fablicas"},{"id":2055,"value":"Resid\u00eancia"},{"id":1509,"value":"Revisor de Texto"},{"id":7240,"value":"Sargento"},{"id":1868,"value":"Secret\u00e1ria"},{"id":412,"value":"Secret\u00e1rio"},{"id":546,"value":"Secret\u00e1rio de Dilig\u00eancias"},{"id":1771,"value":"Secret\u00e1rio Escolar"},{"id":1171,"value":"Secret\u00e1rio Executivo"},{"id":6926,"value":"Segundo-Tenente"},{"id":8677,"value":"Seguran\u00e7a Metroferrovi\u00e1rio"},{"id":1706,"value":"Servente Escolar"},{"id":7328,"value":"Servi\u00e7o Social"},{"id":8295,"value":"Sistemas de Informa\u00e7\u00e3o"},{"id":879,"value":"Soci\u00f3logo"},{"id":8223,"value":"Socorrista"},{"id":8609,"value":"Soldado 3\u00b0 Classe"},{"id":7252,"value":"Soldado Bombeiro Militar"},{"id":8707,"value":"Soldado Bombeiro Militar - N\u00edvel Superior"},{"id":7158,"value":"Soldado Combatente da Pol\u00edcia Militar"},{"id":893,"value":"Soldado da Pol\u00edcia Militar"},{"id":7999,"value":"Soldado da Pol\u00edcia Militar - Auxiliar de Comunica\u00e7\u00f5es"},{"id":8001,"value":"Soldado da Pol\u00edcia Militar - Auxiliar em Sa\u00fade Bucal"},{"id":8004,"value":"Soldado da Pol\u00edcia Militar - T\u00e9cnico em Patologia Cl\u00ednica"},{"id":8555,"value":"Supervisor de Pesquisas - Administra\u00e7\u00e3o"},{"id":8554,"value":"Supervisor de Pesquisas - Tecnologia de Informa\u00e7\u00e3o e Comunica\u00e7\u00e3o"},{"id":692,"value":"Supervisor Escolar"},{"id":1671,"value":"Taqu\u00edgrafo"},{"id":7223,"value":"T\u00e9cnicas de Complexidade Gerencial"},{"id":7221,"value":"T\u00e9cnicas de Suporte"},{"id":693,"value":"T\u00e9cnico"},{"id":4226,"value":"T\u00e9cnico - Administra\u00e7\u00e3o"},{"id":6733,"value":"T\u00e9cnico - Biotecnologia"},{"id":5901,"value":"T\u00e9cnico - Comercial"},{"id":7195,"value":"T\u00e9cnico - Contabilidade"},{"id":8387,"value":"T\u00e9cnico - Contrarregra"},{"id":7318,"value":"T\u00e9cnico - Edifica\u00e7\u00e3o"},{"id":2531,"value":"T\u00e9cnico - El\u00e9trica"},{"id":6734,"value":"T\u00e9cnico - Metrologia"},{"id":8888,"value":"T\u00e9cnico - Motorista"},{"id":2122,"value":"T\u00e9cnico - Nutri\u00e7\u00e3o"},{"id":6757,"value":"T\u00e9cnico - Opera\u00e7\u00e3o de Computadores"},{"id":867,"value":"T\u00e9cnico - Opera\u00e7\u00e3o de Redes"},{"id":3515,"value":"T\u00e9cnico - Previdenci\u00e1rio"},{"id":2953,"value":"T\u00e9cnico - Qu\u00edmica"},{"id":2050,"value":"T\u00e9cnico - Redes de Computadores"},{"id":2954,"value":"T\u00e9cnico - Rela\u00e7\u00f5es P\u00fablicas"},{"id":8112,"value":"T\u00e9cnico - Sa\u00fade Bucal"},{"id":869,"value":"T\u00e9cnico - Seguran\u00e7a do Trabalho"},{"id":7715,"value":"T\u00e9cnico - Seguran\u00e7a Institucional"},{"id":7314,"value":"T\u00e9cnico - Suporte Administrativo"},{"id":1503,"value":"T\u00e9cnico - Tecnologia da Informa\u00e7\u00e3o"},{"id":8110,"value":"T\u00e9cnico - Tecnologia da Informa\u00e7\u00e3o e Comunica\u00e7\u00e3o"},{"id":1290,"value":"T\u00e9cnico 1 \u2013 An\u00e1lise Cl\u00ednica"},{"id":1291,"value":"T\u00e9cnico 1 \u2013 Anatomia Patol\u00f3gica"},{"id":1292,"value":"T\u00e9cnico 1 \u2013 Citot\u00e9cnico"},{"id":1289,"value":"T\u00e9cnico 1 \u2013 Farm\u00e1cia Hospitalar"},{"id":1293,"value":"T\u00e9cnico 1 \u2013 Hematologia E Hemoterapia"},{"id":1296,"value":"T\u00e9cnico 1 \u2013 Radioterapia"},{"id":8,"value":"T\u00e9cnico Administrativo"},{"id":84,"value":"T\u00e9cnico Administrativo  -  Inform\u00e1tica"},{"id":2125,"value":"T\u00e9cnico Administrativo - Recursos Humanos"},{"id":8704,"value":"T\u00e9cnico Administrativo Portu\u00e1rio"},{"id":998,"value":"T\u00e9cnico Agr\u00edcola"},{"id":1856,"value":"T\u00e9cnico Ambiental"},{"id":8329,"value":"T\u00e9cnico Ambiental - Biologia"},{"id":8330,"value":"T\u00e9cnico Ambiental - Engenharia Agron\u00f4mica"},{"id":926,"value":"T\u00e9cnico Ambiental J\u00fanior"},{"id":7776,"value":"T\u00e9cnico Assistente da Pol\u00edcia Civil - Administrativa"},{"id":3463,"value":"T\u00e9cnico Assistente de Procuradoria"},{"id":128,"value":"T\u00e9cnico Banc\u00e1rio"},{"id":7065,"value":"T\u00e9cnico Cient\u00edfico"},{"id":600,"value":"T\u00e9cnico Cient\u00edfico - Administra\u00e7\u00e3o"},{"id":7052,"value":"T\u00e9cnico Cient\u00edfico - Administra\u00e7\u00e3o de Dados"},{"id":7053,"value":"T\u00e9cnico Cient\u00edfico - An\u00e1lise de Sistemas"},{"id":7061,"value":"T\u00e9cnico Cient\u00edfico - Arquitetura"},{"id":7062,"value":"T\u00e9cnico Cient\u00edfico - Biblioteconomia"},{"id":601,"value":"T\u00e9cnico Cient\u00edfico - Contabilidade"},{"id":5298,"value":"T\u00e9cnico Cient\u00edfico - Direito"},{"id":602,"value":"T\u00e9cnico Cient\u00edfico - Economia"},{"id":603,"value":"T\u00e9cnico Cient\u00edfico - Enfermagem do Trabalho"},{"id":604,"value":"T\u00e9cnico Cient\u00edfico - Engenharia Agron\u00f4mica"},{"id":7063,"value":"T\u00e9cnico Cient\u00edfico - Engenharia Ambiental"},{"id":5300,"value":"T\u00e9cnico Cient\u00edfico - Engenharia de Pesca"},{"id":606,"value":"T\u00e9cnico Cient\u00edfico - Engenharia Florestal"},{"id":607,"value":"T\u00e9cnico Cient\u00edfico - Engenharia Mec\u00e2nica"},{"id":608,"value":"T\u00e9cnico Cient\u00edfico - Estat\u00edstica"},{"id":7055,"value":"T\u00e9cnico Cient\u00edfico - Governan\u00e7a em TI"},{"id":610,"value":"T\u00e9cnico Cient\u00edfico - Medicina Veterin\u00e1ria"},{"id":7056,"value":"T\u00e9cnico Cient\u00edfico - Produ\u00e7\u00e3o e Infraestrutura"},{"id":611,"value":"T\u00e9cnico Cient\u00edfico - Psicologia do Trabalho"},{"id":7058,"value":"T\u00e9cnico Cient\u00edfico - Seguran\u00e7a da Informa\u00e7\u00e3o"},{"id":612,"value":"T\u00e9cnico Cient\u00edfico - Servi\u00e7o Social"},{"id":7059,"value":"T\u00e9cnico Cient\u00edfico - Suporte T\u00e9cnico"},{"id":613,"value":"T\u00e9cnico Cient\u00edfico - Tecnologia da Informa\u00e7\u00e3o"},{"id":304,"value":"T\u00e9cnico de Administra\u00e7\u00e3o"},{"id":927,"value":"T\u00e9cnico de Administra\u00e7\u00e3o e Controle J\u00fanior"},{"id":7478,"value":"T\u00e9cnico de Apoio"},{"id":2236,"value":"T\u00e9cnico de Apoio Especializado"},{"id":4565,"value":"T\u00e9cnico de Apoio Especializado - Controle Interno"},{"id":4566,"value":"T\u00e9cnico de Apoio Especializado - Edifica\u00e7\u00f5es"},{"id":4568,"value":"T\u00e9cnico de Apoio Especializado - Seguran\u00e7a"},{"id":4569,"value":"T\u00e9cnico de Apoio Especializado - Transporte"},{"id":26,"value":"T\u00e9cnico de Arquivo"},{"id":3465,"value":"Tecnico de Atividade Judici\u00e1ria"},{"id":928,"value":"T\u00e9cnico de Comercializa\u00e7\u00e3o Log\u00edstica J\u00fanior"},{"id":403,"value":"T\u00e9cnico de Comunica\u00e7\u00e3o Social"},{"id":305,"value":"T\u00e9cnico de Contabilidade"},{"id":7092,"value":"T\u00e9cnico de Contabilidade J\u00fanior"},{"id":255,"value":"T\u00e9cnico de Controle Externo"},{"id":184,"value":"T\u00e9cnico de Controle Externo - \u00c1rea Administrativa"},{"id":405,"value":"T\u00e9cnico de Controle Externo - Ci\u00eancias Cont\u00e1beis"},{"id":424,"value":"T\u00e9cnico de Controle Externo - Tecnologia da Informa\u00e7\u00e3o"},{"id":83,"value":"T\u00e9cnico de Defesa A\u00e9rea e Controle de Tr\u00e1fego A\u00e9reo  -  An\u00e1lise de Sistemas"},{"id":8023,"value":"T\u00e9cnico de Desenvolvimento e Administra\u00e7\u00e3o - Assessoria de Comunica\u00e7\u00e3o"},{"id":8024,"value":"T\u00e9cnico de Desenvolvimento e Administra\u00e7\u00e3o - Biblioteconomia"},{"id":393,"value":"T\u00e9cnico de enfermagem"},{"id":3930,"value":"T\u00e9cnico de enfermagem do trabalho"},{"id":6989,"value":"T\u00e9cnico de Estabilidade J\u00fanior"},{"id":5108,"value":"T\u00e9cnico de Explora\u00e7\u00e3o de Petr\u00f3leo I - Geod\u00e9sia"},{"id":5109,"value":"T\u00e9cnico de Explora\u00e7\u00e3o de Petr\u00f3leo I - Geologia"},{"id":929,"value":"T\u00e9cnico de Explora\u00e7\u00e3o de Petr\u00f3leo J\u00fanior"},{"id":7219,"value":"T\u00e9cnico de Explora\u00e7\u00e3o de Petr\u00f3leo J\u00fanior - Geologia"},{"id":2517,"value":"T\u00e9cnico de Farm\u00e1cia"},{"id":7476,"value":"T\u00e9cnico de Geoci\u00eancias"},{"id":7644,"value":"T\u00e9cnico de Geoci\u00eancias - Hidrologia"},{"id":7643,"value":"T\u00e9cnico de Geoci\u00eancias - Minera\u00e7\u00e3o"},{"id":8761,"value":"T\u00e9cnico de Gest\u00e3o - Administra\u00e7\u00e3o"},{"id":394,"value":"T\u00e9cnico de Inform\u00e1tica"},{"id":7220,"value":"T\u00e9cnico de Inform\u00e1tica J\u00fanior"},{"id":930,"value":"T\u00e9cnico de Inspe\u00e7\u00e3o de Equipamentos e Instala\u00e7\u00f5es J\u00fanior"},{"id":5436,"value":"T\u00e9cnico de Instala\u00e7\u00f5es"},{"id":395,"value":"T\u00e9cnico de Laborat\u00f3rio"},{"id":6305,"value":"T\u00e9cnico de Laborat\u00f3rio - Alimentos"},{"id":1765,"value":"T\u00e9cnico de Laborat\u00f3rio - An\u00e1lises Cl\u00ednicas"},{"id":6556,"value":"T\u00e9cnico de Laborat\u00f3rio - Anatomia e Necropsia"},{"id":4604,"value":"T\u00e9cnico de Laborat\u00f3rio - Anatomia Patol\u00f3gica"},{"id":2382,"value":"T\u00e9cnico de Laborat\u00f3rio - Biologia"},{"id":4855,"value":"T\u00e9cnico de Laborat\u00f3rio - Biot\u00e9rio"},{"id":6560,"value":"T\u00e9cnico de Laborat\u00f3rio - Edifica\u00e7\u00f5es"},{"id":3280,"value":"T\u00e9cnico de Laborat\u00f3rio - Eletroeletr\u00f4nica"},{"id":4857,"value":"T\u00e9cnico de Laborat\u00f3rio - Eletr\u00f4nica"},{"id":2383,"value":"T\u00e9cnico de Laborat\u00f3rio - F\u00edsica"},{"id":2617,"value":"T\u00e9cnico de Laborat\u00f3rio - Geografia"},{"id":4580,"value":"T\u00e9cnico de Laborat\u00f3rio - Hemoterapia"},{"id":2618,"value":"T\u00e9cnico de Laborat\u00f3rio - Histologia"},{"id":2548,"value":"T\u00e9cnico de Laborat\u00f3rio - Inform\u00e1tica"},{"id":2384,"value":"T\u00e9cnico de Laborat\u00f3rio - Mec\u00e2nica"},{"id":2638,"value":"T\u00e9cnico de Laborat\u00f3rio - Microscopia Eletr\u00f4nica"},{"id":6755,"value":"T\u00e9cnico de Laborat\u00f3rio - Nutri\u00e7\u00e3o Animal"},{"id":8163,"value":"T\u00e9cnico de Laborat\u00f3rio - Nutri\u00e7\u00e3o e Diet\u00e9tica"},{"id":7724,"value":"T\u00e9cnico de Laborat\u00f3rio - Patologia"},{"id":2543,"value":"T\u00e9cnico de Laborat\u00f3rio - Qu\u00edmica"},{"id":931,"value":"T\u00e9cnico de Log\u00edstica de Transporte J\u00fanior - Controle"},{"id":932,"value":"T\u00e9cnico de Log\u00edstica de Transporte J\u00fanior - Opera\u00e7\u00e3o"},{"id":7469,"value":"T\u00e9cnico de Manuten\u00e7\u00e3o J\u00fanior"},{"id":933,"value":"T\u00e9cnico de Manuten\u00e7\u00e3o J\u00fanior - Caldeiraria"},{"id":934,"value":"T\u00e9cnico de Manuten\u00e7\u00e3o J\u00fanior - El\u00e9trica"},{"id":935,"value":"T\u00e9cnico de Manuten\u00e7\u00e3o J\u00fanior - Eletr\u00f4nica"},{"id":936,"value":"T\u00e9cnico de Manuten\u00e7\u00e3o J\u00fanior - Instrumenta\u00e7\u00e3o"},{"id":937,"value":"T\u00e9cnico de Manuten\u00e7\u00e3o J\u00fanior - Mec\u00e2nica"},{"id":2931,"value":"T\u00e9cnico de Necr\u00f3psia"},{"id":5167,"value":"T\u00e9cnico de N\u00edvel M\u00e9dio - Administrativa"},{"id":5168,"value":"T\u00e9cnico de N\u00edvel M\u00e9dio - Inform\u00e1tica"},{"id":7809,"value":"T\u00e9cnico de N\u00edvel M\u00e9dio - T\u00e9cnico em Seguran\u00e7a do Trabalho"},{"id":833,"value":"T\u00e9cnico de N\u00edvel Superior"},{"id":7793,"value":"T\u00e9cnico de N\u00edvel Superior - Administra\u00e7\u00e3o"},{"id":4834,"value":"T\u00e9cnico de N\u00edvel Superior - Contabilidade"},{"id":4864,"value":"T\u00e9cnico de N\u00edvel Superior - Direito"},{"id":659,"value":"T\u00e9cnico de N\u00edvel Superior - Economista"},{"id":8593,"value":"T\u00e9cnico de N\u00edvel Superior - Inform\u00e1tica"},{"id":4851,"value":"T\u00e9cnico de N\u00edvel Superior - Tecnologia da Informa\u00e7\u00e3o"},{"id":3203,"value":"T\u00e9cnico de N\u00edvel Superior A - Analista de Sistemas"},{"id":308,"value":"T\u00e9cnico de Opera\u00e7\u00e3o"},{"id":955,"value":"T\u00e9cnico de Opera\u00e7\u00e3o J\u00fanior"},{"id":6988,"value":"T\u00e9cnico de Perfura\u00e7\u00e3o e Po\u00e7os J\u00fanior"},{"id":8031,"value":"T\u00e9cnico de Planejamento e Pesquisa - Infra-Estruturas e Log\u00edstica de Base"},{"id":8033,"value":"T\u00e9cnico de Planejamento e Pesquisa - Prote\u00e7\u00e3o Social,  Direitos e Oportunidades"},{"id":8034,"value":"T\u00e9cnico de Planejamento e Pesquisa - Sustentabilidade Ambiental"},{"id":686,"value":"T\u00e9cnico de Processos Organizacionais - Administrativo"},{"id":7083,"value":"T\u00e9cnico de Projetos"},{"id":5169,"value":"T\u00e9cnico de Projetos, Constru\u00e7\u00e3o e Montagem I - Edifica\u00e7\u00f5es"},{"id":5198,"value":"T\u00e9cnico de Projetos, Constru\u00e7\u00e3o e Montagem I - Mec\u00e2nica"},{"id":938,"value":"T\u00e9cnico de Projetos, Constru\u00e7\u00e3o e Montagem J\u00fanior - Edifica\u00e7\u00f5es"},{"id":939,"value":"T\u00e9cnico de Projetos, Constru\u00e7\u00e3o e Montagem J\u00fanior - El\u00e9trica"},{"id":5110,"value":"T\u00e9cnico de Projetos, Constru\u00e7\u00e3o e Montagem J\u00fanior - Eletr\u00f4nica"},{"id":940,"value":"T\u00e9cnico de Projetos, Constru\u00e7\u00e3o e Montagem J\u00fanior - Estruturas Navais"},{"id":941,"value":"T\u00e9cnico de Projetos, Constru\u00e7\u00e3o e Montagem J\u00fanior - Instrumenta\u00e7\u00e3o"},{"id":942,"value":"T\u00e9cnico de Projetos, Constru\u00e7\u00e3o e Montagem J\u00fanior - Mec\u00e2nica"},{"id":3251,"value":"T\u00e9cnico de Qu\u00edmica"},{"id":957,"value":"T\u00e9cnico de Qu\u00edmica J\u00fanior"},{"id":2237,"value":"T\u00e9cnico de Sa\u00fade"},{"id":334,"value":"T\u00e9cnico de Seguran\u00e7a"},{"id":583,"value":"T\u00e9cnico de Seguran\u00e7a do Trabalho"},{"id":7095,"value":"T\u00e9cnico de Seguran\u00e7a J\u00fanior"},{"id":8535,"value":"T\u00e9cnico de Sistemas de Saneamento - Hidr\u00e1ulica"},{"id":7009,"value":"T\u00e9cnico de Suporte e Comunica\u00e7\u00e3o - TI"},{"id":7181,"value":"T\u00e9cnico de Suporte em Infraestrutura de Transportes"},{"id":309,"value":"T\u00e9cnico de Suprimento de Bens e Servi\u00e7os"},{"id":943,"value":"T\u00e9cnico de Suprimentos de Bens e Servi\u00e7os J\u00fanior - Administra\u00e7\u00e3o"},{"id":944,"value":"T\u00e9cnico de Suprimentos de Bens e Servi\u00e7os J\u00fanior - El\u00e9trica"},{"id":945,"value":"T\u00e9cnico de Suprimentos de Bens e Servi\u00e7os J\u00fanior - Mec\u00e2nica"},{"id":1849,"value":"T\u00e9cnico de Tecnologia da Informa\u00e7\u00e3o"},{"id":529,"value":"T\u00e9cnico do Banco Central"},{"id":8713,"value":"T\u00e9cnico do Minist\u00e9rio P\u00fablico"},{"id":717,"value":"T\u00e9cnico do Minist\u00e9rio P\u00fablico \u2013 \u00c1rea Administrativa"},{"id":6873,"value":"T\u00e9cnico do MP - Direito"},{"id":234,"value":"T\u00e9cnico do Seguro Social"},{"id":1000,"value":"T\u00e9cnico em Administra\u00e7\u00e3o de Empresas"},{"id":1857,"value":"T\u00e9cnico em Agrimensura"},{"id":1491,"value":"T\u00e9cnico em Agropecu\u00e1ria"},{"id":3496,"value":"T\u00e9cnico em Almoxarife"},{"id":8246,"value":"T\u00e9cnico em An\u00e1lises Cl\u00ednicas"},{"id":2389,"value":"T\u00e9cnico em Anatomia e Necr\u00f3psia"},{"id":2615,"value":"T\u00e9cnico em Arquivo"},{"id":2390,"value":"T\u00e9cnico em Artes Gr\u00e1ficas"},{"id":891,"value":"T\u00e9cnico em Assuntos Educacionais"},{"id":3284,"value":"T\u00e9cnico em Atividades Administrativas"},{"id":2642,"value":"T\u00e9cnico em \u00c1udiovisual"},{"id":8871,"value":"T\u00e9cnico em Citopatologia"},{"id":7043,"value":"T\u00e9cnico em Comunica\u00e7\u00e3o e Processamento de Dados"},{"id":579,"value":"T\u00e9cnico em Comunica\u00e7\u00e3o Social"},{"id":1194,"value":"T\u00e9cnico em Comunica\u00e7\u00e3o Social - Jornalismo"},{"id":1196,"value":"T\u00e9cnico em Comunica\u00e7\u00e3o Social - Publicidade e Propaganda"},{"id":1195,"value":"T\u00e9cnico em Comunica\u00e7\u00e3o Social - Rel P\u00fablicas"},{"id":2733,"value":"T\u00e9cnico em Constru\u00e7\u00e3o Civil"},{"id":392,"value":"T\u00e9cnico em Edifica\u00e7\u00f5es"},{"id":4527,"value":"T\u00e9cnico em Educa\u00e7\u00e3o"},{"id":7432,"value":"Tecnico em El\u00e9trica"},{"id":4745,"value":"T\u00e9cnico em Eletricidade"},{"id":2070,"value":"T\u00e9cnico em eletromec\u00e2nica"},{"id":1032,"value":"T\u00e9cnico em Eletr\u00f4nica"},{"id":872,"value":"T\u00e9cnico em Eletrot\u00e9cnica"},{"id":8093,"value":"T\u00e9cnico em Farm\u00e1cia"},{"id":7131,"value":"T\u00e9cnico em Financiamento e Execu\u00e7\u00e3o de Programas e Projetos Educacionais"},{"id":2123,"value":"T\u00e9cnico em Gest\u00e3o"},{"id":3710,"value":"T\u00e9cnico em Gest\u00e3o - Inform\u00e1tica"},{"id":7265,"value":"T\u00e9cnico em Gest\u00e3o de Telecomunica\u00e7\u00f5es \u2013 Assistente Administrativo"},{"id":7266,"value":"T\u00e9cnico em Gest\u00e3o de Telecomunica\u00e7\u00f5es \u2013 Assistente T\u00e9cnico"},{"id":1462,"value":"T\u00e9cnico em Higiene Dental"},{"id":8547,"value":"T\u00e9cnico em Informa\u00e7\u00f5es - Geografia e Estat\u00edsticas"},{"id":1,"value":"T\u00e9cnico em Inform\u00e1tica"},{"id":550,"value":"T\u00e9cnico em Inform\u00e1tica - \u00c1rea Sistemas"},{"id":7791,"value":"Tecnico em Laboratorio"},{"id":7014,"value":"T\u00e9cnico em Laborat\u00f3rio - Biologia"},{"id":7017,"value":"T\u00e9cnico em Laborat\u00f3rio - F\u00edsica"},{"id":1073,"value":"T\u00e9cnico em Laborat\u00f3rio - Qu\u00edmica"},{"id":1174,"value":"T\u00e9cnico em Manuten\u00e7\u00e3o de Computador"},{"id":873,"value":"T\u00e9cnico em Mec\u00e2nica"},{"id":2216,"value":"T\u00e9cnico em Minera\u00e7\u00e3o"},{"id":2643,"value":"T\u00e9cnico em Nutri\u00e7\u00e3o"},{"id":8541,"value":"T\u00e9cnico em Nutri\u00e7\u00e3o e Diet\u00e9tica"},{"id":6631,"value":"T\u00e9cnico em \u00d3ptica"},{"id":4265,"value":"T\u00e9cnico em Patologia Cl\u00ednica"},{"id":1772,"value":"T\u00e9cnico em Planejamento"},{"id":4906,"value":"T\u00e9cnico em Planejamento, Gest\u00e3o e Infra-Estrutura em Propriedade Industrial"},{"id":8678,"value":"T\u00e9cnico em Planejamento, Or\u00e7amento e Finan\u00e7as - Administra\u00e7\u00e3o"},{"id":8679,"value":"T\u00e9cnico em Planejamento, Or\u00e7amento e Finan\u00e7as - Ci\u00eancias Cont\u00e1beis"},{"id":4907,"value":"T\u00e9cnico em Propriedade Industrial"},{"id":1138,"value":"T\u00e9cnico em Pr\u00f3tese Dent\u00e1ria"},{"id":7365,"value":"T\u00e9cnico em Qu\u00edmica"},{"id":397,"value":"T\u00e9cnico em radiologia"},{"id":1773,"value":"T\u00e9cnico em Recursos Humanos"},{"id":42,"value":"T\u00e9cnico em Regula\u00e7\u00e3o"},{"id":348,"value":"T\u00e9cnico em Regula\u00e7\u00e3o - Telecomunica\u00e7\u00f5es"},{"id":7153,"value":"T\u00e9cnico em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil"},{"id":7148,"value":"T\u00e9cnico em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil - \u00c1rea 1"},{"id":7149,"value":"T\u00e9cnico em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil - \u00c1rea 2"},{"id":7150,"value":"T\u00e9cnico em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil - \u00c1rea 3"},{"id":7151,"value":"T\u00e9cnico em Regula\u00e7\u00e3o de Avia\u00e7\u00e3o Civil - \u00c1rea 4"},{"id":7467,"value":"T\u00e9cnico em Regula\u00e7\u00e3o de Sa\u00fade Suplementar"},{"id":7360,"value":"T\u00e9cnico em Regula\u00e7\u00e3o de Servi\u00e7os de Transportes Terrestres"},{"id":3514,"value":"T\u00e9cnico em Restaura\u00e7\u00e3o"},{"id":1493,"value":"T\u00e9cnico em Saneamento"},{"id":7725,"value":"T\u00e9cnico em Sa\u00fade Bucal"},{"id":8336,"value":"T\u00e9cnico em Sa\u00fade P\u00fablica - An\u00e1lises Cl\u00ednicas"},{"id":8519,"value":"T\u00e9cnico em Secretariado"},{"id":7025,"value":"T\u00e9cnico em Seguran\u00e7a do Trabalho"},{"id":8545,"value":"T\u00e9cnico em Seguran\u00e7a e Higiene do Trabalho"},{"id":8615,"value":"T\u00e9cnico em Tecnologia da Infoma\u00e7\u00e3o - Desenvolvimento"},{"id":8523,"value":"T\u00e9cnico em Tecnologia da Infoma\u00e7\u00e3o e Comunica\u00e7\u00e3o - Web Designer"},{"id":8613,"value":"T\u00e9cnico em Tecnologia da Informa\u00e7\u00e3o - Rede de Computadores"},{"id":8841,"value":"T\u00e9cnico em Tecnologia da Informa\u00e7\u00e3o e Comunica\u00e7\u00e3o"},{"id":8457,"value":"T\u00e9cnico em Telecomunica\u00e7\u00f5es"},{"id":6767,"value":"T\u00e9cnico Especializado II"},{"id":1860,"value":"T\u00e9cnico Florestal"},{"id":8291,"value":"T\u00e9cnico Forense"},{"id":4930,"value":"T\u00e9cnico Gerencial - Direito"},{"id":8499,"value":"T\u00e9cnico I"},{"id":5397,"value":"T\u00e9cnico Industrial"},{"id":1992,"value":"T\u00e9cnico Industrial - Mec\u00e2nica"},{"id":1863,"value":"T\u00e9cnico Industrial de Eletr\u00f4nica"},{"id":271,"value":"T\u00e9cnico Judici\u00e1rio"},{"id":6764,"value":"T\u00e9cnico Judici\u00e1rio - Administrativo"},{"id":45,"value":"T\u00e9cnico Judici\u00e1rio - \u00c1rea Administrativa"},{"id":540,"value":"T\u00e9cnico Judici\u00e1rio - \u00c1rea Judici\u00e1ria"},{"id":78,"value":"T\u00e9cnico Judici\u00e1rio - \u00c1rea Servi\u00e7os Gerais"},{"id":79,"value":"T\u00e9cnico Judici\u00e1rio - Artes Gr\u00e1ficas"},{"id":6916,"value":"T\u00e9cnico Judici\u00e1rio - Auxiliar"},{"id":172,"value":"T\u00e9cnico Judici\u00e1rio - Contabilidade"},{"id":3650,"value":"T\u00e9cnico Judici\u00e1rio - Edifica\u00e7\u00f5es"},{"id":564,"value":"T\u00e9cnico Judici\u00e1rio - Eletricidade"},{"id":536,"value":"T\u00e9cnico Judici\u00e1rio - Higiene Dental"},{"id":270,"value":"T\u00e9cnico Judici\u00e1rio - Inform\u00e1tica"},{"id":6935,"value":"T\u00e9cnico Judici\u00e1rio - Judici\u00e1ria e Administrativa"},{"id":637,"value":"T\u00e9cnico Judici\u00e1rio - Mec\u00e2nica"},{"id":7164,"value":"T\u00e9cnico Judici\u00e1rio - Motorista"},{"id":71,"value":"T\u00e9cnico Judici\u00e1rio - Opera\u00e7\u00e3o de Computador"},{"id":628,"value":"T\u00e9cnico Judici\u00e1rio - Programa\u00e7\u00e3o"},{"id":7901,"value":"T\u00e9cnico Judici\u00e1rio - Revisor Judici\u00e1rio"},{"id":6765,"value":"T\u00e9cnico Judici\u00e1rio - Seguran\u00e7a"},{"id":8517,"value":"T\u00e9cnico Judici\u00e1rio - Seguran\u00e7a do Trabalho"},{"id":76,"value":"T\u00e9cnico Judici\u00e1rio - Seguran\u00e7a e Transporte"},{"id":74,"value":"T\u00e9cnico Judici\u00e1rio - Seguran\u00e7a Judici\u00e1ria"},{"id":180,"value":"T\u00e9cnico Judici\u00e1rio - Tecnologia da Informa\u00e7\u00e3o"},{"id":49,"value":"T\u00e9cnico Judici\u00e1rio - Telecomunica\u00e7\u00f5es e Eletricidade"},{"id":73,"value":"T\u00e9cnico Judici\u00e1rio - Telefonia"},{"id":625,"value":"T\u00e9cnico Judici\u00e1rio - Transporte"},{"id":6675,"value":"T\u00e9cnico Jur\u00eddico - Apoio Administrativo"},{"id":3385,"value":"T\u00e9cnico Legislativo"},{"id":896,"value":"T\u00e9cnico Legislativo - Administra\u00e7\u00e3o"},{"id":897,"value":"T\u00e9cnico Legislativo - Processo Legislativo"},{"id":3614,"value":"T\u00e9cnico M\u00e9dio de Defensoria P\u00fablica"},{"id":8379,"value":"T\u00e9cnico Ministerial"},{"id":7533,"value":"T\u00e9cnico Ministerial - Administrativo"},{"id":6947,"value":"T\u00e9cnico Ministerial - \u00c1rea Administrativa"},{"id":6983,"value":"T\u00e9cnico Ministerial - Contabilidade"},{"id":7535,"value":"T\u00e9cnico Ministerial - Execu\u00e7\u00e3o de Mandados"},{"id":6948,"value":"T\u00e9cnico Ministerial - Inform\u00e1tica"},{"id":7117,"value":"T\u00e9cnico Ministerial - Motorista"},{"id":6994,"value":"T\u00e9cnico Ministerial - Telecomunica\u00e7\u00f5es"},{"id":8702,"value":"T\u00e9cnico Operacional Portu\u00e1rio - Meio Ambiente"},{"id":8703,"value":"T\u00e9cnico Operacional Portu\u00e1rio - Seguran\u00e7a do Trabalho"},{"id":8774,"value":"T\u00e9cnico Organizacional - \u00c1rea Administrativa"},{"id":8601,"value":"T\u00e9cnico Previdenci\u00e1rio"},{"id":8864,"value":"T\u00e9cnico Previdenci\u00e1rio - Administrativa"},{"id":335,"value":"T\u00e9cnico Qu\u00edmico"},{"id":947,"value":"T\u00e9cnico Qu\u00edmico de Petr\u00f3leo J\u00fanior"},{"id":710,"value":"T\u00e9cnico Superior Administrador"},{"id":709,"value":"T\u00e9cnico Superior de An\u00e1lise Cont\u00e1bil"},{"id":711,"value":"T\u00e9cnico Superior de An\u00e1lise de Sistemas e M\u00e9todos"},{"id":708,"value":"T\u00e9cnico Superior de Procuradoria"},{"id":8630,"value":"T\u00e9cnico Superior Especializado - Administra\u00e7\u00e3o"},{"id":8631,"value":"T\u00e9cnico Superior Especializado - Administra\u00e7\u00e3o de Dados"},{"id":8639,"value":"T\u00e9cnico Superior Especializado - Biblioteconomia"},{"id":8638,"value":"T\u00e9cnico Superior Especializado - Ci\u00eancias Cont\u00e1beis"},{"id":8632,"value":"T\u00e9cnico Superior Especializado - Desenvolvimento de Sistemas"},{"id":8640,"value":"T\u00e9cnico Superior Especializado - Economia"},{"id":8644,"value":"T\u00e9cnico Superior Especializado - Estat\u00edstica"},{"id":8633,"value":"T\u00e9cnico Superior Especializado - Gest\u00e3o em Tecnologia da Informa\u00e7\u00e3o"},{"id":8635,"value":"T\u00e9cnico Superior Especializado - Seguran\u00e7a da Informa\u00e7\u00e3o"},{"id":8637,"value":"T\u00e9cnico Superior Especializado - Servi\u00e7o Social"},{"id":8636,"value":"T\u00e9cnico Superior Especializado - Suporte"},{"id":3613,"value":"T\u00e9cnico Superior Jur\u00eddico"},{"id":7045,"value":"Tecnologia da Informa\u00e7\u00e3o"},{"id":5810,"value":"Tecnologia da Informa\u00e7\u00e3o - Desenvolvimento de Sistemas"},{"id":7932,"value":"Tecnologista - An\u00e1lise Agr\u00edcola"},{"id":7933,"value":"Tecnologista - An\u00e1lise Pecu\u00e1ria"},{"id":7934,"value":"Tecnologista - An\u00e1lise Socioecon\u00f4mica"},{"id":7935,"value":"Tecnologista - Biblioteconomia"},{"id":7936,"value":"Tecnologista - Cartografia"},{"id":7937,"value":"Tecnologista - Edi\u00e7\u00e3o de V\u00eddeo"},{"id":7938,"value":"Tecnologista - Estat\u00edstica"},{"id":7939,"value":"Tecnologista - Geografia"},{"id":7940,"value":"Tecnologista - Geoprocessamento"},{"id":1545,"value":"Tecnologista em Informa\u00e7\u00f5es Geogr\u00e1ficas - An\u00e1lise Agr\u00edcola"},{"id":7238,"value":"Tecnologista em Propriedade Industrial"},{"id":5913,"value":"Tecnologista em Sa\u00fade - Arquivologia"},{"id":5922,"value":"Tecnologista em Sa\u00fade - Biosseguran\u00e7a"},{"id":5948,"value":"Tecnologista em Sa\u00fade - Engenharia de Produ\u00e7\u00e3o"},{"id":5956,"value":"Tecnologista em Sa\u00fade - Farmacologia Aplicada a Produtos Naturais"},{"id":5957,"value":"Tecnologista em Sa\u00fade - Farmacot\u00e9cnica"},{"id":5959,"value":"Tecnologista em Sa\u00fade - Fisioterapia Pedi\u00e1trica"},{"id":5990,"value":"Tecnologista em Sa\u00fade - Seguran\u00e7a da Informa\u00e7\u00e3o"},{"id":5992,"value":"Tecnologista em Sa\u00fade - Suporte \u00e0 Infra-estrutura Tecnol\u00f3gica da Pesquisa"},{"id":5995,"value":"Tecnologista em Sa\u00fade - TI - Sistemas de Informa\u00e7\u00e3o"},{"id":5999,"value":"Tecnologista em Sa\u00fade - Zoologia M\u00e9dica"},{"id":8577,"value":"Tecnologista J\u00fanior"},{"id":1206,"value":"Tecnologista J\u00fanior - Assist\u00eancia Social - Servi\u00e7o Social"},{"id":1210,"value":"Tecnologista J\u00fanior \u2013 An\u00e1lises Cl\u00ednicas"},{"id":1211,"value":"Tecnologista J\u00fanior \u2013 Anatomia Patol\u00f3gica"},{"id":1227,"value":"Tecnologista J\u00fanior \u2013 Auditor de Qualidade em Radioterapia"},{"id":1305,"value":"Tecnologista J\u00fanior \u2013 Bioinform\u00e1tica"},{"id":1244,"value":"Tecnologista J\u00fanior \u2013 Cancerologia Cir\u00fargica"},{"id":1245,"value":"Tecnologista J\u00fanior \u2013 Cancerologia Cl\u00ednica"},{"id":1246,"value":"Tecnologista J\u00fanior \u2013 Cancerologia Pedi\u00e1trica"},{"id":1220,"value":"Tecnologista J\u00fanior \u2013 C\u00e9lula Tronco e Hematopo\u00e9tica"},{"id":1247,"value":"Tecnologista J\u00fanior \u2013 Cirurgia De Cabe\u00e7a E Pesco\u00e7o"},{"id":1248,"value":"Tecnologista J\u00fanior \u2013 Cirurgia De Tecido \u00d3sseo E Conectivo"},{"id":1249,"value":"Tecnologista J\u00fanior \u2013 Cirurgia Tor\u00e1cica"},{"id":1212,"value":"Tecnologista J\u00fanior \u2013 Citotecnologia"},{"id":1276,"value":"Tecnologista J\u00fanior \u2013 Cria\u00e7\u00e3o De Animais De Laborat\u00f3rio"},{"id":1258,"value":"Tecnologista J\u00fanior \u2013 Endoscopia Digestiva"},{"id":1259,"value":"Tecnologista J\u00fanior \u2013 Endoscopia Peroral"},{"id":1226,"value":"Tecnologista J\u00fanior \u2013 Farm\u00e1cia Hospitalar"},{"id":1231,"value":"Tecnologista J\u00fanior \u2013 Fisioterapia"},{"id":1260,"value":"Tecnologista J\u00fanior \u2013 Hematologia"},{"id":1209,"value":"Tecnologista J\u00fanior \u2013 Imunogen\u00e9tica"},{"id":1264,"value":"Tecnologista J\u00fanior \u2013 Mastologia"},{"id":1306,"value":"Tecnologista J\u00fanior \u2013 Medicina - Hemoterapia"},{"id":1256,"value":"Tecnologista J\u00fanior \u2013 Medicina \u2013 Emerg\u00eancia"},{"id":1267,"value":"Tecnologista J\u00fanior \u2013 Neurocirurgia"},{"id":1277,"value":"Tecnologista J\u00fanior \u2013 Nutri\u00e7\u00e3o Cl\u00ednica"},{"id":1268,"value":"Tecnologista J\u00fanior \u2013 Oftalmologia"},{"id":1215,"value":"Tecnologista J\u00fanior \u2013 Preven\u00e7\u00e3o do C\u00e2ncer"},{"id":1233,"value":"Tecnologista J\u00fanior \u2013 Programas Educacionais em Sa\u00fade"},{"id":1281,"value":"Tecnologista J\u00fanior \u2013 Psicologia Cl\u00ednica"},{"id":1229,"value":"Tecnologista J\u00fanior \u2013 Radiologia"},{"id":1230,"value":"Tecnologista J\u00fanior \u2013 Radioterapia"},{"id":7464,"value":"Tecnologista Pleno"},{"id":7445,"value":"Tecnologista Pleno - Computa\u00e7\u00e3o"},{"id":8539,"value":"Tecn\u00f3logo"},{"id":8540,"value":"Tecn\u00f3logo - Sistemas"},{"id":7267,"value":"Tecn\u00f3logo de Administra\u00e7\u00e3o"},{"id":2178,"value":"Tecn\u00f3logo em Radiologia"},{"id":1141,"value":"Telefonista"},{"id":7279,"value":"Tempor\u00e1rio Superior 1  - Direito"},{"id":1140,"value":"Terapeuta Ocupacional"},{"id":6642,"value":"Titular de Servi\u00e7os de Notas e de Registros"},{"id":368,"value":"Todos os Cargos"},{"id":2652,"value":"Tradutor Int\u00e9rprete"},{"id":6928,"value":"Vestibular"},{"id":2981,"value":"Veterin\u00e1rio"},{"id":1478,"value":"Vigia"},{"id":1177,"value":"Web Designer"},{"id":1178,"value":"Zootecnista"},{"id":784,"value":"\ufeffAgente T\u00e9cnico Legislativo Especializado - Biblioteconomia"}]';
		
		$cargos = json_decode($json, true);
		
		foreach ($cargos as $cargo) {
			$data_cargo = array(
				'car_id' => $cargo['id'],
				'car_nome' => $cargo['value'] 
			); 
			
			$this->cargo_model->salvar($data_cargo);
		}
		
	}
	
	
	
	public function listar_questoes_sem_gabarito()
	{
		$questoes = $this->questao_model->listar_questoes_sem_gabarito();
		
		$retorno = array();
		
		foreach ($questoes as $questao) {
			
			array_push($retorno, implode(",", array(
				$questao['que_qcon_id'],
				is_null($questao['que_texto_base']) ? 1 : 0
			)));
		}
		
		echo json_encode($retorno);
	}


	public function listar_questoes_sem_texto_base()
	{
		$questoes = $this->questao_model->listar_questoes_sem_texto_base(100);
		
		$retorno = array();
		
		foreach ($questoes as $questao) {
			
			array_push($retorno, implode(",", array(
				$questao['que_qcon_id'],
				is_null($questao['que_texto_base']) ? 1 : 0
			)));
		}
		
		echo json_encode($retorno);
	}
	
	public function listar_questoes_com_gabarito_sem_texto_base()
	{
		$questoes = $this->questao_model->listar_questoes_com_gabarito_sem_texto_base();
	
		$retorno = array();
	
		foreach ($questoes as $questao) {
				
			array_push($retorno, implode(",", array(
					$questao['que_qcon_id']
			)));
		}
	
		echo json_encode($retorno);
	}
	
	public function get_questao_sem_gabarito()
	{
		$questao = $this->questao_model->get_questao_sem_gabarito();
	
		echo $questao['que_qcon_id'];
	}
	
	public function recuperar_texto_base($qcon_id)
	{
		$questao = $this->questao_model->get_by_qcon_id($qcon_id);
		echo is_null($questao['que_texto_base']) ? 1 : 0;
	}
	
	public function salvar_gabarito($qcon_id, $resposta)
	{
		$questao = $this->questao_model->get_by_qcon_id($qcon_id);
		$this->questao_model->atualizar_resposta($questao['que_id'], $resposta);
	}
	
	public function salvar_texto_base($qcon_id)
	{
		$questao = $this->questao_model->get_by_qcon_id($qcon_id);
		
		$texto_base = $this->input->post('texto_base');
	
		$this->questao_model->atualizar_texto_base($questao['que_id'], $texto_base);
	}
	
	public function resetar_usuarios_buscador()
	{
		$this->buscador_model->resetar_usuarios_buscador();
	}
	
	public function marcar_usuario_buscador_como_nao_disponivel($usuario_buscador_id)
	{
		$this->buscador_model->marcar_usuario_buscador_como_nao_disponivel($usuario_buscador_id);
	}
	
	public function get_usuario_buscador_disponivel()
	{
		$usuario_buscador = $this->buscador_model->get_usuario_buscador_disponivel();
		
		if($usuario_buscador) {
			echo implode(",", array(
				$usuario_buscador['bus_id'],
				$usuario_buscador['bus_email'],
				$usuario_buscador['bus_senha'],
			));
		}
		else {
			echo "";
		}
		
	}
	
	public function usuario_buscador()
	{
		

		if($this->input->post()) {
			$dados = array(
				'bus_email' => $this->input->post('email'),
				'bus_senha' => $this->input->post('senha')
			);
			
			$this->db->insert('buscador_usuarios', $dados);
		}
		
		$data['buscadores'] = $this->buscador_model->listar_todos();
		$data['num_disponiveis'] = $this->buscador_model->contar_disponiveis();
		
		
		$this->load->view('buscador/usuario_buscador', $data);
	}
	
	public function atualizar_status()
	{
		$this->load->model('questao_model');
		for($z = 0; $z < 300; $z++) {
			$pagina = $this->configuracao_model->get_valor('status', true);
			
			if($pagina > 83909) exit;
// 			$pagina = 10620;
			
			echo "Buscando na pagina: $pagina<br>";
			
			$url = get_buscador_questoes_dir() . $pagina . ".html";
				
			$html = file_get_html($url);
				
			$elementos = $html->find('.questao-anulada');
			
			if(count($elementos) > 0) {
				foreach ($elementos as $elemento) {
					$pai = $elemento->parent();
					
					if($pai) {
						$pai_array = explode("-", $pai->id);
						
						$id = $pai_array[count($pai_array)-1];
						$this->questao_model->atualizar_status_por_qcon_id($id, CANCELADA);
						echo "Encontrada questao anulada - id: " . $id . "<br>"; 
					}
				}
			}
			else {
				echo "Nao encontramos nenhuma questao anulada<br>";
			}
			
			$elementos = $html->find('.questao-desatualizada');
				
			if(count($elementos) > 0) {
				foreach ($elementos as $elemento) {
					$pai = $elemento->parent();
						
					if($pai) {
						$pai_array = explode("-", $pai->id);
			
						$id = $pai_array[count($pai_array)-1];
						$this->questao_model->atualizar_status_por_qcon_id($id, DESATUALIZADA);
						echo "Encontrada questão desatualizada - id: " . $id;
					}
				}
			}
			else {
				echo "Nao encontramos nenhuma questao desatualizada<br>";
			}
			
			$configuracao = array('cfg_nome' => 'status', 'cfg_valor' => $pagina + 1);
			$this->configuracao_model->atualizar($configuracao);
		}
	}
	
	public function area_atuacao()
	{
		for($i = 0; $i < 20; $i++) {
			set_time_limit(30);
			$area_id = $this->configuracao_model->get_valor('area_atuacao_id', true);
			$page = $this->configuracao_model->get_valor('area_atuacao_pg', true);
			
			if($area_id > 16) exit;
			
			log_buscador("info", "***** Processando Area de Atuacao *****");
			log_buscador("info", "Recuperando conteudo da área [$area_id], página[$page]");
			$url = "https://www.qconcursos.com/questoes-de-concursos/provas/search?utf8=%E2%9C%93&area_atuacao_hidden={$area_id}&page={$page}";
	
			$html = file_get_html($url);
			$elementos = $html->find('.txt-resultado-titulo a');
				
			if(!$elementos) {
				log_buscador("info", "Nao existem mais provas para a área [$area_id].");
	
				$configuracao = array('cfg_nome' => 'area_atuacao_id', 'cfg_valor' => $area_id + 1);
				$this->configuracao_model->atualizar($configuracao);
	
				$configuracao = array('cfg_nome' => 'area_atuacao_pg', 'cfg_valor' => 1);
				$this->configuracao_model->atualizar($configuracao);
	
				exit;
			}
				
			foreach ($elementos as $elemento) {
				$prova_url = get_qcon_prova_url($elemento->href, true);
				$prova = $this->prova_model->get_by_url($prova_url);
	
				log_buscador("info", "Prova url [$area_id], página[$page] : $prova_url");
	
				if(!$prova) {
					log_buscador("info", "Essa prova nao esta cadastrada na Exponencial");
					continue;
				}
				else {
					log_buscador("info", "Prova encontrada na Exponencial: {$prova['pro_id']}");
					$area = $this->area_atuacao_model->get_by_id($area_id);
						
					log_buscador("info", "Procurando area de atuacao [$area_id]");
					// cadastra área se não encontrar no banco
					if(!$area) {
						log_buscador("info", "Area de atuacao [$area_id] nao encontrada ");
	
						$nome = $html->find('#lista_itens li', 0)->innertext;
	
						$data_area = array(
								'ara_id' => $area_id,
								'ara_nome' => trim($nome)
						);
	
						log_buscador("info", "Salvando area de atuacao [$area_id] [$nome]");
						$this->area_atuacao_model->salvar($data_area);
					}
						
					log_buscador("info", "Adicionando area de atuacao [$area_id] na prova [{$prova['pro_id']}]");
					$this->prova_model->adicionar_area_atuacao($prova['pro_id'], $area['ara_id']);
				}
			}
				
			$configuracao = array('cfg_nome' => 'area_atuacao_pg', 'cfg_valor' => $page + 1);
			$this->configuracao_model->atualizar($configuracao);
		}
	
	}
	
	public function area_formacao()
	{
		for($i = 0; $i < 20; $i++) {
			set_time_limit(30);
			$area_id = $this->configuracao_model->get_valor('area_formacao_id', true);
			$page = $this->configuracao_model->get_valor('area_formacao_pg', true);
	
			if($area_id > 324) exit;
			
			log_buscador("info", "***** Processando Area de Formacao *****");
			log_buscador("info", "Recuperando conteudo da área [$area_id], página[$page]");
			$url = "https://www.qconcursos.com/questoes-de-concursos/provas/search?utf8=%E2%9C%93&area_formacao_hidden={$area_id}&page={$page}";
	
			$html = file_get_html($url);
			$elementos = $html->find('.txt-resultado-titulo a');
	
			if(!$elementos) {
				log_buscador("info", "Nao existem mais provas para a área [$area_id].");
	
				$configuracao = array('cfg_nome' => 'area_formacao_id', 'cfg_valor' => $area_id + 1);
				$this->configuracao_model->atualizar($configuracao);
	
				$configuracao = array('cfg_nome' => 'area_formacao_pg', 'cfg_valor' => 1);
				$this->configuracao_model->atualizar($configuracao);
	
				exit;
			}
	
			foreach ($elementos as $elemento) {
				$prova_url = get_qcon_prova_url($elemento->href, true);
				$prova = $this->prova_model->get_by_url($prova_url);
	
				log_buscador("info", "Prova url [$area_id], página[$page] : $prova_url");
	
				if(!$prova) {
					log_buscador("info", "Essa prova nao esta cadastrada na Exponencial");
					continue;
				}
				else {
					log_buscador("info", "Prova encontrada na Exponencial: {$prova['pro_id']}");
					$area = $this->area_formacao_model->get_by_id($area_id);
	
					log_buscador("info", "Procurando area de formacao [$area_id]");
					// cadastra área se não encontrar no banco
					if(!$area) {
						log_buscador("info", "Area de formacao [$area_id] nao encontrada ");
	
						$nome = $html->find('#lista_itens li', 0)->innertext;
	
						$data_area = array(
								'arf_id' => $area_id,
								'arf_nome' => trim($nome)
						);
	
						log_buscador("info", "Salvando area de formacao [$area_id] [$nome]");
						$this->area_formacao_model->salvar($data_area);
					}
	
					log_buscador("info", "Adicionando area de formacao [$area_id] na prova [{$prova['pro_id']}]");
					$this->prova_model->adicionar_area_formacao($prova['pro_id'], $area['arf_id']);
				}
			}
	
			$configuracao = array('cfg_nome' => 'area_formacao_pg', 'cfg_valor' => $page + 1);
			$this->configuracao_model->atualizar($configuracao);
		}
	
	}
	
	function povoar_area_atuacao()
	{
		$areas = json_decode('[{"id":1,"value":"Banc\u00e1ria"},{"id":2,"value":"Controle e Gest\u00e3o"},{"id":8,"value":"Educa\u00e7\u00e3o"},{"id":14,"value":"ENEM"},{"id":3,"value":"Fiscal"},{"id":4,"value":"Jur\u00eddica"},{"id":5,"value":"Legislativa"},{"id":13,"value":"Marinha"},{"id":15,"value":"OAB"},{"id":7,"value":"Outras"},{"id":6,"value":"Policial"},{"id":16,"value":"Vestibular"}]');
		
		foreach ($areas as $area) {
			$data_area = array(
					'ara_id' => $area->id,
					'ara_nome' => $area->value
			);
			
			$this->area_atuacao_model->salvar($data_area);
		}
	}
	
	function povoar_area_formacao()
	{
		$areas = json_decode('[{"id":2,"value":"Administra\u00e7\u00e3o"},{"id":209,"value":"Agrimensura"},{"id":168,"value":"Agronomia"},{"id":277,"value":"Agropecu\u00e1ria"},{"id":76,"value":"Arqueologia"},{"id":4,"value":"Arquitetura e Urbanismo"},{"id":3,"value":"Arquivologia"},{"id":173,"value":"Artes"},{"id":304,"value":"\u00c1udio e V\u00eddeo"},{"id":5,"value":"Biblioteconomia"},{"id":44,"value":"Biologia"},{"id":73,"value":"Biomedicina"},{"id":243,"value":"Bioqu\u00edmica"},{"id":229,"value":"Biosseguran\u00e7a"},{"id":224,"value":"Cartografia"},{"id":276,"value":"Ci\u00eancia e Tecnologia"},{"id":113,"value":"Ci\u00eancias"},{"id":79,"value":"Ci\u00eancias Atuariais"},{"id":7,"value":"Ci\u00eancias Cont\u00e1beis"},{"id":34,"value":"Ci\u00eancias Sociais"},{"id":216,"value":"Comunica\u00e7\u00e3o Social"},{"id":180,"value":"Controle e Automa\u00e7\u00e3o"},{"id":308,"value":"Criminal\u00edstica"},{"id":218,"value":"Desenho"},{"id":35,"value":"Desenho Industrial"},{"id":50,"value":"Design Gr\u00e1fico"},{"id":9,"value":"Direito"},{"id":19,"value":"Economia"},{"id":95,"value":"Edifica\u00e7\u00f5es"},{"id":20,"value":"Educa\u00e7\u00e3o F\u00edsica"},{"id":176,"value":"Eletricidade"},{"id":226,"value":"Eletroeletr\u00f4nica"},{"id":165,"value":"Eletromec\u00e2nica"},{"id":92,"value":"Eletr\u00f4nica"},{"id":51,"value":"Eletrot\u00e9cnica"},{"id":11,"value":"Enfermagem"},{"id":58,"value":"Engenharia "},{"id":197,"value":"Engenharia Agr\u00edcola"},{"id":324,"value":"Engenharia Agron\u00f4mica (Agronomia)"},{"id":10,"value":"Engenharia Civil"},{"id":41,"value":"Engenharia de Telecomunica\u00e7\u00f5es"},{"id":290,"value":"Engenharia de Tr\u00e2nsito"},{"id":31,"value":"Engenharia El\u00e9trica"},{"id":211,"value":"Engenharia Eletr\u00f4nica"},{"id":42,"value":"Engenharia Mec\u00e2nica"},{"id":66,"value":"Engenharia Qu\u00edmica"},{"id":198,"value":"Enologia"},{"id":12,"value":"Estat\u00edstica"},{"id":21,"value":"Farm\u00e1cia"},{"id":22,"value":"Filosofia"},{"id":59,"value":"F\u00edsica"},{"id":27,"value":"Fisioterapia"},{"id":55,"value":"Fonoaudiologia"},{"id":120,"value":"Gastronomia"},{"id":36,"value":"Geografia"},{"id":37,"value":"Geologia"},{"id":220,"value":"Hemoterapia"},{"id":161,"value":"Hidrologia"},{"id":53,"value":"Hist\u00f3ria"},{"id":287,"value":"Instrumenta\u00e7\u00e3o Industrial"},{"id":225,"value":"Interpreta\u00e7\u00e3o - Libras"},{"id":56,"value":"Jornalismo"},{"id":14,"value":"Letras"},{"id":185,"value":"Log\u00edstica"},{"id":38,"value":"Marketing"},{"id":15,"value":"Matem\u00e1tica"},{"id":57,"value":"Mec\u00e2nica"},{"id":208,"value":"Mecatr\u00f4nica"},{"id":23,"value":"Medicina"},{"id":136,"value":"Medicina Veterin\u00e1ria"},{"id":279,"value":"Meio Ambiente"},{"id":199,"value":"Metalurgia"},{"id":116,"value":"Meteorologia"},{"id":269,"value":"Metrologia"},{"id":191,"value":"Minera\u00e7\u00e3o"},{"id":144,"value":"Museologia"},{"id":70,"value":"M\u00fasica"},{"id":1,"value":"N\u00e3o definido"},{"id":28,"value":"Nutri\u00e7\u00e3o"},{"id":33,"value":"Oceanografia"},{"id":29,"value":"Odontologia"},{"id":239,"value":"Patologia Cl\u00ednica"},{"id":16,"value":"Pedagogia"},{"id":221,"value":"Pr\u00f3tese e \u00d3rtese"},{"id":17,"value":"Psicologia"},{"id":48,"value":"Psiquiatria"},{"id":104,"value":"Publicidade"},{"id":39,"value":"Qu\u00edmica"},{"id":230,"value":"Radiologia"},{"id":30,"value":"Rela\u00e7\u00f5es Internacionais"},{"id":103,"value":"Rela\u00e7\u00f5es P\u00fablicas"},{"id":148,"value":"Sa\u00fade P\u00fablica"},{"id":52,"value":"Secretariado"},{"id":93,"value":"Seguran\u00e7a do Trabalho"},{"id":90,"value":"Seguran\u00e7a e Transporte"},{"id":25,"value":"Servi\u00e7o Social"},{"id":189,"value":"Sistemas de G\u00e1s"},{"id":184,"value":"Sociologia"},{"id":13,"value":"Tecnologia da Informa\u00e7\u00e3o"},{"id":244,"value":"Tecnologia Educacional"},{"id":219,"value":"Teologia"},{"id":18,"value":"Terapia Ocupacional"},{"id":127,"value":"T\u00eaxtil e Moda"},{"id":102,"value":"Topografia"},{"id":26,"value":"Turismo"},{"id":299,"value":"Zoologia"},{"id":64,"value":"Zootecnia"}]');
	
		foreach ($areas as $area) {
			$data_area = array(
					'arf_id' => $area->id,
					'arf_nome' => $area->value
			);
				
			$this->area_formacao_model->salvar($data_area);
		}
	}
	
	function teste() {
		$url = "https://www.qconcursos.com/questoes-de-concursos/provas/search?utf8=%E2%9C%93&area_atuacao_hidden={$area_id}&page={$page}";
		
		$html = file_get_html($url);
		
		print_r($html->find('#lista_itens', 0));
	}

	function get_questao_opcoes_incompletas() 
	{
		$questoes = $this->questao_model->listar_questoes_ids_opcoes_incompletas(100);

		if($questoes) {
			echo $questoes[0]['que_id'] . ',' . $questoes[0]['que_qcon_id'];
		}
	}

	function atualizar_data_atualizacao($questao_id)
	{
		$this->questao_model->atualizar_data_atualizacao($questao_id);
	}

	function salvar_opcao($questao_id) 
	{
		$opcao = array(
			'que_id' => $questao_id,
			'qop_texto' => $_POST['qop_texto'],
			'qop_ordem' => $_POST['qop_ordem']
		);

		$this->questao_model->salvar_opcao($opcao);

		if(strpos($opcao['qop_texto'], 'img') !== FALSE) {
			$this->questao_model->marcar_questao_como_inativa($questao_id);
		}
	}
}
?>