<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('MODALIDADE', 'modalidade');
define('S2_LIBERA_AREAS', 's2_libera_areas');

define('B2_BAIXAR_ERROS', 'b2_baixar_erros');
define('B2_BAIXAR_PPM', 'b2_baixar_ppm');
define('B2_BAIXAR_STATUS', 'b2_baixar_status');
define('B2_PROXIMA_PAGINA', 'b2_proxima_pagina');
define('B2_ULTIMA_PAGINA', 'b2_ultima_pagina');
define('B2_URL_BASE', 'b2_url_base');

define('P2_PROVAS_PENDENTES', 'p2_provas_pendentes');
define('P2_PROCESSAR_PPM', 'p2_processar_ppm');
define('Ṕ2_PROCESSAR_STATUS', 'p2_processar_status');
define('P2_PROXIMA_PAGINA', 'p2_proxima_pagina');
define('P2_ULTIMA_PAGINA', 'p2_ultima_pagina');

define('V2_PROCESSAR_STATUS', 'v2_processar_status');
define('V2_PROCESSAR_PPM', 'v2_processar_ppm');

class Buscador2 extends CI_Controller {

	public  function __construct() {
		parent::__construct();
		$this->load->helper('prova');
		$this->load->helper('qcon');
		$this->load->model('area_atuacao_model');
		$this->load->model('area_formacao_model');
		$this->load->model('assunto_model');
		$this->load->model('banca_model');
		$this->load->model('configuracao_model');
		$this->load->model('disciplina_model');
		$this->load->model('orgao_model');
		$this->load->model('prova_model');
		$this->load->model('questao_model');
		$this->load->model('buscador_model');

		$this->load->helper('questao');
		$this->output->enable_profiler(true);

	}

	public function index()
	{
	
		$this->load->view('buscador2/header', $data);
		// $this->load->view(get_listar_disciplina_view_url(), $data);
		$this->load->view('buscador2/footer', $data);
	}

	public function baixar()
	{
		if($this->input->post('submit')) {
			// $url_base = $this->input->post('url_base');

			$this->configuracao_model->set(B2_BAIXAR_PPM, $this->input->post('ppm'));
		}

		if($this->input->post('baixar')) {
			$this->configuracao_model->set(B2_BAIXAR_STATUS, '1');
		}

		if($this->input->post('parar')) {
			$this->configuracao_model->set(B2_BAIXAR_STATUS, '0');
		}

		if($this->input->post('resetar')) {
			self::mudar_modalidade(1);
		}

		/*****************************************
		 * BAIXAR PÁGINAS
		 *****************************************/

		$data['erros'] =  $this->configuracao_model->get(B2_BAIXAR_ERROS) ?  count(unserialize($this->configuracao_model->get(B2_BAIXAR_ERROS))) : 0;
		$data['status'] = $this->configuracao_model->get(B2_BAIXAR_STATUS);
		$data['proxima'] = $this->configuracao_model->get(B2_PROXIMA_PAGINA) - 1;
		$data['ultima'] = $this->configuracao_model->get(B2_ULTIMA_PAGINA);
		$data['concluido'] = $data['ultima'] ? $data['proxima'] / $data['ultima'] * 100 : 0;
		$data['url_base'] = $this->configuracao_model->get(B2_URL_BASE);
		$data['ppm'] = $this->configuracao_model->get(B2_BAIXAR_PPM);

		/*****************************************
		 * PROCESSAR PÁGINAS
		 *****************************************/

		$data['p_proxima'] = $this->configuracao_model->get(P2_PROXIMA_PAGINA) - 1;
		$data['p_ultima'] = $this->configuracao_model->get(P2_ULTIMA_PAGINA);
		$data['p_concluido'] = $data['p_ultima'] ? $data['p_proxima'] / $data['p_ultima'] * 100 : 0;

		/*****************************************
		 * PROCESSAR PROVAS
		 *****************************************/
		$data['p_restantes'] = $this->prova_model->contar_provas_sem_escolaridade();

		$this->load->view('buscador2/header', $data);
		$this->load->view('buscador2/baixar', $data);
		$this->load->view('buscador2/footer', $data);		
	}

	public function mudar_modalidade($modalidade)
	{
		$this->configuracao_model->set(MODALIDADE, $modalidade);
		$this->configuracao_model->set(B2_URL_BASE, self::get_modalidade_url($modalidade));
		$this->configuracao_model->set(B2_PROXIMA_PAGINA, '1');
		$this->configuracao_model->set(P2_PROXIMA_PAGINA, '0');
		$this->configuracao_model->set(S2_LIBERA_AREAS, '0');
		$this->configuracao_model->set('area_atuacao_id', '1');
		$this->configuracao_model->set('area_atuacao_pg', '1');
		$this->configuracao_model->set('area_formacao_id', '1');
		$this->configuracao_model->set('area_formacao_pg', '1');

		$url = self::get_modalidade_url($modalidade) . '/questoes?page=1';
		$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
		$html = file_get_html($url, false, $context);

		$ultima = $html->find('.box-link', -1);

		if($ultima) {
			$this->configuracao_model->set(B2_ULTIMA_PAGINA, $ultima->plaintext);
		}
		else {
			self::mudar_modalidade($modalidade);
		}
	}

/*
	public function processar()
	{
		if($this->input->post('submit')) {
			$this->configuracao_model->set(P2_PROCESSAR_PPM, $this->input->post('ppm'));
			$this->configuracao_model->set(P2_PROXIMA_PAGINA, '1');
		}

		if($this->input->post('processar')) {
			$this->configuracao_model->set(P2_PROCESSAR_STATUS, '1');
		}

		if($this->input->post('parar')) {
			$this->configuracao_model->set(P2_PROCESSAR_STATUS, '0');
		}

		if($this->input->post('resetar')) {
			$this->configuracao_model->set(P2_PROXIMA_PAGINA, '1');
		}

		$data['status'] = $this->configuracao_model->get(P2_PROCESSAR_STATUS);
		$data['proxima'] = $this->configuracao_model->get(P2_PROXIMA_PAGINA);
		$data['ultima'] = $this->configuracao_model->get(B2_PROXIMA_PAGINA); // A última página a ser processada é a última página baixada
		$data['concluido'] = $data['ultima'] ? $data['proxima'] / $data['ultima'] * 100 : 0;
		$data['ppm'] = $this->configuracao_model->get(P2_PROCESSAR_PPM);

		$this->load->view('buscador2/header', $data);
		$this->load->view('buscador2/processar', $data);
		$this->load->view('buscador2/footer', $data);
	}

	public function provas()
	{
		if($this->input->post('submit')) {
			$this->configuracao_model->set(V2_PROCESSAR_PPM, $this->input->post('ppm'));
		}

		if($this->input->post('processar')) {
			$this->configuracao_model->set(V2_PROCESSAR_STATUS, '1');
		}

		if($this->input->post('parar')) {
			$this->configuracao_model->set(V2_PROCESSAR_STATUS, '0');
		}

		$data['status'] = $this->configuracao_model->get(V2_PROCESSAR_STATUS);
		$data['ppm'] = $this->configuracao_model->get(V2_PROCESSAR_PPM);
		$data['restantes'] = $this->prova_model->contar_provas_sem_escolaridade();

		$this->load->view('buscador2/header', $data);
		$this->load->view('buscador2/provas', $data);
		$this->load->view('buscador2/footer', $data);
	}
*/

	/**********************************************************************************************
	* CRONJOBS
	***********************************************************************************************/
	
	public function cronjobs()
	{
		echo "Variaveis:<br>";

		echo "MODALIDADE: " . $this->configuracao_model->get('MODALIDADE') . "<br>";
		echo "S2_LIBERA_AREAS: " . $this->configuracao_model->get('S2_LIBERA_AREAS') . "<br>";
		echo "B2_BAIXAR_ERROS: " . $this->configuracao_model->get('B2_BAIXAR_ERROS') . "<br>";
		echo "B2_BAIXAR_PPM: " . $this->configuracao_model->get('B2_BAIXAR_PPM') . "<br>";
		echo "B2_BAIXAR_STATUS: " . $this->configuracao_model->get('B2_BAIXAR_STATUS') . "<br>";
		echo "B2_PROXIMA_PAGINA: " . $this->configuracao_model->get('B2_PROXIMA_PAGINA') . "<br>";
		echo "B2_ULTIMA_PAGINA: " . $this->configuracao_model->get('B2_ULTIMA_PAGINA') . "<br>";
		echo "B2_URL_BASE: " . $this->configuracao_model->get('B2_URL_BASE') . "<br>";
		echo "P2_PROVAS_PENDENTES: " . $this->configuracao_model->get('P2_PROVAS_PENDENTES') . "<br>";
		echo "MP2_PROCESSAR_PPM: " . $this->configuracao_model->get('P2_PROCESSAR_PPM') . "<br>";
		echo "Ṕ2_PROCESSAR_STATUS: " . $this->configuracao_model->get('Ṕ2_PROCESSAR_STATUS') . "<br>";
		echo "P2_PROXIMA_PAGINA: " . $this->configuracao_model->get('P2_PROXIMA_PAGINA') . "<br>";
		echo "P2_ULTIMA_PAGINA: " . $this->configuracao_model->get('P2_ULTIMA_PAGINA') . "<br>";
		echo "V2_PROCESSAR_STATUS: " . $this->configuracao_model->get('V2_PROCESSAR_STATUS') . "<br>";
		echo "V2_PROCESSAR_PPM: " . $this->configuracao_model->get('V2_PROCESSAR_PPM') . "<br>";

		echo "Execundo cron...<br>";

		if($this->configuracao_model->get(B2_BAIXAR_STATUS)) {

			echo "Baixar ativado...<br>";

			
			if($this->configuracao_model->get(S2_LIBERA_AREAS) == '0') {
				echo "Libera areas desativado...<br>";
				echo "Baixando paginas...<br>";

				self::baixar_paginas_de_questoes();

				echo "Baixando paginas com erros...<br>";

				self::baixar_pagina_com_erro();

				echo "Processando paginas...<br>";
				self::processar_paginas();
			}

			else {

			}

			echo "Processando provas...<br>";
			self::processar_provas();
		}

		$b_proxima = $this->configuracao_model->get(B2_PROXIMA_PAGINA);
		$b_fim = $this->configuracao_model->get(B2_ULTIMA_PAGINA);

		$p_proxima = $this->configuracao_model->get(P2_PROXIMA_PAGINA);
		$p_fim = $this->configuracao_model->get(P2_ULTIMA_PAGINA);
		
		$erros = $this->configuracao_model->get(B2_BAIXAR_ERROS);
		
		if($b_proxima > $b_fim && $p_proxima > $p_fim && !$erros) {
			$this->configuracao_model->set(S2_LIBERA_AREAS, '1');
		}

		$atuacao = $this->configuracao_model->get('area_atuacao_pg');
		$formacao = $this->configuracao_model->get('area_formacao_pg');
		if($atuacao > 16 && $formacao > 324) {
			
			$this->configuracao_model->set(B2_PROXIMA_PAGINA, '1');
			$this->configuracao_model->set(P2_PROXIMA_PAGINA, '0');
			$this->configuracao_model->set(S2_LIBERA_AREAS, '0');
			$this->configuracao_model->set('area_atuacao_id', '1');
			$this->configuracao_model->set('area_atuacao_pg', '1');
			$this->configuracao_model->set('area_formacao_id', '1');
			$this->configuracao_model->set('area_formacao_pg', '1');

			$modalidade = $this->configuracao_model->get(MODALIDADE);
			$modalidade++;

			if($modalidade > 5) {
				$this->configuracao_model->set(MODALIDADE, '1');	
			}
			else {
				$this->configuracao_model->set(MODALIDADE, $modalidade);
			}

		} 
	}

	public function get_modalidade_url()
	{
		$modalidade = $this->configuracao_model->get(MODALIDADE);
		
		switch ($modalidade) {
			case '1': return "https://www.qconcursos.com/questoes-de-concursos";	break;
			case '2': return "https://www.qconcursos.com/questoes-da-oab";	break;
			case '3': return "https://www.qconcursos.com/questoes-do-enem";	break;
			case '4': return "https://www.qconcursos.com/questoes-militares";	break;
			case '5': return "https://www.qconcursos.com/questoes-de-concursos/";	break;
			default: return "https://www.qconcursos.com/questoes-de-concursos";	break;
		}
	}

	/**********************************************************************************************
	* FASE 1 - Baixa todas as páginas do Qcon
	***********************************************************************************************/

	public function baixar_paginas_de_questoes()
	{
		$proxima = $this->configuracao_model->get(B2_PROXIMA_PAGINA);
		$fim = $this->configuracao_model->get(B2_ULTIMA_PAGINA);

		echo "Baixando {$proxima} de {$fim}... <br>";

		if($proxima > $fim) {
			return;
		}

		$ppm = $this->configuracao_model->get(B2_BAIXAR_PPM);
		for ($pagina = $proxima; $pagina < $proxima+$ppm; $pagina++) {
			self::baixar_pagina($pagina);
			$this->configuracao_model->set(B2_PROXIMA_PAGINA, $pagina + 1);

			echo "Pagina baixada... <br>";
		}
	}

	public function baixar_pagina_com_erro()
	{
		if($erros = $this->configuracao_model->get(B2_BAIXAR_ERROS)) {
			$erros_a = unserialize($erros);
			$pagina = array_shift($erros_a);
			$erros = serialize($erros_a);
			$this->configuracao_model->set(B2_BAIXAR_ERROS, $erros);

			self::baixar_pagina($pagina);
		}
	}

	public function baixar_pagina($pagina)
	{
		set_time_limit(30);

		$url = self::get_modalidade_url() . '/questoes?page=' . $pagina;

		echo "Baixando página: " . $url . "<br>";

		$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
		$html = file_get_contents($url, false, $context);

		if(strpos($html, "</html>") === FALSE) {
			self::adicionar_pagina_com_erro($pagina);
		}

		file_put_contents(get_buscador_questoes_dir(). $pagina . ".html", $html);
	}

	public function adicionar_pagina_com_erro($pagina)
	{
		if($erros = $this->configuracao_model->get(B2_BAIXAR_ERROS)) {
			$erros_a = unserialize($erros);
		}
		else {
			$erros_a = array();
		}

		array_push($erros_a, $pagina);

		$erros = serialize($erros_a);
		$this->configuracao_model->set(B2_BAIXAR_ERROS, $erros);
	}
	
	/**********************************************************************************************
	* FASE 2 - Processa htmls baixados
	***********************************************************************************************/

	public function processar_paginas()
	{
		$proxima = $this->configuracao_model->get(P2_PROXIMA_PAGINA) ? $this->configuracao_model->get(P2_PROXIMA_PAGINA) : 1;

		echo "Definindo proxima pagina... {$proxima}<br>";
		// define a última

		$erros = $this->configuracao_model->get(B2_BAIXAR_ERROS);
		$erros_a = unserialize($erros);

		if($erros_a) {			
			echo "Ha pagina com erro = {$erros_a[0]}<br>";

			natsort($erros_a);

			$this->configuracao_model->set(P2_ULTIMA_PAGINA, $erros_a[0]-1);
		}
		else {
			echo "Nao ha pagina com erro<br>";

			$ultima = (int)$this->configuracao_model->get(B2_PROXIMA_PAGINA) - 1;
			$this->configuracao_model->set(P2_ULTIMA_PAGINA, $ultima);
		}
		echo "Definindo ultima pagina... {$proxima}<br>";
		
		$fim = $this->configuracao_model->get(P2_ULTIMA_PAGINA);

		if($proxima > $fim) {
			return;
		}

		$ppm = $this->configuracao_model->get(P2_PROCESSAR_PPM) ? $this->configuracao_model->get(P2_PROCESSAR_PPM) : 1;
		for ($pagina = $proxima; $pagina < $proxima+$ppm; $pagina++) {
			self::processar_pagina_de_questoes($pagina);
			self::atualizar_status($pagina);

			$this->configuracao_model->set(P2_PROXIMA_PAGINA, $pagina + 1);
		}
	}

	public function processar_pagina_de_questoes($pagina) 
	{
		echo "Processando pagina de questao... {$pagina}<br>";		

		$url = get_buscador_questoes_dir() . $pagina . ".html";

		echo "Abrindo página: {$url}<br>";		

		$html = file_get_html($url);

		for($i = 0; $i < 5; $i++) {
			$elemento = $html->find('.uma-questao-1', $i);
			
			if(!$elemento) {
				echo "Nao encontramos a questao {$i} da pagina {$pagina}<br>";	
				continue;
			}
	
			$qcon_id = substr(trim($elemento->find('.questao-numero', 0)->plaintext),1);

			// se a questão já está cadastrada no banco pula
			if($this->questao_model->get_by_qcon_id($qcon_id)) {
				echo "Questão QCon {$qcon_id} ja esta cadastrada no banco. Pulando ...<br>";	
				continue;
			}

			$nome_disciplina = trim($elemento->find('.disciplina-assunto div a', 0)->plaintext);

			echo "Processando questao {$i} da pagina {$pagina} : {$qcon_id}<br>";	
			
			$questoes_meta = $elemento->find('.uma-questao-topo-linha2');
			if(!$questoes_meta) {
				echo "Nao encontramos as meta informacoes da questao {$i} da pagina {$pagina}<br>";	
				continue;
			}
			
			$ano = trim($elemento->find('.uma-questao-topo-linha2 div', 0)->find('text',1)->plaintext);
			$nome_banca = trim($elemento->find('.uma-questao-topo-linha2 div', 1)->find('text',1)->plaintext);
			$nome_orgao = trim($elemento->find('.uma-questao-topo-linha2 div', 2)->find('text',1)->plaintext);
			$nome_prova = trim($elemento->find('.uma-questao-topo-linha2 div', 3)->find('a',0)->plaintext);
			$qcon_url = trim($elemento->find('.uma-questao-topo-linha2 div', 3)->find('a',0)->href);
	
			$disciplina = $this->disciplina_model->get_by_nome($nome_disciplina, true);
			
			// realiza a conversao de bancas antigas para as novas
			switch ($nome_banca) {
				case 'AOCP':
					$banca = $this->banca_model->get_by_id(128);
					break;

				case 'CONSULPAM':
					$banca = $this->banca_model->get_by_id(165);
					break;

				default:
					$banca = $this->banca_model->get_by_nome($nome_banca, true);
					break;
			}

			$orgao = $this->orgao_model->get_by_nome($nome_orgao, true);
			$prova = $this->prova_model->get($nome_prova, $ano, $banca['ban_id'], $orgao['org_id'], $qcon_url, true);
			$this->prova_model->atualizar_tipo($prova['pro_id'], self::get_prova_modalidade($qcon_url));

			$enunciado = $elemento->find('.enunciado', 0)->innertext;
			$opcoes_array = $elemento->find('.questoes-texto');
	
			$questoes_opcoes = array();
			foreach ($opcoes_array as $opcao) {
				array_push($questoes_opcoes, $opcao->innertext);
			}
			
			if(is_null($enunciado)) {
				echo "Nao encontramos o encunciado da questao {$i} da pagina {$pagina}<br>";	
				continue;
			}
			
			$questao = $this->questao_model->get(array(
					'que_qcon_id' 	=> $qcon_id,
					'que_enunciado' => $enunciado,
					// 'pro_id' => $prova['pro_id'],
					'que_tipo' => get_questao_tipo($questoes_opcoes),
					'dis_id' => $disciplina['dis_id'],
					'opcoes' => $questoes_opcoes) , true);

			$this->questao_model->salvar_prova_em_questao($prova, $questao);

			echo "Questao: ";
			print_r($questao);
			echo "<br>";

			$q = $this->questao_model->get_by_qcon_id($qcon_id);
			$this->questao_model->atualizar_campo_busca($q['que_id']);
			// $this->questao_model->atualizar_ativo_inativo($q['que_id'], NAO);

			$assuntos_questao = $elemento->find('.disciplina-assunto span[id^=primeiros_tres_assuntos_] a');
			
			foreach ($assuntos_questao as $assunto_nome) {
				$assunto_nome = trim($assunto_nome->innertext);

				$assunto = $this->assunto_model->get(array(
						'ass_nome'	=> $assunto_nome,
						'dis_id'	=> $disciplina['dis_id']
				), true);

				if($conversao = $this->assunto_model->get_conversao($assunto['ass_id'])) {
					$assunto = $this->assunto_model->get_by_id($conversao['ass_novo']);
				}
				
				$this->questao_model->salvar_assunto_em_questao($assunto, $questao);
			}
		}
	}

	public function get_prova_modalidade($qcon_url)
	{
		if(strpos($qcon_url, 'questoes-da-oab') !== FALSE) {
			return 2;
		}

		if(strpos($qcon_url, 'questoes-do-enem') !== FALSE) {
			return 3;
		}

		if(strpos($qcon_url, 'questoes-militares') !== FALSE) {
			return 4;
		}

		if(strpos($qcon_url, 'questoes-de-vestibular') !== FALSE) {
			return 5;
		}

		return 1;
	}

	public function atualizar_status($pagina)
	{
		$this->load->model('questao_model');

		$url = get_buscador_questoes_dir() . $pagina . ".html";
			
		$html = file_get_html($url);
			
		$elementos = $html->find('.questao-anulada');
		
		if(count($elementos) > 0) {
			foreach ($elementos as $elemento) {
				$pai = $elemento->parent();
				
				if($pai) {
					$pai_array = explode("-", $pai->id);
					
					$id = $pai_array[count($pai_array)-1];
					$this->questao_model->atualizar_status_por_qcon_id($id, CANCELADA);
					echo "Encontrada questao anulada - id: " . $id . "<br>"; 
				}
			}
		}
		else {
			echo "Nao encontramos nenhuma questao anulada<br>";
		}
		
		$elementos = $html->find('.questao-desatualizada');
			
		if(count($elementos) > 0) {
			foreach ($elementos as $elemento) {
				$pai = $elemento->parent();
					
				if($pai) {
					$pai_array = explode("-", $pai->id);
		
					$id = $pai_array[count($pai_array)-1];
					$this->questao_model->atualizar_status_por_qcon_id($id, DESATUALIZADA);
					echo "Encontrada questão desatualizada - id: " . $id;
				}
			}
		}
		else {
			echo "Nao encontramos nenhuma questao desatualizada<br>";
		}
			
	}

	/**********************************************************************************************	
	* FASE 3 - Baixa e processa provas
	***********************************************************************************************/

	public function processar_provas()
	{
		$ppm = $this->configuracao_model->get(V2_PROCESSAR_PPM) ? $this->configuracao_model->get(V2_PROCESSAR_PPM) : 1;

		$provas = $this->prova_model->listar_provas_sem_escolaridade($ppm);

		foreach ($provas as $prova) {
			self::processar_prova($prova['pro_id']);
		}
	}

	public function processar_prova($prova_id) 
	{
		self::salvar_pdfs_prova($prova_id);

		$escolaridade_id = self::get_escolaridade_prova($prova_id);
		$this->prova_model->atualizar_escolaridade($prova_id, $escolaridade_id);
	}

	private function get_conteudo_prova_qcon($prova_id)
	{
		$arquivo = get_buscador_provas_dir() . $prova_id . '.html';
		
		if(file_exists($arquivo)) {
			return file_get_html($arquivo);
		}
		else {
			$prova = $this->prova_model->get_by_id($prova_id);

			if($prova['pro_qcon_url']) {
				$url = "http://www.qconcursos.com" . $prova['pro_qcon_url'];
				
				$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
				$html = file_get_contents($url, false, $context);

				if($html) {
					file_put_contents($arquivo, $html);
				}
				else {
					$this->prova_model->atualizar_escolaridade($prova_id, -1);
					return null;	
				}
				
				return file_get_html($arquivo);
			}
			else {
				$this->prova_model->atualizar_escolaridade($prova_id, -1);
				return null;
			}
		}
	}

	private function get_escolaridade_prova($prova_id)
	{
		$html = self::get_conteudo_prova_qcon($prova_id);
		
		if($html) {
			$elements = $html->find('.categoria-txt-link .link-laranja');
			foreach ($elements as $element) {
				$url = $element->href;
				if(strpos($url, 'escolaridade_hidden') === false) continue;
				
				return substr($url, -1);
			}
		}

		return -1;
	}
	
	public function salvar_pdfs_prova($prova_id) 
	{
		$html = self::get_conteudo_prova_qcon($prova_id);
		
		if($html) {
			$arquivo = get_arquivo_edital($prova_id);
			if(!file_exists($arquivo)) {
				$url = $html->find('li[class=baixar-seta] a',2)->href;
				copy(self::preparar_url($url), $arquivo);
			}
			
			$arquivo = get_arquivo_gabarito($prova_id);
			if(!file_exists($arquivo)) {
				$url = $html->find('li[class=baixar-seta] a',1)->href;
				copy(self::preparar_url($url), $arquivo);
			}
			
			$arquivo = get_arquivo_prova($prova_id);
			if(!file_exists($arquivo)) {
				$url = $html->find('li[class=baixar-seta] a',0)->href;
				copy(self::preparar_url($url), $arquivo);
			}
		}
	}

	public function preparar_url($url)
	{
		if($url[0] == '/') {
			return 'https://www.qconcursos.com' . $url;
		}

		return $url;
	}
	
	/**********************************************************************************************	
	* FASE 4 - Areas de Atuação e formação
	***********************************************************************************************/

	public function area_atuacao()
	{
		$area_id = $this->configuracao_model->get_valor('area_atuacao_id', true);
		$page = $this->configuracao_model->get_valor('area_atuacao_pg', true);
		
		if($area_id > 16) exit;
		
		log_buscador("info", "***** Processando Area de Atuacao *****");
		log_buscador("info", "Recuperando conteudo da área [$area_id], página[$page]");
		$url = self::get_modalidade_url() . "/provas/search?utf8=%E2%9C%93&area_atuacao_hidden={$area_id}&page={$page}";

		$html = file_get_html($url);
		$elementos = $html->find('.txt-resultado-titulo a');
			
		if(!$elementos) {
			log_buscador("info", "Nao existem mais provas para a área [$area_id].");

			$configuracao = array('cfg_nome' => 'area_atuacao_id', 'cfg_valor' => $area_id + 1);
			$this->configuracao_model->atualizar($configuracao);

			$configuracao = array('cfg_nome' => 'area_atuacao_pg', 'cfg_valor' => 1);
			$this->configuracao_model->atualizar($configuracao);

			exit;
		}
			
		foreach ($elementos as $elemento) {
			$prova_url = get_qcon_prova_url($elemento->href, true);
			$prova = $this->prova_model->get_by_url($prova_url);

			log_buscador("info", "Prova url [$area_id], página[$page] : $prova_url");

			if(!$prova) {
				log_buscador("info", "Essa prova nao esta cadastrada na Exponencial");
				continue;
			}
			else {
				log_buscador("info", "Prova encontrada na Exponencial: {$prova['pro_id']}");
				$area = $this->area_atuacao_model->get_by_id($area_id);
					
				log_buscador("info", "Procurando area de atuacao [$area_id]");
				// cadastra área se não encontrar no banco
				if(!$area) {
					log_buscador("info", "Area de atuacao [$area_id] nao encontrada ");

					$nome = $html->find('#lista_itens li', 0)->innertext;

					$data_area = array(
							'ara_id' => $area_id,
							'ara_nome' => trim($nome)
					);

					log_buscador("info", "Salvando area de atuacao [$area_id] [$nome]");
					$this->area_atuacao_model->salvar($data_area);
				}
					
				log_buscador("info", "Adicionando area de atuacao [$area_id] na prova [{$prova['pro_id']}]");
				$this->prova_model->adicionar_area_atuacao($prova['pro_id'], $area['ara_id']);
			}
		}
			
		$configuracao = array('cfg_nome' => 'area_atuacao_pg', 'cfg_valor' => $page + 1);
		$this->configuracao_model->atualizar($configuracao);
	}
	
	public function area_formacao()
	{
		$area_id = $this->configuracao_model->get_valor('area_formacao_id', true);
		$page = $this->configuracao_model->get_valor('area_formacao_pg', true);

		if($area_id > 324) exit;
		
		log_buscador("info", "***** Processando Area de Formacao *****");
		log_buscador("info", "Recuperando conteudo da área [$area_id], página[$page]");
		$url = self::get_modalidade_url() . "/provas/search?utf8=%E2%9C%93&area_formacao_hidden={$area_id}&page={$page}";

		$html = file_get_html($url);
		$elementos = $html->find('.txt-resultado-titulo a')
;
		if(!$elementos) {
			log_buscador("info", "Nao existem mais provas para a área [$area_id].");

			$configuracao = array('cfg_nome' => 'area_formacao_id', 'cfg_valor' => $area_id + 1);
			$this->configuracao_model->atualizar($configuracao);

			$configuracao = array('cfg_nome' => 'area_formacao_pg', 'cfg_valor' => 1);
			$this->configuracao_model->atualizar($configuracao);

			exit;
		}

		foreach ($elementos as $elemento) {
			$prova_url = get_qcon_prova_url($elemento->href, true);
			$prova = $this->prova_model->get_by_url($prova_url);

			log_buscador("info", "Prova url [$area_id], página[$page] : $prova_url");

			if(!$prova) {
				log_buscador("info", "Essa prova nao esta cadastrada na Exponencial");
				continue;
			}
			else {
				log_buscador("info", "Prova encontrada na Exponencial: {$prova['pro_id']}");
				$area = $this->area_formacao_model->get_by_id($area_id);

				log_buscador("info", "Procurando area de formacao [$area_id]");
				// cadastra área se não encontrar no banco
				if(!$area) {
					log_buscador("info", "Area de formacao [$area_id] nao encontrada ");

					$nome = $html->find('#lista_itens li', 0)->innertext;

					$data_area = array(
							'arf_id' => $area_id,
							'arf_nome' => trim($nome)
					);

					log_buscador("info", "Salvando area de formacao [$area_id] [$nome]");
					$this->area_formacao_model->salvar($data_area);
				}

				log_buscador("info", "Adicionando area de formacao [$area_id] na prova [{$prova['pro_id']}]");
				$this->prova_model->adicionar_area_formacao($prova['pro_id'], $area['arf_id']);
			}
		}

		$configuracao = array('cfg_nome' => 'area_formacao_pg', 'cfg_valor' => $page + 1);
		$this->configuracao_model->atualizar($configuracao);
	}

	// public function adicionar_prova_pendente($prova_id)
	// {
	// 	if($provas = $this->configuracao_model->get(P2_PROVAS_PENDENTES)) {
	// 		$provas_a = unserialize($provas);

	// 		if(!in_array($prova_id, $provas_a)) {
	// 			array_push($provas_a, $prova_id);
	// 		}
	// 		// se a prova já estiver no array, basta ignorar.
	// 		else {
	// 			return;
	// 		}
	// 	}
	// 	else {
	// 		$provas_a = array();
	// 		array_push($provas_a, $prova_id);
	// 	}

	// 	$this->configuracao_model->set(P2_PROVAS_PENDENTES, serialize($provas_a));

	// }

	/*******************************************************************************/
	/*******************************************************************************/
	/*******************************************************************************/
	/*******************************************************************************/
	/*******************************************************************************/


	
	public function qconcursos($ultima_pagina = 83909, $configuracao_qcon_proxima_pagina = 'parse0_10') 
	{
		echo "Script iniciado <br><br>";
		
		for($z = 0; $z < 30; $z++) {
			echo "z = " . $z . "<br>";
			echo "Iniciando leitura de pagina<br>";
			ob_flush();
			flush();
			
			$pagina = $this->configuracao_model->get_valor($configuracao_qcon_proxima_pagina, true);
				
			echo "Pagina a ser lida: $pagina<br>";
			ob_flush();
			flush();
		
			if($pagina > $ultima_pagina) exit;
				
			// 			$url = "https://www.qconcursos.com/questoes-de-concursos/questoes/search?utf8=%E2%9C%93&q=&orgao=&banca=&ano=&cargo=&escolaridade=&modalidade=&disciplina=" . $disciplina_qcon_id . "&assunto=&area_atuacao=&area_formacao=&nivel_dificuldade=&periodo_de=&periodo_ate=&possui_gabarito_comentado_texto_e_video=&possui_comentarios_gerais=&possui_comentarios=&possui_anotacoes=&sem_dos_meus_cadernos=&sem_anuladas=&sem_desatualizadas=&sem_anuladas_impressao=&sem_desatualizadas_impressao=&prova=&caderno_id=&migalha=&data_comentario_texto=&data=&r=&rc=&re=&nao_resolvidas=&page=" . $pagina;
			$url = get_buscador_questoes_dir() . $pagina . ".html";
				
			echo "Abrindo url: {$url}<br>";
			ob_flush();
			flush();
			
			$html = file_get_html($url);
			
			for($i = 0; $i < 5; $i++) {
				$elemento = $html->find('.uma-questao-1', $i);
				
				if(!$elemento) continue;
		
				$qcon_id =  substr(trim($elemento->find('.questao-numero', 0)->plaintext),1);
				
				echo "Lendo questao {$qcon_id}<br>";
				ob_flush();
				flush();
				
				$nome_disciplina = trim($elemento->find('.disciplina-assunto div a', 0)->plaintext);
				
				$questoes_meta = $elemento->find('.uma-questao-topo-linha2');
				if(!$questoes_meta) continue;
				
				$ano = trim($elemento->find('.uma-questao-topo-linha2 div', 0)->find('text',1)->plaintext);
				$nome_banca = trim($elemento->find('.uma-questao-topo-linha2 div', 1)->find('text',1)->plaintext);
				$nome_orgao = trim($elemento->find('.uma-questao-topo-linha2 div', 2)->find('text',1)->plaintext);
				$nome_prova = trim($elemento->find('.uma-questao-topo-linha2 div', 3)->find('a',0)->plaintext);
				$qcon_url = trim($elemento->find('.uma-questao-topo-linha2 div', 3)->find('a',0)->href);
		
				$disciplina = $this->disciplina_model->get_by_nome($nome_disciplina, true);
				$banca = $this->banca_model->get_by_nome($nome_banca, true);
				$orgao = $this->orgao_model->get_by_nome($nome_orgao, true);
				$prova = $this->prova_model->get($nome_prova, $ano, $banca['ban_id'], $orgao['org_id'], $qcon_url, true);
		
				$escolaridade_id = self::get_escolaridade_prova($prova['pro_id']);
				$this->prova_model->atualizar_escolaridade($prova['pro_id'], $escolaridade_id);
				
				echo "Salvando pdfs<br>";
				ob_flush();
				flush();
				
				self::salvar_pdfs_prova($prova['pro_id']);
		
				$enunciado = $elemento->find('.enunciado', 0)->innertext;
				$opcoes_array = $elemento->find('.questoes-texto');
		
				echo "Lendo opcoes<br>";
				ob_flush();
				flush();
				
				$questoes_opcoes = array();
				foreach ($opcoes_array as $opcao) {
					array_push($questoes_opcoes, $opcao->innertext);
				}
				
				echo "Lendo status<br>";
				ob_flush();
				flush();
		
				$status = self::get_status($elemento);
		
				echo "Salvando questao<br>";
				ob_flush();
				flush();
				
				if(is_null($enunciado)) {
					echo "Enunciado nulo... Pulando questao<br>";
					ob_flush();
					flush();
					continue;
				}
				
				$questao = $this->questao_model->get(array(
						'que_qcon_id' 	=> $qcon_id,
						'que_enunciado' => $enunciado,
						'pro_id' => $prova['pro_id'],
						'que_tipo' => get_questao_tipo($questoes_opcoes),
						'dis_id' => $disciplina['dis_id'],
						'que_status' => $status,
						'opcoes' => $questoes_opcoes) , true);
		
				$assuntos_questao = $elemento->find('.disciplina-assunto span[id^=primeiros_tres_assuntos_] a');
				
				$num_assuntos = count($assuntos_questao);
				echo "Lendo assuntos ($num_assuntos)<br>";
				ob_flush();
				flush();
				
				foreach ($assuntos_questao as $assunto_nome) {
						
					$assunto_nome = trim($assunto_nome->innertext);
						
					$assunto = $this->assunto_model->get(array(
							'ass_nome'	=> $assunto_nome,
							'dis_id'	=> $disciplina['dis_id']
					), true);
					
					echo "Salvando assunto: {$assunto_nome}<br>";
					ob_flush();
					flush();
					
					$this->questao_model->salvar_assunto_em_questao($assunto, $questao);
				}
			}
			
			echo "Atualizando configuracoes<br>";
			ob_flush();
			flush();
				
			$configuracao = array('cfg_nome' => $configuracao_qcon_proxima_pagina, 'cfg_valor' => $pagina + 1);
			$this->configuracao_model->atualizar($configuracao);
			
			echo "Pagina encerrada<br><br>********************************************<br><br>";
			ob_flush();
			flush();
		}
		
		echo "Script encerrado<br><br>";
	}
	
	
	public function salvar_usuario_buscador()
	{
		$dados = array(
				'bus_email' => $this->input->post('email'),
				'bus_senha' => $this->input->post('senha')
			);
			
		$this->db->insert('buscador_usuarios', $dados);
	}
	
// 	private function get_conteudo_prova_qcon($prova_id)
// 	{
// 		$prova = $this->prova_model->get_by_id($prova_id);
// 		$url = "http://www.qconcursos.com" . $prova['pro_qcon_url'];
		
// 		return file_get_html($url);
// 	}
	
	

	
	public function abrir_edital($prova_id)
	{
		$arquivo = get_arquivo_edital($prova_id);
		if(file_exists($arquivo)) {
			self::abrir_pdf($arquivo);
		}
		else {
			$html = self::get_conteudo_prova_qcon($prova_id);
			$url = $html->find('li[class=baixar-seta] a',2)->href;
			copy($url, $arquivo);
		
			self::abrir_pdf($arquivo);
		}
	} 
	
	public function abrir_gabarito($prova_id)
	{
		$arquivo = get_arquivo_gabarito($prova_id);
		if(file_exists($arquivo)) {
			self::abrir_pdf($arquivo);
		}
		else {
			$html = self::get_conteudo_prova_qcon($prova_id);
			$url = $html->find('li[class=baixar-seta] a',1)->href;
			copy($url, $arquivo);
	
			self::abrir_pdf($arquivo);
		}
	}
	
	public function abrir_prova($prova_id)
	{
		$arquivo = get_arquivo_prova($prova_id);
		if(file_exists($arquivo)) {
			self::abrir_pdf($arquivo);
		}
		else {
			$html = self::get_conteudo_prova_qcon($prova_id);
			$url = $html->find('li[class=baixar-seta] a',0)->href;
			copy($url, $arquivo);
	
			self::abrir_pdf($arquivo);
		}
	}
	
	public function abrir_pdf($arquivo) 
	{
		$fp= fopen($arquivo, "r");
	
		header("Cache-Control: maxage=1");
		header("Pragma: public");
		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename=".$arquivo."");
		header("Content-Description: PHP Generated Data");
		header("Content-Transfer-Encoding: binary");
		header('Content-Length:' .filesize($arquivo));
		ob_clean();

		flush();
		
		while (!feof($fp)){
			$buff = fread($fp,1024);
			print $buff;
		}
		exit;
	}
	
	public function listar_questoes_sem_gabarito()
	{
		$questoes = $this->questao_model->listar_questoes_sem_gabarito();
		
		$retorno = array();
		
		foreach ($questoes as $questao) {
			
			array_push($retorno, implode(",", array(
				$questao['que_qcon_id'],
				is_null($questao['que_texto_base']) ? 1 : 0
			)));
		}
		
		echo json_encode($retorno);
	}
	
	public function listar_questoes_com_gabarito_sem_texto_base()
	{
		$questoes = $this->questao_model->listar_questoes_com_gabarito_sem_texto_base();
	
		$retorno = array();
	
		foreach ($questoes as $questao) {
				
			array_push($retorno, implode(",", array(
					$questao['que_qcon_id']
			)));
		}
	
		echo json_encode($retorno);
	}
	
	public function get_questao_sem_gabarito()
	{
		$questao = $this->questao_model->get_questao_sem_gabarito();
	
		echo $questao['que_qcon_id'];
	}
	
	public function recuperar_texto_base($qcon_id)
	{
		$questao = $this->questao_model->get_by_qcon_id($qcon_id);
		echo is_null($questao['que_texto_base']) ? 1 : 0;
	}
	
	public function salvar_gabarito($qcon_id, $resposta)
	{
		$questao = $this->questao_model->get_by_qcon_id($qcon_id);
		$this->questao_model->atualizar_resposta($questao['que_id'], $resposta);
	}
	
	public function salvar_texto_base($qcon_id)
	{
		$questao = $this->questao_model->get_by_qcon_id($qcon_id);
		
		$texto_base = $this->input->post('texto_base');
	
		$this->questao_model->atualizar_texto_base($questao['que_id'], $texto_base);
	}
	
	public function resetar_usuarios_buscador()
	{
		$this->buscador_model->resetar_usuarios_buscador();
	}
	
	public function marcar_usuario_buscador_como_nao_disponivel($usuario_buscador_id)
	{
		$this->buscador_model->marcar_usuario_buscador_como_nao_disponivel($usuario_buscador_id);
	}
	
	public function get_usuario_buscador_disponivel()
	{
		$usuario_buscador = $this->buscador_model->get_usuario_buscador_disponivel();
		
		if($usuario_buscador) {
			echo implode(",", array(
				$usuario_buscador['bus_id'],
				$usuario_buscador['bus_email'],
				$usuario_buscador['bus_senha'],
			));
		}
		else {
			echo "";
		}
		
	}
	
	public function usuario_buscador()
	{
		if($this->input->post()) {
			$dados = array(
				'bus_email' => $this->input->post('email'),
				'bus_senha' => $this->input->post('senha')
			);
			
			$this->db->insert('buscador_usuarios', $dados);
		}
		
		$data['buscadores'] = $this->buscador_model->listar_todos();
		$data['num_disponiveis'] = $this->buscador_model->contar_disponiveis();
		
		
		$this->load->view('buscador/usuario_buscador', $data);
	}
	
	public function teste() {
		$url = "https://www.qconcursos.com/questoes-de-concursos/provas/search?utf8=%E2%9C%93&area_atuacao_hidden={$area_id}&page={$page}";
		
		$html = file_get_html($url);
		
		print_r($html->find('#lista_itens', 0));
	}
}
?>