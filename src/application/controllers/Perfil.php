<?php
class Perfil extends CI_Controller {

	public function __construct() 
	{
		parent::__construct();
		session_start();
	}
	
	public function index($usuario_id = null) 
	{
		$data['usuario'] = get_usuario_array($usuario_id ? $usuario_id : get_current_user_id());

		if($is_professor = is_professor(get_current_user_id(), TRUE)){
			KLoader::model("ProfessorModel");
			$professor = ProfessorModel::get_dados_professor(get_current_user_id());
			$data['professor'] = $professor;
			$data['include_summernote'] = TRUE;
		}
		
		$data['is_professor'] = $is_professor;
		
		$this->load->view(get_perfil_view_url(), $data);
	}
	
	public function editar() 
	{
		redirecionar_se_nao_estiver_logado();
		
		$data['from_checkout'] = (isset($_GET['from']) && $_GET['from'] == 'checkout') ? true : false;
		$data['perfil_basico'] = (isset($_GET['perfil_basico']) && $_GET['perfil_basico'] == 1) ? true : false;
		
		if($this->input->post()) {
			
			$this->form_validation->set_rules('nome', 'Nome', 'required');
			$this->form_validation->set_rules('sobrenome', 'Sobrenome', 'required');
			$this->form_validation->set_rules('data_nascimento', 'Data de Nascimento', 'required');
			
			if(!$data['perfil_basico']) {
				$this->form_validation->set_rules('cpf', 'CPF', 'required');
				$this->form_validation->set_rules('endereco', 'Endereço', 'required');
				$this->form_validation->set_rules('cep', 'CEP', 'required');
				$this->form_validation->set_rules('telefone', 'Telefone', 'required');
				$this->form_validation->set_rules('cidade', 'Cidade', 'required');
				$this->form_validation->set_rules('estado', 'Estado', 'required');
			}
				
			if ($this->form_validation->run () === true) {
				
				$nome = explode(' ', $this->input->post('nome_completo'));

				$usuario = array(
					'id' => get_current_user_id(),
					'nome_completo' => $this->input->post('nome') . " " . $this->input->post('sobrenome'),
					'nome' => $this->input->post('nome'),
					'sobrenome' => $this->input->post('sobrenome'),
					'cpf' => $this->input->post('cpf'),
					'data_nascimento' => $this->input->post('data_nascimento'),
					'endereco' => $this->input->post('endereco'),
					'cidade' => $this->input->post('cidade'),
					'estado' => $this->input->post('estado'),
					'cep' => $this->input->post('cep'),
					'telefone' => $this->input->post('telefone'),
					'bairro' => $this->input->post('bairro'),
					'apelido' => $this->input->post('apelido'),
					'mostrar_apelido' => $this->input->post('mostrar_apelido'),
					'email' => $this->input->post('email'),
					'codigo_desconto' => $this->input->post('codigo_desconto')
				);

				$usuario_antes = get_usuario_array();
				atualizar_usuario($usuario);
				atualizar_login($usuario_antes, $this->input->post('email'), $data['from_checkout']);

				if($is_professor = is_professor(get_current_user_id(), TRUE)){
					
					$professor = array();
					$professor['mini_cv'] = $this->input->post('mini_cv');
					$professor['descricao'] = $this->input->post('descricao');
					$professor[FACEBOOK] = $this->input->post(FACEBOOK);
					$professor[INSTAGRAM] = $this->input->post(INSTAGRAM);
					$professor[YOUTUBE] = $this->input->post(YOUTUBE);
					
					KLoader::model("ProfessorModel");
					ProfessorModel::atualizar_dados_professor(get_current_user_id(), $professor);
				}

				if($data['from_checkout']) {
					redirect('/checkout');
				}
				else {
					redirect(get_perfil_url());
				}

			}	
		}

		if($is_professor = is_professor(get_current_user_id(), TRUE)){
			KLoader::model("ProfessorModel");
			$professor = ProfessorModel::get_dados_professor(get_current_user_id());
			$data['professor'] = $professor;
			$data['include_summernote'] = TRUE;
		}
		
		$data['is_professor'] = $is_professor;
		$data['usuario'] = get_usuario_array(get_current_user_id());
		$data['estados_combo'] = get_estados_array();
		$data['mostrar_apelido_selecionado'] = $data['usuario']['mostrar_apelido'];
		$data['estado_selecionado'] = $data['usuario']['estado'] ?: null;
		$data['cidades_combo'] = get_cidades_array($data['estado_selecionado']);
		$data['cidade_selecionada'] = $data['usuario']['cidade'] ?: null;
		
		$this->load->view(get_editar_perfil_view_url(), $data);
	}
	
	public function recuperar_senha()
	{
		$data['usuario'] = get_usuario_array(get_current_user_id());
		recuperar_senha($data['usuario']['email']);
		
		$this->load->view('perfil/recuperar_senha', $data);
	}
	
	public function alterar_foto()
	{
		redirecionar_se_nao_estiver_logado();
		
		//$this->load->library('VerotUpload');
		
		$dir_dest = $_SERVER['DOCUMENT_ROOT'] . '/questoes/uploads/fotos/';
		$file_name = md5(get_current_user_id()); 
		
		$verot = new VerotUpload($_FILES['userfile']);
		
		if ($verot->uploaded) {
		
			$verot->image_resize = true;
			$verot->image_ratio_crop = true;
			$verot->image_y = 128;
			$verot->image_x = 128;
			$verot->file_new_name_body = $file_name;
			$verot->file_new_name_ext = 'jpg';
			$verot->file_overwrite = true;
			$verot->process($dir_dest);
			
			if ($verot->processed) {
				$verot->clean();
				set_mensagem(SUCESSO, 'Imagem alterada com sucesso.');
				update_user_meta(get_current_user_id(), 'foto_alterada', 1);
			} else {
				set_mensagem(SUCESSO, 'Erro ao carregar imagem. Tente novamente.');
				log_debug('error', $verot->log);
			}
			
		} else {
			set_mensagem(ERRO, 'Erro ao carregar imagem. Tente novamente.');
			log_debug('error', $verot->log);
		}
		 
		redirect(base_url('/perfil'));
	}

	public function configuracoes()
	{
		$this->load->model('configuracao_usuario_model');
		$this->load->library("form_validation");

		$data['include_icheck'] = true;

		$conf_usuario = $this->configuracao_usuario_model->get_config_usuario_by_id(get_current_user_id());

		$data['check_acertos'] = $conf_usuario['mostrar_acertos'];
		$data['check_filtros'] = $conf_usuario['exibir_filtros'];
		$data['check_historico'] = $conf_usuario['manter_historico_marcacao_cadernos'];
		$data['check_url_cad'] = $conf_usuario['exibir_url_imprimir_caderno'];
		$data['check_assuntos'] = $conf_usuario['exibir_assuntos'];
		$data['cabecalho_ordem'] = $conf_usuario['ordem_cabecalho'];

		if($this->input->post('check-salvar'))
		{
			$configuracoes['user_id'] = get_current_user_id();
			$configuracoes['mostrar_acertos'] = $this->input->post('check-acertos')?: 0;
			$configuracoes['exibir_filtros'] = $this->input->post('check-filtros')?: 0;
			$configuracoes['manter_historico_marcacao_cadernos'] = $this->input->post('check-historico')?: 0;
			$configuracoes['exibir_url_imprimir_caderno'] = $this->input->post('check-url-cad')?: 0;
			$configuracoes['exibir_assuntos'] = $this->input->post('check-assuntos')?: 0;
			$configuracoes['ordem_cabecalho'] = $this->input->post('cabecalho_ordem');
			
			if(!empty($conf_usuario['user_id'])){
				$this->configuracao_usuario_model->atualizar($configuracoes);
			}
			else{
				$this->configuracao_usuario_model->salvar($configuracoes);
			}
			
			set_mensagem(SUCESSO, "Configurações salvas com sucesso.");

			redirect('questoes/perfil/configuracoes');
		}

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view('perfil/configuracao/form_configuracao_usuario', $data);
		$this->load->view(get_main_footer_view_url(), $data);
	}

}
?>