<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjobs extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('America/Sao_Paulo');
	}
	
	public function processar_questoes_respondidas($inicio = "2019-02-14", $fim = "2019-10-31")
	{
		echo "Início: {$inicio} / Fim: {$fim}<br/>";
		$inicio = strtotime( "{$inicio} 12:00" );
		$fim = strtotime(date("{$fim} 12:00"));
		
		// Loop between timestamps, 24 hours at a time
		for ( $i = $inicio; $i <= $fim; $i = $i + 86400 ) {
			$thisDate = date( 'Y-m-d', $i ); // 2010-05-01, 2010-05-02, etc
			
			echo $thisDate . "<br>";
			
			self::processar_questoes_respondidas_dia($thisDate);			
		}
	}
	
	// public function processar_pedidos($dia)
	// {
	// 	set_time_limit(360);
		
	// 	$inicio = strtotime($dia . ' 12:00' );
	// 	$fim = strtotime(date('Y-m-d 12:00'));
	
	// 	for ( $i = $inicio; $i <= $fim; $i = $i + 86400 ) {
	// 		$thisDate = date( 'Y-m-d', $i ); // 2010-05-01, 2010-05-02, etc
				
	// 		echo $thisDate . "<br>";
				
	// 		self::processar_pedidos_do_dia($thisDate);
	// 	}
	// }
	
	public function processar_questoes_respondidas_dia($dia = FALSE)
	{
		$this->load->model('relatorios_model');
		$this->load->model('comentario_model');
		
		if(!$dia){
			$dia = date("Y-m-d");
			//$dia = "2016-02-02";			
		}
		else{
			$dia = date("Y-m-d", strtotime($dia));
		}
		$ontem = date("Y-m-d", strtotime($dia . ' -1 day'));
		
		if(! $this->relatorios_model->existe_relatorio_ontem($ontem)){
			$comentarios_agrupados = $this->comentario_model->get_total_comentarios_dia_by_userid($ontem);
			if( $comentarios_agrupados){
				$this->relatorios_model->inserir_questoes_respondidas_dia($comentarios_agrupados);
			}
		}
				
		$comentarios_agrupados = $this->comentario_model->get_total_comentarios_dia_by_userid($dia);
		if( $comentarios_agrupados){
			$this->relatorios_model->inserir_questoes_respondidas_dia($comentarios_agrupados);
		}
	}
	
	public function processar_questoes_respondidas_ontem()
	{
		$this->load->model('relatorios_model');
		$this->load->model('comentario_model');
	
		$ontem = date("Y-m-d", strtotime(' -1 day'));
		
		log_cron("debug", "Processando questões respondidas de " . $ontem);
	
		if(! $this->relatorios_model->existe_relatorio_ontem($ontem)){
			
			$comentarios_agrupados = $this->comentario_model->get_total_comentarios_dia_by_userid($ontem);
			if( $comentarios_agrupados){
				$this->relatorios_model->inserir_questoes_respondidas_dia($comentarios_agrupados);
				log_cron("debug", "Gravando dados de questões respondidas de " . $ontem);
			}
			else {
				log_cron("debug", "Ninguém comentou em " . $ontem);
			}
		}
		else {
			log_cron("error", "Já existe relatório para o dia " . $ontem);
		}
	}
	
	public function processar_questoes_respondidas_mes()
	{
		$this->load->model('relatorios_model');
		
		$mes = date("m", strtotime('-1 month'));
		$ano = date("Y", strtotime('-1 month'));
		
		$primeiro_dia_mes = date("Y-m-d", strtotime($ano."-".$mes."-01"));
		$ultimo_dia_mes = (int) date('t', strtotime($primeiro_dia_mes));
		
		$this->relatorios_model->excluir_questoes_respondidas_mes($mes);
		
		for($x = 1; $x <= $ultimo_dia_mes; $x++){
			self::processar_questoes_respondidas_dia($ano."-".$mes."-".$x);
		}
	}
	
	public function atualizar_assinaturas_sq()
	{
		$validades = listar_assinaturas_sq_validades();
		
		foreach ($validades as $validade) {
			$dias = timestamp_em_dias(strtotime($validade->meta_value) - time());
			
			if($dias < 0) {
				update_post_meta($validade->user_id, ASSINANTE_SQ_META, ASSINANTE_SQ_ATRASADO);
				
				$usuario = get_usuario_array($validade->user_id);
				$titulo = "Sistema de Questões Exponencial Concursos - Acesso Expirado";
				$mensagem = get_template_email ('sq-expirado.php', array (
						'nome' => $usuario['nome_completo']
				) );
				//enviar_email ( $usuario['email'], $titulo, $mensagem );
			}
			
			if($dias == 20) {
				$usuario = get_usuario_array($validade->user_id);
				$titulo = "Sistema de Questões Exponencial Concursos - Acesso expirando em 20 dias";
				$mensagem = get_template_email ('sq-20-dias.php', array (
						'nome' => $usuario['nome_completo']
				) );
				//enviar_email ( $usuario['email'], $titulo, $mensagem );
			}
			
			if($dias == 10) {
				$usuario = get_usuario_array($validade->user_id);
				$titulo = "Sistema de Questões Exponencial Concursos - Acesso expirnado em 10 dias";
				$mensagem = get_template_email ('sq-10-dias.php', array (
						'nome' => $usuario['nome_completo']
				) );
				//enviar_email ( $usuario['email'], $titulo, $mensagem );
			}
		}
	}

	public function processar_pedidos_de_ontem()
	{
		$ontem = date('Y-m-d', strtotime('-1 day'));

		self::processar_pedidos_do_dia($ontem);
	}
	
	public function processar_pedidos_do_dia($dia = null)
	{
		$this->load->model('relatorios_model');
		
		if(is_null($dia)) {
			$dia = date('Y-m-d');
		}

		$pedidos = listar_pedidos_por_periodo($dia, $dia);
		$total_receita = 0;
		$total_pagseguro = 0;
		$total_imposto = 0;
		
		foreach ($pedidos as $pedido){
			if($pedido->status != 'completed') continue;

			$taxa_total_pedido = get_post_meta($pedido->id, 'PagSeguro Total Taxes', true);
			
			$total_pedido = $pedido->get_total();
			$total_desconto = $pedido->get_total_discount();
			$total = $total_pedido + $total_desconto;

			$itens = $pedido->get_items();
			foreach ($itens as $item){

				if(is_produto_assinatura($item['product_id'])){
					$percentual_item = ($total > 0) ? $item['line_subtotal'] / $total : 0;
					$desconto = $total_desconto * $percentual_item;
					$total_receita += $item['line_subtotal'] - $desconto;
					$total_pagseguro += $taxa_total_pedido * $percentual_item;
					$total_imposto += ($item['line_subtotal'] - $desconto) * get_imposto_por_data($dia) / 100;
				}
			}
		}
		
		$this->relatorios_model->salvar_total_pedidos_dia($dia, $total_receita, $total_pagseguro, $total_imposto);
	}

	public function processar_pedidos($ano, $mes)
	{
	    // Turn off output buffering
	    ini_set('output_buffering', 'off');
	    // Turn off PHP output compression
	    ini_set('zlib.output_compression', false);
	    
	    //Flush (send) the output buffer and turn off output buffering
	    while (@ob_end_flush());
	    
		$inicio = "$ano-$mes-01";
		$ultimo = date('Y-m-t', strtotime($inicio));
		
		echo "Reprocessamento de relatório iniciado...<br>";
		echo "Aguarde. Poderá levar alguns minutos...";
		echo str_pad("",1024," ");
		echo " <br />";
		ob_flush();
		flush();

		while (true) {
			
		    echo "Processando dia: " . date("d/m/Y", strtotime($inicio)) . "...";
			echo str_pad("",1024," ");
			echo " <br />";
			ob_flush();
			flush();

			set_time_limit(300);
			self::processar_pedidos_do_dia($inicio);

			if($inicio == $ultimo) {
	
			    echo "Script finalizado...";
			    ob_flush();
			    flush();
				break;
			}

			$inicio = date("Y-m-d", strtotime($inicio . '+1 day'));
		}

		if($r = $_GET['r']) {
		    
		    $url = urldecode($r);
		    echo $url . "<br>";
		    echo "Redirecionando...";
		    ob_flush();
		    flush();
		    
		    echo "<script>window.location.href='$url'</script>";
		}
	}

	public function processar_acumulados_mes_passado()
	{
		$passado = date('Y-m-d', strtotime('-1 month'));

		$passado_a = explode('-', $passado);

		self::processar_acumulados($passado_a[1], $passado_a[0]);
	}	

	public function processar_acumulados_todos()
	{
		$inicio = "2016-01-01";
		$fim = date('Y-m-01', strtotime('-1 month'));

		while($inicio < $fim) {
			set_time_limit(360);
			
			$inicio_a = explode('-', $inicio);
			
			echo "Processando data $inicio<br>";

			self::processar_acumulados($inicio_a[1], $inicio_a[0]);

			$inicio = date('Y-m-01', strtotime($inicio . " +1 month"));
		}
	}	

	public function processar_acumulados($mes, $ano)
	{
		$this->load->model('relatorios_model');

		$this->load->helper(array('math', 'relatorio'));

		$totais = $this->relatorios_model->get_totais_pagseguro($mes, $ano);

		$questoes_comentadas = $this->relatorios_model->get_questoes_comentadas($mes, $ano);

		$total_coments = 0;

		$min_pagamento = get_minimo_valor_acumulado_por_data($ano . " - " . ($mes < 10 ? "0" . $mes : $mes) - "-01", VMA_SQ);

		foreach ($questoes_comentadas as $coments){
			$total_coments += $coments['qcp_qtde_comentada'];
		}

		foreach ($questoes_comentadas as $item) {

			$user_id = $item['user_id'];

			$pg_professor_mes = calc_pg_professor_mes($totais, get_percentual_sem_format($item['qcp_qtde_comentada'], $total_coments));

			$acumulado = $this->relatorios_model->get_valor_acumulado_user($user_id, $mes, $ano);
			$acumulado = $acumulado ? $acumulado['pap_acumulado'] : 0;

			$valor_acumulado = $acumulado + numero_americano($pg_professor_mes) < $min_pagamento ? $acumulado + numero_americano($pg_professor_mes) : 0;

			$nova_data = date('Y-m-d', strtotime($ano.'-'.$mes."-10 +1 month"));
			$nova_data_a = explode('-', $nova_data);

			$this->relatorios_model->salvar_valor_acumulado($user_id, $nova_data_a[1], $nova_data_a[0], $valor_acumulado);
		}
	}

	public function atualizar_simulados_expirados()
	{
		$this->load->model('simulado_model');

		echo "Listando produtos expirados ativos...<br>";

		if($produtos = listar_produtos_expirados()) {

			echo "Encontramos " . count($produtos) . " produto(s) expirado(s) ativo(s)...<br>";

			foreach ($produtos as $produto) {

				echo "Buscando simulados para o produto: {$produto->id}...<br>";

				if($simulados = $this->simulado_model->listar_por_produto($produto->id)) {

					echo "Encontramos " . count($simulados) . " simulado(s)...<br>";

					foreach ($simulados as $simulado) {

						echo "Atualizando status do simulado: {$simulado['sim_id']}...<br>";

						$this->simulado_model->atualizar_status($simulado['sim_id'], INATIVO);
					}
					
				}

				echo "Atualizando status do produto: {$produto->id}...<br>";

				$post = array(
					'ID' => $produto->id,
					'post_status' => 'pending'
				);

				wp_update_post($post);

			}

		}

		else {

			echo "Nenhum produto expirado ativo foi encontrado<br>";
		}

		echo "Script finalizado!";

	}

	public function atualizar_qtde_questoes_por_assunto()
	{
		
		$this->load->model('assunto_model');
		$this->load->model('disciplina_model');
		$this->load->model('questao_model');
		// $this->assunto_model->atualizar_quantidade_questoes();

		$assuntos = $this->assunto_model->listar_todas();
		foreach ($assuntos as $assunto) {
			set_time_limit(360);

			echo "Executando assunto {$assunto['ass_id']} {$assunto['ass_nome']}<br>";

			$filtro['ass_ids'] = [$assunto['ass_id']];

			if($assunto['dis_id']) {
				$filtro['dis_ids'] = [$assunto['dis_id']];
			}
			
			echo "Ass: {$assunto['ass_id']}, Dis: {$assunto['dis_id']}<br>";
			adicionar_assuntos_filhos($filtro);

			$total = $this->questao_model->get_total_resultado($filtro);

			$this->assunto_model->atualizar([
				'ass_id' => $assunto['ass_id'],
				'ass_qtd_questoes' => $total
			]);
		}

		$disciplinas = $this->disciplina_model->listar_todas();

		foreach($disciplinas as $disciplina){
			set_time_limit(360);
			
			echo "<br>Executando disciplina {$disciplina['dis_id']} {$disciplina['dis_nome']}: ";
			
			$filtro_dis['dis_ids'] = [$disciplina['dis_id']];

			$filtro_dis['ass_ids'] = array();

			$assuntos = $this->assunto_model->listar_por_disciplina($disciplina['dis_id']);

			if(empty($assuntos)){
				$total = 0;
				echo "\t Sem assuntos ativos: ";
			}else{

				foreach($assuntos as $assunto){
					array_push($filtro_dis['ass_ids'], $assunto['ass_id']);
				}

				$total = $this->questao_model->get_total_resultado($filtro_dis);

			}

			echo $total;

			$this->disciplina_model->atualizar([
				'dis_id' => $disciplina['dis_id'],
				'dis_qtd_questoes' => $total
			]);

		}

		echo "<br/>Script finalizado!";
	}

	public function processar_questoes_repetidas()
	{
		set_time_limit(360);

		$this->load->model('configuracao_model');
		$this->load->model('questao_model');

		$percentual_similaridade = $this->configuracao_model->get('qr_percentual_similaridade') ?: 100;
		$base_texto = $this->configuracao_model->get('qr_base_texto') ?: SIM;
		$enunciado = $this->configuracao_model->get('qr_enunciado') ?: SIM;
		$ignorar_html = $this->configuracao_model->get('qr_ignorar_html') ?: NAO;
		$mesma_disciplina = $this->configuracao_model->get('qr_mesma_disciplina') ?: SIM;
		$mesma_banca = $this->configuracao_model->get('qr_mesma_banca') ?: SIM;
		$disciplinas = $this->configuracao_model->get('qr_disciplinas') ?: "";
		$anos = $this->configuracao_model->get('qr_anos') ?: "";
		$qtde_questoes = $this->configuracao_model->get('qr_qtde_questoes') ?: 100;

		$questoes = $this->questao_model->listar_proximas_repetidas($qtde_questoes, $disciplinas, $anos);

		echo "Iniciando execucao. " . count($questoes) . " questoes a serem executadas<br>";

		// salva execução
		$execucao_id = $this->questao_model->salvar_execucao_repetidas([
			'qrx_data_hora' => get_data_hora_agora(),
			'qrx_percentual' => $percentual_similaridade,
			'qrx_texto_base' => $base_texto,
			'qrx_enunciado' => $enunciado,
			'qrx_texto_plano' => $ignorar_html,
			'qrx_qtde_questoes' => $qtde_questoes,
			'qrx_tempo_gasto' => 0,
			'qrx_mesma_disciplina' => $mesma_disciplina,
			'qrx_mesma_banca' => $mesma_banca,
			'qrx_disciplinas' => $disciplinas,
			'qrx_anos' => $anos,
		]);

		$timestamp_inicio = time();
		echo "Data/hora inicio: " . date("d/m/Y H:i:s", $timestamp_inicio) . "<br>";

		foreach ($questoes as $questao) {

			echo "Salvando detalhe execucao questao {$questao['que_id']}<br>";

			// salva detalhe da execução
			$this->questao_model->salvar_execucao_repetidas_detalhes([
				'qrx_id' => $execucao_id,
				'que_id' => $questao['que_id']
			]);
			
			$questao = $this->questao_model->get_by_id($questao['que_id']);

			$disciplina_id = $mesma_disciplina == SIM ? $questao['dis_id'] : null;

			$bancas_ids = null;
			if($mesma_banca == SIM) {
				$bancas_ids = [];

				$provas = $this->questao_model->listar_provas($questao['que_id']);

				if($provas) {
					foreach ($provas as $prova) {
						array_push($bancas_ids, $prova['ban_id']);
					}	
				}
				
			}

			$candidatas = $this->questao_model->listar_candidatas_repetidas($questao['que_id'], $disciplina_id, $bancas_ids);

			$total = count($candidatas);

			echo "Inicio processamento candidatas: $total <br>";
			
			foreach($candidatas as $candidata) {

				$ok_texto_base = true;
				$ok_enunciado = true;

				if($base_texto == SIM) {

					$ok_texto_base = false;

					$questao_texto_base = $ignorar_html ? strip_tags($questao['que_texto_base']) : $questao['que_texto_base'];
					$candidata_texto_base = $ignorar_html ? strip_tags($candidata['que_texto_base']) : $candidata['que_texto_base'];

					similar_text(trim($questao_texto_base), trim($candidata_texto_base), $similaridade);
				
					if($similaridade >= $percentual_similaridade) {
						$ok_texto_base = true;
					}

				}

				if($enunciado == SIM) {

					$ok_enunciado = false;

					$questao_enunciado = $ignorar_html ? strip_tags($questao['que_enunciado']) : $questao['que_enunciado'];
					$candidata_enunciado = $ignorar_html ? strip_tags($candidata['que_enunciado']) : $candidata['que_enunciado'];

					similar_text(trim($questao_enunciado), trim($candidata_enunciado), $similaridade);
				
					if($similaridade >= $percentual_similaridade) {
						$ok_enunciado = true;
					}

				}

				if($ok_enunciado && $ok_texto_base) {
					$this->questao_model->salvar_questao_repetida($questao['que_id'], $candidata['que_id'], $execucao_id);
				}

			}

			echo "Fim processamento candidatas<br>";

		}

		$timestamp_fim = time();
		$tempo_gasto = $timestamp_fim - $timestamp_inicio;
		$this->questao_model->atualizar_tempo_gasto_execucao_repetidas($execucao_id, $tempo_gasto);

		echo "Data/hora fim: " . date("d/m/Y H:i:s", $timestamp_fim) . "<br>";
		echo "Script executado com sucesso";
	}

	/**
	 * Atualiza a contagem de cursos por professor, matéria e concurso diariamente
	 * devido à data de disponibilidade de cada curso
	 * 
	 * @since K5
	 *
	 */
	public function atualizar_quantidade_cursos(){

		KLoader::model("ProdutoModel");
		KLoader::model("ColaboradorModel");
		KLoader::model("CategoriaModel");

		//Autores
		$autores = ColaboradorModel::listar_por_tipo();
		foreach($autores as $autor){
			$qtde_cursos = ProdutoModel::contar_por_professor($autor->user_id);
			ColaboradorModel::atualizar_quantidade_cursos($autor->user_id, $qtde_cursos);
		}

		//Matérias
		$materias = CategoriaModel::listar_filhas(CATEGORIA_MATERIA);
		foreach($materias as $materia){
			CategoriaModel::atualizar_quantidade_cursos($materia->term_id);
		}

		//Concursos
		$concursos = CategoriaModel::listar_filhas(CATEGORIA_CONCURSO);
		foreach($concursos as $concurso){
			CategoriaModel::atualizar_quantidade_cursos($concurso->term_id);
		}

		echo "Script executado com sucesso";
	}

	/**
	 * Buscar questões repetidas que não estão inativas e torna elas inativas
	 * 
	 * @since L1
	 */
	public function inativar_questoes_repetidas(){

		echo "Script iniciado<br>";
		
		$this->load->model('questao_model');
		$this->load->helper('questao');

		$filtro = array ('filtro_repetidas' => REPETIDAS, 'filtro_ativo' => ATIVO) ;

		$questoes_ids = $this->questao_model->get_questoes_ids_por_filtro( $filtro, null);

		echo "Foram encontradas " . count($questoes_ids) . " questoes repetidas que estão ativas e serão inativadas<br>";

		foreach ($questoes_ids as $linha) {
			if(is_array($linha)){
				$questao = $linha['que_id'];
			}else{
				$questao = $linha;
			}
			$this->questao_model->atualizar_ativo_inativo($questao, INATIVO);
		}

		echo "Script executado com sucesso<br>";
	}

}