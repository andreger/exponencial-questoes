<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once $_SERVER['DOCUMENT_ROOT'] . "/wp-admin/includes/user.php";

class Selenium extends CI_Controller {

	var $email = "selenium@exponencialconcursos.com.br";
	
	var $senha = "Zcr43209$#";

	
	public  function __construct() {
		parent::__construct();
	}
	
	public function excluir_usuario_teste()
	{
		$usuario = get_usuario_by_email($this->email);
		
		$pedidos = get_pedidos($usuario->ID);
		foreach ($pedidos as $pedido) {
			wp_delete_post($pedido->post_id, true);
		}

		excluir_posts_usuario($usuario->ID);
		
		wp_delete_user($usuario->ID);
	}
	
	public function notificar_por_email()
	{
		$titulo = $this->input->post('titulo');
		$mensagem = $this->input->post('mensagem');
		
		//enviar_email('leonardo.coelho@exponencialconcursos.com.br', 'Erro Selenium', $mensagem, 'exponencial@exponencialconcursos.com.br', null);
	}
	
	public function excluir_comentarios_jmeter()
	{
		$this->load->model('Comentario_model');
		$que_ids = $this->Comentario_model->excluir_comentarios_jmeter();

		$this->load->model("Questao_model");
		foreach($que_ids as $que_id){
			$total = $this->comentario_model->contar_comentarios($que_id);
			$this->Questao_model->atualizar_numero_comentarios($que_id, $total);
		}
	}
}
?>