<?php
class Xhr extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct ();

		session_start();
	}

	public function get_usuarios_designados($simulado_id)
	{
		
		$this->load->model('simulado_model');
		$data['usuarios_designados'] = $this->simulado_model->get_usuarios_designados($simulado_id);
		
		
		echo $this->load->view('xhr/get_usuarios_designados', $data, true);	
	}
	
	public function cancelar_designacao($simulado_usuario_id)
	{
		$this->load->model('simulado_model');
		$data['usuarios_designados'] = $this->simulado_model->excluir_simulado_usuario($simulado_usuario_id);
	}
	
	public function get_cidades_por_estado()
	{
		$uf = $this->input->get('estado');
		$cidades = get_cidades_por_estado($uf);
		$string = '[ [ "", "Selecione uma cidade..." ]';
		foreach ($cidades as $cidade) {
			$string .= ', [ "' . $cidade['cid_codigo_municipio'] . '", "' . $cidade['cid_nome'] . '" ]';
		}
		$string .= ' ]';
		echo $string;
	}
	
	public function listar_assuntos()
	{
		print_r($_POST);
	}
	
	public function get_arvore()
	{
		$this->load->model('assunto_model');
		$this->load->model('disciplina_model');
		
		if($disciplinas_ids = $this->input->get('dis_ids')) {
		
			if(is_null($disciplinas_ids)) {
				$disciplinas = $this->disciplina_model->listar_todas();
			}
			else {
				$disciplinas = $this->disciplina_model->listar_por_ids($disciplinas_ids);
			}		
			
			foreach ($disciplinas as &$disciplina) {
				/** [JOULE][J2] UPGRADE - Definir ordem de assuntos da Taxonomia
	 			 * 	https://podio.com/profinfnetedubr/lslc/apps/upgrades/items/334
	 			 *
	 			 *	O primeiro nível da árvore de levar em consideração a prioridade. Os demais, ordem alfabética
	 			 */
				$disciplina['filhos'] = $this->assunto_model->get_arvore($disciplina['dis_id'], null, "case when ass_nome = '".SEM_ASSUNTO."' then 1 else 0 end, ass_prioridade asc, ass_nome");
			}
			
			$item['filhos'] = $disciplinas;
			
			echo get_arvore($item);	
		}
		else {
			echo false;
		}
	}

	public function caderno_compartilhado_url($caderno_id, $is_coaching = FALSE)
	{
		$this->load->helper('caderno');
		
		if($is_coaching)
		{
			echo get_caderno_coaching_url($caderno_id);
		}
		else
		{
			echo get_caderno_compartilhado_url($caderno_id);
		}
	}

	public function usuarios_designados_caderno($caderno_id)
	{
		$this->load->model('caderno_model');
		
		$data['usuarios_designados'] = $this->caderno_model->listar_designados($caderno_id);
		
		echo $this->load->view('xhr/usuarios_designados_caderno', $data, true);	
	}
	
	public function salvar_resposta_marcada($questao_id, $resposta)
	{
		$this->load->helper('questao');
		salvar_resposta_marcada($questao_id, $resposta);
	}

	public function zerar_marcadas()
	{
		$this->load->helper('questao');
		zerar_respostas_marcadas();
	}

	public function contar_acertos($simulado_id)
	{
		$this->load->model('questao_model');

		$acertos = 0;
		$opcoes_questao = $this->input->post('opcao_questao');

		if($opcoes_questao) { 
			foreach ($opcoes_questao as $questao_id => $selecao) {
				$questao = $this->questao_model->get_by_id($questao_id);

				if($questao['que_resposta'] == $selecao || $questao['que_status'] == CANCELADA) {
					$acertos++;
				}
			}
		}

		echo $acertos;
	}

	public function contar_erros($simulado_id)
	{
		$this->load->model('questao_model');

		$erros = 0;
		$opcoes_questao = $this->input->post('opcao_questao');

		if($opcoes_questao) { 
			foreach ($opcoes_questao as $questao_id => $selecao) {
				$questao = $this->questao_model->get_by_id($questao_id);

				if($selecao && $questao['que_resposta'] != $selecao && $questao['que_status'] != CANCELADA) {
					$erros++;
				}
			}
		}

		echo $erros;
	}


	/**
	 * Atualiza a prioridade dos simulados. Todos os simulados são atualizados.
	 * 
	 * [JOULE][J1] UPGRADE - Controle de prioridade dos simulados
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/340
	 * 
	 * @since 10.0.0
	 * 
	 * @param array $_POST['simulados'] Lista de simulados inconsistentes
	 */

	public function atualizar_prioridade_simulados()
	{
		$this->load->model('simulado_model');

		$usuario_logado = get_usuario_array( get_current_user_id() );

		$this->db->trans_start();

		$msg = date('Y-m-d H:i:s')."[".$usuario_logado['login']."]"."[atualizar_prioridade_simulado]". stripslashes($this->input->post('simulados'));

		error_log($msg."\n", 3, '../logs/simulados'.date('Y-m-d').'.log');

		// recupera input
		$simulados_inconsistentes = json_decode(stripslashes($this->input->post('simulados')), true);
		$simulados_diversos = $this->simulado_model->listar_sem_prioridade();

		// define as prioridades atuais no banco como baixíssimas (maior significa menos prioridade)
		$this->simulado_model->atualizar_prioridade(NULL, PRIORIDADE_DEFAULT);

		// atualiza a prioridade dos simulados inconsistentes
		$i = 0;
		foreach ($simulados_inconsistentes as $simulado) {
			$i++;
			$this->simulado_model->atualizar_prioridade($simulado['sim_id'], $i);
		}

		$this->db->trans_complete();

	}

	/**
	 * Atualiza a prioridade dos assuntos de uma determinada disciplina.
	 * 
	 * [JOULE][J2] UPGRADE - Definir ordem de assuntos da Taxonomia
	 * 
	 * @see https://podio.com/profinfnetedubr/lslc/apps/upgrades/items/334
	 * 
	 * @since 10.1.0
	 */

	public function atualizar_ordem_taxonomia()
	{

		$this->load->model('assunto_model');

		// recupera input
		$disciplina_id = json_decode(stripslashes($this->input->post('disciplina_id')), true);
		$assuntos = json_decode(stripslashes($this->input->post('assuntos')), true);

		$m_assuntos = $this->assunto_model->listar_por_disciplina($disciplina_id);
	
		// define as prioridades atuais no banco como baixíssimas (maior significa menos prioridade)		
		foreach ($m_assuntos as $item) {
			$this->assunto_model->atualizar_prioridade($item['ass_id'], PRIORIDADE_DEFAULT);
		}

		// atualiza a prioridade dos assuntos
		$i = 0;
		foreach ($assuntos as $item) {
			$i++;
			$this->assunto_model->atualizar_prioridade($item['ass_id'], $i);
		}
	}
	/**
	 * TODO: Ver duplicidade com Main->salvar_resultado($simulado_id)
	 */
	public function simulado_nota($simulado_id) {
		$this->load->model('simulado_model');

		foreach ($this->input->post() as $key => $input) {
			if(!strcmp($key, 'enviar') == 0) {
				$post_name = explode("_", $key);

				if($post_name[1] == 'acertos'){
					$disciplinas_acertos[$post_name[2]] = $input;
				}elseif($post_name[1] == 'erros'){
					$disciplinas_erros[$post_name[2]] = $input;
				}
			}
		}

		if(isset($disciplinas_erros)){
			echo $this->simulado_model->salvar_nota(TRUE, get_current_user_id(), $simulado_id, $disciplinas_acertos, $disciplinas_erros);
		}else{
			echo $this->simulado_model->salvar_nota(TRUE, get_current_user_id(), $simulado_id, $disciplinas_acertos);
		}
	}

	public function simulado_nota_detalhe($simulado_id) 
	{
		$this->load->model('questao_model');
		$this->load->model('simulado_model');

		$simulado = $this->simulado_model->get_by_id($simulado_id);

		$disciplinas_acertos = [];
		$disciplinas_erros = [];

		foreach ($this->input->post('opcao_questao') as $questao_id => $selecao) {
			$questao = $this->questao_model->get_by_id($questao_id);
			
			if($selecao) {
				if($selecao == $questao['que_resposta']) {
					if(isset($disciplinas_acertos[$questao['dis_id']])) {
						$disciplinas_acertos[$questao['dis_id']]++;
					}
					else {
						$disciplinas_acertos[$questao['dis_id']] = 1;
					}					
				}elseif($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']){
					if(isset($disciplinas_erros[$questao['dis_id']])) {
						$disciplinas_erros[$questao['dis_id']]++;
					}
					else {
						$disciplinas_erros[$questao['dis_id']] = 1;
					}
				}
			}
		}
		
		if($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']){
			echo $this->simulado_model->salvar_nota(TRUE, get_current_user_id(), $simulado_id, $disciplinas_acertos, $disciplinas_erros);
		}else{
			echo $this->simulado_model->salvar_nota(TRUE, get_current_user_id(), $simulado_id, $disciplinas_acertos);
		}
	}

	function listar_desempenho_por_disciplina()
	{
		$this->load->model('questao_model');
		$this->load->helper('tabela');	

		$aluno_id = $this->input->post('aluno_id');
		$inicio = $this->input->post('inicio');
		$fim = $this->input->post('fim');
		$bancas_ids = $this->input->post('bancas_ids');
		$disciplinas_ids = $this->input->post('disciplinas_ids');

		$where = "1=1 ";

		if($bancas_ids) {
			$bancas_a = [];
			foreach ($bancas_ids as $id) {
				array_push($bancas_a, "ban_id = {$id}");
			}
			$bancas_str = implode(" OR ", $bancas_a);
			$where .= " AND ($bancas_str)";
		}

		if($disciplinas_ids) {
			$disciplinas_a = [];
			foreach ($disciplinas_ids as $id) {
				array_push($disciplinas_a, "d.dis_id = {$id}");
			}
			$disciplinas_str = implode(" OR ", $disciplinas_a);
			$where .= " AND ($disciplinas_str)";
		}

		$desempenhos_disciplina = $this->questao_model->listar_desempenho_disciplina($aluno_id, $inicio, $fim, -1, false, false, $where);
		$tbody_array_disciplina = get_tabela_desempenho_disciplina($desempenhos_disciplina);

		echo json_encode($tbody_array_disciplina);
	}

	function listar_desempenho_por_banca()
	{
		$this->load->model('questao_model');
		$this->load->helper('tabela');	

		$aluno_id = $this->input->post('aluno_id');
		$inicio = $this->input->post('inicio');
		$fim = $this->input->post('fim');
		$bancas_ids = $this->input->post('bancas_ids');
		$disciplinas_ids = $this->input->post('disciplinas_ids');

		$where = "1=1 ";

		if($bancas_ids) {
			$bancas_a = [];
			foreach ($bancas_ids as $id) {
				array_push($bancas_a, "b.ban_id = {$id}");
			}
			$bancas_str = implode(" OR ", $bancas_a);
			$where .= " AND ($bancas_str)";
		}

		if($disciplinas_ids) {
			$disciplinas_a = [];
			foreach ($disciplinas_ids as $id) {
				array_push($disciplinas_a, "d.dis_id = {$id}");
			}
			$disciplinas_str = implode(" OR ", $disciplinas_a);
			$where .= " AND ($disciplinas_str)";
		}

		$desempenhos_banca = $this->questao_model->listar_desempenho_banca($aluno_id, $inicio, $fim, -1, false, false, $where);
		$tbody_array_banca = get_tabela_desempenho_banca($desempenhos_banca);

		echo json_encode($tbody_array_banca, JSON_UNESCAPED_SLASHES);
	}

	function listar_desempenho_por_assunto()
	{
		$this->load->model('questao_model');
		$this->load->helper('tabela');	

		$aluno_id = $this->input->post('aluno_id');
		$inicio = $this->input->post('inicio');
		$fim = $this->input->post('fim');
		$assuntos_ids = $this->input->post('assuntos_ids');
		$disciplinas_ids = $this->input->post('disciplinas_ids');

		$where = "1=1 ";

		if($assuntos_ids) {
			$assuntos_a = [];
			foreach ($assuntos_ids as $id) {
				array_push($assuntos_a, "a.ass_id = {$id}");
			}
			$assuntos_str = implode(" OR ", $assuntos_a);
			$where .= " AND ($assuntos_str)";
		}

		if($disciplinas_ids) {
			$disciplinas_a = [];
			foreach ($disciplinas_ids as $id) {
				array_push($disciplinas_a, "d.dis_id = {$id}");
			}
			$disciplinas_str = implode(" OR ", $disciplinas_a);
			$where .= " AND ($disciplinas_str)";
		}

		$desempenhos_assunto = $this->questao_model->listar_desempenho_assunto($aluno_id, $inicio, $fim, -1, false, false, $where);
		$tbody_array_assunto = get_tabela_desempenho_assunto($desempenhos_assunto);

		echo json_encode($tbody_array_assunto, JSON_UNESCAPED_SLASHES);
	}

	public function get_meus_simulados_resultados($offset = 0, $limit = LIMIT_MEUS_RESULTADOS_SIMULADOS)
	{
		$this->load->model('Simulado_model');

		$corpo = "";
		$footer = "";

		$titulo = $this->input->post('titulo') ? $this->input->post('titulo') : null;
		$inicio = $this->input->post('inicio') ? $this->input->post('inicio') : null;
		$fim = $this->input->post('fim') ? $this->input->post('fim') : null;
		
		$simulados_ranking = $this->Simulado_model->listar_simulados_ranking(get_current_user_id(), $offset, $limit, $titulo, $inicio, $fim);
		$total_simulados_ranking = count($this->Simulado_model->listar_simulados_ranking(get_current_user_id(), 0, null, $titulo, $inicio, $fim));

		if($total_simulados_ranking == 0) {
			$footer = "Nenhum resultado encontrado";
		}
		else {

			foreach ($simulados_ranking as &$simulado) {
				$simulado['nota'] = $this->Simulado_model->get_nota(get_current_user_id(), $simulado['sim_id']);

				$corpo .= meus_simulados_resultados($simulado);
			}

			$novo_offset = $offset + $limit;//LIMIT_MEUS_RESULTADOS_SIMULADOS;

			if($novo_offset < $total_simulados_ranking) {
				$footer = "<a class='simulado-resultado-mais' data-offset='{$novo_offset}' href='#'>Carregar Mais</a>";	
			}
		}

		echo json_encode(['corpo' => $corpo, 'footer' => $footer]);
	}

	public function salvar_tamanho_fonte($tamanho)
	{
		session_start();
		$_SESSION['tamanho-fonte'] = $tamanho;
	}
	
	public function modal_resultados_simulados($simulado_id) 
	{	
		//$this->output->enable_profiler(TRUE);		

		$this->load->model('simulado_model');
		$this->load->helper('simulado');
		
		$data['resultados'] = $this->simulado_model->get_resultados($simulado_id, get_current_user_id());
		$data['simulado'] = $this->simulado_model->get_by_id($simulado_id);
		$data['disciplinas'] = $this->simulado_model->get_disciplinas_simulado($simulado_id);
		$data['total_questoes'] = $this->simulado_model->get_total_questoes($simulado_id);

		echo $this->load->view('modal/resultado_simulado', $data, TRUE);
	}

	/**
	 * Escreve algumas das notifcações de um usuário
	 * 
	 * @since L1
	 * 
	 * @param int $offset
	 * @param bool $is_lida Se a notificação é lida, não lida ou ambas (NULL)
	 * 
	 */
	public function carregar_mais_notificacoes($offset = 0, $is_lida = NULL)
	{
		$this->load->model('notificacao_model');
		$this->load->helper('questao');

		$notificacoes = $this->notificacao_model->listar_por_usuario(get_current_user_id(), $is_lida, $offset);

		$notificacoes_html = KLoader::view('header/barra_superior/notificacoes/notificacoes_corpo', ['notificacoes' => $notificacoes], TRUE);

		echo mb_convert_encoding($notificacoes_html, "UTF-8", "UTF-8");
	}

	/**
	 * Retorna as informações de notificações não lidas, algumas das notifcações de um usuário
	 * 
	 * @since L1
	 * 
	 */
	public function carregar_notificacoes()
	{
		$this->load->model('notificacao_model');
		$this->load->helper('questao');

		$result = array();

		//Total não lidas
		$result['qtd_nao_lidas'] = $this->notificacao_model->contar_por_usuario(get_current_user_id(), FALSE);

		//Total de notificações
		$result['qtd_todas'] = $this->notificacao_model->contar_por_usuario(get_current_user_id());

		//Todas as notificações
		$notificacoes = $this->notificacao_model->listar_por_usuario(get_current_user_id());

		$notificacoes_html = KLoader::view('header/barra_superior/notificacoes/notificacoes_corpo', ['notificacoes' => $notificacoes], TRUE);
		$notificacoes_html = mb_convert_encoding($notificacoes_html, "UTF-8", "UTF-8");
		$result['todas'] = $notificacoes_html;

		//Notificações não lidas
		$notificacoes = $this->notificacao_model->listar_por_usuario(get_current_user_id(), FALSE);

		$notificacoes_html = KLoader::view('header/barra_superior/notificacoes/notificacoes_corpo', ['notificacoes' => $notificacoes], TRUE);
		$notificacoes_html = mb_convert_encoding($notificacoes_html, "UTF-8", "UTF-8");
		$result['nao_lidas'] = $notificacoes_html;
		//print_r("RESULT: ");
		//print_r($result);
		echo json_encode($result);
	}

	/**
	 * Marca todas as notificações não lidas do usuário logado como lidas
	 * 
	 * @since L1
	 * 
	 */
	public function marcar_todas_notificacoes_como_lidas()
	{
		$this->load->model('notificacao_model');

		$this->notificacao_model->marcar_todas_como_lidas(get_current_user_id());
	}

	/**
	 * Marca uma notificação como lida ou não lida
	 * 
	 * @since L1
	 * 
	 */
	public function marcar_notificacao_como_lida_nao_lida($notificacao_id, $is_lida = TRUE)
	{
		$this->load->model('notificacao_model');

		$notificacao = $this->notificacao_model->get_notificacao($notificacao_id);

		if( ($notificacao['not_destinatario_id'] == get_current_user_id()) && ($is_lida xor $notificacao['not_data_lida']) )
		{
			$this->notificacao_model->marcar_como_lida_nao_lida($notificacao_id, $is_lida);
		}

		echo $this->notificacao_model->contar_por_usuario(get_current_user_id(), FALSE);

	}
	
	public function listar_dados_combo_filtro($disciplinas_somente_ativas = TRUE)
	{

		$this->load->model("disciplina_model");
		$this->load->model("banca_model");
		$this->load->model("cargo_model");
		$this->load->model("orgao_model");

		$bancas = to_select2_format( get_banca_multiselect($this->banca_model->listar_todas()) );
		$orgaos = to_select2_format( get_orgao_multiselect($this->orgao_model->listar_todas()) );
		$cargos = to_select2_format( get_cargo_multiselect($this->cargo_model->listar_todas()) );
		$disciplinas = to_select2_format( get_disciplina_multiselect($this->disciplina_model->listar_todas(FALSE, $disciplinas_somente_ativas), TRUE) );

		$result['ban_ids'] = $bancas;
		$result['org_ids'] = $orgaos;
		$result['car_ids'] = $cargos;
		$result['dis_ids'] = $disciplinas;

		echo json_encode( $result );
	}

}