<?php
class Login extends CI_Controller 
{
	public function __construct() {
		session_start();

		parent::__construct();
		
		// Load facebook library and pass associative array which contains appId and secret key
		$this->load->library('facebook');
	}

    public function facebook() {
        if ($this->facebook->is_authenticated()) {

            $user_profile = $this->facebook->request('get', '/me?fields=name,email');
            $socialId = $user_profile['id'];
            $email = $user_profile['email'];
            $nome = $user_profile['name'];
            $fotoUrl = "https://graph.facebook.com/{$socialId}/picture";

            $usuario = $this->criarOuObterUsuario($email, $nome);
            update_user_meta($usuario->ID, 'facebook', $socialId);
            $this->atualizarFoto($usuario, $fotoUrl);
            $this->logarNoWP($email);
        } else {
            redirect($this->facebook->login_url());
        }
    }

    public function google()
    {
        include_once APPPATH . "../vendor/autoload.php";

        $google_client = new Google_Client();

        $google_client->setClientId(GOOGLE_OAUTH_CLIENT_ID); //Define your ClientID
        $google_client->setClientSecret(GOOGLE_OAUTH_CLIENT_SECRET); //Define your Client Secret Key
        $google_client->setRedirectUri(GOOGLE_OAUTH_REDIRECT); //Define your Redirect Uri
        $google_client->addScope('email');
        $google_client->addScope('profile');

        if(!$this->session->userdata('google_access_token'))

            if (isset($_GET["code"])) {
                $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

                if (!isset($token["error"])) {
                    $google_client->setAccessToken($token['access_token']);
                    $this->session->set_userdata('google_access_token', $token['access_token']);

                    $google_service = new Google_Service_Oauth2($google_client);
                    $data = $google_service->userinfo->get();

                    $usuario = $this->criarOuObterUsuario($data['email'], $data['name']);
                    update_user_meta($usuario->ID, 'google', $data['id']);
                    $this->atualizarFoto($usuario, $data['picture']);
                    $this->logarNoWP($data['email']);
                }

                else {
                    echo "no token";
                }
            }
            else {
                redirect($google_client->createAuthUrl());
            }
    }

    /**
     * ObtÃ©m o usuÃ¡rio (se existir) ou cria um novo
     *
     * @param $email
     * @param $nome
     * @return mixed
     */
    public function criarOuObterUsuario($email, $nome)
    {
        if (! $usuario = get_user_by('login', $email) ) {
            $usuario = criar_usuario($email, get_senha_oauth($email), $nome);
        } else {
            wp_set_password(get_senha_oauth($email), $usuario->ID);
        }

        return $usuario;
    }

    /**
     * Atualiza a foto do usuÃ¡rio caso nÃ£o tenha sido alterada
     *
     * @param $usuario
     * @param $fotoUrl
     */
    public function atualizarFoto($usuario, $fotoUrl)
    {
        $foto_alterada = get_user_meta($usuario->ID, 'foto_alterada');

        if (! $foto_alterada) {
            $foto_perfil = get_foto_usuario_filepath($usuario->ID);
            copy($fotoUrl, $foto_perfil);
        }
    }

    /**
     * Realiza o login no WP
     *
     * @param $email
     */
    public function logarNoWP($email)
    {
        wp_signon(array(
            'user_login' => $email,
            'user_password' => get_senha_oauth($email),
            'remember' => false
        ), false);

        redirect('/');
    }

    public function logout()
	{
		$this->facebook->destroy_session();
        $this->session->unset_userdata('google_access_token');
		session_destroy();

		limpar_carrinho();
		
		wp_logout();
	}

	public function get_login_box_html()
    {
        echo KLoader::view("login/modal", ['ref_url' => '/questoes/main/resolver_questoes'], true);
    }

}
?>