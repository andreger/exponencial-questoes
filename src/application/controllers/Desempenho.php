<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

/**
 * Controller responsável pela página Meu Desempenho e subpáginas
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2608
 */

class Desempenho extends CI_Controller {

	/**
	 * Construtor
	 */

	public function __construct() {
		parent::__construct ();

		if(is_area_desativada(array(PAINEL_SQ_GERAL))){
			redirecionar_erro_500();
		}

		session_start();

		$this->load->model ( 'disciplina_model' );
		$this->load->model ( 'banca_model' );
		$this->load->model ( 'orgao_model' );
		$this->load->model ( 'cargo_model' );
		$this->load->model ( 'assunto_model' );
		$this->load->model ( 'prova_model' );
		$this->load->model ( 'questao_model' );
		$this->load->model ( 'area_formacao_model' );
		$this->load->model ( 'area_atuacao_model' );
		$this->load->model ( 'simulado_model' );
		$this->load->model ( 'comentario_model' );
		$this->load->model ( 'caderno_model' );
		$this->load->model ( 'configuracao_usuario_model' );

		$this->load->helper('questao');
		$this->load->helper('simulado');
		$this->load->helper('prova');
		$this->load->helper('aluno');
		$this->load->helper('desempenho');

		init_profiler();
	}

	/**
	 * URL: /questoes/desempenho/disciplina
	 */

	public function disciplinas()
	{
	    $data = [];
	    $disciplinas = [];

		self::init_desempenho($data);

		$data['desempenho_menu'] = 'disciplinas';

		$desempenhos_disciplina = $this->questao_model->listar_desempenho_disciplina($data['aluno_id'], $data['inicio_ymd'], $data['fim_ymd'], $data['bancas'], $data['disciplinas']);
		$desempenho_disciplina_grafico = $this->questao_model->listar_desempenho_disciplina($data['aluno_id'], $data['inicio_ymd'], $data['fim_ymd'], $data['bancas'], $data['disciplinas'], 20, true, true);

		$data['tem_gr_hist_disciplina'] = !empty($desempenho_disciplina_grafico);
		foreach ($desempenho_disciplina_grafico as $key => $disciplina) {

			$performance = number_format($disciplina['performance'], 1);

			$disciplinas[] = array(
				'nome' => $disciplina['dis_nome'],
				'performance' => $performance,
				'nota' => $disciplina['acertos'],
				'total_questoes' => $disciplina['total']
			);
			if($key >= 30){
				break;
			}

		}

		function comp_performance($sim1, $sim2){
			return $sim1['performance'] > $sim2['performance'] ? 0 : 1;
		}

		if($disciplinas) {
			usort($disciplinas, 'comp_performance');
		}

		$gr_disciplina_notas = "s";
		$gr_disciplina_10 = grafico_desempenho($disciplinas, 10);
		$gr_disciplina_30 = grafico_desempenho($disciplinas, 30, $gr_disciplina_notas);

		$data['gr_disciplina_notas'] = $gr_disciplina_notas;
		$data['gr_disciplina_10'] = $gr_disciplina_10;
		$data['gr_disciplina_30'] = $gr_disciplina_30;

		$data['thead_array_disciplina'] = array('Disciplina', 'Resolvidas', 'Acertos', 'Erros', '(%)');
		$data['tbody_array_disciplina'] = get_tabela_desempenho_disciplina($desempenhos_disciplina);
		$data['tfoot_array_disciplina'] = get_footer_tabela_desempenho_disciplina($desempenhos_disciplina);

		$qr_disciplina = $this->questao_model->listar_questoes_respondidas_agrupadas_por_disciplina($data['aluno_id'], $data['inicio_ymd'], $data['fim_ymd'], $data['bancas'], $data['disciplinas']);
		$data['tem_gr_disciplina'] = !empty($qr_disciplina);
		$data['qr_disciplina'] = grafico_questoes_respondidas_por_disciplina($qr_disciplina);

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view('desempenho/por_disciplinas', $data);
		$this->load->view(get_main_footer_view_url(), $data);
	}

	/**
	 * URL: /questoes/desempenho/bancas
	 */

	public function bancas()
	{
		self::init_desempenho($data);

		$data['desempenho_menu'] = 'bancas';
		$data['assuntos_combo'] = null;

		$desempenhos_banca = $this->questao_model->listar_desempenho_banca($data['aluno_id'], $data['inicio_ymd'], $data['fim_ymd'], $data['bancas'], $data['disciplinas']);

		$data['thead_array_bancas'] = array('Disciplina', 'Banca', 'Questões resolvidas', '% acerto', 'Ações');
		$data['tbody_array_bancas'] = get_tabela_desempenho_banca($desempenhos_banca);
		$data['tfoot_array_bancas'] = $data['thead_array_bancas'];

		$qr_banca = $this->questao_model->listar_questoes_respondidas_agrupadas_por_banca($data['aluno_id'], $data['inicio_ymd'], $data['fim_ymd'], $data['bancas'], $data['disciplinas']);

		$data['tem_gr_banca'] = !empty($qr_banca);
		$data['qr_banca'] = grafico_questoes_respondidas_por_banca($qr_banca);
		$data['acertos_banca_10'] = grafico_acertos_questoes_por_banca($qr_banca, 10);
		$data['acertos_banca_30'] = grafico_acertos_questoes_por_banca($qr_banca, 30);

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view('desempenho/por_bancas', $data);
		$this->load->view(get_main_footer_view_url(), $data);
	}

	/**
	 * URL: /questoes/desempenho/assuntos
	 */

	public function assuntos()
	{
		self::init_desempenho($data);

		$data['desempenho_menu'] = 'assuntos';

		if($data['disciplinas']){
			$data['assuntos_combo'] = get_assunto_multiselect($this->assunto_model->listar_por_disciplinas($data['disciplinas']));
		}else{
			$data['assuntos_combo'] = null;
		}

		$desempenhos_assunto = $this->questao_model->listar_desempenho_assunto($data['aluno_id'], $data['inicio_ymd'], $data['fim_ymd'], $data['disciplinas'], $data['assuntos']);

		$data['thead_array_assuntos'] = array('Disciplina', 'Assunto', 'Questões resolvidas', '% acerto', 'Ações');
		$data['tbody_array_assuntos'] = get_tabela_desempenho_assunto($desempenhos_assunto);
		$data['tfoot_array_assuntos'] = $data['thead_array_assuntos'];

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view('desempenho/por_assuntos', $data);
		$this->load->view(get_main_footer_view_url(), $data);
	}

	/**
	 * URL: /questoes/desempenho/simulados
	 */

	public function simulados()
	{
		self::init_desempenho($data);

		$data['desempenho_menu'] = 'simulados';

		$desempenho_simulado_grafico = $this->simulado_model->listar_simulados_ranking($data['aluno_id'], 0, LIMIT_MEUS_RESULTADOS_SIMULADOS, $data['sim_nome'], $data['inicio_ymd'], $data['fim_ymd']);
// 		$desempenho_simulado_grafico = $this->simulado_model->listar_resolvidos($data['aluno_id']);
		$data['tem_gr_simulado'] = !empty($desempenho_simulado_grafico);

// 		var_dump($desempenho_simulado_grafico); exit;

		foreach ($desempenho_simulado_grafico as $key => $simulado) {
			$total_questoes = $this->simulado_model->get_pontuacao_maxima($simulado['sim_id']);
			$nota = $this->simulado_model->get_nota($data['aluno_id'], $simulado['sim_id']);
			$performance = number_format( ($nota / $total_questoes) * 100, 2);
			$ranking = $this->simulado_model->get_posicao_ranking($simulado['sim_id'], $data['aluno_id']);

			$simulados[] = array(
					'sim_id' => $simulado['sim_id'],
					'nome' => $simulado['sim_nome'],
					'performance' => $performance,
					'nota' => $nota,
					'posicao' => $ranking['posicao'],
					'total_questoes' => $total_questoes,
					'data' => date('d/m/Y', strtotime($simulado['sim_usu_data_resolucao'])),
					'data_original' => $simulado['sim_usu_data_resolucao']
			);
			if($key >= 30){
				break;
			}
		}

		function comp_performance($sim1, $sim2){
			return $sim1['performance'] > $sim2['performance'] ? 0 : 1;
		}

		if($simulados) {
			usort($simulados, 'comp_performance');
		}

		$data['thead_array_simulados'] = array('Simulado', 'Nota', 'Data', 'Ranking', 'Ranking Completo');
		$data['tbody_array_simulados'] = get_tabela_desempenho_simulado($simulados);
		$data['tfoot_array_simulados'] = $data['thead_array_simulados'];

		$gr_simulado_notas = "s";
		$gr_simulado_10 = grafico_desempenho($simulados, 10);
		$gr_simulado_30 = grafico_desempenho($simulados, 30, $gr_simulado_notas);

		$data['gr_simulado_notas'] = $gr_simulado_notas;
		$data['gr_simulado_10'] = $gr_simulado_10;
		$data['gr_simulado_30'] = $gr_simulado_30;

		$this->load->view(get_main_header_view_url(), $data);
		$this->load->view('desempenho/por_simulados', $data);
		$this->load->view(get_main_footer_view_url(), $data);
	}

	/**
	 * Função que inicializa os itens em comum das páginas de desempenho
	 */

	private function init_desempenho(&$data)
	{
		if(is_area_desativada(array(PAINEL_SQ_MEU_DESEMPENHO))){
			redirecionar_erro_500();
		}

		// Checagem das permissões
		tem_acesso(array(
			/*ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, CONSULTOR, ASSINANTE_SQ, REVISOR*/ USUARIO_LOGADO
		), ACESSO_NEGADO_SQ);

		// Proteção contra ataques de acesso indevido
		if($aluno_id = $this->input->get('aluno_id')) {
			tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, CONSULTOR), ACESSO_NEGADO_SQ);
		}

		// Fluxo pós checagem
		$this->load->helper('grafico');

		// Inicializa sessão
		$filtro_desempenho['md_inicio'] = isset($filtro_desempenho['md_inicio']) ? $filtro_desempenho['md_inicio'] : date('Y-m-d', strtotime('-1 week'));
		$filtro_desempenho['md_fim'] = isset($filtro_desempenho['md_fim']) ? $filtro_desempenho['md_fim'] : date('Y-m-d');

		$filtro_desempenho['md_aluno_id'] = isset($filtro_desempenho['md_aluno_id']) ? $filtro_desempenho['md_aluno_id'] : get_current_user_id();
		$filtro_desempenho['md_is_aluno_logado'] = isset($filtro_desempenho['md_is_aluno_logado']) ? $filtro_desempenho['md_is_aluno_logado'] : true;
		$filtro_desempenho['md_aluno_selecionado'] = isset($filtro_desempenho['md_aluno_selecionado']) ? $filtro_desempenho['md_aluno_selecionado'] : null;
		$filtro_desempenho['md_bancas_selecionadas'] = isset($filtro_desempenho['md_bancas_selecionadas']) ? $filtro_desempenho['md_bancas_selecionadas'] : null;
		$filtro_desempenho['md_disciplinas_selecionadas'] = isset($filtro_desempenho['md_disciplinas_selecionadas']) ? $filtro_desempenho['md_disciplinas_selecionadas'] : null;
		$filtro_desempenho['md_assuntos_selecionados'] = isset($filtro_desempenho['md_assuntos_selecionados']) ? $filtro_desempenho['md_assuntos_selecionados'] : null;

		// Definição de flags para visão pessoal (perfis administrativos)
		if($this->input->get('minha_visao')) {
			$filtro_desempenho['md_aluno_id'] = get_current_user_id();
			$filtro_desempenho['md_is_aluno_logado'] = true;
			$filtro_desempenho['md_aluno_selecionado'] = null;
		}
		else {
			// Definição de flags para visão de um determinado aluno (perfis administrativos)
			if($this->input->get('aluno_id')) {
				$filtro_desempenho['md_aluno_id'] = $this->input->get('aluno_id');
				$filtro_desempenho['md_is_aluno_logado'] = false;
				$filtro_desempenho['md_aluno_selecionado'] = $filtro_desempenho['md_aluno_id'];
			}

			// // Definição de flags para visão pessoal (aluno logado)
			// else {
			// 	$filtro_desempenho['md_aluno_id'] = get_current_user_id();
			// 	$filtro_desempenho['md_is_aluno_logado'] = true;
			// 	$filtro_desempenho['md_aluno_selecionado'] = null;
			// }
		}

		$data['aluno_id'] = $filtro_desempenho['md_aluno_id'];
		$data['is_aluno_logado'] = $filtro_desempenho['md_is_aluno_logado'];
		$data['aluno_selecionado'] = $filtro_desempenho['md_aluno_selecionado'];

		if($this->input->get('inicio')) {
			$filtro_desempenho['md_inicio'] = $this->input->get('inicio');
			$filtro_desempenho['md_fim'] = $this->input->get('fim');
		}

		//Se o formulário foi submetido então altera as variáveis de sessão
		if($_GET){

			if($this->input->get('bancas_ids')) {
				$filtro_desempenho['md_bancas_selecionadas'] = explode(',', $this->input->get('bancas_ids'));
			}else{
				$filtro_desempenho['md_bancas_selecionadas'] = null;
			}

			if($this->input->get('disciplinas_ids')) {

				$filtro_desempenho['md_disciplinas_selecionadas'] = explode(',', $this->input->get('disciplinas_ids'));

				$assuntos_disponiveis = $this->assunto_model->listar_por_disciplinas($filtro_desempenho['md_disciplinas_selecionadas']);

				if($assuntos_post = $this->input->get('assuntos_ids')) {

					$assuntos_post = explode(',', $assuntos_post);

					$assuntos_post = array_intersect($assuntos_post, array_column($assuntos_disponiveis, 'ass_id'));

					$filtro_desempenho['md_assuntos_selecionados'] = $assuntos_post;

				}else{
					$filtro_desempenho['md_assuntos_selecionados'] = null;
				}

			}else{
				$filtro_desempenho['md_disciplinas_selecionadas'] = null;
				$filtro_desempenho['md_assuntos_selecionados'] = null;
			}

		}

		// definição do período dos dados
		$inicio = $filtro_desempenho['md_inicio'];
		$fim = $filtro_desempenho['md_fim'];

		$data['inicio'] = converter_para_ddmmyyyy($inicio);
		$data['fim'] = converter_para_ddmmyyyy($fim);
		$data['inicio_ymd'] = $inicio;
		$data['fim_ymd'] = $fim;
		$data['sim_nome'] = $this->input->get('sim_nome') ?? null;
		$data['bancas'] = $filtro_desempenho['md_bancas_selecionadas'];
		$data['disciplinas'] = $filtro_desempenho['md_disciplinas_selecionadas'];
		$data['assuntos'] = $filtro_desempenho['md_assuntos_selecionados'];

		// dados para filtro de alunos (perfis administrativos)
		if(tem_acesso([ADMINISTRADOR, COORDENADOR_SQ, CONSULTOR])) {

			if($filtro_aluno = $this->input->post('filtro_aluno')) {
				$data['alunos'] = get_ajax_main_alunos_acompanhamento($filtro_aluno);
				$data['filtro_aluno'] = array_map('intval', explode(',', $filtro_aluno));
			}
			else {
				$data['filtro_aluno'] = [];
				$data['alunos'] = get_ajax_main_alunos_acompanhamento();
			}
		}

		// inclusão de libs
		$data['include_data_tables'] = true;
		$data['include_chosen'] = true;
		$data['include_main_painel_aluno'] = true;
		$data['include_flot'] = true;
		$data['include_daterangepicker'] = true;
		$data['include_c3'] = true;
		$data['include_jquery_chained'] = true;
		$data['js'] = [
			'/questoes/assets-admin/js/pagina/desempenho/filtro_alunos.js',
			'/questoes/assets-admin/js/pagina/desempenho/filtro_periodo.js',
		];

		$data['bancas_combo'] = get_banca_multiselect($this->banca_model->listar_todas());
		$data['disciplinas_combo'] = get_disciplina_multiselect($this->disciplina_model->listar_todas());

		$query_string = http_build_query($_GET);
		if($query_string){
			$query_string = "?" . $query_string;
		}
		$data['query_string'] = $query_string;

	}
}