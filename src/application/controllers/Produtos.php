<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );

class Produtos extends CI_Controller {
	
	public function __construct() {
		parent::__construct ();
		session_start();

		$this->load->model ( 'caderno_model' );
		$this->load->model ( 'questao_model' );
		
		if(isset($_GET['profiler'])) {
			$this->output->enable_profiler(TRUE);
			error_reporting(E_ALL);
		}
	}
	
	public function listar()
	{
		tem_acesso(array(ADMINISTRADOR), ACESSO_NEGADO_SQ);
		
		$data['produtos'] = $this->caderno_model->listar_cadernos_produtos();
	}
	
	public function meus_cadernos($categoria_id = null)
	{
		
	}
	
}