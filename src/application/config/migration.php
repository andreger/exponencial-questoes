<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Enable/Disable Migrations
|--------------------------------------------------------------------------
|
| Migrations are disabled by default but should be enabled
| whenever you intend to do a schema migration.
|
*/
$config['migration_enabled'] = TRUE;


/*
|--------------------------------------------------------------------------
| Migrations version
|--------------------------------------------------------------------------
|
| This is used to set migration version that the file system should be on.
| If you run $this->migration->latest() this is the version that schema will
| be upgraded / downgraded to.
|
*/

//$config['migration_version'] = 20200106133500;
//$config['migration_version'] = 20200212142800;
//$config['migration_version'] = 20200214141800;
//$config['migration_version'] = 20200306082100;
#$config['migration_version'] = 20200312112000;
#$config['migration_version'] = 20200325132200;
//$config['migration_version'] = 20200328124400;
//$config['migration_version'] = 20200402092100;
//$config['migration_version'] = 20200408205000;
//$config['migration_version'] = 20200409163000;
//$config['migration_version'] = 20200427132000;
//$config['migration_version'] = 20200602140000;
//$config['migration_version'] = 20200615155100;
$config['migration_version'] = 20200625213300;

/*
|--------------------------------------------------------------------------
| Migrations Path
|--------------------------------------------------------------------------
|
| Path to your migrations folder.
| Typically, it will be within your application path.
| Also, writing permission is required within the migrations path.
|
*/
$config['migration_path'] = APPPATH . 'migrations/';

$config['migration_type'] = 'timestamp';


/* End of file migration.php */
/* Location: ./application/config/migration.php */