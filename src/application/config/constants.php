<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('MULTIPLA_ESCOLHA', 1);
define('CERTO_ERRADO', 2);
define('MULTIPLA_ESCOLHA_5', 3);
define('OPCAO_A', 1);
define('OPCAO_B', 2);
define('OPCAO_C', 3);
define('OPCAO_D', 4);
define('OPCAO_E', 5);
define('CERTO', 1);
define('ERRADO', 2);
define('SEM_AREA_FORMACAO', 0);
define('SEM_AREA_ATUACAO', 0);
define('OPCAO_ZERO', "");
define('SEM_ESCOLARIDADE', 0);
define('FUNDAMENTAL', 1);
define('MEDIO', 2);
define('SUPERIOR', 3);
define('SEM_DIFICULDADE', 0);
define('MUITO_FACIL', 1);
define('FACIL', 2);
define('MEDIA', 3);
define('DIFICIL', 4);
define('MUITO_DIFICIL', 5);
define('NORMAL', 1);
define('ANULADA', 2);
define('CANCELADA', 2);
define('DESATUALIZADA', 3);
define('TODAS', 6);
define('NAO_RESOLVIDAS', 1);
define('RESOLVIDAS', 2);
define('QUE_ACERTEI', 3);
define('QUE_ERREI', 4);
define('NOTIFICACAO_ERRO', 5);
define('FAVORITAS', 7);
define('ATIVO', 1);
define('INATIVO', 2);
define('INCONSISTENTE', 3);
define('QUE_ADQUIRI', 1);
define('QUE_NAO_ADQUIRI', 2);
define('GRATUITOS', 4);
define('ROTA_EXPONENCIAL', 5);
define('DO_COACHING', 3);
define('MAIS_NOVOS', 1);
define('MAIS_ANTIGOS', 2);
define('MENOR_PRECO', 3);
define('MAIOR_PRECO', 4);
define('NOME_ASC', 5);
define('PRIORIDADE', 6);
define('BUSCA_ADMIN', 0);
define('BUSCA_ALUNO', 1);
define('BUSCA_DESEMPENHO', 2);
define('BUSCA_EDITAVEIS', 3);

define('TIPO_ASSUNTO', 'ass');
define('TIPO_DISCIPLINA', 'dis');
define('TIPO_SIMULADO', 'sim');
define('TIPO_QUESTAO', 'que');
define('TIPO_BANCA', 'ban');
define('TIPO_PROVA', 'pro');
define('TIPO_BANCA_DISCIPLINA', 'ban_dis');

define('COM_COMENTARIOS_DE_PROFESSORES', 1);
define('COM_COMENTARIOS', 2);
define('NENHUM_COMENTARIO', 3);
define('COM_COMENTARIOS_DE_ALUNOS', 4);
define('MEUS_COMENTARIOS', 5);
define('COM_COMENTARIOS_EM_VIDEO', 7);//Era 6 mas o "Todas" usa o 6
define('COM_RESPOSTA', 1);
define('SEM_RESPOSTA', 2);
define('PENDENTE', 11);
define('REVISADO', 12);

define('DOS_MEUS_CADERNOS', 1);
define('REPETIDAS', 4);
define('NAO_REPETIDAS', 5);

define('LIMIT_LISTAR_QUESTOES', 1000/*400*/);
define('LIMIT_RANKING', 20);
define('LIMIT_IMPRIMIR_QUESTOES', 10000);
define('LIMIT_MEUS_RESULTADOS_SIMULADOS', 10);

define('LIMITE_NOTIFICACOES', 4);

define('ALERTA_SUCESSO', 1);
define('ALERTA_ERRO', 2);
define('ALERTA_ATENCAO', 3);
define('ALERTA_INFO', 4);

define('SEM_ASSUNTO', '(Sem assunto)');

define('SUCESSO', 'sucesso');
define('ERRO', 'erro');
define('AVISO', 'aviso');

define('TEMPO_GASTO_DEFAULT', 45);

define('GOOGLE_SHORTEN_URL_KEY', 'AIzaSyBTuAM8SR-u4EjcxK3Q8JkX4zgm9hBNqHc');

define('MAX_QUESTOES_ADICIONADAS_CADERNO', 5000);

define('QUEBRA_IMPRESSAO', "<div style='page-break-after: always;'></div>");

define('NUM_COLUNAS_GABARITO', 8);

define('ASSINANTE_SQ_META', 'assinante-sq');
define('ASSINANTE_SQ_VALIDADE_META', 'assinante-sq-validade');
define('ASSINANTE_SQ_EM_DIA', 'yes');
define('ASSINANTE_SQ_ATRASADO', 'arrear');

define('ASSUNTOS_POR_PAGINA', 100);
define('PROVAS_POR_PAGINA', 100);
define('SIMULADOS_POR_PAGINA', 18);
define('CADERNOS_POR_PAGINA', 20);
define('CADERNOS_POR_PAGINA_WIDGET', 18);
define('QUESTOES_ASSUNTOS_CORRETOR_POR_PAGINA', 50);

define('HISTORICO_DISCIPLINA', 1);
define('HISTORICO_STATUS', 2);
define('HISTORICO_DIFICULDADE', 3);
define('HISTORICO_ASSUNTOS', 4);
define('HISTORICO_GABARITO', 5);
define('HISTORICO_ATIVO', 6);
define('HISTORICO_ALTERNATIVAS', 7);
define('HISTORICO_ENUNCIADO', 8);
define('HISTORICO_TEXTO_BASE', 9);

define('CAMPO_DISCIPLINA', 1);
define('CAMPO_BANCA', 2);
define('CAMPO_ANO', 3);
define('CAMPO_ESCOLARIDADE', 4);
define('CAMPO_AREA_ATUACAO', 5);
define('CAMPO_STATUS', 6);
define('CAMPO_TIPO', 7);
define('CAMPO_COMENTARIOS_PROFESSORES', 8);
define('CAMPO_GABARITO', 9);
define('CAMPO_SITUACAO', 10);

define('SIMULADO_RESPOSTA_CERTA', '<div class="alert alert-success resposta-simulado">Você Acertou!</div>');
define('SIMULADO_RESPOSTA_ERRADA', '<div class="alert alert-danger resposta-simulado">Você Errou!</div>');

define('LIMITE_CADERNO', 5000); // Limite máximo de questões em um caderno 
define('CADERNOS_QUESTOES_ALEATORIAS', 1);
define('CADERNOS_QUESTOES_ESCOLHIDAS', 2);

define('ULTIMA_URL', 'ultima_url');

define('CATEGORIA_COMPARTILHADOS', 'Compartilhados');
define('CATEGORIA_SLUG_ROTA_EXPONENCIAL', 'rota-exponencial');

define('ENTIDADE_DISCIPLINA', 1);
define('ENTIDADE_BANCA', 2);

define('PRIORIDADE_DEFAULT', 99999);

define('TODOS_SIMULADOS', -1);
define('TODOS_SIMULADOS_ATIVOS', -2);
define('TODOS_SIMULADOS_MENOS_INATIVOS', -3);

define('TODOS_CADERNOS', -1);

define('URL_RETORNO', 'url_retorno');

define('FILTRO_KEY_RESOLVER_QUESTOES', 'filtro');
define('FILTRO_KEY_REVISAR_QUESTOES', 'filtro_revisar');
define('FILTRO_KEY_LISTAR_QUESTOES', 'filtro_listar');

define('TAMANHO_FONTE', 13);

define('STATUS_COMPARACAO_RESOLVIDA', 1);
define('STATUS_COMPARACAO_IGNORADA', 2);

define('ALUNO_TODOS', 0);
define('ALUNO_PODIO', 1);
define('ALUNO_COACHING', 3);

define('PAGE_MEU_DESEMPENHO', 0);
define('PAGE_SIMULADOS', 1);
define('PAGE_CADERNOS', 2);
define('PAGE_RESOLVER_QUESTOES', 3);
define('PAGE_REVISAR_QUESTOES', 4);
define('PAGE_RESOLVER_SIMULADO', 5);
DEFINE('PAGE_ATUALIZAR_CADERNO', 6);

define('SIMULADO_QUESTAO_TODAS', 0);
define('SIMULADO_QUESTAO_NAO_RESOLVIDAS', 1);
define('SIMULADO_QUESTAO_RESOLVIDAS', 2);
define('SIMULADO_QUESTAO_QUE_ACERTEI', 3);
define('SIMULADO_QUESTAO_QUE_ERREI', 4);

define('PRAZO_SIMULADO_TODOS', 0);
define('PRAZO_SIMULADO_ATRASADOS', 1);
define('PRAZO_SIMULADO_FUTURO', 2);
define('PRAZO_SIMULADO_PRESENTE', 3);
define('PRAZO_SIMULADO_SEM_PREVISAO', 4);

define('MEUS_CADERNOS_MAIS_RECENTE', 1);
define('MEUS_CADERNOS_MENOS_RECENTE', 2);
define('MEUS_CADERNOS_ACESSO_MAIS_RECENTE', 3);
define('MEUS_CADERNOS_ACESSO_MENOS_RECENTE', 4);
define('MEUS_CADERNOS_AZ', 5);
define('MEUS_CADERNOS_ZA', 6);
define('MEUS_CADERNOS_MAIS_ACERTOS', 7);
define('MEUS_CADERNOS_MENOS_ACERTOS', 8);
define('MEUS_CADERNOS_MAIS_ERROS', 9);
define('MEUS_CADERNOS_MENOS_ERROS', 10);

define('QUESTAO_FAVORITA', 1);
define('QUESTAO_ACOMPANHADA', 2);

define('QUESTOES_PROFESSOR_POR_PAGINA', 50);

define('MODO_QUESTOES_PADRAO', 1);
define('MODO_QUESTOES_COCKPIT', 2);

define('CONFIG_USUARIO_MOSTRAR_ACERTOS_ERROS', 1);
define('CONFIG_USUARIO_EXIBIR_FILTROS', 1);
define('CONFIG_USUARIO_MANTER_HISTORICO_CADERNO', 0);
define('CONFIG_USUARIO_ESCONDER_URL_QUESTAO', 0);
define('CONFIG_USUARIO_EXIBIR_ASSUNTOS', 1);
define('CONFIG_USUARIO_ORDERM_CABECALHO_QUESTAO', 'ID,Ano,Bancas,Órgãos,Provas,Disciplina,Assuntos');

/* Relatórios */
define('REL_QUESTOES_COMENTADAS_DATA_FIXADA', '2019-03-31');

switch ($_SERVER['HTTP_HOST']) {
	case 'localhost':
	case 'exponencial': 
	case 'homol.exponencialconcursos.com.br': {
		define('FACEBOOK_APP_ID', '843811369071446');
		define('FACEBOOK_APP_SECRET', '9b7e7edf90d0e111ff928d1bfb4e994c');
		break;
	}
	case 'exponencialconcursos.com.br': 
	case 'www.exponencialconcursos.com.br': {
		define('FACEBOOK_APP_ID', '842077389244844');
		define('FACEBOOK_APP_SECRET', 'd46cae050603ec944210454cf4f1a48f');
		break;
	}
}

/* End of file constants.php */
/* Location: ./application/config/constants.php */
