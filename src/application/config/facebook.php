<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['facebook_app_id']              = FACEBOOK_APP_ID;
$config['facebook_app_secret']          = FACEBOOK_APP_SECRET;
$config['facebook_login_type']          = 'web';
$config['facebook_login_redirect_url']  = 'login/facebook';
$config['facebook_logout_redirect_url'] = 'login/logout';
$config['facebook_permissions']         = array('email');
$config['facebook_graph_version']       = 'v2.6';
$config['facebook_auth_on_load']        = TRUE;