<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relatorios_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_ultima_atualizacao()
	{
		$this->db->order_by('qcp_data', 'desc');
		$query = $this->db->get('questoes_comentadas_professor');
		
		$resultado = $query->row_array();
		
		return $resultado['qcp_data'];
	}
	
	public function inserir_questoes_respondidas_dia($comentarios_agrupados)
	{
		
		foreach($comentarios_agrupados as $comentarios){
			if(is_professor($comentarios['user_id'])){
				$dados = array(
						'user_id' => $comentarios['user_id'],
						'qcp_data' => $comentarios['data'],
						'qcp_qtde_comentada' => $comentarios['qtde_professor'],
				);
				$this->db->insert('questoes_comentadas_professor', $dados);
			}
		}
	}
	
	public function existe_relatorio_ontem($ontem)
	{
		$this->db->where("qcp_data", $ontem);
		$query = $this->db->get("questoes_comentadas_professor");
		
		if($query->num_rows() > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	
	public function excluir_questoes_respondidas_mes($mes)
	{
		$this->db->where("month(qcp_data) = {$mes}");
		$this->db->delete("questoes_comentadas_professor");
	}
	
	public function contar_questoes_comentadas_por_usuario($mes, $ano, $user_id = false, $pos_travamento = FALSE)
	{

		/** 
		 * @see constants.php.REL_QUESTOES_COMENTADAS_DATA_FIXADA
		 * @see B2595 - Travar relatorio de questões comentadas (mudança de % pra fixo)
		 */
		$dia_travamento = "";
		if($pos_travamento == TRUE)
		{
			$dia_travamento = REL_QUESTOES_COMENTADAS_DATA_FIXADA;
			$dia_travamento = " and qcp_data > '" . $dia_travamento . "' ";
		}

		$primeiro_dia_mes = date("Y-m-d", strtotime($ano."-".$mes."-01"));
		$ultimo_dia_mes = date('Y-m-t', strtotime($primeiro_dia_mes));
		
		$this->db->select("sum(qcp_qtde_comentada) as qcp_qtde_comentada", false);
		$this->db->where("qcp_id in (
							select qcp_id from questoes_comentadas_professor
						    where qcp_data <= '{$ultimo_dia_mes}' {$dia_travamento}
						)");
		if($user_id){
			$this->db->where("user_id", $user_id);
		}

		$query = $this->db->get("questoes_comentadas_professor");
	
		if($query->num_rows() > 0){
			$result = $query->row_array();
			
			return $result['qcp_qtde_comentada'];
		}
		else{
			return FALSE;
		}
	}
	
	public function get_questoes_comentadas($mes, $ano, $user_id = false)
	{
		$primeiro_dia_mes = date("Y-m-d", strtotime($ano."-".$mes."-01"));
		$ultimo_dia_mes = date('Y-m-t', strtotime($primeiro_dia_mes));
		
		$this->db->select("sum(qcp_qtde_comentada) as qcp_qtde_comentada, user_id", false);
		$this->db->where("qcp_id in (
							select qcp_id from questoes_comentadas_professor
						    where qcp_data <= '{$ultimo_dia_mes}'
						)");
		if($user_id){
			$this->db->where("user_id", $user_id);
		}
		$this->db->group_by("user_id");
		$this->db->order_by("qcp_qtde_comentada desc");
		$query = $this->db->get("questoes_comentadas_professor");
	
		if($query->num_rows() > 0){
			$resultado = $query->result_array();

			$i = 0;
			foreach ($resultado as &$item) {
				$i++;
				$item['posicao'] = $i;
			}

			return $resultado;
		}
		else{
			return FALSE;
		}
	}

	public function travar_questoes_comentadas()
	{
		//Apaga os dados atuais e se já passou da data de fixação, gera os dados novamente
		$this->db->where('1=1');
		$this->db->delete('questoes_comentadas_professor_travado');

		if(time() >= strtotime(REL_QUESTOES_COMENTADAS_DATA_FIXADA))
		{

			$this->db->select('user_id');
			$this->db->distinct();
			$this->db->from('questoes_comentadas_professor');
			$this->db->where('qcp_data <=', REL_QUESTOES_COMENTADAS_DATA_FIXADA);

			$query = $this->db->get();

			$inserts = array();
			foreach($query->result_array() as $row)
			{
				$data_fixa_arr = explode('-', REL_QUESTOES_COMENTADAS_DATA_FIXADA);
				$total = self::contar_questoes_comentadas_por_usuario($data_fixa_arr[1], $data_fixa_arr[0], $row['user_id']);
				array_push($inserts, array(
					'usu_id' => $row['user_id'],
					'qct_total' => $total
				));
			}

			//Total de comentários de professor
			$total = self::contar_questoes_comentadas_por_usuario($data_fixa_arr[1], $data_fixa_arr[0]);
			array_push($inserts, array(
				'usu_id' => null,
				'qct_total' => $total
			));

			$this->db->insert_batch('questoes_comentadas_professor_travado', $inserts);
		}
	}

	public function get_questoes_comentadas_travado($user_id = null)
	{
		if(is_null($user_id))
		{
			$this->db->where('usu_id IS NULL');
		}
		else
		{
			$this->db->where('usu_id', $user_id);
		}

		$this->db->select('qct_total');
		$this->db->from('questoes_comentadas_professor_travado');

		$query = $this->db->get();

		$result = $query->row_array();

		if($result)
		{
			return $result['qct_total'];
		}
		else
		{
			return null;
		}

	}

	public function get_posicao($mes, $ano, $user_id)
	{
		$resultado = self::get_questoes_comentadas($mes, $ano);

		if($resultado)	{
			foreach ($resultado as $item) {
				if($item['user_id']  == $user_id) {
					return $item['posicao'];
				}
			}

		}

		return NULL;
	}

	public function get_num_participantes($mes = NULL, $ano = NULL)
	{
		if(is_null($mes)) {
			$mes = date('m');
		}

		if(is_null($ano)) {
			$ano = date('Y');
		}

		$qc = self::get_questoes_comentadas($mes, $ano);

		return $qc ? count($qc) : 0;
	}
	
	public function get_totais_pagseguro($mes, $ano)
	{
		$primeiro_dia_mes = date("Y-m-d", strtotime($ano."-".$mes."-01"));
		$ultimo_dia_mes = date('Y-m-t', strtotime($primeiro_dia_mes));
		
		$this->db->select("sum(tpd_receita) as receita, sum(tpd_pagseguro) as pagseguro, sum(tpd_imposto) as imposto, sum(tpd_cancelamento) as cancelamento, sum(tpd_receita_liquida) as receita_liquida", false);
		$this->db->where("tpd_id in (
							select tpd_id from totais_pagseguro_dia
						    where tpd_dia between '{$primeiro_dia_mes}' and '{$ultimo_dia_mes}'
						)");
		$query = $this->db->get("totais_pagseguro_dia");
	
		if($query->num_rows() > 0){
			return $query->row_array();
		}
		else{
			return FALSE;
		}
	}
	
	public function get_valor_acumulado_user($user_id, $mes, $ano)
	{
		
		$this->db->select("pap_acumulado");
		$this->db->where("month(pap_mes) = {$mes}");
		$this->db->where("year(pap_mes) = {$ano}");
		$this->db->where("user_id", $user_id);
		
		$query = $this->db->get("pag_acumulado_prof");
		
		if($query->num_rows() > 0){
			return $query->row_array();
		}
		else{
			return FALSE;
		}
	}

	public function salvar_valor_acumulado($user_id, $mes, $ano, $acumulado)
	{
		//apaga registros do mes, caso exista
		$this->db->where('pap_mes', $ano . '-' . $mes . '-10');
		$this->db->where('user_id', $user_id);
		$this->db->delete('pag_acumulado_prof');

		$dados = array(
			'pap_mes' => $ano . '-' . $mes . '-10',
			'pap_acumulado' => $acumulado,
			'user_id' => $user_id
		);

		$this->db->insert('pag_acumulado_prof', $dados);
	}
	
	public function salvar_total_pedidos_dia($dia, $total_receita, $total_pagseguro, $total_imposto)
	{
		//apaga registros do dia, caso exista
		$this->db->where('tpd_dia', $dia);
		$this->db->delete('totais_pagseguro_dia');
		
		$dados = array(
				'tpd_dia' => $dia,
				'tpd_receita' => $total_receita,
				'tpd_pagseguro' => $total_pagseguro,
				'tpd_imposto'	=> $total_imposto,
				'tpd_receita_liquida' => ($total_receita - $total_pagseguro) - $total_imposto
		);
		$this->db->insert('totais_pagseguro_dia', $dados);
	}

	public function get_avaliacao_comentario_questao($is_total, $professor_id, $media_avaliacoes, $comparador_media_avaliacoes, $quantidade_avaliacoes, $ordem, $pos_ini = 0, $offset = 10){
		
		if($is_total){
			$this->db->select('count(com.com_id) as qtd, sum(com.com_media_avaliacao) as media, sum(com.com_media_simples_avaliacoes) as media_simples');
		}else{
			$this->db->select('que.que_id, que.que_url_reduzida, com.user_id, com.com_qtd_avaliacoes as qtd, com.com_media_avaliacao as media, com.com_media_simples_avaliacoes as media_simples, com.com_media_qtd_avaliacoes as qtd_media');
		}

		$this->db->from('comentarios com');
		$this->db->join('questoes que', 'que.que_id = com.que_id');

		$this->db->where('com.com_destaque', true);
		$this->db->where('com.com_media_avaliacao is not null');
		if($professor_id){
			$this->db->where('com.user_id', $professor_id);
		}
		
		if($quantidade_avaliacoes){
			$this->db->where('com.com_qtd_avaliacoes >=', $quantidade_avaliacoes);
		}

		if($media_avaliacoes && $comparador_media_avaliacoes){
			$this->db->where('com.com_media_avaliacao '.$comparador_media_avaliacoes, $media_avaliacoes);
		}
		
		if(!$is_total){
			$this->db->order_by($ordem);
			$this->db->limit($offset, $pos_ini);
		}

		$query = $this->db->get();

		if($query->num_rows() > 0){
			if($is_total){
				return $query->row_array();
			}else{
				return $query->result_array();
			}
		}
		else{
			return FALSE;
		}

	}
}