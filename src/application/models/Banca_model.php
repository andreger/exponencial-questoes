<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banca_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function listar_por_ids($ids) {
		$this->db->from('bancas');
		$this->db->where_in('ban_id', $ids);
		$this->db->order_by('ban_nome', 'asc');
		$query = $this->db->get();
		
		return $query->result_array();
	}

	public function listar_todas()
	{
		$this->db->from('bancas');
		$this->db->order_by('ban_nome');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_by_id($banca_id)
	{
		$query = $this->db->get_where('bancas', array('ban_id' => $banca_id));
		return $query->row_array();
	}
	
	public function get_by_nome($nome, $incluir_novo_registro = false)
	{
		$nome = trim($nome);
		$query = $this->db->get_where('bancas', array('ban_nome' => $nome));
	
		$result = $query->row_array();
	
		if(!$incluir_novo_registro)
			return $result;
	
		if(count($result) > 0)
			return $result;
	
		$data = array('ban_nome' => $nome);
		self::salvar($data);
		return self::get_by_nome($nome);
	}

	public function salvar($banca)
	{
		return $this->db->insert('bancas', $banca);
	}

	public function atualizar($banca)
	{
		$this->db->where('ban_id', $banca['ban_id']);
		$this->db->set($banca);
		return $this->db->update('bancas');
	}

	public function excluir($banca_id)
	{
		$this->db->where('ban_id', $banca_id);
		return $this->db->delete('bancas');
	}
}