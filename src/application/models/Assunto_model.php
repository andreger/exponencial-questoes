<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Assunto_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function listar_todas()
	{
		self::joins();
		$this->db->from('assuntos a');
		$this->db->order_by('a.ass_nome');
		$query = $this->db->get();
	
		return $query->result_array();
	}
	
	public function listar_filhos($pai_id, &$filhos = null, $somente_ativos = TRUE)
	{

		if($somente_ativos == TRUE)
		{
			$this->db->where('ass_ativo', ATIVO);
		}

		$this->db->where('ass_pai_id', $pai_id);
		$query = $this->db->get('assuntos');
	
		if(is_null($filhos)) {
			$filhos = array();
		}
		
		$resultado = $query->result_array();
		if($resultado) {
			foreach ($query->result_array() as $filho) {
				array_push($filhos, $filho);
				self::listar_filhos($filho['ass_id'], $filhos, $somente_ativos);
			}
		}
		
		return $filhos;
		
	}
	
	public function get_arvore($disciplina_id, $assunto_id = null, $order_by = 'ass_nome')
	{
		$filhos = self::listar_arvore_filhos($disciplina_id, $assunto_id, $order_by);
	
		foreach ($filhos as &$filho) { 
			/** [JOULE][J2] UPGRADE - Definir ordem de assuntos da Taxonomia
 			 * 	https://podio.com/profinfnetedubr/lslc/apps/upgrades/items/334
			 *
			 *	Somente a primeira ordenação leva em conta a prioridade. A partir da segunda a 
			 *	ordem alfabética é utilizada.
			 */

			$filho['filhos'] = self::get_arvore($disciplina_id, $filho['ass_id'], 'ass_nome');
		}
	
		return $filhos;
	}
	
	public function listar_arvore_filhos($disciplina_id, $assunto_id = null, $order_by = 'ass_nome')
	{
		$this->db->where('dis_id', $disciplina_id);
		$this->db->where('ass_pai_id', $assunto_id);
		$this->db->where('ass_ativo', SIM);
	
		$this->db->order_by($order_by);
		$query = $this->db->get('assuntos');
	
		$result = $query->result_array();
		
		if($order_by == 'ass_nome')
		{
    		usort($result, function($a, $b){
    		    return strnatcasecmp($a['ass_nome'], $b['ass_nome']);
    		});
		}
		
		return $result;
	}

	/**
	 * Verifica se o assunto possui filhos ativos
	 * 
	 * @since G1
	 * 
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1097
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2330
	 *
	 * @param int $assunto_id Id do assunto
	 * 
	 * @return bool
	 */ 
	
	public function possui_filhos($assunto_id)
	{
		$this->db->from('assuntos');
		$this->db->where('ass_pai_id', $assunto_id);
		$this->db->where('ass_ativo', SIM);
		
		return $this->db->count_all_results() > 0 ? true : false; 
	}
	
	public function buscar($offset = 0, $palavra = null, $ativo = null, $inativo = null)
	{
		self::joins();
		$this->db->from('assuntos a');
		
		if(!is_null($palavra)) {
			$like = "(a.ass_nome like '%{$palavra}%' or d.dis_nome like '%{$palavra}%')";
			$this->db->where($like);
			//$this->db->like('a.ass_nome', $palavra);
			//$this->db->or_like('d.dis_nome', $palavra);
		}
		
		if( ( ATIVO == $ativo && INATIVO != $inativo ) ){
			$this->db->where('a.ass_ativo', ATIVO);
		}else if( ATIVO != $ativo && INATIVO == $inativo ){
			$this->db->where('a.ass_ativo', INATIVO);
		}

		$this->db->order_by('a.ass_nome');
		$this->db->limit(ASSUNTOS_POR_PAGINA, $offset);
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function contar($palavra = null, $ativo = null, $inativo = null)
	{
		self::joins();
		$this->db->from('assuntos a');
		
		if(!is_null($palavra)) {
			$like = "(a.ass_nome like '%{$palavra}%' or d.dis_nome like '%{$palavra}%')";
			$this->db->where($like);
			//$this->db->like('a.ass_nome', $palavra);
			//$this->db->or_like('d.dis_nome', $palavra);
		}
		
		if( ( ATIVO == $ativo && INATIVO != $inativo ) ){
			$this->db->where('a.ass_ativo', ATIVO);
		}else if( ATIVO != $ativo && INATIVO == $inativo ){
			$this->db->where('a.ass_ativo', INATIVO);
		}

		return $this->db->count_all_results();
	}

	public function listar_por_disciplina($disciplina_id,  $somente_ativos = TRUE)
	{

		if($somente_ativos) {
			$this->db->where('ass_ativo', SIM);
		}

		$this->db->where('dis_id', $disciplina_id);

		$this->db->order_by('ass_nome');
		$query = $this->db->get('assuntos');

		return $query->result_array();
	}

	/**
	 * Lista os assuntos raízes de uma disciplina, ou seja, aqueles que não 
	 * possuem assunto-pai
	 * 
	 * @since 10.0.0
	 *
	 * @param int $disciplina_id Id da disciplina
	 * @param string $order_by Ordenação da listagem
	 * @param bool $somente_ativos Restringe, ou não, a assuntos ativos
	 */

	public function listar_raizes($disciplina_id, $order_by = 'ass_nome', $somente_ativos = true)
	{
		// restrição de assuntos ativos
		if($somente_ativos) {
			$this->db->where('ass_ativo', SIM);
		}

		// ignora o SEM_ASSUNTO da disciplina
		$this->db->not_like('ass_nome', SEM_ASSUNTO);
		$this->db->where('ass_pai_id IS NULL');
		$this->db->where('dis_id', $disciplina_id);
		$this->db->order_by($order_by);
		$query = $this->db->get('assuntos');

		return $query->result_array();
	}
	
	public function listar_por_disciplinas($disciplinas_ids, $somente_ativos = TRUE, $order_by = 'ass_nome')
	{
		// foreach ($disciplinas_ids as $item) {
		// 	$this->db->or_where('dis_id', $item);
		// }

		if($somente_ativos) {
			$this->db->where('ass_ativo', SIM);
		}

		$this->db->where_in('dis_id', $disciplinas_ids);
		$this->db->order_by($order_by);
		$query = $this->db->get('assuntos');

		return $query->result_array();
	}
	
	public function listar_ids_por_disciplina($disciplina_id)
	{
		$assuntos = self::listar_por_disciplina($disciplina_id);
		
		$assuntos_ids = array();
		foreach ($assuntos as $assunto) {
			array_push($assuntos_ids, $assunto['ass_id']);
		}
		
		return $assuntos_ids;
	}

	public function get_by_nome($nome)
	{
		$query = $this->db->get_where('assuntos', array('ass_nome' => $nome));
		return $query->row_array();
	}
	
	public function get_by_nome_e_disciplina($nome, $disciplina_id)
	{
		$query = $this->db->get_where('assuntos', array(
			'ass_nome' => $nome,
			'dis_id' => $disciplina_id			
		));
		
		return $query->row_array();
	}
	
	public function get($assunto, $incluir_novo_registro = false)
	{
		$query = $this->db->get_where('assuntos', $assunto);
	
		$result = $query->row_array();
	
		if(!$incluir_novo_registro)
			return $result;
	
		if(count($result) > 0)
			return $result;
	
		$assunto['ass_ativo'] = NAO;
		self::salvar($assunto);
		
		return self::get($assunto);
	}
	
	public function get_by_id($assunto_id)
	{
		$query = $this->db->get_where('assuntos', array('ass_id' => $assunto_id));
		return $query->row_array();
	}

	public function get_total_questoes($assunto_id) {
		$query = $this->db->get_where('questoes_assuntos', array('ass_id' => $assunto_id));
		return count($query->result_array());
	}
	public function salvar($assunto)
	{
		return $this->db->insert('assuntos', $assunto);
	}

	public function atualizar($assunto)
	{
		$this->db->where('ass_id', $assunto['ass_id']);
		$this->db->set($assunto);
		return $this->db->update('assuntos');
	}

	public function excluir($assunto_id)
	{
		self::remover_filhos($assunto_id);
		
		$this->db->where('ass_id', $assunto_id);
		return $this->db->delete('assuntos');
	}
	
	public function remover_filhos($pai_id)
	{
		$this->db->where('ass_pai_id', $pai_id);
		$this->db->set('ass_pai_id', null);
		return $this->db->update('assuntos');
	}

	public function listar_conversoes($disciplinas_ids_antigos = NULL, $disciplinas_ids_novos = NULL, $dados_detalhados = TRUE)
	{
		$this->db->from('assuntos_conversao ac');

		if($dados_detalhados) {
			$this->db->select('ac.dis_id, a1.ass_id as ass_antigo_id, a1.ass_nome as ass_antigo_nome, d1.dis_nome as dis_antigo_nome, a2.ass_id as ass_novo_id, a2.ass_nome as ass_novo_nome, d2.dis_nome as dis_novo_nome, ac.asc_data_criacao');
			$this->db->join('assuntos a1', 'ac.ass_antigo = a1.ass_id', 'left');
			$this->db->join('disciplinas d1', 'ifnull(a1.dis_id, ac.dis_id) = d1.dis_id');
			$this->db->join('assuntos a2', 'ac.ass_novo = a2.ass_id');
			$this->db->join('disciplinas d2', 'a2.dis_id = d2.dis_id');
		}

		if($disciplinas_ids_antigos) {
			$this->db->where_in('d1.dis_id', $disciplinas_ids_antigos);
		}
		if($disciplinas_ids_novos) {
			$this->db->or_where_in('d2.dis_id', $disciplinas_ids_novos);
		}

		if($dados_detalhados) {
//			$this->db->order_by('ass_antigo_nome ASC, dis_antigo_nome ASC, ass_novo_nome ASC, dis_novo_nome ASC');
			$this->db->order_by('ac.asc_data_criacao DESC');
		}

		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_conversao($assunto_antigo, $disciplina = null)
	{
		
		if($disciplina){
			return self::get_conversao_default($disciplina);
		}

		$this->db->where('ass_antigo', $assunto_antigo);
		$query = $this->db->get('assuntos_conversao');

		return $query->row_array();
	}

	public function get_conversao_default($disciplina){
		$this->db->where('dis_id', $disciplina);
		$query = $this->db->get('assuntos_conversao');
		return $query->row_array();
	}

	public function adicionar_conversao($assunto_antigo, $assunto_novo, $disciplina = null)
	{
		$conversao = self::get_conversao($assunto_antigo, $disciplina);

		if(!$conversao) {

			$dados = array(
				'ass_antigo' => $assunto_antigo,
				'ass_novo' => $assunto_novo,
				'dis_id' => $disciplina,
				'asc_data_criacao' => get_data_hora_agora()
			);

			$this->db->insert('assuntos_conversao', $dados);
		}


	}

	public function excluir_conversao($assunto_antigo_id)
	{
		$this->db->where('ass_antigo', $assunto_antigo_id);
		$this->db->delete('assuntos_conversao');
	}
	
	/**
	 * Atualiza a prioridade de um determinado assunto
	 * 
	 * [JOULE][J2] UPGRADE - Definir ordem de assuntos da Taxonomia
	 * 
	 * @see https://podio.com/profinfnetedubr/lslc/apps/upgrades/items/334
	 * 
	 * @since 10.1.0
	 * @param int|null $assunto_id Id do assunto
	 * @param int $prioridade Valor da prioridade. O valor 1 é o mais alto.
	 */

	public function atualizar_prioridade($assunto_id, $prioridade)
	{
		$this->db->where('ass_id', $assunto_id);	
		$this->db->set('ass_prioridade', $prioridade);
		$this->db->update('assuntos');
	}

	public function atualizar_quantidade_questoes()
	{
		$sql = "update exponenc_corp.assuntos ass
				set ass.ass_qtd_questoes = (
					select count(*) from exponenc_corp.questoes_assuntos qa where qa.ass_id = ass.ass_id
				)
				where ass.ass_id > 0;";


		$this->db->query($sql);

		self::atualizar_quantidade_questoes_filhos();
	}

	public function atualizar_quantidade_questoes_filhos($pai_id = null)
	{
		if($pai_id) {
			$pai = self::get_by_id($pai_id);
			$qtde_questoes = $pai['ass_qtd_questoes'];
		}

		// lista assuntos filhos
		$this->db->where('ass_pai_id', $pai_id);
		$query = $this->db->get('assuntos');

		if($resultado = $query->result_array()) {

			$qtde_questoes_filhos = 0;

			foreach ($resultado as $filho) {
				set_time_limit(30);

				$qtde_questoes_filhos += (int)self::atualizar_quantidade_questoes_filhos($filho['ass_id']);
			}

			if($pai_id) {
				$qtde_questoes += $qtde_questoes_filhos;
				
				$sql = "update exponenc_corp.assuntos
					set ass_qtd_questoes = $qtde_questoes where ass_id = {$pai['ass_id']}";
			
				$this->db->query($sql);	
			}

		}

		if($pai_id) {
			return $pai['ass_qtd_questoes'];
		}

		return 0;
	}

	/**
	 * Lista as questões afetadas em uma possível conversão presentes em simulados ativos
	 * 
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1997
	 * 
	 * @since k2
	 * 
	 * @param int $assunto_id Id do assunto
	 * 
	 * @return array
	 */

	public function listar_questoes_afetadas_conversao($assunto_id)
	{
		$status = ATIVO;
		
		$sql = "SELECT sq.que_id, sq.sim_id FROM simulados_questoes sq JOIN simulados s ON (sq.sim_id = s.sim_id AND s.sim_status = $status) JOIN questoes q ON q.que_id = sq.que_id JOIN questoes_assuntos qa ON q.que_id = qa.que_id WHERE qa.ass_id = $assunto_id";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	private function joins()
	{
		$this->db->select('a.*,d.*,ap.ass_nome as ass_pai_nome');
		$this->db->join('disciplinas d', 'd.dis_id = a.dis_id', 'left');
		$this->db->join('assuntos ap', 'ap.ass_id = a.ass_pai_id', 'left'); // join para assuntos pai
	}
}