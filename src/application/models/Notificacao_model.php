<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Classe responsável pela manipulação no banco de dados referente às notificações
 * 
 * @author João Paulo Ferreira <jpaulofms@gmail.com>
 * 
 * @since L1
 */
class Notificacao_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		//$this->output->enable_profiler(TRUE);
	}

	/**
	 * Contar notificações
	 * 
	 * @since L1
	 * 
	 * @param int $usuario_id ID do usuário destinatário
	 * @param bool $is_lida Se a notificações já tevem ter sido lidas ou não. Enviar nulo para todas
	 * 
	 * @return int Total de notificações
	 */
	public function contar_por_usuario($usuario_id, $is_lida = NULL)
	{
		if(!is_null($is_lida))
		{
			if($is_lida == TRUE)
			{
				$this->db->where('not_data_lida IS NOT NULL');
			}
			else
			{
				$this->db->where('not_data_lida IS NULL');
			}
		}

		$this->db->where('not_destinatario_id', $usuario_id);

		$this->db->from('notificacoes');

		return $this->db->count_all_results();
	}

	/**
	 * Retorna uma notificação com base em seu ID
	 * 
	 * @since L1
	 * 
	 * @param int $notificacao_id ID da notificação
	 * 
	 * @return array contendo os dados da notificação, caso ela exista
	 */
	public function get_notificacao($notificacao_id)
	{
		$this->db->where("not_id", $notificacao_id);
		$query = $this->db->get("notificacoes");
		return $query->row_array();
	}

	/**
	 * Lista as notificações para um usuário
	 * 
	 * @since L1
	 * 
	 * @param int $usuario_id ID do usuário destinatário
	 * @param bool $is_lida Se a notificações já tevem ter sido lidas ou não. Enviar nulo para todas
	 * 
	 * @return array contendo os dados das notificações
	 */
	public function listar_por_usuario($usuario_id, $is_lida = NULL, $offset = 0, $limite = LIMITE_NOTIFICACOES)
	{
		if(!is_null($is_lida))
		{
			if($is_lida == TRUE)
			{
				$this->db->where('not_data_lida IS NOT NULL');
			}
			else
			{
				$this->db->where('not_data_lida IS NULL');
			}
		}

		$this->db->where('not_destinatario_id', $usuario_id);

		$this->db->select('not.*, com.que_id, com.com_texto, dis.dis_nome');
		$this->db->from('notificacoes not');
		$this->db->join('comentarios com', 'com.com_id = not.com_id');
		$this->db->join('questoes que', 'que.que_id = com.que_id');
		$this->db->join('disciplinas dis', 'que.dis_id = dis.dis_id');

		$this->db->order_by('not_data_criacao', 'DESC');

		$this->db->limit($limite);
		$this->db->offset($offset);
		
		$query = $this->db->get();
		
		return $query->result_array();
	}

	/**
	 * Marca a data de leitura de uma notificação
	 * 
	 * @since L1
	 * 
	 * @param $notificacao_id ID da notificação
	 */
	public function marcar_como_lida_nao_lida($notificacao_id, $is_lida = TRUE)
	{
		$this->db->where('not_id', $notificacao_id);
		if($is_lida)
		{
			$this->db->set('not_data_lida', date('Y-m-d H:i:s'));
		}
		else
		{
			$this->db->set('not_data_lida', NULL);
		}
		
		return $this->db->update('notificacoes');
	}

	/**
	 * Marca a data de leitura de todas as notificações não lidas de um usuário
	 * 
	 * @since L1
	 * 
	 * @param $usuario_id ID do usuário
	 */
	public function marcar_todas_como_lidas($usuario_id)
	{
		$this->db->where('not_destinatario_id', $usuario_id);
		$this->db->set('not_data_lida', date('Y-m-d H:i:s'));
		return $this->db->update('notificacoes');
	}

	/**
	 * Salva uma nova notificação
	 * 
	 * @since L1
	 * 
	 * @param array $notificacao_id Array contendo os dados da notificação
	 */
	public function salvar($notificacao)
	{
		$notificacao['not_data_criacao'] = date('Y-m-d H:i:s');
		return $this->db->insert('notificacoes', $notificacao);
	}

}