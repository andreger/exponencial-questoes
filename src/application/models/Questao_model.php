<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questao_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function listar_todas($extra_info = false, $limit = LIMIT_LISTAR_QUESTOES, $offset = 0)
	{
		$this->db->from('questoes q');

		if($extra_info) {
			$this->db->join('questoes_provas qp', 'q.que_id = qp.que_id');
			$this->db->join('provas p', 'qp.pro_id = p.pro_id');
			$this->db->join('bancas b', 'p.ban_id = b.ban_id');
			$this->db->join('orgaos o', 'p.org_id = o.org_id');
			$this->db->join('disciplinas d', 'd.dis_id = q.dis_id');
		}

		if($limit) {
			$this->db->limit($limit, $offset);
		}

		$query = $this->db->get();
		return $query->result_array();
	}

	public function atualizar_termos($tabela, $campo, $valor_antigo, $valor_novo)
	{
		$sql = "UPDATE {$tabela}
			SET {$campo} = REPLACE({$campo}, '{$valor_antigo}', '{$valor_novo}')
			WHERE {$campo} LIKE BINARY '%{$valor_antigo}%'";

		$this->db->query($sql);
	}

	public function atualizacao_rapida($questao)
	{
		$questao_antiga = self::get_by_id($questao['que_id']);
		$assuntos_antigos = self::get_assuntos_questao($questao['que_id']);

		if($questao['dis_id']) {
			$this->db->set('dis_id', $questao['dis_id']);
		}

		if($questao['que_dificuldade']) {
			$this->db->set('que_dificuldade', $questao['que_dificuldade']);
		}

		$this->db->set('que_status', $questao['que_status']);
		$this->db->set('que_resposta', $questao['que_resposta']);
		$this->db->set('que_ativo', $questao['que_ativo']);
		$this->db->where('que_id', $questao['que_id']);
		$this->db->update('questoes');

		self::atualizar_assuntos($questao['que_id'], $questao['ass_ids']);

		$questao_nova = self::get_by_id($questao['que_id']);
		$assuntos_novos = self::get_assuntos_questao($questao['que_id']);

		self::verificar_alteracoes($questao_antiga, $questao_nova, $assuntos_antigos, $assuntos_novos);
		self::atualizar_inativa_manual($questao_antiga, $questao_nova);
	}

	public function verificar_alteracoes($questao_antiga, $questao_nova, $assuntos_antigos, $assuntos_novos)
	{
		$historico_base = array(
			'que_id' => $questao_antiga['que_id'],
			'usu_id' => get_current_user_id(),
			'qhi_data' => date('Y-m-d H:i:s')
		);

		// Disciplina
		if($questao_antiga['dis_id'] != $questao_nova['dis_id']) {
			$disciplina_antiga = $this->disciplina_model->get_by_id($questao_antiga['dis_id']);
			$disciplina_nova = $this->disciplina_model->get_by_id($questao_nova['dis_id']);

			$historico = $historico_base;
			$historico['qhi_campo'] = HISTORICO_DISCIPLINA;
			$historico['qhi_valor_antigo'] = $disciplina_antiga['dis_nome'];
			$historico['qhi_valor_novo'] = $disciplina_nova['dis_nome'];

			self::salvar_historico($historico);
		}

		// Status
		if($questao_antiga['que_status'] != $questao_nova['que_status']) {
			$status_antigo = get_questao_status_nome($questao_antiga['que_status'], true);
			$status_novo = get_questao_status_nome($questao_nova['que_status'], true);

			$historico = $historico_base;
			$historico['qhi_campo'] = HISTORICO_STATUS;
			$historico['qhi_valor_antigo'] = $status_antigo;
			$historico['qhi_valor_novo'] = $status_novo;

			self::salvar_historico($historico);
		}

		// Dificuldade
		if((int)$questao_antiga['que_dificuldade'] != (int)$questao_nova['que_dificuldade']) {

			$dificuldade_antiga = get_questao_dificuldade_nome($questao_antiga['que_dificuldade'], true);
			$dificuldade_nova = get_questao_dificuldade_nome($questao_nova['que_dificuldade'], true);

			$historico = $historico_base;
			$historico['qhi_campo'] = HISTORICO_DIFICULDADE;
			$historico['qhi_valor_antigo'] = $dificuldade_antiga;
			$historico['qhi_valor_novo'] = $dificuldade_nova;

			self::salvar_historico($historico);
		}

		// Assuntos
		$assuntos_antigos_str = get_assuntos_str($assuntos_antigos);
		$assuntos_novos_str = get_assuntos_str($assuntos_novos);
		if($assuntos_antigos_str != $assuntos_novos_str) {
			$historico = $historico_base;
			$historico['qhi_campo'] = HISTORICO_ASSUNTOS;
			$historico['qhi_valor_antigo'] = $assuntos_antigos_str;
			$historico['qhi_valor_novo'] = $assuntos_novos_str;

			self::salvar_historico($historico);
		}

		// Gabarito
		if($questao_antiga['que_resposta'] != $questao_nova['que_resposta']) {

			$campo_antigo = get_letra_opcao_questao($questao_antiga['que_resposta'], $questao_antiga['que_tipo']);
			$campo_novo = get_letra_opcao_questao($questao_nova['que_resposta'], $questao_nova['que_tipo']);

			$historico = $historico_base;
			$historico['qhi_campo'] = HISTORICO_GABARITO;
			$historico['qhi_valor_antigo'] = ucfirst($campo_antigo);
			$historico['qhi_valor_novo'] = ucfirst($campo_novo);

			self::salvar_historico($historico);
		}

		// Ativo/Inativo
		if($questao_antiga['que_ativo'] != $questao_nova['que_ativo']) {

			$campo_antigo = $questao_antiga['que_ativo'] == ATIVO ? "Ativo" : "Inativo";
			$campo_novo = $questao_nova['que_ativo'] == ATIVO ? "Ativo" : "Inativo";

			$historico = $historico_base;
			$historico['qhi_campo'] = HISTORICO_ATIVO;
			$historico['qhi_valor_antigo'] = $campo_antigo;
			$historico['qhi_valor_novo'] = $campo_novo;

			self::salvar_historico($historico);
		}

	}

	public function salvar_historico($historico)
	{
		$this->db->insert('questoes_historico', $historico);
	}

	public function listar_historicos($questao_id)
	{
		$this->db->where('que_id', $questao_id);
		$this->db->order_by('qhi_data', 'desc');
		$query = $this->db->get('questoes_historico');

		return $query->result_array();
	}

	public function buscar_termo($campo, $valor)
	{
		$this->db->from('questoes');
		$this->db->where("{$campo} like binary '%{$valor}%'");
		$query = $this->db->get();

		echo $this->db->last_query();

		return $query->result_array();
	}

	public function buscar_termo_opcoes($valor)
	{
		$this->db->from('questoes');
		$this->db->like('qop_texto', $valor);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function listar_questoes_sem_url_reduzida($limit = 10)
	{
		$this->db->where('que_url_reduzida', null);
// 		$this->db->order_by('que_ctrl_url_prioridade', 'desc');
		$this->db->limit($limit);
		$query = $this->db->get('questoes');

		return $query->result_array();
	}

	/**
	 * Recupera uma questão pelo seu id. Pode retornar dados básicos ou completos da questão
	 *
	 * @since K5
	 *
	 * @param int $questao_id Id da questão.
	 * @param bool $extra_info Flag para dados básicos ou completos.
	 *
	 * @return array Array associativo da questão
	 */

	public function get_by_id($questao_id, $extra_info = false)
	{
		$this->db->from('questoes q');
		$this->db->where('q.que_id', $questao_id);

		$query = $this->db->get();

		$questao = $query->row_array();
		$questoes = [$questao];

		if($extra_info) {
			self::adicionar_info_extra($questoes, 0);
			return $questoes[0];
		}

		return $questao;
	}

	/**
	 * Recupera uma questão pelo id do QC
	 *
	 * @since K5
	 *
	 * @param int $qcon_id Id do QC da questão.
	 *
	 * @return array Array associativo da questão
	 */

	public function get_by_qcon_id($qcon_id)
	{
		$query = $this->db->get_where('questoes', array('que_qcon_id' => $qcon_id));
		return $query->row_array();
	}

	public function get($questao, $incluir_novo_registro = false)
	{
		$query = $this->db->get_where('questoes', array('que_qcon_id' => $questao['que_qcon_id']));

		$result = $query->row_array();

		if(!$incluir_novo_registro)
			return $result;

		if(count($result) > 0)
			return $result;

		$questao_id = self::salvar_qconcurso($questao);
		$questao['que_id'] = $questao_id;

		$i = 1;
		foreach ($questao['opcoes'] as $opcao) {
			$data = array(
				'qop_texto' => $opcao,
				'qop_ordem' => $i,
				'que_id' => $questao_id
			);
			$this->db->insert('questoes_opcoes', $data);
			$i++;
		}


		return self::get($questao);
	}

	public function salvar_qconcurso($data)
	{
		$data = array(
				'que_enunciado'  	=> $data['que_enunciado'],
				'que_qcon_id' 	 	=> $data['que_qcon_id'],
				'que_tipo'			=> $data['que_tipo'],
				'dis_id'			=> $data['dis_id'],
				'pro_id'			=> $data['pro_id'],
				'que_data_criacao'	=> get_data_hora_agora(),
				'que_ativo'			=> NAO
		);

		$this->db->insert('questoes', $data);

		return $this->db->insert_id();
	}

	public function listar_por_discipina($disciplina_id) {
		$query = $this->db->get_where('questoes', array('dis_id' => $disciplina_id));
		return $query->result_array();
	}

	public function listar_por_simulado_disciplina($simulado_id, $disciplina_id)
	{
		$this->db->from('questoes q');
		$this->db->join('simulados_questoes sq', 'q.que_id = sq.que_id');
		$this->db->where('sq.sim_id', $simulado_id);
		$this->db->where('q.dis_id', $disciplina_id);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_total_simulado($simulado_id) {
		$this->db->select('q.que_id');
		$this->db->distinct();
		$this->db->from('questoes as q');
		$this->db->join('simulados_questoes sq', 'sq.que_id=q.que_id', 'left');
		$this->db->where('sq.sim_id', $simulado_id);

		return $this->db->count_all_results();
	}

	public function listar_por_simulado_e_disciplina($simulado_id, $disciplina_id)
	{
		$this->db->from('questoes as q');
		$this->db->join('simulados_questoes sq', 'sq.que_id=q.que_id');
		$this->db->where('sq.sim_id', $simulado_id);
		$this->db->where('q.dis_id', $disciplina_id);

		$query = $this->db->get();

		return $query->result_array();
	}


	public function listar_por_simulado($simulado_id, $limit = null, $page = null, $disciplina_id = null, $tipo_resultado = null)
	{
		$this->db->select('q.que_id, q.que_texto_base, q.que_tipo, q.que_enunciado, q.que_qcon_id, q.que_resposta, q.que_url_reduzida, q.que_referencia_id, q.que_status, q.que_ativo, d.dis_id, d.dis_nome, d.dis_ativo, sr.sro_nome');
		$this->db->from('questoes as q');
		$this->db->join('disciplinas as d', 'd.dis_id=q.dis_id', 'left');
		$this->db->join('simulados_questoes sq', 'sq.que_id=q.que_id', 'left');
		$this->db->join('simulados_disciplinas sd', 'sd.dis_id=d.dis_id', 'left');
		$this->db->join('simulados_rotulos sr', 'sq.sro_id = sr.sro_id', 'left');
		$this->db->where('sq.sim_id', $simulado_id);
		$this->db->where('sd.sim_id', $simulado_id);
		$this->db->order_by('sd.sid_ordem','ASC');
		$this->db->order_by('q.que_id', 'DESC');

		if(!is_null($limit))
			$this->db->limit($limit, $page);

		if($disciplina_id) {
			$this->db->where('d.dis_id', $disciplina_id);
		}

		switch ($tipo_resultado) {
		    case SIMULADO_QUESTAO_NAO_RESOLVIDAS: {
		        $this->db->where('not exists(select 1 from questoes_resolvidas qr where qr.que_id = sq.que_id and qr.sim_id = sq.sim_id and qr.qre_selecao is not null and qr.usu_id = '. get_current_user_id().')');
		        break;
		    }

		    case SIMULADO_QUESTAO_RESOLVIDAS: {
		        $this->db->where('exists(select 1 from questoes_resolvidas qr where qr.que_id = sq.que_id and qr.sim_id = sq.sim_id and qr.qre_selecao is not null and qr.usu_id = '. get_current_user_id().')');
		        break;
		    }

		    case SIMULADO_QUESTAO_QUE_ACERTEI: {
		        $this->db->where('exists(select 1 from questoes_resolvidas qr where qr.que_id = sq.que_id and qr.sim_id = sq.sim_id and qr.qre_selecao = q.que_resposta and qr.usu_id = '. get_current_user_id().')');
		        break;
		    }

		    case SIMULADO_QUESTAO_QUE_ERREI: {
		        $this->db->where('exists(select 1 from questoes_resolvidas qr where qr.que_id = sq.que_id and qr.sim_id = sq.sim_id and qr.qre_selecao != q.que_resposta and qr.usu_id = '. get_current_user_id().')');
		        break;
		    }

		}

		$query = $this->db->get();

		$questoes = $query->result_array();
		self::adicionar_info_extra($questoes, $page);

		return $questoes;
	}

	public function listar_por_simulado_rotulo($simulado_id, $limit = null, $page = null, $disciplina_id = null)
	{
		$this->db->select('q.que_id, q.que_texto_base, q.que_tipo, q.que_enunciado, q.que_qcon_id, q.que_resposta, q.que_url_reduzida, q.que_referencia_id, q.que_status, q.que_ativo, d.dis_id, d.dis_nome, d.dis_ativo, ro.sro_nome');
		$this->db->from('questoes as q');
		$this->db->join('questoes_provas as qp', 'qp.que_id = q.que_id', 'left');
		$this->db->join('provas as p', 'qp.pro_id = p.pro_id', 'left');
		$this->db->join('orgaos as o', 'o.org_id=p.org_id', 'left');
		$this->db->join('bancas as b', 'b.ban_id=p.ban_id', 'left');
		$this->db->join('disciplinas as d', 'd.dis_id=q.dis_id', 'left');
		$this->db->join('simulados_questoes sq', 'sq.que_id=q.que_id', 'left');
		$this->db->join('simulados_rotulos ro', 'ro.sro_id=sq.sro_id', 'left');
		$this->db->join('simulados_disciplinas sd', 'sd.dis_id=d.dis_id', 'left');
		$this->db->where('sq.sim_id', $simulado_id);
		$this->db->where('sd.sim_id', $simulado_id);
		$this->db->order_by('ro.sro_ordem','ASC');
		$this->db->order_by('q.que_id', 'DESC');

		if(!is_null($limit))
			$this->db->limit($limit, $page);

		if($disciplina_id) {
			$this->db->where('d.dis_id', $disciplina_id);
		}

		$query = $this->db->get();

		$questoes = $query->result_array();
		self::adicionar_info_extra($questoes, $page);

		return $questoes;
	}

	private function adicionar_filtros($filtro, $usuario_id = null)
	{
	    KLoader::helper("StringHelper");

		$join_questoes_resolvidas = false;

		if(isset($filtro['ignoradas'])) {
			$this->db->where_not_in('q.que_id', $filtro['ignoradas']);
		}

		if (isset($filtro['busca'])) {

		    // declaração de caracteres especiais
		    $kespaco = "[KESPACO]";
		    $kpipe = "[KPIPE]";
		    $kand = "[KAND]";

			$busca = trim(stripslashes($filtro['busca']));

			// buscas os termos exatos
			$exatas = StringHelper::get_substrings($busca, '"', '"');

			// substitui caracteres especiais nos termos exatos
			foreach($exatas as $item) {
			    $item_modificado = str_replace(" ", $kespaco, $item);
			    $item_modificado = str_replace("|", $kpipe, $item_modificado);
			    $busca = str_replace($item, $item_modificado, $busca);
			}

			$or_a_match = [];
			$or_a_like = [];
			$busca_or = explode("|", $busca);

			// define o OR com no máximo 3 elementos
            $busca_or = array_slice($busca_or, 0, 3);

			foreach ($busca_or as $item_or) {

			    $and_a_match = [];
			    $and_a_like = [];

			    $busca_and = explode(" ", $item_or);

                // define o AND com no máximo 5 elementos
                $busca_and = array_slice($busca_and, 0, 5);

			    foreach ($busca_and as $item_and) {
			        $item_and = trim($item_and);

			        if($item_and) {
			            $and_a_match[] = "$item_and";
			            $and_a_like[] = "q.que_busca LIKE '%{$item_and}%'";
			        }
			    }

			    $or_a_match[] = implode($kand, $and_a_match);
			    $or_a_like[] = implode($kand, $and_a_like);
			}

			// se não houver OR adiciona um + na frente do primeiro termo
			$busca_match = count($or_a_match) == 1 ? "+" . $or_a_match[0] : implode(" ", $or_a_match);
			$busca_like = implode(" OR ", $or_a_like);

			// retorna com os caracteres especiais
			$busca_match = str_replace($kespaco, " ", $busca_match);
			$busca_match = str_replace($kpipe, "|", $busca_match);
			$busca_match = str_replace($kand, " +", $busca_match);

			$busca_like = str_replace('"', "", $busca_like);

			$busca_like = str_replace($kespaco, " ", $busca_like);
			$busca_like = str_replace($kpipe, "|", $busca_like);
			$busca_like = str_replace($kand, " AND ", $busca_like);

			$this->db->where(" MATCH(`q`.`que_busca`) AGAINST ('{$busca_match}')", null, false);

			// Removida a busca like por conta de performance
//			$this->db->where($busca_like, null, false);
		}

		if(isset($filtro['que_qcon_id'])) {
			$this->db->where('q.que_qcon_id', trim($filtro['que_qcon_id']));
		}

		if(isset($filtro['professores_ids'])) {
			$prof_where = "";
			for($i = 0; $i < count($filtro['professores_ids']); $i++) {
				$prof_where .= " user_id = " . $filtro['professores_ids'][$i];

				if($i != count($filtro['professores_ids']) - 1) {
					$prof_where .= " OR ";
				}
			}

			$this->db->where("q.que_id IN (SELECT DISTINCT que_id FROM comentarios WHERE $prof_where AND com_destaque = 1)", null, false);
		}

		if(isset($filtro['status_ids'])) {
			$status = implode(",",$filtro['status_ids']);
			$this->db->where("q.que_status IN ({$status})", null, false);

		}

		if($filtro['favoritas']){
			if(is_null($usuario_id)) {
				$usuario_id = get_current_user_id();
			}

            // somente para usuário logado
            if($usuario_id) {
                $this->db->join('questoes_favoritas as qf2', 'qf2.que_id=q.que_id');
                $this->db->where('qf2.usu_id', $usuario_id);
                $this->db->where('qf2.qfa_tipo', QUESTAO_FAVORITA);
            }
		}

		if(isset($filtro['filtro_incluir']) && $filtro['filtro_incluir'] != TODAS) {
			if(is_null($usuario_id)) {
				$usuario_id = get_current_user_id();
			}

			switch ($filtro['filtro_incluir']) {

				case NAO_RESOLVIDAS:
                    // somente para usuário logado
                    if($usuario_id) {
                        $this->db->where("q.que_id NOT IN (SELECT que_id FROM questoes_resolvidas WHERE usu_id = {$usuario_id})", null, false);
                    }
					break;

				case RESOLVIDAS: {
                    // somente para usuário logado
                    if($usuario_id) {
                        $join_questoes_resolvidas = true;

                        $this->db->join('questoes_resolvidas as qr', 'qr.que_id=q.que_id');
                        $this->db->where('qr.usu_id', $usuario_id);
                    }
					break;
				}

				case QUE_ACERTEI: {
                    // somente para usuário logado
                    if($usuario_id) {
                        $join_questoes_resolvidas = true;

                        $this->db->join('questoes_resolvidas as qr', 'qr.que_id=q.que_id');
                        $this->db->where('qr.usu_id', $usuario_id);
                        $this->db->where('qr.qre_selecao = q.que_resposta');
                    }
					break;
				}

				case QUE_ERREI: {
                    // somente para usuário logado
                    if($usuario_id) {
                        $join_questoes_resolvidas = true;

                        $this->db->join('questoes_resolvidas as qr', 'qr.que_id=q.que_id');
                        $this->db->where('qr.usu_id', $usuario_id);
                        $this->db->where('(qr.qre_selecao != q.que_resposta or qr.qre_selecao is null)');
                    }
                    break;
				}

				case NOTIFICACAO_ERRO: {
					$this->db->where('q.que_status', DESATUALIZADA);
					break;
				}

				case FAVORITAS: {

                    // somente para usuário logado
				    if($usuario_id) {
                        $this->db->join('questoes_favoritas as qf', 'qf.que_id=q.que_id');
                        $this->db->where('qf.usu_id', $usuario_id);
                        $this->db->where('qf.qfa_tipo', QUESTAO_FAVORITA);
                    }
				    break;
				}
			}
		}

		if(isset($filtro['filtro_excluir'])) {
			if(is_null($usuario_id)) {
				$usuario_id = get_current_user_id();
			}
			foreach($filtro['filtro_excluir'] as $excluir){
				switch($excluir){
					case ANULADA:{
						$this->db->where('q.que_status !=', ANULADA);
						break;
					}
					case DESATUALIZADA:{
						$this->db->where('q.que_status !=', DESATUALIZADA);
						break;
					}
					case DOS_MEUS_CADERNOS:{
					    // somente para usuário logado
					    if($usuario_id) {
                            $this->db->where("q.que_id NOT IN (SELECT que_id FROM cadernos_questoes cq JOIN cadernos c ON c.cad_id = cq.cad_id where c.usu_id = {$usuario_id})");
                        }
					    break;
					}
					case REPETIDAS:{
						$this->db->where('not exists(select 1 from exponenc_corp.questoes_repetidas qur where qur.que_id_repetida = q.que_id and qur.qur_status = '.STATUS_COMPARACAO_RESOLVIDA.')');
					    break;
					}
				}
			}
		}

		$join_comentarios = false;

		if(isset($filtro['filtro_comentarios'])) {
			switch($filtro['filtro_comentarios']) {
				case COM_COMENTARIOS: {
					$this->db->where("(q.que_id IN (SELECT DISTINCT que_id FROM comentarios) OR EXISTS(SELECT 1 FROM comentarios_videos cv WHERE cv.que_id = q.que_id))", NULL, FALSE);
					break;
				}
				case COM_COMENTARIOS_DE_PROFESSORES: {
					//VER TAMBÉM O FILTRO 'sim_excluir_questoes'
					//$this->db->join("comentarios c", "c.que_id = q.que_id");
					//$this->db->where('(c.com_destaque = 1  OR EXISTS(SELECT 1 FROM comentarios_videos cv WHERE cv.que_id = q.que_id))', NULL, FALSE);
					//$join_comentarios = true;
					$this->db->where('q.que_tem_destaque', TRUE);
					break;
				}
				case COM_COMENTARIOS_DE_ALUNOS: {
					$this->db->join("comentarios c", "c.que_id = q.que_id");
					$this->db->where('c.com_destaque', 0);
					$join_comentarios = true;
					break;
				}
				case NENHUM_COMENTARIO: {
					$this->db->where("q.que_id NOT IN (SELECT DISTINCT que_id FROM comentarios)", NULL, FALSE);
					$this->db->where("NOT EXISTS(SELECT 1 FROM comentarios_videos cv WHERE cv.que_id = q.que_id)", NULL, FALSE);
					break;
				}
				case MEUS_COMENTARIOS: {
					if($usuario_id = get_current_user_id()) {
						$this->db->where("( q.que_id IN (SELECT DISTINCT que_id FROM comentarios WHERE user_id = {$usuario_id})  OR EXISTS(SELECT 1 FROM comentarios_videos cv WHERE cv.que_id = q.que_id AND cv.usu_id = {$usuario_id}) )", null, false);
					}
					break;
				}
				case COM_COMENTARIOS_EM_VIDEO: {
					$this->db->where("EXISTS(SELECT 1 FROM comentarios_videos cv WHERE cv.que_id = q.que_id)", NULL, FALSE);
					break;
				}
			}
		}

		if(isset($filtro['filtro_resposta']))
		{
			switch($filtro['filtro_resposta']) {
				case COM_RESPOSTA: {
					$this->db->where('q.que_resposta !=', 0);
					break;
				}
				case SEM_RESPOSTA: {
					$this->db->where('q.que_resposta', 0);
					break;
				}
			}
		}

		if(isset($filtro['filtro_repetidas']))
		{
			switch($filtro['filtro_repetidas']){
				case REPETIDAS:{
					$this->db->where('exists(select 1 from exponenc_corp.questoes_repetidas qur where qur.que_id_repetida = q.que_id and qur.qur_status = '.STATUS_COMPARACAO_RESOLVIDA.')');
					break;
				}
				case NAO_REPETIDAS:{
					$this->db->where('not exists(select 1 from exponenc_corp.questoes_repetidas qur where qur.que_id_repetida = q.que_id and qur.qur_status = '.STATUS_COMPARACAO_RESOLVIDA.')');
					break;
				}
			}
		}

		if(isset($filtro['filtro_ativo'])) 
		{
			switch($filtro['filtro_ativo']) {
				case ATIVO: {
					$this->db->where('q.que_ativo', ATIVO);
					break;
				}
				case INATIVO: {
					$this->db->where('q.que_ativo', INATIVO);
					break;
				}
			}
		}


		if($questoes_ids = $filtro['filtro_questoes']) {
			foreach ($questoes_ids as $id) {
				$this->db->or_where('q.que_id', $id);
			}
		}

		if($filtro['que_id']) {

			$filtro['que_id'] = str_replace(" ", "", $filtro['que_id']);
			$filtro['que_id'] = str_replace("e", "", $filtro['que_id']);

			$que_ids_a = $filtro['que_id'];
			$this->db->where("q.que_id IN ({$que_ids_a})");
		}

		if ($filtro['data_inicio']) {
			$data_inicio = converter_para_yyyymmdd($filtro['data_inicio']) . ' 00:00:00';
			$this->db->where("q.que_data_criacao >= '{$data_inicio}'");
		}

		if ($filtro['data_fim']) {
			$data_fim = converter_para_yyyymmdd($filtro['data_fim']) . ' 23:59:59';
			$this->db->where("q.que_data_criacao <= '{$data_fim}'");
		}

		if($cad_id = $filtro['cad_id']) {

			/**
			 * [JOULE][J1] BUG - Filtro de questões erradas equivocado
			 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2014
			 *
			 * Busca alterada para considerar apenas as questões resolvidas gerais (fora de cadernos e simulados)
			 *
			 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1898
			 *
			 * Necessário um LEFT JOIN para considerar questões não respondidas
			 */
			if(!$join_questoes_resolvidas) {
				$join_questoes_resolvidas = true;
				$this->db->join('questoes_resolvidas as qr', 'qr.que_id=q.que_id', 'left');
			}

			$this->db->join("cadernos_questoes cq", "cq.que_id = q.que_id");

			$this->db->where("qr.sim_id IS NULL AND qr.cad_id IS NULL AND cq.cad_id = {$cad_id}", null, false);

			$this->db->order_by('caq_ordem ASC, q.que_id DESC');
		}

		if($filtro['neg_cad_ids']) {

			$where = "";
			$ids_a = array();

			foreach ($filtro['neg_cad_ids'] as $id) {
				if($id == TODOS_CADERNOS) {
					$ids_a = null;
					break;
				}
				else {
					array_push($ids_a, "cad_id = {$id}");
				}
			}

			if ($ids_a) {
				$neg_cad_ids = implode(" OR ", $ids_a);
			    $where = "WHERE {$neg_cad_ids}";
            }

			$this->db->where("q.que_id NOT IN (SELECT que_id FROM cadernos_questoes {$where})", null, false);
		}

		if($filtro['sim_ids']) {
			$where = "";
			$ids_a = array();

			foreach ($filtro['sim_ids'] as $id) {
				if($id == TODOS_SIMULADOS) {
					$ids_a = null;
					break;
				}elseif($id == TODOS_SIMULADOS_ATIVOS){
					$ids_a = null;
					$where = " INNER JOIN simulados s ON s.sim_id = sq.sim_id WHERE s.sim_status = " . ATIVO;
					break;
				}elseif($id == TODOS_SIMULADOS_MENOS_INATIVOS){
					$ids_a = null;
					$where = " INNER JOIN simulados s ON s.sim_id = sq.sim_id WHERE s.sim_status != " . INATIVO;
					break;
				}else {
					array_push($ids_a, "sq.sim_id = {$id}");
				}
			}

			if ($ids_a) {
				$neg_sim_ids = implode(" OR ", $ids_a);
			    $where = "WHERE {$neg_sim_ids}";
			}

			$this->db->where("q.que_id NOT IN (SELECT sq.que_id FROM simulados_questoes sq {$where})", null, false);
		}

		$posts = array('ban_ids' => 'p.ban_id', 'org_ids' => 'p.org_id', 'car_ids' => 'pc.car_id', 'pro_anos' => 'q.que_ano',
				'esc_ids' => 'p.esc_id','arf_ids' => 'pf.arf_id', 'dis_ids' => 'q.dis_id', 'ass_ids' => 'qa.ass_id', 'ara_ids' => 'pa.ara_id',
				'que_tipos' => 'q.que_tipo', 'que_ids' => 'q.que_id', 'que_dificuldades' => 'q.que_dificuldade', 'que_statuses' => 'q.que_status', 'pro_tipo_ids' => 'p.pro_tipo', 'pro_ids' => 'p.pro_id'
		);

		$join_provas = /*($filtro['order_num_comentarios'] == TRUE);//*/false;
		$join_disciplinas = FALSE;

		foreach($posts as $post => $campo) {


			if(isset($filtro[$post])) {

				if(in_array($post, array('ban_ids', 'org_ids'/*, 'pro_anos'*/, 'esc_ids', 'pro_tipo_ids', 'pro_ids'))) {
					if(!$join_provas) {
						$this->db->join('questoes_provas as qp', 'q.que_id = qp.que_id');
						$this->db->join('provas as p', 'qp.pro_id = p.pro_id');
						$join_provas = true;
					}
				}

				elseif($post == 'ass_ids') {
					$this->db->join('questoes_assuntos as qa', 'qa.que_id=q.que_id');
				}

				elseif($post == 'car_ids') {

					if(!$join_provas) {
						$this->db->join('questoes_provas as qp', 'q.que_id = qp.que_id');
						$this->db->join('provas as p', 'qp.pro_id = p.pro_id');
						$join_provas = true;
					}

					$this->db->join('provas_cargos as pc', 'p.pro_id=pc.pro_id');
				}

				elseif($post == 'arf_ids') {
					if(!$join_provas) {
						$this->db->join('questoes_provas as qp', 'q.que_id = qp.que_id');
						$this->db->join('provas as p', 'qp.pro_id = p.pro_id');
						$join_provas = true;
					}

					$this->db->join('provas_areas_formacao as pf', 'p.pro_id=pf.pro_id', 'left');
				}

				elseif($post == 'ara_ids') {
					if(!$join_provas) {
						$this->db->join('questoes_provas as qp', 'q.que_id = qp.que_id');
						$this->db->join('provas as p', 'qp.pro_id = p.pro_id');
						$join_provas = true;
					}

					$this->db->join('provas_areas_atuacao as pa', 'p.pro_id=pa.pro_id', 'left');
				}

				elseif($post == 'sim_ids') {
					$this->db->join('simulados_questoes as sq', 'sq.que_id=q.que_id');
				}

				$count= 1;
				$item_ids = $filtro[$post];
				$where = "(";

				foreach($item_ids as $item_id) {

					if(($post == 'que_dificuldades' || $post == 'arf_ids' || $post == 'ara_ids' || $post == 'car_ids' || $post == 'dis_ids') && $item_id == 0) {
						$item_id = null;
					}

					if($post == 'dis_ids' && $item_id == -1)  {

						if(!$join_disciplinas) {
							$this->db->join('disciplinas d', 'q.dis_id = d.dis_id', 'left');
							$join_disciplinas = TRUE;
						}

						if($count == 1) {

							$where .= 'd.dis_ativo = 2 OR q.dis_id = -1';
						}
						else {
							$where .= ' OR d.dis_ativo = 2';
						}
					}
					else {
						if(is_null($item_id)) {
							if($count == 1) {
								$where .= $campo . " IS NULL";
							}
							else {
								$where .= " OR " . $campo . " IS NULL";
							}
						}
						else {
							if($count == 1) {
								$where .= $campo . "=" . $item_id;
							}
							else {
								$where .= " OR " . $campo . "=" . $item_id;
							}
						}
					}

					$count++;
				}
				$where .= ")";
				//echo "<br/>" . $where;
				$this->db->where($where, null, false);
			}
		}

		//echo 'if->sim_excluir_questoes';
		if(isset($filtro['sim_excluir_questoes'])){
			//echo 'sim_excluir_questoes';
			foreach($filtro['sim_excluir_questoes'] as $item){

				switch ($item) {

					case EXCLUIR_QUESTOES_INATIVAS: {
						//echo 'excluir_inativas';
						$this->db->where('q.que_ativo', SIM);
						break;
					}
					case EXCLUIR_QUESTOES_SEM_COMENTARIO_PROFESSOR: {
						//VER TAMBÉM O FILTRO 'filtro_comentarios'
						//if (!$join_comentarios) {
						//	$this->db->join("comentarios c", "c.que_id = q.que_id");
						//}
						//$this->db->where('(c.com_destaque = 1  OR EXISTS(SELECT 1 FROM comentarios_videos cv WHERE cv.que_id = q.que_id))', NULL, FALSE);
						$this->db->where('q.que_tem_destaque', TRUE);
						break;
					}
					case EXCLUIR_QUESTOES_DE_OUTROS_SIMULADOS:{
						$outros_sql = "not exists(select 1 from simulados_questoes sim_quest where sim_quest.que_id = q.que_id";
						if(isset($filtro['excluir_sim_id'])){
							$outros_sql .= " and sim_quest.sim_id != ".$filtro['excluir_sim_id'];
						}
						$outros_sql .= ")";
						$this->db->where($outros_sql, null, false);
						break;
					}

				}

			}

		}

		if(isset($filtro['ass_ids_excluir']) && !empty($filtro['ass_ids_excluir']))
		{
			/*if(!$join_assuntos)
			{
				$this->db->join('questoes_assuntos as qa', 'qa.que_id=q.que_id');
			}
			$this->db->where_not_in('qa.ass_id', $filtro['ass_ids_excluir']);*/

			$ass_ids_excluir = implode(",", $filtro['ass_ids_excluir']);

			$this->db->where("not exists(select 1 from questoes_assuntos qa_ex where qa_ex.que_id = q.que_id and qa_ex.ass_id in ({$ass_ids_excluir}))", NULL, FALSE);
		}

	}

	public function listar_com_opcoes_e_assuntos($filtro, $limit = LIMIT_LISTAR_QUESTOES, $page = null, $incluir_inativas = TRUE)
	{
		$questoes = self::listar_por_filtros($filtro, $limit, $page, FALSE, $incluir_inativas);

		//Essa chamada abaixo (self::adicionar_info_extra) já é realizada no método acima (self::listar_por_filtros). @jpaulofms
		//self::adicionar_info_extra($questoes, $page);

		// foreach ($questoes as &$questao) {
		// 	$questao['numero'] = ++$page;
		// 	$questao['assuntos'] = self::get_assuntos_questao($questao['que_id']);
		// 	if($questao['que_tipo'] == MULTIPLA_ESCOLHA)
		// 		$questao['opcoes'] = self::listar_opcoes($questao['que_id']);
		// }

		return $questoes;
	}

	public function listar_por_caderno($caderno_id, $limit = null, $page = null)
	{
		$this->db->select('q.que_id, q.que_texto_base, q.que_tipo, q.que_enunciado, q.que_qcon_id, q.que_status, q.que_resposta, p.pro_nome, p.pro_ano, o.org_nome, b.ban_nome, d.dis_id, d.dis_nome, d.dis_ativo');
		$this->db->from('questoes as q');
		$this->db->join('questoes_provas as qp', 'q.que_id = qp.pro_id', 'left');
		$this->db->join('provas as p', 'qp.pro_id = p.pro_id', 'left');
		$this->db->join('orgaos as o', 'o.org_id=p.org_id', 'left');
		$this->db->join('bancas as b', 'b.ban_id=p.ban_id', 'left');
		$this->db->join('disciplinas as d', 'd.dis_id=q.dis_id', 'left');
		$this->db->join('cadernos_questoes as cq', 'cq.que_id=q.que_id', 'left');
		$this->db->where('cq.cad_id', $caderno_id);
		$this->db->order_by('cq.caq_ordem ASC, q.que_id DESC');

		if(!is_null($limit)) {
			if(!is_null($page)) {
				$this->db->limit($limit, $page);
			}
			else {
				$this->db->limit($limit);
			}
		}
		$this->db->group_by('q.que_id');

		$query = $this->db->get();

		$questoes = $query->result_array();

		self::adicionar_info_extra($questoes, $page);

		return $questoes;
	}

	public function adicionar_info_extra(&$questoes, $page = 0)
	{
		$this->load->model('banca_model');
		$this->load->model('disciplina_model');
		$this->load->model('orgao_model');
		$this->load->model('prova_model');
		$this->load->model('comentario_model');

		$index = 0;
		foreach ($questoes as &$questao) {

			$questao['numero'] = ++$page;
			$questao['assuntos'] = self::get_assuntos_questao($questao['que_id']);

			if($questao['que_tipo'] == MULTIPLA_ESCOLHA || $questao['que_tipo'] == MULTIPLA_ESCOLHA_5) {
				$questao['opcoes'] = self::listar_opcoes($questao['que_id']);
			}

			$questao['provas'] = [];
			$questao['anos'] = [];
			$questao['bancas'] = [];
			$questao['orgaos'] = [];

			if($questao_provas = self::listar_provas($questao['que_id'])) {

				foreach ($questao_provas as $questao_prova) {

					$prova = $this->prova_model->get_by_id($questao_prova['pro_id']);
					$banca = $this->banca_model->get_by_id($prova['ban_id']);
					$orgao = $this->orgao_model->get_by_id($prova['org_id']);

					array_push($questao['provas'], $prova['pro_nome']);
					array_push($questao['anos'], $prova['pro_ano']);
					array_push($questao['bancas'], $banca['ban_nome']);
					array_push($questao['orgaos'], $orgao['org_nome']);

				}

				$questao['provas'] = array_unique($questao['provas']);
				$questao['anos'] = array_unique($questao['anos']);
				$questao['bancas'] = array_unique($questao['bancas']);
				$questao['orgaos'] = array_unique($questao['orgaos']);
			}

			$disciplina = $this->disciplina_model->get_by_id($questao['dis_id']);
			$questao['dis_nome'] = $disciplina['dis_nome'];
			$questao['dis_ativo'] = $disciplina['dis_ativo'];
			$questao['total_comentarios'] = $questao['que_num_comentarios'];

			$questao['comentario_video'] = $this->comentario_model->get_comentario_video_por_questao($questao['que_id']);

			$index++;
		}
	}

	/**
	 * Busca os IDs das questões encontradas com base no filtro
	 *
	 * @since L1
	 *
	 * @param array $filtro filtro das questões
	 * @param int $limit máximo de questões a serem retornadas
	 * @param int $page página, para caso seja usado em uma listagem
	 * @param bool $imprimir para o caso em que as questões serão impressas
	 * @param bool $has_filtro se a busca contém algum filtro inserido pelo usuári
	 * 
	 * @return array Array contendo os IDs de todas as questões encontradas
	 */
//	public function get_questoes_ids_por_filtro( $filtro, $limit= LIMIT_LISTAR_QUESTOES, $page= null, $imprimir= false, $has_filtro = true, $force_admin = FALSE){
    public function get_questoes_ids_por_filtro( $filtro, $limit= LIMIT_LISTAR_QUESTOES, $page= null, $imprimir= false, $incluir_inativas = false){

		// $this->db->select(' q.que_id, q.que_resposta, q.que_texto_base, q.que_tipo, q.que_enunciado, q.que_qcon_id, q.que_status, q.que_referencia_id, p.pro_nome, p.pro_ano, o.org_nome, b.ban_nome, d.dis_nome');

		/**********************************************************************************************************
		 * O DB perde muita performance nas consultas com limit alto (ex: 200.000). Para reduzir isso uma técnica
		 * é fazer a busca trazendo apenas o id.
		 **********************************************************************************************************/
		$this->db->select('q.que_id');
		$this->db->from('questoes as q');

		//Somente questões "aprovadas" são exibidas
		// $this->db->where('q.que_aprovacao', QUESTAO_APROVADA);

		//Usar somente no controlador para ordenar o resultado
		if($filtro['order_num_comentarios'] == TRUE){

			//$this->db->join('questoes_provas as qp', 'q.que_id = qp.que_id', 'left');
			//$this->db->join('provas as p', 'qp.pro_id = p.pro_id', 'left');

			//$this->db->join('comentarios as com_prof', '(com_prof.que_id = q.que_id and com_prof.com_destaque = 1)', 'left');
			
			$this->db->order_by('q.que_tem_destaque', 'DESC');
			$this->db->order_by('q.que_ano', 'DESC');
			//$this->db->order_by('q.que_num_comentarios', 'DESC');
			$this->db->order_by('q.que_id', 'DESC');
		}

		if(!empty($filtro)) {
			self::adicionar_filtros($filtro);
		}

		if(!$incluir_inativas) {
			$this->db->where('q.que_ativo', SIM);
			$this->db->where('q.que_resposta IS NOT NULL', null, false);
			$this->db->where('(q.que_resposta != 0 OR q.que_status = 2)', null, false);

			//B2761 - Exercícios de Fixação: 2 - ambas as categorias (inéditas e fixação) não devem ser buscáveis para assinantes do SQ. 
			//Ou seja, estas questões ficarão escondidas no SQ, sendo acessíveis hoje apenas por meio de cadernos que faremos pra vendas
			//Incluído posteriormente a questão adaptada
			if(!$filtro['cad_id'])
			{
				//Nega o operador igual null-safe, pq o ' != ' ignora os null
				$this->db->where('NOT(q.que_procedencia <=> ' . QUESTAO_INEDITA . ')', null, false);
				$this->db->where('NOT(q.que_procedencia <=> ' . QUESTAO_FIXACAO . ')', null, false);
				$this->db->where('NOT(q.que_procedencia <=> ' . QUESTAO_ADAPTADA . ')', null, false);
			}
		}

		//remove o $filtro para não influenciar na verificação do filtro
		unset($filtro['order_num_comentarios']);

		if(nenhum_filtro_aplicado($filtro)) {

			if($page > 1000) {
				$this->db->where('q.que_id >=', $page);
				$this->db->limit($limit);
				$limit = null; // previne o limit de ser reescrito
			}
			$this->db->order_by('q.que_id', 'ASC');
		}
		else {
			$this->db->order_by('q.dis_id', 'ASC');
		}

		if(!is_null($limit)) {
			$this->db->limit($limit, $page);
		}
		
		$this->db->group_by(['q.que_id', 'q.que_ano']);

		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result_array();
	}

	public function listar_por_filtros( $filtro, $limit= LIMIT_LISTAR_QUESTOES, $page= null, $imprimir= false, $incluir_inativas = false)
	{

		$resultado = self::get_questoes_ids_por_filtro($filtro, $limit, $page, $imprimir, $incluir_inativas);

		$questoes = self::get_questoes_completas_para_listagem($resultado, $page);

		return $questoes;
	}

	/**
	 * Retorna um array contento as questões completas que possuem os IDs informados por parâmetro
	 *
	 * @since L1
	 *
	 * @param array IDs das questões
	 *
	 * @return array Questões completas
	 */
	public function get_questoes_completas_para_listagem($questoes_ids, $page = 0){
		
		$questoes = array();

		if( empty($questoes) )
		{
			foreach ($questoes_ids as $linha) {
				if(is_array($linha)){
					$questao = self::get_by_id($linha['que_id']);
				}else{
					$questao = self::get_by_id($linha);
				}
				array_push($questoes, $questao);
			}
		}

		self::adicionar_info_extra($questoes, $page);

		return $questoes;
	}

	public function listar_desempenho_disciplina($usuario_id, $inicio, $fim, $bancas, $disciplinas, $minimo = -1, $ordenar_desempenho = false, $desc = false, $where = false) {
		return self::listar_desempenho($usuario_id, "disciplinas", $inicio, $fim, $bancas, $disciplinas, null, $minimo, $ordenar_desempenho, $desc, $where);
	}

	public function listar_desempenho_assunto($usuario_id, $inicio, $fim, $disciplinas, $assuntos, $minimo = -1, $ordenar_desempenho = false, $desc = false, $where = false) {
		return self::listar_desempenho($usuario_id, "assuntos", $inicio, $fim, null, $disciplinas, $assuntos, $minimo, $ordenar_desempenho, $desc, $where);
	}

	public function listar_desempenho_banca($usuario_id, $inicio, $fim, $bancas, $disciplinas, $minimo = -1, $ordenar_desempenho = false, $desc = false, $where = false) {
		return self::listar_desempenho($usuario_id, "bancas", $inicio, $fim, $bancas, $disciplinas, null, $minimo, $ordenar_desempenho, $desc, $where);
	}

	// private function listar_desempenho($usuario_id, $tabela, $meses, $minimo, $ordenar_desempenho , $desc) {
	private function listar_desempenho($usuario_id, $tabela, $inicio, $fim, $bancas, $disciplinas, $assuntos, $minimo, $ordenar_desempenho , $desc, $where = false)
    {
		if($tabela == "assuntos") {
			$ordem = "ass_nome";
			$select = "select ass_id, ass_nome, dis_id, dis_nome, ";
			$sub_select = "from (select distinct a.*, d.dis_nome, ";
			$from = "from assuntos as a ";
			$join = "join disciplinas as d on d.dis_id=a.dis_id ";
			$join .= "join questoes_assuntos as qa on qa.ass_id=a.ass_id ";
			$join .= "join questoes as q on q.que_id=qa.que_id ";
			$sub_group = "GROUP BY a.ass_id ";
			$group = " group by ass_id ";
		} elseif ($tabela == "disciplinas") {
			$ordem = "dis_nome";
			$select = "select dis_id, dis_nome, ";
			$sub_select = "from (select distinct d.*, ";
			$from = "from disciplinas as d ";
			$join = "join questoes as q on q.dis_id=d.dis_id ";
			$join .= "join questoes_provas as qp on q.que_id = qp.que_id ";
			$join .= "join provas as p on qp.pro_id = p.pro_id ";
			$sub_group = "GROUP BY d.dis_id ";
			$group = " group by dis_id ";
		} elseif ($tabela == "bancas") {
			$ordem = "ban_nome";
			$select = "select ban_id, ban_nome, dis_id, dis_nome, ";
			$sub_select = "from (select distinct b.*, d.*, ";
			$from = "from bancas as b ";
			$join = "join provas as p on b.ban_id=p.ban_id ";
			$join .= "join questoes_provas as qp on qp.pro_id = p.pro_id ";
			$join .= "join questoes as q on q.que_id = qp.que_id ";
			$join .= "join disciplinas as d on q.dis_id=d.dis_id ";
			$sub_group = "GROUP BY b.ban_id, d.dis_id ";
			$group = " group by ban_id, dis_id ";
		}


		if($ordenar_desempenho)
			$ordem = "performance";

		$asc_desc = "asc";
		if($desc)
			$asc_desc = "desc";

		$sql = $select;
		$sql .= "count(case when qre_selecao = que_resposta then 1 end) as acertos, ";
		$sql .= "count(case when qre_selecao != que_resposta or qre_selecao is null then 1 end) as erros, ";
		$sql .= "count(que_id) as total, ";
		$sql .= "round(100 * count(case when qre_selecao = que_resposta then 1 end) / count(que_id),1) as performance ";

		$sql .= $sub_select;
		$sql .= "qr.qre_selecao, q.que_resposta, q.que_id ";
		$sql .= $from;
		$sql .= $join;
		$sql .= "join questoes_resolvidas as qr on qr.que_id=q.que_id ";
		$sql .= "where qr.cad_id IS NULL and qr.sim_id IS NULL ";
		$sql .= "and qr.usu_id={$usuario_id} ";
		$sql .= "and q.que_ativo = 1 ";

		if($bancas){
			$bancas_ids = implode(',', $bancas);
			$sql .= "and p.ban_id in ({$bancas_ids}) ";
		}

		if($disciplinas){
			$disciplinas_ids = implode(',', $disciplinas);
			$sql .= "and d.dis_id in ({$disciplinas_ids}) ";
		}

		if($assuntos){
			$assuntos_ids = implode(',', $assuntos);
			$sql .= "and a.ass_id in ({$assuntos_ids}) ";
		}

		if($where) {
			$sql .= "and {$where} ";
		}

		if($inicio && $fim) {
			$sql .= " AND qr.qre_data >= '{$inicio} 00:00:00' AND qr.qre_data <= '{$fim} 23:59:59' ";
		}

		$sql .= $sub_group;
		$sql .= ", q.que_id ";
		$sql .= ") a ";
		$sql .= $group;
		if($minimo > 0)
			$sql .= "having total >= {$minimo} ";
		$sql .= "order by {$ordem} {$asc_desc}";
		$query = $this->db->query($sql);
//		echo "<br><br>SQL: {$sql}<br><br>";
		return $query->result_array();
//        return [];
	}
	public function get_total_resolvidas($usuario_id = null, $meses = null) {
		$this->db->select('d.*, ');
		$this->db->from('disciplinas as d');
		$this->db->join('questoes as q', 'q.dis_id=d.dis_id', 'left');
		self::adicionar_filtros(array('filtro_incluir' => RESOLVIDAS), $usuario_id);
		$this->db->group_by('q.que_id');
		return $this->db->count_all_results();
	}

//	public function get_total_resultado( $filtro, $has_filtro = TRUE, $force_admin = FALSE ) {
    public function get_total_resultado( $filtro, $incluir_inativas = false ) {

		//$this->output->enable_profiler(TRUE);

		$this->db->select('q.que_id');
		$this->db->distinct();
		$this->db->from('questoes as q');

		//Somente questões "aprovadas" são exibidas
		// $this->db->where('q.que_aprovacao', QUESTAO_APROVADA);

		if(!empty($filtro)) {
			self::adicionar_filtros($filtro);
		}

		if(!$incluir_inativas) {
			$this->db->where('q.que_ativo', SIM);
			$this->db->where('q.que_resposta IS NOT NULL', null, false);
			$this->db->where('(q.que_resposta != 0 OR q.que_status = 2)', null, false);

			//B2761 - Exercícios de Fixação: 2 - ambas as categorias (inéditas e fixação) não devem ser buscáveis para assinantes do SQ. 
			//Ou seja, estas questões ficarão escondidas no SQ, sendo acessíveis hoje apenas por meio de cadernos que faremos pra vendas
			//Inserido posteriormente questão adaptada
			if(!$filtro['cad_id'])
			{
				//Nega o operador igual null-safe, pq o ' != ' ignora os null
				$this->db->where('NOT(q.que_procedencia <=> ' . QUESTAO_INEDITA . ')', null, false);
				$this->db->where('NOT(q.que_procedencia <=> ' . QUESTAO_FIXACAO . ')', null, false);
				$this->db->where('NOT(q.que_procedencia <=> ' . QUESTAO_ADAPTADA . ')', null, false);
			}
		}
		//echo "Query a ser executada: " . $this->db->get_compiled_select();
		$total = $this->db->count_all_results();
		//echo "Última query ({$total}): ".$this->db->last_query();
		return $total;
	}

	public function get_ids_questoes( $filtro ) {
		//$this->output->enable_profiler(TRUE);

		$this->db->select('q.que_id');
		$this->db->distinct();
		$this->db->from('questoes as q');

		//Somente questões "aprovadas" são exibidas
		// $this->db->where('q.que_aprovacao', QUESTAO_APROVADA);

		if(!empty($filtro)) {
			self::adicionar_filtros($filtro);
		}

		if(!tem_acesso(array(ADMINISTRADOR, PROFESSOR, REVISOR))) {
			$this->db->where('q.que_ativo', SIM);
			$this->db->where('q.que_resposta IS NOT NULL', null, false);
			$this->db->where('(q.que_resposta != 0 OR q.que_status = 2)', null, false);
		}
		//echo "Última query: ".$this->db->last_query();
		return $this->db->get()->result_array();
	}

	public function remove_questoes_caderno($caderno_id, $arr_questao_id) {
		$this->db->where('cad_id', $caderno_id);
		$this->db->where_in('que_id', $arr_questao_id);
		$this->db->delete('cadernos_questoes');
		return $this->db->affected_rows();
	}

	public function get_resultado_questoes( $filtro ) {
		//$this->output->enable_profiler(TRUE);

		$this->db->select('q.que_id');
		//$this->db->distinct();
		$this->db->from('questoes as q');

		//Somente questões "aprovadas" são exibidas
		// $this->db->where('q.que_aprovacao', QUESTAO_APROVADA);

		if(!empty($filtro)) {
			self::adicionar_filtros($filtro);
		}

		if(!tem_acesso(array(ADMINISTRADOR, PROFESSOR, REVISOR))) {
			$this->db->where('q.que_ativo', SIM);
			$this->db->where('q.que_resposta IS NOT NULL', null, false);
			$this->db->where('(q.que_resposta != 0 OR q.que_status = 2)', null, false);
		}

		$total = $this->db->get();
		//echo "Última query: ".$this->db->last_query();
		return $total->result_array();
	}

	public function get_total_ativas_com_respostas() {
		$this->db->select('q.que_id');
		$this->db->distinct();
		$this->db->from('questoes as q');

		//Somente questões "aprovadas" são exibidas
		$this->db->where('q.que_aprovacao', QUESTAO_APROVADA);

		$this->db->where('q.que_ativo', SIM);
		$this->db->where('q.que_resposta IS NOT NULL', null, false);
		$this->db->where('(q.que_resposta != 0 OR q.que_status = 2)', null, false);

		return $this->db->count_all_results();
	}

	public function listar_questoes_sem_campo_busca($limit = 1000)
	{
		$this->db->from('questoes');
		$this->db->where('que_busca', null);
		$this->db->limit($limit);
		$query = $this->db->get();

		return $query->result_array();
	}

	public function atualizar_campo_busca($questao_id)
	{
		$questao = self::get_by_id($questao_id);

		$texto = $questao['que_texto_base'] . ' ' . $questao['que_enunciado'];

		$opcoes = self::listar_opcoes($questao_id);

		if($opcoes) {
			foreach ($opcoes as $opcao) {
				$texto .= ' ' . $opcao['qop_texto'];
			}
		}

		$texto = sanitizar($texto);

		$this->db->where('que_id', $questao_id);
		$this->db->set('que_busca', $texto);
		$this->db->update('questoes');
	}

	public function get_total_caderno($caderno_id) {
		$this->db->select('q.que_id');
		$this->db->distinct();
		$this->db->from('questoes as q');
		$this->db->join('questoes_provas as qp', 'qp.que_id = q.que_id', 'left');
		$this->db->join('provas as p', 'qp.pro_id = p.pro_id', 'left');
		$this->db->join('orgaos as o', 'o.org_id=p.org_id', 'left');
		$this->db->join('bancas as b', 'b.ban_id=p.ban_id', 'left');
		$this->db->join('disciplinas as d', 'd.dis_id=q.dis_id', 'left');
		$this->db->join('cadernos_questoes as cq', 'cq.que_id=q.que_id', 'left');
		$this->db->where('cad_id', $caderno_id);

		return $this->db->count_all_results();
	}

	public function get_resposta($questao_id) {
		$questao = self::get_by_id($questao_id);
		return $questao['que_resposta'];
	}

	public function listar_opcoes($questao_id, $ordenar = TRUE)
	{
		$this->db->from('questoes_opcoes');
		$this->db->where('que_id', $questao_id);
		$this->db->where('qop_texto !=', "<p><br></p>");
		$this->db->where('qop_texto !=', "");
		$this->db->order_by('qop_ordem');
		$query = $this->db->get();
		$result_array = $query->result_array();

		if($ordenar == FALSE){
			return $result_array;
		}

		$ordem = 1;
		$posicao_result = 0;
		$return_array = array();
		for($count = 0; $count < count($result_array); $count++) {
			if(isset($result_array[$posicao_result]['qop_ordem']) &&
					$ordem == $result_array[$posicao_result]['qop_ordem']) {
				$return_array[$count] = $result_array[$posicao_result];
				$posicao_result++;
			} else {
				$return_array[$count] = array('qop_texto' => '', 'qop_ordem' => $ordem, 'que_id' => $questao_id);
			}
			$ordem++;
		}

		return $return_array;
	}

	public function listar_tipos_erros_questao() {
		$this->db->from('questoes_erros_tipos');
		$this->db->order_by('qet_nome', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_assuntos_questao($questao_id, $somente_ativos = TRUE) {
		$this->db->select('a.ass_id, a.ass_nome');
		$this->db->from('assuntos a');
		$this->db->where('qa.que_id', $questao_id);

		if($somente_ativos) {
			$this->db->where('a.ass_ativo', SIM);
		}

		$this->db->join('questoes_assuntos qa','qa.ass_id=a.ass_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_provas_questao($questao_id) {
		$this->db->select('p.pro_id, p.pro_nome');
		$this->db->from('provas p');
		$this->db->where('qp.que_id', $questao_id);

		$this->db->join('questoes_provas qp','qp.pro_id=p.pro_id');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function salvar_opcoes($questao_id, $opcoes)
	{
		self::remover_opcoes($questao_id);

		$ordem = 1;
		foreach ($opcoes as $opcao) {
			$this->db->set('que_id', $questao_id );
			$this->db->set('qop_texto', $opcao);
			$this->db->set('qop_ordem', $ordem);
			$this->db->insert('questoes_opcoes');

			$ordem++;
		}
	}

	public function salvar_opcao($opcao)
	{
		self::remover_opcao($opcao['que_id'], $opcao['qop_ordem']);

		$this->db->insert('questoes_opcoes', $opcao);
	}

	public function alterar_opcao_texto($qop_id, $texto){
		$this->db->set('qop_texto', $texto);
		$this->db->where('qop_id', $qop_id);
		$this->db->update('questoes_opcoes');
	}

	public function remover_opcoes($questao_id)
	{
		$this->db->where('que_id', $questao_id);
		$this->db->delete('questoes_opcoes');
	}

	public function remover_opcao($questao_id, $ordem)
	{
		$this->db->where('que_id', $questao_id);
		$this->db->where('qop_ordem', $ordem);
		$this->db->delete('questoes_opcoes');
	}

	public function listar_respostas_usuarios_cadernos()
	{
		$this->db->where('cad_id IS NOT NULL', null, false);
		$query = $this->db->get('questoes_resolvidas');

		return $query->result_array();
	}

	public function listar_respostas_usuarios_simulados()
	{
		$this->db->where('sim_id IS NOT NULL', null, false);
		$query = $this->db->get('questoes_resolvidas');

		return $query->result_array();
	}

	public function salvar_resposta_usuario($usuario_id, $questao_id, $selecao, $simulado_id = null, $caderno_id = null) {

		if(is_null(self::get_resposta_usuario($usuario_id, $questao_id, $simulado_id, $caderno_id))) {

			$is_livre = !$simulado_id && !$caderno_id;

			$this->db->insert ( 'questoes_resolvidas', array(
				'usu_id' => $usuario_id,
				'que_id' => $questao_id,
				'qre_selecao' => $selecao,
			    'qre_data' => get_data_hora_agora(),
				'sim_id' => $simulado_id,
				'cad_id' => $caderno_id,
				'qre_primeira_selecao' => $is_livre ? $selecao : null

			));

			if($is_livre){

				$dificuldade = self::calcular_dificuldade_questao($questao_id);

				$this->db->set('que_dificuldade', $dificuldade);
				$this->db->where('que_id', $questao_id);
				$this->db->update('questoes');

			}

		} else {
			$this->db->set('qre_selecao', $selecao);
			$this->db->set('qre_data', get_data_hora_agora());
			$this->db->where('usu_id', $usuario_id);
			$this->db->where('que_id', $questao_id);
			$this->db->where('sim_id', $simulado_id);
			$this->db->where('cad_id', $caderno_id);
			$this->db->update('questoes_resolvidas');
		}

		// se for caderno ou simulado gravar resposta individual
		if($simulado_id || $caderno_id) {
			self::salvar_resposta_usuario($usuario_id, $questao_id, $selecao);
		}
	}

	public function get_questao_dificuldade($questao_id){

		$this->db->select('que_dificuldade');
		$this->db->from('questoes');
		$this->db->where('que_id', $questao_id);
		$query = $this->db->get();

		return $query->row_array()['que_dificuldade'];
	}

	public function calcular_dificuldade_questao($questao_id){

		$media = self::calcular_media_acertos_questao($questao_id);

		if($media <= 0.2500){
			return 5;
		}elseif($media <= 0.5000){
			return 4;
		}else if($media <= 0.7000){
			return 3;
		}else if($media <= 0.8500){
			return 2;
		}else{
			return 1;
		}

	}

	public function calcular_media_acertos_questao($questao_id) {

		$this->db->select('truncate(sum(case when q.que_resposta = qr.qre_primeira_selecao then 1 else 0 end)/count(*), 4) as media');
		$this->db->from('questoes q');
		$this->db->join('questoes_resolvidas qr', 'q.que_id = qr.que_id');

		$this->db->where('q.que_id', $questao_id);
		//primeira resposta só é salva para questões livres
		$this->db->where('qr.sim_id is null');
		$this->db->where('qr.cad_id is null');

		//ignora respostas vazias
		$this->db->where('qr.qre_primeira_selecao is not null');

		$query = $this->db->get();

		return $query->row_array()['media'];
	}

	public function get_resposta_usuario($usuario_id, $questao_id, $simulado_id = null, $caderno_id = null) {

		$this->db->join('questoes q', 'q.que_id = qr.que_id');
		$query = $this->db->get_where('questoes_resolvidas qr', array(
				'qr.que_id' => $questao_id,
				'qr.usu_id' => $usuario_id,
				'qr.sim_id' => $simulado_id,
				'qr.cad_id' => $caderno_id
		));

		if($query->num_rows() > 0)
			return $query->row_array();
		else
			return null;
	}

	public function is_resposta_certa($usuario_id, $questao_id, $simulado_id = null, $caderno_id = null)
	{
		$questao = self::get_by_id($questao_id);
		$resposta = self::get_resposta_usuario($usuario_id, $questao_id, $simulado_id, $caderno_id);

		return ($questao['que_resposta'] == $resposta['qre_selecao']) || ($questao['que_status'] == CANCELADA);
	}

	public function atualizar_assuntos($questao_id, $assuntos_selecionados)
	{
		$questao = self::get_by_id($questao_id);
		// busca pelo "Sem assunto" da disciplina da questão
		$sem_assunto = $this->assunto_model->get_by_nome_e_disciplina(SEM_ASSUNTO, $questao['dis_id']);

		// remove todos os assuntos da questão
		self::remover_todos_assuntos_de_questao($questao_id);

		// se tiver assunto selecionado procura pelo "Sem assunto" e o remove do array
		if($assuntos_selecionados) {
			$assuntos_selecionados = array_diff($assuntos_selecionados, array($sem_assunto['ass_id']));
		}

		// se não tiver nenhum assunto adiciona o "Sem assunto"
		if(!$assuntos_selecionados) {
			$assuntos_selecionados = array();
			array_push($assuntos_selecionados, $sem_assunto['ass_id']);
		}

		// salva os assuntos na questao
		foreach ($assuntos_selecionados as $assunto_id) {
			if(empty($assunto_id)) continue;
			$assunto = array('ass_id' => $assunto_id);
			$questao = array('que_id' => $questao_id);
			self::salvar_assunto_em_questao($assunto, $questao);
		}
	}

	public function atualizar_provas($questao_id, $provas_selecionadas)
	{
		$questao = self::get_by_id($questao_id);

		//Guarda as provas atuais da questão
		$provas_afetadas = self::get_provas_questao($questao_id);

		// remove todos os provas da questão
		self::remover_todas_provas_de_questao($questao_id);

		// salva as provas na questão
		foreach ($provas_selecionadas as $prova_id) {
			$prova = array('pro_id' => $prova_id);
			$questao = array('que_id' => $questao_id);
			self::salvar_prova_em_questao($prova, $questao);
			array_push($provas_afetadas, $prova);
		}

		self::atualizar_quantidade_questoes_provas($provas_afetadas);

		//Atualiza o ano da questão na tabela de questoes
		self::atualizar_ano_questoes(NULL, $questao_id);
	}

	/**
	 * Atualiza o ano das questões relacionadas a questão ou prova
	 * 
	 * @since L1
	 * 
	 * @param int $prova_id ID da prova
	 * @param int $questao_id ID da questão
	 */
	public function atualizar_ano_questoes($prova_id, $questao_id)
	{

		if($questao_id)
		{
			$this->db->where('q.que_id', $questao_id);
		}

		if($prova_id)
		{
			$this->db->where("q.que_id IN ( SELECT qp.que_id FROM questoes_provas qp WHERE qp.pro_id = {$prova_id} )", NULL, FALSE);
		}

		$this->db->set('q.que_ano', '(SELECT MAX(p.pro_ano) FROM provas p, questoes_provas qp WHERE p.pro_id = qp.pro_id AND qp.que_id = q.que_id)', FALSE);

		$this->db->update('questoes q');

	}

	/**
	 * Atualiza a informação de destaque de uma questão
	 * 
	 * @since L1
	 * 
	 * @param int $questao_id
	 */
	public function atualizar_destaque_questao($questao_id)
	{
		$this->db->set('q.que_tem_destaque', '( EXISTS( SELECT 1 FROM comentarios c WHERE c.que_id = q.que_id AND c.com_destaque = 1 ) OR EXISTS(SELECT 1 FROM comentarios_videos cv WHERE cv.que_id = q.que_id) )', FALSE);
		$this->db->where('q.que_id', $questao_id);
		$this->db->update('questoes q');
	}

	/**
	 * Atualiza a quantidade de questões das provas
	 * 
	 * @since L1
	 * 
	 * @param array $provas Provas que terão a quantidade de questões atualizadas
	 */
	public function atualizar_quantidade_questoes_provas( $provas ){
		
		foreach($provas as $prova){
			
			$this->db->where('pro_id', $prova['pro_id']);
			$this->db->from('questoes_provas');

			$total = $this->db->count_all_results();

			$this->db->where('pro_id', $prova['pro_id']);
			$this->db->set('pro_num_questoes', $total);

			$this->db->update('provas');
		}

	}

	public function remover_todos_assuntos_de_questao($questao_id)
	{
		$this->db->where('que_id', $questao_id);
		$this->db->delete('questoes_assuntos');
	}

	public function remover_todas_provas_de_questao($questao_id)
	{
		$this->db->where('que_id', $questao_id);
		$this->db->delete('questoes_provas');
	}

	public function salvar($questao)
	{
		$opcoes = $questao['opcoes'];
		$assuntos = $questao['ass_ids'];
		$provas = $questao['pro_ids'];

		unset($questao['opcoes']);
		unset($questao['ass_ids']);
		unset($questao['pro_ids']);
		$questao['que_data_criacao'] = get_data_hora_agora();

		$this->db->insert('questoes', $questao);
		$questao_id = $this->db->insert_id();

		self::salvar_opcoes($questao_id, $opcoes);
		self::atualizar_assuntos($questao_id, $assuntos);
		self::atualizar_provas($questao_id, $provas);
		self::atualizar_campo_busca($questao_id);
		return $questao_id;
	}

	public function salvar_assunto_em_questao($assunto, $questao)
	{
		$data = array('ass_id' => $assunto['ass_id'], 'que_id' => $questao['que_id']);

		if(!self::existe_assunto_em_questao($assunto, $questao)) {
			$this->db->insert('questoes_assuntos', $data);
		}
	}

	public function salvar_prova_em_questao($prova, $questao)
	{
		$data = array('pro_id' => $prova['pro_id'], 'que_id' => $questao['que_id']);

		if(!self::existe_prova_em_questao($prova, $questao)) {
			$this->db->insert('questoes_provas', $data);
		}
	}

	public function remover_assunto_de_questao($assunto_id, $questao_id)
	{
		$this->db->where('ass_id', $assunto_id);
		$this->db->where('que_id', $questao_id);
		$this->db->delete('questoes_assuntos');
	}

	public function remover_prova_de_questao($prova_id, $questao_id)
	{
		$this->db->where('pro_id', $prova_id);
		$this->db->where('que_id', $questao_id);
		$this->db->delete('questoes_provas');
	}

	public function existe_assunto_em_questao($assunto, $questao)
	{
		$data = array('ass_id' => $assunto['ass_id'], 'que_id' => $questao['que_id']);
		$query = $this->db->where($data);

		return $this->db->count_all_results('questoes_assuntos') > 0 ? true : false;
	}


	public function existe_prova_em_questao($prova, $questao)
	{
		$data = array('pro_id' => $prova['pro_id'], 'que_id' => $questao['que_id']);
		$query = $this->db->where($data);

		return $this->db->count_all_results('questoes_provas') > 0 ? true : false;
	}

	public function salvar_comentario($comentario) {
		$this->db->insert('comentarios', $comentario);
	}

	public function salvar_notificacao($notificacao) {
		$this->db->insert('questoes_erros', $notificacao);

		$usuario = get_usuario_array($notificacao['usu_id']);
		$notificacao['que_id'] = $this->input->post("id");
		$notificacao['qet_id'] = $this->input->post("qet_id");
		$notificacao['qer_justificativa'] = $this->input->post("qer_justificativa");

		$tipo = self::get_tipo_notificacao_by_id($notificacao['qet_id']);

		$dados = array(
			'questao_id' => $notificacao['que_id'],
			'justificativa' => $notificacao['qer_justificativa'],
			'usuario' => $usuario['nome_completo'],
			'email' => $usuario['email'],
			'tipo' => $tipo['qet_nome'],
			'url' => get_questao_url($notificacao['que_id'])
		);

		$mensagem = get_template_email('notificacao-questao.php', $dados);
		enviar_email(EMAIL_QUESTOES, "Notificação de erro na questão {$notificacao['que_id']} ({$tipo['qet_nome']})", $mensagem, NULL, EMAIL_LEONARDO);
	}

	/**
	 * TODO: Refatorar e eliminar a necessidade dos "unsets"
	 */
	public function atualizar($questao)
	{
		$this->load->helper('questao');

		$questao_antes = self::get_by_id($questao['que_id']);
		$assuntos_antes = self::get_assuntos_questao($questao_antes['que_id']);
		$provas_antes = self::get_provas_questao($questao_antes['que_id']);

		// remove formatação desnecessária do summernote
		$questao = formatar_input_summernote($questao);

		$opcoes = $questao['opcoes'];
		$assuntos = $questao['ass_ids'];
		$provas = $questao['pro_ids'];

		unset($questao['opcoes']);
		unset($questao['ass_ids']);
		unset($questao['pro_ids']);

		$this->db->where('que_id', $questao['que_id']);
		$this->db->set($questao);
		$this->db->update('questoes');

		self::salvar_opcoes($questao['que_id'], $opcoes);
		self::atualizar_assuntos($questao['que_id'], $assuntos);
		self::atualizar_provas($questao['que_id'], $provas);
		self::atualizar_campo_busca($questao['que_id']);

		$questao_nova = self::get_by_id($questao['que_id']);
		$assuntos_novos = self::get_assuntos_questao($questao['que_id']);

		self::verificar_alteracoes($questao_antes, $questao_nova, $assuntos_antes, $assuntos_novos);
		self::atualizar_inativa_manual($questao_antes, $questao_nova);

		return $questao['que_id'];
	}

	public function excluir($questao_id)
	{
		$provas_afetadas = self::get_provas_questao($questao_id);

		$this->db->where('que_id', $questao_id);
		
		$result = $this->db->delete('questoes');

		self::atualizar_quantidade_questoes_provas($provas_afetadas);

		return $result;
	}

	public function contar_por_disciplina($disciplina_id)
	{
		$this->db->from('questoes');
		$this->db->where('dis_id', $disciplina_id);
		return $this->db->count_all_results();
	}

	/**
	 * Métodos para scripts
	 */

	public function atualizar_todos_tipos_como_nulo()
	{
		$this->db->set('que_tipo', null);
		$this->db->update('questoes');
	}

	public function listar_questoes_com_tipo_nulo()
	{
		$this->db->limit(100);
		$query = $this->db->get_where('questoes', array('que_tipo' => null));
		return $query->result_array();
	}

	public function listar_questoes_com_disciplina_nula($primeiro_id)
	{
		$this->db->select('q.*, a.dis_id as disciplina_id');
		$this->db->limit(100);
		$this->db->from('questoes q');
		$this->db->join('questoes_assuntos qa ', 'qa.que_id = q.que_id');
		$this->db->join('assuntos a ', 'a.ass_id = qa.ass_id');
		$this->db->where('q.dis_id', null);
		$this->db->where('q.que_id >', $primeiro_id);
		$this->db->order_by('q.que_id');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function contar_opcoes($questao_id)
	{
		$this->db->where('que_id', $questao_id);
		$this->db->from('questoes_opcoes');
		return $this->db->count_all_results();
	}

	public function atualizar_tipo($questao_id, $tipo)
	{
		$this->db->set('que_tipo', $tipo);
		$this->db->where('que_id', $questao_id);
		$this->db->update('questoes');
	}

	public function atualizar_data_atualizacao($questao_id)
	{
		$this->db->set('que_data_atualizacao', get_data_hora_agora());
		$this->db->where('que_id', $questao_id);
		$this->db->update('questoes');
	}

	public function atualizar_disciplina($questao_id, $dis_id)
	{
		$this->db->set('dis_id', $dis_id);
		$this->db->where('que_id', $questao_id);
		$this->db->update('questoes');
	}

	public function contar_todas()
	{
		return $this->db->count_all('questoes');
	}

	public function contar_questoes($linha_campo, $linha_valor, $coluna_campo, $coluna_valor, $filtro)
	{
		// self::adicionar_filtros();

		$this->db->from('questoes q');
		$this->db->join('questoes_provas qp', 'qp.que_id = q.que_id');
		$this->db->join('provas p', 'qp.pro_id = p.pro_id');
		$this->db->join('bancas b', 'p.ban_id = b.ban_id');
		$this->db->join('orgaos o', 'p.org_id = o.org_id');
		$this->db->join('disciplinas d', 'd.dis_id = q.dis_id');
		$this->db->join('provas_areas_atuacao as pa', 'p.pro_id=pa.pro_id', 'left');

		if($linha_campo == 'c.user_id' || $coluna_campo == 'c.user_id') {
			$this->db->join('comentarios c', 'c.que_id = q.que_id');
		}

		$objetos = [$linha_campo => $linha_valor, $coluna_campo => $coluna_valor];

		foreach ($objetos as $campo => $valor) {

			switch ($campo) {
				case 'q.que_resposta':
					if($valor == COM_RESPOSTA) {
						$this->db->where('q.que_resposta > 0');
					}
					else {
						$this->db->where('(q.que_resposta = 0 OR q.que_resposta IS NULL)');
					}

					break;

				case 'c.user_id':
					$this->db->where('c.user_id', $valor);
					$this->db->where('c.com_destaque', 1);
					break;

				default:
					$this->db->where($campo, $valor);
					break;
			}
		}

		// adicao de filtros
		$posts = array('ban_ids' => 'p.ban_id', 'pro_anos' => 'p.pro_ano', 'esc_ids' => 'p.esc_id', 'dis_ids' => 'q.dis_id', 'ara_ids' => 'pa.ara_id', 'que_tipos' => 'q.que_tipo', 'que_statuses' => 'q.que_status'
		);

		foreach($posts as $post => $campo) {

			if(isset($filtro[$post])) {
				$count= 1;
				$item_ids = $filtro[$post];
				$where = "(";

				foreach($item_ids as $item_id) {

					if(($post == 'ara_ids' || $post == 'dis_ids') && $item_id == 0) {
						$item_id = null;
					}

					if(is_null($item_id) || $item_id == -1) {
						if($count == 1) {
							$where .= $campo . " IS NULL";
						}
						else {
							$where .= " OR " . $campo . " IS NULL";
						}
					}
					else {
						if($count == 1) {
							$where .= $campo . "=" . $item_id;
						}
						else {
							$where .= " OR " . $campo . "=" . $item_id;
						}
					}

					$count++;
				}
				$where .= ")";
				$this->db->where($where, null, false);
			}
		}

		return $this->db->count_all_results();
	}

	public function contar_questoes_com_tipo_nulo()
	{
		$this->db->where('que_tipo', null);
		$this->db->from('questoes');
		return $this->db->count_all_results();
	}

	public function contar_questoes_com_disciplina_nula($primeiro_id)
	{
		$this->db->where('que_id >', $primeiro_id);
		$this->db->where('dis_id', null);
		$this->db->from('questoes');
		return $this->db->count_all_results();
	}

	public function get_tipo_notificacao_by_id($id)
	{
		$this->db->where('qet_id', $id);
		$this->db->from('questoes_erros_tipos');
		$query = $this->db->get();

		return $query->row_array();
	}

	public function atualizar_url_reduzida($questao_id, $url_reduzida)
	{
		$this->db->set('que_url_reduzida', $url_reduzida);
		$this->db->where('que_id', $questao_id);
		$this->db->update('questoes');
	}

	public function atualizar_enunciado($questao_id, $enunciado)
	{
		$this->db->set('que_enunciado', $enunciado);
		$this->db->where('que_id', $questao_id);
		$this->db->update('questoes');

 		//echo $this->db->last_query();

	}

	public function atualizar_texto_opcao($questao_opcao_id, $texto)
	{
		$this->db->set('qop_texto', $texto);
		$this->db->where('qop_id', $questao_opcao_id);
		$this->db->update('questoes_opcoes');
	}

	public function salvar_questao_imagem($dados)
	{
		$this->db->insert('questoes_imagens',$dados);
	}

	public function atualizar_questao_imagem($questao_imagem_id, $dados)
	{
		$this->db->where('qim_id', $questao_imagem_id);
		$this->db->update('questoes_imagens',$dados);
	}

	public function listar_questao_imagens($tratadas = false, $limit = 20, $offset = 0)
	{
		$this->db->where('qim_tratada', (int)$tratadas);

		if($limit) {
			$this->db->limit($limit, $offset);
		}

		$query = $this->db->get('questoes_imagens');

		return $query->result_array();
	}

	public function listar_questoes_ativas_imagens_nao_tratadas()
	{
		$this->db->distinct();
		$this->db->select("q.que_id");
		$this->db->from('questoes q');
		$this->db->join('questoes_imagens qi', 'q.que_id = qi.que_id');
		$this->db->where('qi.qim_tratada', (int)0);
		$this->db->where('q.que_ativo', SIM);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function listar_todas_imagens_tratadas($inicio = null, $fim = null, $usuario_id = null)
	{
		$this->db->from('questoes_imagens');
		$this->db->where('qim_tratada', (int)1);

		if($inicio) {
			$this->db->where('qim_data_tratamento >=', $inicio . ' 00:00:00');
		}
		if($fim) {
			$this->db->where('qim_data_tratamento <=', $fim . ' 23:59:59');
		}
		if($usuario_id) {
			$this->db->where('qim_usu_id_tratamento', $usuario_id);
		}

		$query = $this->db->get();

		return $query->result_array();
	}

	public function listar_imagens_nao_tratadas($not_like = NULL, $limit = 100, $offset = 0)
	{
		$this->db->from('questoes_imagens');
		$this->db->where('qim_tratada', 0);

		if($not_like) {
			$this->db->not_like('qim_original_url', $not_like);
		}

		$this->db->limit($limit, $offset);
		$query = $this->db->get();

		return $query->result_array();
	}


	public function listar_usuarios_tratadores()
	{
		$this->db->distinct();
		$this->db->select('qim_usu_id_tratamento');
		$this->db->from('questoes_imagens');
		$this->db->where('qim_usu_id_tratamento IS NOT NULL', null, TRUE);
		$query = $this->db->get();

		$usuarios = array();
		if($resultado = $query->result_array()) {
			foreach ($resultado as $row) {
				array_push($usuarios, get_usuario_array($row['qim_usu_id_tratamento']));
			}
		}

		return $usuarios;
	}

	public function marcar_questao_como_inativa($questao_id)
	{
		$this->db->set('que_ativo', NAO);
		$this->db->where('que_id', $questao_id);
		$this->db->update('questoes');
	}

	public function marcar_questao_como_ativa($questao_id)
	{
		$this->db->set('que_ativo', SIM);
		$this->db->where('que_id', $questao_id);
		$this->db->update('questoes');
	}

	public function get_questao_imagens($questao_id, $tratadas = false, $limit = 20)
	{
		$this->db->where('qim_tratada', (int)$tratadas);
		$this->db->where('que_id', $questao_id);
		$this->db->limit($limit);
		$query = $this->db->get('questoes_imagens');

		return $query->result_array();
	}

	public function get_proxima_questao_imagem($questao_id, $tratadas = false)
	{
		$imagens = self::get_questao_imagem($questao_id);

		if(count($imagens)) {
			return $imagens[0];
		}

		return null;
	}

	public function contar_imagens_de_questao($questao_id, $tratadas = false)
	{
		$this->db->from('questoes_imagens');
		$this->db->where('qim_tratada', (int)$tratadas);
		$this->db->where('que_id', $questao_id);

		return $this->db->count_all_results();
	}

	public function get_questao_imagem($questao_id)
	{
		$this->db->where('qim_id', $questao_id);
		$query = $this->db->get('questoes_imagens');

		return $query->row_array();
	}

	public function contar_questao_imagens($tratadas = false)
	{
		$this->db->from('questoes_imagens');
		$this->db->where('qim_tratada', (int)$tratadas);

		return $this->db->count_all_results();
	}

	public function marcar_questao_imagem_como_tratada($imagem_id)
	{
		$this->db->set('qim_data_tratamento', get_data_hora_agora());
		$this->db->set('qim_usu_id_tratamento', get_current_user_id());
		$this->db->set('qim_tratada', 1);
		$this->db->where('qim_id', $imagem_id);
		$this->db->update('questoes_imagens');
	}

	public function listar_questoes_sem_gabarito($limit = 10)
	{
		$this->db->limit($limit);
		$this->db->order_by('que_id', 'random');
		$this->db->where('que_resposta', null);
		$this->db->where('que_qcon_id IS NOT NULL');
		$query = $this->db->get('questoes');

		return $query->result_array();
	}

	public function listar_questoes_com_gabarito_sem_texto_base($limit = 1000)
	{
		$this->db->limit($limit);
		$this->db->where('que_resposta IS NOT NULL');
		$this->db->where('que_qcon_id IS NOT NULL');
		$this->db->where('que_texto_base', null);
		$query = $this->db->get('questoes');

		return $query->result_array();
	}

	public function listar_questoes_sem_texto_base($limit = 1000)
	{
		$this->db->limit($limit);
		$this->db->where('que_qcon_id IS NOT NULL');
		$this->db->where('que_texto_base', null);
		$query = $this->db->get('questoes');

		return $query->result_array();
	}

	public function atualizar_resposta($questao_id, $resposta)
	{
		$this->db->where('que_id', $questao_id);
		$this->db->update('questoes', array(
			'que_resposta' => $resposta
		));
	}

	public function atualizar_texto_base($questao_id, $texto_base)
	{
		$this->db->where('que_id', $questao_id);
		$this->db->update('questoes', array(
				'que_texto_base' => $texto_base
		));
	}


	public function contar_questoes_respondidas($usuario_id, $data_inicio = null, $data_fim = null)
	{
		if(is_null($data_inicio)) {
			$data_inicio = date('Y-m-d 00:00:00');
		}

		if(is_null($data_fim)) {
			$data_fim = date('Y-m-d 23:59:59');
		}

		$this->db->from("questoes_resolvidas FORCE INDEX (que_usu_sim_cad_idx)");
		$this->db->where('usu_id', $usuario_id);
		$this->db->where('qre_data >=', $data_inicio);
		$this->db->where('qre_data <=', $data_fim);

		return $this->db->count_all_results();
	}


	public function atualizar_status_por_qcon_id($qcon_id, $status)
	{
		$this->db->where('que_qcon_id', $qcon_id);
		$this->db->update('questoes', array(
				'que_status' => $status
		));
	}

	public function atualizar_ativo_inativo($que_id, $ativo)
	{
		$this->db->where('que_id', $que_id);
		$this->db->update('questoes', array(
				'que_ativo' => $ativo
		));
	}

	public function atualizar_questao_relacionada($questao_id, $questao_relacionada_id)
	{
		if($questao_relacionada_id == 0) {
			$questao_relacionada_id = null;
		}

		$this->db->where('que_id', $questao_id);
		$this->db->update('questoes', array(
			'que_referencia_id' => $questao_relacionada_id
		));
	}

	public function listar_simulados_onde_questao_aparece($questao_id, $excluir_simulado_id = null)
	{

		$this->db->from('simulados s');
		$this->db->join('simulados_questoes sq', 's.sim_id  = sq.sim_id');
		$this->db->where('sq.que_id', $questao_id);

		if(!is_null($excluir_simulado_id)) {
			$this->db->where('s.sim_id !=', $excluir_simulado_id);
		}

		$query = $this->db->get();

		return $query->result_array();
	}

	public function marcar_desmarcar_favorita($questao_id, $usuario_id, $tipo = QUESTAO_FAVORITA)
	{
		if(!self::is_favorita($questao_id, $usuario_id, $tipo)) {

			$this->db->insert('questoes_favoritas', array(
				'que_id' => $questao_id,
				'usu_id' => $usuario_id,
				'qfa_data' => date('Y-m-d H:i:s'),
				'qfa_tipo' => $tipo
			));

			//Quem favorita a questão passa a acompanhar automaticamente
			if($tipo == QUESTAO_FAVORITA && !self::is_favorita($questao_id, $usuario_id, QUESTAO_ACOMPANHADA)){

				$this->db->insert('questoes_favoritas', array(
					'que_id' => $questao_id,
					'usu_id' => $usuario_id,
					'qfa_data' => date('Y-m-d H:i:s'),
					'qfa_tipo' => QUESTAO_ACOMPANHADA
				));

			}
		}
		else {
			$this->db->where('que_id', $questao_id);
			$this->db->where('usu_id', $usuario_id);
			$this->db->where('qfa_tipo', $tipo);
			$this->db->delete('questoes_favoritas');
		}
	}

	/**
	 * Retorna se a questão está marcada como favorita para o usuário
	 * Uma questão favorita do tipo QUESTAO_FAVORITA é uma questão favoritada pelo usuário
	 * Uma questão favorita do tipo QUESTAO_ACOMPNHADA é uma questão apenas acompanhada pelo usuário
	 * 
	 * @since L1
	 * 
	 * @param int $questao_id ID da questão
	 * @param int $usuario_id ID do usuário
	 * @param int $tipo tipo de favorito
	 * 
	 * @return array informações do primeiro registro de favorito encontrado
	 */
	public function is_favorita($questao_id, $usuario_id, $tipo = QUESTAO_FAVORITA)
	{
		$this->db->where('que_id', $questao_id);
		$this->db->where('usu_id', $usuario_id);

		//if(!is_null($tipo)){
			$this->db->where('qfa_tipo', $tipo);
		//}

		$query = $this->db->get('questoes_favoritas');

		return $query->row_array();
	}

	public function contar_questoes_resolvidas($usuario_id, $disciplina_id = null)
	{
		$this->db->from('questoes_resolvidas qr');
		$this->db->where('qr.usu_id', $usuario_id);

		if(!is_null($disciplina_id)) {
			$this->db->join('questoes q', 'q.que_id = qr.que_id');
			$this->db->where('q.dis_id', $disciplina_id);
		}

		// $query = $this->db->get('questoes');
		return $this->db->count_all_results();
	}

	/**
	 * TODO: Verificar necessidade de função semelhante em Simulado_model::contar_acertos_por_disciplina
	 **/
	public function contar_questoes_acertadas($usuario_id, $disciplina_id = null, $simulado_id = null)
	{
		$this->db->from('questoes_resolvidas qr');
		$this->db->where('qr.usu_id', $usuario_id);
		$this->db->join('questoes q', 'q.que_id = qr.que_id');

		if(!is_null($disciplina_id)) {
			$this->db->where('q.dis_id', $disciplina_id);
		}

		if($simulado_id) {
			$this->db->where('qr.sim_id', $simulado_id);
		}

		$this->db->where('(qr.qre_selecao = q.que_resposta OR q.que_status = 2)'); // inclui questoes anuladas

		return $this->db->count_all_results();
	}

	public function listar_imagens_erradas($limit = 1000)
	{
		$this->db->from('questoes_imagens');
		$this->db->like('qim_original_url', 'Imagem', 'before');
		$this->db->like('qim_url', '_tb.jpg', 'before');
		$this->db->order_by('que_id');
		$this->db->limit($limit);
		$query = $this->db->get();

		return $query->result_array();
	}


	public function listar_disciplinas_favoritas($usuario_id)
	{
		$this->db->distinct();
		$this->db->select('d.*');
		$this->db->from('disciplinas d');
		$this->db->join('questoes q', 'd.dis_id = q.dis_id');
		$this->db->join('questoes_favoritas qf', 'q.que_id = qf.que_id');
		$this->db->where('qf.usu_id', $usuario_id);
		$this->db->where('qf.qfa_tipo', QUESTAO_FAVORITA);
		$this->db->order_by('d.dis_nome', 'asc');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_total_resolvidas_favoritas($usuario_id, $disciplina_id, $resultado = null)
	{
		$this->db->from('questoes q');
		$this->db->join('questoes_favoritas qf', 'q.que_id = qf.que_id');
		$this->db->join('questoes_resolvidas qr', 'qr.que_id = qf.que_id');
		$this->db->where('dis_id', $disciplina_id);
		$this->db->where('qf.usu_id', $usuario_id);
		$this->db->where('qf.qfa_tipo', QUESTAO_FAVORITA);
		$this->db->where('qr.usu_id', $usuario_id);

		if(!is_null($resultado)) {

			if($resultado == 1) {
				$this->db->where('qre_selecao = q.que_resposta');
			}
			else {
				$this->db->where('qre_selecao != q.que_resposta');
			}
		}

		return $this->db->count_all_results();
	}

	public function get_total_questoes_favoritas($usuario_id, $disciplina_id = null)
	{
		$this->db->from('questoes_favoritas qf');
		$this->db->join('questoes q', 'q.que_id = qf.que_id');
		$this->db->where('qf.usu_id', $usuario_id);
		$this->db->where('qf.qfa_tipo', QUESTAO_FAVORITA);

		if($disciplina_id) {
			$this->db->where('dis_id', $disciplina_id);
		}

		return $this->db->count_all_results();
	}

	public function listar_imagens_tratadas_de_questoes_inativas()
	{
		$this->db->from('questoes_imagens qi');
		$this->db->join('questoes q' , 'qi.que_id = q.que_id');
		$this->db->where('q.que_ativo', NAO);
		$this->db->where('qi.qim_tratada', SIM);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function trocar_assunto($antigo_id, $novo_id)
	{
		$this->db->where('ass_id', $antigo_id);
		$this->db->update('questoes_assuntos', array('ass_id' => $novo_id));
	}

	public function listar_por_assunto($assunto_id)
	{
		$this->db->where('ass_id', $assunto_id);
		$query = $this->db->get('questoes_assuntos');

		return $query->result_array();
	}

	public function listar_questoes_ids_opcoes_incompletas($limit = 10)
	{
		$query = $this->db->query("select q.que_id, q.que_qcon_id from questoes q left join questoes_opcoes qo on q.que_id = qo.que_id where que_tipo = 1 and que_data_atualizacao is null group by q.que_id having count(*) < 4 order by rand() limit {$limit}");

		return $query->result_array();
	}



	/******************************************************************************
	 * Ativar todas as questões sem imagem que possuem resposta e estão inativas
	 * https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1760
	 *****************************************************************************/

	public function listar_questoes_inativas_nao_marcadas($limit = 100)
	{
		$this->db->where('que_sem_imagem IS NULL');
		$this->db->where('que_ativo', INATIVO);
		$this->db->limit($limit);
		$query = $this->db->get('questoes');

		return $query->result_array();
	}

	public function contar_questoes_inativas_nao_marcadas()
	{
		$this->db->from('questoes');
		$this->db->where('que_sem_imagem IS NULL');
		$this->db->where('que_ativo', INATIVO);

		return $this->db->count_all_results();
	}

	/**
	 * Marca questões que não possuem imagem
	 *
	 * @param int $questao_id
	 * @param bool $sem_imagem
	 *
	 * @return array
	 */

	public function marcar_sem_imagem($questao_id, $sem_imagem)
	{
		$this->db->set('que_sem_imagem', $sem_imagem);
		$this->db->where('que_id', $questao_id);
		$this->db->update('questoes');
	}

	/**
	 * Desmarca flag "sem imagem" de todas as questões inativas
	 *
	 * @since J5
	 */

	public function desmarcar_sem_imagem_questoes_inativas()
	{
		$this->db->set('que_sem_imagem', null);
		$this->db->where('que_ativo', INATIVO);
		$this->db->update('questoes');
	}

	/**
	 * Lista questões inativas que não possuem imagem
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1981
	 *
	 * @param int $limit
	 *
	 * @return array
	 */

	public function listar_questoes_inativas_sem_imagem($limit = 100)
	{
		$this->db->where('que_ativo', INATIVO);

		#1981 - Anuladas não precisam ter resposta
		$this->db->where('((que_resposta > 0 AND que_sem_imagem = 1) OR que_status = 2)');

		$this->db->where('que_inativa_manual', 0);

		$this->db->limit($limit);
		$query = $this->db->get('questoes');

		return $query->result_array();
	}

	/**
	 * Conta número de questões inativas que não possuem imagem
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1981
	 *
	 * @return int
	 */

	public function contar_questoes_inativas_sem_imagem()
	{
		$this->db->from('questoes');
		$this->db->where('que_ativo', INATIVO);

		#1981 - Anuladas não precisam ter resposta
		$this->db->where('((que_resposta > 0 AND que_sem_imagem = 1) OR que_status = 2)');

		$this->db->where('que_inativa_manual', 0);

		return $this->db->count_all_results();
	}

	public function listar_questoes_respondidas_agrupadas_por_banca($usuario_id, $inicio, $fim, $bancas, $disciplinas) {

		$where_bancas = "";
		if($bancas){
			$bancas_ids = implode(',', $bancas);
			$where_bancas = " and b.ban_id in ({$bancas_ids}) ";
		}

		$where_disciplinas = "";
		if($disciplinas){
			$disciplinas_ids = implode(',', $disciplinas);
			$where_disciplinas = " and q.dis_id in ({$disciplinas_ids}) ";
		}

		$sql = "SELECT b.*, count(distinct q.que_id) as total, count(case when qr.qre_selecao = q.que_resposta then 1 end) as acertos
				from bancas b join provas p on p.ban_id = b.ban_id
				join questoes_provas qp on qp.pro_id = p.pro_id
				join questoes q on q.que_id = qp.que_id
				join questoes_resolvidas qr on qr.que_id = q.que_id
				where qr.qre_data >= '{$inicio} 00:00:00'
				AND qr.qre_data <= '{$fim} 23:59:59'
				and qr.cad_id IS NULL
				and qr.sim_id IS NULL
				and qr.usu_id={$usuario_id}
				and q.que_ativo = 1
				{$where_bancas}
				{$where_disciplinas}
				group by ban_id
				order by total desc";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function listar_questoes_respondidas_agrupadas_por_disciplina($usuario_id, $inicio, $fim, $bancas, $disciplinas) {

		$where_bancas = "";
		if($bancas){
			$bancas_ids = implode(',', $bancas);
			$where_bancas = " and exists(select 1 from questoes_provas qp, provas p where qp.pro_id = p.pro_id and qp.que_id = q.que_id and p.ban_id in ({$bancas_ids})) ";
		}

		$where_disciplinas = "";
		if($disciplinas){
			$disciplinas_ids = implode(',', $disciplinas);
			$where_disciplinas = " and q.dis_id in ({$disciplinas_ids}) ";
		}

		$sql = "SELECT d.*, count(distinct q.que_id) as total
				from disciplinas d
				join questoes q on q.dis_id = d.dis_id
				join questoes_resolvidas qr on qr.que_id = q.que_id
				where qr.qre_data >= '{$inicio} 00:00:00'
				AND qr.qre_data <= '{$fim} 23:59:59'
				and qr.cad_id IS NULL
				and qr.sim_id IS NULL
				and qr.usu_id={$usuario_id}
				and q.que_ativo = 1
				{$where_bancas}
				{$where_disciplinas}
				group by dis_id
				order by total desc";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function contar_questao_resposta($questao_id, $is_certa, $simulado_id = null, $caderno_id = null, $is_primeira_resposta = FALSE){

		$this->db->from('questoes_resolvidas qr');
		$this->db->join('questoes q', 'qr.que_id = q.que_id');
		$this->db->where('qr.que_id', $questao_id);
		$this->db->where('qr.sim_id', $simulado_id);
		$this->db->where('qr.cad_id', $caderno_id);

		$campo_selecao = "qr.qre_selecao";
		if(is_null($simulado_id) && is_null($caderno_id) && $is_primeira_resposta == TRUE){
			$campo_selecao = "qr.qre_primeira_selecao";
		}

		if($is_certa){
			$this->db->where($campo_selecao.' = q.que_resposta');
		}else{
			$this->db->where($campo_selecao.' != q.que_resposta');
		}
		#$this->output->enable_profiler(TRUE);
		return $this->db->count_all_results();
	}

	public function contar_questao_resposta_alternativas($questao_id, $simulado_id = null, $caderno_id = null,$is_primeira_resposta = FALSE){

		$campo_selecao = "qre_selecao";
		if(is_null($simulado_id) && is_null($caderno_id) && $is_primeira_resposta == TRUE){
			$campo_selecao = "qre_primeira_selecao";
		}

		$this->db->select($campo_selecao.' as item, count(*) as total');

		$this->db->where('que_id', $questao_id);
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('cad_id', $caderno_id);
		$this->db->where($campo_selecao.' is not null');

		$this->db->group_by($campo_selecao);
		$this->db->order_by($campo_selecao, 'asc');

		$query = $this->db->get('questoes_resolvidas');

		return $query->result_array();
	}

	public function buscar_questoes_provas_nao_inicializadas()
	{
		$query = $this->db->query("SELECT que_id, pro_id from questoes where que_id not in (select que_id from questoes_provas) limit 10000");
		return $query->result_array();
	}

	public function contar_questoes_provas_nao_inicializadas()
	{
		$query = $this->db->query("SELECT count(*) as soma from questoes where que_id not in (select que_id from questoes_provas)");

		$resultado = $query->row_array();

		return $resultado['soma'];
	}

	/**
	 * Listar provas associadas a uma questão
	 *
	 * @since K1
	 *
	 * @param $questao_id int
	 *
	 * @return array
	 */

	public function listar_questao_prova_por_questao_id($questao_id)
	{
		$this->db->where('que_id', $questao_id);
		$query = $this->db->get('questoes_provas');

		return $query->result_array();
	}

	/**
	 * Recupera um registro na tabela questoes_provas
	 *
	 * @since K1
	 *
	 * @param $questao_id int
	 * @param $prova_id int
	 *
	 * @return array
	 */

	public function get_questao_prova($questao_id, $prova_id)
	{
		$this->db->where('que_id', $questao_id);
		$this->db->where('pro_id', $prova_id);
		$query = $this->db->get('questoes_provas');

		return $query->row_array();
	}

	/**
	 * Salva um registro na tabela questoes_provas
	 *
	 * @since K1
	 *
	 * @param $questao_id int
	 * @param $prova_id int
	 */

	public function salvar_questao_prova($questao_id, $prova_id)
	{
		$this->db->set('que_id', $questao_id);
		$this->db->set('pro_id', $prova_id);
		$this->db->insert('questoes_provas');
	}

	public function salvar_questoes_provas($questoes_provas)
	{
		$this->db->insert_batch('questoes_provas', $questoes_provas);
	}

	public function listar_questoes_repetidas($status = null)
	{
		$this->db->where('qur_status', $status);
		$query = $this->db->get('questoes_repetidas');

		return $query->result_array();
	}

	public function listar_questoes_por_master_id($master_id)
	{
		$this->db->where('que_id_master', $master_id);
		$this->db->where('qur_status', STATUS_COMPARACAO_RESOLVIDA);

		$query = $this->db->get('questoes_repetidas');

		return $query->result_array();
	}

	public function listar_questoes_por_repetida_id($repetida_id)
	{
		$this->db->where('que_id_repetida', $repetida_id);
		$this->db->where('qur_status', STATUS_COMPARACAO_RESOLVIDA);

		$query = $this->db->get('questoes_repetidas');

		return $query->result_array();
	}

	public function atualizar_questao_repetida($master_id, $repetida_id, $inativar, $usuario_id, $data_hora, $inverter_master = false) {

		$this->db->set('usu_id', $usuario_id);
		$this->db->set('qur_data_hora', $data_hora);
		$this->db->set('qur_status', STATUS_COMPARACAO_RESOLVIDA);

		$this->db->where('que_id_master', $master_id);
		$this->db->where('que_id_repetida', $repetida_id);

		if($inverter_master) {
			$this->db->set('que_id_master', $repetida_id);
			$this->db->set('que_id_repetida', $master_id);
		}

		$this->db->update('questoes_repetidas');

		if($inativar) {
			$inativar_id = $inverter_master ? $master_id : $repetida_id;

			self::marcar_questao_como_inativa($inativar_id);

			$manter_id = $inverter_master ? $repetida_id : $master_id;

			self::atualizar_questao_repetida_cadernos($manter_id, $inativar_id);
			self::atualizar_questao_repetida_simulados($manter_id, $inativar_id);
			self::atualizar_questao_repetida_resolvidas($manter_id, $inativar_id);
		}
	}

	public function ignorar_questao_repetida($master_id, $repetida_id, $usuario_id, $data_hora)
	{
		$this->db->set('usu_id', $usuario_id);
		$this->db->set('qur_data_hora', $data_hora);
		$this->db->set('qur_status', STATUS_COMPARACAO_IGNORADA);

		$this->db->where('que_id_master', $master_id);
		$this->db->where('que_id_repetida', $repetida_id);

		$this->db->update('questoes_repetidas');
	}

	public function atualizar_questao_repetida_cadernos($master_id, $repetida_id)
	{
		$sql = "UPDATE ignore cadernos_questoes set que_id = {$master_id} where que_id = ${repetida_id}";
		$this->db->query($sql);
	}

	public function atualizar_questao_repetida_simulados($master_id, $repetida_id)
	{
		$sql = "UPDATE ignore simulados_questoes set que_id = {$master_id} where que_id = ${repetida_id}";
		$this->db->query($sql);
	}

	public function atualizar_questao_repetida_resolvidas($master_id, $repetida_id)
	{
		$sql = "UPDATE ignore questoes_resolvidas set que_id = {$master_id} where que_id = ${repetida_id}";
		$this->db->query($sql);
	}

	public function listar_proximas_repetidas($limit, $disciplinas, $anos)
	{
		$join = "";
		$where = "";
		if($disciplinas) {
			$where .= " AND q.dis_id IN ($disciplinas) ";
		}

		if($anos) {
			$join = " LEFT JOIN questoes_provas qp ON q.que_id = qp.que_id LEFT JOIN provas p ON qp.pro_id = p.pro_id ";
			$where .= " AND p.pro_ano IN ($anos) ";
		}

		$sql = "SELECT q.que_id, min(qrx_data_hora) from questoes q {$join} left join questoes_repetidas_detalhes qrd on q.que_id = qrd.que_id left join questoes_repetidas_execucoes qrx on qrx.qrx_id = qrd.qrx_id WHERE 1=1 {$where} group by q.que_id order by min(qrx.qrx_data_hora) limit $limit";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function listar_candidatas_repetidas($questao_id, $disciplina_id, $bancas_ids)
	{
		$where_disciplina = $disciplina_id ? " and q.dis_id = {$disciplina_id} " : "";
		$where_bancas = $bancas_ids ? " AND p.ban_id IN ($bancas_ids) " : "";
		$join_bancas = $bancas_ids ? " LEFT JOIN questoes_provas qp ON qp.que_id = q.que_id LEFT JOIN provas p ON p.pro_id = qp.pro_id " : "";

		$sql =
			"SELECT q.que_id, que_enunciado, que_texto_base from questoes q {$join_bancas}
				where q.que_id != {$questao_id}
					{$where_disciplina}
					{$where_banca}
					and q.que_id not in (select que_id_repetida from questoes_repetidas where que_id_master = {$questao_id})
					and q.que_id not in (select que_id_master from questoes_repetidas where que_id_repetida = {$questao_id})";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function salvar_execucao_repetidas($dados)
	{
		$this->db->insert('questoes_repetidas_execucoes', $dados);
		return $this->db->insert_id();
	}

	public function salvar_execucao_repetidas_detalhes($dados)
	{
		$this->db->insert('questoes_repetidas_detalhes', $dados);
	}

	public function salvar_questao_repetida($master_id, $repetida_id, $execucao_id)
	{
		$this->db->set('que_id_master', $master_id);
		$this->db->set('que_id_repetida', $repetida_id);
		$this->db->set('qrx_id', $execucao_id);
		$this->db->insert('questoes_repetidas');
	}

	public function atualizar_tempo_gasto_execucao_repetidas($execucao_id, $tempo_gasto)
	{
		$this->db->set('qrx_tempo_gasto', $tempo_gasto);
		$this->db->where('qrx_id', $execucao_id);
		$this->db->update('questoes_repetidas_execucoes');
	}

	public function listar_questoes_repetidas_execucoes()
	{
		$sql =
			"SELECT qrx.*,
				(select count(*) from questoes_repetidas_detalhes where qrx_id = qrx.qrx_id) as qrx_num_executadas,
			    (select count(*) from questoes_repetidas where qrx_id = qrx.qrx_id) as qrx_num_repetidas
			from questoes_repetidas_execucoes qrx
			order by qrx_data_hora desc limit 100";

 		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function listar_questoes_repetidas_executadas($execucao_id, $somente_repetidas = false)
	{
		if($somente_repetidas) {
			$sql = "SELECT * from questoes_repetidas where qrx_id = {$execucao_id}";
		}
		else {
			$sql = "SELECT * from questoes_repetidas_detalhes where qrx_id = {$execucao_id}";
		}

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	public function listar_provas($questao_id)
	{
		$this->db->where('que_id', $questao_id);
		$query = $this->db->get('questoes_provas');

		return $query->result_array();
	}

	public function contar_assuntos_questao($que_id){
		$this->db->from('questoes_assuntos');
		$this->db->where('que_id', $que_id);
		return $this->db->count_all_results();
	}

	/**
	 * Lista as questões que possuem assuntos que não pertecem à disciplina da questão
	 *
	 * @since k2
	 *
	 * @param $limit int Limit
	 *
	 * @return array Questões
	 */

	public function listar_questoes_com_assuntos_incorretos($limit = 100)
	{
		$sql = "SELECT *, q.dis_id as q_dis_id from questoes q join questoes_assuntos qa on q.que_id = qa.que_id join assuntos a on qa.ass_id = a.ass_id where a.dis_id != q.dis_id limit $limit";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	/**
	 * Lista as questões que não possuem nenhum assunto ativo
	 *
	 * @since k2
	 *
	 * @return array Questões
	 */

	public function listar_questoes_com_nenhum_assunto_ativo()
	{
		$sql = "SELECT que_id, dis_id from questoes where dis_id != -1 and que_id not in (select distinct que_id from questoes_assuntos qa join assuntos a on qa.ass_id = a.ass_id where a.ass_ativo = 1)";

		$query = $this->db->query($sql);

		return $query->result_array();
	}


	/**
	 * Conta o número de questões que possuem assuntos que não pertecem à disciplina da questão
	 *
	 * @since k2
	 *
	 * @return int Número de questões
	 */

	public function contar_questoes_com_assuntos_incorretos()
	{
		$sql  = "SELECT COUNT(*) as num from questoes q join questoes_assuntos qa on q.que_id = qa.que_id join assuntos a on qa.ass_id = a.ass_id where a.dis_id != q.dis_id";

		$query = $this->db->query($sql);

		$linha = $query->row_array();

		return $linha['num'];
	}

	/**
	 * Salva correção de assuntos de questão
	 *
	 * @since k2
	 *
	 * @param array $dados
	 */

	public function salvar_questao_assunto_corretor($dados)
	{
		$this->db->insert('questoes_assuntos_corretor', $dados);
	}

	/**
	 * Lista as correções de assuntos de questões
	 *
	 * @since k2
	 *
	 * @param int $offset
	 * @param string $palavra
	 * @param int $limit
	 *
	 * @return array Questões
	 */

	public function listar_questoes_assuntos_corretor($offset, $palavra = NULL, $limit = QUESTOES_ASSUNTOS_CORRETOR_POR_PAGINA)
	{
		$this->db->from('questoes_assuntos_corretor');

		if(!is_null($palavra)) {
			$this->db->like('que_id', $palavra);
			$this->db->or_like('qac_antes', $palavra);
			$this->db->or_like('qac_depois', $palavra);
		}

		$this->db->order_by("qac_data_hora DESC");
		$this->db->limit($limit, $offset);

		$query = $this->db->get();

		return $query->result_array();

	}

	/**
	 * Conta o número de correções de assuntos de questões
	 *
	 * @since k2
	 *
	 * @param string $palavra
	 *
	 * @return int Número de questões
	 */

	public function contar_questoes_assuntos_corretor($palavra = NULL)
	{
		$this->db->from('questoes_assuntos_corretor a');

		if(!is_null($palavra)) {
			$this->db->like('que_id', $palavra);
			$this->db->or_like('qac_antes', $palavra);
			$this->db->or_like('qac_depois', $palavra);
		}

		return $this->db->count_all_results();
	}

	public function listar_questoes_sem_conversao($disciplina_id){

		$this->db->distinct();
		$this->db->select('q.*');
		$this->db->from('questoes q');
		$this->db->join('questoes_assuntos qa', 'qa.que_id = q.que_id');
		$this->db->where('q.dis_id', $disciplina_id);
		$this->db->where('not exists(select 1 from assuntos_conversao ac where ac.ass_antigo = qa.ass_id and ac.dis_id is null)');

		$query = $this->db->get();

		return $query->result_array();

	}

	public function listar_questoes_afetadas_conversao_default($disciplina_id){

		$this->db->distinct();
		$this->db->select('q.que_id, sq.sim_id');
		$this->db->from('questoes q');
		$this->db->join('questoes_assuntos qa', 'qa.que_id = q.que_id');
		$this->db->join('simulados_questoes sq', 'q.que_id = sq.que_id');
		$this->db->where('q.dis_id', $disciplina_id);
		$this->db->where('not exists(select 1 from assuntos_conversao ac where ac.ass_antigo = qa.ass_id and ac.dis_id is null)');

		$query = $this->db->get();

		return $query->result_array();

	}

	/**
	 * Conta quantas questões estão sem comentário em destaque de professor para um determinado caderno
	 *
	 * @since K5
	 *
	 * @param int $caderno_id Id do caderno
	 */
	public function contar_questoes_caderno_sem_comentario_professor($caderno_id){
		return self::contar_questoes_sem_comentario_professor(null, $caderno_id);
	}

	/**
	 * Conta quantas questões estão sem comentário em destaque de professor para um determinado simulado
	 *
	 * @since K5
	 *
	 * @param int $simulado_id Id do simulado
	 */
	public function contar_questoes_simulado_sem_comentario_professor($simulado_id){
		return self::contar_questoes_sem_comentario_professor($simulado_id, null);
	}

	/**
	 * Conta quantas questões estão sem comentário em destaque de professor para um determinado simulado ou caderno
	 *
	 * @since K5
	 *
	 * @param int $simulado_id Id do simulado
	 * @param int $caderno_id Id do caderno
	 */
	private function contar_questoes_sem_comentario_professor($simulado_id, $caderno_id){

		if(!is_null($simulado_id)){
			$this->db->from('simulados_questoes q');
			$this->db->where('q.sim_id', $simulado_id);
		}

		if(!is_null($caderno_id)){
			$this->db->from('cadernos_questoes q');
			$this->db->where('q.cad_id', $caderno_id);
		}

		$this->db->where('not exists(select 1 from comentarios c where c.que_id = q.que_id and c.com_destaque = 1)');

		return $this->db->count_all_results();

	}

	public function aprovar_reprovar_questao($que_id, $is_aprovar){

		if($is_aprovar == TRUE){
			$this->db->set('que_aprovacao', QUESTAO_APROVADA);
		}else{
			$this->db->set('que_aprovacao', QUESTAO_REPROVADA);
		}

		$this->db->where('que_id', $que_id);

		$this->db->update('questoes');
	}

	public function listar_questoes_professor($filtros, $limit = null, $offset = null, $contar = false)
	{
		$this->db->from('questoes q');
		$this->db->where('q.usu_id IS NOT NULL');

		if(isset($filtros)) {

		    if(isset($filtros['professores']) && $filtros['professores']) {
		        $this->db->where_in('q.usu_id', $filtros['professores']);
		    }

    		if(isset($filtros['disciplinas']) && $filtros['disciplinas']) {
    		    $this->db->where_in('q.dis_id', $filtros['disciplinas']);
			}

			if(isset($filtros['assuntos']) && $filtros['assuntos']) {
				$assuntos_ids = implode(',', $filtros['assuntos']);
    		    $this->db->where("exists(select 1 from questoes_assuntos qa where qa.que_id = q.que_id and qa.ass_id in ({$assuntos_ids}))");
			}

    		if(isset($filtros['aprovacao']) && $filtros['aprovacao']) {
    		    $this->db->where_in('q.que_aprovacao', $filtros['aprovacao']);
    		}

    		if(isset($filtros['procedencia']) && $filtros['procedencia']) {
    		    $this->db->where_in('q.que_procedencia', $filtros['procedencia']);
    		}
	    }

		if($limit) {
		    $this->db->limit($limit, $offset);
		}

		if($contar) {
		    return $this->db->count_all_results();
		}
		else {
    		$this->db->order_by('q.que_id asc');

    		$query = $this->db->get();

    		$questoes = $query->result_array();

    		self::adicionar_info_extra($questoes);

    		return $questoes;
		}
	}

	public function contar_questoes_professor($filtros)
	{
	    return self::listar_questoes_professor($filtros, null, null, true);
	}


	/**
	 * Altera o valor do campo inativa manual
	 *
	 * @since L1
	 *
	 * @param array $questao_antiga Array associativo da questão antes da mudança
	 * @param array $questao_nova Array associativo da questão após a mudança
	 */

	public function atualizar_inativa_manual($questao_antiga, $questao_nova)
	{

	    // Critérios para para marcar como inativa manual:
	    // 1 - Campo que_ativo deve ter novo valor = INATIVA
	    // 2 - Campo que_ativo dever ser ter sido modificado
	    if(($questao_antiga["que_ativo"] != $questao_nova["que_ativo"]) && $questao_nova["que_ativo"] == INATIVO) {

	        $this->db->where("que_id", $questao_nova["que_id"]);
	        $this->db->update("questoes", [
	            "que_inativa_manual" => SIM
	        ]);
	    }
	}

	/**
	 * Atualiza a quantidade de comentários de uma questão
	 *
	 * @since L1
	 *
	 * @param int $que_id ID da questão que terá a quantidade de comentários alterada
	 * @param int $total A quantidade de comentários da questão
	 */

	 public function atualizar_numero_comentarios($que_id, $total){

		//Atualiza a quantidade de comentários na questão
		$this->db->where("que_id", $que_id);

		$this->db->set("que_num_comentarios", $total);

		$this->db->update("questoes");

	 }

	 /**
	  * Retorna se a questão foi ou não marcada como repetida
	  * 
	  * @since L1
	  * 
	  * @param int $questao_id ID da questão
	  *
	  * @return TRUE se a questão é repetida e FALSE caso contrário
	  *
	  */
	  public function is_questao_repetida($questao_id){

		$this->db->where('que_id_repetida', $questao_id);
		$this->db->where('qur_status', STATUS_COMPARACAO_RESOLVIDA);
		$this->db->from('questoes_repetidas');

		return $this->db->count_all_results() > 0;

	  }

	/**
	 * Lista todos os usuário que estão acompanhando uma questão.
	 * 
	 * @since L1
	 * 
	 * @param int $questao_id ID da questão
	 * @param int $comentador_id ID do usuário que comentou a questão. Quando informado ele será excluído da lista de retorno
	 * 
	 * @return array contendo dados básicos dos usuários encontrados
	 */
	  public function listar_usuarios_acompanhando_questao($questao_id, $comentador_id = NULL)
	  {

		$this->db->select('u.ID as user_id, u.display_name as nome, u.user_email as email');
		$this->db->distinct();
		$this->db->from('exponenc_db.wp_users u');
		$this->db->join('questoes_favoritas qfa', 'qfa.usu_id = u.ID');
		$this->db->where('qfa.que_id', $questao_id);
		$this->db->where('qfa.qfa_tipo', QUESTAO_ACOMPANHADA);

		if(!is_null($comentador_id)){
			$this->db->where('qfa.usu_id !=', $comentador_id);
		}

		$query = $this->db->get();

		return $query->result_array();

	  }

	  /**
	   * Altera o texto de uma anotação
	   * 
	   * @since L1
	   * 
	   * @param int $qan_id ID da anotação
	   * @param string $texto_anotacao Novo texto da anotação
	   */
	  public function alterar_questao_anotacao($qan_id, $texto_anotacao){
		  
		$this->db->where('qan_id', $qan_id);
		$this->db->set('qan_anotacao', $texto_anotacao);
		$this->db->set('qan_data_atualizacao', date('Y-m-d H:i:s'));
		$this->db->update('questoes_anotacoes');

	  }

	  /**
	   * Retorna a anotação com id informado
	   * 
	   * @since L1
	   * 
	   * @param int $qan_id ID da anotação
	   * 
	   * @return Array Dados da anotação
	   */
	  public function get_questao_anotacao($qan_id){
		  $this->db->where('qan_id', $qan_id);
		  $query = $this->db->get('questoes_anotacoes');
		  return $query->row_array();
	  }

	  /**
	   * Lista todas as anotações de um usuário para uma determinada questão
	   * 
	   * @since L1
	   * 
	   * @param int $questao_id ID da questão
	   * @param int $usuario_id ID do usuário
	   * 
	   * @return Array contendo os dados das anotações
	   */
	  public function listar_questoes_anotacoes($questao_id, $usuario_id){

		$this->db->where('que_id', $questao_id);
		$this->db->where('usu_id', $usuario_id);
		$this->db->order_by('qan_data_criacao', 'ASC');

		$query = $this->db->get('questoes_anotacoes');

		return $query->result_array();

	  }

	  /**
	   * Remove uma anotação de uma questão
	   * 
	   * @since L1
	   * 
	   * @param int $qan_id ID da anotação
	   */
	  public function remover_questao_anotacao($qan_id){
		  $this->db->where('qan_id', $qan_id);
		  $this->db->delete('questoes_anotacoes');
	  }

	  /**
	   * Salva uma anotação de uma questão para um usuário
	   * 
	   * @since L1
	   * 
	   * @param array $anotacao Anotação a ser salva
	   * 
	   * @return int ID da anotação
	   */
	  public function salvar_questao_anotacao($anotacao){
		
		$anotacao['qan_data_criacao'] = date('Y-m-d H:i:s');
		$anotacao['qan_data_atualizacao'] = date('Y-m-d H:i:s');
		
		$this->db->insert('questoes_anotacoes', $anotacao);

		$qan_id = $this->db->insert_id();

		return $qan_id;
	  }
	
}