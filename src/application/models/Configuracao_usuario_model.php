<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuracao_usuario_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function salvar($configuracoes)
	{			
		$this->db->insert('configuracoes_usuarios', $configuracoes);		
	}

	public function get_config_usuario_by_id($id)
	{		
		$this->db->where('user_id',$id);
		$query = $this->db->get('configuracoes_usuarios');

		$default = array(
			'mostrar_acertos' => CONFIG_USUARIO_MOSTRAR_ACERTOS_ERROS,
			'exibir_filtros' => CONFIG_USUARIO_EXIBIR_FILTROS,
			'manter_historico_marcacao_cadernos' => CONFIG_USUARIO_MANTER_HISTORICO_CADERNO,
			'exibir_url_imprimir_caderno' => CONFIG_USUARIO_ESCONDER_URL_QUESTAO,
			'exibir_assuntos' => CONFIG_USUARIO_EXIBIR_ASSUNTOS,
			'ordem_cabecalho' => CONFIG_USUARIO_ORDERM_CABECALHO_QUESTAO
		);

		if($result = $query->row_array())
		{
			foreach($default as $key => $value)
			{
				if(is_null($result[$key]))
				{
					$result[$key] = $value;
				}
			}

			return $result;
		}

		return $default;
	}

	public function atualizar($configuracoes)
	{			
		$this->db->where('user_id', $configuracoes['user_id']);
		$this->db->update('configuracoes_usuarios', $configuracoes);		
	}
	
}