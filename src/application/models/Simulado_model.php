<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Simulado_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function atualizar_ordem_disciplinas($simulado_id, $disciplinas_ids)
	{
		for($i = 0; $i < count($disciplinas_ids); $i++) {
			$this->db->set('sid_ordem', $i + 1);
			$this->db->where('sim_id', $simulado_id);
			$this->db->where('dis_id', $disciplinas_ids[$i]);
			$this->db->update('simulados_disciplinas');
		}
	}
	
	public function listar_disciplinas_por_ordem($simulado_id)
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->order_by('sid_ordem');
		$query = $this->db->get('simulados_disciplinas');
		
		return $query->result_array();
	}
	
	public function get_ordem_de_disciplina($simulado_id, $disciplina_id)
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('dis_id', $disciplina_id);
		
		$query = $this->db->get('simulados_disciplinas');
		$result = $query->row_array();
		
		return $result['sid_ordem'];
		
	}

	public function contar($somente_ativos = FALSE, $filtro = null){

		$this->db->from('simulados s');
		$this->db->join('cargos c', 's.car_id = c.car_id');

		if($somente_ativos == TRUE){
			$this->db->where('s.sim_status', ATIVO);
		}

		self::filtrar_ordenar($filtro, TRUE);

		return $this->db->count_all_results();
	}

	private function filtrar_ordenar($filtro, $is_contar){
		
		if(!is_null($filtro)){

			if($filtro['filtro_nome']){
				$this->db->like('upper(s.sim_nome)', strtoupper($filtro['filtro_nome']));
			}

			if($filtro['filtro_bancas']){
				$this->db->where_in('s.ban_id', $filtro['filtro_bancas']);
			}

			if($filtro['filtro_instituicoes']){
				$this->db->where_in('s.org_id', $filtro['filtro_instituicoes']);
			}

			if($filtro['filtro_produto']){
				$this->db->where('s.prd_id', $filtro['filtro_produto']);
			}

			if($filtro['filtro_busca']){

				switch($filtro['filtro_busca']){

					case QUE_ADQUIRI:{
						$produtos =  get_produtos_ids_dos_pedidos(get_current_user_id());
						if($produtos){
							$this->db->where_in('s.prd_id', $produtos);
						}else{
							//TODO: ver uma forma melhor de fazer isso
							$this->db->where('1=2');//força vir vazio porque não possui produtos
						}
						break;
					}
					case QUE_NAO_ADQUIRI:{
						$produtos =  get_produtos_ids_dos_pedidos(get_current_user_id());
						if($produtos){
							$this->db->where_not_in('s.prd_id', $produtos);
						}
						$this->db->where('pm.meta_value > 0');
						break;
					}
					case GRATUITOS:{
						$produtos =  get_produtos_ids_dos_pedidos(get_current_user_id());
						if($produtos){
							$this->db->where_not_in('s.prd_id', $produtos);
						}
						$this->db->where('pm.meta_value = 0');
						break;
					}

				}
			}

			if(!$is_contar && $filtro['filtro_ordem']){

				switch($filtro['filtro_ordem']){

					case MAIS_NOVOS:{
						$this->db->order_by('s.sim_id', 'desc');
						break;
					}

					case MAIS_ANTIGOS:{
						$this->db->order_by('s.sim_id', 'asc');
						break;	
					}

					case MENOR_PRECO:{
						$this->db->order_by('pm.meta_value', 'asc');
						break;	
					}

					case MAIOR_PRECO:{
						$this->db->order_by('pm.meta_value', 'desc');
						break;	
					}

					case NOME_ASC:{
						$this->db->order_by('s.sim_nome', 'asc');
						break;
					}

					default:{
						$this->db->order_by('s.sim_id', 'desc');
						break;
					}

				}

			}

			/*Realiza os joins de acordo com a necessidade, sem duplicação*/
			//left wp_post_meta pm
			//TODO: Deve passar a utilizar a tabela exponenc_db.produtos
			if(($filtro['filtro_busca'] && ($filtro['filtro_busca'] == QUE_NAO_ADQUIRI || $filtro['filtro_busca'] == GRATUITOS))
				|| (!$is_contar && $filtro['filtro_ordem'] && ($filtro['filtro_ordem'] == MAIOR_PRECO || $filtro['filtro_ordem'] == MENOR_PRECO) )
				){
				$this->db->join(DB_NAME . ".wp_postmeta pm", "pm.post_id = s.prd_id and pm.meta_key = '_price'", "left");
			}

		}elseif(!$is_contar){
			$this->db->order_by('s.sim_id', 'desc');
		}

	}

	public function listar($offset = null, $limite = SIMULADOS_POR_PAGINA, $somente_ativos = FALSE, $filtro = null)
	{
		$this->db->from('simulados s');
		$this->db->join('cargos c', 's.car_id = c.car_id');

		if($somente_ativos == TRUE){
			$this->db->where('s.sim_status', ATIVO);
		}
				
		self::filtrar_ordenar($filtro, FALSE);

		if($offset >= 0){
			$this->db->limit($limite, $offset);
		}
		$query = $this->db->get();

		return $query->result_array();
	}

	public function contar_por_filtro($filtro){
		return self::buscar_por_filtro(FALSE, TRUE, $filtro);
	}

	public function listar_por_filtro($filtro, $offset = 0){
		return self::buscar_por_filtro(TRUE, FALSE, $filtro, $offset);
	}

	public function listar_por_filtro_com_total($filtro, $offset = 0){
		return self::buscar_por_filtro(TRUE, TRUE, $filtro, $offset);
	}

	private function buscar_por_filtro($is_result, $is_contar, $filtro, $offset = 0)
	{
		if($filtro) {
			if($ban_ids = $filtro['ban_ids']) {
			$this->db->where_in('ban_id', $ban_ids);
			}

			if($org_ids = $filtro['org_ids']) {
				$this->db->where_in('org_id', $org_ids);
			}

			$dis_ids = $filtro['dis_ids'];
			$prof_ids = $filtro['prof_ids'];

			if($dis_ids && $prof_ids){

				$dis_frag = implode(', ', $dis_ids);
				$prof_frag = implode(', ', $prof_ids);

				$this->db->where("sim_id IN (SELECT sd.sim_id FROM simulados_disciplinas sd 
												INNER JOIN simulados_revisores sr ON (sd.sim_id = sr.sim_id AND sd.dis_id = sr.dis_id) 
											WHERE sd.dis_id IN ({$dis_frag}) AND sr.usu_id IN ({$prof_frag}))");

			}elseif($dis_ids) {
					$dis_frag_a = [];

					foreach ($dis_ids as $item) {
						array_push($dis_frag_a, "dis_id = {$item}");
					}

					$dis_frag = implode(' OR ', $dis_frag_a);

					$this->db->where("sim_id IN (SELECT sim_id FROM simulados_disciplinas WHERE {$dis_frag})");

			}elseif($prof_ids) {
					
				$prof_frag_a = [];

				foreach ($prof_ids as $item) {
					array_push($prof_frag_a, "usu_id = {$item}");
				}

				$prof_frag = implode(' OR ', $prof_frag_a);

				$this->db->where("sim_id IN (SELECT sim_id FROM simulados_revisores WHERE {$prof_frag})");
				
			}

			if($texto = $filtro['search_filter']) {
				$this->db->like('sim_nome', $texto);
			}

			if($status = $filtro['check_simulado']) {

				if($status != TODAS) {
					$this->db->where('sim_status', $status);	
				}
				$is_dis_inconsistente = ($status == INCONSISTENTE && !empty($dis_ids));
			}

			if($prazos = $filtro['check_prazo']){

				$prazo_sql = "";

				foreach($prazos as $prazo){

					if(!$prazo_sql){
						$prazo_sql .= "(";
					}else{
						$prazo_sql .= " or ";
					}
					
					switch($prazo){
						case PRAZO_SIMULADO_ATRASADOS:
							$prazo_sql .= " sim_data_entrega < '" . hoje_yyyymmdd() . " 00:00:00' ";
							break;				
						case PRAZO_SIMULADO_FUTURO:
							$futuro = date('Y-m-d', strtotime(hoje_yyyymmdd() . ' +15 days'));
							$prazo_sql .= " sim_data_entrega > '" . $futuro . " 23:59:59' ";
							break;
						case PRAZO_SIMULADO_PRESENTE:
							$futuro = date('Y-m-d', strtotime(hoje_yyyymmdd() . ' +15 days'));
							$prazo_sql .= " (sim_data_entrega >= '" . hoje_yyyymmdd() . " 00:00:00' ";
							$prazo_sql .= " and sim_data_entrega <= '" . $futuro . " 23:59:59') ";
							break;	
						case PRAZO_SIMULADO_SEM_PREVISAO:
							$prazo_sql .= " sim_data_entrega is null ";
							break;
					}
				}

				if($prazo_sql){
					$prazo_sql .= ")";
					$this->db->where($prazo_sql);
				}

			}

		}
		
		$this->db->from('simulados s');
		$this->db->join('cargos c', 's.car_id = c.car_id');

		/* [JOULE][J1] UPGRADE - Controle de prioridade dos simulados
	 	 * https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/340
		 *
		 * Permite a escolha da ordenação a ser utilizada
	 	 */
		if($filtro['ordenacao']) {
			switch ($filtro['ordenacao']) {
				case PRIORIDADE:
					$order_by = 's.sim_prioridade asc, s.sim_data_inconsistencia desc, s.sim_id desc';
					break;				
				case MAIS_NOVOS:
					$order_by = 's.sim_id desc';
					break;
				case MAIS_ANTIGOS:
					$order_by = 's.sim_id asc';
					break;
			}
		}
		else {
			$order_by = 's.sim_id desc';
		}

		$total = 0;

		//Tem filtro pós-query
		if($is_dis_inconsistente || ($filtro['check_situacao'] && $filtro['check_situacao'] != TODAS)){

			$this->db->order_by($order_by);
			$query = $this->db->get();

		}else{

			if($is_result){
				$this->db->order_by($order_by);
				$this->db->limit(SIMULADOS_POR_PAGINA, $offset);
				$query = $this->db->get();
			}

			if($is_contar && !$is_result){
				$total = $this->db->count_all_results();
			}else if($is_contar){
				$total = self::contar_por_filtro($filtro);
			}

			if($is_contar && !$is_result){
				return $total;
			}else if(!$is_contar && $is_result){
				return $query->result_array();	
			}

			$result = $query->result_array();

			array_push($result, $total);

			return $result;
		}
		
		if($resultado = $query->result_array()) {
			
			/*
			 * Vide: https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2008
			 */
			if($is_dis_inconsistente) {
				$simulados = [];
				foreach ($resultado as $item) {
					if(pode_revisar_simulado($item['sim_id'])) {
						foreach ($dis_ids as $dis_id) {
							array_push($simulados, $item);
							break;

						}
					}
				}

				if($is_contar && !$is_result){
					return count($simulados);
				}else if(!$is_contar && $is_result){
					return array_slice($simulados, $offset, SIMULADOS_POR_PAGINA);	
				}

				$result = array_slice($simulados, $offset, SIMULADOS_POR_PAGINA);

				array_push($result, count($simulados));

				return $result;

			}else if($filtro['check_situacao'] && $filtro['check_situacao'] != TODAS){
				
				$simulados = [];
				
				foreach ($resultado as $item) {

					$disciplinas = self::listar_disciplinas_por_ordem($item['sim_id']);

					$situacao = REVISADO;
					
					if(pode_revisar_simulado($item['sim_id'])) {
						
						/*Podio#2008: Se houver disciplina selecionada e busco apenas as pendentes 
						então só tenho interesse em verificar as disciplinas que informei*/
						$is_dis_pendente = ($filtro['check_situacao'] == PENDENTE && !empty($dis_ids));
						
						foreach ($disciplinas as $disciplina) {

							if($is_dis_pendente && !in_array($disciplina['dis_id'], $dis_ids)){
								continue;
							}
							
							if(!is_disciplina_simulado_completa($item['sim_id'], $disciplina['dis_id'])) {
								$situacao = PENDENTE;
								break;
							}

						}
						
					}
					
					if($situacao == $filtro['check_situacao']) {
						array_push($simulados, $item);
					}

				}
				
				if($is_contar && !$is_result){
					return count($simulados);
				}else if(!$is_contar && $is_result){
					return array_slice($simulados, $offset, SIMULADOS_POR_PAGINA);	
				}

				$result = array_slice($simulados, $offset, SIMULADOS_POR_PAGINA);

				array_push($result, count($simulados));
				
				return $result;

			}else {
				return $resultado;
			}
		}
	}

	public function listar_simulados_ranking($usuario_id, $offset = 0, $limit = LIMIT_MEUS_RESULTADOS_SIMULADOS, $titulo = null, $inicio = null, $fim = null)
	{

		if($titulo) {
			$this->db->like("s.sim_nome", $titulo);
		}

		if($inicio) {
			$this->db->where("su.sim_usu_data_resolucao >=", $inicio . " 00:00:00");
		}

		if($fim) {
			$this->db->where("su.sim_usu_data_resolucao <=", $fim . " 23:59:59");
		}

		if($limit) {
			$this->db->limit($limit, $offset);	
		}

		$this->db->from('simulados s');
		$this->db->join('simulados_usuarios su', 's.sim_id = su.sim_id');
		$this->db->where('su.usu_id', $usuario_id);

		$query = $this->db->get();

		return $query->result_array();
	}
		
	public function listar_todas($usuario_id = null, $busca = BUSCA_ADMIN)
	{
		if(is_null($usuario_id))
			$usuario_id = get_id_usuario_atual();
			$this->db->select('(select sum(sid_qtde_questoes) from simulados_disciplinas where sim_id=s.sim_id) as total_questoes, (select count(pro_id) from provas where org_id=o.org_id) as total_provas, count(case when qr.qre_selecao = 1 then 1 end) as acertos, count(qr.que_id) as resolvidas, o.*, p.*, c.*, su.*, s.*');
			$this->db->from('simulados as s');
			$this->db->join('simulados_disciplinas as sd', 'sd.sim_id=s.sim_id', 'left');
			$this->db->join('orgaos as o', 'o.org_id=s.org_id', 'left');
			$this->db->join('provas as p', 'p.org_id=o.org_id', 'left');
			$this->db->join('cargos as c', 'c.car_id=s.car_id', 'left');
			$this->db->join('simulados_questoes as sq', 'sq.sim_id=s.sim_id', 'left');
		if($busca == BUSCA_ADMIN) {
			$this->db->join('simulados_usuarios as su', 'su.sim_id=s.sim_id', 'left');
			$this->db->join('questoes_resolvidas as qr', 'qr.que_id=sq.que_id', 'left');
		} elseif($busca == BUSCA_EDITAVEIS){
			$this->db->join('simulados_usuarios as su', 'su.sim_id=s.sim_id', 'left');
			$this->db->join('questoes_resolvidas as qr', 'qr.que_id=sq.que_id', 'left');
// 			$this->db->where('qr.que_id', null);
		} elseif($busca == BUSCA_ALUNO){
			$this->db->join('simulados_usuarios as su', 'su.sim_id=s.sim_id and su.usu_id=' . $usuario_id, 'left');
			$this->db->join('questoes_resolvidas as qr', 'qr.que_id=sq.que_id and qr.usu_id=' . $usuario_id, 'left');
		} elseif ($busca == BUSCA_DESEMPENHO) {
			$this->db->join('simulados_usuarios as su', 'su.sim_id=s.sim_id and su.usu_id=' . $usuario_id, 'left');
			$this->db->join('questoes_resolvidas as qr', 'qr.que_id=sq.que_id and qr.usu_id=' . $usuario_id, 'left');
// 			$this->db->where('su.sim_usu_resultado is not null', null);
		}
	
		$this->db->join('questoes', 'q.que_id = qr.que_id');
		$this->db->group_by('s.sim_id');
		$this->db->order_by('s.sim_id', 'desc');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_by_id($simulado_id)
	{
		$query = $this->db->get_where('simulados', array('sim_id' => $simulado_id));
		return $query->row_array();
	}

	public function salvar($simulado)
	{
		$this->db->insert('simulados', $simulado);
		return $this->db->insert_id();
	}

	public function get_by_nome($nome)
	{
		$query = $this->db->get_where('simulados', array('sim_nome' => $nome));
		return $query->row_array();
	}

	public function atualizar_dados_basicos($simulado)
	{
		$this->db->where('sim_id', $simulado['sim_id']);
		$this->db->update('simulados', $simulado);
	}
	
	public function atualizar($simulado)
	{
		$exclusoes = $simulado['exclusoes'];
		$disciplinas_selecionadas = $simulado ['disciplinas_selecionadas'];
		$assuntos_selecionados = $simulado ['assuntos_selecionados'];
		
		if($simulado['sim_sortear_questoes']) {
			self::sortear_questoes($simulado);
		}else{
			$simulado['sim_sortear_questoes'] = '0';
		}

		if(empty($simulado['sim_revisao_ativada'])){
			$simulado['sim_revisao_ativada'] = '0';
		}
		
		unset($simulado['exclusoes']);
		unset($simulado['disciplinas_selecionadas']);
		unset($simulado['assuntos_selecionados']);
		
		$this->db->where('sim_id', $simulado['sim_id']);
		$this->db->set($simulado);
		$this->db->update('simulados');

		self::atualizar_exclusoes_simulado($simulado['sim_id'], $exclusoes);
		self::atualizar_disciplinas_simulado($simulado['sim_id'], $disciplinas_selecionadas);
		self::atualizar_assuntos_simulado($simulado['sim_id'], $assuntos_selecionados);
		
		// Se simulado estiver ativo e não for consistente muda status para inconsistente
		if(($simulado['sim_status'] == ATIVO) && (!self::is_simulado_consistente($simulado['sim_id']))) {
			/**
			 * EC-116, Podio 2905: Foi solicitado deixar o simulado inconsistente somente manualmente
			 */
			//self::atualizar_status($simulado['sim_id'], INCONSISTENTE);
		}
		
		return $simulado['sim_id'];
	}

	public function atualizar_dados_produto($simulado_id, $produto_id, $descricao){

		$this->db->where('sim_id', $simulado_id);
		$this->db->set('prd_id', $produto_id);
		$this->db->set('sim_descricao', $descricao);
		$this->db->update('simulados');

	}
	
	public function verificar_consistencia(&$simulado)
	{
		foreach($simulado['disciplinas_selecionadas'] as $disciplina_id => $disciplina_qtde) {
			$total_por_disciplina = $this->questao_model->contar_por_disciplina($disciplina_id);
				
			if($total_por_disciplina < $disciplina_id) {
				$simulado['sim_status'] = INCONSISTENTE;
			}
		}
	}
	
	public function sortear_questoes(&$simulado)
	{
		$questoes_ids_associadas = self::listar_questoes_ids_simulados_associados($simulado_id);
		
		$ordem = 1;
		self::excluir_todas_questoes_de_simulado($simulado['sim_id']);
		foreach($simulado['disciplinas_selecionadas'] as $disciplina_id => $disciplina_qtde) {
			set_time_limit(30);
			
			$assuntos_ids_por_disciplina = $this->assunto_model->listar_ids_por_disciplina($disciplina_id);
			$assuntos = array_intersect($assuntos_ids_por_disciplina, $simulado['assuntos_selecionados']);
			if(count($assuntos) > 0) {

				$this->db->select('q.que_id');
				$this->db->from('questoes q');
				$this->db->join('provas p', 'q.pro_id=p.pro_id', 'left');
				$this->db->join('orgaos o', 'o.org_id=p.org_id', 'left');
				$this->db->join('bancas b', 'b.ban_id=p.ban_id', 'left');
				$this->db->join('disciplinas d', 'd.dis_id=q.dis_id', 'left');
				$this->db->join('questoes_assuntos as qa', 'qa.que_id=q.que_id', 'left');
				
				$filtro = unserialize($_SESSION['simulado_filtro']);

				if($filtro) {
					self::adicionar_filtros($filtro);
				}
				
				foreach($filtro['sim_excluir_questoes'] as $item){

					switch ($item) {
			
						case EXCLUIR_QUESTOES_INATIVAS: {
							//echo 'excluir_inativas';
							$this->db->where('q.que_ativo', SIM);
							break;
						}
						case EXCLUIR_QUESTOES_SEM_COMENTARIO_PROFESSOR: {
							//VER TAMBÉM O FILTRO 'filtro_comentarios'
							$this->db->join("comentarios c", "c.que_id = q.que_id");
							$this->db->where('c.com_destaque', 1);
							break;
						}
						case EXCLUIR_QUESTOES_DE_OUTROS_SIMULADOS:{
							$outros_sql = "not exists(select 1 from simulados_questoes sim_quest where sim_quest.que_id = q.que_id";
							
							$outros_sql .= " and sim_quest.sim_id != ".$simulado['sim_id'];
							
							$outros_sql .= ")";
							$this->db->where($outros_sql, null, false);
							break;
						}
			
					}

				}

				$this->db->where_in('qa.ass_id', $assuntos);
				
				$this->db->where_not_in('q.que_id', $questoes_ids_associadas);
				$this->db->order_by('q.que_id', 'random');
				$this->db->limit($disciplina_qtde);
				$this->db->group_by('q.que_id');
				
				$query = $this->db->get();
				
				$questoes_sorteadas = $query->result_array();
				self::salvar_questoes_em_simulado($simulado['sim_id'], $questoes_sorteadas, $ordem);
			}
		}
		
	}

	public function listar_por_produto($produto_id)
	{
		$this->db->where('prd_id', $produto_id);
		$query = $this->db->get('simulados');

		return $query->result_array();
	}	
	
	/**
	 * Atualiza o status de um simulado. 
	 * 
	 * OBS.: Ao alterar ess método @see kcore\models\SimuladoModel::tornar_simulado_incosistente
	 *
	 * @since 10.0.0
	 * 
	 * @param int $simulado_id Id do simulado
	 * @param int $status Novo status do simulado. Valores possíveis: ATIVO, INATIVO, INCONSISTENTE.
	 */

	public function atualizar_status($simulado_id, $status)
	{
		/** 	
		 * [JOULE] UPGRADE - Controle de prioridade dos simulados
		 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/340
		 *
		 * [JOULE] Simulado recém inconsistente deve ficar na primeira posição de prioridade
		 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2084
		 */
		if($status == INCONSISTENTE) {
			self::shift_prioridades();

			$this->db->where('sim_id', $simulado_id);
			$this->db->set('sim_status', $status);
			$this->db->set('sim_data_inconsistencia', hoje_yyyymmdd());
			$this->db->set('sim_prioridade', 1);
			$this->db->update('simulados');
		}
		else {
			$this->db->where('sim_id', $simulado_id);
			$this->db->set('sim_status', $status);
			$this->db->update('simulados');
		}
		
	}

	/**
	 * Modifica a prioridade de todos os simulados 
	 * 
	 * [JOULE] Simulado recém inconsistente deve ficar na primeira posição de prioridade
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2084
	 * 
	 * @since 10.1.0
	 *
	 * @param int $valor Valor a ser somado às prioridades atuais
	 */

	public function shift_prioridades($valor = 1)
	{
		$simulados = self::listar();

		foreach ($simulados as $simulado) {
			// Considera apenas simulados com prioridade definida			
			if($simulado['sim_prioridade']) {
				$nova_prioridade = $simulado['sim_prioridade'] + $valor;
				self::atualizar_prioridade($simulado['sim_id'], $nova_prioridade);
			}
		}
	}
	
	public function atualizar_status_revisao($simulado_id, $status)
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->set('sim_revisao_ativada', $status);
		$this->db->update('simulados');
	}

	/*************************************************************************
	 * Verifica se um simulado está inconsistente ou não
	 * @param int $simulado_id
	 *************************************************************************/
	public function is_simulado_consistente($simulado_id) {
		$this->load->model('Questao_model');
		
		$disciplinas = self::listar_disciplinas_por_ordem($simulado_id);
		foreach ($disciplinas as $disciplina) {
			$questoes = $this->Questao_model->listar_por_simulado_disciplina($simulado_id, $disciplina['dis_id']);
				
			if($disciplina['sid_qtde_questoes'] != count($questoes)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * @deprecated
	 */
	public function checar_consistencia($simulado_id) 
	{
		$this->load->model('Questao_model');
		
		$disciplinas = self::listar_disciplinas_por_ordem($simulado_id);
		foreach ($disciplinas as $disciplina) {
			$questoes = $this->Questao_model->listar_por_simulado_disciplina($simulado_id, $disciplina['dis_id']);
			
			if($disciplina['sid_qtde_questoes'] != count($questoes)) {
				$simulado = self::get_by_id($simulado_id);

				/********************************************************************
				 * Ajuste da notificação de alteração de simulado
				 * https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1391
				 * Adicionando: https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2203
				 ********************************************************************/

				$estava_ativo_txt = "";

				if( $simulado['sim_status'] == ATIVO ){
					// Verifica se o simulado já se encontrava ativo antes
					$estava_ativo_txt = "HÁ ALUNOS COM ACESSO A ESSE SIMULADO. ";

					// Atualizar o status do simulado para INCONSISTENTE
					self::atualizar_status($simulado_id, INCONSISTENTE);
				}

				if((contar_usuarios_que_compraram($simulado['prd_id']) > 0) || (self::contar_usuarios_designados($simulado_id) > 0)) {
					$usuario = get_usuario_array(get_current_user_id());

					$titulo = "Simulado Inconsistente - {$simulado['sim_nome']}";
					$mensagem = "O simulado {$simulado['sim_nome']} se encontra inconsistente. {$estava_ativo_txt}Última alteração feita por {$usuario['nome_completo']}.";
				
					
					$coodenadores_sq = listar_coordenadores_sq_ativos();
					$adm_email = new stdClass;
					$adm_email->user_email = 'leonardo.coelho@exponencialconcursos.com.br';
					array_unshift($coodenadores_sq, $adm_email);

					foreach ($coodenadores_sq as $coodenador_sq)
					{
						//enviar_email($coodenador_sq->user_email, $titulo, $mensagem, null, null, null, true);
					}
					
				}				
				return;
			}
		}
		
		
	}
	
	public function salvar_questoes_em_simulado($simulado_id, $questoes, &$ordem)
	{
		$data = array();
		
		foreach ($questoes as $item)
		{
			if(!self::is_questao_adicionada($item['que_id'], $simulado_id)) {
				array_push($data, array(
						'que_id' 	=> $item['que_id'],
						'sim_id' 	=> $simulado_id,
						'siq_ordem'	=> $ordem++
				));
			}
		}	
		if(count($data) > 0)
			$this->db->insert_batch('simulados_questoes', $data);
	}
	
	public function is_questao_adicionada($questao_id, $simulado_id) {
		$query = $this->db->get_where('simulados_questoes', array('sim_id' => $simulado_id, 'que_id' => $questao_id));
		if(count($query->row_array()) > 0)
			return true;
		
		return false;
	}
	
	public function is_questao_associada_simulado_ativo($questao_id) {
		$this->db->from('simulados_questoes sq');
		$this->db->join('simulados s', 's.sim_id = sq.sim_id');
		$this->db->where('sq.que_id', $questao_id);
		$this->db->where_in('s.sim_status', array(ATIVO, INCONSISTENTE));
		$query = $this->db->get();
		
		if(count($query->row_array()) > 0)
			return true;
		return false;
	}
	
	public function get_proxima_ordem_questao($simulado_id) {
		$this->db->select_max('siq_ordem');
		$this->db->where('sim_id', $simulado_id);
		$result = $this->db->get('simulados_questoes');
		return $result->row()->siq_ordem + 1;
	}
	
	public function excluir_todas_questoes_de_simulado($simulado_id)
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->delete('simulados_questoes');
	}
	
	public function excluir_questao_de_simulado($simulado_id, $questao_id)
	{
		$this->db->where('que_id', $questao_id);
		$this->db->where('sim_id', $simulado_id);
		$this->db->delete('simulados_questoes');
	}
	
	public function adicionar_questao_ao_simulado($simulado_id, $questao_id)
	{
		$this->db->set('que_id', $questao_id);
		$this->db->set('sim_id', $simulado_id);
		$this->db->set('siq_ordem', 0);
		$this->db->insert('simulados_questoes');
	}
	
	public function get_simulados_questao($questao_id) {
		$this->db->select('sim_id');
		$this->db->where('que_id', $questao_id);
		$result = $this->db->get('simulados_questoes');
		return $result->result_array();
	}
	
	public function get_questoes_simulado($simulado_id) {
		$this->db->where('sim_id', $simulado_id);
		$result = $this->db->get('simulados_questoes');
		return $result->result_array();
	}

	/**
	 * Retorna a quantidade de questões de um simulado
	 * 
	 * @since K5
	 * 
	 * @param int $simulado_id Id do simulado
	 * 
	 * @return int Número de questões de um simulado
	 */
	public function contar_questoes_simulado($simulado_id, $tipo_resultado = NULL){
		$this->db->where('sq.sim_id', $simulado_id);
		$this->db->from('simulados_questoes sq');

		if(!is_null($tipo_resultado))
		{

			$this->db->join('questoes q', 'q.que_id = sq.que_id');

			switch ($tipo_resultado) {
				case SIMULADO_QUESTAO_NAO_RESOLVIDAS: {
					$this->db->where('not exists(select 1 from questoes_resolvidas qr where qr.que_id = sq.que_id and qr.sim_id = sq.sim_id and qr.qre_selecao is not null and qr.usu_id = '. get_current_user_id().')');
					break;
				}

				case SIMULADO_QUESTAO_RESOLVIDAS: {
					$this->db->where('exists(select 1 from questoes_resolvidas qr where qr.que_id = sq.que_id and qr.sim_id = sq.sim_id and qr.qre_selecao is not null and qr.usu_id = '. get_current_user_id().')');
					break;
				}

				case SIMULADO_QUESTAO_QUE_ACERTEI: {
					$this->db->where('exists(select 1 from questoes_resolvidas qr where qr.que_id = sq.que_id and qr.sim_id = sq.sim_id and qr.qre_selecao = q.que_resposta and qr.usu_id = '. get_current_user_id().')');
					break;
				}

				case SIMULADO_QUESTAO_QUE_ERREI: {
					$this->db->where('exists(select 1 from questoes_resolvidas qr where qr.que_id = sq.que_id and qr.sim_id = sq.sim_id and qr.qre_selecao != q.que_resposta and qr.usu_id = '. get_current_user_id().')');
					break;
				}

			}
		}

		return $this->db->count_all_results();
	}
	
	public function salvar_simulado_questao($questao){
		return $this->db->insert('simulados_questoes', $questao);
	}
	
	public function mudar_status($simulado, $status) {
		$this->db->where('sim_id', $simulado['sim_id']);
		$this->db->set("sim_status", $status);
		$this->db->update('simulados');
		return $status;
	}
	
	public function excluir($simulado_id)
	{
		$this->db->where('sim_id', $simulado_id);
		return $this->db->delete('simulados');
	}
	
	public function has_simulado_img($simulado_id) {
		$result = glob("uploads/simulados/img_simulado_{$simulado_id}.*");
		if(count($result) > 0)
			return true;
	
		return false;
	}
	
	public function copia_imagem_simulado($simulado_origem_id, $simulado_destino_id)
	{
		if(self::has_simulado_img($simulado_origem_id)) {
			$result = glob("uploads/simulados/img_simulado_{$simulado_origem_id}.*");
			$origem = $_SERVER['DOCUMENT_ROOT'] . '/questoes/' . $result[0];
			$destino = str_replace('_' . $simulado_origem_id . '.', '_' . $simulado_destino_id . '.', $origem); 
			
			copy($origem, $destino);
		}
	}

	public function has_simulado_pdf($simulado_id) {
		$result = glob("uploads/simulados/pdf_simulado_{$simulado_id}.*");
		if(count($result) > 0)
			return true;
	
		return false;
	}
	
	public function copia_pdf_simulado($simulado_origem_id, $simulado_destino_id)
	{
		if(self::has_simulado_img($simulado_origem_id)) {
			$result = glob("uploads/simulados/pdf_simulado_{$simulado_origem_id}.*");
			$origem = $_SERVER['DOCUMENT_ROOT'] . '/questoes/' . $result[0];
			$destino = str_replace('_' . $simulado_origem_id . '.', '_' . $simulado_destino_id . '.', $origem); 
			
			copy($origem, $destino);
		}
	}
	
	public function get_exclusoes_simulado($simulado_id, $campo = null) {
		$this->db->from('simulados_exclusoes');
		$this->db->where('sim_id', $simulado_id);
		if(!is_null($campo))
			$this->db->where('sie_campo', $campo);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function salvar_simulado_exclusao($exclusao){		
		return $this->db->insert('simulados_exclusoes', $exclusao);
	}
	
	public function remover_exclusoes_simulado($simulado_id, $campo = null, $ignorar_campo = null)
	{
		$this->db->where('sim_id', $simulado_id);

		if(!is_null($campo))
		{
			$this->db->where('sie_campo', $campo);
		}

		if(!is_null($ignorar_campo))
		{
			$this->db->where('sie_campo !=', $ignorar_campo);
		}

		$this->db->delete('simulados_exclusoes');
	}
	
	public function atualizar_exclusoes_simulado($simulado_id, $campos_exclusoes_selecionadas, $ignorar_campo = null) {
		
		self::remover_exclusoes_simulado($simulado_id, null, $ignorar_campo);
		
		$dados = array();
		foreach ($campos_exclusoes_selecionadas as $key => $values) {
			foreach ($values as $value) {
				array_push($dados, array(
					'sie_campo' => $key,
					'sie_valor' => $value,
					'sim_id' =>$simulado_id
				));
			}
		}
		
		if($dados) {
			$this->db->insert_batch('simulados_exclusoes', $dados);
		}
	}
	
	public function get_disciplinas_simulado($simulado_id, $disciplina_id = null) {
		$this->db->select('d.*, sd.sid_qtde_questoes as qtde, sd.sid_ordem');
		$this->db->from('disciplinas d');
		$this->db->where('sd.sim_id', $simulado_id);
		if(!is_null($disciplina_id))
			$this->db->where('sd.dis_id', $disciplina_id);
		$this->db->join('simulados_disciplinas sd','sd.dis_id=d.dis_id', 'left');
		$this->db->order_by('sd.sid_ordem','ASC');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_dados_simulados_disciplinas($simulado_id) {
		$this->db->from('simulados_disciplinas');
		$this->db->where('sim_id', $simulado_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function salvar_simulado_disciplina($disciplina)
	{
		return $this->db->insert('simulados_disciplinas', $disciplina);
	}
	
	public function listar_disciplinas_simulado($simulado_id, $order_by = 'd.dis_nome', $order = 'asc')
	{
		$this->db->select('d.*, sd.sid_qtde_questoes as qtde');
		$this->db->from('disciplinas d');
		$this->db->where('sd.sim_id', $simulado_id);
		$this->db->order_by($order_by, $order);
		$this->db->join('simulados_disciplinas sd','sd.dis_id=d.dis_id', 'left');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function get_maximo_questoes_disciplina($simulado_id, $disciplina_id) 
	{
		$this->db->select('sid_qtde_questoes');
		$this->db->from('simulados_disciplinas');
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('dis_id', $disciplina_id);
		$query = $this->db->get();
		
		$row = $query->row_array();
		return $row['sid_qtde_questoes'];
	}

	/**
	 * Conta o número de questões de uma disciplina de um simulado
	 *
	 * @since J5
	 *
	 * @param int $simulado_id Id do simulado
	 * @param int $disciplina Id da disciplina
	 * 
	 * @return int
	 */
	
	public function get_atual_questoes_disciplina($simulado_id, $disciplina_id) 
	{
		$this->db->from('simulados_questoes sq');
		$this->db->join('questoes q', 'sq.que_id = q.que_id');
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('dis_id', $disciplina_id);
		return $this->db->count_all_results();
	}

	/**
	 * Lista todas as questões de uma disciplina de um simulado
	 *
	 * @since K1
	 *
	 * @param int $simulado_id Id do simulado
	 * @param int $disciplina Id da disciplina
	 * 
	 * @return array
	 */

	public function listar_questoes_disciplina($simulado_id, $disciplina_id) 
	{
		$this->db->from('simulados_questoes sq');
		$this->db->join('questoes q', 'sq.que_id = q.que_id');
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('dis_id', $disciplina_id);
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function atualizar_disciplinas_simulado($simulado_id, $disciplinas_selecionadas) 
	{
		$disciplinas_salvas = self::get_disciplinas_simulado($simulado_id);
		if (count( $disciplinas_salvas ) > 0) {
			$ids_disciplinas = get_coluna($disciplinas_salvas, 'dis_id');
			$qtdes_disciplinas = get_coluna($disciplinas_salvas, 'sid_qtde_questoes');

			$count = 0;
			foreach ( $disciplinas_selecionadas as $disciplina_selecionada => $qtd ) {
				if (! in_array ( $disciplina_selecionada, $ids_disciplinas )) {
					$this->db->set ( 'sim_id', $simulado_id );
					$this->db->set ( 'dis_id', $disciplina_selecionada );
					$this->db->set ( 'sid_qtde_questoes', $qtd );
					$this->db->insert ( 'simulados_disciplinas' );
				} else {
						$this->db->where('sim_id', $simulado_id);
						$this->db->where('dis_id', $disciplina_selecionada);
						$this->db->update('simulados_disciplinas', array('sid_qtde_questoes' => $qtd));
				}
			}
			
			$ids_selecionados = array_keys($disciplinas_selecionadas);
			foreach ( $ids_disciplinas as $i => $id_disciplina ) {
				if (! in_array ( $id_disciplina, $ids_selecionados )) {
					$this->db->where ( 'sim_id', $simulado_id );
					$this->db->where ( 'dis_id', $id_disciplina );
					$this->db->delete ( 'simulados_disciplinas' );
				}
			}
		} else {
			foreach ( $disciplinas_selecionadas as $disciplina_selecionada => $qtd ) {
				$this->db->set ( 'sim_id', $simulado_id );
				$this->db->set ( 'dis_id', $disciplina_selecionada );
				$this->db->set ( 'sid_qtde_questoes', $qtd );
				$this->db->insert ( 'simulados_disciplinas' );
			}
		}

		self::remover_questoes_disciplinas_excluidas($simulado_id);
	}

	public function remover_disciplina($simulado_id, $disciplina_id) 
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('dis_id', $disciplina_id);
		$this->db->delete('simulados_disciplinas');
	}

	public function remover_revisores_disciplina($simulado_id, $disciplina_id) 
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('dis_id', $disciplina_id);
		$this->db->delete('simulados_revisores');
	}

	public function remover_questoes_disciplinas($simulado_id, $disciplina_id)
	{
		if($disciplinas = self::listar_disciplinas_simulado($simulado_id)) {
			foreach ($disciplinas as $disciplina) {
				array_push($ids, $disciplina['dis_id']);
			}
		}

		$this->db->from('simulados_questoes sq');
		$this->db->join('questoes q', 'q.que_id = sq.que_id');
		$this->db->where('sq.sim_id', $simulado_id);
		$this->db->where('q.dis_id', $disciplina_id);
		$query = $this->db->get();

		if($resultado = $query->result_array()) {

			foreach ($resultado as $questao) {
				$this->db->where('sim_id', $simulado_id);
				$this->db->where('que_id', $questao['que_id']);
				$this->db->delete('simulados_questoes');
			}
		}
	}

	public function remover_questoes_disciplinas_excluidas($simulado_id)
	{
		$ids = array();
		if($disciplinas = self::listar_disciplinas_simulado($simulado_id)) {
			foreach ($disciplinas as $disciplina) {
				array_push($ids, $disciplina['dis_id']);
			}
		}

		$this->db->from('simulados_questoes sq');
		$this->db->join('questoes q', 'q.que_id = sq.que_id');
		$this->db->where('sq.sim_id', $simulado_id);
		if($ids){
			$this->db->where_not_in('q.dis_id', $ids);
		}
		$query = $this->db->get();

		if($resultado = $query->result_array()) {

			echo "Processando simulado {$simulado_id}. " . count($resultado) . " questoes para excluir.<br>";

			foreach ($resultado as $questao) {
				echo "Excluindo {$questao['que_id']}.<br>";

				$this->db->where('sim_id', $simulado_id);
				$this->db->where('que_id', $questao['que_id']);
				$this->db->delete('simulados_questoes');
			}
		}
	}
	
	public function listar_questoes_ignoradas($simulado_id)
	{
		$questoes_ids = array();
		$siqs = $this->db->get_where('simulados_questoes', array('sim_id' => $simulado_id));
		
		foreach ($siqs->result_array() as $item) {
			array_push($questoes_ids, $item['que_id']);
		}
		
		// ignora questões dos simulados associados
		$questoes_simulados_associados = self::listar_questoes_ids_simulados_associados($simulado_id);
		
		if($questoes_simulados_associados) {
			$questoes_ids = array_merge($questoes_ids, $questoes_simulados_associados);
		}
				
		return $questoes_ids;
	}

	public function atualizar_assuntos_simulado($simulado_id, $assuntos_selecionados) {
		$assuntos_salvos = self::get_assuntos_simulado($simulado_id);
		if (count( $assuntos_salvos ) > 0) {
			$assuntos_ids = get_assuntos_ids($assuntos_salvos);
			if(count($assuntos_selecionados) > 0) {
				foreach ( $assuntos_selecionados as $assunto_selecionado ) {
					if (!in_array ( $assunto_selecionado, $assuntos_ids ) && !empty($assunto_selecionado) ) {
						$this->db->set ( 'sim_id', $simulado_id );
						$this->db->set ( 'ass_id', $assunto_selecionado );
						$this->db->insert ( 'simulados_assuntos' );
					}
				}
			}
				
			foreach ( $assuntos_ids as $assunto_id ) {
				if (! in_array ( $assunto_id, $assuntos_selecionados )) {
					$this->db->where ( 'sim_id', $simulado_id );
					$this->db->where ( 'ass_id', $assunto_id );
					$this->db->delete ( 'simulados_assuntos' );
				}
			}
		} else {
			foreach ( $assuntos_selecionados as $assunto_selecionado ) {
				if(!empty($assunto_selecionado)) {
					$this->db->set ( 'sim_id', $simulado_id );
					$this->db->set ( 'ass_id', $assunto_selecionado );
					$this->db->insert ( 'simulados_assuntos' );
				}
			}
		}
	}
	
	public function get_assuntos_simulado($simulado_id) {
		$this->db->select('a.ass_id, a.ass_nome');
		$this->db->from('assuntos a');
		$this->db->where('sa.sim_id', $simulado_id);
		$this->db->join('simulados_assuntos sa','sa.ass_id=a.ass_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_dados_simulados_assuntos($simulado_id) {
		$this->db->from('simulados_assuntos');
		$this->db->where('sim_id', $simulado_id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function salvar_simulado_assunto($assunto){
		return $this->db->insert('simulados_assuntos', $assunto);
	}

	public function get_usuario_simulado($simulado_id, $usuario_id) {
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('usu_id', $usuario_id);
		$query = $this->db->get('simulados_usuarios');

		if($simulado_usuario = $query->row_array()) {
			return $simulado_usuario;
		}
		else {
			self::salvar_usuario_simulado($simulado_id, $usuario_id);		
			return self::get_usuario_simulado($simulado_id, $usuario_id);
		}
	}

	public function existe_usuario_simulado($simulado_id, $usuario_id)
	{
		$this->db->from('simulados_usuarios');
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('usu_id', $usuario_id);

		return $this->db->count_all_results() > 0 ? TRUE : FALSE;
	}
	
	public function salvar_usuario_simulado($simulado_id, $usuario_id) {
		$data = array(
			'sim_id' => $simulado_id,
			'usu_id' => $usuario_id
		);
		
		$this->db->insert('simulados_usuarios', $data);
		return $this->db->insert_id();
	}
	
	public function excluir_resultado_simulado($siu_id, $detalhado = false)
	{
		$this->db->where('siu_id', $siu_id);
		if($detalhado)
			$this->db->delete('simulados_resultados_detalhados');
		else
			$this->db->delete('simulados_resultados');
	}
	
	public function excluir_simulado_usuario($simulado_usuario_id)
	{
		$this->db->where('siu_id', $simulado_usuario_id);
		$this->db->delete('simulados_usuarios');
	}
	
	public function get_usuarios_designados($simulado_id) {
		$query = $this->db->get_where('simulados_usuarios', array('sim_id' => $simulado_id));
		return $query->result_array();
	}
	
	public function contar_usuarios_designados($simulado_id) {
		$this->db->from('simulados_usuarios');
		$this->db->where(array('sim_id' => $simulado_id));
		return $this->db->count_all_results();
	}
	
	public function is_aluno_designado($simulado_id, $aluno_id) {
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('usu_id', $aluno_id);
		$this->db->where('siu_designado', 1);
		$this->db->from('simulados_usuarios');

		return $this->db->count_all_results() > 0 ? true : false;
	}

	public function designar_usuario($simulado_id, $usuario_id, $data_limite)
	{
		self::get_usuario_simulado($simulado_id, $usuario_id);

		$dados = array('siu_designado' => 1, 'sim_data_limite'=> $data_limite);

		$this->db->where('sim_id', $simulado_id);
		$this->db->where('usu_id', $usuario_id);
		$this->db->update('simulados_usuarios',$dados);
	}
	
	public function designar_usuarios($simulado_id, $usuarios_id, $data_limite) {

		if(count($usuarios_id) > 0) {
			
			$simulado = self::get_by_id($simulado_id);
			
			foreach ($usuarios_id as $usu_id) {
				self::designar_usuario($simulado_id, $usu_id, $data_limite);
				
				$usuario = get_usuario_array($usu_id);
				
				$titulo = "Coaching Exponencial Concursos - {$simulado['sim_nome']}";
				$mensagem = get_template_email ( 'aviso-simulado-designado.php', array (
						'url' => get_resolver_simulado_url($simulado['sim_id']),
						'nome' => $usuario['nome_completo'],
						'simulado' => $simulado['sim_nome'],
						'data_limite' => converter_para_ddmmyyyy($data_limite)
				) );
				//enviar_email ( $usuario['email'], $titulo, $mensagem );

			}
		}
	}
	
	public function atualizar_resultados_simulado($usuario_id, $simulado_id)
	{
		$disciplinas = self::listar_disciplinas_por_ordem($simulado_id);
		$usuario_simulado = self::get_usuario_simulado($simulado_id, $usuario_id);
		
		self::excluir_resultado_simulado($usuario_simulado['siu_id']);
		
		foreach ($disciplinas as $disciplina) {
			$this->db->insert('simulados_resultados', array(
				'siu_id' => $usuario_simulado['siu_id'],		
				'dis_id' => $disciplina['dis_id'],
				'sre_resultado' => self::contar_acertos_por_disciplina($usuario_id, $disciplina['dis_id'], $simulado_id)
			));
		}
	}

	/**
	 * TODO: Verificar necessidade de função semelhante em Questao_model::contar_questoes_acertadas
	 **/
	public function contar_acertos_por_disciplina($usuario_id, $disciplina_id, $simulado_id)
	{
		$this->db->from('questoes_resolvidas qr');
		$this->db->join('questoes q', 'qr.que_id = q.que_id');
		$this->db->where('q.dis_id', $disciplina_id);
		$this->db->where('qr.usu_id', $usuario_id);
		$this->db->where('qr.sim_id', $simulado_id);
		$this->db->where('(qr.qre_selecao = q.que_resposta OR q.que_status = 2)'); // inclui questoes anuladas
		
		return $this->db->count_all_results();
	}

	public function contar_erros_por_disciplina($usuario_id, $disciplina_id, $simulado_id)
	{
		$this->db->from('questoes_resolvidas qr');
		$this->db->join('questoes q', 'qr.que_id = q.que_id');
		$this->db->where('q.dis_id', $disciplina_id);
		$this->db->where('qr.usu_id', $usuario_id);
		$this->db->where('qr.sim_id', $simulado_id);
		$this->db->where('(qr.qre_selecao is not null and qr.qre_selecao != q.que_resposta and q.que_status != 2)'); // inclui questoes anuladas
		
		return $this->db->count_all_results();
	}

	public function get_resultado_por_grupo($usuario_id, $simulado_id, $grupo_id)
	{
		$simulado_usuario = self::get_usuario_simulado($simulado_id, $usuario_id);

		//$this->db->select_sum('sr.sre_acertos');
		//$this->db->select_sum('sr.sre_resultado');
		$this->db->select_sum('sr.sre_acertos * (case when rd.rad_peso is not null then rd.rad_peso else 1 end)', 'sre_acertos');
		$this->db->select_sum('sr.sre_resultado * (case when rd.rad_peso is not null then rd.rad_peso else 1 end)', 'sre_resultado');
		$this->db->from('simulados_resultados sr');
		$this->db->join('rankings_disciplinas rd', 'sr.dis_id = rd.dis_id');
		$this->db->where('rd.rag_id', $grupo_id);
		$this->db->where('sr.siu_id', $simulado_usuario['siu_id']);
		$query = $this->db->get();

		return $query->row_array();
	}

	public function get_resultado_por_simulado($usuario_id, $simulado_id)
	{
		$simulado_usuario = self::get_usuario_simulado($simulado_id, $usuario_id);

		$this->db->select_sum('sr.sre_acertos');
		$this->db->select_sum('sr.sre_resultado');
		$this->db->from('simulados_resultados sr');
		$this->db->where('sr.siu_id', $simulado_usuario['siu_id']);
		$query = $this->db->get();

		return $query->row_array();
	}
	
	public function salvar_resultados_simulado($siu_id, $resultados, $detalhado = false) {
		$this->db->where('siu_id', $siu_id);
		$this->db->set( 'sim_usu_data_resolucao', date("Y-m-d H:i:s"));
		$this->db->update('simulados_usuarios');
		
		if($detalhado) {
			self::excluir_resultado_simulado($siu_id, true);
			$this->db->insert_batch('simulados_resultados_detalhados', $resultados);
		} else {
			self::excluir_resultado_simulado($siu_id);
			$this->db->insert_batch('simulados_resultados', $resultados);
		}
		
		$this->session->set_flashdata(ALERTA_SUCESSO, "Resultados do simulado gravados com sucesso");
	}

	public function listar_produtos_ids_de_simulados()
	{
		$this->db->select('prd_id');
		$this->db->distinct();
		$query = $this->db->get('simulados');
		$result = $query->result_array();
		
		$produtos_ids = array();
		foreach ($result as $row) {
			array_push($produtos_ids, $row['prd_id']);
		}
		
		return $produtos_ids;
	}
	
	public function comprou_simulado($simulado_id, $usuario_id, $somente_pagos = false)
	{
// 		$simulados = self::listar_produtos_ids_de_simulados();
		$comprados = get_produtos_ids_dos_pedidos($usuario_id, $somente_pagos);
		
		$simulado = self::get_by_id($simulado_id);

		return in_array($simulado['prd_id'], $comprados);
	}
	
	/**
	 * @todo remover do código
	 */

	public function get_simulado_usuario($simulado_id, $usuario_id)
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('usu_id', $usuario_id);
		$query = $this->db->get('simulados_usuarios');
		
		return $query->row_array();
	}
	
	public function get_resultados($simulado_id, $usuario_id)
	{
		$usuario_simulado = self::get_usuario_simulado($simulado_id, $usuario_id);
		
		$this->db->where('siu_id', $usuario_simulado['siu_id']);
		$query = $this->db->get('simulados_resultados');

		return $query->result_array();
	}

	public function get_resultado_disciplina($simulado_id, $disciplina_id, $usuario_id)
	{
		$usuario_simulado = self::get_usuario_simulado($simulado_id, $usuario_id);
		
		$this->db->where('siu_id', $usuario_simulado['siu_id']);
		$this->db->where('dis_id', $disciplina_id);
		$query = $this->db->get('simulados_resultados');
		
		return $query->row_array();
	}

	public function get_media_resultado_disciplina($simulado_id, $disciplina_id)
	{
		$this->db->select("AVG(sre_resultado) as media");
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('dis_id', $disciplina_id);
		$this->db->join('simulados_usuarios su', 'sr.siu_id = su.siu_id');
		$query = $this->db->get('simulados_resultados sr');
		
		$resultado = $query->row_array();

		return $resultado['media'];
	}
	
	public function get_aproveitamento($simulado_id, $usuario_id)
	{
		$total_questoes = self::get_pontuacao_maxima($simulado_id);
		$total_resultado = self::get_total_resultado($simulado_id, $usuario_id);
		
		return !is_null($total_resultado['sre_resultado']) ? ($total_resultado['sre_resultado'] / $total_questoes * 100) : 0;
	}

	public function get_pontuacao_maxima($simulado_id)
	{	
		if(!class_exists('Ranking_model')) {
			$this->load->model('Ranking_model');	
		}

		$ranking = $this->Ranking_model->get_ranking_por_simulado($simulado_id);
		
		// verifica se existe ranking
		if($ranking) {

			$pontuacao_maxima = 0;

			// recupera as disciplinas
			$disciplinas = self::listar_disciplinas_simulado($simulado_id);

			foreach ($disciplinas as $disciplina) {
				$peso = get_peso_utilizado($ranking['ran_id'], $disciplina['dis_id']);
				$pontuacao_maxima += ($disciplina['qtde'] * $peso);
			}

			return $pontuacao_maxima;
		}
		// se não existe ranking retorna quantidade de questões
		else {
			return self::get_total_questoes($simulado_id);
		}
	}

	public function get_pontuacao_maxima_disciplina($simulado_id, $disciplina_id)
	{	
		$ci =& get_instance();
		$ci->load->model('Ranking_model');	

		$ranking = $ci->Ranking_model->get_ranking_por_simulado($simulado_id);
		
		// verifica se existe ranking
		if($ranking) {
			// recupera as disciplinas
			$disciplina = self::get_simulado_disciplina($simulado_id, $disciplina_id);
			
			$peso = get_peso_utilizado($ranking['ran_id'], $disciplina_id);
			
			return $disciplina['sid_qtde_questoes'] * $peso;
		}
		// se não existe ranking retorna quantidade de questões
		else {
			return self::get_maximo_questoes_disciplina($simulado_id, $disciplina_id);
		}
	}
	
	public function get_total_questoes($simulado_id)
	{
		$this->db->select_sum('sid_qtde_questoes');
		$this->db->where('sim_id', $simulado_id);
		$query = $this->db->get('simulados_disciplinas');
		
		$row = $query->row_array();
		return $row['sid_qtde_questoes'];
	}
	
	public function get_total_resultado($simulado_id, $usuario_id)
	{
		$usuario_simulado = self::get_usuario_simulado($simulado_id, $usuario_id);
		
		$this->db->select_sum('sre_resultado');
		$this->db->where('siu_id', $usuario_simulado['siu_id']);
		$query = $this->db->get('simulados_resultados');
		
		return $query->row_array();
	}
	
	public function get_ranking($simulado_id, $offset = 0, $limit = LIMIT_RANKING, $somente_aprovados = false, $exportar = false)
	{
		$this->db->select('su.*, sum(sr.sre_resultado) as total_resultado');
		$this->db->join('simulados_usuarios su', 'sr.siu_id = su.siu_id');
		$this->db->where('sim_id', $simulado_id);
		$this->db->group_by('siu_id');
		$this->db->order_by('total_resultado desc, siu_id asc');

		if($somente_aprovados) {
			$this->db->where("su.siu_id not in (select siu_id from simulados_resultados where sre_aprovado = 0)");
			$this->db->having("sum(sr.sre_resultado) > 0");
		}
		
		if(!$exportar && !is_null($limit)) {
			$this->db->limit($limit, $offset);
		}

		$query = $this->db->get('simulados_resultados sr');

		$resultado = $query->result_array();

		return $resultado;
	}

	public function get_ranking_by_id($ranking_id, $offset = 0, $limit = LIMIT_RANKING, $somente_aprovados = false, $exportar = false)
	{
		$this->db->select('su.usu_id, sum(sr.sre_resultado * (case when rd.rad_peso is not null then rd.rad_peso else 1 end)) as total_resultado');
		$this->db->join(DB_NAME_CORP . '.simulados_usuarios su', 'sr.siu_id = su.siu_id');
		$this->db->join(DB_NAME_CORP . '.rankings_simulados rs', 'rs.sim_id = su.sim_id');		
		$this->db->join(DB_NAME_CORP . '.rankings_disciplinas rd', '(sr.dis_id = rd.dis_id and rs.ran_id = rd.ran_id)', 'left');
		// $this->db->join(DB_NAME . '.wp_users u', 'u.ID = su.usu_id');
		$this->db->where('rs.ran_id', $ranking_id);
		$this->db->group_by('usu_id');
		// $this->db->order_by('total_resultado desc, u.display_name');
		$this->db->order_by('total_resultado desc');

		if($somente_aprovados) {
			$this->db->where("su.siu_id not in (select siu_id from " . DB_NAME_CORP . ".simulados_resultados where sre_aprovado = 0)");
			$this->db->having("sum(sr.sre_resultado) > 0");
		}
		
		if(!$exportar && !is_null($limit)) {
			$this->db->limit(LIMIT_RANKING, $offset);
		}

		$query = $this->db->get(DB_NAME_CORP . '.simulados_resultados sr');

		$resultado = $query->result_array();

		return $resultado;
	}

	public function get_media_ranking($simulado_id, $num_primeiros, $somente_aprovados = false)
	{
		$ranking = self::get_ranking($simulado_id, 0, $num_primeiros, $somente_aprovados);
		
		$soma = 0;
		foreach ($ranking as $item) {
			$soma += $item['total_resultado'];
		}

		return $soma / $num_primeiros;
	}

	public function get_posicao_ranking($simulado_id, $usuario_id, $somente_aprovados = 0)
	{
		$ranking = self::get_ranking($simulado_id, 0, NULL, $somente_aprovados);
		
		for($i = 0; $i < count($ranking); $i++) {
			if($usuario_id == $ranking[$i]['usu_id']) {
				$item = $ranking[$i];
				$item['posicao'] = $i + 1;
				return $item;
			}
		}
		
		return false;
	}
	
	public function count_ranking($simulado_id, $somente_aprovados)
	{
		return count(self::get_ranking($simulado_id, 0, NULL, $somente_aprovados));
	}

	public function count_ranking_by_id($ranking_id, $somente_aprovados)
	{
		return count(self::get_ranking_by_id($ranking_id, 0, NULL, $somente_aprovados));
	}
	
	public function get_resultados_detalhados($simulado_id, $usuario_id)
	{
		$usuario_simulado = self::get_usuario_simulado($simulado_id, $usuario_id);
	
		$this->db->where('siu_id', $usuario_simulado['siu_id']);
		$query = $this->db->get('simulados_resultados_detalhados');
	
		return $query->result_array();
	}
	
	public function listar_resolvidos($usuario_id)
	{
		$this->db->select('s.*');
		$this->db->distinct();
		$this->db->from('simulados s');
		$this->db->join('simulados_usuarios su', 's.sim_id = su.sim_id');
		$this->db->join('simulados_resultados sr', 'sr.siu_id = su.siu_id');
		$this->db->where('usu_id', $usuario_id);
		$query = $this->db->get();
		
		return $query->result_array();
	}

	public function listar_simulado_questoes($simulado_id, $ordem = null)
    {
        $this->db->where('sim_id', $simulado_id);
        if (!is_null($ordem)) {
            $this->db->order_by($ordem);
        }
        $query = $this->db->get('simulados_questoes');

        return $query->result_array();
    }

	public function listar_questoes_ids_por_simulado($simulado_id) 
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->order_by('siq_ordem', 'asc');
		$query = $this->db->get('simulados_questoes');
		
		$resultado = $query->result_array();
		
		$ids = array();
		
		foreach ($resultado as $item) {
			array_push($ids, $item['que_id']);	
		}
	
		return $ids; 
	}
	
	public function resolver_simulado($simulado_id, $questoes_respostas)
	{
		$resultados = array();
		
		$questoes_ids = self::listar_questoes_ids_por_simulado($simulado_id);
		
		foreach ($questoes_ids as $questao_id) {
			$this->questao_model->salvar_resposta_usuario(get_current_user_id(), $questao_id, $questoes_respostas[$questao_id], $simulado_id);
		}
		
		self::atualizar_resultados_simulado(get_current_user_id(), $simulado_id);
		
		return $resultados;
	}

	public function listar_questoes_respondidas_respostas($simulado_id, $usuario_id) {
		$resultados = array();
		
		$questoes_ids = self::listar_questoes_ids_por_simulado($simulado_id);
		
		foreach ($questoes_ids as $questao_id) {
			$resposta = self::get_questao_respondida($simulado_id, $questao_id, $usuario_id);

			if(!$resposta) {
				$resultados[$questao_id] = NULL;
			}
			else {
				$resultados[$questao_id] = $resposta['qre_selecao'];
			}	
		}
		
		return $resultados;
	}

	public function get_questao_respondida($simulado_id, $questao_id, $usuario_id)
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('que_id', $questao_id);
		$this->db->where('usu_id', $usuario_id);
		$query = $this->db->get('questoes_resolvidas');

		return $query->row_array();
	}

	public function get_respostas()
	{
		$resultados = array();
		
		$questoes_ids = self::listar_questoes_ids_por_simulado($simulado_id);
		
		foreach ($questoes_ids as $questao_id) {
			if(!array_key_exists($questao_id, $questoes_respostas)) {
				$resultados[$questao_id] = ERRADO;
			}
			else {
				$query = $this->db->get_where('questoes', array('que_id' => $questao_id));
				$questao = $query->row_array();
				
				if($questoes_respostas[$questao_id] == $questao['que_resposta'] || $questao['que_status'] == CANCELADA) {
					$resultados[$questao_id] = CERTO;
				}
				else {
					$resultados[$questao_id] = ERRADO;
				}
			}	
			
			$this->questao_model->salvar_resposta_usuario(get_current_user_id(), $questao_id, $questoes_respostas[$questao_id], $simulado_id);
		}
		
		self::atualizar_resultados_simulado(get_current_user_id(), $simulado_id);
		
		return $resultados;
	}	

	private function adicionar_filtros($filtro) {
		
		$this->db->join('provas_cargos as pc', 'p.pro_id=pc.pro_id', 'left');
		$this->db->join('provas_areas_formacao as pf', 'p.pro_id=pf.pro_id', 'left');
		$this->db->join('provas_areas_atuacao as pa', 'p.pro_id=pa.pro_id', 'left');
		$this->db->join('simulados_questoes as sq', 'sq.que_id=q.que_id', 'left');
	
		$posts = array(
			'ban_ids' => 'p.ban_id', 
			'org_ids' => 'p.org_id', 
			'car_ids' => 'pc.car_id', 
			'pro_anos' => 'p.pro_ano',
			'esc_ids' => 'p.esc_id',
			'arf_ids' => 'pf.arf_id', 
			'dis_ids' => 'q.dis_id', 
			'ass_ids' => 'qa.ass_id', 
			'ara_ids' => 'pa.ara_id',
			'que_tipos' => 'q.que_tipo', 
			'que_ids' => 'q.que_id', 
			'que_dificuldades' => 'q.que_dificuldade', 
			'sim_ids' => 'sq.sim_id',
			'que_statuses' => 'q.que_status'
		);
	
		foreach($posts as $post => $campo) {
			if(isset($filtro[$post])) {
				$count= 1;
				$ban_ids = $filtro[$post];
				$where = "(";
				foreach($ban_ids as $ban_id) {
					if($count == 1)
						$where .= $campo . "=" . $ban_id;
					else
						$where .= " OR " . $campo . "=" . $ban_id;
	
					$count++;
				}
				$where .= ")";
				$this->db->where($where, null, false);
			}
		}

	}
	
	function listar_simulados_associados($simulado_id)
	{
		$this->db->join('simulados_associados sa', 's.sim_id = sa.sim_id');
		$this->db->where('s.sim_id', $simulado_id);
		$query = $this->db->get('simulados s');
		
		return $query->result_array();
	}
	
	function listar_simulados_exceto($simulado_id = null)
	{
		if(!is_null($simulado_id)) {
			$this->db->where('sim_id !=', $simulado_id);
		}
			
		$this->db->order_by('sim_nome');
		$query = $this->db->get('simulados');
	
		return $query->result_array();
	}
	
	function atualizar_simulados_associados($simulado_id, $simulados_associados_ids)
	{
		self::excluir_simulados_associados($simulado_id);
		
		$dados = array();
		
		if($simulados_associados_ids) {
			foreach ($simulados_associados_ids as $id) {
				array_push($dados, array(
					'sim_id' => $simulado_id,
					'sim_associado_id' => $id
				));
			}
		}
		
		if($dados) {
			$this->db->insert_batch('simulados_associados', $dados);
		}
	}
	
	function excluir_simulados_associados($simulado_id)
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->delete('simulados_associados');
	}
	
	function listar_questoes_ids_simulados_associados($simulado_id)
	{
		$simulados_associados = self::listar_simulados_associados($simulado_id);
		
		// retorna nulo se não houver simulados associados
		if(!$simulados_associados) return null;
		
		$this->db->distinct();
		$this->db->select('que_id');
		
		foreach ($simulados_associados as $simulado_associado) {
			$this->db->where('sim_id', $simulado_associado['sim_associado_id']);
		}
		
		$query = $this->db->get('simulados_questoes');
		$questoes = $query->result_array();
		
		$questoes_ids = array();
		foreach($questoes as $questao) {
			array_push($questoes_ids, $questao['que_id']);
		}
		
		return $questoes_ids;
	}
	
	public function incrementar_tempo_gasto($simulado_id, $usuario_id, $segundos = TEMPO_GASTO_DEFAULT)
	{
		$simulado_usuario = self::get_usuario_simulado($simulado_id, $usuario_id);
		
		$tempo_gasto = is_null($simulado_usuario['siu_tempo_gasto']) ? $segundos : $simulado_usuario['siu_tempo_gasto'] + $segundos;
	
		$this->db->set('siu_tempo_gasto', $tempo_gasto);
		$this->db->where('siu_id', $simulado_usuario['siu_id']);
		$this->db->update('simulados_usuarios');
	}
	
	public function zerar_tempo_gasto($simulado_id, $usuario_id)
	{
		$simulado_usuario = self::get_usuario_simulado($simulado_id, $usuario_id);
		
		$this->db->set('siu_tempo_gasto', 0);
		$this->db->where('siu_id', $simulado_usuario['siu_id']);
		$this->db->update('simulados_usuarios');
		
	}
	
	public function finalizar_simulado($siu_id, $tempo_gasto, $exibir_detalhes = true)
	{
		$this->db->where('siu_id', $siu_id);
		$this->db->set('siu_tempo_gasto', $tempo_gasto);
		$this->db->set('sim_usu_data_resolucao', date('Y-m-d H:i:s'));
		$this->db->set('siu_exibir_detalhes', (int)$exibir_detalhes);
		$this->db->update('simulados_usuarios');
	}
	
	public function get_resposta($simulado_id, $usuario_id, $questao_id)
	{
		$query = $this->db->get_where('questoes_resolvidas', array(
				'que_id' => $questao_id,
				'sim_id' => $simulado_id,
				'usu_id' => $usuario_id
		));
		
		return $query->row_array();
	}
	
	public function listar_respostas($simulado_id, $usuario_id)
	{
		$questoes_ids = self::listar_questoes_ids_por_simulado($simulado_id);
		
		$respostas = array();
		
		foreach ($questoes_ids as $questao_id) {
			$r = self::get_resposta($simulado_id, $usuario_id, $questao_id);
			
			$respostas[$questao_id] = is_null($r) ? $r : $r['qre_selecao'];
		}
		
		return $respostas;
	}
	
	public function atualizar_revisores($simulado_id, $revisores)
	{
		self::remover_todos_revisores($simulado_id);	

		foreach ($revisores as $revisor) {
			$this->db->insert('simulados_revisores', array(
				'sim_id' => $simulado_id,
				'dis_id' => $revisor['disciplina_id'],
				'usu_id' => $revisor['usuario_id']
			));

		}
	}

	public function adicionar_revisor($simulado_id, $disciplina_id, $usuario_id)
	{
		$this->db->insert('simulados_revisores', array(
			'sim_id' => $simulado_id,
			'dis_id' => $disciplina_id,
			'usu_id' => $usuario_id
		));
	}
	
	public function remover_todos_revisores($simulado_id)
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->delete('simulados_revisores');
	}
	
	public function listar_revisores_por_disciplina($simulado_id, $disciplina_id)
	{
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('dis_id', $disciplina_id);
		$query = $this->db->get('simulados_revisores');
		
		return $query->result_array();
	}
	
	public function listar_revisores_ids_por_disciplina($simulado_id, $disciplina_id)
	{
		$revisores = self::listar_revisores_por_disciplina($simulado_id, $disciplina_id);
	
		$ids = array();
		foreach ($revisores as $revisor) {
			if(is_professor_oculto($revisor['usu_id'])) {
				continue;
			}
			array_push($ids, $revisor['usu_id']);
		}
		
		return $ids;
	}
	
	public function listar_revisores_agrupados_por_disciplina($simulado_id, $somente_ids = false)
	{
		$disciplinas = self::listar_disciplinas_por_ordem($simulado_id);
		
		$revisores = array();
		foreach ($disciplinas as $disciplina) {			
			if($somente_ids) {
				$revisores[$disciplina['dis_id']] = self::listar_revisores_ids_por_disciplina($simulado_id, $disciplina['dis_id']);
			}
			else {
				$revisores[$disciplina['dis_id']] = self::listar_revisores_por_disciplina($simulado_id, $disciplina['dis_id']);
			}
		}
		
		return $revisores;
	}
	
	public function listar_revisores($simulado_id)
	{
		$this->db->where('sim_id', $simulado_id);
		$query = $this->db->get('simulados_revisores');
		
		return $query->result_array();
	}
	
	public function pode_revisar_simulado($simulado_id, $usuario_id)
	{
		$simulado = self::get_by_id($simulado_id);
		if(!$simulado['sim_revisao_ativada']) return false;
		
		$revisores = self::listar_revisores($simulado_id);
		
		foreach ($revisores as $revisor) {
			if($revisor['usu_id'] == $usuario_id) {
				return true;
			}
		}
		
		return false;
	}
	
	public function pode_revisar_disciplina_simulado($simulado_id, $disciplina_id, $usuario_id)
	{
		$simulado = self::get_by_id($simulado_id);
		if(!$simulado['sim_revisao_ativada']) return false;
	
		$revisores = self::listar_revisores_por_disciplina($simulado_id, $disciplina_id);
	
		foreach ($revisores as $revisor) {
			if($revisor['usu_id'] == $usuario_id) {
				return true;
			}
		}
	
		return false;
	}

	public function contar_comentarios_de_professor($simulado_id, $disciplina_id)
	{
		$this->db->join('questoes q', 'q.que_id = sq.que_id');		
		$this->db->from('simulados_questoes sq');
		$this->db->where('q.dis_id', $disciplina_id);
		$this->db->where('sq.sim_id', $simulado_id);
		$query = $this->db->get();
		
		$resultado = $query->result_array();
		
		
		$num = 0;
		foreach ($resultado as $item) {
			$num += $item['que_num_comentarios_professores'];			
		}
		
		return $num;
	}
	
	public function get_ranking_by_simulado_id($simulado_id)
	{
		$this->db->from('rankings_simulados rs');
		$this->db->join('rankings r', 'r.ran_id = rs.ran_id');
		$this->db->where('rs.sim_id', $simulado_id);
		$query = $this->db->get();

		return $query->row_array();
	}

	public function get_nota($usuario_id, $simulado_id)
	{
		$resultado = 0;

		$notas = self::listar_notas($usuario_id, $simulado_id);

		foreach ($notas as $nota) {
			$resultado += $nota['sre_resultado'];
		}

		return $resultado;
	}

	public function listar_notas($usuario_id, $simulado_id)
	{
		$siu = self::get_usuario_simulado($simulado_id, $usuario_id);

		$this->db->where('siu_id', $siu['siu_id']);
		$query = $this->db->get('simulados_resultados');

		return $query->result_array();
	}

	public function salvar_nota($is_apenas_calculo, $usuario_id, $simulado_id, $disciplinas_acertos = null, $disciplinas_erros = null)
	{
		$resultado = 0;

		if(!$is_apenas_calculo){
			self::excluir_simulado_resultado($usuario_id, $simulado_id);
		}

		
		if($disciplinas = self::listar_disciplinas_por_ordem($simulado_id)) {

			// salva a nota de cada disciplina
			foreach ($disciplinas as $disciplina) {

				if(is_null($disciplinas_acertos)) {
					$resultado += self::salvar_nota_por_disciplina($is_apenas_calculo, $usuario_id, $simulado_id, $disciplina['dis_id']);
				}

				else {
					$acertos = isset($disciplinas_acertos[$disciplina['dis_id']]) ? $disciplinas_acertos[$disciplina['dis_id']] : 0;
					if(is_null($disciplinas_erros)){
						$resultado += self::salvar_nota_por_disciplina($is_apenas_calculo, $usuario_id, $simulado_id, $disciplina['dis_id'], $acertos);
					}else{
						$erros = isset($disciplinas_erros[$disciplina['dis_id']]) ? $disciplinas_erros[$disciplina['dis_id']] : 0;
						$resultado += self::salvar_nota_por_disciplina($is_apenas_calculo, $usuario_id, $simulado_id, $disciplina['dis_id'], $acertos, $erros);
					}
				}
			}

			if(!$is_apenas_calculo){
				// salva o resultado de cada disciplina (se usuário foi aprovado ou não)
				foreach ($disciplinas as $disciplina) {
					if(is_null($disciplinas_acertos)) {
						$aprovado = self::verificar_resultado_por_disciplina($usuario_id, $simulado_id, $disciplina['dis_id']);
					}

					else {
						$acertos = isset($disciplinas_acertos[$disciplina['dis_id']]) ? $disciplinas_acertos[$disciplina['dis_id']] : 0;

						$aprovado = self::verificar_resultado_por_disciplina($usuario_id, $simulado_id, $disciplina['dis_id'], $acertos);
					}

					self::salvar_simulado_aprovado_disciplina($usuario_id, $simulado_id, $disciplina['dis_id'], $aprovado); 
				}
			}
		}

		return $resultado;
	}


	public function salvar_nota_por_disciplina($is_apenas_calculo, $usuario_id, $simulado_id, $disciplina_id, $acertos = null, $erros = null)
	{
		if(!class_exists('Ranking_model')) {
			$this->load->model('ranking_model');	
		}

		$nota = 0;
		
		log_sq("DEBUG", "salvar_nota_por_disciplina({$is_apenas_calculo}, {$usuario_id}, {$simulado_id}, {$disciplina_id}, {$acertos}, {$erros})");

		if(is_null($acertos)) {
			$acertos = self::contar_acertos_por_disciplina($usuario_id, $disciplina_id, $simulado_id);
			log_sq("DEBUG", "buscou acertos({$acertos})");
		}

		// tem ranking associado
		if($ranking = self::get_ranking_by_simulado_id($simulado_id)) {
			log_sq("DEBUG", "tem ranking({$ranking})");

			$disciplina_grupo = $this->ranking_model->get_disciplina_grupo($ranking['ran_id'], $disciplina_id);

			if($rag_id = $disciplina_grupo['rag_id']) {
				log_sq("DEBUG", "tem grupo rag_id = ({$rag_id})");
				// disciplina está associada a algum grupo

				$grupo = $this->ranking_model->get_grupo($rag_id);

				log_sq("DEBUG", "Vamos testar os valores => rag_peso: {$grupo['rag_peso']}, rad_peso: {$disciplina_grupo['rad_peso']}, ras_peso: {$ranking['ras_peso']}");
				// grupo tem peso associado
				if($grupo['rag_peso'] > 1) {
					log_sq("DEBUG", "usou peso grupo rag_peso = ({$grupo['rag_peso']})");
					$nota = $grupo['rag_peso'] * $acertos;
				}

				// disciplina tem peso associado
				elseif($disciplina_grupo['rad_peso'] > 1){
					log_sq("DEBUG", "usou peso disciplina rad_peso = ({$disciplina_grupo['rad_peso']})");
					$nota = $disciplina_grupo['rad_peso'] * $acertos;
				}

				// grupo não tem peso associado, nesse caso usamos o peso do ranking
				else {
					log_sq("DEBUG", "usou peso simulado ras_peso = ({$ranking['ras_peso']})");
					$nota = ($ranking['ras_peso']?:1) * $acertos;
				}

			}
			else {
				log_sq("DEBUG", "nao tem rag_id");
				log_sq("DEBUG", "Vamos testar os valores => rad_peso: {$disciplina_grupo['rad_peso']}, ras_peso: {$ranking['ras_peso']}");
				// disciplina não está associada a nenhum grupo
				if($disciplina_grupo['rad_peso'] > 1){
					log_sq("DEBUG", "usou peso disciplina = ({$disciplina_grupo['rad_peso']})");
					$nota = $disciplina_grupo['rad_peso'] * $acertos;
				}else{
					log_sq("DEBUG", "usou peso simulado = ({$ranking['ras_peso']})");
					$nota = ($ranking['ras_peso']?:1) * $acertos;				
				}
			}

			log_sq("DEBUG", "Nota calculada foi: {$nota}");
			
			//O peso dos erros é geral então posso fazer a subtração no final
			if($ranking['ran_peso_erros'])
			{
				if(is_null($erros)) {
					$erros = self::contar_erros_por_disciplina($usuario_id, $disciplina_id, $simulado_id);
				}
				$nota = $nota - ( $ranking['ran_peso_erros'] * $erros );
				log_sq("DEBUG", "Usando os {$erros} erros e o peso {$ranking['ran_peso_erros']} a nota RE-calculada foi: {$nota}");
			}

		}

		// não tem ranking associado
		else {
			$nota = $acertos;

			$simulado = self::get_by_id($simulado_id);

			//Se os erros anularem alguma certa...
			if($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']){
				if(is_null($erros)) {
					$erros = self::contar_erros_por_disciplina($usuario_id, $disciplina_id, $simulado_id);
				}
				$nota = $acertos - $erros * $simulado['sim_peso_erros'];
			}
		}

		// $aprovado = self::verificar_resultado_por_disciplina($usuario_id, $simulado_id, $disciplina_id, $acertos);


		// echo $disciplina_id . ' - ' . $nota . ' - ' . (int)$aprovado . "<br>";
		if(!$is_apenas_calculo){
			self::excluir_simulado_resultado_disciplina($usuario_id, $simulado_id, $disciplina_id);
			self::salvar_simulado_resultado_disciplina($usuario_id, $simulado_id, $disciplina_id, $nota, $acertos);
		}
		
		return $nota;
	}

	public function excluir_simulado_resultado_disciplina($usuario_id, $simulado_id, $disciplina_id)
	{
		$siu = self::get_usuario_simulado($simulado_id, $usuario_id);

			// exclui resultado antigo
		$this->db->where('siu_id', $siu['siu_id']);
		$this->db->where('dis_id', $disciplina_id);
		$this->db->delete('simulados_resultados');
	}

	public function excluir_simulado_resultado($usuario_id, $simulado_id)
	{
		$siu = self::get_usuario_simulado($simulado_id, $usuario_id);

			// exclui resultado antigo
		$this->db->where('siu_id', $siu['siu_id']);
		$this->db->delete('simulados_resultados');
	}

	public function salvar_simulado_resultado_disciplina($usuario_id, $simulado_id, $disciplina_id, $nota, $acertos)
	{
		$siu = self::get_usuario_simulado($simulado_id, $usuario_id);

		$this->db->set('siu_id', $siu['siu_id']);
		$this->db->set('dis_id', $disciplina_id);
		$this->db->set('sre_resultado', $nota);
		$this->db->set('sre_acertos', $acertos);
		$this->db->insert('simulados_resultados');
	}

	public function salvar_simulado_aprovado_disciplina($usuario_id, $simulado_id, $disciplina_id, $aprovado)
	{
		$siu = self::get_usuario_simulado($simulado_id, $usuario_id);

		$this->db->where('siu_id', $siu['siu_id']);
		$this->db->where('dis_id', $disciplina_id);
		$this->db->set('sre_aprovado', (int)$aprovado);
		$this->db->update('simulados_resultados');
	}

	public function is_aprovado_por_ranking($usuario_id, $ranking_id, $total_ranking, $ran_corte){
		
		if(!class_exists('Ranking_model')) {
			$this->load->model('ranking_model');	
		}

		$ranking_simulados = $this->ranking_model->listar_simulados($ranking_id);

		foreach($ranking_simulados as $ranking_simulado){

			$aprovado = self::is_aprovado($usuario_id, $ranking_simulado['sim_id']);
			if((!$aprovado)){
				return false;
			}

		}

		if($ran_corte){
			return $total_ranking >= $ran_corte;
		}

		return true;

	}
	
	public function is_aprovado($usuario_id, $simulado_id)
	{
		if(!class_exists('Ranking_model')) {
			$this->load->model('ranking_model');	
		}

		$notas = self::listar_notas($usuario_id, $simulado_id);

		foreach ($notas as $nota) {
			if(!$nota['sre_aprovado']) {
				return false;
			}
		}

		$nota = self::get_nota($usuario_id, $simulado_id);

		if($ranking_simulado = self::get_ranking_by_simulado_id($simulado_id)
			&& $ranking_simulado['ras_corte']){
			return $nota >= $ranking_simulado['ras_corte'];
		}

		return $nota > 0 ? TRUE : FALSE;
	}

	public function verificar_resultado_por_disciplina($usuario_id, $simulado_id, $disciplina_id, $acertos = null)
	{
		if(!class_exists('Ranking_model')) {
			$this->load->model('ranking_model');	
		}

		if(is_null($acertos)) {
			$acertos = self::contar_acertos_por_disciplina($usuario_id, $disciplina_id, $simulado_id);
		}

		// tem ranking associado
		if($ranking = self::get_ranking_by_simulado_id($simulado_id)) {
				
			$disciplina_grupo = $this->ranking_model->get_disciplina_grupo($ranking['ran_id'], $disciplina_id);

			if($rag_id = $disciplina_grupo['rag_id']) {
				// disciplina está associada a algum grupo

				$grupo = $this->ranking_model->get_grupo($rag_id);

				$resultado_grupo = self::get_resultado_por_grupo($usuario_id, $simulado_id, $rag_id);

				//$acertos_grupo = $resultado_grupo['sre_acertos'];
				$resultados_grupo = $resultado_grupo['sre_resultado'];
				
				// grupo e disciplina tem peso associado
				if($grupo['rag_peso'] && $disciplina_grupo['rad_peso']) {
					return ($grupo['rag_peso'] * $resultados_grupo) >= $grupo['rag_corte']
						&& ($disciplina_grupo['rad_peso'] * $acertos) >= $disciplina_grupo['rad_corte'];
				}
				
				// grupo tem peso associado
				elseif($grupo['rag_peso']) {
					return ($grupo['rag_peso'] * $resultados_grupo) >= $grupo['rag_corte'];
				}

				// disciplina tem peso associado
				elseif($disciplina_grupo['rad_peso']){
					return ($disciplina_grupo['rad_peso'] * $acertos) >= $disciplina_grupo['rad_corte'];
				}

				// grupo não tem peso associado, nesse caso usamos o peso do ranking
				else {
					return ($ranking['ran_peso'] * $resultados_grupo) >= $ranking['ran_corte'];
				}

			}
			else {
				// disciplina não está associada a nenhum grupo
				return ($disciplina_grupo['rad_peso'] * $acertos) >= $disciplina_grupo['rad_corte'];				
			}

		}

		// não tem ranking associado
		else {
			return true;
		}
	}

	public function get_simulado_disciplina($simulado_id, $disciplina_id)
	{
		$this->db->from('simulados_disciplinas');
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('dis_id', $disciplina_id);
		$query = $this->db->get();

		return $query->row_array();
	}

	/**
	 * Lista simulados por status.
	 * 
	 * [JOULE][J1] UPGRADE - Controle de prioridade dos simulados
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/340
	 * 
	 * @since 10.0.0
	 * 
	 * @param int $status Status disponíveis: ATIVO, INATIVO, INCONSISTENTE
	 * @param string $order_by Ordenação desejada
	 *
	 * @return array Simulados
	 */

	public function listar_por_status($status, $order_by = 'sim_nome asc')
	{
		$this->db->from('simulados s');
		$this->db->where('s.sim_status', $status);
		$this->db->order_by($order_by);

		$query = $this->db->get();

		return $query->result_array();
	}

	/**
	 * Define a prioridade de todos os simulados para null
	 * 
	 * [JOULE][J1] UPGRADE - Controle de prioridade dos simulados
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/340
	 * 
	 * @since 10.0.0
	 */

	public function remover_prioridades()
	{
		$this->db->where(1,1);
		$this->db->set('sim_prioridade', null);
		$this->db->update('simulados');
	}

	/**
	 * Lista os simulados que possuem prioridade igual a null
	 * 
	 * [JOULE][J1] UPGRADE - Controle de prioridade dos simulados
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/340
	 * 
	 * @since 10.0.0
	 *
	 * @return array Simulados
	 */

	public function listar_sem_prioridade()
	{
		$this->db->from('simulados');
		$this->db->where('sim_prioridade IS NULL');
		$query = $this->db->get();

		return $query->result_array();
	}

	/**
	 * Atualiza a prioridade de um determinado simulado ou de todos
	 * 
	 * [JOULE][J1] UPGRADE - Controle de prioridade dos simulados
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/340
	 * 
	 * @since 10.0.0
	 *
	 * @param int|null $simulado_id Id do simulado
	 * @param int $prioridade Valor da prioridade. O valor 1 é o mais alto.
	 */

	public function atualizar_prioridade($simulado_id, $prioridade)
	{
		// atualiza prioridade de um simulado específico
		if($simulado_id) {
			$this->db->where('sim_id', $simulado_id);	
		}

		// se $simulado_id for null atualiza a prioridade de todos os simulados
		else {
			$this->db->where('sim_id >', 0);
		}
		
		$this->db->set('sim_prioridade', $prioridade);
		$this->db->update('simulados');
	}

	/**
	 * Lista os rótulos do simulado informado
	 *
	 * @since K1
	 *
	 * @param int $simulado_id Id do simulado
	 * 
	 * @return array
	 */

	public function listar_rotulos($simulado_id)
	{
		$this->db->where("sim_id", $simulado_id);
		$this->db->order_by('sro_ordem ASC');
		$query = $this->db->get("simulados_rotulos");

		return $query->result_array();
	}

	/**
	 * Criar rótulos caso ainda não tenham sido criados
	 *
	 * @since K1
	 *
 	 * @param int $simulado_id Id do simulado
	 */

	public function criar_rotulos_iniciais($simulado_id)
	{
		if($disciplinas = self::get_disciplinas_simulado($simulado_id)) {

			$ordem = 0;
			foreach ($disciplinas as $disciplina) {
				$rotulo_id = self::salvar_rotulo([
					'sro_nome' => $disciplina['dis_nome'],
					'sro_ordem' => $disciplina['sid_ordem'] ?: time() + ($ordem++),
					'sim_id' => $simulado_id
				]);	

				$rotulo = self::get_rotulo($rotulo_id);

				$questoes = self::listar_questoes_disciplina($simulado_id, $disciplina['dis_id']);
				foreach ($questoes as $questao) {
					self::atualizar_rotulo_em_questao($simulado_id, $questao['que_id'], $rotulo_id);
				}
			}
		}
	}

	/**
	 * Salva um rótulo
	 *
	 * @since K1
	 *
	 * @param array $rotulo array com dados dos rótulos
	 */

	public function salvar_rotulo($rotulo)
	{
		$this->db->set($rotulo);

		if(isset($rotulo['sro_id']) && $rotulo['sro_id']) {
			$this->db->where('sro_id', $rotulo['sro_id']);
			$this->db->update('simulados_rotulos');

			return $rotulo['sro_id'];
		}
		else {
			$this->db->insert('simulados_rotulos');	
			return $this->db->insert_id();
		}


	}

	/**
	 * Recupera um rótulo
	 *
	 * @since K1
	 *
	 * @param int $simulado_id Id do simulado
	 * 
	 * @return array
	 */

	public function get_rotulo($rotulo_id)
	{
		$this->db->where('sro_id', $rotulo_id);
		$query = $this->db->get('simulados_rotulos');

		return $query->row_array();
	}

	/**
	 * Recupera um rótulo
	 *
	 * @since K1
	 *
	 * @param int $simulado_id Id do simulado
	 * 
	 * @return array
	 */

	public function atualizar_rotulo_em_questao($simulado_id, $questao_id, $rotulo_id)
	{
		$this->db->set('sro_id', $rotulo_id);
		$this->db->where('sim_id', $simulado_id);
		$this->db->where('que_id', $questao_id);
		$this->db->update('simulados_questoes');
	}

	/**
	 * Lista questões de um determinado rótulo
	 *
	 * @since K1
	 *
	 * @param int $rotulo_id Id do rótulo
	 * 
	 * @return array
	 */

	public function listar_questoes_por_rotulo($rotulo_id)
	{
		$this->db->where("sro_id", $rotulo_id);
		$this->db->order_by("siq_ordem ASC");
		$query = $this->db->get("simulados_questoes");

		return $query->result_array();
	}

	public function get_proxima_ordem_rotulo($simulado_id)
	{
		$this->db->select_max("sro_ordem");
		$this->db->where("sim_id", $simulado_id);
		$query = $this->db->get("simulados_rotulos");

		if($resultado = $query->row_array()) {
			return $resultado['sro_ordem'] + 1;
		};

		return 1;
	}

	public function excluir_rotulo($rotulo_id)
	{
		$this->db->where("sro_id", $rotulo_id);
		$this->db->delete("simulados_rotulos");
	}

	public function get_rotulos_disciplina($simulado_id, $disciplina_id){

		$this->db->select("group_concat(DISTINCT sr.sro_nome ORDER BY sr.sro_nome ASC SEPARATOR '/') as rotulos");
		$this->db->from('simulados_rotulos sr');
		$this->db->join('simulados_questoes sq', 'sq.sro_id = sr.sro_id');
		$this->db->join('questoes que', 'que.que_id = sq.que_id');
		$this->db->where('sr.sim_id', $simulado_id);
		$this->db->where('que.dis_id', $disciplina_id);
		
		$query = $this->db->get();

		$result = $query->result_array();
		if($result){
			return $result[0]['rotulos'];
		}else{
			return null;
		}

	}

	public function is_disciplina_simulado_com_questao_anulada_desatualizada($simulado_id, $disciplina_id){

		$this->db->from('simulados_questoes sq');
		$this->db->join('questoes q', 'q.que_id = sq.que_id');
		$this->db->where('sq.sim_id', $simulado_id);
		$this->db->where('q.dis_id', $disciplina_id);
		$this->db->where_in('q.que_status', [CANCELADA, DESATUALIZADA]);

		return $this->db->count_all_results() > 0;
	}

	public function is_disciplina_simulado_com_questao_em_outro_simulado($simulado_id, $disciplina_id){
		
		$this->db->from('simulados_questoes sq_outro');
		$this->db->join('simulados s', 's.sim_id = sq_outro.sim_id');

		$this->db->join("simulados_questoes sq", "sq.que_id = sq_outro.que_id");
		$this->db->join('questoes q', 'q.que_id = sq.que_id');
		
		$this->db->where('sq.sim_id', $simulado_id);
		$this->db->where('q.dis_id', $disciplina_id);
		$this->db->where('sq_outro.sim_id !=', $simulado_id);
		$this->db->where('s.sim_status', ATIVO);

		return $this->db->count_all_results() > 0;

	}

}