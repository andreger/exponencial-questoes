<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ranking_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_by_id($id) 
	{
		$this->db->where('ran_id', $id);
		$query = $this->db->get('rankings');

		return $query->row_array();
	}

	public function listar_todos()
	{
		$this->db->from('rankings');
		$this->db->order_by('ran_nome');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function excluir($ranking_id)
	{
		$this->db->where('ran_id', $ranking_id);
		$this->db->delete('rankings');
	}

	/*****************************************************************
	 * Lista simulados que não foram associados a nenhum ranking
	 * ***************************************************************/

	public function listar_simulados_disponiveis()
	{
		$this->db->from('simulados');
		$this->db->where('sim_id NOT IN (SELECT sim_id FROM rankings_simulados)', null, TRUE);
		$this->db->where('sim_status != ', INATIVO);
		$this->db->order_by('sim_nome');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function listar_simulados($ranking_id)
	{
		$this->db->from('rankings_simulados rs');
		$this->db->join('simulados s', 's.sim_id = rs.sim_id');
		$this->db->where('ran_id', $ranking_id);
		$this->db->order_by('sim_nome');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function salvar_ranking($ranking)
	{
		$this->db->set('ran_nome', $ranking['ran_nome']);
		$this->db->set('ran_media_ponderada', $ranking['ran_media_ponderada']);

		if($id = $ranking['ran_id']) {
			$this->db->where('ran_id', $id);
			$this->db->update('rankings');
			return $id;
		}
		else {
			$this->db->insert('rankings');
			return $this->db->insert_id();
		}
	}

	public function alterar_corte_peso_erro_ranking($ranking_id, $corte, $peso_erros){
		$this->db->set('ran_corte', $corte?:null);
		$this->db->set('ran_peso_erros', $peso_erros);
		$this->db->where('ran_id', $ranking_id);
		$this->db->update('rankings');
	}

	public function remover_diferenca_disciplina_ranking_grupo($ranking_id, $grupo_id, $disciplinas_ids)
	{
		$this->db->where_not_in('dis_id', $disciplinas_ids);
		$this->db->where('ran_id', $ranking_id);
		$this->db->where('rag_id', $grupo_id);
		$this->db->delete('rankings_disciplinas');
	}

	public function salvar_disciplina_ranking($ranking_id, $grupo_id, $disciplina_id)
	{
		$this->db->set('ran_id', $ranking_id);
		$this->db->set('dis_id', $disciplina_id);
		$this->db->set('rag_id', $grupo_id);
		$this->db->insert('rankings_disciplinas');
	}

	public function get_disciplina_ranking($ranking_id, $grupo_id, $disciplina_id)
	{
		$this->db->where('ran_id', $ranking_id);
		$this->db->where('dis_id', $disciplina_id);
		$this->db->where('rag_id', $grupo_id);
		$query = $this->db->get('rankings_disciplinas');

		return $query->row_array();
	}

	public function get_disciplina_grupo($ranking_id, $disciplina_id)
	{
		$this->db->where('ran_id', $ranking_id);
		$this->db->where('dis_id', $disciplina_id);
		$query = $this->db->get('rankings_disciplinas');

		return $query->row_array();
	}

	public function listar_disciplinas_grupos($ranking_id)
	{
		$this->db->where('ran_id', $ranking_id);
		$query = $this->db->get('rankings_disciplinas');

		return $query->result_array();
	}

	public function salvar_ranking_simulados($ranking_id, $simulados_ids)
	{
		foreach ($simulados_ids as $sim_id) {

			if(!self::get_ranking_simulado($ranking_id, $sim_id)) {
				$this->db->set('ran_id', $ranking_id);
				$this->db->set('sim_id', $sim_id);
				$this->db->insert('rankings_simulados');
			}
		}
	}

	public function listar_rankings_disciplinas($ranking_id)
	{
		$this->db->where('ran_id', $ranking_id);
		$query = $this->db->get('rankings_disciplinas');

		return $query->result_array();
	}

	public function get_ranking_simulado($ranking_id, $simulado_id)
	{
		$this->db->where('ran_id', $ranking_id);
		$this->db->where('sim_id', $simulado_id);		
		$query = $this->db->get('rankings_simulados');

		return $query->row_array();
	}

	public function get_ranking_por_simulado($simulado_id)
	{
		$this->db->where('rs.sim_id', $simulado_id);
		$this->db->join('rankings r', 'r.ran_id = rs.ran_id');
		$this->db->from('rankings_simulados rs');
		$query = $this->db->get();

		return $query->row_array();
	}

	public function remover_diferenca_ranking_simulados($ranking_id, $simulados_ids)
	{
		$this->db->where_not_in('sim_id', $simulados_ids);
		$this->db->where('ran_id', $ranking_id);
		$this->db->delete('rankings_simulados');
	}

	public function salvar_grupo($grupo)
	{
		if($id = $grupo['rag_id']) {
			$this->db->where('rag_id', $id);
			$this->db->update('rankings_grupos', $grupo);
			return $id;
		}
		else {
			$this->db->insert('rankings_grupos', $grupo);
			return $this->db->insert_id();
		}
	}

	public function excluir_grupo($grupo_id)
	{
		$this->db->where('rag_id', $grupo_id);
		$this->db->delete('rankings_grupos');
	}

	public function get_grupo($grupo_id)
	{
		$this->db->where('rag_id', $grupo_id);
		$query = $this->db->get('rankings_grupos');

		return $query->row_array();
	}

	public function listar_grupos($ranking_id)
	{
		$this->db->from('rankings_grupos');
		$this->db->where('ran_id', $ranking_id);
		$this->db->order_by('rag_nome');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function listar_grupos_de_simulado($ranking_id, $simulado_id) {
		$sql =  "SELECT DISTINCT rg.* FROM rankings_grupos rg 
					JOIN rankings_disciplinas rd ON rg.rag_id = rd.rag_id 
					JOIN simulados_disciplinas sd ON rd.dis_id = sd.dis_id
					WHERE rg.ran_id = {$ranking_id} AND sd.sim_id = {$simulado_id}";

		$query = $this->db->query($sql);

		return $query->result_array();	
	}

	public function listar_disciplinas_por_ranking($ranking_id){
		
		$this->db->from('rankings_disciplinas rd');
		$this->db->join('disciplinas dis', 'rd.dis_id = dis.dis_id');
		$this->db->join('rankings_grupos rag', 'rag.rag_id = rd.rag_id', 'left');
		$this->db->where('rd.ran_id', $ranking_id);
		$this->db->order_by('dis.dis_nome');

		$query = $this->db->get();

		return $query->result_array();
	}

	public function listar_disciplinas_sem_grupo($ranking_id)
	{
		$this->db->from('rankings_disciplinas rd');
		$this->db->join('disciplinas d', 'rd.dis_id = d.dis_id');
		$this->db->where('ran_id', $ranking_id);
		$this->db->where('rag_id IS NULL', null, TRUE);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function listar_disciplinas_sem_grupo_de_simulado($ranking_id, $simulado_id)
	{
		$sql = "SELECT * FROM rankings_disciplinas rd 
				JOIN disciplinas d ON d.dis_id = rd.dis_id 
				JOIN simulados_disciplinas sd ON rd.dis_id = sd.dis_id
				WHERE rd.ran_id = {$ranking_id} AND sd.sim_id = {$simulado_id} AND rd.rag_id IS NULL";

		$query = $this->db->query($sql);

		return $query->result_array();	
	}

	public function listar_disciplinas_em_grupo($ranking_id, $grupo_id)
	{
		$this->db->from('rankings_disciplinas rd');
		$this->db->join('disciplinas d', 'rd.dis_id = d.dis_id');
		$this->db->where('ran_id', $ranking_id);
		$this->db->where('rag_id', $grupo_id);

		$query = $this->db->get();

		return $query->result_array();
	}

	public function atualizar_simulado_ranking($simulado_ranking)
	{
		$this->db->where('ran_id', $simulado_ranking['ran_id']);
		$this->db->where('sim_id', $simulado_ranking['sim_id']);
		$this->db->update('rankings_simulados', $simulado_ranking);
	}


	public function atualizar_grupo_ranking($grupo_ranking)
	{
		$this->db->where('ran_id', $grupo_ranking['ran_id']);
		$this->db->where('rag_id', $grupo_ranking['rag_id']);
		$this->db->update('rankings_grupos', $grupo_ranking);
	}

	public function atualizar_disciplina_ranking($disciplina_ranking)
	{
		$this->db->where('ran_id', $disciplina_ranking['ran_id']);
		$this->db->where('dis_id', $disciplina_ranking['dis_id']);
		$this->db->update('rankings_disciplinas', $disciplina_ranking);

		// echo $this->db->last_query();exit;
	}

	public function get_data_resolucao($ranking_id, $usuario_id) {
		$sql = "SELECT usu_id, max(sim_usu_data_resolucao) as data from simulados_usuarios su 
					join rankings_simulados rs on su.sim_id = rs.sim_id
					where ran_id = {$ranking_id} and usu_id = {$usuario_id}";
		
		$query = $this->db->query($sql);

		if($row = $query->row_array()) {
			return $row['data'];
		}
		return "";

	}

	/**
	 * Soma o máximo de pontos possível de um ranking
	 * 
	 * @since L1
	 * 
	 * @param int $ranking_id ID do ranking
	 * 
	 * @return float|int Total de pontos possível
	 * 
	 */
	public function get_maximo_pontos($ranking_id)
	{
		log_sq("DEBUG", "Ranking_model::get_maximo_pontos({$ranking_id})");

		$disciplinas_grupos = self::listar_disciplinas_grupos($ranking_id);

		log_sq("DEBUG", "Encontrados " . count($disciplinas_grupos) . " grupos para o ranking");

		$ranking = self::get_by_id($ranking_id);

		$total = 0;

		foreach($disciplinas_grupos as $disciplina_grupo)
		{
			//log_sq("DEBUG", print_r($disciplina_grupo, true));
			$qtd_questoes_disciplina = self::get_quantidade_questoes_disciplina($ranking_id, $disciplina_grupo['dis_id']);
			log_sq("DEBUG", "Quantidade de questões disciplina ({$disciplina_grupo['dis_id']}) = ({$qtd_questoes_disciplina})");

			if($rag_id = $disciplina_grupo['rag_id']) {
				// disciplina está associada a algum grupo

				$grupo = $this->ranking_model->get_grupo($rag_id);

				// grupo tem peso associado
				if($grupo['rag_peso'] > 1) {
					$nota = $grupo['rag_peso'] * $qtd_questoes_disciplina;
				}

				// disciplina tem peso associado
				elseif($disciplina_grupo['rad_peso'] > 1){
					$nota = $disciplina_grupo['rad_peso'] * $qtd_questoes_disciplina;
				}

				// grupo não tem peso associado, nesse caso usamos o peso do ranking
				else {
					$nota = ($ranking['ras_peso']?:1) * $qtd_questoes_disciplina;
				}

			}
			else {
				// disciplina não está associada a nenhum grupo
				if($disciplina_grupo['rad_peso'] > 1){
					$nota = $disciplina_grupo['rad_peso'] * $qtd_questoes_disciplina;
				}else{
					$nota = ($ranking['ras_peso']?:1) * $qtd_questoes_disciplina;				
				}
			}

			$total += $nota;
		}

		log_sq("DEBUG", "Total de pontos possíveis para o ranking {$ranking_id} = {$total}");

		return $total;
	}

	/**
	 * Retorna a quantidade de questões de uma disciplina de um ranking
	 * 
	 * @since L1
	 * 
	 * @param int $ranking_id ID do ranking
	 * @param int $disciplina_id ID da disciplina
	 * 
	 * @return int Quantidade de questões de uma disciplina em um ranking
	 */
	public function get_quantidade_questoes_disciplina($ranking_id, $disciplina_id)
	{
		$this->db->select_sum('sid.sid_qtde_questoes', 'sid_qtde_questoes');

		$this->db->from('rankings_simulados ras');
		$this->db->join('simulados_disciplinas sid', 'sid.sim_id = ras.sim_id');

		$this->db->where('sid.dis_id', $disciplina_id);
		$this->db->where('ras.ran_id', $ranking_id);

		$query = $this->db->get();

		$row = $query->row_array();
		
		return $row['sid_qtde_questoes'];

	}

}