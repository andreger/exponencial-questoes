<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Buscador_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_usuario_buscador_disponivel()
	{
		$this->db->where('bus_disponivel', 1);
		$query = $this->db->get('buscador_usuarios');
		
		return $query->row_array();
	}
	
	public function marcar_usuario_buscador_como_nao_disponivel($usuario_buscador_id)
	{
		$this->db->where('bus_id', $usuario_buscador_id);
		$this->db->update('buscador_usuarios', array(
			'bus_disponivel' => 0
		));
	}
	
	public function resetar_usuarios_buscador()
	{
		$this->db->update('buscador_usuarios', array(
				'bus_disponivel' => 1
		));
	}
	
	public function listar_todos()
	{
		$this->db->order_by('bus_disponivel desc');
		$this->db->order_by('bus_email asc');
		$query = $this->db->get('buscador_usuarios');
		return $query->result_array();
	}
	
	public function contar_disponiveis()
	{
		$this->db->where('bus_disponivel', 1);
		return $this->db->count_all_results('buscador_usuarios');
	}
}