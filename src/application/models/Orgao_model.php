<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orgao_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function listar_todas()
	{
		$this->db->from('orgaos');
		$this->db->order_by('org_nome');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_by_id($orgao_id)
	{
		$query = $this->db->get_where('orgaos', array('org_id' => $orgao_id));
		return $query->row_array();
	}
	
	public function get_by_nome($nome, $incluir_novo_registro = false)
	{
		$nome = trim($nome);
		$query = $this->db->get_where('orgaos', array('org_nome' => $nome));
	
		$result = $query->row_array();
	
		if(!$incluir_novo_registro)
			return $result;
	
		if(count($result) > 0)
			return $result;
	
		$data = array('org_nome' => $nome);
		self::salvar($data);
		return self::get_by_nome($nome);
	}

	public function salvar($orgao)
	{
		return $this->db->insert('orgaos', $orgao);
	}

	public function atualizar($orgao)
	{
		$this->db->where('org_id', $orgao['org_id']);
		$this->db->set($orgao);
		return $this->db->update('orgaos');
	}

	public function excluir($orgao_id)
	{
		$this->db->where('org_id', $orgao_id);
		return $this->db->delete('orgaos');
	}
}