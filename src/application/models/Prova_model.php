<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prova_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function listar_todas()
	{
		self::joins();
		$this->db->from('provas p');
		$this->db->order_by('pro_ano', 'desc');
		$this->db->order_by('pro_orgao_nome','asc');
		$this->db->order_by('pro_banca_nome', 'asc');
		$this->db->order_by('pro_nome', 'asc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function buscar($offset = 0, $filtros = null)
	{
		self::joins();
		
		$this->db->from('provas p');
		
		self::incluir_filtros($filtros);
		
		$this->db->order_by('pro_nome', 'asc');
		$this->db->limit(PROVAS_POR_PAGINA, $offset);
		
		$query = $this->db->get();

		return $query->result_array();
	}
	
	public function contar($filtros)
	{
		self::joins();

		$this->db->from('provas p');
	
		self::incluir_filtros($filtros);
		
		return $this->db->count_all_results();
	}

	private function incluir_filtros($filtros)
	{
		if($nome = $filtros['nome']) {
			$this->db->like('p.pro_nome', $nome);
		}

		if($anos = $filtros['anos']) {
			$this->db->where_in('p.pro_ano', $anos);
		}

		if($orgaos = $filtros['orgaos']) {
			$this->db->where_in('o.org_id', $orgaos);
		}

		if($bancas = $filtros['bancas']) {
			$this->db->where_in('b.ban_id', $bancas);
		}

		if($areas_atuacao = $filtros['areas_atuacao']) {
			$this->db->where_in('a.ara_id', $areas_atuacao);
		}
	}
	
	function get_cargos_prova($prova_id) {
		$this->db->select('c.car_id, c.car_nome');
		$this->db->from('cargos c');
		$this->db->where('pc.pro_id', $prova_id);
		$this->db->join('provas_cargos pc','pc.car_id=c.car_id');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function get_areas_formacao_prova($prova_id) {
		$this->db->select('a.arf_id, a.arf_nome');
		$this->db->from('areas_formacao a');
		$this->db->where('pa.pro_id', $prova_id);
		$this->db->join('provas_areas_formacao pa','pa.arf_id=a.arf_id');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	/**
	 *  Recupera as áreas de atuação de uma prova
	 *  
	 * @param int $prova_id ID da prova
	 * 
	 * @return array
	 */
	public function get_areas_atuacao_prova($prova_id) 
	{
		$this->db->select('a.ara_id, a.ara_nome');
		$this->db->from('areas_atuacao a');
		$this->db->where('pa.pro_id', $prova_id);
		$this->db->join('provas_areas_atuacao pa','pa.ara_id=a.ara_id');
		
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	/**
	 *  Recupera um texto com as áreas de atuação de uma prova
	 *
	 * @param int $prova_id ID da prova
	 *
	 * @return string
	 */
	public function get_areas_atuacao_prova_str($prova_id)
	{
	    $nomes = [];
	    
	    $areas = self::get_areas_atuacao_prova($prova_id);
	    
	    if($areas) {
    	    foreach ($areas as $area) {
    	        $nomes[] = $area['ara_nome'];
    	    }
    	    
    	    return implode(", ", $nomes);
	    }
	    
	    return "";
	}

	public function get_by_id($prova_id)
	{
		$query = $this->db->get_where('provas', array('pro_id' => $prova_id));
		return $query->row_array();
	}
	
	public function get_by_url($url)
	{
		$query = $this->db->get_where('provas', array('pro_qcon_url' => $url));
		return $query->row_array();
	}
	
	public function get($nome, $ano, $banca_id, $orgao_id, $qcon_url, $incluir_novo_registro = false)
	{
		$nome = trim($nome);
		$data = array('pro_nome' => $nome, 'pro_ano' => $ano, 'ban_id' => $banca_id, 'org_id' => $orgao_id);
	
	
		$query = $this->db->get_where('provas', $data);
		$result = $query->row_array();
	
		if(!$incluir_novo_registro)
			return $result;
	
		if(count($result) > 0)
			return $result;
	
		$data['pro_qcon_url'] = $qcon_url;
		self::salvar($data);
		return self::get($nome, $ano, $banca_id, $orgao_id, $qcon_url);
	}

	public function get_by_nome($nome)
	{
		$query = $this->db->get_where('provas', array('pro_nome' => $nome));
		return $query->row_array();
	}
	
	public function salvar($prova)
	{
		$cargos = $prova['car_ids'];
		$areas_formacao = $prova['arf_ids'];
		$areas_atuacao = $prova['ara_ids'];

		unset($prova['car_ids']);
		unset($prova['arf_ids']);
		unset($prova['ara_ids']);


		
		$this->db->insert('provas', $prova);
		$prova_id = $this->db->insert_id();

		if($cargos) {
			self::atualizar_cargos($prova_id, $cargos);
		}
		
		if($areas_formacao) {
			self::atualizar_areas_formacao($prova_id, $areas_formacao);
		}
		
		if($areas_atuacao) {
			self::atualizar_areas_atuacao($prova_id, $areas_atuacao);
		}
		
		self::gerar_nome_arquivo_prova( $prova_id );

		return $prova_id;
	}

	public function atualizar($prova)
	{
		$cargos = $prova['car_ids'];
		$areas_formacao = $prova['arf_ids'];
		$areas_atuacao = $prova['ara_ids'];
		unset($prova['car_ids']);
		unset($prova['arf_ids']);
		unset($prova['ara_ids']);
		$this->db->where('pro_id', $prova['pro_id']);
		$this->db->set($prova);
		$this->db->update('provas');
		self::atualizar_cargos($prova['pro_id'], $cargos);
		self::atualizar_areas_formacao($prova['pro_id'], $areas_formacao);
		self::atualizar_areas_atuacao($prova['pro_id'], $areas_atuacao);

		self::gerar_nome_arquivo_prova( $prova['pro_id'] );

		return $prova['pro_id'];
	}
	
	public function atualizar_cargos($prova_id, $cargos_selecionados) {
		$cargos_selecionados = array_diff($cargos_selecionados, [0]);

		$cargos_salvos = self::get_cargos_prova($prova_id);
		if (! empty ( $cargos_salvos )) {
			$cargos_ids = get_cargos_ids($cargos_salvos);
			foreach ( $cargos_selecionados as $cargo_selecionado ) {
				if (! in_array ( $cargo_selecionado, $cargos_ids )) {
					$this->db->set ( 'pro_id', $prova_id );
					$this->db->set ( 'car_id', $cargo_selecionado );
					$this->db->insert ( 'provas_cargos' );
				}
			}
			
			foreach ( $cargos_ids as $cargo_id ) {
				if (! in_array ( $cargo_id, $cargos_selecionados )) {
					$this->db->where ( 'pro_id', $prova_id );
					$this->db->where ( 'car_id', $cargo_id );
					$this->db->delete ( 'provas_cargos' );
				}
			}
		} else {
			foreach ( $cargos_selecionados as $cargo_selecionado ) {
				$this->db->set ( 'pro_id', $prova_id );
				$this->db->set ( 'car_id', $cargo_selecionado );
				$this->db->insert ( 'provas_cargos' );
			}
		}
	}
	
	public function atualizar_areas_formacao($prova_id, $areas_formacao_selecionadas) {

		$areas_formacao_selecionadas = array_diff($areas_formacao_selecionadas, [0]);

		$areas_formacao_salvas = self::get_areas_formacao_prova($prova_id);
		if (! empty ( $areas_formacao_salvas )) {
			$areas_formacao_ids = get_areas_formacao_ids($areas_formacao_salvas);
			foreach ( $areas_formacao_selecionadas as $area_formacao_selecionada ) {
				if (! in_array ( $area_formacao_selecionada, $areas_formacao_ids )) {
					$this->db->set ( 'pro_id', $prova_id );
					$this->db->set ( 'arf_id', $area_formacao_selecionada );
					$this->db->insert ( 'provas_areas_formacao' );
				}
			}
				
			foreach ( $areas_formacao_ids as $area_formacao_id ) {
				if (! in_array ( $area_formacao_id, $areas_formacao_selecionadas )) {
					$this->db->where ( 'pro_id', $prova_id );
					$this->db->where ( 'arf_id', $area_formacao_id );
					$this->db->delete ( 'provas_areas_formacao' );
				}
			}
		} else {
			foreach ( $areas_formacao_selecionadas as $area_formacao_selecionada ) {
				$this->db->set ( 'pro_id', $prova_id );
				$this->db->set ( 'arf_id', $area_formacao_selecionada );
				$this->db->insert ( 'provas_areas_formacao' );
			}
		}
	}
	
	public function atualizar_areas_atuacao($prova_id, $areas_atuacao_selecionadas) {
		
		$areas_atuacao_selecionadas = array_diff($areas_atuacao_selecionadas, [0]);

		$areas_atuacao_salvas = self::get_areas_atuacao_prova($prova_id);
		if (! empty ( $areas_atuacao_salvas )) {
			$areas_atuacao_ids = get_areas_atuacao_ids($areas_atuacao_salvas);
			foreach ( $areas_atuacao_selecionadas as $area_atuacao_selecionada ) {
				if (! in_array ( $area_atuacao_selecionada, $areas_atuacao_ids )) {
					$this->db->set ( 'pro_id', $prova_id );
					$this->db->set ( 'ara_id', $area_atuacao_selecionada );
					$this->db->insert ( 'provas_areas_atuacao' );
				}
			}
	
			foreach ( $areas_atuacao_ids as $area_atuacao_id ) {
				if (! in_array ( $area_atuacao_id, $areas_atuacao_selecionadas )) {
					$this->db->where ( 'pro_id', $prova_id );
					$this->db->where ( 'ara_id', $area_atuacao_id );
					$this->db->delete ( 'provas_areas_atuacao' );
				}
			}
		} else {
			foreach ( $areas_atuacao_selecionadas as $area_atuacao_selecionada ) {
				$this->db->set ( 'pro_id', $prova_id );
				$this->db->set ( 'ara_id', $area_atuacao_selecionada );
				$this->db->insert ( 'provas_areas_atuacao' );
			}
		}
	}
	
	public function has_prova_pdf($prova) {
		if(file_exists(get_prova_pdf_url( get_nome_arquivo_prova( "prova", $prova))))
			return true;
		
		return false;
	}
	
	public function has_gabarito_pdf($prova) {
		if(file_exists(get_gabarito_pdf_url( get_nome_arquivo_prova( "gabarito", $prova ))))
			return true;
	
		return false;
	}

	public function has_edital_pdf ( $prova ){
		if(file_exists(get_edital_pdf_url( get_nome_arquivo_prova( "edital", $prova )))){
			return true;
		}
		return false;
	}
	
	public function listar_anos() {
		$this->db->select('pro_ano');
		$this->db->distinct();
		$this->db->order_by('pro_ano', "desc");
		$query = $this->db->get('provas');
		return $query->result_array();
	}

	public function listar_provas_sem_escolaridade($limit = null)
	{
		$this->db->where('esc_id', 0);
		
		if($limit) {
			$this->db->limit($limit);
		}
		
		$query = $this->db->get('provas');


		return $query->result_array();
	}

	public function contar_provas_sem_escolaridade()
	{
		$this->db->where('esc_id', 0);
		$this->db->from('provas');

		return $this->db->count_all_results();
	}

	public function excluir($prova_id)
	{
		$this->db->where('pro_id', $prova_id);
		return $this->db->delete('provas');
	}
	
	public function atualizar_escolaridade($prova_id, $escolaridade_id)
	{
		$this->db->where('pro_id', $prova_id);
		$this->db->set('esc_id', $escolaridade_id);
		$this->db->update('provas');
	}

	public function atualizar_tipo($prova_id, $tipo_id)
	{
		$this->db->where('pro_id', $prova_id);
		$this->db->set('pro_tipo', $tipo_id);
		$this->db->update('provas');
	}
	
	public function adicionar_area_atuacao($prova_id, $area_id) 
	{
		if(!self::existe_prova_area_atuacao($prova_id, $area_id)) {
			
			$this->db->set('pro_id', $prova_id);
			$this->db->set('ara_id', $area_id);
			$this->db->insert('provas_areas_atuacao');
		}
	}
	
	public function existe_prova_area_atuacao($prova_id, $area_id)
	{
		$this->db->where('pro_id', $prova_id);
		$this->db->where('ara_id', $area_id);
		$this->db->from('provas_areas_atuacao');
		
		return $this->db->count_all_results() > 0 ? true : false;
	}

	public function excluir_prova_area_atuacao($prova_id, $area_id)
	{
		$this->db->where('pro_id', $prova_id);
		$this->db->where('ara_id', $area_id);
		$this->db->delete('provas_areas_atuacao');
	}
	
	public function adicionar_area_formacao($prova_id, $area_id)
	{
		if(!self::existe_prova_area_formacao($prova_id, $area_id)) {
				
			$this->db->set('pro_id', $prova_id);
			$this->db->set('arf_id', $area_id);
			$this->db->insert('provas_areas_formacao');
		}
	}
	
	public function existe_prova_area_formacao($prova_id, $area_id)
	{
		$this->db->where('pro_id', $prova_id);
		$this->db->where('arf_id', $area_id);
		$this->db->from('provas_areas_formacao');
	
		return $this->db->count_all_results() > 0 ? true : false;
	}

	/**********************************************
	 * Troca a banca de todas as provas
	 **********************************************/
	public function trocar_banca($banca_antiga, $banca_nova)
	{
		$this->db->where('ban_id', $banca_antiga);
		$this->db->update('provas', array('ban_id' => $banca_nova));
	}

	/**
	 * Gera e salva o prefixo do nome de um arquivo de determinada prova
	 * Formatos: 
	 * 		Orgao-Cargo-Ano-edital-ID.pdf
	 * 		Orgao-Cargo-Ano-gabarito-ID.pdf
	 * 		Orgao-Cargo-Ano-prova-ID.pdf
	 * 
	 * @since L2
	 * 
	 * @param int $prova_id da prova
	 * 
	 * @return string O prefixo do arquivo para a prova
	 */
	function gerar_nome_arquivo_prova( $prova_id )
	{
		$this->load->helper("security");

		$prova = self::get_by_id( $prova_id );

		if(!class_exists('orgao_model'))
		{
			$this->load->model('orgao_model');
		}

		$orgao = $this->orgao_model->get_by_id ( $prova[ 'org_id' ] );

		$prefixo = (trim($orgao['org_nome'])?:"SemOrgao") . "-";

		$cargos = self::get_cargos_prova ( $prova['pro_id'] );
		
		if ( !empty( $cargos ) )
		{
			foreach( $cargos as $cargo )
			{
				$prefixo .= trim( $cargo['car_nome'] ) . "-";
			}
		}
		else
		{
			$prefixo .= "SemCargo-";
		}

		$prefixo .= ($prova['pro_ano']?:"SemAno") . "-";
		$prefixo .= "(tipo)-";

		$nome_arquivo = $prefixo . $prova['pro_id'] . ".pdf";

		$nome_arquivo = sanitize_filename( $nome_arquivo );

		$nome_arquivo = preg_replace('/\s+/', '_', $nome_arquivo);

		log_debug("INFO", "Novo: {$nome_arquivo} Antigo: {$prova['pro_prefixo_nome_arquivo']}");

		$needs_rename = FALSE;

		if ( $nome_arquivo != $prova['pro_prefixo_nome_arquivo'] )
		{
			$this->db->where( "pro_id", $prova['pro_id'] );
			$this->db->set("pro_prefixo_nome_arquivo", $nome_arquivo);
			$this->db->update("provas");

			$needs_rename = TRUE;
		}

		if ( $needs_rename == TRUE ) {

			$old_nome_gabarito = get_arquivo_gabarito( $prova );//Assim recupera o nome antigo sem forçar recarregamento
			$new_nome_gabarito = get_arquivo_gabarito( $prova_id );//Assim força o recarregamento dos dados
			$result = rename( $old_nome_gabarito, $new_nome_gabarito );
			log_sq("INFO", "Renomear {$old_nome_gabarito} para {$new_nome_gabarito}: " . ( $result?"OK":"NOT OK" ) );

			$old_nome_edital = get_arquivo_edital( $prova );
			$new_nome_edital = get_arquivo_edital( $prova_id );
			$result = rename( $old_nome_edital, $new_nome_edital );
			log_sq("INFO", "Renomear {$old_nome_edital} para {$new_nome_edital}: " . ( $result?"OK":"NOT OK" ) );

			$old_nome_prova = get_arquivo_prova( $prova );
			$new_nome_prova = get_arquivo_prova( $prova_id );
			$result = rename( $old_nome_prova, $new_nome_prova );
			log_sq("INFO", "Renomear {$old_nome_prova} para {$new_nome_prova}: " . ( $result?"OK":"NOT OK" ) );
		}

		return $nome_arquivo;

	}
	
	private function joins()
	{
		$this->db->select('p.*, b.ban_nome as pro_banca_nome, o.org_nome as pro_orgao_nome');
		$this->db->join('bancas b', 'p.ban_id = b.ban_id', 'left');
		$this->db->join('orgaos o', 'p.org_id = o.org_id', 'left');
		$this->db->join('provas_areas_atuacao a', 'p.pro_id = a.pro_id', 'left');
		$this->db->distinct();
	}
}