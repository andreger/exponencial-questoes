<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Caderno_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		//$this->output->enable_profiler(TRUE);
	}

	public function adicionar_caderno($data)
	{
		$this->db->insert('cadernos', $data);
		return $this->db->insert_id();
	}
	
	public function atualizar_caderno($data)
	{

		$this->db->trans_start();

		if(!$data['cad_coaching'])
		{
			//Remove a referência ao caderno gerador, no caso de cadernos de coaching
			$this->db->set('cad_ref_id', NULL);
			$this->db->where('cad_ref_id', $data['cad_id']);
			$this->db->where('cad_compartilhado', 1);
			$this->db->where('cad_coaching', 1);
			$this->db->update('cadernos');
		}

		$this->db->where('cad_id', $data['cad_id']);
		$this->db->set('cad_nome', $data['cad_nome']);
		$this->db->set('cad_comercializavel', $data['cad_comercializavel']);
		$this->db->set('cad_coaching', $data['cad_coaching']);

		$result =  $this->db->update('cadernos');

		$this->db->trans_complete();

		return $result;
	}
	
	public function atualizar_categoria($data)
	{
		$this->db->where('cat_id', $data['cat_id']);
		$this->db->set('cat_nome', $data['cat_nome']);
		$this->db->update('categorias');
	}
	
	public function atualizar_categoria_produto($data)
	{
		$this->db->where('ccp_id', $data['ccp_id']);
		$this->db->set('ccp_nome', $data['ccp_nome']);
		$this->db->update('categorias_cp');
	}

	/**
	 * Atualiza os totais de questões, acertos e erro de um caderno
	 * 
	 * @since L1
	 * 
	 * @param $caderno_id ID do caderno
	 * @param $usuario_id ID do usuário dono do caderno
	 * @param $incluir_inativas Flag indicando se deve-se incluir as questões inativas na contagem
	 */
	public function atualizar_totais($caderno_id, $usuario_id = null, $incluir_inativas = null){

		if(is_null($usuario_id) || is_null($incluir_inativas))
		{
			$caderno = self::get_caderno($caderno_id);
			$usuario_id = $caderno['usu_id'];
			$incluir_inativas = is_administrador($caderno['usu_id']) || is_coordenador_sq($caderno['usu_id']) || is_professor($caderno['usu_id']);
		}

		$questoes_certas = self::get_total_resolvidas($caderno_id, $usuario_id, CERTO, $incluir_inativas);
		$questoes_erradas = self::get_total_resolvidas($caderno_id, $usuario_id, ERRADO, $incluir_inativas);
		$total_questoes = self::get_total_questoes($caderno_id, $incluir_inativas);

		$this->db->where('cad_id', $caderno_id);
		
		$this->db->set('cad_qtd_acertos', $questoes_certas);
		$this->db->set('cad_qtd_erros', $questoes_erradas);
		$this->db->set('cad_qtd_questoes_usuario', $total_questoes);

		$this->db->update('cadernos');
	}
	
	public function contar_cadernos($usuario_id, $categoria_id = null, $filtro = null)
    {
        // somente busca para usuários logados
	    if($usuario_id) {
            self::busca_para_cadernos(TRUE, $usuario_id, $categoria_id, $filtro);
            $this->db->from('cadernos c');
            return $this->db->count_all_results();
        }

	    return 0;
	}

	/**
	 * Lista os assuntos das questões de um caderno
	 * 
	 * @since L1
	 * 
	 * @param int $caderno_id Id do caderno
	 * @param bool $incluir_inativas Somente questões ativas ou todas
	 * 
	 * @return array Array associativo com colunas: ass_id, ass_nome e ass_pai_id
	 */

	public function buscar_assuntos($caderno_id, $incluir_inativas = false)
	{
	    $join_ativas =!$incluir_inativas ? " JOIN questoes q ON q.que_id = cq.que_id " : "";
	    $where_ativas = !$incluir_inativas ? " AND que_ativo = 1 " : "";

		$sql = "SELECT DISTINCT a.ass_id, a.ass_nome, a.ass_pai_id, a.dis_id 
                FROM cadernos_questoes cq 
                JOIN questoes_assuntos qa ON cq.que_id = qa.que_id 
                {$join_ativas}
                JOIN assuntos a ON a.ass_id = qa.ass_id 
                WHERE cad_id = {$caderno_id} {$where_ativas}
                ";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	/**
	 * Conta o número de questões de um caderno agrupado por assuntos
	 * 
	 * @since K5
	 * 
	 * @param int $caderno_id Id do caderno
	 * @param array $ass_ids IDs dos assuntos a serem incluídos (a princípio um assunto e seus filhos)
	 * @param bool $incluir_inativas Somente questões ativas ou todas
	 * 
	 * @return int quantidade de questões pertecentes ao caderno que estão incluídas em algum dos assuntos informados
	 */

	public function contar_questoes_por_assuntos($caderno_id, $ass_ids, $incluir_inativas = false)
	{
	    $join_ativas =!$incluir_inativas ? " JOIN questoes q ON q.que_id = cq.que_id " : "";
	    $where_ativas = !$incluir_inativas ? " AND que_ativo = 1 " : "";
		$ass_ids_str = implode(", ", $ass_ids);

		$sql = "SELECT count(distinct cq.que_id) AS qtde 
                FROM cadernos_questoes cq 
                JOIN questoes_assuntos qa ON cq.que_id = qa.que_id 
                {$join_ativas}
                JOIN assuntos a ON a.ass_id = qa.ass_id 
                WHERE cad_id = {$caderno_id} {$where_ativas}
				AND a.ass_id IN ($ass_ids_str) 
                ";

		//echo $sql;
		$query = $this->db->query($sql);

		return $query->row_array()['qtde'];
	}

	/**
	 * Conta o número de questões de um caderno agrupado por disciplinas
	 * 
	 * @since K5
	 * 
	 * @param int $caderno_id Id do caderno
	 * @param bool $incluir_inativas Somente questões ativas ou todas
	 * 
	 * @return array Array associativo com colunas: dis_id, dis_nome e qtde
	 */

	public function contar_questoes_por_disciplinas($caderno_id, $incluir_inativas = false)
	{
	    $where_ativas = !$incluir_inativas ? " AND que_ativo = 1 " : "";

		$sql = "SELECT d.dis_id, d.dis_nome, count(*) AS qtde 
				FROM cadernos_questoes cq 
				JOIN questoes q ON cq.que_id = q.que_id 
				JOIN disciplinas d ON q.dis_id = d.dis_id 
				WHERE cad_id = {$caderno_id} {$where_ativas} 
				GROUP BY d.dis_id ORDER BY d.dis_nome ASC";

		$query = $this->db->query($sql);

		return $query->result_array();
	}

	/**
	 * Conta o número de questões de um caderno agrupado por disciplinas
	 * 
	 * @since K5
	 * 
	 * @param int $caderno_id Id do caderno
	 * @param bool $incluir_inativas Somente questões ativas ou todas
	 * 
	 * @return int Total de questões do caderno
	 */

	public function contar_questoes($caderno_id, $incluir_inativas = false)
	{
	    if(!$incluir_inativas) {
			$this->db->join('questoes q', 'cq.que_id = q.que_id');
			$this->db->where('que_ativo', SIM);		
		}

		$this->db->where('cad_id', $caderno_id);
		$this->db->from('cadernos_questoes cq');

		return $this->db->count_all_results(); 
	}

	private function busca_para_cadernos($is_contar, $usuario_id, $categoria_id = null, $filtro = null)
    {
		if(!is_null($categoria_id) && $categoria_id > 0) {
			$ids = self::listar_cadernos_ids_por_categoria($categoria_id);
			
			if(count($ids) == 0) {
				$ids[0] = 0; 
			}
			
			$this->db->where_in('c.cad_id', $ids);
		}
		else {			
			if(tem_acesso(array(ADMINISTRADOR))) {
				$this->db->where('(c.usu_id = '.$usuario_id.' or c.cad_comercializavel = 1) and cad_ref_id is null');
			}else{
				$this->db->where('c.usu_id', $usuario_id);
			}
		}
		
		if(!is_null($filtro)){

			if($filtro['filtro_nome']){
				$this->db->like('upper(c.cad_nome)', strtoupper($filtro['filtro_nome']));
			}
			
			if($filtro['filtro_legenda']){

				switch($filtro['filtro_legenda']){

					case FILTRO_LEGENDA_CRIADOS:{
						$this->db->where("c.usu_id", $usuario_id);
						$this->db->where("c.usu_designador_id is null");
						$this->db->where("c.cad_comprado", FALSE);
						$this->db->where("c.cad_compartilhado", FALSE);
						break;
					}
					case FILTRO_LEGENDA_COMPARTILHADOS:{
						$this->db->where("c.cad_compartilhado", TRUE);
						break;
					}
					case FILTRO_LEGENDA_DESIGNADOS:{
						$this->db->where("c.usu_designador_id is not null");
						$this->db->join(DB_NAME . ".wp_usermeta um", "um.user_id = c.usu_designador_id");
						$this->db->where("um.meta_key", "wp_capabilities");
						$this->db->like("um.meta_value", "administrator");
						break;
					}
					case FILTRO_LEGENDA_COMPRADOS:{
						$this->db->where("c.cad_comprado", TRUE);
						break;
					}

				}

			}

			if(!$is_contar && $filtro['filtro_ordenacao']){

				switch($filtro['filtro_ordenacao']){

					case MEUS_CADERNOS_MAIS_RECENTE:{
						$this->db->order_by('c.cad_data_criacao', 'DESC');
						break;
					}
					case MEUS_CADERNOS_MENOS_RECENTE:{
						$this->db->order_by('c.cad_data_criacao', 'ASC');
						break;
					}
					case MEUS_CADERNOS_ACESSO_MAIS_RECENTE:{
						$this->db->order_by('c.cad_ultimo_acesso ', 'DESC');
						break;
					}
					case MEUS_CADERNOS_ACESSO_MENOS_RECENTE:{
						$this->db->order_by('c.cad_ultimo_acesso ', 'ASC');
						break;
					}
					case MEUS_CADERNOS_AZ:{
						$this->db->order_by('c.cad_nome', 'ASC');
						break;
					}
					case MEUS_CADERNOS_ZA:{
						$this->db->order_by('c.cad_nome', 'DESC');
						break;
					}
					case MEUS_CADERNOS_MAIS_ACERTOS:{
						$this->db->order_by('(c.cad_qtd_acertos/c.cad_qtd_questoes_usuario)', 'DESC');
						break;
					}
					case MEUS_CADERNOS_MENOS_ACERTOS:{
						$this->db->order_by('(c.cad_qtd_acertos/c.cad_qtd_questoes_usuario)', 'ASC');
						break;
					}
					case MEUS_CADERNOS_MAIS_ERROS:{
						$this->db->order_by('(c.cad_qtd_erros/c.cad_qtd_questoes_usuario)', 'DESC');
						break;
					}
					case MEUS_CADERNOS_MENOS_ERROS:{
						$this->db->order_by('(c.cad_qtd_erros/c.cad_qtd_questoes_usuario)', 'ASC');
						break;
					}

				}

			}elseif(!$is_contar){
				$this->db->order_by('c.cad_nome', 'ASC');
			}

		}

		/** 
		 * Cadernos gerados através das aulas de um produto e cadernos demo não são listados, são acessados somente via página do aluno
		 *	@see U389 - Cadernos associados a Produtos do WP
		*/
		$this->db->where('cad_demo', FALSE);

		if(!tem_acesso([ADMINISTRADOR])) {
            $this->db->where("not exists(select 1 from exponenc_corp.cadernos_pedidos cpe where cpe.cad_id = c.cad_id)", NULL, FALSE);
        }
	}

	public function listar_cadernos($usuario_id, $categoria_id = null, $offset = null, $max_por_pagina = null, $filtro = null)
	{
	    if($usuario_id) {
            self::busca_para_cadernos(FALSE, $usuario_id, $categoria_id, $filtro);

            if ($offset >= 0) {
                $this->db->limit($max_por_pagina, $offset);
            }

            $this->db->order_by('cad_nome', 'asc');
            $query = $this->db->get('cadernos c');

            return $query->result_array();
        }
	    else {
	        return [];
        }
	}

	public function contar($usuario_id, $filtro = null){
		//$this->output->enable_profiler(TRUE);
		self::filtro_para_cadernos(TRUE, $usuario_id, $filtro);
		$this->db->from('cadernos c');
		return $this->db->count_all_results();
		
	}

	private function filtro_para_cadernos($is_contar, $usuario_id, $filtro){
		
		$this->db->join("cadernos_produtos cp", "c.cad_id = cp.cad_id");
		
		if($filtro['filtro_nome']){
			$this->db->like('upper(c.cad_nome)', strtoupper($filtro['filtro_nome']));
		}
		
		$listar_por = $filtro['filtro_busca'];
		
		if(is_null($listar_por)){
			$listar_por = TODAS;
		}

		$this->db->where('c.cad_comercializavel', TRUE);

		switch($listar_por){
			case QUE_ADQUIRI:{
				/*$exists = "EXISTS(SELECT DISTINCT(pm.meta_value) as user_id 
				FROM  exponenc_db.wp_woocommerce_order_itemmeta om 
					JOIN exponenc_db.wp_woocommerce_order_items oi ON om.order_item_id = oi.order_item_id 
					JOIN exponenc_db.wp_postmeta pm ON pm.post_id = oi.order_id 
					JOIN exponenc_db.wp_posts p ON pm.post_id = p.ID
				WHERE p.post_status LIKE 'wc-completed' 
					AND om.meta_key LIKE  '_product_id' 
					AND om.meta_value LIKE cp.produto_id 
					AND pm.meta_key LIKE '_customer_user' 
					AND pm.meta_value = '$usuario_id'
				)";*/

				$exists = "EXISTS(SELECT 1 FROM cadernos c_comp where c_comp.usu_id = '$usuario_id' and c_comp.cad_ref_id = c.cad_id)";

				$this->db->where($exists);
				
				break;
			}
			case QUE_NAO_ADQUIRI:{
				$this->db->where('c.usu_id !=', $usuario_id);
				$this->db->where('wp.meta_value > 0');
				break;
			}
			case GRATUITOS:{
				$this->db->where('c.usu_id !=', $usuario_id);
				$this->db->where('wp.meta_value', '0');
				break;
			}
			case TODAS:{
				//$this->db->where('(c.usu_id = '.$usuario_id.' or c.cad_comercializavel = 1)');
				break;
			}
			case ROTA_EXPONENCIAL:{
				
				KLoader::helper("CategoriaHelper");
				
				$cat_rota_exponencial_id = CategoriaHelper::categoria_constante_id(CATEGORIA_SLUG_ROTA_EXPONENCIAL);

				$this->db->join('exponenc_db.wp_term_relationships tr', 'tr.object_id = cp.produto_id');
				$this->db->join('exponenc_db.wp_term_taxonomy tt', 'tr.term_taxonomy_id = tt.term_taxonomy_id');
                $this->db->where('tt.term_id', $cat_rota_exponencial_id);
				break;
			}
		}

		if($is_contar == FALSE){

			$ordenar_por = $filtro['filtro_ordem'];
			if(is_null($ordenar_por)){
				$ordenar_por = MAIS_NOVOS;
			}

			switch ($ordenar_por) {

				case MAIS_NOVOS:{
					$order_by = 'c.cad_id desc';
					break;		
				}
				case MAIS_ANTIGOS:{
					$order_by = 'c.cad_id asc';
					break;
				}
				case MENOR_PRECO:{
					$order_by = 'wp.meta_value asc';
					break;
				}
				case MAIOR_PRECO:{
					$order_by = 'wp.meta_value desc';
					break;
				}
				case NOME_ASC:{
					$order_by = 'upper(c.cad_nome) asc';
					break;
				}
			}

			$this->db->order_by($order_by);

		}

		//Faz os joins de acordo com a necessidade
		if($listar_por == QUE_NAO_ADQUIRI || $listar_por == GRATUITOS || $ordenar_por == MAIOR_PRECO || $ordenar_por == MENOR_PRECO){
			$this->db->join(DB_NAME . ".wp_postmeta wp", "cp.produto_id = wp.post_id and wp.meta_key = '_price'", "left");
		}

		/** 
		 * Cadernos gerados através das aulas de um produto e cadernos demo não são listados, são acessados somente via página do aluno
		 *	@see U389 - Cadernos associados a Produtos do WP
		*/
		$this->db->where('c.cad_demo', FALSE);
		$this->db->where("not exists(select 1 from exponenc_corp.cadernos_pedidos cpe where cpe.cad_id = c.cad_id)", NULL, FALSE);

	}

	public function listar($usuario_id, $offset = null, $max_por_pagina = null, $filtro){
		//$this->output->enable_profiler(TRUE);
		self::filtro_para_cadernos(FALSE, $usuario_id, $filtro);

		$this->db->join('cadernos_questoes cq', 'cq.cad_id = c.cad_id');
		$this->db->join('questoes q', 'q.que_id = cq.que_id');
		$this->db->where('q.que_ativo', ATIVO);
		
		$tem_postmeta = ($filtro['filtro_ordem'] == MAIOR_PRECO || $filtro['filtro_ordem'] == MENOR_PRECO);

//		$group_by = array('c.cad_id', 'c.cad_nome', 'c.usu_id', 'c.usu_designador_id', 'c.cad_comercializavel', 'c.cad_compartilhado', 'c.cad_comprado', 'c.cad_ref_id', 'cp.produto_id');
		$select = 'c.cad_id, c.cad_nome, c.usu_id, c.usu_designador_id, c.cad_comercializavel, c.cad_compartilhado, c.cad_comprado, c.cad_ref_id, cp.produto_id as produto_id, count(*) as total_questoes';
		
		if($tem_postmeta){
//			array_push($group_by, 'wp.meta_value');
			$select .= ', wp.meta_value';
		}

		$this->db->group_by('c.cad_id');

		if($offset >= 0){
			$this->db->limit($max_por_pagina, $offset);
		}

		$this->db->from('cadernos c');
		$this->db->select($select);

		$query = $this->db->get();

		return $query->result_array();

	}

	public function is_comprou_caderno($cad_ref_id, $usuario_id){
		$this->db->from('cadernos c');
		$this->db->where('c.cad_comprado', TRUE);
		$this->db->where('c.cad_ref_id', $cad_ref_id);
		$this->db->where('c.usu_id', $usuario_id);
		return $this->db->count_all_results() > 0 ? true : false;
	}

	public function listar_todos()
	{
		$query = $this->db->get('cadernos');

		return $query->result_array();
	}
	
	public function listar_cadernos_ids_por_categoria($categoria_id)
	{
		$this->db->select('cad_id');
		$this->db->where('cat_id', $categoria_id);
		$query = $this->db->get('cadernos_categorias');
	
		$ids = array();
		foreach ($query->result_array() as $item) {
			array_push($ids, $item['cad_id']);
		}
		
		return $ids;
	}

	public function listar_cadernos_referenciados($caderno_id, $excluir_coaching = TRUE) 
	{

		if($excluir_coaching == TRUE)
		{
			$this->db->where('cad_coaching', '0');
		}

		$this->db->where('cad_ref_id', $caderno_id);
		$query = $this->db->get('cadernos');

		return $query->result_array();
	}
	
	public function adicionar_questao_em_caderno($caderno_id, $questao_id)
	{
		if(!self::existe_questao_em_caderno($caderno_id, $questao_id)) {
			$ordem = self::get_proximo_indice_ordem($caderno_id);

			$item = array(
					'cad_id' => $caderno_id,
					'que_id' => $questao_id,
					'caq_ordem' => $ordem
			);
	
			$this->db->insert('cadernos_questoes', $item);

			//Altera a quantidade de questões do caderno
			self::atualizar_totais($caderno_id);
		}

		// atualiza cadernos referenciados
		$refs = self::listar_cadernos_referenciados($caderno_id);

		if($refs) {
			foreach ($refs as $ref) {
				self::adicionar_questao_em_caderno($ref['cad_id'], $questao_id);
			}
		}
	}	
	
	public function adicionar_questoes_ids_em_caderno($caderno_id, $questoes_ids){
		
		$questoes = array();

		foreach($questoes_ids as $questao_id){

			array_push($questoes, ['que_id' => $questao_id]);

		}

		self::adicionar_questoes_em_caderno($caderno_id, $questoes);

	}

	public function adicionar_questoes_em_caderno($caderno_id, $questoes, $limite = MAX_QUESTOES_ADICIONADAS_CADERNO)
	{
		$questoes_caderno = array();
	
		$i = 0;

		$ordem = self::get_proximo_indice_ordem($caderno_id);

		foreach ($questoes as $questao) {
			set_time_limit(30);
			
			if($i >= $limite) break; 

			if(self::existe_questao_em_caderno($caderno_id, $questao['que_id'])) {
				continue;
			}

			$item = array(
					'cad_id' => $caderno_id,
					'que_id' => $questao['que_id'],
					'caq_ordem' => $ordem + $i
			);

			array_push($questoes_caderno, $item);
			$i++;
		}
		
		if(count($questoes_caderno) > 0) {
			$this->db->insert_batch('cadernos_questoes', $questoes_caderno);
		}

		//Altera a quantidade de questões do caderno
		self::atualizar_totais($caderno_id);

		// atualiza cadernos referenciados
		$refs = self::listar_cadernos_referenciados($caderno_id);
		
		if($refs) {
			foreach ($refs as $ref) {
				self::adicionar_questoes_em_caderno($ref['cad_id'], $questoes, $limite);
			}
		}
	}
	
	public function existe_questao_em_caderno($caderno_id, $questao_id)
	{
		$this->db->where('cad_id', $caderno_id);
		$this->db->where('que_id', $questao_id);
		$this->db->from('cadernos_questoes');
	
		return $this->db->count_all_results() > 0 ? true : false;
	}
	
	public function get_caderno($caderno_id)
	{
		$this->db->where('c.cad_id', $caderno_id);
		$query = $this->db->get('cadernos c');
	
		return $query->row_array();
	}

	public function get_caderno_pedido($caderno_id)
	{
		$this->db->where('cpe.cad_id', $caderno_id);
		$query = $this->db->get('cadernos_pedidos cpe');
	
		return $query->row_array();
	}
	
	public function excluir_caderno($caderno_id)
	{

		$this->db->trans_start();

		//Remove a referência ao caderno gerador, no caso de cadernos de coaching
		//para que não sejam excluídos no cascade
		$this->db->set('cad_ref_id', NULL);
		$this->db->where('cad_ref_id', $caderno_id);
		$this->db->where('cad_compartilhado', 1);
		$this->db->where('cad_coaching', 1);
		$this->db->update('cadernos');

		//Apaga o caderno
		$this->db->where('cad_id', $caderno_id);
		$this->db->delete('cadernos');

		$this->db->trans_complete();
	}

	public function excluir_caderno_compartilhado($caderno_id, $usuario_id)
	{
		$this->db->where('cad_id', $caderno_id);
		$this->db->where('usu_id', $usuario_id);
		$this->db->delete('cadernos_compartilhados');
	}
	
	public function excluir_categoria($categoria_id)
	{
		$this->db->where('cat_id', $categoria_id);
		$this->db->delete('categorias');
	}
	
	public function excluir_categoria_produto($categoria_id)
	{
		$this->db->where('ccp_id', $categoria_id);
		$this->db->delete('categorias_cp');
	}
		
	public function listar_categorias($usuario_id)
	{
		$this->db->where('usu_id', $usuario_id);
		$this->db->order_by('cat_nome', 'asc');
		$query = $this->db->get('categorias');
		
		return $query->result_array();
	}
	
	public function get_categoria($categoria_id)
	{
		$this->db->where('cat_id', $categoria_id);
		$query = $this->db->get('categorias');
	
		return $query->row_array();
	}
	
	public function get_categoria_produto($categoria_id)
	{
		$this->db->where('ccp_id', $categoria_id);
		$query = $this->db->get('categorias_cp');
	
		return $query->row_array();
	}
	
	public function adicionar_categoria($data)
	{
		$this->db->insert('categorias', $data);
		return $this->db->insert_id();
	}
	
	public function adicionar_categoria_cp($data)
	{
		$this->db->insert('categorias_cp', $data);
		return $this->db->insert_id();
	}
	
	public function adicionar_categorias_em_caderno($caderno_id, $categorias_ids, $excluir_atuais = TRUE)
	{
		if($excluir_atuais) {
			self::excluir_todas_categorias_de_caderno($caderno_id);
		}
		
		$categorias_caderno = array();
	
		if($categorias_ids) {
			foreach ($categorias_ids as $categoria_id) {
				if(self::existe_categoria_em_caderno($caderno_id, $categoria_id)) {
					continue;
				}
					
				$item = array(
						'cad_id' => $caderno_id,
						'cat_id' => $categoria_id
				);
					
				array_push($categorias_caderno, $item);
			}
		}
		
		if(count($categorias_caderno) > 0) {
			$this->db->insert_batch('cadernos_categorias', $categorias_caderno);
		}
		
	}

	public function adicionar_categoria_em_caderno($caderno_id, $categoria_id)
	{

		if(!self::existe_categoria_em_caderno($caderno_id, $categoria_id)) {

			$item = array(
					'cad_id' => $caderno_id,
					'cat_id' => $categoria_id
			);
				
			$this->db->insert('cadernos_categorias', $item);
		}
		
	}
	
	public function adicionar_categorias_cp_em_produto($produto_id, $categorias_ids)
	{
		self::excluir_todas_categorias_cp_de_produto($produto_id);
	
		$categorias_caderno_produto = array();
	
		foreach ($categorias_ids as $categoria_id) {
			if(self::existe_categoria_em_produto($produto_id, $categoria_id)) {
				continue;
			}
	
			$item = array(
					'cpr_id' => $produto_id,
					'ccp_id' => $categoria_id
			);
	
			array_push($categorias_caderno_produto, $item);
		}
	
		if(count($categorias_caderno_produto) > 0) {
			$this->db->insert_batch('cadernos_produtos_categorias', $categorias_caderno_produto);
		}
	
	}
	
	public function existe_categoria_em_caderno($caderno_id, $categoria_id)
	{
		$this->db->where('cad_id', $caderno_id);
		$this->db->where('cat_id', $categoria_id);
		$query = $this->db->get('cadernos_categorias');
		$result = $query->result_array();
	
		return count($result) > 0 ? true : false;
	}
	
	public function existe_categoria_em_produto($produto_id, $categoria_id)
	{
		$this->db->where('cpr_id', $produto_id);
		$this->db->where('ccp_id', $categoria_id);
		$query = $this->db->get('cadernos_produtos_categorias');
		$result = $query->result_array();
	
		return count($result) > 0 ? true : false;
	}
	
	public function existe_categoria_nome_em_caderno($caderno_id, $categoria_nome)
	{
		$this->db->from('cadernos_categorias cc');
		$this->db->join('categorias c', 'c.cat_id = cc.cat_id');
		$this->db->where('cc.cad_id', $caderno_id);
		$this->db->where('c.cat_nome', $categoria_nome);
		$query = $this->db->get();
		$result = $query->result_array();
	
		return count($result) > 0 ? true : false;
	}
	
	public function existe_categoria_em_usuario($usu_id, $categoria_nome)
	{
		$this->db->select('cat_id');
		$this->db->where('cat_nome', $categoria_nome);
		$this->db->where('usu_id', $usu_id);
		
		$query = $this->db->get('categorias');
	
		return $query->num_rows() > 0 ? $query->row()->cat_id : false;
	}
	
	public function listar_categorias_ids_de_caderno($caderno_id)
	{
		$this->db->where('cad_id', $caderno_id);
		$query = $this->db->get('cadernos_categorias');
		
		return $query->result_array();
	}
	
	public function listar_categorias_ids_de_produto($produto_id)
	{
		$this->db->where('cpr_id', $produto_id);
		$query = $this->db->get('cadernos_produtos_categorias');
	
		return $query->result_array();
	}
	
	public function excluir_todas_categorias_de_caderno($caderno_id)
	{
		$this->db->where('cad_id', $caderno_id);
		$this->db->delete('cadernos_categorias');
	}
	
	public function excluir_todas_categorias_cp_de_produto($produto_id)
	{
		$this->db->where('cpr_id', $produto_id);
		$this->db->delete('cadernos_produtos_categorias');
	}	
	
	public function excluir_questao_de_caderno($caderno_id, $questao_id)
	{
		$this->db->where('cad_id', $caderno_id);
		$this->db->where('que_id', $questao_id);
		$this->db->delete('cadernos_questoes');

		//Altera a quantidade de questões do caderno
		self::atualizar_totais($caderno_id);

		// atualiza cadernos referenciados
		$refs = self::listar_cadernos_referenciados($caderno_id);

		if($refs) {
			foreach ($refs as $ref) {
				self::excluir_questao_de_caderno($ref['cad_id'], $questao_id);
			}
		}
	}
	
	/**
	 * Remove as questões informadas (ou todas) de um determinado caderno e seus referenciados
	 * 
	 * @since L1
	 * 
	 * @param int $caderno_id ID do caderno
	 * @param array $questoes_ids IDs das questões a serem removidas, no caso de NULO todas as questões são excluídas
	 */
	public function excluir_questoes_de_caderno($caderno_id, $questoes_ids = null)
	{

		if($questoes_ids){
			$this->db->where_in('que_id', $questoes_ids);
		}

		$this->db->where('cad_id', $caderno_id);
		$this->db->delete('cadernos_questoes');

		//Altera a quantidade de questões do caderno
		self::atualizar_totais($caderno_id);

		// atualiza cadernos referenciados
		$refs = self::listar_cadernos_referenciados($caderno_id);

		if($refs) {
			foreach ($refs as $ref) {
				self::excluir_questoes_de_caderno($ref['cad_id'], $questoes_ids);
			}
		}
	}

	public function get_total_resolvidas($caderno_id, $usuario_id, $resultado = null, $incluir_inativas = false)
	{
		$this->db->from('cadernos_questoes cq');
		$this->db->join('questoes_resolvidas qr', 'qr.que_id = cq.que_id');
		$this->db->join('questoes q', 'qr.que_id = q.que_id');
		$this->db->where('qr.cad_id', $caderno_id);
		$this->db->where('cq.cad_id', $caderno_id);
		$this->db->where('qr.usu_id', $usuario_id);
		
		if(!$incluir_inativas){
			$this->db->where('q.que_ativo', SIM);
		}
		
		if(!is_null($resultado)) {

			if($resultado == 1) {
				$this->db->where('qre_selecao = q.que_resposta');
			}
			else {
				$this->db->where('qre_selecao != q.que_resposta');
			}
			
		}
		
		return $this->db->count_all_results();
	}
	
	public function get_total_questoes($caderno_id, $incluir_inativas = false)
	{
		$this->db->from('cadernos_questoes cq');
		$this->db->join('questoes q', 'q.que_id = cq.que_id');
		$this->db->where('cad_id', $caderno_id);
		if(!$incluir_inativas){
			$this->db->where('q.que_ativo', SIM);
		}
				
		return $this->db->count_all_results();
	}

	public function get_total_questoes_anuladas($caderno_id)
	{
		$this->db->from('cadernos_questoes cq');
		$this->db->join('questoes q', 'q.que_id = cq.que_id');
		$this->db->where('cad_id', $caderno_id);
		$this->db->where('q.que_status', CANCELADA);
		$this->db->where('q.que_ativo', SIM);
				
		return $this->db->count_all_results();
	}
	
	public function salvar_acesso($caderno_id)
	{
		$data = array(
			'cad_ultimo_acesso' => get_data_hora_agora()	
		);
		
		$this->db->where('cad_id', $caderno_id);
		$this->db->update('cadernos', $data);
	}
	
	public function incrementar_tempo_gasto($caderno_id, $segundos = TEMPO_GASTO_DEFAULT)
	{
		$caderno = self::get_caderno($caderno_id);

		//Só incrementa se for o caderno do usuário logado
		if($caderno['usu_id'] == get_current_user_id())
		{		
			$tempo_gasto = is_null($caderno['cad_tempo_gasto']) ? $segundos : $caderno['cad_tempo_gasto'] + $segundos;
			
			$this->db->set('cad_tempo_gasto', $tempo_gasto);
			$this->db->where('cad_id', $caderno_id);
			$this->db->update('cadernos');
		}
	}
	
	public function listar_cadernos_produtos($categoria_id)
	{
		if(!is_null($categoria_id)) {
			$this->db->join('cadernos_produtos_categorias cpc', 'cpc.cpr_id = cp.cpr_id');
			$this->db->where('cpc.ccp_id', $categoria_id);
		}
		
		$this->db->from('cadernos_produtos cp');
		$this->db->join('cadernos c', 'c.cad_id = cp.cad_id');
		$query = $this->db->get();
		
		return $query->result_array();
	}
	
	public function listar_cadernos_comercializaveis()
	{
		$this->db->where('cad_comercializavel', true);
		$this->db->order_by('cad_nome', 'asc');
		$query = $this->db->get('cadernos');
		
		return $query->result_array();
	}
	
	public function salvar_caderno_produto($caderno_produto)
	{
		$this->db->insert('cadernos_produtos', $caderno_produto);
	}
	
	public function atualizar_caderno_produto($caderno_produto)
	{
		$this->db->where('cpr_id', $caderno_produto['cpr_id']);
		$this->db->update('cadernos_produtos', $caderno_produto);
	}
	
	public function ativar_caderno_produto($caderno_produto_id)
	{
		$this->db->set('cpr_status', true);
		$this->db->where('cpr_id', $caderno_produto_id);
		$this->db->update('cadernos_produtos');
	}

	public function is_caderno_associado_a_produto($caderno_id)
	{
		$this->db->where('cad_id', $caderno_id);
		$query = $this->db->get('cadernos_produtos');

		return $query->result_array() ? TRUE : FALSE;
	}
	
	public function inativar_caderno_produto($caderno_produto_id)
	{
		$this->db->set('cpr_status', false);
		$this->db->where('cpr_id', $caderno_produto_id);
		$this->db->update('cadernos_produtos');
	}	
	
	public function get_caderno_produto($id)
	{
		$this->db->where('cpr_id', $id);
		$query = $this->db->get('cadernos_produtos');
		
		return $query->row_array();
	}

	public function listar_categorias_cp()
	{
		$this->db->order_by('ccp_nome', 'asc');
		$query = $this->db->get('categorias_cp');
		
		return $query->result_array();
	}
	
	/**
	 * Embaralha as questões de um caderno
	 * 
	 * @since k2
	 * 
	 * @param int $caderno_id
	 */
	
	public function embaralhar_questoes($caderno_id) 
	{
		$questoes = self::listar_caderno_questoes($caderno_id, 'RAND()');			

		for ($i=0; $i < count($questoes); $i++) {				
		
			self::salvar_caderno_questao_ordem($i, $caderno_id, $questoes[$i]['que_id']); 
		}   			  		

	}

	/**
	 * Repera a próximo índice de ordem de um caderno
	 * 
	 * @since k2
	 * 
	 * @param int $caderno_id
	 * 
	 * @return int Próximo índice
	 */

	public function get_proximo_indice_ordem($caderno_id)
	{
		$sql = "SELECT MAX(caq_ordem) as max_ordem FROM cadernos_questoes WHERE cad_id = $caderno_id";

		$query = $this->db->query($sql);

		if($linha = $query->row_array()) {
			return $linha['max_ordem'] + 1;
		}

		return 1;
	}

	/**
	 * Salva a ordem de uma questão de caderno
	 * 
	 * @since k2
	 *
	 * @param int $ordem
	 * @param int $caderno_id
	 * @param int $questao_id 
	 * 
	 * @return int Próximo índice
	 */

	public function salvar_caderno_questao_ordem($ordem, $caderno_id, $questao_id)
	{
		$this->db->set('caq_ordem', $ordem);		
		$this->db->where('cad_id', $caderno_id);
		$this->db->where('que_id', $questao_id);
		$this->db->update('cadernos_questoes');
	}

	public function listar_questoes_em_cadernos_comprados($usuario_id)
	{
		if($usuario_id) {
            $this->db->from('cadernos_questoes cq');
            $this->db->join('cadernos c', 'cq.cad_id = c.cad_id');
            $this->db->where('c.cad_comprado', 1);
            $this->db->where('c.usu_id', $usuario_id);
            $query = $this->db->get();

            return $query->result_array();
        }
		else {
		    return [];
        }
	}
	
	public function listar_questoes_ids_em_cadernos_comprados($usuario_id)
	{
		$questoes = self::listar_questoes_em_cadernos_comprados($usuario_id);
		
		$questoes_ids = array();
		
		foreach ($questoes as $questao) {
			array_push($questoes_ids, $questao['que_id']);
		}
		
		return $questoes_ids;
	}

	public function listar_questoes_ids($caderno_id, $order_random = FALSE, $limite = NULL){

		$this->db->select('cq.que_id');
		$this->db->from('cadernos_questoes cq');
		$this->db->where('cq.cad_id', $caderno_id);

		if($order_random == TRUE){
			$this->db->order_by(' RAND() ');
		}

		if(!is_null($limite)){
			$this->db->limit($limite);
		}

		$query = $this->db->get();
		
		$result = $query->result_array();

		$questoes_ids = array();
		
		foreach ($result as $questao) {
			array_push($questoes_ids, $questao['que_id']);
		}
		
		return $questoes_ids;
	}

	public function is_caderno_compartilhado_adicionado($caderno_id, $usuario_id)
	{
		return self::get_caderno_compartilhado($caderno_id, $usuario_id) ? true : false;
	}

	public function get_caderno_compartilhado($caderno_id, $usuario_id)
	{
		$this->db->where('cad_ref_id', $caderno_id);
		$this->db->where('usu_id', $usuario_id);
		$this->db->where('cad_compartilhado', 1);
		$this->db->where('cad_coaching', 0);
		$this->db->from('cadernos');

		$query = $this->db->get();

		return $query->row_array();
	}

	/**
	 * Retorna um caderno compartilhado e que será de coaching
	 * 
	 * @param int $caderno_id ID do caderno original
	 * @param int $usuario_id ID do usuário do caderno
	 * 
	 * @return Array contendo os dados do caderno
	 */
	public function get_caderno_coaching($caderno_id, $usuario_id)
	{
		$this->db->where('cad_ref_id', $caderno_id);
		$this->db->where('usu_id', $usuario_id);
		$this->db->where('cad_compartilhado', 1);
		$this->db->where('cad_coaching', 1);
		$this->db->from('cadernos');

		$query = $this->db->get();

		return $query->row_array();
	}

	public function adicionar_caderno_compartilhado($caderno_id, $usuario_id, $is_coaching = FALSE)
	{
		$this->db->trans_start();

		$novo_caderno_id = self::duplicar_caderno($caderno_id, $usuario_id);

		$this->db->set('cad_compartilhado', '1');
		$this->db->set('cad_coaching', $is_coaching);
		$this->db->where('cad_id', $novo_caderno_id);
		$this->db->update('cadernos');
		
		$this->db->trans_complete();
		
		return $novo_caderno_id;
	}
	
	/**
     * Duplica um caderno. O caderno gerado é tratado como um novo caderno e associado ao usuário informado.
     *
     * @since K5
     * 
     * @param int $caderno_ref_id Id do caderno original
     * @param int $novo_usuario_id Id do usuário do novo caderno
     * @param bool $salvar_ref_id Opção para gravar, ou não, a referência do caderno original
     * @param string $sufixo Se definido, acrescenta-se ao final do nome do caderno
     * 
     * @throws InvalidArgumentException
     * @throws UnexpectedValueException Disparado caso caderno duplicado não consiga ser criado
     * 
     * @return string URL do caderno
     */ 

	public function duplicar_caderno($caderno_ref_id, $novo_usuario_id, $salvar_ref_id = true, $sufixo = null)
	{
		/* === tratamento dos parâmetros === */
		if(!(filter_var($caderno_ref_id, FILTER_VALIDATE_INT))) 
			throw new InvalidArgumentException("Argumento 'caderno_ref_id' precisa ser inteiro");
		if(!(filter_var($novo_usuario_id, FILTER_VALIDATE_INT)) && filter_var($novo_usuario_id, FILTER_VALIDATE_INT) !== 0)
			throw new InvalidArgumentException("Argumento 'novo_usuario_id' precisa ser inteiro");
	    
	    /* === lógica principal === */
	    
	    // recupera caderno referência e suas questões
		$caderno = self::get_caderno($caderno_ref_id);
		$questoes = self::listar_caderno_questoes($caderno_ref_id);
		
		// edita para ser utilizado como novo caderno
		$caderno['cad_id'] = null;
		$caderno['cad_data_criacao'] = date('Y-m-d H:i:s');
		$caderno['cad_ultimo_acesso'] = null;
		$caderno['cad_tempo_gasto'] = 0;
		$caderno['usu_id'] = $novo_usuario_id;
		$caderno['cad_ref_id'] = $salvar_ref_id ? $caderno_ref_id : null;
		
		// trata nome caso haja sufixo
		if($sufixo) {
		    $caderno['cad_nome'] .= $sufixo;
		}

		// cria novo caderno
		$caderno_id = self::adicionar_caderno($caderno);
		
		// interrompe caso não consiga criar caderno
		if(is_null($caderno_id)) {
		    throw new UnexpectedValueException("Erro ao criar o caderno duplicado");
		}

        // adiciona questões ao caderno
		if($questoes) {
			foreach ($questoes as $questao) {
				self::adicionar_questao_em_caderno($caderno_id, $questao['que_id']);
			}	
		}

		return $caderno_id;
	}
	
	/**
	 * Zera as estatísticas de um caderno
	 *
	 * @since L1
	 *
	 * @param int $caderno_id Id do caderno original
	 * @param int $usuario_id Id do usuário. Usuário precisa ser levado em conta por conta de situações onde ADMINISTRADOR acessa cadernos de outros usuários.
	 *
	 * @throws InvalidArgumentException
	 */
	
	public function zerar_estatisticas($caderno_id, $usuario_id)
	{
	    /* === tratamento dos parâmetros === */
	    
	    if(!(filter_var($caderno_id, FILTER_VALIDATE_INT))) throw new InvalidArgumentException("Argumento 'caderno_id' precisa ser inteiro");
	    if(!(filter_var($usuario_id, FILTER_VALIDATE_INT))) throw new InvalidArgumentException("Argumento 'usuario_id' precisa ser inteiro");
	    
	    /* === lógica principal === */
	    
	    $this->db->where("cad_id", $caderno_id);
	    $this->db->where("usu_id", $usuario_id); 
	    $this->db->delete("questoes_resolvidas");
	}

	public function listar_caderno_questoes($caderno_id, $ordem = null)
	{
		$this->db->where('cad_id', $caderno_id);
			if (!is_null($ordem)) {
			$this->db->order_by($ordem);
		}		
		$query = $this->db->get('cadernos_questoes');

		return $query->result_array();
	}
	
    public function listar_produtos_de_caderno($caderno_id)
    {
        $this->db->where("cad_id", $caderno_id);
        $this->db->from("cadernos_produtos");
        
        $query = $this->db->get();
        
        return $query->result_array();
    }
    
 
	public function salvar_usuario_designado($caderno_id, $usuario_id)
	{
		if(!self::is_usuario_designado($caderno_id, $usuario_id)) {
			$dados = array(
				'cad_id' => $caderno_id,
				'usu_id' => $usuario_id,
				'cde_data' => date('Y-m-d H:i:s')
			);
			$this->db->insert('cadernos_designados', $dados);	
		}
	}

	public function is_usuario_designado($caderno_id, $usuario_id)
	{
		$this->db->from('cadernos_designados');
		$this->db->where('cad_id', $caderno_id);
		$this->db->where('usu_id', $usuario_id);

		return $this->db->count_all_results();
	}

	public function listar_designados($caderno_id)
	{
		$this->db->where('cad_id', $caderno_id);
		$query = $this->db->get('cadernos_designados');

		return $query->result_array();
	}

	/**
	 * Atualiza o filtro utilizado para gerar as questões do caderno
	 * 
	 * @param int $caderno_id ID do caderno a ter o filtro alterado
	 * @param string $filtro serializado a ser salvo
	 * @param bool $merge se verdadeiro faz o merge do filtro novo com o filtro atual
	 */
	public function atualizar_filtros($caderno_id, $filtro, $merge = TRUE)
	{
		if(!is_null($filtro)) {

			$caderno = self::get_caderno($caderno_id);

			// merge com filtro existente
			if($merge == TRUE && $caderno['cad_filtros']) {
				$antigo_a = unserialize($caderno['cad_filtros']);
				$filtro_a = unserialize($filtro);

				$novo_a = array_merge_recursive_ex($antigo_a, $filtro_a);

				$filtro = serialize($novo_a);
			}			

			$this->db->set('cad_filtros', $filtro);
			$this->db->where('cad_id', $caderno_id);
			$this->db->update('cadernos');
		}
	}

	public function atualizar_filtro_questao_avulsa($caderno_id, $questao_id)
	{
		$caderno = self::get_caderno($caderno_id);

		if($caderno['cad_filtros']) {
			$filtro = unserialize($caderno['cad_filtros']);
		}
		else {
			$filtro = array();

		}

		if(!$filtro['filtro_questoes']) {
			$filtro['filtro_questoes'] = array($questao_id);
		}
		else {
			array_push($filtro['filtro_questoes'], $questao_id);
		}

		$this->db->set('cad_filtros', serialize($filtro));
		$this->db->where('cad_id', $caderno_id);
		$this->db->update('cadernos');
	}

	public function get_caderno_por_usuario_caderno_referencia($usuario_id, $caderno_referencia_id){
		$this->db->where("usu_id", $usuario_id);
		$this->db->where("cad_ref_id", $caderno_referencia_id);
		$query = $this->db->get("cadernos");
		return $query->row_array();
	}

	public function atualizar_quantidade_questoes($caderno_id, $qtd_questoes){
		
		$this->db->where('cad_id', $caderno_id);
		$this->db->set('cad_qtd_questoes', $qtd_questoes);
		$this->db->update('cadernos');

	}

	/**
	 * Cria um caderno demo para o usuário logado com referência para o caderno informado
	 * 
	 * @since L1
	 * 
	 * @param row_array Caderno de referência
	 * 
	 */
	public function criar_caderno_demo($caderno_ref)
	{

        $novo_caderno = array(
            'cad_nome' => $caderno_ref['cad_nome'],
            'cad_ref_id' => $caderno_ref['cad_id'],
            'cad_data_criacao' => date('Y-m-d H:i:s'),
            'usu_id' => get_current_user_id(),
			'cad_comprado' => 0,
			'cad_demo' => 1
        );

        $this->db->insert('cadernos', $novo_caderno);
        $novo_caderno_id = $this->db->insert_id();
        
		$questoes = self::listar_questoes_ids($caderno_ref['cad_id']);

		$novo_caderno_questoes = array();
        foreach ($questoes as $questao) {
            $novo_caderno_questao = array(
                'cad_id' => $novo_caderno_id,
                'que_id' => $questao
            );
			array_push($novo_caderno_questoes, $novo_caderno_questao);
		}
		
		$this->db->insert_batch('cadernos_questoes', $novo_caderno_questoes);

	}

	/**
	 * Busca um caderno demo com referência ao caderno informado
	 * 
	 * @since L1
	 * 
	 * @param int $caderno_ref_id
	 * 
	 * @return row_array Caderno demo
	 */
	public function get_caderno_demo($caderno_ref_id, $usuario_id = null)
	{

		if(is_null($usuario_id))
		{
			$usuario_id = get_current_user_id();
		}

		$this->db->where('cad_demo', TRUE);
		$this->db->where('usu_id', $usuario_id);
		$this->db->where('cad_ref_id', $caderno_ref_id);

		$query = $this->db->get('cadernos');

		return $query->row_array();
		
	}
}