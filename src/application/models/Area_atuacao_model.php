<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area_atuacao_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function listar_todas()
	{
		$this->db->from('areas_atuacao');
		$this->db->order_by('ara_nome');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_by_id($area_atuacao_id)
	{
		$query = $this->db->get_where('areas_atuacao', array('ara_id' => $area_atuacao_id));
		return $query->row_array();
	}

	public function get_by_nome($nome)
	{
		$query = $this->db->get_where('areas_atuacao', array('ara_nome' => $nome));
		return $query->row_array();
	}
	
	public function salvar($area_atuacao)
	{
		return $this->db->insert('areas_atuacao', $area_atuacao);
	}

	public function atualizar($area_atuacao)
	{
		$this->db->where('ara_id', $area_atuacao['ara_id']);
		$this->db->set($area_atuacao);
		return $this->db->update('areas_atuacao');
	}

	public function excluir($area_atuacao_id)
	{
		$this->db->where('ara_id', $area_atuacao_id);
		return $this->db->delete('areas_atuacao');
	}
}