<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Area_formacao_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function listar_todas()
	{
		$this->db->from('areas_formacao');
		$this->db->order_by('arf_nome');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get_by_id($area_formacao_id)
	{
		$query = $this->db->get_where('areas_formacao', array('arf_id' => $area_formacao_id));
		return $query->row_array();
	}

	public function get_by_nome($nome)
	{
		$query = $this->db->get_where('areas_formacao', array('arf_nome' => $nome));
		return $query->row_array();
	}
	
	public function salvar($area_formacao)
	{
		return $this->db->insert('areas_formacao', $area_formacao);
	}

	public function atualizar($area_formacao)
	{
		$this->db->where('arf_id', $area_formacao['arf_id']);
		$this->db->set($area_formacao);
		return $this->db->update('areas_formacao');
	}

	public function excluir($area_formacao_id)
	{
		$this->db->where('arf_id', $area_formacao_id);
		return $this->db->delete('areas_formacao');
	}
}