<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuracao_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_by_id($id)
	{
		self::prepare_query();
		$this->db->where('cfg_id', $id);
		$query = $this->db->get();
		
		return $query->row_array();
	}
	
	public function get_by_nome($nome)
	{
		$query = $this->db->get_where('configuracoes', array('cfg_nome' => $nome));
		return $query->row_array();
		
	}
	
	public function get_valor($cfg_nome, $incluir_novo_registro = false) {
		$result = self::get_by_nome($cfg_nome);
		
		if(!$incluir_novo_registro)
			return $result['cfg_valor'];
		
		if(count($result) > 0)
			return $result['cfg_valor'];
		
		$configuracao = array('cfg_nome' => $cfg_nome, 'cfg_valor' => 1);
		self::salvar($configuracao);
		return self::get_valor($cfg_nome);
	}
	
	
	public function salvar($data)
	{
		$this->db->insert('configuracoes', $data);
	}
	
	public function atualizar($data)
	{
		$this->db->where('cfg_nome', $data['cfg_nome']);
		$this->db->update('configuracoes', $data);
	}

	public function get($nome)
	{
		$config = self::get_by_nome($nome);

		if($config) {
			return $config['cfg_valor'];
		}
		return null;
	}

	public function set($nome, $valor)
	{
		$config = self::get_by_nome($nome);

		$dados = array(
			'cfg_nome' => $nome,
			'cfg_valor' => $valor
		);

		if($config) {
			self::atualizar($dados);
		}
		else {
			self::salvar($dados);	
		}
	}

	private function prepare_query()
	{
		$this->db->from('configuracoes');
	}
	
}