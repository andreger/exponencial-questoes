<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cargo_model extends CI_Model {
	
	public function __construct()
	{
		$this->load->database();
	}
	
	public function listar_todas() 
	{
		$this->db->from('cargos');
		$this->db->order_by('car_nome');
		$query = $this->db->get();

		return $query->result_array();
	}
	
	public function get_by_id($cargo_id)
	{
		$query = $this->db->get_where('cargos', array('car_id' => $cargo_id));
		return $query->row_array();
	}

	public function get_by_nome($nome)
	{
		$query = $this->db->get_where('cargos', array('car_nome' => $nome));
		return $query->row_array();
	}
	
	public function salvar($cargo)
	{
		return $this->db->insert('cargos', $cargo);
	}
	
	public function atualizar($cargo)
	{
		$this->db->where('car_id', $cargo['car_id']);
		$this->db->set($cargo);
		return $this->db->update('cargos');
	}
	
	public function excluir($cargo_id)
	{
		$this->db->where('car_id', $cargo_id);
		return $this->db->delete('cargos');
	}
}