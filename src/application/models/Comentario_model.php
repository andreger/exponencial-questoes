<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comentario_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function listar_todas()
	{
		$this->db->from('comentarios');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_by_id($comentario_id)
	{
		$query = $this->db->get_where('comentarios', array('com_id' => $comentario_id));
		return $query->row_array();
	}
	
	public function listar_por_questao($questao_id, $destaque = null, $comentario_id = null)
	{
		if(!is_null($destaque)) {
			$this->db->where('com_destaque', (int)$destaque);
		}

		if(!is_null($comentario_id)){
			$this->db->where('com_id !=', $comentario_id);
		}

		$this->db->order_by('com_data_criacao', 'desc');
		$query = $this->db->get_where('comentarios', array('que_id' => $questao_id));
		return $query->result_array();
	}

    public function listar_destaques_por_questao($questao_id)
    {
        $this->db->order_by('com_data_criacao', 'desc');
        $query = $this->db->get_where('comentarios', [
            'que_id' => $questao_id,
            'com_destaque' => 1
        ]);
        return $query->result_array();
    }
	
	public function contar_comentarios($questao_id, $destaque = null)
	{
		if(!is_null($destaque)) {
			$this->db->where('com_destaque', (int)$destaque);
		}

		$this->db->where('que_id', $questao_id);
		$this->db->from('comentarios');
		return $this->db->count_all_results();
	}
	
	public function contar_comentarios_professores($questao_id)
	{
		$this->db->where('que_id', $questao_id);
		$query = $this->db->get('comentarios');
		$result = $query->result_array();
		
		$comentarios_professores = 0;
		foreach ($result as $item) {
			if(is_professor($item['user_id'])) {
				$comentarios_professores = $comentarios_professores + 1;
			}
		}
		
		return $comentarios_professores;
	}
	
	public function professor_pode_comentar($questao_id, $professor_id)
	{
		if(is_administrador($professor_id)) return true;
		
		$comentarios = self::listar_por_questao($questao_id);
		foreach ($comentarios as $item) {
			if(is_professor($item['user_id']) && ($item['user_id'] != $professor_id)) {
				return false;
			}
		}
		return true;
	}
	
	public function get_professor_que_comentou($questao_id)
	{
		$comentarios = self::listar_por_questao($questao_id);
		foreach ($comentarios as $item) {
			
			if(is_professor($item['user_id'])) {
				return get_usuario_array($item['user_id']);
			}
		}
		return false;
	}

	public function listar_por_usuario($usuario_id)
	{
		$query = $this->db->get_where('comentarios', array('user_id' => $usuario_id));
		return $query->result_array();
	}
	
	public function salvar($comentario)
	{
		$comentario['com_destaque'] = (int)self::pode_ser_destaque($comentario);
		$this->db->insert('comentarios', $comentario);
		return $this->db->insert_id();
	}

	public function atualizar($comentario)
	{
		$comentario['com_destaque'] = (int)self::pode_ser_destaque($comentario);

		$this->db->where('com_id', $comentario['com_id']);
		$this->db->set($comentario);
		$this->db->update('comentarios');
		return $comentario['com_id'];
	}

	private function pode_ser_destaque($comentario)
	{
		// se é professor
		if(is_professor($comentario['user_id'])) {

			// se questão não possui destaque
			if(count(self::listar_por_questao($comentario['que_id'], true, $comentario['com_id'])) == 0) {

				return true;
			}
		}
		
		return false;
	}

	public function resetar_destaque()
	{
		$this->db->set('com_destaque', (int)0);
		$this->db->where('com_id >', 0);
		$this->db->update('comentarios');
	}

	public function get_destaque($questao_id)
	{
		$this->db->where('com_destaque', 1);
		$this->db->where('que_id', $questao_id);
		$query = $this->db->get('comentarios');

		return $query->row_array();
	}

	public function contar_todos()
	{
		return $this->db->count_all('comentarios');
	}

	public function atualizar_destaque($comentario)
	{
		if(self::pode_ser_destaque($comentario)) {
			$this->db->set('com_destaque', 1);
			$this->db->where('com_id', $comentario['com_id']);
			$this->db->update('comentarios');

			return TRUE;
		}

		return FALSE;
	}

	public function listar_a_partir($num, $limit)
	{
		$this->db->where('com_id >', $num);
		$this->db->limit($limit);
		$this->db->order_by('com_id asc');
		$query = $this->db->get('comentarios');

		return $query->result_array();

	}

	public function excluir($comentario_id)
	{
		// armazena informações sobre comentário para uso posterior
		$comentario = self::get_by_id($comentario_id);

		// exclui comentário
		$this->db->where('com_id', $comentario_id);
		$this->db->delete('comentarios');

		// procura eleger como destaque outro comentário de professor na questão
		$comentarios = self::listar_por_questao($comentario['que_id']);

		//Verifica se precisa diminuir o valor fixo de um professor
		if($comentario['com_destaque'] 
			&& $comentario['com_data_criacao'] <= strtotime(REL_QUESTOES_COMENTADAS_DATA_FIXADA . " 23:59:59"))
		{
			$this->db->where('usu_id', $comentario['user_id']);
			$this->db->set('qct_total', 'qct_total - 1', FALSE);
			$this->db->update('questoes_comentadas_professor_travado');
		}

		foreach ($comentarios as $comentario) {
			if(self::atualizar_destaque($comentario)) {
				//Aumenta o valor fixo de comentários do professor com destaque atual
				$this->db->where('usu_id', $comentario['user_id']);
				$this->db->set('qct_total', 'qct_total + 1', FALSE);
				$this->db->update('questoes_comentadas_professor_travado');
				break;
			}
		}

	}
	
	public function excluir_comentarios_jmeter()
	{
		//Antes de excluir busca as questões afetadas
		$this->db->select_distinct("que_id");
		$this->db->from("comentarios");
		$this->db->where('com_texto', '<p>teste-jmeter<br></p>');
		$query = $this->db->get();
		$que_ids = $query->result_array();

		//Exclui os comentários
		$this->db->where('com_texto', '<p>teste-jmeter<br></p>');
		$this->db->delete('comentarios');

		//retorna os ids das questões afetadas para que a quantidade de comentários seja alterada
		return $que_ids;
	}
	
	public function is_autor($comentario_id)
	{
		$comentario = self::get_by_id($comentario_id);
		$user_id = get_id_usuario_atual();
		if($comentario['user_id'] == $user_id)
			return true;
		return false;
	}
	
	public function atualizar_status($comentario_id, $status) {
		$this->db->set('com_status', $status);
		$this->db->where('com_id', $comentario_id);
		$this->db->update('comentarios');
	}
	
	public function get_total_comentarios_dia_by_userid($dia)
	{
		$this->db->select("user_id, count(user_id) as qtde_professor, DATE_FORMAT(com_data_criacao,'%Y-%m-%d') as data", FALSE);
		$this->db->from('comentarios c');
		$this->db->join('questoes q', 'c.que_id = q.que_id');
		$this->db->where("DATE_FORMAT(com_data_criacao,'%Y-%m-%d') = '{$dia}'");
		$this->db->where('com_destaque', 1);
		$this->db->where('que_ativo', ATIVO);
		$this->db->group_by("user_id");
		$query = $this->db->get();
		
		if($query->num_rows() > 0){
			return $query->result_array();
		}
		else{
			return FALSE;
		}
	}

	function get_total_comentarios_por_usuario($usuario_id, $somente_destaques = 1)
	{
		$this->db->from('comentarios');
		$this->db->where('com_destaque', (int)$somente_destaques);
		$this->db->where('user_id', $usuario_id);
		
		return $this->db->count_all_results();
	}

	public function salvar_comentario_avaliacao($comentario_id, $usuario_id, $avaliacao){
		
		$from_db = self::get_comentario_avaliacao($comentario_id, $usuario_id);	

		if(is_null($from_db)){
			$this->db->insert('comentarios_avaliacoes', array(
				'usu_id' => $usuario_id, 
				'com_id' => $comentario_id, 
				'cav_avaliacao' => $avaliacao
			));
		}else{
			$this->db->where('cav_id', $from_db['cav_id']);
			$this->db->set('cav_avaliacao', $avaliacao);
			$this->db->update('comentarios_avaliacoes');
		}

		return self::atualiza_comentario_media_avaliacao($comentario_id);

	}

	public function get_comentario_avaliacao($comentario_id, $usuario_id){
		
		$this->db->where('com_id', $comentario_id);
		$this->db->where('usu_id', $usuario_id);

		$query = $this->db->get('comentarios_avaliacoes');
		
		if($query->num_rows() > 0)
			return $query->row_array();
		else 
			return null;
	}

	public function listar_comentarios_avaliacoes($questao_id, $usuario_id){
				
		$this->db->select('cav.*');
		$this->db->from('comentarios com');
		$this->db->join('comentarios_avaliacoes cav', 'com.com_id = cav.com_id');
		$this->db->where('com.que_id', $questao_id);
		$this->db->where('cav.usu_id', $usuario_id);
		$query = $this->db->get();

		$result = $query->result_array();
		
		$map = array();
		
		foreach($result as $item){
			$map[$item['com_id']] = $item;
		}

		return $map;

	}

	private function atualiza_comentario_media_avaliacao($comentario_id){
		
		//Vê quantas avaliações ocorreram
		$this->db->select('count(com_id) as total, sum(cav_avaliacao) as soma');
		$this->db->from('comentarios_avaliacoes');
		$this->db->where('com_id', $comentario_id);
		$query = $this->db->get();
		$result_total = $query->row_array();
		$total = $result_total['total'];
		$max = $total;
		if($total >= 10){//Garantir que o percentual excluído seja maior que um
			$max = floor($total * 0.9);//excluirá as 10% piores avaliações
		}
		$media = 0;
		$media_simples = 0;
		if($max > 0){
			//Busca as maiores avaliações até o limite calculado, soma e calcula a média
			$this->db->select('cav_avaliacao');
			$this->db->where('com_id', $comentario_id);
			$this->db->order_by('cav_avaliacao', 'DESC');
			$this->db->limit($max);

			$query = $this->db->get('comentarios_avaliacoes');

			$result = $query->result_array();

			foreach ($result as $item) {
				$media += $item['cav_avaliacao'];
			}
			$media = round(($media/$max), 2);
			$media_simples = round(($result_total['soma']/$total), 2);
		}

		//Atualiza a média de avaliação do comentário
		$this->db->where('com_id', $comentario_id);
		$this->db->set('com_media_avaliacao', $media);
		$this->db->set('com_media_qtd_avaliacoes', $max);
		$this->db->set('com_media_simples_avaliacoes', $media_simples);
		$this->db->set('com_qtd_avaliacoes', $total);
		$this->db->update('comentarios');

		return $media;
	}
	
	/**
	 * Lista a quantidade de comentários por professor para um determinado simulado
	 * 
	 * @since K5
	 * 
	 * @param int $simulado_id Id do simulado
	 */
	public function contar_comentarios_simulado_por_professor($simulado_id){
		return self::contar_comentarios_por_professor($simulado_id, null);
	}

	/**
	 * Lista a quantidade de comentários por professor para um determinado caderno
	 * 
	 * @since K5
	 * 
	 * @param int $caderno_id Id do caderno
	 */
	public function contar_comentarios_caderno_por_professor($caderno_id){
		return self::contar_comentarios_por_professor(null, $caderno_id);
	}

	/**
	 * Lista a quantidade de comentários por professor para um determinado simulado ou caderno
	 * 
	 * @since K5
	 * 
	 * @param int $simulado_id Id do simulado
	 * @param int $caderno_id Id do caderno
	 */
	private function contar_comentarios_por_professor($simulado_id, $caderno_id){
		
		$this->db->select('u.id, u.display_name as nome, count(*) as total');
		$this->db->from('comentarios c');
		$this->db->join('exponenc_db.wp_users u', 'u.id = c.user_id');
		$this->db->where('c.com_destaque', TRUE);

		if(!is_null($simulado_id)){
			$this->db->join('simulados_questoes q', 'q.que_id = c.que_id');
			$this->db->where('q.sim_id', $simulado_id);
		}

		if(!is_null($caderno_id)){
			$this->db->join('cadernos_questoes q', 'q.que_id = c.que_id');
			$this->db->where('q.cad_id', $caderno_id);
		}
		
		$this->db->group_by('u.id, u.display_name');
		$this->db->order_by('u.display_name', 'asc');
		
		$query = $this->db->get();

		return $query->result_array();
	}

	/**
	 * Salva comentario em vídeo de uma questão,
	 * 
	 * @since L1
	 * 
	 * @param array $comentario_video
	 * 
	 * @return int ID do novo comentário de vídeo
	 */
	public function salvar_comentario_video($comentario_video){
				
		$comentario_video['cvi_data'] = get_data_hora_agora();
		$this->db->insert('comentarios_videos', $comentario_video);
		return $this->db->insert_id();

	}

	/**
	 * Remove o comentário em vídeo de uma questão
	 * 
	 * @since L1
	 * 
	 * @param array $comentario_video
	 */
	public function excluir_comentario_video($comentario_video){

		$this->db->where('cvi_id', $comentario_video['cvi_id']);
		$this->db->delete('comentarios_videos');

	}

	/**
	 * Atualizar comentário em vídeo de uma questão
	 * 
	 * @since L1
	 * 
	 * @param array $comentario_video
	 */
	public function atualizar_comentario_video($comentario_video){

		$this->db->where('cvi_id', $comentario_video['cvi_id']);

		$this->db->set('cvi_url', $comentario_video['cvi_url']);
		$this->db->set('usu_id', $comentario_video['usu_id']);

		$this->db->update('comentarios_videos');
	}

	/**
	 * Busca um comentário em vídeo para uma certa questão
	 * 
	 * @since L1
	 * 
	 * @param $questao_id ID da questão
	 * 
	 * @return array $comentario_video
	 */
	public function get_comentario_video_por_questao($questao_id){

		$this->db->where('que_id', $questao_id);
		$query = $this->db->get('comentarios_videos');
		return $query->row_array();

	}

}