<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Disciplina_model extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
	
	public function listar_por_ids($ids) {
		$this->db->from('disciplinas');
		$this->db->where_in('dis_id', $ids);
		$this->db->order_by('dis_nome', 'asc');
		$query = $this->db->get();
		
		return $query->result_array();
	}

	public function listar_sem_assunto()
	{
		$sem_assunto = SEM_ASSUNTO;

		$sql = "SELECT * from disciplinas d where d.dis_id not in (SELECT distinct dis_id from assuntos a where a.ass_nome = '$sem_assunto')";

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function listar_todas($incluir_soma_questoes = FALSE, $somente_ativas = TRUE)
	{
		if($incluir_soma_questoes) {
			$this->db->select("d.*, count(q.que_id) as total");
			$this->db->from('disciplinas as d');
			$this->db->join('questoes as q', 'q.dis_id=d.dis_id', 'left');
			$this->db->group_by('dis_id');
			$this->db->order_by('dis_nome');
		}
		else {
			$this->db->select("d.*");
			$this->db->from('disciplinas as d');
			$this->db->order_by('dis_nome');
		}

		if($somente_ativas) {
			$this->db->where('dis_ativo', SIM);
		}
		
		$query = $this->db->get();

		return $query->result_array();
	}
	
	public function get_by_id($disciplina_id)
	{
		$query = $this->db->get_where('disciplinas', array('dis_id' => $disciplina_id));
		return $query->row_array();
	}
	
	public function get_by_simulado($simulado_id) {
		$this->db->from('simulados_disciplinas');
		$this->db->where('sim_id', $simulado_id);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_by_nome($nome, $incluir_novo_registro = false)
	{
		$nome = trim($nome);
		$query = $this->db->get_where('disciplinas', array('dis_nome' => $nome));
	
		$result = $query->row_array();
	
		if(!$incluir_novo_registro)
			return $result;
	
		if(count($result) > 0)
			return $result;
	
		$data = array('dis_nome' => $nome);
		self::salvar($data);
		return self::get_by_nome($nome);
	}
	
	public function salvar($disciplina)
	{
		$this->db->insert('disciplinas', $disciplina);
		return $this->db->insert_id();
	}
	
	public function atualizar($disciplina)
	{
		$this->db->where('dis_id', $disciplina['dis_id']);
		$this->db->set($disciplina);
		return $this->db->update('disciplinas');
	}
	
	public function excluir($disciplina_id)
	{
		$this->db->where('dis_id', $disciplina_id);
		return $this->db->delete('disciplinas');
	}
	
	public function tem_assunto($disciplina_id, $assunto_nome)
	{
		$this->db->where('dis_id', $disciplina_id);
		$this->db->where('ass_nome', $assunto_nome);
		$query = $this->db->get('assuntos');
		
		return $query->result_array() ? true : false;
	}
	
	public function get_arvore($disciplinas_ids = null)
	{
	 	$filhos = self::listar_filhos($disciplinas_ids);
		
		foreach ($filhos as $filho) {
		 	$filho['filhos'] = self::get_arvore($filho['dis_id']);
		}
		
		return $filhos; 
	}
	
	public function listar_filhos($disciplina_id = null)
	{
		if($disciplina_id) {
			$this->db->where('dis_pai_id', $disciplina_id);
		}
		
		$this->db->order_by('dis_nome');
		$query = $this->db->get('disciplinas');
		
		return $query->result_array();
	}

	public function listar_simulados_atrelados($disciplina_id)
	{
		$this->db->join('simulados s', 's.sim_id = sd.sim_id');
		$this->db->where('sd.dis_id', $disciplina_id);
		$query = $this->db->get('simulados_disciplinas sd');

		return $query->result_array();
	}

	public function tem_simulados_atrelados($disciplina_id)
	{
		return self::listar_simulados_atrelados($disciplina_id) ? TRUE : FALSE;
	}
}