<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabelas_slugs_historicos extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        /* Tabela concursos_slugs_historico */
        
        $campos = [
                'con_slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 300
                ),
                'con_id' => array(
                    'type' => 'INT',
                )
        ];

        $this->dbforge->add_field($campos);

        $this->dbforge->add_key('con_slug', TRUE);
        $this->dbforge->create_table('concursos_slugs_historico');
        
        $this->db->query("INSERT INTO concursos_slugs_historico SELECT post_name, ID FROM wp_posts where post_type = 'concurso'");
        
        /* Tabela blogs_slugs_historico */
        
        $campos = [
            'blo_slug' => array(
                'type' => 'VARCHAR',
                'constraint' => 300
            ),
            'blo_id' => array(
                'type' => 'INT',
            )
        ];
        
        $this->dbforge->add_field($campos);
        
        $this->dbforge->add_key('blo_slug', TRUE);
        $this->dbforge->create_table('blogs_slugs_historico');
        $this->db->query("INSERT INTO blogs_slugs_historico SELECT post_name, ID FROM wp_posts where post_type = 'post'");
        
        /* Tabela landing_pages_slugs_historico */
        
        $campos = [
            'lap_slug' => array(
                'type' => 'VARCHAR',
                'constraint' => 300
            ),
            'lap_id' => array(
                'type' => 'INT',
            )
        ];
        
        $this->dbforge->add_field($campos);
        
        $this->dbforge->add_key('lap_slug', TRUE);
        $this->dbforge->create_table('landing_pages_slugs_historico');
        $this->db->query("INSERT INTO landing_pages_slugs_historico SELECT post_name, ID FROM wp_posts WHERE post_type = 'landing_pages'");

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('landing_pages_slugs_historico');
        $this->dbforge->drop_table('blogs_slugs_historico');
        $this->dbforge->drop_table('concursos_slugs_historico');

        $this->db->query("use " . DB_NAME_CORP);
    }
}