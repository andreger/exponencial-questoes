<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_preco_assinaturas extends CI_Migration
{
    public function up()
    {   
        $this->db->query("use " . DB_NAME);

        $campos = [

            'preco' => [
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ],

        ];

        $this->dbforge->add_column('assinaturas', $campos);

        $this->db->query("use " . DB_NAME_CORP);

    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('assinaturas', 'preco');

        $this->db->query("use " . DB_NAME_CORP);
        
    }
}