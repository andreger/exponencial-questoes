<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_tabela_questoes_anotacoes extends CI_Migration
{
    public function up()
    {   

        $campos = [

            'qan_id' => [
                'type' => 'INT',
                'auto_increment' => TRUE
            ],
            'que_id' => [
                'type' => 'INT',
                'null' => FALSE,
            ],
            'usu_id' => [
                'type' => 'INT',
                'null' => FALSE,
            ],
            'qan_data_criacao' => [
                'type' => 'DATETIME',
            ],
            'qan_data_atualizacao' => [
                'type' => 'DATETIME',
            ],
            'qan_anotacao' => [
                'type' => 'TEXT',
            ],
        ];

        $this->dbforge->add_field($campos);
        $this->dbforge->add_key('qan_id', TRUE);
        $this->dbforge->create_table('questoes_anotacoes');

    }

    public function down()
    {
        $this->dbforge->drop_table('questoes_anotacoes');
    }
}