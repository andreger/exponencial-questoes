<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_alterar_chave_tabela_assuntos_conversao extends CI_Migration {

        public function up()
        {

                $this->db->query('alter table assuntos_conversao drop primary key');
                $this->db->query('alter table assuntos_conversao add constraint unique_key unique(ass_antigo, ass_novo, dis_id)');

                $campos = [
                        'ass_antigo' => array(
                                'name' => 'ass_antigo',
                                'type' => 'INT',
                                'constraint' => '11',
                                'NULL' => TRUE
                        )
                ];

                $this->dbforge->modify_column('assuntos_conversao', $campos);
                
        }

        public function down()
        {
                $campos = [
                        'ass_antigo' => array(
                                'name' => 'ass_antigo',
                                'type' => 'INT',
                                'constraint' => '11',
                                'NULL' => FALSE
                        )
                ];

                $this->dbforge->modify_column('assuntos_conversao', $campos);

                $this->db->query('alter table assuntos_conversao drop index unique_key');
                $this->db->query('alter table assuntos_conversao add primary key (ass_antigo)');

        }
}