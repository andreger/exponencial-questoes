<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_atualizar_tipo_questoes extends CI_Migration
{
    public function up()
    {   

        $this->db->query("UPDATE questoes q SET q.que_tipo = (CASE (SELECT COUNT(*) FROM questoes_opcoes qo WHERE qo.que_id = q.que_id) WHEN 5 THEN 3 WHEN 0 THEN 2 ELSE 1 END);");

    }

    public function down()
    {
        $this->db->query("UPDATE questoes q SET q.que_tipo = 1 WHERE que_tipo = 3");
        
    }
}