<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_adicionar_coluna_recorrente extends CI_Migration {
    
    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $fields = array(
            'ppr_recorrente_id' => array(
                'type' => 'INT',
                'default' => null,
                'after' => 'ped_data_base'
            ),
        );

        $this->dbforge->add_column('pedido_produto', $fields);

        $this->db->query("use " . DB_NAME_CORP);
    }
    
    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('pedido_produto', 'ppr_recorrente_id');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}