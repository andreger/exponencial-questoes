<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_colunas_colaboradores extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $campos = [
            'col_prioridade' => [
                'type' => 'TINYINT',
            ],
            'col_oculto' => [
                'type' => 'TINYINT',
            ],
            'col_cargo' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'col_mini_cv' => [
                'type' => 'TEXT',
            ],
            'col_descricao' => [
                'type' => 'TEXT',
            ],
            'col_foto_url_200' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
            ],
            'col_foto_url_365' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
            ],
            'col_youtube' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'col_linkedin' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'col_facebook' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'col_twitter' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'col_googleplus' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],
            'col_instagram' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
            ],

        ];

        $this->dbforge->add_column('colaboradores', $campos);

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('colaboradores', 'col_prioridade');
        $this->dbforge->drop_column('colaboradores', 'col_oculto');
        $this->dbforge->drop_column('colaboradores', 'col_cargo');
        $this->dbforge->drop_column('colaboradores', 'col_mini_cv');
        $this->dbforge->drop_column('colaboradores', 'col_foto_url_200');
        $this->dbforge->drop_column('colaboradores', 'col_foto_url_365');
        $this->dbforge->drop_column('colaboradores', 'col_youtube');
        $this->dbforge->drop_column('colaboradores', 'col_linkedin');
        $this->dbforge->drop_column('colaboradores', 'col_facebook');
        $this->dbforge->drop_column('colaboradores', 'col_twitter');
        $this->dbforge->drop_column('colaboradores', 'col_instagram');
        $this->dbforge->drop_column('colaboradores', 'col_googleplus');
        $this->dbforge->drop_column('colaboradores', 'col_descricao');

        $this->db->query("use " . DB_NAME_CORP);
    }
}