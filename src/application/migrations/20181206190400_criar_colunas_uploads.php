<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_colunas_uploads extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $campos = [
            'pro_upload_nao_realizado' => [
                'type' => 'TINYINT',
            ],
            'pro_upload_pendente' => [
                'type' => 'TINYINT',
            ],
            'pro_is_mapa_mental' => [
                'type' => 'TINYINT',
            ],
        ];

        $this->dbforge->add_column('produtos', $campos);

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('produtos', 'pro_upload_nao_realizado');
        $this->dbforge->drop_column('produtos', 'pro_upload_pendente');
        $this->dbforge->drop_column('produtos', 'pro_is_mapa_mental');

        $this->db->query("use " . DB_NAME_CORP);
    }
}