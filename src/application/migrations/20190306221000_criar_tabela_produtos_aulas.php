<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabela_produtos_aulas extends CI_Migration {
    
    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        /**
         * Tabela produtos_exames
         */
        
        $campos = [
            'post_id' => array(
                'type' => 'INT',
            ),
            'pau_num' => array(
                'type' => 'INT',
            ),
            'pau_data' => array(
                'type' => 'DATE',
            ),
            'pau_tem_arquivo' => array(
                'type' => 'TINYINT',
            ),
        ];
        
        $this->dbforge->add_field($campos);
        
        // Primary key
        $this->dbforge->add_key('post_id', TRUE);
        $this->dbforge->add_key('pau_num', TRUE);
        
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (post_id) REFERENCES produtos(post_id) ON DELETE CASCADE ON UPDATE CASCADE');
        
        $this->dbforge->create_table('produtos_aulas');
        
        
        $this->db->query("use " . DB_NAME_CORP);
    }
    
    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('produtos_aulas');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}