<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabela_produtos extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        /**
         * Tabela produtos
         */ 

        $campos = [
                'post_id' => array(
                        'type' => 'INT',
                ),
                'pro_busca' => array(
                        'type' => 'TEXT',
                ),
                'pro_is_pacote' => array(
                        'type' => 'TINYINT',
                ),
                'pro_is_destaque' => array(
                        'type' => 'TINYINT',
                ),
                'pro_vendas_ate' => array(
                        'type' => 'DATE',
                        'null' => TRUE
                ),
                'pro_disponivel_ate' => array(
                        'type' => 'DATE',
                        'null' => TRUE
                ),
                'pro_qtde_aulas' => array(
                        'type' => 'MEDIUMINT',
                        'null' => TRUE
                ),
                'pro_qtde_esquemas' => array(
                        'type' => 'MEDIUMINT',
                        'null' => TRUE
                ),
                'pro_qtde_questoes' => array(
                        'type' => 'MEDIUMINT',
                        'null' => TRUE
                ),
                'pro_preco_sem_desconto' => array(
                        'type' => 'DECIMAL',
                        'constraint' => '10,2',
                ),
                'pro_preco' => array(
                        'type' => 'DECIMAL',
                        'constraint' => '10,2',
                ),
                'pro_publicado_em' => array(
                        'type' => 'DATETIME',
                ),
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('post_id', TRUE);

        $this->dbforge->create_table('produtos');

        /**
         * Tabela produtos_autores
         */ 

        $campos = [
                'post_id' => array(
                        'type' => 'INT',
                ),
                'user_id' => array(
                        'type' => 'INT',
                ),
                'pru_percentual' => array(
                        'type' => 'DECIMAL',
                        'constraint' => '10,4',
                ),
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('post_id', TRUE);
        $this->dbforge->add_key('user_id', TRUE);

         // Foreign key
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (post_id) REFERENCES produtos(post_id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('produtos_professores');

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('produtos_professores');
        $this->dbforge->drop_table('produtos');

        $this->db->query("use " . DB_NAME_CORP);
    }
}