<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_adicionar_colunas_produtos_aulas extends CI_Migration {
    
    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $campos = [
            'pau_tem_resumo' => [
                'type' => 'TINYINT',
                'default' => 0,
                'after' => 'pau_tem_arquivo'
            ],
            'pau_tem_mapa' => [
                'type' => 'TINYINT',
                'default' => 0,
                'after' => 'pau_tem_arquivo'
            ]
        ];
        
        $this->dbforge->add_column('produtos_aulas', $campos);

        $this->db->query("use " . DB_NAME_CORP);
    }
    
    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('produtos_aulas', 'pau_tem_resumo');

        $this->dbforge->drop_column('produtos_aulas', 'pau_tem_mapa');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}