<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_indice_produtos extends CI_Migration
{
    public function up()
    {   

        $this->db->query("use " . DB_NAME);

        $this->db->query("CREATE FULLTEXT INDEX pro_busca_idx ON produtos(pro_busca)");

        $this->db->query("use " . DB_NAME_CORP);

    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->db->query("ALTER TABLE produtos DROP INDEX pro_busca_idx");

        $this->db->query("use " . DB_NAME_CORP);
    }
}