<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_tabela_premium_info extends CI_Migration
{
    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        /* === Tabela premium_info === */
        $campos = [
            'pri_id' => [
                'type' => 'INT',
                'auto_increment' => true
            ],
            'pri_ano_mes' => [
                'type' => 'INT',
            ],
            'pro_id' => [
                'type' => 'INT',
            ],
            'pri_preco' => [
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ],
        ];

        $this->dbforge->add_field($campos);
        $this->dbforge->add_key('pri_id', TRUE);
        $this->dbforge->create_table('premium_info');
        
        /* === Tabela premium_info_itens === */
        $campos = [
            'pri_id' => [
                'type' => 'INT',
            ],
            'pro_item_id' => [
                'type' => 'INT',
            ],
            'pri_item_preco' => [
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ],
        ];
        
        $this->dbforge->add_field($campos);
        $this->dbforge->add_key('pri_id', TRUE);
        $this->dbforge->add_key('pro_item_id', TRUE);
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (pri_id) REFERENCES premium_info(pri_id) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->dbforge->create_table('premium_info_itens');
        
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('premium_info_itens');
        $this->dbforge->drop_table('premium_info');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}