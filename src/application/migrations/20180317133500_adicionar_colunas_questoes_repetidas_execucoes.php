<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Adicionar_colunas_questoes_repetidas_execucoes extends CI_Migration {

        public function up()
        {
                $campos = [
                        'qrx_mesma_banca' => [
                                'type' => 'INT',
                                'constraint' => 11,
                        ],
                        'qrx_disciplinas' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                        ),
                        'qrx_anos' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                        )
                ];

                $this->dbforge->add_column('questoes_repetidas_execucoes', $campos);
        }

        public function down()
        {
                $this->dbforge->drop_column('questoes_repetidas_execucoes', 'qrx_mesma_banca');
                $this->dbforge->drop_column('questoes_repetidas_execucoes', 'qrx_disciplinas');
                $this->dbforge->drop_column('questoes_repetidas_execucoes', 'qrx_anos');
        }
}