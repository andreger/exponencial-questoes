<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabelas_blog extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

         /**
         * Tabela blogs
         */ 

        $campos = [
                'blo_id' => array(
                    'type' => 'INT',
                ),
                'user_id' => array(
                    'type' => 'SMALLINT',
                ),
                'blo_slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '200',
                ),
                'blo_thumb' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '300',
                ),
                'blo_resumo' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '300',
                ),
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('blo_id', TRUE);

        $this->dbforge->create_table('blogs');

        /**
         * Tabela blogs_tipos
         */ 

        $campos = [
                'blo_id' => array(
                        'type' => 'INT',
                ),
                'blo_tipo' => array(
                        'type' => 'INT',
                ),
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('blo_id', TRUE);
        $this->dbforge->add_key('blo_tipo', TRUE);

         // Foreign key
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (blo_id) REFERENCES blogs(blo_id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('blogs_tipos');

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('blogs_tipos');
        $this->dbforge->drop_table('blogs');

        $this->db->query("use " . DB_NAME_CORP);
    }
}