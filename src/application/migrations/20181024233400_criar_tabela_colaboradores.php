<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabela_colaboradores extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        /**
         * Tabela produtos
         */ 

        $campos = [
                'user_id' => array(
                        'type' => 'INT',
                ),
                'col_tipo' => array(
                        'type' => 'TINYINT',
                ),
                'col_qtde_cursos' => array(
                    'type' => 'SMALLINT',
                ),
                'col_slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                ),
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('user_id', TRUE);

        $this->dbforge->create_table('colaboradores');

        $this->db->query("use " . DB_NAME_CORP);

        KLoader::model("UsuarioModel");

        $colaboradores = listar_professores_e_consultores();
        foreach ($colaboradores as $item) {
            UsuarioModel::atualizar_metadados($item['ID']);
        }
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('colaboradores');

        $this->db->query("use " . DB_NAME_CORP);
    }
}