<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_ordem_cabecalho extends CI_Migration {

        public function up()
        {
                $campos = [
                        'ordem_cabecalho' => [
                                'type' => 'VARCHAR(50)',
                                'default' => 'ID,Ano,Bancas,Órgãos,Provas,Disciplina,Assuntos'
                        ]
                ];

                $this->dbforge->add_column('configuracoes_usuarios', $campos);
        }

        public function down()
        {
                $this->dbforge->drop_column('configuracoes_usuarios', 'ordem_cabecalho');
        }
}