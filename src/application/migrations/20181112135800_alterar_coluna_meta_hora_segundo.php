<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_alterar_coluna_meta_hora_segundo extends CI_Migration {

    public function up()
    {

        $fields = array(
            'met_hbc' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => FALSE
            ),
        );
        $this->dbforge->add_column('coaching_metas', $fields);

        $this->dbforge->drop_column('coaching_metas', 'met_hbc_segundos');
        $this->dbforge->drop_column('coaching_metas', 'met_hbc_minutos');
     
    }

    public function down()
    {
        $this->dbforge->drop_column('coaching_metas', 'met_hbc');
        
        $fields = array(
            'met_hbc_minutos' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => FALSE
            ),
            'met_hbc_segundos' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => FALSE
            )
        );
        $this->dbforge->add_column('coaching_metas', $fields);

    }
}