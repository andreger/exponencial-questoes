<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_tipo_favorito extends CI_Migration
{
    public function up()
    {   

        $campos = [

            'qfa_tipo' => [
                'type' => 'INT',
            ],

        ];

        $this->dbforge->add_column('questoes_favoritas', $campos);

        $this->db->query("UPDATE exponenc_corp.questoes_favoritas SET qfa_tipo = " . QUESTAO_FAVORITA . " WHERE qfa_tipo IS NULL");

        $this->db->query("ALTER TABLE exponenc_corp.questoes_favoritas DROP PRIMARY KEY, ADD PRIMARY KEY(que_id, usu_id, qfa_tipo)");
        //alter table xx drop primary key, add primary key(k1, k2, k3);

    }

    public function down()
    {
        $this->dbforge->drop_column('questoes_favoritas', 'qfa_tipo');        
    }
}