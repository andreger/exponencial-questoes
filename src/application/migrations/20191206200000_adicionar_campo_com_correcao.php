<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_adicionar_campo_com_correcao extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $campos = [
            'pro_com_correcao' => [
                'type' => 'TINYINT',
                'default' => 0
            ],
            
            'pro_is_coaching' => [
                'type' => 'TINYINT',
                'default' => 0
            ],
        ];
        
        $this->dbforge->add_column('produtos', $campos);
        
        $this->db->query("use " . DB_NAME_CORP);
        
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_column('produtos', 'pro_com_correcao');
        $this->dbforge->drop_column('produtos', 'pro_is_coaching');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}