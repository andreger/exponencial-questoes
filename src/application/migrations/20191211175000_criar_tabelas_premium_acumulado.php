<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_tabelas_premium_acumulado extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $campos = [

            'ppc_data' => [
                'type' => 'DATE',
            ],
            'ppc_cota_sq' => [
                'type' => 'FLOAT',
                'constraint' => '10,2',
            ],
            'ppc_cota_professor' => [
                'type' => 'FLOAT',
                'constraint' => '10,2',
            ],
            'ppc_cota_sq' => [
                'type' => 'FLOAT',
                'constraint' => '10,2',
            ],
            'ppc_minimo_pgto' => [
                'type' => 'FLOAT',
                'constraint' => '10,2',
            ]
        ];

        $this->dbforge->add_field($campos);
        $this->dbforge->add_key('ppc_data', TRUE);
        $this->dbforge->create_table('premium_configs');
        
        
        $campos = [
            
            'ppa_periodo' => [
                'type' => 'VARCHAR',
                'constraint' => '6'
            ],
            'user_id' => [
                'type' => INT,
            ],
            'ppa_valor_acumulado' => [
                'type' => 'FLOAT',
                'constraint' => '10,2',
            ],
        ];
        
        $this->dbforge->add_field($campos);
        $this->dbforge->add_key('ppa_periodo', TRUE);
        $this->dbforge->add_key('user_id', TRUE);
        $this->dbforge->create_table('premium_pgtos_acumulados');
        
        $this->db->query("use " . DB_NAME_CORP);
        
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('premium_pgtos_acumulados');
        $this->dbforge->drop_table('premium_configs');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}