<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_conversao_default extends CI_Migration {

        public function up()
        {
                $campos = [
                        'dis_id' => [
                                'type' => 'INT',
                                'constraint' => 11
                        ]
                ];

                $this->dbforge->add_column('assuntos_conversao', $campos);
        }

        public function down()
        {
                $this->dbforge->drop_column('assuntos_conversao', 'dis_id');
        }
}