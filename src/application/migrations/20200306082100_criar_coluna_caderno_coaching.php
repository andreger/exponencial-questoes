<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_caderno_coaching extends CI_Migration {

    public function up()
    {
        
        $campos = [
            'cad_coaching' => [
                'type' => 'INT',
                'default' => 0,
                'NULL' => FALSE
            ],
        ];
        
        $this->dbforge->add_column('cadernos', $campos);
        
    }

    public function down()
    {
        
        $this->dbforge->drop_column('cadernos', 'cad_coaching');

    }

}