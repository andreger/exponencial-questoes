<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_forum_id extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $campos = [
            'pro_forum_id' => [
                'type' => 'INT',
                'null' => TRUE
            ],
        ];

        $this->dbforge->add_column('produtos', $campos);

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('produtos', 'pro_forum_id');

        $this->db->query("use " . DB_NAME_CORP);
    }
}