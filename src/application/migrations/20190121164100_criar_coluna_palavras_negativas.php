<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_palavras_negativas extends CI_Migration {

        public function up()
        {
                $this->db->query("use " . DB_NAME);

                $campos = [
                        'palavras_negativas' => [
                                'type' => 'TEXT',
                        ]
                ];

                $this->dbforge->add_column('saidas', $campos);
                $this->dbforge->add_column('avisos', $campos);

                $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
                $this->db->query("use " . DB_NAME);

                $this->dbforge->drop_column('saidas', 'palavras_negativas');
                $this->dbforge->drop_column('avisos', 'palavras_negativas');

                $this->db->query("use " . DB_NAME_CORP);
        }
}