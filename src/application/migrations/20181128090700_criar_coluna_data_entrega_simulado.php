<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_data_entrega_simulado extends CI_Migration {

    public function up()
    {
        $campos = [
            'sim_data_entrega' => [
                'type' => 'DATETIME',
            ],
        ];

        $this->dbforge->add_column('simulados', $campos);

    }

    public function down()
    {

        $this->dbforge->drop_column('simulados', 'sim_data_entrega');

    }
}