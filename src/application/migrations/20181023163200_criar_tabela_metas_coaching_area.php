<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabela_metas_coaching_area extends CI_Migration {

    public function up()
    {
        /**
         * Tabela coaching_metas_areas
         */ 
        
        $campos = [
                'met_id' => [
                        'type' => 'INT',
                        'constraint' => 11,
                ],
                'are_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                )
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('met_id', TRUE);
        $this->dbforge->add_key('are_id', TRUE);

        // Foreign key
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (met_id) REFERENCES coaching_metas(met_id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('coaching_metas_areas');
    }

    public function down()
    {
        $this->dbforge->drop_table('coaching_metas_areas');
    }
}