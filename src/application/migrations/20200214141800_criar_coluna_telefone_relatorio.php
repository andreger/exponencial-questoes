<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_telefone_relatorio extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $campos = [
            'ven_aluno_telefone' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'default' => null,
                'after' => 'ven_aluno_cpf'
            ],
        ];
        
        $this->dbforge->add_column('wp_vendas', $campos);
        
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_column('wp_vendas', 'ven_aluno_telefone');

        $this->db->query("use " . DB_NAME_CORP);
    }

}