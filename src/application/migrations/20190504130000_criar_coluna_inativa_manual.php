<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_inativa_manual extends CI_Migration
{
    public function up()
    {
        $campos = [
            'que_inativa_manual' => [
                'type' => 'TINYINT',
                'default' => 0,
            ]
        ];

        $this->dbforge->add_column('questoes', $campos, 'que_ativo');
    }

    public function down()
    {
        $this->dbforge->drop_column('questoes', 'que_inativa_manual');
    }
}