<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_adicionar_campo_tempo_acesso_produtos extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $campos = [
            'pro_tempo_acesso' => [
                'type' => 'INT',
                'NULL' => TRUE
            ],
        ];
        
        $this->dbforge->add_column('produtos', $campos);
        
        $campos = [
            'ped_data_base' => [
                'type' => 'DATE',
                'NULL' => TRUE
            ],
        ];
        
        $this->dbforge->add_column('pedido_produto', $campos);
        
        $this->db->query("use " . DB_NAME_CORP);
        
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_column('produtos', 'pro_tempo_acesso');
        $this->dbforge->drop_column('pedido_produto', 'ped_data_base');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}