<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_qtd_questoes_caderno extends CI_Migration
{
    public function up()
    {   

        $campos = [

            'cad_qtd_questoes' => [
                'type' => 'INT',
            ],

        ];

        $this->dbforge->add_column('cadernos', $campos);

        $this->db->query('update cadernos cad set cad.cad_qtd_questoes = (select count(*) from cadernos_questoes cq where cq.cad_id = cad.cad_id)');

    }

    public function down()
    {
        
        $this->dbforge->drop_column('cadernos', 'cad_qtd_questoes');
        
    }
}