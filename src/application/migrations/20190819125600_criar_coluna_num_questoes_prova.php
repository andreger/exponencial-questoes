<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_num_questoes_prova extends CI_Migration
{
    public function up()
    {   

        $campos = [

            'pro_num_questoes' => [
                'type' => 'INT',
            ],

        ];

        $this->dbforge->add_column('provas', $campos);

        $this->db->query('update provas pro set pro.pro_num_questoes = (select count(*) from questoes_provas qp where qp.pro_id = pro.pro_id)');

    }

    public function down()
    {
        $this->dbforge->drop_column('provas', 'pro_num_questoes');
        
    }
}