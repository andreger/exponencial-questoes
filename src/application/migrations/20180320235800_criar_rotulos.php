<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_Rotulos extends CI_Migration {

        public function up(){

            /**
             * Simulados_Rotulos
             */
            $this->dbforge->add_field(array(
                    'sro_id' => array(
                            'type' => 'INT',
                            'constraint' => 11,
                            'auto_increment' => TRUE
                    ),
                    'sro_nome' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '200'
                    ),
                    'sro_ordem' => array(
                            'type' => 'INT',
                            'constraint' => 3
                    ),
                    'sim_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                    ),
            ));
            
            //Primary key
            $this->dbforge->add_key('sro_id', TRUE);

            //Foreign key
            $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (sim_id) REFERENCES simulados(sim_id) ON DELETE CASCADE');
            
            //Unique key
            $this->dbforge->add_field('CONSTRAINT UNIQUE (sro_ordem, sim_id)');

            $this->dbforge->create_table('simulados_rotulos');

            /**
             * Simulados_Rotulos_Questoes
             */
            $this->dbforge->add_field(array(
                'sro_id' => array(
                        'type' => 'INT',
                        'constraint' => 11
                ),
                'que_id' => array(
                        'type' => 'INT',
                        'constraint' => '11'
                )
            ));

            //Primary key
            $this->dbforge->add_key('sro_id', TRUE);
            $this->dbforge->add_key('que_id', TRUE);
            
            //Foreign key
            $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (sro_id) REFERENCES simulados_rotulos(sro_id) ON DELETE CASCADE');
            $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (que_id) REFERENCES questoes(que_id) ON DELETE CASCADE');

            $this->dbforge->create_table('simulados_rotulos_disciplinas');
        }

        public function down(){
            $this->dbforge->drop_table('simulados_rotulos_disciplinas');
            $this->dbforge->drop_table('simulados_rotulos');
        }
}

?>