<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_corrigir_campo_procedencia_questao extends CI_Migration {

    public function up()
    {
        $sql = "UPDATE questoes que 
                    SET que.que_procedencia = 1 
                WHERE que.que_procedencia IS NULL 
                    AND que.que_id IN (SELECT q.que_id FROM questoes_provas q, provas p, bancas b WHERE b.ban_id = p.ban_id AND q.pro_id = p.pro_id AND b.ban_nome = 'Exponencial Concursos')";
        $this->db->query($sql);
        
    }

    public function down()
    {
        //Vazio, não possui rollback
    }
}