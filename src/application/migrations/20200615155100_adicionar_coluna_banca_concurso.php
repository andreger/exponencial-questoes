<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_adicionar_coluna_banca_concurso extends CI_Migration {
    
    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $fields = array(
            'con_banca' => array(
                'type' => 'VARCHAR',
                'constraint' => 200,
            ),
        );

        $this->dbforge->add_column('concursos', $fields);

        $this->db->query("use " . DB_NAME_CORP);
    }
    
    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('concursos', 'con_banca');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}