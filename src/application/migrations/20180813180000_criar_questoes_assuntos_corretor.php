<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_questoes_assuntos_corretor extends CI_Migration {

    public function up()
    {
        $campos = [
            'qac_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'que_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'qac_antes' => array(
                'type' => 'TEXT',
            ),
            'qac_depois' => array(
                'type' => 'TEXT',
            ),
            'qac_data_hora' => array(
                'type' => 'DATETIME',
            ),
        ];

        $this->dbforge->add_field($campos);
        $this->dbforge->add_key('qac_id', TRUE);

        $this->dbforge->create_table('questoes_assuntos_corretor');
    }

    public function down()
    {
        $this->dbforge->drop_table('questoes_assuntos_corretor');
    }
}