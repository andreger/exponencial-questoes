<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabela_produtos_concursos extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

         /**
         * Tabela concursos
         */ 

        $campos = [
                'con_id' => array(
                        'type' => 'INT',
                ),
                'con_qtde_cursos' => array(
                    'type' => 'SMALLINT',
                ),
                'con_slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                ),
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('con_id', TRUE);

        $this->dbforge->create_table('concursos');

        /**
         * Tabela produtos_concursos
         */ 

        $campos = [
                'post_id' => array(
                        'type' => 'INT',
                ),
                'con_id' => array(
                        'type' => 'INT',
                ),
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('post_id', TRUE);
        $this->dbforge->add_key('con_id', TRUE);

         // Foreign key
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (post_id) REFERENCES produtos(post_id) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (con_id) REFERENCES concursos(con_id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('produtos_concursos');

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('produtos_concursos');
        $this->dbforge->drop_table('concursos');

        $this->db->query("use " . DB_NAME_CORP);
    }
}