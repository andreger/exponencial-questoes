<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_saida_expiracao extends CI_Migration
{

    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_column('saidas', 'palavras_negativas');
        $this->dbforge->drop_column('saidas', 'horas');
        
        $campos = [
            'sai_palavras_negativas' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'sai_dias_expiracao' => [
                'type' =>  'SMALLINT',
                'null' => true
            ],
            
        ];

        $this->dbforge->add_column('saidas', $campos);
        
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_column('saidas', 'sai_palavras_negativas');
        $this->dbforge->drop_column('saidas', 'sai_dias_expiracao');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}