<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_tabela_mc_areas_interesses extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);
       
        $campos = [
            'mca_lista_id' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'mca_campo_id' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'mca_id' => array(
                'type' => 'VARCHAR',
                'constraint' => 20
            ),
            'mca_nome' => array(
                'type' => 'VARCHAR',
                'constraint' => 200
            ),
        ];
        
        $this->dbforge->add_field($campos);
        
        // Primary key
        $this->dbforge->add_key('mca_lista_id', TRUE);
        $this->dbforge->add_key('mca_campo_id', TRUE);
        $this->dbforge->add_key('mca_id', TRUE);
        
        $this->dbforge->create_table('mc_areas_interesses');
        
        
        $this->db->query("use " . DB_NAME_CORP);
    }
    
    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('mc_areas_interesses');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}