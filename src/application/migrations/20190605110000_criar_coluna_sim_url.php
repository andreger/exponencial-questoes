<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_sim_url extends CI_Migration
{
    public function up()
    {  
        $campos = [
            'sim_url_edital' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true
            ],
        ];

        $this->dbforge->add_column('simulados', $campos);
    }

    public function down()
    {
        $this->dbforge->drop_column('simulados', 'sim_url_edital');
    }
}