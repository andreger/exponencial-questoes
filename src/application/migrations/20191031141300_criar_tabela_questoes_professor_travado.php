<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_tabela_questoes_professor_travado extends CI_Migration {

    public function up()
    {
        
        $campos = [

            'qct_id' => [
                'type' => 'INT',
                'auto_increment' => TRUE
            ],
            'usu_id' => [
                'type' => 'INT',
                'null' => TRUE,
            ],
            'qct_total' => [
                'type' => 'INT',
                'null' => FALSE,
            ],

        ];

        $this->dbforge->add_field($campos);
        $this->dbforge->add_key('qct_id', TRUE);
        $this->dbforge->create_table('questoes_comentadas_professor_travado');
        
    }

    public function down()
    {
        
        $this->dbforge->drop_table('questoes_comentadas_professor_travado');

    }
}