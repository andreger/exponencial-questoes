<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_is_simulado extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $campos = [
            'pro_is_simulado' => [
                'type' => 'TINYINT',
            ],
        ];

        $this->dbforge->add_column('produtos', $campos);

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('produtos', 'pro_is_simulado');

        $this->db->query("use " . DB_NAME_CORP);
    }
}