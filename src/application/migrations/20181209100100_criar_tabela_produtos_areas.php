<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabela_produtos_areas extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

         /**
         * Tabela concursos
         */ 

        $campos = [
                'post_id' => array(
                        'type' => 'INT',
                ),
                'area_id' => array(
                    'type' => 'INT',
                )
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('post_id', TRUE);
        $this->dbforge->add_key('area_id', TRUE);

         // Foreign key
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (post_id) REFERENCES produtos(post_id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('produtos_areas');

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('produtos_areas');

        $this->db->query("use " . DB_NAME_CORP);
    }
}