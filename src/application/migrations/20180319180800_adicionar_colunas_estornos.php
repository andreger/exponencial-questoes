<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Adicionar_colunas_estornos extends CI_Migration {

        public function up()
        {
                $this->db->query("use " . DB_NAME);

                $campos = [
                        'est_motivo' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '500',
                        ),
                ];

                $this->dbforge->add_column('estornos', $campos);

                $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
                $this->db->query("use " . DB_NAME);

                $this->dbforge->drop_column('estornos', 'est_motivo');

                $this->db->query("use " . DB_NAME_CORP);
        }
}