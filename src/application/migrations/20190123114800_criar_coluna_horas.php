<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_horas extends CI_Migration {

        public function up()
        {
                $this->db->query("use " . DB_NAME);

                $campos = [
                        'horas' => [
                                'type' => 'TINYINT'
                        ]
                ];

                $this->dbforge->add_column('saidas', $campos);
                $this->dbforge->add_column('avisos', $campos);

                $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
                $this->db->query("use " . DB_NAME);

                $this->dbforge->drop_column('saidas', 'horas');
                $this->dbforge->drop_column('avisos', 'horas');

                $this->db->query("use " . DB_NAME_CORP);
        }
}