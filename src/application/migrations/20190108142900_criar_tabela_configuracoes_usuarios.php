<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabela_configuracoes_usuarios extends CI_Migration {

    public function up()
    {
       
         /**
         * Tabela configuracoes_usuarios
         */ 

        $campos = [
                'user_id' => array(
                    'type' => 'INT',
                ),
                'mostrar_acertos' => array(
                    'type' => 'TINYINT',
                    'default' => 1
                ),
                'exibir_filtros' => array(
                    'type' => 'TINYINT',
                    'default' => 1
                ),
                'manter_historico_marcacao_cadernos' => array(
                    'type' => 'TINYINT',
                    'default' => 0
                ),
        ];

        $this->dbforge->add_field($campos);

        // Primary key       
        $this->dbforge->add_key('user_id', TRUE);
       

        $this->dbforge->create_table('configuracoes_usuarios');

    }

    public function down()
    {               
        $this->dbforge->drop_table('configuracoes_usuarios');
    }
}