<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_corte_ranking extends CI_Migration {

        public function up()
        {
                $campos = [
                        'ran_corte' => [
                                'type' => 'INT',
                                'constraint' => 11
                        ]
                ];

                $this->dbforge->add_column('rankings', $campos);
        }

        public function down()
        {
                $this->dbforge->drop_column('rankings', 'ran_corte');
        }
}