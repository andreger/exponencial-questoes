<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabela_produtos_materias extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

         /**
         * Tabela materias
         */ 

        $campos = [
                'mat_id' => array(
                        'type' => 'INT',
                ),
                'mat_qtde_cursos' => array(
                    'type' => 'SMALLINT',
                ),
                'mat_slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '50',
                ),
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('mat_id', TRUE);

        $this->dbforge->create_table('materias');

        /**
         * Tabela produtos_materias
         */ 

        $campos = [
                'post_id' => array(
                        'type' => 'INT',
                ),
                'mat_id' => array(
                        'type' => 'INT',
                ),
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('post_id', TRUE);
        $this->dbforge->add_key('mat_id', TRUE);

         // Foreign key
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (post_id) REFERENCES produtos(post_id) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (mat_id) REFERENCES materias(mat_id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('produtos_materias');

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('produtos_materias');
        $this->dbforge->drop_table('materias');

        $this->db->query("use " . DB_NAME_CORP);
    }
}