<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_seo_imagens extends CI_Migration {

        public function up()
        {
                $this->db->query("use " . DB_NAME);

                $campos = [
                        'sie_id' => [
                                'type' => 'INT',
                                'constraint' => 11,
                                'auto_increment' => TRUE
                        ],
                        'sie_enviados' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'sie_processados' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'sie_com_erro' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'sie_qualidade' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'sie_data_hora' => array(
                                'type' => 'DATETIME',
                        )
                ];

                $this->dbforge->add_field($campos);
                $this->dbforge->add_key('sie_id', TRUE);

                $this->dbforge->create_table('seo_imagens_execucoes');

                $campos = [
                        'sid_id' => [
                                'type' => 'INT',
                                'constraint' => 11,
                                'auto_increment' => TRUE
                        ],
                        'sid_arquivo' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '500',
                        ),
                        'sid_tamanho_antes' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'sid_tamanho_depois' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'sid_status' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'sie_id' => [
                                'type' => 'INT',
                                'constraint' => 11,
                        ],
                ];

                $this->dbforge->add_field($campos);
                $this->dbforge->add_key('sid_id', TRUE);

                //Foreign key
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (sie_id) REFERENCES seo_imagens_execucoes(sie_id) ON DELETE CASCADE');

                $this->dbforge->create_table('seo_imagens_detalhes');

                $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
                $this->db->query("use " . DB_NAME);

                $this->dbforge->drop_table('seo_imagens_detalhes');
                $this->dbforge->drop_table('seo_imagens_execucoes');

                $this->db->query("use " . DB_NAME_CORP);
        }
}