<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_colunas_pontos_negativos extends CI_Migration {

        public function up()
        {
                $campos = [
                        'sim_pontos_negativos' => [
                                'type' => 'INT',
                                'constraint' => 11,
                                'default' => 0
                        ],
                        'sim_peso_erros' => [
                                'type' => 'INT',
                                'constraint' => 11
                        ]
                ];

                $this->dbforge->add_column('simulados', $campos);
        }

        public function down()
        {
                $this->dbforge->drop_column('simulados', 'sim_pontos_negativos');
                $this->dbforge->drop_column('simulados', 'sim_peso_erros');
        }
}