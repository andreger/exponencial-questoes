<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_editor extends CI_Migration
{
    public function up()
    {
        $campos = [
            'met_editor_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'null' => true
            ]
        ];

        $this->dbforge->add_column('coaching_metas', $campos);
    }

    public function down()
    {
        $this->dbforge->drop_column('coaching_metas', 'met_editor_id');
    }
}