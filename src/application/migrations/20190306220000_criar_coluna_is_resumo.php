<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_is_resumo extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $campos = [
            'pro_is_resumo' => [
                'type' => 'TINYINT',
            ],
            'pro_is_audiobook' => [
                'type' => 'TINYINT',
            ],
        ];

        $this->dbforge->add_column('produtos', $campos);

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('produtos', 'pro_is_resumo');
        $this->dbforge->drop_column('produtos', 'pro_is_audiobook');

        $this->db->query("use " . DB_NAME_CORP);
    }
}