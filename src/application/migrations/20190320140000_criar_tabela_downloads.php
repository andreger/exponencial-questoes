<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_tabela_downloads extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);
       
        $campos = [
            'dow_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
            'dow_relatorio_id' => array(
                'type' => 'TINYINT',
            ),
            'dow_filtros' => array(
                'type' => 'TEXT',
            ),
            'dow_data' => array(
                'type' => 'DATETIME',
            ),
            'dow_status' => array(
                'type' => 'TINYINT',
            ),
            'dow_tentativas' => array(
                'type' => 'TINYINT',
            ),
            'dow_arquivo' => array(
                'type' => 'VARCHAR',
                'constraint' => 200,
                'null' => true
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
            ),
        ];
        
        $this->dbforge->add_field($campos);
        
        // Primary key
        $this->dbforge->add_key('dow_id', TRUE);

        
        $this->dbforge->create_table('downloads');
        
        
        $this->db->query("use " . DB_NAME_CORP);
    }
    
    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('downloads');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}