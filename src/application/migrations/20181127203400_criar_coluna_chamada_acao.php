<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_chamada_acao extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $campos = [
            'pro_chamada_acao' => [
                'type' => 'TINYINT',
            ],
            'pro_is_caderno' => [
                'type' => 'TINYINT',
            ],
            'pro_is_visivel' => [
                'type' => 'TINYINT',
            ],
            'pro_carga_horaria' => [
                'type' => 'MEDIUMINT',
                
            ],
            'pro_botao_comprar' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
            ],
        ];

        $this->dbforge->add_column('produtos', $campos);

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('produtos', 'pro_chamada_acao');
        $this->dbforge->drop_column('produtos', 'pro_is_caderno');
        $this->dbforge->drop_column('produtos', 'pro_is_visivel');
        $this->dbforge->drop_column('produtos', 'pro_carga_horaria');
        $this->dbforge->drop_column('produtos', 'pro_botao_comprar');

        $this->db->query("use " . DB_NAME_CORP);
    }
}