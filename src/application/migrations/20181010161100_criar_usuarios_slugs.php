<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_usuarios_slugs extends CI_Migration {

        public function up()
        {
            global $wpdb;

            $usuarios = listar_professores_e_consultores();

            if($usuarios) {

                $i = 1;
                foreach ($usuarios as $item) {

                    $u_a = get_usuario_array($item['ID']);

                    $slug = sanitize_title($u_a['nome_completo']);

                    if(tem_usuario_slug_duplicado($item['ID'], $slug)) {
                        $slug .= "-{$i}";
                        $i++;
                    }

                    update_user_meta($item['ID'], "slug", $slug);
                }
            }
        }

        public function down()
        {
           global $wpdb;

           $sql = "DELETE FROM wp_usermeta WHERE meta_key = 'slug'";
           $wpdb->query($sql);
        }
}