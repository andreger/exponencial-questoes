<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_alterar_tabelas_rankings extends CI_Migration {

    public function up()
    {
        //Adicina colunas novas em rankings
        $fields = array(
            'ran_media_ponderada' => array(
                'type' => 'INT', 
                'NULL' => FALSE, 
                'DEFAULT' => 0
            ),
            'ran_peso_erros' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
                'NULL' => FALSE, 
                'DEFAULT' => 0
            ),
        );
        $this->dbforge->add_column('rankings', $fields);

        //Adiciona colunas novas em simulados
        $fields = array(
            'sim_media_ponderada' => array(
                'type' => 'INT', 
                'NULL' => FALSE, 
                'DEFAULT' => 0
            )
        );
        $this->dbforge->add_column('simulados', $fields);

        //Altera tipo de colunas em simulados
        $fields = array(
            'sim_peso_erros' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
        );
        $this->dbforge->modify_column('simulados', $fields);
        
        //Altera tipo de colunas em rankings
        $fields = array(
            'ran_corte' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
        );
        $this->dbforge->modify_column('rankings', $fields);

        //Altera tipo de colunas em rankings_disciplinas
        $fields = array(
            'rad_corte' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
            'rad_peso' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
        );
        $this->dbforge->modify_column('rankings_disciplinas', $fields);

        //Altera tipo de colunas em rankings_grupos
        $fields = array(
            'rag_corte' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
            'rag_peso' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
        );
        $this->dbforge->modify_column('rankings_grupos', $fields);

        //Altera tipo de colunas em rankings_simulados
        $fields = array(
            'ras_corte' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
            'ras_peso' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
        );
        $this->dbforge->modify_column('rankings_simulados', $fields);

        //Altera tipo de colunas em simulados_resultados
        $fields = array(
            'sre_resultado' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
        );
        $this->dbforge->modify_column('simulados_resultados', $fields);
        
    }

    public function down()
    {

        $this->dbforge->drop_column('rankings', 'ran_media_ponderada');
        $this->dbforge->drop_column('rankings', 'ran_peso_erros');
        $this->dbforge->drop_column('simulados', 'sim_media_ponderada');
        
        $fields = array(
            'ran_corte' => array(
                'type' => 'INT',
            ),
        );
        $this->dbforge->modify_column('rankings', $fields);

        $fields = array(
            'rad_corte' => array(
                'type' => 'INT',
            ),
            'rad_peso' => array(
                'type' => 'INT',
            ),
        );
        $this->dbforge->modify_column('rankings_disciplinas', $fields);

        $fields = array(
            'rag_corte' => array(
                'type' => 'INT',
            ),
            'rag_peso' => array(
                'type' => 'INT',
            ),
        );
        $this->dbforge->modify_column('rankings_grupos', $fields);

        $fields = array(
            'ras_corte' => array(
                'type' => 'INT',
            ),
            'ras_peso' => array(
                'type' => 'INT',
            ),
        );
        $this->dbforge->modify_column('rankings_simulados', $fields);

        $fields = array(
            'sim_peso_erros' => array(
                'type' => 'INT',
            ),
        );
        $this->dbforge->modify_column('simulados', $fields);

        $fields = array(
            'sre_resultado' => array(
                'type' => 'INT',
            ),
        );
        $this->dbforge->modify_column('simulados_resultados', $fields);

    }
}