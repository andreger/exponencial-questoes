<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_seo_h1 extends CI_Migration {

        public function up()
        {
                $this->db->query("use " . DB_NAME);

                $campos = [
                        'sh1_id' => [
                                'type' => 'INT',
                                'constraint' => 11,
                                'auto_increment' => TRUE
                        ],
                        'sh1_url' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '300'
                        ),
                        'sh1_titulo' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '300'
                        ),
                        'sh1_h1' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '500'
                        ),
                        'sh1_num' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'sh1_edit_url' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '200'
                        ),
                ];

                $this->dbforge->add_field($campos);
                $this->dbforge->add_key('sh1_id', TRUE);

                $this->dbforge->create_table('seo_h1');

                $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
                $this->db->query("use " . DB_NAME);

                $this->dbforge->drop_table('seo_h1');

                $this->db->query("use " . DB_NAME_CORP);
        }
}