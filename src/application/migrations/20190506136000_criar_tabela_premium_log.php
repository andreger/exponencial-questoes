<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_tabela_premium_log extends CI_Migration
{
    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $campos = [
            'pedido_id' => [
                'type' => 'INT',
            ],
            'produto_id' => [
                'type' => 'INT',
            ],
            'item_id' => [
                'type' => 'INT',
            ],
            'pedido_oculto_id' => [
                'type' => 'INT',
            ],
            'prl_data' => [
                'type' => 'DATETIME',
            ],
            
        ];

        $this->dbforge->add_field($campos);
        
        // Primary key
        $this->dbforge->add_key('pedido_id', TRUE);
        $this->dbforge->add_key('produto_id', TRUE);
        $this->dbforge->add_key('item_id', TRUE);
        
        $this->dbforge->create_table('premium_log');
        
        
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('premium_log');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}