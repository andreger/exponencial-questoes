<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_estornos extends CI_Migration {

        public function up()
        {
                $this->db->query("use " . DB_NAME);

                $campos = [
                        'est_id' => [
                                'type' => 'INT',
                                'constraint' => 11,
                                'auto_increment' => TRUE
                        ],
                        'usu_id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                        ),
                        'est_data_hora' => array(
                                'type' => 'DATETIME',
                        ),
                        'est_produtos_ids' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                        ),
                        'est_valor' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '20',
                        ),
                        'est_status' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                        )
                ];

                $this->dbforge->add_field($campos);
                $this->dbforge->add_key('est_id', TRUE);

                $this->dbforge->create_table('estornos');

                $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
                $this->db->query("use " . DB_NAME);

                $this->dbforge->drop_table('estornos');

                $this->db->query("use " . DB_NAME_CORP);
        }
}