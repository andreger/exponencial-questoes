<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_tabela_coaching_metas_agregadas extends CI_Migration {
    
    public function up()
    {
        
        /* Metas agregadas */
        $campos = [
            'mea_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ],
            'mea_nome' => array(
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => FALSE
            ),
            'mea_hbc' => array(
                'type' => 'VARCHAR',
                'constraint' => '10',
                'default' => '00:00',
                'null' => FALSE
            ),
            'mea_data_criacao' => array(
                'type' => 'DATETIME',
                'NULL' => FALSE
            ),
            'mea_criador_id' => array(
                'type' => 'INT',
                'NULL' => FALSE
            ),
            'mea_data_edicao' => array(
                'type' => 'DATETIME',
                'NULL' => FALSE,
            ),
            'mea_editor_id' => array(
                'type' => 'INT',
                'NULL' => FALSE
            ),
            'mea_anotacoes' => array(
                'type' => 'TEXT',
                'NULL' => TRUE
            ),
        ];
        //Campos
        $this->dbforge->add_field($campos);
        //Primary key
        $this->dbforge->add_key('mea_id', TRUE);
        //Tabela
        $this->dbforge->create_table('coaching_metas_agregadas');
        
        
        
        /* Metas agregadas metas atômicas */
        $campos = [
            'mea_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'met_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'mam_ordem' => [
                'type' => 'INT',
                'constraint' => 11,
                'null' => FALSE
            ],
        ];
        //Campos
        $this->dbforge->add_field($campos);
        //Primary key
        $this->dbforge->add_key('mea_id', TRUE);
        $this->dbforge->add_key('met_id', TRUE);
        // Foreign key
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (mea_id) REFERENCES coaching_metas_agregadas(mea_id) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (met_id) REFERENCES coaching_metas(met_id) ON DELETE CASCADE ON UPDATE CASCADE');
        //Tabela
        $this->dbforge->create_table('coaching_metas_agregadas_metas');
        
        
        
        /* Metas agregadas áreas */
        $campos = [
            'mea_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'are_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
        ];
        //Campos
        $this->dbforge->add_field($campos);        
        //Primary key
        $this->dbforge->add_key('mea_id', TRUE);
        $this->dbforge->add_key('are_id', TRUE);        
        // Foreign key
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (mea_id) REFERENCES coaching_metas_agregadas(mea_id) ON DELETE CASCADE ON UPDATE CASCADE');
        //Tabela
        $this->dbforge->create_table('coaching_metas_agregadas_areas');
        
    }
    
    public function down()
    {
        //Remove tabelas
        $this->dbforge->drop_table('coaching_metas_agregadas_areas');
        $this->dbforge->drop_table('coaching_metas_agregadas_metas');
        $this->dbforge->drop_table('coaching_metas_agregadas');
        
    }
}