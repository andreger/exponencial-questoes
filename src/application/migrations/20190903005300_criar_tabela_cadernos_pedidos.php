<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_tabela_cadernos_pedidos extends CI_Migration
{
    public function up()
    {   

        $campos = [

            'cpe_id' => [
                'type' => 'INT',
                'auto_increment' => TRUE
            ],
            'cad_id' => [
                'type' => 'INT',
                'unique' => TRUE,
                'null' => FALSE,
            ],
            'cpe_prd_id' => [
                'type' => 'INT',
                'null' => FALSE,
            ],
            'cpe_ped_id' => [
                'type' => 'INT',
                'null' => FALSE,
            ],

        ];

        $this->dbforge->add_field($campos);
        $this->dbforge->add_key('cpe_id', TRUE);
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (cad_id) REFERENCES cadernos(cad_id) ON DELETE CASCADE');
        $this->dbforge->create_table('cadernos_pedidos');

    }

    public function down()
    {
        $this->dbforge->drop_table('cadernos_pedidos');
    }
}