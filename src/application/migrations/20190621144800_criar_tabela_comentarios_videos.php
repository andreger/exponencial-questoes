<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_tabela_comentarios_videos extends CI_Migration
{
    public function up()
    {
        $this->db->query("use " . DB_NAME_CORP);
        
        $campos = [
            'cvi_id' => [
                'type' => 'INT',
                'auto_increment' => true
            ],
            'que_id' => [
                'type' => 'INT',
            ],
            'usu_id' => [
                'type' => 'INT',
            ],
            'cvi_url' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
            ],
            'cvi_data' => [
                'type' => 'DATETIME',
            ],
        ];

        $this->dbforge->add_field($campos);
        $this->dbforge->add_key('cvi_id', TRUE);
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (que_id) REFERENCES questoes(que_id) ON DELETE CASCADE');
        $this->dbforge->create_table('comentarios_videos');
        
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME_CORP);
        
        $this->dbforge->drop_table('comentarios_videos');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}