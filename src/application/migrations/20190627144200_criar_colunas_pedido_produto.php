<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_colunas_pedido_produto extends CI_Migration
{
    public function up()
    {   
        $this->db->query("use " . DB_NAME);

        $this->db->query('delete from pedido_produto');

        $campos = [

            'usu_id' => [
                'type' => 'INT',
                'null' => false
            ],
            'order_item_id' => [
                'type' => 'INT',
                'null' => false
            ],

        ];

        $this->dbforge->add_column('pedido_produto', $campos);

        $this->db->query("use " . DB_NAME_CORP);

    }

    public function down()
    {

        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('pedido_produto', 'usu_id');
        $this->dbforge->drop_column('pedido_produto', 'order_item_id');

        $this->db->query("use " . DB_NAME_CORP);
        
    }
}