<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_renomear_coluna_blogs_user_id extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $this->db->query("ALTER TABLE blogs CHANGE user_id user_id INT");

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->db->query("ALTER TABLE blogs CHANGE user_id user_id SMALLINT");

        $this->db->query("use " . DB_NAME_CORP);
    }
}