<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_hbc extends CI_Migration
{
    public function up()
    {
        $campos = [
            'met_hbc' => [
                'type' => 'VARCHAR',
                'constraint' => 5
            ]
        ];

        $this->dbforge->add_column('coaching_metas', $campos);
        $this->dbforge->drop_column('coaching_metas', 'met_hbc_minutos');
        $this->dbforge->drop_column('coaching_metas', 'met_hbc_segundos');
    }

    public function down()
    {
        $this->dbforge->drop_column('coaching_metas', 'met_hbc');
    }
}