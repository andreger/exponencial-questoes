<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_rodape_coluna extends CI_Migration
{
    public function up()
    {
    	$this->db->query("use " . DB_NAME);

    	$sql = "DELETE FROM rodape_itens WHERE roi_id > 0";
    	$this->db->query($sql);

    	$campos = [
    			'roi_coluna' => array(
    					'type' => 'INT',
    					'constraint' => true,
    			)
    	];

    	$this->dbforge->add_column('rodape_itens', $campos);

    	$data = array(
    			array(
    					'roi_titulo' => 'Resultados de Aprovação',
    					'roi_url' => '/resultados',
    					'roi_coluna' => 1,
    					'roi_index' => 0
    			),
    			array(
    					'roi_titulo' => 'Programa de Afiliados',
    					'roi_url' => 'https://exponencialconcursos.postaffiliatepro.com/affiliates/',
    					'roi_coluna' => 1,
    					'roi_index' => 1
    			),
    			array(
    					'roi_titulo' => 'Nossa Metodologia',
    					'roi_url' => '/metodologia-exponencial',
    					'roi_coluna' => 1,
    					'roi_index' => 2
    			),
    			array(
    					'roi_titulo' => 'Como Funciona',
    					'roi_url' => '/como-funciona',
    					'roi_coluna' => 1,
    					'roi_index' => 3
    			),
    			array(
    					'roi_titulo' => 'Quem Somos',
    					'roi_url' => '/quem-somos',
    					'roi_coluna' => 1,
    					'roi_index' => 4
    			),
    			array(
    					'roi_titulo' => 'Política de Privacidade',
    					'roi_url' => '/politica-de-privacidade',
    					'roi_coluna' => 2,
    					'roi_index' => 1
    			),
    			array(
    					'roi_titulo' => 'Perguntas Frequentes',
    					'roi_url' => '/perguntas-frequentes-portal',
    					'roi_coluna' => 2,
    					'roi_index' => 2
    			),
    			array(
    					'roi_titulo' => 'Termos de uso',
    					'roi_url' => '/termos-de-uso',
    					'roi_coluna' => 2,
    					'roi_index' => 3
    			),
    			array(
    					'roi_titulo' => 'Parceiros',
    					'roi_url' => '/parceiros',
    					'roi_coluna' => 2,
    					'roi_index' => 4
    			),


    	);
    	$this->db->insert_batch('rodape_itens', $data);

    	$this->db->query("use " . DB_NAME_CORP);

    }

    public function down()
    {
    	$this->db->query("use " . DB_NAME);

    	$this->dbforge->drop_column('rodape_itens', 'roi_coluna');

    	$this->db->query("use " . DB_NAME_CORP);

    }
}