<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_tabela_notificacoes extends CI_Migration
{
    public function up()
    {   

        $campos = [

            'not_id' => [
                'type' => 'INT',
                'auto_increment' => TRUE
            ],
            'com_id' => [
                'type' => 'INT',
                'null' => FALSE,
            ],
            'not_remetente_id' => [
                'type' => 'INT',
                'null' => FALSE,
            ],
            'not_destinatario_id' => [
                'type' => 'INT',
                'null' => FALSE,
            ],
            'not_data_criacao' => [
                'type' => 'DATETIME',
                'null' => FALSE
            ],
            'not_data_lida' => [
                'type' => 'DATETIME',
                'null' => TRUE
            ],
        ];

        $this->dbforge->add_field($campos);
        $this->dbforge->add_key('not_id', TRUE);
        $this->dbforge->create_table('notificacoes');

    }

    public function down()
    {
        $this->dbforge->drop_table('notificacoes');
    }
}