<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabela_colaboradores_slugs_historico extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $campos = [
                'col_slug' => array(
                    'type' => 'VARCHAR',
                    'constraint' => 50
                ),
                'user_id' => array(
                    'type' => 'INT',
                )
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('col_slug', TRUE);

        $this->dbforge->create_table('colaboradores_slugs_historico');

        //$this->db->query("INSERT INTO colaboradores_slugs_historico SELECT col_slug, user_id FROM colaboradores");

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('colaboradores_slugs_historico');

        $this->db->query("use " . DB_NAME_CORP);
    }
}