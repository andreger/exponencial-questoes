<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_exibir_assuntos extends CI_Migration {

        public function up()
        {
                $campos = [
                        'exibir_assuntos' => [
                                'type' => 'TINYINT',
                                'default' => 1
                        ]
                ];

                $this->dbforge->add_column('configuracoes_usuarios', $campos);
        }

        public function down()
        {
                $this->dbforge->drop_column('configuracoes_usuarios', 'exibir_assuntos');
        }
}