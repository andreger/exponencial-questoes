<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_configuracoes_usuarios extends CI_Migration {

    public function up()
    {
           
        $campos = [
                'exibir_url_imprimir_caderno' => array(
                    'type' => 'TINYINT',
                    'default' => 0
                ),
        ];

        $this->dbforge->add_column('configuracoes_usuarios', $campos);
        

    }

    public function down()
    {               
        $this->dbforge->drop_column('configuracoes_usuarios', 'exibir_url_imprimir_caderno');
    }
}