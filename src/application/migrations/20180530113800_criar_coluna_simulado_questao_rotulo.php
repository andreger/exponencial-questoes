<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_coluna_simulado_questao_rotulo extends CI_Migration {

        public function up()
        {
                $campos = [
                        'sro_id' => [
                                'type' => 'INT',
                                'constraint' => 11,
                        ]
                ];

                $this->dbforge->add_column('simulados_questoes', $campos);
        }

        public function down()
        {
                $this->dbforge->drop_column('simulados_questoes', 'sro_id');
        }
}