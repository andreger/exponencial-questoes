<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_tabela_depoimentos extends CI_Migration
{
    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $campos = [
            'dep_id' => [
                'type' => 'INT',
                'NULL' => FALSE
            ],
            'dep_slug' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'NULL' => FALSE
            ],
            'dep_tipo' => [
                'type' => 'INT',
                'NULL' => TRUE
            ],
            'dep_url' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'NULL' => TRUE
            ]
        ];

        $this->dbforge->add_field($campos);
        
        // Primary key
        $this->dbforge->add_key('dep_id', TRUE);
        
        $this->dbforge->create_table('depoimentos');
        
        
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('depoimentos');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}