<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_alterar_cascade_rankings extends CI_Migration {

    public function up()
    {
        $this->db->query("ALTER TABLE rankings_disciplinas DROP FOREIGN KEY fk_rankings_disciplinas_rankings1");
        $this->db->query("ALTER TABLE rankings_disciplinas ADD CONSTRAINT fk_rankings_disciplinas_rankings1 FOREIGN KEY (ran_id) REFERENCES rankings(ran_id) ON DELETE CASCADE");

    }

    public function down()
    {

        $this->db->query("ALTER TABLE rankings_disciplinas DROP FOREIGN KEY fk_rankings_disciplinas_rankings1");
        $this->db->query("ALTER TABLE rankings_disciplinas ADD CONSTRAINT fk_rankings_disciplinas_rankings1 FOREIGN KEY (ran_id) REFERENCES rankings(ran_id) ON DELETE NO ACTION");

    }
}