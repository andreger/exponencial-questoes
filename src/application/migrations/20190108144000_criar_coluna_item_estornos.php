<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_item_estornos extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $campos = [
            'est_itens_ids' => [
                'type' => 'VARCHAR',
                'constraint' => 200,
                'null' => TRUE
            ],
        ];

        $this->dbforge->add_column('estornos', $campos);

        $this->db->query("use " . DB_NAME_CORP);

    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('estornos', 'est_itens_ids');

        $this->db->query("use " . DB_NAME_CORP);

    }
}