<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_colunas_arquivos_provas extends CI_Migration {

    public function up()
    {
        
        $campos = [
            'pro_prefixo_nome_arquivo' => [
                'type' => 'VARCHAR',
                'CONSTRAINT' => 512
            ],
        ];
        
        $this->dbforge->add_column('provas', $campos);
        
    }

    public function down()
    {
        
        $this->dbforge->drop_column('provas', 'pro_prefixo_nome_arquivo');

    }

}