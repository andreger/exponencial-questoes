<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabela_produtos_relacionados extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

         /**
         * Tabela blogs
         */ 

        $campos = [
                'post_id' => array(
                    'type' => 'INT',
                ),
                'rel_id' => array(
                    'type' => 'INT',
                )
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('post_id', TRUE);
        $this->dbforge->add_key('rel_id', TRUE);

        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (post_id) REFERENCES produtos(post_id) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (rel_id) REFERENCES produtos(post_id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('produtos_relacionados');

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('produtos_relacionados');

        $this->db->query("use " . DB_NAME_CORP);
    }
}