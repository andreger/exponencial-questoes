<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_data_prova extends CI_Migration
{

    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $campos = [
            'con_data_prova' => [
                'type' => 'DATE',
                'null' => true
            ]
        ];

        $this->dbforge->add_column('concursos', $campos);
        
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_column('concursos', 'con_data_prova');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}