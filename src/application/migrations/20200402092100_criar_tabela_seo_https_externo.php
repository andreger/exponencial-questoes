<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_tabela_seo_https_externo extends CI_Migration {
    
    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        /* SEO HTTPS Externo */
        $campos = [
            'she_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'NULL' => FALSE,
                'auto_increment' => TRUE
            ],
            'she_tipo' => [
                'type' => 'VARCHAR',
                'constraint' => 20,
                'NULL' => FALSE
            ],
            'she_titulo' => [
                'type' => 'TEXT',
                'NULL' => FALSE
            ],
            'she_data_atualizacao' => [
                'type' => 'DATETIME',
                'NULL' => FALSE
            ],
            'she_edit_url' => [
                'type' => 'VARCHAR',
                'constraint' => 300,
                'NULL' => FALSE
            ],
            
        ];
        //Campos
        $this->dbforge->add_field($campos);
        //Primary key
        $this->dbforge->add_key('she_id', TRUE);
        //Tabela
        $this->dbforge->create_table('seo_https_externo');
        
        $this->db->query("use " . DB_NAME_CORP);

    }
    
    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        //Remove tabelas
        $this->dbforge->drop_table('seo_https_externo');
        
        $this->db->query("use " . DB_NAME_CORP);
        
    }
}