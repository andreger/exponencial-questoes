<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_data_criacao_conversao extends CI_Migration {

    public function up()
    {
        $campos = [
            'asc_data_criacao' => [
                'type' => 'DATETIME',
                'null' => TRUE
            ],
        ];

        $this->dbforge->add_column('assuntos_conversao', $campos);
    }

    public function down()
    {
        $this->dbforge->drop_column('assuntos_conversao', 'asc_data_criacao');
    }
}