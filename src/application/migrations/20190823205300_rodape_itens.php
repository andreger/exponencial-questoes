<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_rodape_itens extends CI_Migration
{
    public function up()
    {
    	$this->db->query("use " . DB_NAME);

    	$campos = [
    			'roi_id' => array(
    					'type' => 'INT',
    					'constraint' => 11,
    					'auto_increment' => TRUE
    			),
		    	'roi_titulo' => array(
		    			'type' => 'VARCHAR',
		    			'constraint' => 200,
		    			'null' => true
		    	),
    			'roi_url' => array(
    					'type' => 'VARCHAR',
    					'constraint' => 200,
    					'null' => true
    			),
    			'roi_index' => array(
    					'type' => 'INT',
    					'constraint' => true,
    			)
    	];

    	$this->dbforge->add_field($campos);

    	// Primary key
    	$this->dbforge->add_key('roi_id', TRUE);


    	$this->dbforge->create_table('rodape_itens');

    	$data = array(
    			array(
    					'roi_titulo' => 'Resultados de Aprovação',
    					'roi_url' => '/resultados',
    					'roi_index' => 0
    			),
    			array(
    					'roi_titulo' => 'Programa de Afiliados',
    					'roi_url' => 'https://exponencialconcursos.postaffiliatepro.com/affiliates/',
    					'roi_index' => 1
    			),
    			array(
    					'roi_titulo' => 'Nossa Metodologia',
    					'roi_url' => '/metodologia-exponencial',
    					'roi_index' => 2
    			),
    			array(
    					'roi_titulo' => 'Como Funciona',
    					'roi_url' => '/como-funciona',
    					'roi_index' => 3
    			),
    			array(
    					'roi_titulo' => 'Quem Somos',
    					'roi_url' => '/quem-somos',
    					'roi_index' => 4
    			),
    			array(
    					'roi_titulo' => 'Política de Privacidade',
    					'roi_url' => '/politica-de-privacidade',
    					'roi_index' => 5
    			),
    			array(
    					'roi_titulo' => 'Perguntas Frequentes',
    					'roi_url' => '/perguntas-frequentes-portal',
    					'roi_index' => 6
    			),
    			array(
    					'roi_titulo' => 'Termos de uso',
    					'roi_url' => '/termos-de-uso',
    					'roi_index' => 7
    			),
    			array(
    					'roi_titulo' => 'Parceiros',
    					'roi_url' => '/parceiros',
    					'roi_index' => 8
    			),


    	);
    	$this->db->insert_batch('rodape_itens', $data);



    	$this->db->query("use " . DB_NAME_CORP);

    }

    public function down()
    {
    	$this->db->query("use " . DB_NAME);

    	$this->dbforge->drop_table('rodape_itens');

    	$this->db->query("use " . DB_NAME_CORP);

    }
}