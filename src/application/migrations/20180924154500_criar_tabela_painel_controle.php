<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_tabela_painel_controle extends CI_Migration {

        public function up()
        {
                $this->db->query("use " . DB_NAME);

                $campos = [
                        'pai_id' => [
                                'type' => 'INT',
                                'constraint' => 11
                        ],
                        'pai_area' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'null' => FALSE
                        ),
                        'pai_exibe' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => FALSE,
                                'default' => 1
                        ),
                        'pai_ativo' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => FALSE,
                                'default' => 1
                        )
                ];

                $this->dbforge->add_field($campos);
                $this->dbforge->add_key('pai_id', TRUE);

                $this->dbforge->create_table('painel_controle');


                /*
                1 - SQ - Geral
                2 - SQ - Filtro de Questões
                3 - SQ - Simulados
                4 - SQ - Cadernos
                5 - SQ - Meu desempenho
                6 - SQ - Administração
                7 - Inscrição no Coaching
                8 - Fórum
                9 - Scripts/Cronjobs
                10 - Login de usuário
                */
                $this->db->insert('painel_controle', array('pai_id' => 1, 'pai_area' => 'SQ - Geral'));
                $this->db->insert('painel_controle', array('pai_id' => 2, 'pai_area' => 'SQ - Filtro de Questões'));
                $this->db->insert('painel_controle', array('pai_id' => 3, 'pai_area' => 'SQ - Simulados'));
                $this->db->insert('painel_controle', array('pai_id' => 4, 'pai_area' => 'SQ - Cadernos'));
                $this->db->insert('painel_controle', array('pai_id' => 5, 'pai_area' => 'SQ - Meu desempenho'));
                $this->db->insert('painel_controle', array('pai_id' => 6, 'pai_area' => 'SQ - Administração'));
                $this->db->insert('painel_controle', array('pai_id' => 7, 'pai_area' => 'Inscrição no Coaching'));
                $this->db->insert('painel_controle', array('pai_id' => 8, 'pai_area' => 'Fórum'));
                $this->db->insert('painel_controle', array('pai_id' => 9, 'pai_area' => 'Scripts/Cronjobs', 'pai_exibe' => 0));
                $this->db->insert('painel_controle', array('pai_id' => 10, 'pai_area' => 'Login de usuário'));

                $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
                $this->db->query("use " . DB_NAME);

                $this->dbforge->drop_table('painel_controle');

                $this->db->query("use " . DB_NAME_CORP);
        }
}