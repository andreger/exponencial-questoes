<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_alterar_coluna_ven_data extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $this->db->query("ALTER TABLE wp_vendas CHANGE COLUMN ven_data ven_data DATETIME");

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->db->query("ALTER TABLE wp_vendas CHANGE COLUMN ven_data ven_data DATE");

        $this->db->query("use " . DB_NAME_CORP);
    }
}