<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_tabela_pedido_produto extends CI_Migration
{
    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        /* === Tabela premium_info === */
        $campos = [
            'pedido_id' => [
                'type' => 'INT',
            ],
            'produto_id' => [
                'type' => 'INT',
            ],
            'status_usuario' => [
                'type' => 'INT',
            ],
        ];

        $this->dbforge->add_field($campos);
        $this->dbforge->create_table('pedido_produto');
                
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_table('pedido_produto');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}