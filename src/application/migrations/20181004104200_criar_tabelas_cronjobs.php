<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_tabelas_cronjobs extends CI_Migration {

        public function up()
        {
                $this->db->query("use " . DB_NAME);

                $campos = [
                        'cro_id' => [
                                'type' => 'INT',
                                'constraint' => 11,
                                'auto_increment' => TRUE
                        ],
                        'cro_minuto' => [
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'null' => FALSE
                        ],
                        'cro_hora' => [
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'null' => FALSE
                        ],
                        'cro_dia' => [
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'null' => FALSE
                        ],
                        'cro_mes' => [
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'null' => FALSE
                        ],
                        'cro_dia_semana' => [
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'null' => FALSE
                        ],
                        'cro_comando' => [
                                'type' => 'VARCHAR',
                                'constraint' => '500',
                                'null' => FALSE
                        ],
                        'cro_descricao' => [
                                'type' => 'TEXT',
                                'null' => TRUE
                        ],
                        'cro_status' => [
                                'type' => 'INT',
                                'constraint' => 11,
                                'null' => FALSE,
                                'default' => 1
                        ]                       
                ];

                $this->dbforge->add_field($campos);
                $this->dbforge->add_key('cro_id', TRUE);

                $this->dbforge->create_table('cronjobs');


                $campos = [
                        'cro_id' => [
                                'type' => 'INT',
                                'constraint' => 11
                        ],
                        'crt_minuto' => [
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'null' => FALSE
                        ],
                        'crt_hora' => [
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'null' => FALSE
                        ],
                        'crt_dia' => [
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'null' => FALSE
                        ],
                        'crt_mes' => [
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'null' => FALSE
                        ],
                        'crt_dia_semana' => [
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                                'null' => FALSE
                        ]                       
                ];

                $this->dbforge->add_field($campos);

                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (cro_id) REFERENCES cronjobs(cro_id) ON DELETE CASCADE');

                $this->dbforge->create_table('cronjobs_table');

                $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
                $this->db->query("use " . DB_NAME);

                $this->dbforge->drop_table('cronjobs_table');
                $this->dbforge->drop_table('cronjobs');

                $this->db->query("use " . DB_NAME_CORP);
        }
}