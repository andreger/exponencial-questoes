<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_coluna_caderno_questao_ordem extends CI_Migration {

        public function up()
        {
                $campos = [
                        'caq_ordem' => [
                                'type' => 'INT',
                                'constraint' => 11,
                        ]
                ];

                $this->dbforge->add_column('cadernos_questoes', $campos);
        }

        public function down()
        {
                $this->dbforge->drop_column('cadernos_questoes', 'sro_id');
        }
}