<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_refactoring_blog extends CI_Migration {
    
    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $fields = array(
            'blo_titulo' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'default' => null,
                'after' => 'blo_thumb'
            ),
            'blo_conteudo' => array(
                'type' => 'TEXT',
                'default' => null,
                'after' => 'blo_titulo'
            ),
            'blo_created_on' => array(
                'type' => 'DATETIME',
                'default' => null,
                'after' => 'blo_resumo'
            ),
            'blo_updated_on' => array(
                'type' => 'DATETIME',
                'default' => null,
                'after' => 'blo_created_on'
            )
        );

        $this->dbforge->add_column('blogs', $fields);

        $this->db->query("use " . DB_NAME_CORP);
    }
    
    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('blogs', 'blo_titulo');
        $this->dbforge->drop_column('blogs', 'blo_conteudo');
        $this->dbforge->drop_column('blogs', 'blo_created_on');
        $this->dbforge->drop_column('blogs', 'blo_updated_on');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}