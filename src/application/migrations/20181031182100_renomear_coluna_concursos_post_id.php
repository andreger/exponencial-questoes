<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_renomear_coluna_concursos_post_id extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $this->db->query("ALTER TABLE concursos CHANGE post_id con_post_id INT");

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->db->query("ALTER TABLE concursos CHANGE con_post_id post_id INT");

        $this->db->query("use " . DB_NAME_CORP);
    }
}