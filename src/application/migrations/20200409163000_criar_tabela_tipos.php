<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_tabela_tipos extends CI_Migration {
    
    public function up()
    {
        $this->db->query("use " . DB_NAME);

        /* Metas agregadas */
        $campos = [
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'nome' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
            ),
        ];
        //Campos
        $this->dbforge->add_field($campos);
        //Primary key
        $this->dbforge->add_key('id', TRUE);
        //Tabela
        $this->dbforge->create_table('tipos');

        $this->db->insert("tipos", [
            'id' => 1,
            'nome' => 'Notícias'
        ]);

        $this->db->insert("tipos", [
            'id' => 2,
            'nome' => 'Artigos'
        ]);

        $this->db->insert("tipos", [
            'id' => 14,
            'nome' => 'Vídeos'
        ]);

        $this->db->query("ALTER TABLE blogs_tipos ADD CONSTRAINT blogs_tipos_ibfk_2 
            FOREIGN KEY (blo_tipo) REFERENCES tipos(id) ON DELETE CASCADE ON UPDATE CASCADE");

        $this->db->query("use " . DB_NAME_CORP);
    }
    
    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->db->query("ALTER TABLE blogs_tipos DROP CONSTRAINT blogs_tipos_ibfk_2");

        //Remove tabelas
        $this->dbforge->drop_table('tipos');

        $this->db->query("use " . DB_NAME_CORP);
    }
}