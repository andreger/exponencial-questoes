<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_novas_colunas_aviso extends CI_Migration
{

    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_column('avisos', 'horas');
        
        $campos = [
            'urls_negativas' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'dias_expiracao' => [
                'type' =>  'SMALLINT',
                'null' => true
            ],
            
        ];

        $this->dbforge->add_column('avisos', $campos);
        
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_column('avisos', 'urls_negativas');
        $this->dbforge->drop_column('avisos', 'dias_expiracao');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}