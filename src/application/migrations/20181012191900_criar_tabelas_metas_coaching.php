<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Criar_tabelas_metas_coaching extends CI_Migration {

    public function up()
    {
        /**
         * Tabela coaching_metas
         */ 
        
        $campos = [
                'met_id' => [
                        'type' => 'INT',
                        'constraint' => 11,
                        'auto_increment' => TRUE
                ],
                'met_nome' => array(
                        'type' => 'VARCHAR',
                        'constraint' => '200',
                ),
                'met_orientacoes' => array(
                        'type' => 'TEXT'
                ),
                'met_hbc_minutos' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                ),
                'met_hbc_segundos' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                ),
                'met_anotacoes' => array(
                        'type' => 'TEXT'
                ),
                'met_data_criacao' => array(
                        'type' => 'DATETIME',
                ),
                'met_data_edicao' => array(
                        'type' => 'DATETIME',
                ),
                'usu_id' => array(
                       'type' => 'INT',
                       'constraint' => 11,
                )
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('met_id', TRUE);

        $this->dbforge->create_table('coaching_metas');

        /**
         * Tabela coaching_metas_disciplinas
         */ 
        
        $campos = [
                'met_id' => [
                        'type' => 'INT',
                        'constraint' => 11,
                ],
                'dis_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                )
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('met_id', TRUE);
        $this->dbforge->add_key('dis_id', TRUE);

        // Foreign key
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (met_id) REFERENCES coaching_metas(met_id) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (dis_id) REFERENCES disciplinas(dis_id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('coaching_metas_disciplinas');

        /**
         * Tabela coaching_metas_assuntos
         */ 
        
        $campos = [
                'met_id' => [
                        'type' => 'INT',
                        'constraint' => 11,
                ],
                'ass_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                )
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('met_id', TRUE);
        $this->dbforge->add_key('ass_id', TRUE);

        // Foreign key
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (met_id) REFERENCES coaching_metas(met_id) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (ass_id) REFERENCES assuntos(ass_id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('coaching_metas_assuntos');

        /**
         * Tabela coaching_metas_cadernos
         */ 
        
        $campos = [
                'met_id' => [
                        'type' => 'INT',
                        'constraint' => 11,
                ],
                'cad_id' => array(
                        'type' => 'INT',
                        'constraint' => 11,
                )
        ];

        $this->dbforge->add_field($campos);

        // Primary key
        $this->dbforge->add_key('met_id', TRUE);
        $this->dbforge->add_key('cad_id', TRUE);

        // Foreign key
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (met_id) REFERENCES coaching_metas(met_id) ON DELETE CASCADE ON UPDATE CASCADE');
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (cad_id) REFERENCES cadernos(cad_id) ON DELETE CASCADE ON UPDATE CASCADE');

        $this->dbforge->create_table('coaching_metas_cadernos');

    }

    public function down()
    {
        $this->dbforge->drop_table('coaching_metas_cadernos');
        $this->dbforge->drop_table('coaching_metas_assuntos');
        $this->dbforge->drop_table('coaching_metas_disciplinas');
        $this->dbforge->drop_table('coaching_metas');
    }
}