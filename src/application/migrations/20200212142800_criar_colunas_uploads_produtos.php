<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_colunas_uploads_produtos extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $campos = [
            'pro_upload_caderno_nao_realizado' => [
                'type' => 'TINYINT',
                'default' => 0,
                'after' => 'pro_upload_nao_realizado'
            ],
            'pro_upload_video_nao_realizado' => [
                'type' => 'TINYINT',
                'default' => 0,
                'after' => 'pro_upload_nao_realizado'
            ],
            'pro_upload_caderno_pendente' => [
                'type' => 'TINYINT',
                'default' => 0,
                'after' => 'pro_upload_pendente'
            ],
            'pro_upload_video_pendente' => [
                'type' => 'TINYINT',
                'default' => 0,
                'after' => 'pro_upload_pendente'
            ],
        ];
        
        $this->dbforge->add_column('produtos', $campos);
        
        $campos = [
            'pau_tem_caderno' => [
                'type' => 'TINYINT',
                'default' => 0,
                'after' => 'pau_tem_arquivo'
            ],
            'pau_tem_video' => [
                'type' => 'TINYINT',
                'default' => 0,
                'after' => 'pau_tem_arquivo'
            ]
        ];
        
        $this->dbforge->add_column('produtos_aulas', $campos);
        
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_column('produtos_aulas', 'pau_tem_video');
        $this->dbforge->drop_column('produprodutos_aulastos', 'pau_tem_caderno');
        
        $this->dbforge->drop_column('produtos', 'pro_upload_caderno_pendente');
        $this->dbforge->drop_column('produtos', 'pro_upload_video_pendente');
        $this->dbforge->drop_column('produtos', 'pro_upload_caderno_nao_realizado');
        $this->dbforge->drop_column('produtos', 'pro_upload_video_nao_realizado');

        $this->db->query("use " . DB_NAME_CORP);
    }

}