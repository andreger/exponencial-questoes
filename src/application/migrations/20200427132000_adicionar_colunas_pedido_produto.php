<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_adicionar_colunas_pedido_produto extends CI_Migration {
    
    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $fields = array(
            'ppr_nome_item' => array(
                'type' => 'TEXT',
                'default' => null,
                'after' => 'order_item_id'
            ),
        );

        $this->dbforge->add_column('pedido_produto', $fields);

        $this->db->query("UPDATE pedido_produto pp SET pp.ppr_nome_item = (SELECT oi.order_item_name FROM wp_woocommerce_order_items oi WHERE oi.order_item_id = pp.order_item_id)");

        $this->db->query("ALTER TABLE pedido_produto ADD PRIMARY KEY (pedido_id, produto_id), ADD INDEX usu_id_idx (usu_id)");

        $this->db->query("use " . DB_NAME_CORP);
    }
    
    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('pedido_produto', 'ppr_nome_item');

        $this->db->query("ALTER TABLE pedido_produto DROP PRIMARY KEY");

        $this->db->query("ALTER TABLE pedido_produto DROP INDEX usu_id_idx");
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}