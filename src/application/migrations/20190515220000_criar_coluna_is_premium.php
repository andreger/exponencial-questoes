<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_is_premium extends CI_Migration
{
    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $campos = [
            'pro_is_assinatura_sq' => [
                'type' => 'INT',
            ],
            'pro_is_premium' => [
                'type' => 'INT',
            ],
        ];

        $this->dbforge->add_column('produtos', $campos);
        
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_column('produtos', 'pro_is_assinatura_sq');
        $this->dbforge->drop_column('produtos', 'pro_is_premium');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}