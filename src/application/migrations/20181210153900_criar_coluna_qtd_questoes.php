<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_qtd_questoes extends CI_Migration {

    public function up()
    {
        $campos = [
            'dis_qtd_questoes' => [
                'type' => 'INT',
                'null' => TRUE
            ],
        ];

        $this->dbforge->add_column('disciplinas', $campos);

    }

    public function down()
    {

        $this->dbforge->drop_column('disciplinas', 'dis_qtd_questoes');

    }
}