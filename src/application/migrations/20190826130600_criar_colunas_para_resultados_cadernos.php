<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_colunas_para_resultados_cadernos extends CI_Migration
{
    public function up()
    {   

        $campos = [

            'cad_qtd_erros' => [
                'type' => 'INT',
            ],

            'cad_qtd_acertos' => [
                'type' => 'INT',
            ],

            'cad_qtd_questoes_usuario' => [
               'type' => 'INT',
            ],

        ];

        $this->dbforge->add_column('cadernos', $campos);

    }

    public function down()
    {
        $this->dbforge->drop_column('cadernos', 'cad_qtd_erros');
        $this->dbforge->drop_column('cadernos', 'cad_qtd_acertos');
        $this->dbforge->drop_column('cadernos', 'cad_qtd_questoes_usuario');
        
    }
}