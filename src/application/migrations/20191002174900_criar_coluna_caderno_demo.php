<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_caderno_demo extends CI_Migration
{
    public function up()
    {   

        $campos = [

            'cad_demo' => [
                'type' => 'INT',
                'default' => 0,
                'null' => FALSE
            ],

        ];

        $this->dbforge->add_column('cadernos', $campos);

    }

    public function down()
    {
        $this->dbforge->drop_column('cadernos', 'cad_demo');        
    }
}