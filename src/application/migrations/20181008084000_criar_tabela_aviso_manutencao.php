<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_tabela_aviso_manutencao extends CI_Migration {

        public function up()
        {
            $this->db->query("use " . DB_NAME);

            $campos = [
                    'pca_id' => [
                            'type' => 'INT',
                            'constraint' => 11,
                            'auto_increment' => TRUE
                    ],
                    'pca_email' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '200',
                            'null' => FALSE
                    ),
                    'pca_data_hora' => array(
                            'type' => 'TIMESTAMP',
                            'null' => FALSE
                    )
            ];

            $this->dbforge->add_field($campos);
            $this->dbforge->add_key('pca_id', TRUE);

            $this->dbforge->create_table('painel_controle_aviso');

            $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
            $this->db->query("use " . DB_NAME);

            $this->dbforge->drop_table('painel_controle_aviso');

            $this->db->query("use " . DB_NAME_CORP);
        }
}