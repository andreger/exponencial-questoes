<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_colunas_tipo_produto extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $campos = [
            'pro_is_pdf' => [
                'type' => 'TINYINT',
            ],
            'pro_is_video' => [
                'type' => 'TINYINT',
            ],
            'pro_is_turma_coaching' => [
                'type' => 'TINYINT',
            ]
        ];

        $this->dbforge->add_column('produtos', $campos);

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('produtos', 'pro_is_pdf');
        $this->dbforge->drop_column('produtos', 'pro_is_video');
        $this->dbforge->drop_column('produtos', 'pro_is_turma_coaching');

        $this->db->query("use " . DB_NAME_CORP);
    }
}