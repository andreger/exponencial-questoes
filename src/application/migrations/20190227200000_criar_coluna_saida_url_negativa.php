<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_saida_url_negativa extends CI_Migration
{

    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        $campos = [
            'sai_urls_negativas' => [
                'type' => 'TEXT',
                'null' => true
            ],
        ];

        $this->dbforge->add_column('saidas', $campos);
        
        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        $this->dbforge->drop_column('saidas', 'sai_urls_negativas');
        
        $this->db->query("use " . DB_NAME_CORP);
    }
}