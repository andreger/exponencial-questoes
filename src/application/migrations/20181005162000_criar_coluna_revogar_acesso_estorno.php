<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_revogar_acesso_estorno extends CI_Migration {

        public function up()
        {
                $this->db->query("use " . DB_NAME);

                $campos = [
                        'est_revoga_acesso' => array(
                                'type' => 'INT',
                                'constraint' => '11'
                        )
                ];


                $this->dbforge->add_column('estornos', $campos);

                $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
                $this->db->query("use " . DB_NAME);

                $this->dbforge->drop_column('estornos', 'est_revoga_acesso');

                $this->db->query("use " . DB_NAME_CORP);
        }
}