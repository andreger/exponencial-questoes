<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_coluna_tipo_estorno extends CI_Migration {

        public function up()
        {
                $this->db->query("use " . DB_NAME);

                $campos = [
                        'est_tipo' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '2'
                        )
                ];


                $this->dbforge->add_column('estornos', $campos);

                $this->db->where('length(est_produtos_ids) = 0');
		$this->db->set('est_tipo', 'V');
		$this->db->update('estornos');

                $this->db->where('length(est_produtos_ids) > 0');
		$this->db->set('est_tipo', 'P');
		$this->db->update('estornos');

                $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
                $this->db->query("use " . DB_NAME);

                $this->dbforge->drop_column('estornos', 'est_tipo');

                $this->db->query("use " . DB_NAME_CORP);
        }
}