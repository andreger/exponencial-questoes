<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_criar_coluna_data_pedido_nota_fiscal extends CI_Migration
{
    public function up()
    {   
        $this->db->query("use " . DB_NAME);

        $campos = [

            'nf_data_pedido' => [
                'type' => 'VARCHAR',
                'constraint' => '45',
            ],

        ];

        $this->dbforge->add_column('notas_fiscais', $campos);

        $this->db->query('UPDATE notas_fiscais SET nf_data_pedido = nf_data_emissao WHERE nf_data_pedido IS NULL');

        $this->db->query("use " . DB_NAME_CORP);

    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('notas_fiscais', 'nf_data_pedido');

        $this->db->query("use " . DB_NAME_CORP);
        
    }
}