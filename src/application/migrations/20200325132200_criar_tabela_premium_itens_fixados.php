<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_tabela_premium_itens_fixados extends CI_Migration {
    
    public function up()
    {
        $this->db->query("use " . DB_NAME);
        
        /* Metas agregadas */
        $campos = [
            'ped_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'pro_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'ite_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            
        ];
        //Campos
        $this->dbforge->add_field($campos);
        //Primary key
        $this->dbforge->add_key('ped_id', TRUE);
        $this->dbforge->add_key('pro_id', TRUE);
        $this->dbforge->add_key('ite_id', TRUE);
        //Tabela
        $this->dbforge->create_table('premium_itens_fixados');
        
        $this->db->query("use " . DB_NAME_CORP);

    }
    
    public function down()
    {
        $this->db->query("use " . DB_NAME);
        
        //Remove tabelas
        $this->dbforge->drop_table('premium_itens_fixados');
        
        $this->db->query("use " . DB_NAME_CORP);
        
    }
}