<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_criar_colunas_concursos_vendas extends CI_Migration {

    public function up()
    {
        $this->db->query("use " . DB_NAME);

        $campos = [
            'con_vendas' => [
                'type' => 'MEDIUMINT',
            ],
            'con_menor_preco' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
            ),
            'con_status' => [
                'type' => 'TINYINT',
            ],
            'con_thumb' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
            ],
            'post_id' => [
                'type' => 'INT',
            ],
        ];

        $this->dbforge->add_column('concursos', $campos);

        $this->db->query("use " . DB_NAME_CORP);
    }

    public function down()
    {
        $this->db->query("use " . DB_NAME);

        $this->dbforge->drop_column('concursos', 'con_menor_preco');
        $this->dbforge->drop_column('concursos', 'con_vendas');
        $this->dbforge->drop_column('concursos', 'con_status');
        $this->dbforge->drop_column('concursos', 'con_thumb');
        $this->dbforge->drop_column('concursos', 'post_id');

        $this->db->query("use " . DB_NAME_CORP);
    }
}