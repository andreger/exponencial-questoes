<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_adicionar_coluna_captura_saida extends CI_Migration {

        public function up()
        {
                $this->db->query("use " . DB_NAME);

                $campos = [
                        'sai_titulo' => [
                                'type' => 'VARCHAR',
                                'constraint' => 100
                        ],
                        'sai_data_inicio' => [
                                'type' => 'DATETIME'
                        ],
                        'sai_data_fim' => [
                                'type' => 'DATETIME'
                        ],
                        'sai_link' => [
                                'type' => 'VARCHAR',
                                'constraint' => 200
                        ],
                        'sai_fundo' => [
                                'type' => 'VARCHAR',
                                'constraint' => 7
                        ]
                ];

                $this->dbforge->add_column('saidas', $campos);

                $this->db->query("use " . DB_NAME_CORP);
        }

        public function down()
        {
                $this->db->query("use " . DB_NAME);

                $this->dbforge->drop_column('saidas', 'sai_titulo');
                $this->dbforge->drop_column('saidas', 'sai_data_inicio');
                $this->dbforge->drop_column('saidas', 'sai_data_fim');
                $this->dbforge->drop_column('saidas', 'sai_link');
                $this->dbforge->drop_column('saidas', 'sai_fundo');

                $this->db->query("use " . DB_NAME_CORP);
        }
}