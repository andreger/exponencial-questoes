<?php
function get_tabela_disciplinas($disciplinas)
{
	$tabela = array();

	foreach ($disciplinas as $disciplina) {
		$row = array (
			0 => $disciplina['dis_nome'],
			1 => $disciplina['dis_ativo'] == SIM ? 'Ativa' : 'Inativa',
			2 => get_admin_botao_editar('Editar Disciplina', get_editar_disciplina_url($disciplina)) . ' ' . get_admin_botao_excluir('Excluir Disciplina', get_excluir_disciplina_url($disciplina)) . ' ' . get_admin_botao_icone('Listar Questões', get_resolver_por_disciplina_url($disciplina['dis_id']), '')
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_bancas($bancas)
{
	$tabela = array();

	foreach ($bancas as $banca) {
		$row = array (
				0 => $banca['ban_nome'],
				1 => $banca['ban_nome_completo'],
				2 => get_admin_botao_editar('Editar Banca', get_editar_banca_url($banca)) . ' ' . get_admin_botao_excluir('Excluir Banca', get_excluir_banca_url($banca))
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_orgaos($orgaos)
{
	$tabela = array();

	foreach ($orgaos as $orgao) {
		$row = array (
				0 => $orgao['org_nome'],
				1 => $orgao['org_nome_completo'],
				2 => get_admin_botao_editar('Editar Instituição', get_editar_orgao_url($orgao)) . ' ' . get_admin_botao_excluir('Excluir Instituição', get_excluir_orgao_url($orgao))
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_cargos($cargos)
{
	$tabela = array();

	foreach ($cargos as $cargo) {
		$row = array (
				0 => $cargo['car_nome'],
				1 => get_admin_botao_editar('Editar Cargo', get_editar_cargo_url($cargo)) . ' ' . get_admin_botao_excluir('Excluir Cargo', get_excluir_cargo_url($cargo))
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_assuntos($assuntos)
{
	$tabela = array();

	foreach ($assuntos as $assunto) {
		$row = array (
				0 => $assunto['dis_nome'],
				1 => $assunto['ass_pai_nome'],
				2 => $assunto['ass_nome'],
				3 => $assunto['ass_ativo'] == ATIVO ? "Ativo" : "Inativo",
				4 => get_admin_botao_editar('Editar Assunto', get_editar_assunto_url($assunto)) . ' ' . get_admin_botao_excluir('Excluir Assunto', get_excluir_assunto_url($assunto))
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_qac($qacs)
{
	$tabela = array();

	foreach ($qacs as $qac) {
		$row = array (
				0 => $qac['que_id'],
				1 => $qac['qac_antes'],
				2 => $qac['qac_depois'],
				3 => converter_para_ddmmyyyy_HHiiss($qac['qac_data_hora'])
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_conversoes($conversoes)
{
	$tabela = array();

	foreach ($conversoes as $conversao) {
		$row = array (
			0 => $conversao['dis_antigo_nome'],
			1 => $conversao['dis_id']?"Sim":"Não",
			2 => $conversao['ass_antigo_nome'],
			3 => $conversao['dis_novo_nome'],
			4 => $conversao['ass_novo_nome'],
			5 => $conversao['asc_data_criacao'] ? "<div style='white-space:nowrap'>" . converter_para_ddmmyyyy_HHiiss($conversao['asc_data_criacao']) . "</div>" : "",
			6 =>  "<div style='white-space:nowrap'>" . get_admin_botao_excluir('Excluir', get_excluir_conversao_url($conversao['ass_antigo_id'])) . ' ' . get_admin_botao_icone('Rodar', get_rodar_conversao_url($conversao['ass_antigo_id'], $conversao['dis_id']), 'fa-play') . '</div>'
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_historico($historicos)
{
	$tabela = array();

	foreach ($historicos as $historico) {
		$usuario = get_usuario_array($historico['usu_id']);
		$campo = get_campo_nome($historico['qhi_campo']);

		$row = array (
				0 => converter_para_ddmmyyyy_HHiiss($historico['qhi_data']),
				1 => $usuario['nome_completo'],
				2 => $campo,
				3 => $historico['qhi_valor_antigo'],
				4 => $historico['qhi_valor_novo'],
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_questoes($questoes)
{
	$tabela = array();

	foreach ($questoes as $questao) {
		$row = array (
				0 => $questao['que_enunciado'],
				1 => get_questao_status_nome($questao),
// 				2 => $questao['pro_id'],
				3 => get_admin_botao_editar('Editar Questão', get_editar_questao_url($questao)) . ' ' . get_admin_botao_excluir('Excluir Questão', get_excluir_questao_url($questao)) . ' ' . get_admin_model_adicionar($questao)
			);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_provas($provas)
{
	$tabela = array();

	foreach ($provas as $prova) {
		$row = array (
				0 => $prova['pro_nome'],
				1 => $prova['pro_ano'],
				2 => $prova['pro_orgao_nome'],
				3 => $prova['pro_banca_nome'],
		        4 => $prova['areas_atuacao'],
				5 => $prova['pro_num_questoes'],
				6 => "<div style='white-space:nowrap'>" . get_admin_botao_editar('Editar', get_editar_prova_url($prova)) . ' ' .
					 get_admin_botao_excluir('Excluir', get_excluir_prova_url($prova)) . ' ' .
					 get_admin_botao_listar('Listar Questões', get_listar_questoes_prova_url($prova), "target='_blank'") . get_admin_botao_icone('Duplicar', get_duplicar_prova_url($prova), "fa fa-files-o") . "</div>"
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_grupos($grupos)
{
	$tabela = array();

	foreach ($grupos as $grupo) {
		$row = array (
				0 => $grupo['rag_nome'],
				1 => get_admin_botao_editar('Editar Grupo', get_editar_grupo_url($grupo['rag_id'])) . ' ' . get_admin_botao_excluir('Excluir Grupo', get_excluir_grupo_url($grupo['rag_id']))
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_rankings($rankings)
{
	$tabela = array();

	foreach ($rankings as $ranking) {
		$row = array (
				0 => $ranking['ran_nome'],
				1 => get_admin_botao_editar('Editar Ranking', get_editar_ranking_url($ranking['ran_id'])) . ' ' . get_admin_botao_excluir('Excluir Ranking', get_excluir_ranking_url($ranking['ran_id']))
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_areas_formacao($areas_formacao)
{
	$tabela = array();

	foreach ($areas_formacao as $area_formacao) {
		$row = array (
				0 => $area_formacao['arf_nome'],
				1 => get_admin_botao_editar('Editar Área', get_editar_area_formacao_url($area_formacao)) . ' ' . get_admin_botao_excluir('Excluir Área', get_excluir_area_formacao_url($area_formacao))
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_areas_atuacao($areas_atuacao)
{
	$tabela = array();

	foreach ($areas_atuacao as $area_atuacao) {
		$row = array (
				0 => $area_atuacao['ara_nome'],
				1 => get_admin_botao_editar('Editar Área', get_editar_area_atuacao_url($area_atuacao)) . ' ' . get_admin_botao_excluir('Excluir Área', get_excluir_area_atuacao_url($area_atuacao))
		);

		array_push($tabela, $row);
	}

	return $tabela;
}

function get_tabela_desempenho_assunto($desempenhos_assunto)
{
	$tabela = array();

	foreach ($desempenhos_assunto as $desempenho) {
		$row = array (
				0 => "<a href='". get_resolver_questoes_url(TIPO_DISCIPLINA, $desempenho['dis_id'])."'>{$desempenho['dis_nome']}</a>",
				1 => get_desempenho_icone($desempenho['performance']) . "<a href='". get_resolver_questoes_url(TIPO_ASSUNTO, $desempenho['ass_id'])."'>{$desempenho['ass_nome']}</a>",
				2 => $desempenho['total'],
				3 => $desempenho['performance'],
				4 => get_admin_botao_responsivo('Resolver Erradas', 'Resolver Erradas', get_resolver_questoes_url(TIPO_ASSUNTO, $desempenho['ass_id'], QUE_ERREI)) . ' ' . get_admin_botao_responsivo('Resolver Mais', 'Resolver Mais', get_resolver_questoes_url(TIPO_ASSUNTO, $desempenho['ass_id'], NAO_RESOLVIDAS))
		);

		array_push($tabela, $row);
	}
	return $tabela;
}

function get_tabela_desempenho_simulado($desempenhos_simulado)
{
	$tabela = array();

	foreach ($desempenhos_simulado as $desempenho) {
		$href = get_ranking_simulado_url($desempenho['sim_id']);
		$button = get_botao_ranking('Ranking', $href);
		$row = array (
				0 => get_desempenho_icone($desempenho['performance']) . "<a href='". get_resolver_questoes_url(TIPO_SIMULADO, $desempenho['sim_id'])."'>{$desempenho['nome']}</a>",
				1 => $desempenho['nota'],
				2 => "<span style='display: none'>{$desempenho['data_original']}</span>" . $desempenho['data'],
				3 => $desempenho['posicao'],
				4 => $button
				);

		array_push($tabela, $row);
	}
	return $tabela;
}

function get_tabela_desempenho_banca($desempenhos_banca)
{
	$tabela = array();

	foreach ($desempenhos_banca as $desempenho) {
		$row = array (
				0 => "<a href='". get_resolver_questoes_url(TIPO_DISCIPLINA, $desempenho['dis_id'])."'>{$desempenho['dis_nome']}</a>",
				1 => "<a href='". get_resolver_questoes_url(TIPO_BANCA, $desempenho['ban_id'])."'>{$desempenho['ban_nome']}</a>",
				2 => $desempenho['total'],
				3 => $desempenho['performance'],
				4 => get_admin_botao_responsivo('Resolver Erradas', 'Resolver Erradas', get_resolver_questoes_url(TIPO_BANCA_DISCIPLINA, $desempenho['ban_id'] . '-' . $desempenho['dis_id'], QUE_ERREI)) . ' ' . get_admin_botao_responsivo('Resolver Mais', 'Resolver Mais', get_resolver_questoes_url(TIPO_BANCA_DISCIPLINA, $desempenho['ban_id'] . '-' . $desempenho['dis_id'], NAO_RESOLVIDAS))
		);

		array_push($tabela, $row);
	}
	return $tabela;
}

function get_tabela_desempenho_disciplina($desempenhos_disciplina)
{
	$tabela = array();

	foreach ($desempenhos_disciplina as $desempenho) {
		$row = array (
		        0 => get_desempenho_icone($desempenho['performance']) . $desempenho['dis_nome'],
				1 => $desempenho['total'],
				2 => $desempenho['acertos'],
				3 => $desempenho['erros'],
				4 => $desempenho['performance']
		);

		array_push($tabela, $row);
	}
	return $tabela;
}

function get_footer_tabela_desempenho_disciplina($desempenhos_disciplina)
{
	$tabela = array();

	$i = 0;
	$soma_total = 0;
	$soma_acertos = 0;
	$soma_erros = 0;
	$soma_performance = 0;

	foreach ($desempenhos_disciplina as $desempenho) {
		$i++;
		$soma_total += $desempenho['total'];
		$soma_acertos += $desempenho['acertos'];
		$soma_erros += $desempenho['erros'];
		$soma_performance += $desempenho['performance'];
	}
	return array (
			'Todas as Disciplinas',
			$soma_total,
			$soma_acertos,
			$soma_erros,
			$i > 0 ? str_replace(' ','', porcentagem($soma_performance / $i)) : 0
		);
}

function get_tabela_questoes_repetidas($questoes_repetidas)
{

	$tabela = array();

	if($questoes_repetidas) {

		foreach ($questoes_repetidas as $item) {
			$row = array (
					0 => $item['que_id_master'],
					1 => $item['que_id_repetida'],
					2 => get_admin_botao_branco('Comparar Questões', get_comparar_questoes_url($item['que_id_master'], $item['que_id_repetida']))
			);

			array_push($tabela, $row);
		}
	}

	return $tabela;
}

function get_tabela_questoes_repetidas_execucoes($execucoes)
{

	$tabela = array();

	if($execucoes) {

		$CI =& get_instance();
		$CI->load->model('disciplina_model');

		foreach ($execucoes as $item) {
			$link = get_questoes_repetidas_excecucoes_detalhes_url($item['qrx_id']);

			$disciplinas_nomes = "";
			if($item['qrx_disciplinas']) {
				$disciplinas_ids = explode(",", $item['qrx_disciplinas']);
				$disciplinas_nomes_a = [];

				foreach ($disciplinas_ids as $id) {
					$disciplina = $CI->disciplina_model->get_by_id($id);

					if($disciplina) {
						array_push($disciplinas_nomes_a, $disciplina['dis_nome']);
					}
				}

				asort($disciplinas_nomes_a);
				$disciplinas_nome = implode(', ', $disciplinas_nomes_a);
			}

			$anos = "";
			if($item['qrx_anos']) {
				$anos = explode(",", $item['qrx_anos']);
				asort($anos);
				$anos = implode(", ", $anos);
			}

			$row = array (
				0 => "<a href='{$link}'>#" . $item['qrx_id'] . "</a>",
				1 => converter_para_ddmmyyyy_HHii($item['qrx_data_hora']),
				2 => $item['qrx_percentual'],
				3 => $item['qrx_enunciado'] == SIM ? "Sim" : "Não",
				4 => $item['qrx_texto_base'] == SIM ? "Sim" : "Não",
				5 => $item['qrx_texto_plano'] == SIM ? "Sim" : "Não",
				6 => $item['qrx_mesma_disciplina'] == SIM ? "Sim" : "Não",
				7 => $item['qrx_mesma_banca'] == SIM ? "Sim" : "Não",
				8 => $disciplinas_nome,
				9 => $anos,
				10 => $item['qrx_tempo_gasto'],
				11 => $item['qrx_num_executadas'],
				12 => $item['qrx_num_repetidas']
			);

			array_push($tabela, $row);
		}
	}

	return $tabela;
}

function get_tabela_comentarios_professores($comentarios_professores){
	$tabela = array();

	foreach ($comentarios_professores as $comentario_professor) {
		$row = array (
				0 => $comentario_professor['nome'],
				1 => $comentario_professor['total'],
				2 => porcentagem($comentario_professor['percentual'])
		);

		array_push($tabela, $row);
	}
	return $tabela;
}