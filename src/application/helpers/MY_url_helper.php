<?php
function ci_site_url($uri = '')
{
	$CI =& get_instance();
	return $CI->config->site_url($uri);
}

function get_current_url()
{
	$CI =& get_instance();
	return base_url($CI->router->fetch_class() . '/' . $CI->router->fetch_method());
}