<?php

function nenhum_filtro_aplicado($filtro)
{
	return $filtro == ['filtro_incluir' => 6];
}

function get_questoes_favoritas_tooltip($disciplina)
{
	return "Acertos: {$disciplina['questoes_certas']}, Erros: {$disciplina['questoes_erradas']}, Não resolvidas: {$disciplina['total_questoes_nao_respondidas']}";
}

function get_novo_offset($qpp, $offset)
{
	return floor(($offset + 1) / $qpp) * $qpp;
}

function formatar_input_summernote($questao)
{
	$questao['que_texto_base'] = remover_p_br_p_summernote($questao['que_texto_base']);
	$questao['que_enunciado'] = remover_p_br_p_summernote($questao['que_enunciado']);
	
	foreach ($questao['opcoes'] as &$opcao) {
		$opcao['qop_texto'] = remover_p_br_p_summernote($opcao['qop_texto']);
	}
	
	return $questao;
}

function formatar_opcao_questao($texto) 
{
	$texto = stripcslashes(trim($texto));

	if(starts_with($texto, "<div")) {
		$texto = str_replace_first("<div", "<div style='display:inline-block'", $texto);
	}

	$texto = str_replace("font-size:","font-sizze", $texto);

	return $texto;
}

function remover_p_br_p_summernote($texto)
{
	if($texto == "<p><br></p>") {
		$texto = "";
	}
	
	return $texto;
}


function get_qc_questao_url($qc_id)
{
	return "https://www.qconcursos.com/questoes-de-concursos/questoes/search?q=q{$qc_id}";
}

function get_responder_questao_url($questao_id)
{
	$CI = &get_instance();
	$CI->load->model('questao_model');
	
	$respondidas = $CI->questao_model->contar_questoes_respondidas(get_current_user_id());
	return ($respondidas >= get_max_respondidas_nao_assinante()) ? get_assine_sq_url() : '#';
}

function get_questao_status_nome($questao, $exibir_normal = false)
{
	if($questao['que_status'] == CANCELADA) {
		return "Anulada";
	}
	else if($questao['que_status'] == DESATUALIZADA) {
		return "Desatualizada";
	}
	else {
		if($exibir_normal) {
			return "Normal";
		}
		return ""; 
	}
}

function get_questao_dificuldade_icone($questao){
	if($questao['que_dificuldade']){
		$link = "/wp-content/themes/academy/images/icons/dificuldade/";
		switch($questao['que_dificuldade']) {
			case MUITO_FACIL : return  $link."muito-facil.png"; break;
			case FACIL : return  $link."facil.png"; break;
			case MEDIA : return  $link."medio.png"; break;
			case DIFICIL : return  $link."dificil.png"; break;
			case MUITO_DIFICIL : return  $link."muito-dificil.png"; break;
			
		}
		return "";
	}
	return "";
}

function get_questao_dificuldade_nome($questao)
{
	if($questao['que_dificuldade']) {
		
		switch($questao['que_dificuldade']) {
			case MUITO_FACIL : return  "Muito fácil"; break;
			case FACIL : return  "Fácil"; break;
			case MEDIA : return  "Média"; break;
			case DIFICIL : return  "Difícil"; break;
			case MUITO_DIFICIL : return  "Muito difícil"; break;
			
		}
		return "";
	}
	
	return "";
}

function get_questao_dificuldade_url($questao_id){
	$questao = array('que_id' => $questao_id);
	return get_questao_dificuldade($questao, true);
}

function get_questao_dificuldade($questao, $is_ajax = false){

	if($is_ajax){
		$CI = &get_instance();
		$CI->load->model('questao_model');
		$questao['que_dificuldade'] = $CI->questao_model->get_questao_dificuldade($questao['que_id']) ;
	}

	if($questao['que_dificuldade'] && (tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, REVISOR)) || get_questao_acertou_errou_str(get_current_user_id(), $questao['que_id']))){
		//return "<span class='questao-dificuldade'><i class='fa fa-question-circle'></i> Dificuldade: ".get_questao_dificuldade_nome($questao)."</span>";
		return "<span style='display: none;' class='questao-dificuldade' title='".get_questao_dificuldade_nome($questao)."'>".get_questao_dificuldade_nome($questao)."<img src='".get_questao_dificuldade_icone($questao)."' /></span>";
	}

	return "";
}

function get_questao_ver_dificuldade($questao, $is_ajax = false){

	if($is_ajax){
		$CI = &get_instance();
		$CI->load->model('questao_model');
		$questao['que_dificuldade'] = $CI->questao_model->get_questao_dificuldade($questao['que_id']) ;
	}

	if($questao['que_dificuldade'] && (tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, REVISOR)) || get_questao_acertou_errou_str(get_current_user_id(), $questao['que_id']))){
		return "<span class='questao-ver-dificuldade btn btn-outline btn-default' onclick=dificuldade(".$questao['que_id'].");><i class='fa fa-question-circle'></i> Ver Dificuldade </span>"
			.get_questao_dificuldade($questao);
	}
}

function get_questao_ver_dificuldade_url($questao_id){
	$questao = array('que_id' => $questao_id);
	return get_questao_ver_dificuldade($questao, true);
}

/**
 * Retorna a informação de questão reprovada (quando ela for) para os usuários que podem vesualizar essa informação
 * 
 * @since L1
 * 
 * @return HTML da informação de reprovada ou vazio
 */
function get_questao_reprovada($questao){

	$html = "";

	if($questao['que_aprovacao'] == QUESTAO_REPROVADA && tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, REVISOR))){
		$html = "<div style='margin-bottom: 20px; text-align: right;'><span class='questao-repetida'><i class='fa fa-ban'></i> Questão reprovada </span></div>";
	}

	return $html;
}

function get_questao_master_ou_repetida($questao_id){

	$CI = &get_instance();
	$CI->load->model('questao_model');

	$html = "";

	$repetidas = $CI->questao_model->listar_questoes_por_master_id($questao_id) ;

	foreach ($repetidas as $item) {
		$link = get_questao_url($item['que_id_repetida']);
		$html .= 
				"<span class='questao-master'>
					<a href='$link'>Master de {$item['que_id_repetida']}</a>
				</span>";
	}
	
	$masters = $CI->questao_model->listar_questoes_por_repetida_id($questao_id) ;

	foreach ($masters as $item) {
		$link = get_questao_url($item['que_id_master']);
		$html .= 
			"<span class='questao-repetida'>
				<a href='$link'>Repetida de {$item['que_id_master']}</a>
			</span>";
	}
	
	if($html){
		$html = 
		"
		<div class='panel'>
			<div class='questao-repetida' role='tab' id='questaoRepetidaTitle{$questao_id}'>
				<a id='collapse-id-filtro' data-toggle='collapse' data-parent='#faq' href='#questaoRepetida{$questao_id}' aria-expanded='false' aria-controls='questaoRepetida{$questao_id}'><div>Questão Repetida - <span class='mostrar-div'>Mostrar</span><span class='esconder-div'>Esconder</span></div></a>
			</div>
			<div id='questaoRepetida{$questao_id}' class='panel-collapse collapse' role='tabpanel' aria-labelledby='questaoRepetidaTitle{$questao_id}'>
				<div>
					{$html}
				</div>
			</div>
		</div>
		";
	}

	return $html;
}

function get_campo_nome($campo_id)
{
	if($campo_id) {

		switch($campo_id) {
			case HISTORICO_DISCIPLINA : return "Disciplina"; break;
			case HISTORICO_STATUS : return "Status"; break;
			case HISTORICO_DIFICULDADE : return "Dificuldade"; break;
			case HISTORICO_ASSUNTOS : return "Assuntos"; break;
			case HISTORICO_GABARITO : return "Gabarito"; break;
			case HISTORICO_ATIVO : return "Situação"; break;
			case HISTORICO_ALTERNATIVAS : return "Alternativas"; break;
			case HISTORICO_ENUNCIADO : return "Enunciado"; break;
			case HISTORICO_TEXTO_BASE : return "Texto base"; break;
		}
		return "";
	}

	return "";
}

function get_questao_assuntos_str($questao)
{
	$assunto_array = array();
	foreach ($questao['assuntos'] as $assunto) {
		array_push($assunto_array, $assunto['ass_nome']);
	}
	
	return implode(", ", $assunto_array);
}

function get_questao_acertou_errou_str($usuario_id, $questao_id)
{
	if($usuario_id) {
		$ci = &get_instance();
		$ci->load->model('questao_model');
		$resposta = $ci->questao_model->get_resposta_usuario($usuario_id, $questao_id);
		
		$acertou_str = "<span style='color: #005fab'><i class='fa fa-thumbs-up' style='font-size:18px!important'></i> Você acertou esta questão</span>";
		$errou_str = "<span style='color: #fc3c3c'><i class='fa fa-thumbs-down' style='font-size:18px!important'></i> Você errou esta questão</span>";

		if($resposta) {
			if(is_null($resposta['qre_selecao'])) {
				return $errou_str;
			}
			else {
				if($resposta['qre_selecao'] == $resposta['que_resposta']) {
					return $acertou_str;
				}
				else {
					return $errou_str;
				}
			}
		}
	}
	return "";
}

function get_resposta_usuario($questao_id, $simulado_id = null, $caderno_id = null)
{
	$ci = &get_instance();
	$ci->load->model('questao_model');

	if($resposta = $ci->questao_model->get_resposta_usuario(get_current_user_id(), $questao_id, $simulado_id, $caderno_id))
	{
		return $resposta['qre_selecao'];
	}
	else
	{
		return null;
	}
}

function is_favorita($questao_id, $usuario_id)
{
	$ci = &get_instance();
	$ci->load->model('questao_model');
	return $ci->questao_model->is_favorita($questao_id, $usuario_id, QUESTAO_FAVORITA);
}

/**
 * Verifica se uma questão está sendo acompanhada pelo usuário
 * 
 * @since L1
 * 
 * @param $questao_id ID da questãp
 * @param $usuario_id ID do usuário
 * 
 * @return TRUE caso a questão esteja sendo acompanhada pelo usuário, FALSE caso contrário
 */
function is_questao_acompanhada($questao_id, $usuario_id)
{
	$ci = &get_instance();
	$ci->load->model('questao_model');
	return $ci->questao_model->is_favorita($questao_id, $usuario_id, QUESTAO_ACOMPANHADA);
}

function is_questao_em_caderno_comprado($questao_id)
{
	$CI = &get_instance();
	$CI->load->model('caderno_model');
	$questoes_ids = $CI->caderno_model->listar_questoes_ids_em_cadernos_comprados(get_current_user_id());

	return in_array($questao_id, $questoes_ids) ? true : false;
}

function sanitizar($input)
{
	// remove tags html
	$input = preg_replace('/<[^>]+>/', '', $input);

	// remove tabulação
	$input = str_replace(array("\r", "\n", "\t"), ' ', $input);

	// remove entity de espaço
	$input = str_replace("&nbsp;", ' ', $input);

	// remove espaços duplos
	$input = str_replace('  ', ' ', $input);

	// remove espaços no início e fim
	$input = trim($input);

	return $input;
}

function salvar_resposta_marcada($questao_id, $resposta)
{
	session_start();

	if($questao_id && $resposta) {
		$_SESSION["marcadas"][$questao_id] = $resposta;
	}
}

function get_resposta_marcada($questao_id)
{
	return $_SESSION["marcadas"] && $_SESSION["marcadas"][$questao_id] ? $_SESSION["marcadas"][$questao_id] : null;
}

function is_opcao_resposta_marcada($questao_id, $opcao)
{
	if($marcada = get_resposta_marcada($questao_id)) {
		return $marcada == $opcao;
	}

	return false;
}

function zerar_respostas_marcadas()
{
	if($_GET['zf']) {
		$_SESSION["marcadas"] = null;
	}
}

function zerar_filtro()
{
	if($_GET['zf']) {
		$_SESSION['filtro'] = null;
	}
}

function get_gabarito($questoes, $num_colunas)
{
	$gabarito = [];
	$linha = 0;
		
	for($i = 0; $i < count($questoes); $i++) {
		$coluna = $i % $num_colunas;
		
		$opcoes = array();
		if($questoes[$i]['que_tipo'] == MULTIPLA_ESCOLHA) {
			$opcoes = array(1 => 'A', 2 => 'B', 3 => 'C', 4=> 'D');
		} 
		elseif($questoes[$i]['que_tipo'] == MULTIPLA_ESCOLHA_5) {
			$opcoes = array(1 => 'A', 2 => 'B', 3 => 'C', 4=> 'D', 5 => 'E');
		} 
		else {
			$opcoes = array(1 => 'C', 2 => 'E');
		}
	
		if($questoes[$i]['que_status'] == CANCELADA) {
			$resposta = 'X';
		}
		else {
			$resposta = $questoes[$i]['que_resposta'] ? $opcoes[$questoes[$i]['que_resposta']] : "";
		}
		
		$gabarito[$linha][$coluna] = array(
			'num' => $i+1,
			'resposta' => $resposta
		);
		
		if($coluna == ($num_colunas - 1)) {
			$linha++;
		}
	}

	return $gabarito;
}

function get_tipo_questao_by_id($id) 
{
	$items = get_tipo_questao_multiselect();

	return $items[$id];
}

function get_dificuldade_questao_by_id($id) 
{
	$items = get_dificuldade_questao_multiselect();

	return $items[$id];
}

function get_status_questao_by_id($id) 
{
	$items = get_status_questao_combo();

	return $items[$id];
}

function get_situacao_by_id($id) 
{
	$items = get_situacoes_combo();

	return $items[$id];
}

function numero_para_letra($num)
{
	switch ($num) {
		case 1: return 'a';	break;
		case 2: return 'b';	break;
		case 3: return 'c';	break;
		case 4: return 'd';	break;
		case 5: return 'e';	break;
	}
}

function get_questao_para_comparacao($questao, $questao_opcoes)
{
	$CI =& get_instance();

	$cabecalho = $CI->load->view('questoes/cabecalho/cabecalho', array('questao' => $questao, 'remover_assuntos' => TRUE, 'esconder_favorita' => TRUE), TRUE);

	$simulados_onde_estou = $CI->load->view('questoes/rodape/rodape', array('questao' => $questao), TRUE);
	
	$html = 
			"<div class='ibox float-e-margins'>
				$cabecalho           
	            <div class='ibox-content'>
	            <div class='row'>
				<div class='col-md-12'>{$simulados_onde_estou}
	            <a class='btn btn-primary btn-block	' 
	                target='_blank' href='/questoes/main/resolver_questoes/que/{$questao['que_id']}/0'><i class='fa fa-eye'></i> Ver no SQ </a>   
             
	            <a class='btn btn-outline btn-default btn-block' 
				href='/questoes/admin/editar_questao/{$questao['que_id']}'			
	               ><i class='fa fa-pencil'></i> Editar Questão </a>

	            <a class='btn btn-outline btn-default btn-edicao-rapida btn-block'  href='#' data-id=\"{$questao['que_id']}\"><i class='fa fa-pencil'></i> Edição Rápida </a>	 
	             <div id=\"edicao-rapida-{$questao['que_id']}\" style=\"display:none; background-color: #eee; padding: 20px\"></div>            
	             </div>  
	             </div> 
	            	<h5>Texto Base</h5>
	                <div>{$questao['que_texto_base']}</div>
	                <hr />
	                <h5>Enunciado</h5>
	                <div>{$questao['que_enunciado']}</div>
	                <hr />";


	if($questao_opcoes) {

		foreach ($questao_opcoes as $opcao) {
			$letra = strtoupper(numero_para_letra($opcao['qop_ordem']));
			$html .= 
				"<h5>Opção {$letra}</h5>
				<div>{$opcao['qop_texto']}</div>
				<hr />";
		}
	}

	$html .= 
			"</div>
			</div>";

	return $html;

}

function get_questoes_comparacao($master, $master_opcoes, $repetida, $repetida_opcoes)
{
	
	$CI =& get_instance();

	$params = ['from_text'=> $master['que_texto_base'], 'to_text' => $repetida['que_texto_base']];
	$CI->load->library("FineDiff");
	$texto_base = $CI->finediff->renderDiffToHTML($params);



	$params = ['from_text'=> $master['que_enunciado'], 'to_text' => $repetida['que_enunciado']];
	$CI->load->library("FineDiff");
	$enunciado = $CI->finediff->renderDiffToHTML($params);

	$html = 
		"<div class='ibox float-e-margins'>
			<div class='ibox-title'>
                <h5>Comparação</h5>
            </div>
            <div class='ibox-content'>
				<h5>Texto Base</h5>
				<div>{$texto_base}</div>
				<hr />
				<h5>Enunciado</h5>
				<div>{$enunciado}</div>
				<hr />";

	if($master_opcoes) {
		for($i = 0; $i < count($master_opcoes); $i++) {
			$letra = strtoupper(numero_para_letra($i+1));

			$params = ['from_text'=> $master_opcoes[$i]['qop_texto'], 'to_text' => $repetida_opcoes[$i]['qop_texto']];
			$CI->load->library("FineDiff");
			$opcao = $CI->finediff->renderDiffToHTML($params);

			$html .= 
				"<h5>Opção $letra</h5>
				<div>{$opcao}</div>
				<hr />";
		}
	}

	$html .= 
				"</div>
	        </div>";

	return $html;
}

function get_tamanho_fonte() 
{
	return isset($_SESSION['tamanho-fonte']) ? $_SESSION['tamanho-fonte'] : TAMANHO_FONTE;
}

function formatar_texto_de_questao($texto)
{
	$texto = stripcslashes($texto);
	$texto = str_replace("font-size:","font-sizze", $texto);
	return $texto;
}

function get_assuntos_questao_str($questao_id)
{
	$CI =& get_instance();
	$CI->load->model('questao_model');
	$assuntos = $CI->questao_model->get_assuntos_questao($questao_id, FALSE);

	$assuntos_a = [];
	foreach ($assuntos as $assunto) {
		array_push($assuntos_a, "({$assunto['ass_id']}) {$assunto['ass_nome']}");
	}

	return implode(", ", $assuntos_a);
}

/**
 * Retorna o número máximo permitido de questões respondidas por usuário não assinante. Se option não estiver definida retorna um valor padrão
 * 
 * @return int
 */ 

function get_max_respondidas_nao_assinante()
{
	return get_option('CFG_SQ_QTDE_QUESTOES_DIARIAS') ?: 50;
}

function get_questao_titulo($questao)
{
    $disciplina = $questao['dis_nome'] ? "de {$questao['dis_nome']}" : "";
    
    return "Questão {$disciplina}: {$questao['que_id']}";
}