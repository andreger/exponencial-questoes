<?php 

function get_ajax_main_alunos_acompanhamento($filtro_aluno_str = NULL, $com_opcao_zero = TRUE){

    /*if(is_null($filtro_aluno_str)){
        $filtro_aluno_str = $this->input->post('filtro_aluno');
    }*/
    $filtro_aluno = array_map('intval', explode(',', $filtro_aluno_str));

    return get_alunos_acompanhamento($filtro_aluno, $com_opcao_zero);

}

function get_alunos_acompanhamento($filtro_aluno = null, $com_opcao_zero = TRUE){
    
    $alunos = array();

    if(is_null($filtro_aluno) || empty($filtro_aluno) || in_array(ALUNO_TODOS, $filtro_aluno)){
        $filtro_aluno = array(ALUNO_PODIO, ALUNO_COACHING);
    }
    
    if(in_array(ALUNO_PODIO, $filtro_aluno)){
        $usuario_logado = get_usuario_array(get_current_user_id());
        $podio = get_alunos_ativos_podio($usuario_logado['nome_completo']);
        $alunos = array_merge($alunos, $podio);
    }

    if(in_array(ALUNO_COACHING, $filtro_aluno)){
        $coaching = get_alunos_ativos_turma_coaching();
        $alunos = array_merge($alunos, $coaching);
    }

    $alunos_unicos = array();

    $aux = array();
    foreach($alunos as $aluno){
        if(!array_key_exists($aluno['id'], $aux)){
            array_push($alunos_unicos, $aluno);
            $aux[$aluno['id']] = strtolower($aluno['nome_completo']);
        }
    }

    array_multisort($aux, SORT_ASC, $alunos_unicos);

    return get_alunos_combo($alunos_unicos, $com_opcao_zero);
}
    
function get_config_aluno()
{
    $CI = &get_instance();
    $CI->load->model('configuracao_usuario_model');

    return $CI->configuracao_usuario_model->get_config_usuario_by_id(get_current_user_id());
}    

?>