<?php
function get_qcon_prova_url($href, $relativo = false)
{
	return $relativo ? $href : QCON_DOMINIO . $href;
}

function get_questao_tipo($opcoes)
{
	$count = count($opcoes);
	if($count >= 5){
		return MULTIPLA_ESCOLHA_5;
	}elseif($count >= 2){
		return MULTIPLA_ESCOLHA;
	}else{
		return CERTO_ERRADO;
	}
}