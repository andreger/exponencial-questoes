<?php

function calc_pg_professor_mes($totais_pagseguro, $qtdp){
	// echo $qtdp;exit;
	// echo round($totais_pagseguro['receita_liquida'],2) * 0.5 * $qtdp ; exit;
	$calculo = round(($totais_pagseguro['receita_liquida'] * 0.5) * $qtdp,2);
	return number_format($calculo, 2, ',', '.');
}

function get_valor_acumulado_prof($user_id, $mes_ano){
	$ci =& get_instance();
	$ci->load->model('relatorios_model');
	$data = explode("/", $mes_ano);
	$acumulado = $ci->relatorios_model->get_valor_acumulado_user($user_id, $data[0], $data[1]);
	return $acumulado['pap_acumulado'];
}

function get_valor_acumulado_prox_mes($valor_pago, $valor_acumulado, $min_pag = 100){
	$soma = get_float($valor_pago) + get_float($valor_acumulado);

	if($soma < $min_pag){
		return number_format($soma, 2, ',', '.');
	}
	else{
		return "0,00";
	}
}

function get_valor_a_receber($valor_pago, $valor_acumulado, $min_pag = 100){
	$soma = get_float($valor_pago) + get_float($valor_acumulado);

		if($soma < $min_pag){
		return "0,00";
	}
	else{
		return number_format($soma, 2, ',', '.');
	}
}

function exportar_excel($table_result)
{
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="stats.xls"');array(
		' > ' => ' maior ',
		' >= ' => ' maior ou igual ',
		' < ' => ' menor ',
		' <= ' => ' menor ou '
	);
	
	$table_result = str_replace("<table", "<table style='border-collapse:collapse; border: 1px solid black;'", $table_result);
	$table_result = str_replace("<tr", "<tr style='border: 1px solid black;'", $table_result);
	$table_result = str_replace("<td", "<td style='border: 1px solid black;'", $table_result);
	
	$list = get_html_translation_table(HTML_ENTITIES);
	unset($list['"']);
	unset($list['<']);
	unset($list['>']);
	unset($list['&']);
	
	$search = array_keys($list);
	$values = array_values($list);
	$search = array_map('utf8_encode', $search);
	
	$out =  str_replace($search, $values, $table_result);
	
	echo "<html xmlns:x='urn:schemas-microsoft-com:office:excel'>
		<head>
			<meta http-equiv='Content-Type' content='text/html;charset=UTF-8' />
		    <!--[if gte mso 9]>
		    <xml>
		        <x:ExcelWorkbook>
		            <x:ExcelWorksheets>
		                <x:ExcelWorksheet>
		                    <x:Name>Sheet 1</x:Name>
		                    <x:WorksheetOptions>
		                        <x:Print>
		                            <x:ValidPrinterInfo/>
		                        </x:Print>
		                    </x:WorksheetOptions>
		                </x:ExcelWorksheet>
		            </x:ExcelWorksheets>
		        </x:ExcelWorkbook>
		    </xml>
		    <![endif]-->
		</head>
	
		<body>
		   " . $out . "
		</body></html>";
	
	exit;
}

function get_media_avaliacao_comparador_combo(){
	return array(
		' > ' => ' maior ',
		' >= ' => ' maior ou igual ',
		' < ' => ' menor ',
		' <= ' => ' menor ou igual '
	);
}

function get_avaliacao_comentario_ordem_combo(){
	return array(
		'que.que_id' => 'Id Questão',
		'com.com_media_qtd_avaliacoes' => 'Quantidade de avaliações',
		'com.com_media_avaliacao' => 'Média das avaliações'
	);
}

function get_avaliacao_comentario_sentido_combo(){
	return array(
		'ASC' => 'Crescente',
		'DESC' => 'Decrescente'
	);
}


