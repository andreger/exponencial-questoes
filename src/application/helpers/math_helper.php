<?php
function get_percentual($valor, $total) {
	$percentual = ( $valor / $total ) * 100;
	return number_format($percentual, 2, ',', '.');
}

function get_percentual_sem_format($valor, $total) {
	return ( $valor / $total ) ;
}

function get_float($str) {
	if(strstr($str, ",")) {
		$str = str_replace(".", "", $str); // replace dots (thousand seps) with blancs
		$str = str_replace(",", ".", $str); // replace ',' with '.'
	}

	if(preg_match("#([0-9\.]+)#", $str, $match)) { // search for number that may contain '.'
		return floatval($match[0]);
	} else {
		return floatval($str); // take some last chances with floatval
	}
}

function tempo_gasto($segundos)
{
	$H = floor($segundos / 3600);
	$i = ($segundos / 60) % 60;
	$s = $segundos % 60;

	return sprintf("%02d:%02d:%02d", $H, $i, $s);
}