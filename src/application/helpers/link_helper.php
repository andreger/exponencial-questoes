<?php
/**
 * Grupo de links para Area Administrativa
 */

function get_admin_header_view_url() {
	return 'admin/header';
}

function get_admin_menu_view_url() {
	return 'admin/menu';
}

function get_admin_footer_view_url() {
	return 'admin/footer';
}

/**
 * Grupo de links para Disciplina
 */
function get_editar_disciplina_url($disciplina = array()) {
	if(empty($disciplina))
		return base_url('admin/editar_disciplina');
	
	return base_url('admin/editar_disciplina/' . $disciplina['dis_id']);
}

function get_resolver_por_disciplina_url($disciplina_id) {
	return base_url("main/resolver_por_disciplina/{$disciplina_id}");
}

function get_arvore_disciplinas_url() {
	return base_url('admin/arvore_disciplinas');
}


function get_listar_disciplinas_url() {
	return base_url('admin/listar_disciplinas');
}

function get_excluir_disciplina_url($disciplina) {
	return base_url('admin/excluir_disciplina/' . $disciplina['dis_id']);
}

function get_listar_disciplina_view_url() {
	return 'admin/listar_disciplinas';
}

function get_arvore_disciplinas_view_url() {
	return 'admin/arvore_disciplinas';
}

function get_editar_disciplina_view_url() {
	return 'admin/editar_disciplina';
}

/**
 * Grupo de links para Banca
 */
function get_editar_banca_url($banca = array()) {
	if(empty($banca))
		return base_url('admin/editar_banca');

	return base_url() . 'admin/editar_banca/' . $banca['ban_id'];
}

function get_listar_bancas_url() {
	return base_url('admin/listar_bancas/');
}

function get_excluir_banca_url($banca) {
	return base_url('admin/excluir_banca/' . $banca['ban_id']);
}

function get_listar_banca_view_url() {
	return 'admin/listar_bancas';
}

function get_editar_banca_view_url() {
	return 'admin/editar_banca';
}

/**
 * Grupo de links para Órgão
 */
function get_editar_orgao_url($orgao = array()) {
	if(empty($orgao))
		return base_url('admin/editar_orgao');

	return base_url('admin/editar_orgao/' . $orgao['org_id']);
}

function get_listar_orgaos_url() {
	return base_url('admin/listar_orgaos/');
}

function get_excluir_orgao_url($orgao) {
	return base_url('admin/excluir_orgao/' . $orgao['org_id']);
}

function get_listar_orgao_view_url() {
	return 'admin/listar_orgaos';
}

function get_editar_orgao_view_url() {
	return 'admin/editar_orgao';
}

/**
 * Grupo de links para assunto
 */
function get_editar_cargo_url($cargo = array()) {
	if(empty($cargo))
		return base_url('admin/editar_cargo');

	return base_url('admin/editar_cargo/' . $cargo['car_id']);
}

function get_listar_cargos_url() {
	return base_url('admin/listar_cargos/');
}

function get_excluir_cargo_url($cargo) {
	return base_url('admin/excluir_cargo/' . $cargo['car_id']);
}

function get_listar_cargo_view_url() {
	return 'admin/listar_cargos';
}

function get_editar_cargo_view_url() {
	return 'admin/editar_cargo';
}

/**
 * Grupo de links para Assunto
 */
function get_editar_assunto_url($assunto = array()) {
	if(empty($assunto))
		return base_url('admin/editar_assunto');

	return base_url('admin/editar_assunto/' . $assunto['ass_id']);
}

function get_conversao_assuntos_url() {
	return base_url('admin/conversao_assuntos/');
}

/**
 * Retorna a URL de Admin > Assuntos > Ordem da Taxonomia
 *
 * @since 10.1.0
 * 
 * @return string URL
 */

function get_ordem_taxonomia_url()
{
	return base_url('admin/ordem_taxonomia/');
}

function get_listar_assuntos_url() {
	return base_url('admin/listar_assuntos/');
}

function get_excluir_assunto_url($assunto) {
	return base_url('admin/excluir_assunto/' . $assunto['ass_id']);
}

function get_excluir_conversao_url($assunto_antigo_id) {
	return base_url('admin/excluir_conversao/' . $assunto_antigo_id);
}

function get_rodar_conversao_url($assunto_antigo_id, $default_dis_id) {
	$url = "admin/rodar_conversao/" . ($assunto_antigo_id?$assunto_antigo_id:"0");
	if($default_dis_id){
		$url = $url."/".$default_dis_id;
	}
	return base_url($url);
}

function get_listar_assunto_view_url() {
	return 'admin/listar_assuntos';
}

function get_editar_assunto_view_url() {
	return 'admin/editar_assunto';
}

function get_ajax_get_ass_id_url($destacar_assuntos_folha = false, $remover_sem_assunto = false, $param = 'dis_id', $somente_ativos = SIM, $selecionado_ass_id = 0) {
	return '/questoes/admin/get_ass_id_ajax/' . ($destacar_assuntos_folha ? 1 : 0) . '/' . ($remover_sem_assunto ? 1 : 0) . '/' . $param . '/' . ($somente_ativos?:0)  . '/' . ($selecionado_ass_id ?: 0);
}

function get_ajax_main_ass_ids_url() {
	return '/questoes/main/get_main_ass_id_ajax/';
}

/**
 * Grupo de links para Prova
 */
function get_editar_prova_url($prova = array()) {
	if(empty($prova))
		return base_url('admin/editar_prova');

	return base_url('admin/editar_prova/' . $prova['pro_id']);
}

function get_duplicar_prova_url($prova)
{
	return base_url('admin/duplicar_prova/' . $prova['pro_id']);
}

function get_listar_provas_url() {
	return base_url('admin/listar_provas/');
}

function get_excluir_prova_url($prova) {
	return base_url('admin/excluir_prova/' . $prova['pro_id']);
}

function get_listar_questoes_prova_url($prova) {
	return base_url("main/resolver_questoes/pro/{$prova['pro_id']}/0");
}

function get_listar_prova_view_url() {
	return 'admin/provas/listar_provas';
}

function get_editar_prova_view_url() {
	return 'admin/editar_prova';
}

function get_prova_pdf_url($prova_id) {
	return './uploads/provas/' . $prova_id ;
}

function get_prova_pdf_link($prova) {
	return base_url('uploads/provas/' . $prova);
}

function get_exluir_prova_pdf_link($prova_id) {
	return base_url('admin/excluir_prova_pdf/' . $prova_id);
}

function get_exluir_gabarito_pdf_link($prova_id) {
	return base_url('admin/excluir_gabarito_pdf/' . $prova_id);
}

function get_gabarito_pdf_url($prova_id) {
	return './uploads/gabaritos/' . $prova_id;
}

function get_gabarito_pdf_link($prova) {
	return base_url('uploads/gabaritos/' . $prova);
}

function get_edital_pdf_url($prova_id) {
	return './uploads/editais/' . $prova_id;
}

function get_edital_pdf_link($prova) {
	return base_url('uploads/editais/' . $prova);
}

function get_exluir_edital_pdf_link($prova_id) {
	return base_url('admin/excluir_edital_pdf/' . $prova_id);
}

/**
 * Grupo de links para Questão
 */

function get_questao_seo_url($questao) 
{
	$url = slugify($questao['org_nome'] . '-' . $questao['pro_nome'] . '-' 
		. $questao['pro_ano'] . '-' . $questao['ban_nome'] . '-' . $questao['dis_nome']);

	return base_url('/' . $questao['que_id'] . '/' . $url);
}

/**
 * Recupera o link de edição ou adição de questão
 * 
 * @param int|array|null Id ou array associativo da questão. Se vazio ou null retorna link de adição de nova questão
 * 
 * @return string
 */

function get_editar_questao_url($questao = array()) {
	if(empty($questao)) {
		return base_url('admin/editar_questao');
	}

	if(is_numeric($questao)) {
		$questao = array('que_id' => $questao);
	}

	return base_url('admin/editar_questao/' . $questao['que_id']);
}

function get_aprovar_questao_url($questao_id){
	if(!$questao_id || !is_administrador()){
		return "";
	}
	return base_url('admin/aprovar_questao/' . $questao_id);
}

function get_reprovar_questao_url($questao_id){
	if(!$questao_id || !is_administrador()){
		return "";
	}
	return base_url('admin/reprovar_questao/' . $questao_id);
}

/**
 * Recupera o link de exclusão de comentário de questão na edição da questão
 * 
 * @param int Id da questão
 * @param int Id do comentário
 * 
 * @return string
 */

function get_excluir_comentario_questao_admin($questao_id, $comentario_id)
{
	return base_url("admin/excluir_comentario_questao/$questao_id/$comentario_id");
}

function get_adicionar_questao_url($questao) {
	return base_url('admin/adicionar_questao') . "/{$questao['que_id']}";
}

function get_listar_questoes_url() {
	return base_url('admin/listar_questoes');
}

function get_listar_questoes_professor_view_url(){
	return 'admin/questoes/professor/listar_questoes_professor';
}

function get_listar_questoes_professor_url(){
	return base_url('admin/listar_questoes_professor/');
}

function get_excluir_questao_url($questao) {
	return base_url('admin/excluir_questao/' . $questao['que_id']);
}

function get_listar_questoes_view_url() {
	return 'admin/questoes/listar_questoes';
}

function get_admin_questoes_filtro_modal_view_url() {
	return 'admin/questoes/filtro/filtro_modal';
}

function get_admin_simulados_revisar_questoes_filtro_modal_view_url() {
	return 'admin/simulados/revisar_questoes/filtro/filtro_modal';
}


function get_editar_questao_view_url() {
	return 'admin/questoes/editar_questao';
}

function get_contar_questoes_view_url() {
	return 'admin/questoes/contar_questoes';
}

function get_historico_questao_view_url() {
	return 'admin/historico_questao';
}

function get_contagem_questoes_view_url() {
	return 'admin/contagem_questoes';
}

function get_listar_questoes_repetidas_url() {
	return base_url() . "admin/listar_questoes_repetidas";
}

function get_comparar_questoes_url($master_id, $repetida_id) {
	return base_url() . "admin/comparar_questoes/{$master_id}/{$repetida_id}";
}

function get_questoes_repetidas_excecucoes_url() {
	return base_url() . "admin/questoes_repetidas_execucoes";
}

function get_questoes_repetidas_excecucoes_detalhes_url($id) {
	return base_url() . "admin/questoes_repetidas_execucoes_detalhes/{$id}";
}

function get_questoes_repetidas_configuracoes_url() {
	return base_url() . "admin/editar_configs_questoes_repetidas";
}

function get_ajax_main_remover_formatacao_alternativas_url() {
	return "/questoes/main/get_ajax_main_remover_formatacao_alternativas";
}

/**
 * Retorna a URL de Admin > Questões > Contagem de Questões
 *
 * @since 10.0.0
 * 
 * @return string URL
 */

function get_contagem_questoes_url() {
	return base_url('admin/contar_questoes');
}

function get_modal_adicionar_questao_url($questao) {
	return "/questoes/admin/get_modal_adicionar_questao/{$questao['que_id']}";
}

function get_adicionar_comentario_url($comentario = null) {
	if(is_null($comentario))
		return "/questoes/main/adicionar_comentario/";
	else 
		return "/questoes/main/adicionar_comentario/{$comentario['com_id']}";
}

function get_editar_comentario_url() {
	return "/questoes/barra_inferior/editar_comentario";
}

function get_excluir_comentario_url() {
		return "/questoes/main/excluir_comentario/";
}

function get_ajax_get_que_resposta_url() {
	return '/questoes/admin/get_que_resposta_ajax/';
}

function get_ajax_main_comentarios_questao_url($simulado_id = null) {
	$sufixo = $simulado_id ?: '';
	
	return "/questoes/main/get_ajax_main_comentarios_questao_url/" . $sufixo;
}

function get_ajax_main_avaliar_comentario_url(){
	return "/questoes/main/get_ajax_main_avaliar_comentario_url/";
}

function get_ajax_cadernos_adicionar_caderno_questao_url() {
	return "/questoes/cadernos_xhr/get_adicionar_caderno_questao/";
}

function get_ajax_main_estatisticas_questao_url() {
	return "/questoes/main/get_ajax_main_estatisticas_questao/";
}

/**
 * Grupo de links para Área de Formação
 */
function get_editar_area_formacao_url($area_formacao = array()) {
	if(empty($area_formacao))
		return base_url('admin/editar_area_formacao');

	return base_url('admin/editar_area_formacao/' . $area_formacao['arf_id']);
}

function get_listar_areas_formacao_url() {
	return base_url('admin/listar_areas_formacao/');
}

function get_excluir_area_formacao_url($area_formacao) {
	return base_url('admin/excluir_area_formacao/' . $area_formacao['arf_id']);
}

function get_listar_areas_formacao_view_url() {
	return 'admin/listar_areas_formacao';
}

function get_editar_area_formacao_view_url() {
	return 'admin/editar_area_formacao';
}

/**
 * Grupo de links para Área de Atuação
 */
function get_editar_area_atuacao_url($area_atuacao = array()) {
	if(empty($area_atuacao))
		return base_url('admin/editar_area_atuacao');

	return base_url('admin/editar_area_atuacao/' . $area_atuacao['ara_id']);
}

function get_listar_areas_atuacao_url() {
	return base_url('admin/listar_areas_atuacao/');
}

function get_excluir_area_atuacao_url($area_atuacao) {
	return base_url('admin/excluir_area_atuacao/' . $area_atuacao['ara_id']);
}

function get_listar_areas_atuacao_view_url() {
	return 'admin/listar_areas_atuacao';
}

function get_editar_area_atuacao_view_url() {
	return 'admin/editar_area_atuacao';
}

/**
 * Grupo de links para Simulado
 */

function get_listar_simulados_url($is_busca_sessao = FALSE) {
	if($is_busca_sessao == TRUE){
		$offset = $_SESSION['listar_simulado_filtro']['offset'];

		if(is_null($offset)){
			$offset = 0;
		}

		return base_url('admin/listar_simulados').'/'.$offset;
	}
	return base_url('admin/listar_simulados');
}

function get_listar_simulados_view_url() {
	return 'admin/simulados/listar_simulados';
}

/**
 * Retorna a URL de Admin > Simulados > Prioridade de Simulados
 *
 * @since 10.0.0
 * 
 * @return string URL
 */

function get_prioridade_simulados_url() {
	return base_url('admin/prioridade_simulados');
}

function get_mudar_status_simulado_url($simulado) {
	return base_url('admin/mudar_status_simulado') . "/{$simulado['sim_id']}";
}

function get_designar_alunos_url($simulado) {
	return base_url('admin/designar_alunos') . "/{$simulado['sim_id']}";
}

function get_editar_revisores_simulado_url($simulado_id) {
	return base_url("admin/editar_revisores/{$simulado_id}");
}

function get_rotulos_materias_simulado_url($simulado_id) {
	return base_url("admin/rotulos_materias/{$simulado_id}");
}

function get_excluir_rotulo_url($simulado_id, $rotulo_id) {
	return base_url("admin/excluir_rotulo/$simulado_id/$rotulo_id");
}

function get_editar_simulado_url($simulado = array(), $passo = null) {
	if(empty($simulado)){
		return base_url('admin/editar_simulado');
	}
	if(is_null($passo)){
		return base_url('admin/editar_simulado'). "/{$simulado['sim_id']}";
	}
	return base_url('admin/editar_simulado'). "/{$simulado['sim_id']}/{$passo}";
}

function get_editar_simulado_view_url($passo) {
	return "admin/simulados/editar_simulado_{$passo}";	
}

function get_excluir_simulado_url($simulado) {
	return base_url('admin/excluir_simulado/' . $simulado['sim_id']);
}

function get_simulado_img_url($simulado_id) {
	$result = glob("uploads/simulados/img_simulado_{$simulado_id}.*");
	return $result[0];
}

function get_simulado_img_link($simulado_id) {
	$result = glob("uploads/simulados/img_simulado_{$simulado_id}.*");
	return base_url($result[0]);
}

function get_exluir_simulado_img_link($simulado_id) {
	return base_url('admin/excluir_simulado_img/' . $simulado_id);
}

function get_simulado_pdf_link($simulado_id) {
	$result = glob("uploads/simulados/pdf_simulado_{$simulado_id}.*");
	return base_url($result[0]);
}

function get_exluir_simulado_pdf_link($simulado_id) {
	return base_url('admin/excluir_simulado_pdf/' . $simulado_id);
}

function get_img_loading() {
	return base_url("/assets-admin/img/loading.gif");
}

function get_ajax_get_simulado_ass_id_url() {
	return '/questoes/admin/get_simulado_ass_id_ajax/';
}

function get_simulado_revisores_url($simulado_id) {
	return '/questoes/admin/simulado_revisores/' . $simulado_id;
}

function get_revisar_questoes_view_url() {
	return 'admin/simulados/revisar_questoes/revisar_questoes';
}

function get_revisar_questoes_url($simulado_id, $disciplina_id = '') {
	return base_url('admin/revisar_questoes' . '/' . $simulado_id . '/' . $disciplina_id);
}

function get_adicionar_questoes_ao_simulado_url($simulado_id, $disciplina_id = '') {
	return base_url('admin/adicionar_questoes_ao_simulado/' . $simulado_id . '/' . $disciplina_id);
}

function get_excluir_questoes_de_simulado_url($simulado_id, $disciplina_id = '') {
	return base_url('admin/excluir_questoes_de_simulado/' . $simulado_id . '/' . $disciplina_id);
}

function get_alterar_status_simulado_url($simulado_id) 
{
	return base_url('admin/alterar_status_simulado/' . $simulado_id);
}

function get_ativar_simulado_url($simulado_id) 
{
	return base_url('admin/ativar_simulado/' . $simulado_id);
}

function get_inativar_simulado_url($simulado_id)
{
	return base_url('admin/inativar_simulado/' . $simulado_id);
}

function get_ativar_revisao_url($simulado_id)
{
	return base_url('admin/ativar_revisao/' . $simulado_id);
}

function get_inativar_revisao_url($simulado_id)
{
	return base_url('admin/inativar_revisao/' . $simulado_id);
}

function get_atualizar_notas_url($simulado_id)
{
	return base_url('admin/atualizar_notas/' . $simulado_id);
}

/**
 * Grupo de links para Produtos
 */

function get_listar_cadernos_produtos_url() 
{
	return base_url(get_listar_cadernos_produtos_view_url());
}

function get_listar_cadernos_produtos_view_url() 
{
	return 'admin/listar_produtos';
}

function get_editar_caderno_produto_url($produto_id = null) 
{
	if(is_null($produto_id))
		return base_url(get_editar_caderno_produto_view_url());

	return base_url(get_editar_caderno_produto_view_url()). "/{$produto_id}";
}

function get_ativar_desativar_caderno_produto_link($produto)
{
	if($produto['cpr_status'] == ATIVO) {
		$label = "Inativar";
		$url = base_url("/admin/inativar_caderno_produto/{$produto['cpr_id']}");
	}
	else {
		$label = "Ativar";
		$url = base_url("/admin/ativar_caderno_produto/{$produto['cpr_id']}");
	}

	return "<a href='{$url}'>{$label}</a>";
}

function get_editar_caderno_produto_view_url()
{
	return 'admin/editar_produto';
}

/**
 * Grupo de links para Ranking
 */
function get_excluir_ranking_url($ranking_id) {
	return base_url('admin/excluir_ranking/' . $ranking_id);
}

function get_listar_rankings_url() 
{
	return base_url(get_listar_rankings_view_url());
}


function get_editar_ranking_url($id = null, $pagina = 1)
{
	if($id) {
		return base_url('admin/editar_ranking/' . $id . '/' . $pagina);	
	}
	return base_url('admin/editar_ranking/');
}

function get_listar_rankings_view_url() 
{
	return 'admin/listar_rankings';
}

function get_editar_ranking_view_url($pagina)
{
	return 'admin/editar_ranking_' . $pagina;
}

function get_editar_grupo_url($id)
{
	if($id) {
		return base_url('admin/editar_grupo/' . $id );	
	}
	return base_url('admin/editar_grupo/');
}

function get_excluir_grupo_url($id) {
	return base_url('admin/excluir_grupo/' . $id);
}

/**
 * Grupo de links para Area do Aluno
 */

function get_main_header_view_url() {
	return 'main/header';
}

function get_main_footer_view_url() {
	return 'main/footer';
}

function get_main_menu_view_url() {
	return 'main/menu';
}

function get_main_topbar_view_url() {
	return 'main/topbar';
}

function get_admin_topbar_view_url() {
	return 'admin/topbar';
}

function get_resolver_questoes_view_url() {
	return 'main/resolver_questoes';
}

function get_resolver_simulado_view_url() {
	return 'main/resolver_simulado';
}

function get_listar_imagens_tratadas_view_url() {
	return 'admin/questoes/imagens/listar_imagens_tratadas';
}

function get_listar_imagens_tratadas_url() {
	return base_url('admin/listar_imagens_tratadas');
}

function get_contar_questoes_url() {
	return base_url('admin/contar_questoes');
}

function get_correcao_assuntos_view_url() {
	return 'admin/correcao_assuntos';
}

function get_correcao_assuntos_url() {
	return base_url(get_correcao_assuntos_view_url());
}

function get_listar_imagens_nao_tratadas_url($offset = null) {
	$sufixo = $offset ? "/" . $offset : "";

	return base_url('admin/listar_imagens_nao_tratadas' . $sufixo);
}

function get_confirmar_tratar_imagem_url($imagem_id, $offset = null)
{
	$sufixo = $offset ? "/" . $offset : "";
	return base_url("admin/confirmar_tratar_imagem/{$imagem_id}" . $sufixo);
}

function get_listar_imagens_nao_tratadas_view_url() {
	return 'admin/questoes/imagens/listar_imagens_nao_tratadas';
}

function get_tratar_imagem_url($imagem_id, $offset = null) {
	$sufixo = $offset ? "/" . $offset : "";

	return base_url('admin/tratar_imagem/' . $imagem_id . $sufixo);
}

function get_marcar_imagem_como_tratada_url($imagem_id, $offset) {
	return base_url('admin/marcar_imagem_como_tratada/' . $imagem_id . '/' . $offset);
}

function get_resolver_questoes_url($tipo = null, $id = null, $filtro_Questoes = TODAS) {
	if(is_null($tipo))
		return base_url(get_resolver_questoes_view_url());
	return base_url(get_resolver_questoes_view_url() . "/{$tipo}/{$id}/{$filtro_Questoes}");
}

function get_favoritas_url($valida = TRUE) {
	// return base_url(get_favoritas_view_url()) "?zm=1";

	if($valida == FALSE){
		return base_url(get_favoritas_view_url());
	}

	if(!is_usuario_logado()) {
		return login_url();
	}
	else {
		/*if(tem_acesso(array(
				ADMINISTRADOR, PROFESSOR, CONSULTOR, ASSINANTE_SQ, ALUNO, REVISOR
		))) {
			return base_url(get_favoritas_view_url());
		}
		return '#modal-assine-sq';*/
		return base_url(get_favoritas_view_url());
	}
}

function get_configuracoes_url() 
{
	return base_url("/perfil/configuracoes");
}

function get_resolver_favoritas_url($disciplina_id = null) {
	$sufixo = $disciplina_id ? "/" . $disciplina_id  : "";

	return base_url("/main/resolver_favoritas" . $sufixo);
}

function get_favoritas_view_url()
{
	return 'main/favoritas';
}

function get_resolver_url() {
	return base_url("/main/resolver");
}

function get_questao_url($questao_id) 
{
	return base_url("/main/resolver_questoes/que/{$questao_id}/0");
}

function get_historico_questao_url($questao_id)
{
	return base_url("/admin/historico_questao/{$questao_id}");
}

function get_url_reduzida($questao) {
	$CI = &get_instance();
	$CI->load->helper('google');
	$CI->load->model('Questao_model');
	
	if(is_null($questao['que_url'])) {
		$url = get_resolver_questoes_url(TIPO_QUESTAO, $questao['que_id']);
		$url_reduzida = get_shorten_url($url);
		
		$CI->Questao_model->atualizar_url_reduzida($questao['que_id'], $url_reduzida);
		
		return $url_reduzida;
	}
	else {
		return $questao['que_url'];
	}
	
}

function get_acesso_meus_cadernos_url()
{
	return '/questoes/cadernos/acesso_meus_cadernos';
}

function listar_todas_categorias_meus_cadernos_url()
{
	return '/questoes/cadernos/limpar_categoria';
}

function get_meus_cadernos_url($valida = TRUE)
{
	 
	if($valida == FALSE){
		return base_url(get_meus_cadernos_view_url());
	}

	if(!is_usuario_logado()) {
		return login_url();
	}
	else {
		// if(tem_acesso(array(
		// 		ADMINISTRADOR, PROFESSOR, CONSULTOR, ASSINANTE_SQ, ALUNO, REVISOR
		// ))) {
			return base_url(get_meus_cadernos_view_url());
		// }
		// return '#modal-assine-sq';
	}
}

function get_cadernos_exponencial_url()
{
   return base_url("/cadernos/exponencial");
}

function get_meus_cadernos_view_url()
{
	return 'cadernos/meus_cadernos';
}

function get_cadernos_url(){
	if(!is_usuario_logado()) {
		return login_url();
	}
	else {
		return base_url(get_cadernos_view_url());
	}
}

function get_cadernos_view_url(){
	return 'cadernos/cadernos';
}

function get_adicionar_a_novo_caderno_url()
{
	return base_url(get_adicionar_a_novo_caderno_view_url());
}

function get_adicionar_a_nova_categoria_url()
{
	return base_url(get_adicionar_a_nova_categoria_view_url());
}

function get_adicionar_a_nova_categoria_cp_url()
{
	return base_url(get_adicionar_a_nova_categoria_cp_view_url());
}

function get_editar_caderno_url()
{
	return base_url('main/editar_caderno'); 
}

function get_editar_categoria_url()
{
	return base_url('main/editar_categoria');
}

function get_editar_categoria_produto_url()
{
	return base_url('admin/editar_categoria_produto');
}

function get_excluir_caderno_url()
{
	return base_url('main/excluir_caderno');
}

function get_excluir_caderno_compartilhado_url()
{
	return base_url('main/excluir_caderno_compartilhado');
}

function get_excluir_categoria_url()
{
	return base_url('main/excluir_categoria');
}

function get_excluir_categoria_produto_url()
{
	return base_url('admin/excluir_categoria_produto');
}

function get_adicionar_a_caderno_existente_url()
{
	return base_url(get_adicionar_a_caderno_existente_view_url());
}

function get_adicionar_a_categoria_existente_url()
{
	return base_url(get_adicionar_a_categoria_existente_view_url());
}

function get_adicionar_a_categoria_existente_view_url()
{
	return 'main/adicionar_categoria_existente';
}

function get_adicionar_a_categoria_cp_existente_url()
{
	return base_url(get_adicionar_a_categoria_cp_existente_view_url());
}

function get_adicionar_a_categoria_cp_existente_view_url()
{
	return 'admin/adicionar_categoria_cp_existente';
}

function get_adicionar_a_caderno_existente_view_url()
{
	return 'main/adicionar_caderno_existente';
}

function get_adicionar_a_novo_caderno_view_url()
{
	return 'main/adicionar_novo_caderno';
}

function get_adicionar_a_nova_categoria_view_url()
{
	return 'main/adicionar_nova_categoria';
}

function get_adicionar_a_nova_categoria_cp_view_url()
{
	return 'admin/adicionar_nova_categoria_cp';
}

function get_modal_designar_caderno_view_url() {
	return 'cadernos/modal_designar_caderno';
}

function get_modal_compartilhar_caderno_view_url() {
	return 'modal/compartilhar_caderno';
}

function get_modal_designar_caderno_url($caderno_id) {
	return base_url(get_modal_designar_caderno_view_url() . "/" . $caderno_id);
}

function get_modal_compartilhar_caderno_url($caderno_id) {
	return base_url(get_modal_compartilhar_caderno_view_url() . "/" . $caderno_id);
}

function get_designar_caderno_url($caderno_id) {
	return base_url('cadernos/designar_caderno') . "/" . $caderno_id;
}

function get_resolver_simulado_url($simulado_id, $zerar_tempo_gasto = true, $pagina = NULL) {
// 	$sufixo = $zerar_tempo_gasto ? '?start=1' : '';
	
	$pagina_fragmento = !is_null($pagina) ? "/" . $pagina : "";
	
	return base_url('main/resolver_simulado/' . $simulado_id . $pagina_fragmento /* . $sufixo */);
}

function get_resolver_caderno_url($caderno_id, $pagina = NULL, $zerar_filtros = FALSE) {
	$pagina_fragmento = !is_null($pagina) ? "/" . $pagina : "";

	$sufixo = $zerar_filtros ? "?zf=1" : "";
	
	return base_url('main/resolver_caderno/' . $caderno_id . $pagina_fragmento . $sufixo);
}

function get_alterar_questoes_por_pagina_url()
{
	return base_url('main/alterar_questoes_por_pagina/');
}

function get_alterar_questoes_por_pagina_caderno_url()
{
	return base_url('cadernos/alterar_questoes_por_pagina/');
}

function get_imprimir_questoes_view_url() 
{
	return 'main/imprimir_questoes';
}

function get_embaralhar_questoes_url($caderno_id, $ref = FALSE)
{
	$sufixo = $ref ? "?ref={$_SERVER['REQUEST_URI']}" : "";

	return base_url('cadernos/embaralhar_questoes/' . $caderno_id . $sufixo);
}

function get_imprimir_questoes_url() {
	return base_url(get_imprimir_questoes_view_url());
}

function get_imprimir_questoes_html_url() {
	$query_string = "";
	if($_SERVER['QUERY_STRING']){
		$query_string = "?".$_SERVER['QUERY_STRING'];
	}
	return base_url('/main/imprimir_questoes_html') . $query_string;
}

function get_ajax_main_notificar_erro_questao_url() {
	return '/questoes/main/ajax_notificar_erro_questao';
}

function get_xhr_cadernos_adicionar_novo_caderno_url() {
	return '/questoes/cadernos_xhr/adicionar_novo_caderno';
}

function get_xhr_cadernos_adicionar_caderno_existente_url() {
	return '/questoes/cadernos_xhr/adicionar_caderno_existente';
}

function get_modal_assine_sq_view_url() {
	return 'modal/assine_sq';
}

function get_modal_detalhe_resultado_simulado_view_url() {
	return 'simulados/resim/resultado_modal';
}

function get_questoes_barra_tarefas_adicionar_a_caderno_view_url() {
	return 'questoes/barra_tarefas/adicionar_a_caderno/adicionar_a_caderno_modal';
}

function get_main_form_editar_caderno_view_url() {
	return 'main/form_editar_caderno';
}

function get_main_form_editar_categoria_view_url() {
	return 'main/form_editar_categoria';
}

function get_main_form_categorizar_caderno_view_url() {
	return 'main/form_categorizar_caderno';
}

function get_main_form_categorizar_caderno_produto_view_url() {
	return 'main/form_categorizar_caderno_produto';
}

function get_admin_form_editar_categoria_produto_view_url() {
	return 'admin/form_editar_categoria_produto';
}

function get_ajax_main_questao_dificuldade_url() {
	return '/questoes/main/get_ajax_main_questao_dificuldade/';
}

function get_ajax_main_questao_ver_dificuldade_url(){
	return '/questoes/main/get_ajax_main_questao_ver_dificuldade/';
}

function get_ajax_main_responder_url() {
	return '/questoes/main/get_ajax_main_responder_url/';
}

function get_ajax_main_alunos_acompanhamento_url($pagina) {
	return '/questoes/main/get_ajax_main_alunos_acompanhamento/'.$pagina;
}

function get_ajax_main_resultado_detalhado_url() {
	return '/questoes/main/main_resultado_detalhado_url/';
}

function get_ajax_main_resultado_url($simulado_id) {
	return '/questoes/xhr/modal_resultados_simulados/' . $simulado_id;
}

function get_ajax_main_form_notificacao_url() {
	return '/questoes/main/get_ajax_main_form_notificacao_url/';
}

function get_painel_aluno_view_url() {
	return 'main/painel_aluno';
}

function get_painel_aluno_url($valida = TRUE) {

	if($valida == FALSE){
		return base_url('desempenho/disciplinas');
	}

	if(!is_usuario_logado()) {
		return login_url();	
	}
	else {
		/*if(tem_acesso(array(
				ADMINISTRADOR, PROFESSOR, CONSULTOR, ASSINANTE_SQ, ALUNO, REVISOR	
			))) {
			return base_url('desempenho/disciplinas');
		}
		return "#modal-assine-sq";*/
		return base_url('desempenho/disciplinas');
	}
}

function get_assine_sq_url() {
	return base_url('main/assine');
}

function get_meus_simulados_view_url() {
	return 'simulados/meus';
}

function get_meus_simulados_url() {
	return base_url(get_meus_simulados_view_url());
}

function get_imprimir_meus_simulados_view_url() {
	return 'main/imprimir_meus_simulados';
}

function get_imprimir_meus_cadernos_view_url() {
	return 'main/imprimir_meus_cadernos';
}

function get_imprimir_meus_simulados_url($simulado) {
	return base_url(get_imprimir_meus_simulados_view_url() . "/{$simulado['sim_id']}");
}

function get_imprimir_meus_simulados_html_url($simulado) {
	return base_url("/main/imprimir_meus_simulados_html/{$simulado['sim_id']}");
}

function get_imprimir_resultado_simulado_url($simulado, $tipo_resultado = SIMULADO_QUESTAO_TODAS) {
	return base_url("/main/imprimir_resultado_simulado/{$simulado['sim_id']}/{$tipo_resultado}");
}

function get_imprimir_gabarito_simulado_url($simulado) {
	return base_url("/main/imprimir_gabarito/{$simulado['sim_id']}");
}

function get_finalizar_simulado_url($simulado) {
	return base_url("/main/finalizar_simulado/{$simulado['sim_id']}");
}

function get_imprimir_cartao_resposta_simulado_url($simulado) {
	return base_url("/main/imprimir_cartao_resposta/{$simulado['sim_id']}");
}

function get_duplicar_simulado_url($simulado, $opcao) {
	return base_url("/main/imprimir_meus_simulados_html/{$simulado['sim_id']}");
}

function get_imprimir_meus_cadernos_url($caderno) {
	return base_url(get_imprimir_meus_cadernos_view_url() . "/{$caderno['cad_id']}");
}

function get_imprimir_meus_cadernos_html_url($caderno) {
	$query_string = "";
	if($_SERVER['QUERY_STRING']){
		$query_string = "?".$_SERVER['QUERY_STRING'];
	}
	return base_url("/main/imprimir_meus_cadernos_html/{$caderno['cad_id']}") . $query_string;
}

function get_modal_resultados_simulados_view_url() {
	return 'xhr/modal_resultados_simulados';
}

function get_modal_resultados_simulados_url($simulado_id = null) {
	if($simulado_id){
		return base_url(get_modal_resultados_simulados_view_url()) . "/{$simulado_id}";
	}
	return base_url(get_modal_resultados_simulados_view_url());
}

function get_revisar_simulado_url($simulado_id) {
	return get_resolver_simulado_url($simulado_id) . '/0/1';
}

function get_modal_duplicar_simulados_view_url() {
	return 'admin/modal_duplicar_simulados';
}

function get_modal_duplicar_simulados_url($simulado) {
	return base_url(get_modal_duplicar_simulados_view_url() . "/{$simulado['sim_id']}");
}

function get_modal_comentarios_professores_simulados_view_url() {
	return 'admin/modal_comentarios_professores_simulado';
}

function get_modal_comentarios_professores_simulados_url($simulado_id) {
	return base_url(get_modal_comentarios_professores_simulados_view_url() . "/{$simulado_id}");
}

function get_modal_comentarios_professores_cadernos_view_url() {
	return 'admin/modal_comentarios_professores_caderno';
}

function get_modal_comentarios_professores_cadernos_url($caderno_id) {
	return base_url(get_modal_comentarios_professores_cadernos_view_url() . "/{$caderno_id}");
}

function get_modal_indice_caderno_url($caderno_id)
{
	return "cadernos/modal_indice/" . $caderno_id;
}

function get_duplicar_simulados_url($simulado_id, $opcao) {
	return base_url("admin/duplicar_simulado/{$simulado_id}/{$opcao}");
}

function get_salvar_resultado_url($simulado = array()) {
	if(empty($simulado))
		return base_url('main/salvar_resultado');
	return base_url('main/salvar_resultado/'. $simulado['sim_id']);
}

function get_salvar_resultado_detalhado_url($simulado = array()) {
	if(empty($simulado))
		return base_url('main/salvar_resultado_detalhado');
	return base_url('main/salvar_resultado_detalhado/'. $simulado['sim_id']);
}

function get_ranking_view_url() {
	return 'main/ranking';
}

function get_ranking_excel_view_url(){
	return 'main/ranking_excel';
}

function get_ranking_simulado_url($simulado_id, $somente_aprovados = 0, $offset = 0, $exportar = 0, $valida = TRUE)
{
	if($valida == TRUE && is_visitante()) {
		return login_url();
	}

	$sufixo = $offset ? "/$offset" : "";

	if($exportar && tem_acesso(array(ADMINISTRADOR, COORDENADOR_COACHING, COORDENADOR_SQ))){
		if(empty($sufixo)){
			$sufixo = "/0/".$exportar;
		}else{
			$sufixo = $sufixo."/{$exportar}";
		}
		
	}

	return base_url("main/ranking/{$simulado_id}/{$somente_aprovados}" . $sufixo);
}


function get_editar_rotulo_url($simulado_id, $rotulo_id = null) {
	$rotulo_id = $rotulo_id ? "/$rotulo_id" : "";
	return base_url("admin/editar_rotulo/$simulado_id" . $rotulo_id);
}

function get_editar_perfil_url() 
{
	return base_url('perfil/editar');
}

function get_editar_perfil_view_url()
{
	return 'perfil/edicao/editar';
}

function get_perfil_url() 
{
	return base_url('perfil');
}

function get_perfil_view_url() 
{
	return 'perfil/visualizacao/index';
}

function get_categoria_url($categoria_id, $com_query_string = FALSE)
{
	$query_string = "";
	if($com_query_string && $_SERVER['QUERY_STRING']){
		$query_string = "?".$_SERVER['QUERY_STRING'];
	}
	return base_url('cadernos/meus_cadernos/' . $categoria_id).$query_string;
}

function get_categoria_produto_url($categoria_id)
{
	return base_url('admin/listar_produtos/' . $categoria_id);
}

function get_buscador_provas_dir()
{
	return $_SERVER['DOCUMENT_ROOT'] . '/questoes/buscador/provas/';
}

function get_buscador_questoes_dir()
{
	return $_SERVER['DOCUMENT_ROOT'] . '/questoes/buscador/questoes/';
}

function get_excluir_questao_de_caderno_url($caderno_id, $questao_id, $pagina_retorno = 1)
{
	return base_url('cadernos/excluir_questao_de_caderno/' . $caderno_id . '/' . $questao_id . '/' . $pagina_retorno);
}

function get_caderno_compartilhado_url($caderno_id)
{
	return base_url('/cadernos/publico/' . encode_id_compartilhado($caderno_id));
}

function get_caderno_coaching_url($caderno_id)
{
	return base_url('/cadernos/coaching/' . encode_id_compartilhado($caderno_id));
}

function gerar_novo_caderno_url($caderno_id)
{
	return base_url('/cadernos/gerar_novo/' . $caderno_id);
}

function get_ajax_cadernos_alunos_acompanhamento_url($pagina){
	return '/questoes/cadernos/get_ajax_cadernos_alunos_acompanhamento_url/'.$pagina;
}

function get_minha_colocacao_url($simulado_id, $usuario_id, $somente_aprovados)
{
	if($somente_aprovados && !is_simulado_usuario_aprovado($simulado_id, $usuario_id)) {
		$somente_aprovados = 0;
	}

	$offset = get_offset_ranking($simulado_id, $usuario_id, $somente_aprovados);

	return base_url("/main/ranking/$simulado_id/{$somente_aprovados}/{$offset}");
}

/* Relatório */
function get_rel_avaliacoes_comentarios_url() {
	return base_url("/relatorios/avaliacoes_comentarios/");
}

/**
 * Retorna URL para duplicação de cadernos
 *
 * @since L1
 * 
 * @param int $caderno_id Id do caderno
 
 * @throws InvalidArgumentException
 * 
 * @return string URL do caderno
 */ 

function get_duplicar_caderno_url($caderno_id) 
{
    /* === tratamento dos parâmetros === */
    if(!(filter_var($caderno_id, FILTER_VALIDATE_INT))) throw new InvalidArgumentException("Argumento 'caderno_id' precisa ser inteiro");
    
    /* === lógica principal === */
    return base_url("/cadernos/duplicar/{$caderno_id}");
}

/**
* Retorna URL para atualização de cadernos
*
* @since L1
* 
* @param int $caderno_id Id do caderno

* @throws InvalidArgumentException
* 
* @return string URL do caderno
*/ 

function get_atualizar_caderno_url($caderno_id) 
{
   /* === tratamento dos parâmetros === */
   if(!(filter_var($caderno_id, FILTER_VALIDATE_INT))) throw new InvalidArgumentException("Argumento 'caderno_id' precisa ser inteiro");
   
   /* === lógica principal === */
   return base_url("/cadernos/atualizar/{$caderno_id}");
}

/**
 * Retorna URL para zerar estatísticas de cadernos
 *
 * @since L1
 *
 * @return string URL da operacão de zerar estatísticas do caderno
 */ 

function get_zerar_estatisticas_url()
{
    return base_url('cadernos/zerar_estatisticas');
}

/**
 * 
 */
function get_query_string($get_array, $ignored_keys = null)
{
	if(is_null($get_array) || empty($get_array))
	{
		return "";
	}
	else
	{
		if(is_array($ignored_keys))
		{
			foreach($ignored_keys as $key)
			{
				unset($get_array[$key]);
			}
		}
		elseif(is_string($ignored_keys))
		{
			unset($get_array[$ignored_keys]);
		}

		$query_string = http_build_query($get_array);
		if($query_string){
			$query_string = "?" . $query_string;
		}

		return $query_string;
	}
}