<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function get_arquivo_edital($prova)
{
	$nome = get_nome_arquivo_prova( "edital", $prova );

	return $_SERVER['DOCUMENT_ROOT'] . '/questoes/uploads/editais/' . $nome ; 
}

function get_arquivo_prova($prova)
{
	$nome = get_nome_arquivo_prova( "prova", $prova );

	return $_SERVER['DOCUMENT_ROOT'] . '/questoes/uploads/provas/' . $nome ;
}

function get_arquivo_gabarito($prova)
{
	$nome = get_nome_arquivo_prova( "gabarito", $prova );

	return $_SERVER['DOCUMENT_ROOT'] . '/questoes/uploads/gabaritos/' . $nome ;
}

function get_nome_arquivo_prova ( $tipo, $prova )
{
	if(!is_array($prova))
	{
		$CI = &get_instance();
		if(!class_exists('Prova_model'))
        {
            $CI->load->model('prova_model');
		}
		$prova = $CI->prova_model->get_by_id( $prova );
	}

	if($prova['pro_prefixo_nome_arquivo'])
	{
		$nome = str_replace( "(tipo)", $tipo, $prova['pro_prefixo_nome_arquivo'] );
	}
	else
	{
		$nome = $prova['pro_id'] . ".pdf";
	}

	return $nome;
}

function get_abrir_edital_url($prova_id)
{
	return base_url('/buscador/abrir_edital/'. $prova_id);
}

function get_abrir_prova_url($prova_id)
{
	return base_url('/buscador/abrir_prova/'. $prova_id);
}

function get_abrir_gabarito_url($prova_id)
{
	return base_url('/buscador/abrir_gabarito/'. $prova_id);
}

function get_abrir_edital_link($prova_id, $class = '') 
{
	$url = get_abrir_edital_url($prova_id);
	return "<a target='_blank' class='{$class}' href='{$url}'>Abrir</a>";
}

function get_abrir_prova_link($prova_id, $class = '')
{
	$url = get_abrir_prova_url($prova_id);
	return "<a target='_blank' class='{$class}' href='{$url}'>Abrir</a>";
}

function get_abrir_gabarito_link($prova_id, $class = '')
{
	$url = get_abrir_gabarito_url($prova_id);
	return "<a target='_blank' class='{$class}' href='{$url}'>Abrir</a>";
}

function get_escolaridade_by_id($id) 
{
	$items = get_escolaridade_multiselect();

	return $items[$id];
}

function get_modalidade_by_id($id) 
{
	$items = get_modalidades_combo();

	return $items[$id];
}