<?php
function get_disciplina_multiselect($disciplinas, $sem_disciplina = true, $destaque_inativa = null) {

	$array = array ();

	if($sem_disciplina) {
		$array[-1] = "(Sem disciplina)";
	}

	foreach ($disciplinas as $disciplina) {
		$destacar = $disciplina['dis_ativo'] == NAO && $destaque_inativa;
		$array[$disciplina['dis_id']] = $disciplina['dis_nome'] . ( $destacar ? $destaque_inativa : "" );
	}

	return $array;
}

function get_disciplina_combo($disciplinas, $destaque_inativa = null) {
	$array = array (OPCAO_ZERO => "Selecione uma disciplina...");
	return array_replace($array, get_disciplina_multiselect($disciplinas, true, $destaque_inativa));
}

function get_disciplina_select2($disciplinas) {
	$array = array (NULL => "");
	return array_replace($array, get_disciplina_multiselect($disciplinas));
}

function get_disciplina_simulado_combo($disciplinas, $simulado_id) 
{
	$CI = &get_instance();
	$CI->load->model('simulado_model');

	$array = array (OPCAO_ZERO => "Selecione uma disciplina...");

	foreach ($disciplinas as $disciplina) {
		if(!pode_revisar_disciplina_simulado($simulado_id, $disciplina['dis_id'])) continue;		
		
		$atual_questoes = $CI->simulado_model->get_atual_questoes_disciplina($simulado_id, $disciplina['dis_id']);
		$max_questoes = $CI->simulado_model->get_maximo_questoes_disciplina($simulado_id, $disciplina['dis_id']);

		$completa = is_disciplina_simulado_completa($simulado_id, $disciplina['dis_id']) ? "" : " *";
		$sem_assunto = is_disciplina_simulado_com_questao_sem_assunto($simulado_id, $disciplina['dis_id']) ? " !" : "";
		$anulada_desatualizada = is_disciplina_simulado_com_questao_anulada_desatualizada($simulado_id, $disciplina['dis_id']) ? " #" : "";
		$outro_simulado = is_disciplina_simulado_com_questao_em_outro_simulado($simulado_id, $disciplina['dis_id']) ? " &" : "";

		$array[$disciplina['dis_id']] = "{$disciplina['dis_nome']} ({$atual_questoes}/{$max_questoes}){$completa}{$sem_assunto}{$anulada_desatualizada}{$outro_simulado}";
	}
	
	return $array;
}

function is_disciplina_simulado_com_questao_anulada_desatualizada($simulado_id, $disciplina_id){
	$CI = &get_instance();
	$CI->load->model('simulado_model');
	return $CI->simulado_model->is_disciplina_simulado_com_questao_anulada_desatualizada($simulado_id, $disciplina_id);
}

function is_disciplina_simulado_com_questao_em_outro_simulado($simulado_id, $disciplina_id){
	$CI = &get_instance();
	$CI->load->model('simulado_model');
	return $CI->simulado_model->is_disciplina_simulado_com_questao_em_outro_simulado($simulado_id, $disciplina_id);
}

/************************************************************************************************************
 * Uma disciplina de um simulado está completa se:
 * 1) Tem o número de questões conforme previsto
 * 2) Todas as questões possuem comentários de professores
 * 3) Todas as questões são ativas
 * https://podio.com/profinfnetedubr/lslc/apps/bug-tracker/items/1070
 ************************************************************************************************************/
function is_disciplina_simulado_completa($simulado_id, $disciplina_id)
{
	$CI = &get_instance();
	$CI->load->model('simulado_model');

	$atual_questoes = $CI->simulado_model->get_atual_questoes_disciplina($simulado_id, $disciplina_id);
	$max_questoes = $CI->simulado_model->get_maximo_questoes_disciplina($simulado_id, $disciplina_id);
	
	if($atual_questoes != $max_questoes) {
		return FALSE;
	}
	else {
		if(tem_questoes_sem_comentario_professor($simulado_id, $disciplina_id)) {
			return FALSE;
		}
		else {
			if(tem_questoes_inativas($simulado_id, $disciplina_id)) {
				return FALSE;
			}
			else {
				return TRUE;
			}
		}
	}
}

function is_disciplina_simulado_com_questao_sem_assunto($simulado_id, $disciplina_id)
{
	$CI = &get_instance();
	$CI->load->model('assunto_model');
	$CI->load->model('questao_model');
	
	$sem_assunto = $CI->assunto_model->get_by_nome_e_disciplina(SEM_ASSUNTO, $disciplina_id);
	$questoes = $CI->questao_model->listar_por_simulado($simulado_id, null, null, $disciplina_id);

	foreach ($questoes as $questao) {
		if($CI->questao_model->existe_assunto_em_questao($sem_assunto, $questao)) {
			return TRUE;
		}
	}
	
	return FALSE;
}


function tem_questoes_inativas($simulado_id, $disciplina_id)
{
	$CI = &get_instance();
	$CI->load->model('questao_model');

	$questoes = $CI->questao_model->listar_por_simulado_e_disciplina($simulado_id, $disciplina_id);

	foreach ($questoes as $questao) {
		if($questao['que_ativo'] == NAO) {
			return TRUE;
		}
	}

	return FALSE;
}

function tem_questoes_sem_comentario_professor($simulado_id, $disciplina_id) 
{
	$CI = &get_instance();
	$CI->load->model('comentario_model');
	$CI->load->model('questao_model');

	$questoes = $CI->questao_model->listar_por_simulado_e_disciplina($simulado_id, $disciplina_id);

	foreach ($questoes as $questao) {
		$num_comentarios_professor = $CI->comentario_model->contar_comentarios($questao['que_id'], SIM);

		if($num_comentarios_professor == 0) {
			return TRUE;		
		}
	}

	return FALSE;		
}



function get_disciplinas_ids($disciplinas) {
	$array = array ();
	foreach ($disciplinas as $disciplina)
		$array[] = $disciplina['dis_id'];

	return $array;
}

function get_assunto_multiselect($assuntos, $destacar_assuntos_folha = false, $remover_sem_assunto = false) {
	$CI = &get_instance();
	$CI->load->model('assunto_model');
	
	$array = array ();
	foreach ($assuntos as $assunto) {
		if($remover_sem_assunto) {
			if($assunto['ass_nome'] == SEM_ASSUNTO) continue;
		}
		
		$array[$assunto['ass_id']] = $assunto['ass_nome'];
		
		if($destacar_assuntos_folha) {
			$possui_filhos = $CI->assunto_model->possui_filhos($assunto['ass_id']);
			
			if(!$possui_filhos) {
				$array[$assunto['ass_id']] .= " *";
			}
		}
		
	}
	return $array;
}

function get_assunto_combo($assuntos, $destacar_assunto_folha = false, $remover_sem_assunto_false = false) {
	$array = array (OPCAO_ZERO => "Selecione um assunto...");
	return array_replace($array, get_assunto_multiselect($assuntos, $destacar_assunto_folha, $remover_sem_assunto_false));
}

function get_assuntos_ids($assuntos) {
	$array = array ();
	foreach ($assuntos as $assunto)
		$array[] = $assunto['ass_id'];

	return $array;
}

function get_provas_ids($provas) {
	$array = array ();
	foreach ($provas as $prova)
		$array[] = $prova['pro_id'];

	return $array;
}

function get_orgao_multiselect($orgaos) {
	$array = array ();
	foreach ($orgaos as $orgao)
		$array[$orgao['org_id']] = $orgao['org_nome'];

	return $array;
}

function get_orgao_combo($orgaos) {
	$array = array (OPCAO_ZERO => "Selecione um órgão...");
	return array_replace($array, get_orgao_multiselect($orgaos));
}

function get_banca_multiselect($bancas) {
	$array = array ();
	foreach ($bancas as $banca)
		$array[$banca['ban_id']] = $banca['ban_nome'];

	return $array;
}

function get_banca_combo($bancas) {
	$array = array (OPCAO_ZERO => "Selecione uma banca...");
	return array_replace($array, get_banca_multiselect($bancas));
}

function get_banca_select2($bancas) {
	$array = array (NULL => "");
	return array_replace($array, get_banca_multiselect($bancas));
}

function get_escolaridade_multiselect() {
	$array = array(
			SEM_ESCOLARIDADE => "(Sem escolaridade)",
			FUNDAMENTAL => "Fundamental",
			MEDIO => "Médio",
			SUPERIOR => "Superior"
	);
	return $array;
}

function get_escolaridade_combo() {
	$array = array (OPCAO_ZERO => "Selecione uma escolaridade...");
	return array_replace($array, get_escolaridade_multiselect());
}

function get_cargo_multiselect($cargos) {
	$array = array ();
	foreach ($cargos as $cargo)
		$array[$cargo['car_id']] = $cargo['car_nome'];

	return $array;
}

function get_cargo_combo($cargos) {
	$array = array (OPCAO_ZERO => "Selecione um cargo...");
	return array_replace($array, get_cargo_multiselect($cargos));
}

function get_cargos_ids($cargos) {
	$array = array ();
	foreach ($cargos as $cargo)
		$array[] = $cargo['car_id'];

	return $array;
}

function get_prova_multiselect($provas) {
	$array = array();
	foreach ($provas as $prova)
		$array[$prova['pro_id']] = $prova['pro_ano'] . ' - ' . $prova['pro_orgao_nome'] . ' - ' . $prova['pro_banca_nome'] . ' - ' . $prova['pro_nome'];

	return $array;
}

function get_prova_combo($provas) {
	$array = array (OPCAO_ZERO => "Selecione uma prova...");
	return array_replace($array, get_prova_multiselect($provas));
}

function get_resposta_combo($tipo) {
	$array = array();
	switch ($tipo) {	
		case MULTIPLA_ESCOLHA :
			$array = array(
				OPCAO_ZERO => "Selecione uma resposta...",
				OPCAO_A => "A",
				OPCAO_B => "B",
				OPCAO_C => "C",
				OPCAO_D => "D"
			);
			break;
		case MULTIPLA_ESCOLHA_5 :
			$array = array(
				OPCAO_ZERO => "Selecione uma resposta...",
				OPCAO_A => "A",
				OPCAO_B => "B",
				OPCAO_C => "C",
				OPCAO_D => "D",
				OPCAO_E => "E"
			);
			break;
		case CERTO_ERRADO :
			$array = array(
				OPCAO_ZERO => "Selecione uma resposta...",
				CERTO => "Certo",
				ERRADO => "Errado"
			);
			break;
		default :
			$array = array(
					OPCAO_ZERO => "Selecione uma resposta...",
					OPCAO_A => "A",
					OPCAO_B => "B",
					OPCAO_C => "C",
					OPCAO_D => "D",
					OPCAO_E => "E"
			);
	}
	return $array;
}

function get_tipo_questao_multiselect() {
	$array = array(
			MULTIPLA_ESCOLHA_5 => "Múltipla Escolha - 5 alternativas",
			MULTIPLA_ESCOLHA => "Múltipla Escolha - 4 alternativas", 
			CERTO_ERRADO => "Certo/Errado"
	);
	return $array;
}

function get_tipo_questao_combo() {
	$array = array (OPCAO_ZERO => "Selecione um tipo...");
	return array_replace($array, get_tipo_questao_multiselect());
}

function get_dificuldade_questao_multiselect() {
	$array = array(
			SEM_DIFICULDADE => "(Sem dificuldade)",
			MUITO_FACIL => "Muito fácil",
			FACIL => "Fácil",
			MEDIA => "Média",
			DIFICIL => "Difícil",
			MUITO_DIFICIL => "Muito difícil"
	);
	return $array;
}

function get_dificuldade_questao_combo() {
	$array = array (OPCAO_ZERO => "Selecione uma dificuldade...");
	return array_replace($array, get_dificuldade_questao_multiselect());
}

function get_status_questao_combo() {
	$array = array(
			NORMAL => "Normal",
			CANCELADA => "Anulada",
			DESATUALIZADA => "Desatualizada"
	);
	return $array;
}

function get_procedencia_questao_combo(){
	$array = array (OPCAO_ZERO => "Selecione uma procedência...");
	return array_replace($array, get_procedencia_questao_multiselect());
}

function get_procedencia_questao_multiselect(){
	$array = array(
		QUESTAO_INEDITA => "Inédita",
		QUESTAO_ADAPTADA => "Adaptada",
		QUESTAO_CONCURSO => "Concurso",
		QUESTAO_FIXACAO => "Exercício de Fixação"
	);
	return $array;
}

function get_tipo_erro_multiselect($tipos_erro) {
	$array = array ();
	foreach ($tipos_erro as $tipo_erro)
		$array[$tipo_erro['qet_id']] = $tipo_erro['qet_nome'];

	return $array;
}

function get_tipo_erro_combo($tipos_erro) {
	$array = array (OPCAO_ZERO => "Selecione uma notificação...");
	return array_replace($array, get_tipo_erro_multiselect($tipos_erro));
}

function get_professores_multiselect($professores) {
	$array = array();
	foreach ($professores as $professor)
		$array[$professor->ID] = $professor->display_name;
	
	return $array;
}

function get_professores_combo($professores) {
	$array = array (OPCAO_ZERO => "Selecione um professor...");
	return array_replace($array, get_professores_multiselect($professores));
}

function get_alunos_multiselect($alunos) {
	$array = array();
	foreach ($alunos as $aluno) {
		$array[$aluno['id']] = $aluno['nome_completo'] . " - " . $aluno['email'];
	}

	return $array;
}

function get_alunos_combo($alunos, $com_opcao_zero = TRUE) {
	
	$array = array();

	if($com_opcao_zero){
		$array[OPCAO_ZERO] = "Selecione um aluno...";
	}

	return array_replace($array, get_alunos_multiselect($alunos));
}

function get_usuarios_multiselect($usuarios) 
{
	$array = array();
	foreach ($usuarios as $usuario) {
		$array[$usuario['id']] = $usuario['nome_completo'];
	}
	
	return $array;
}

function get_usuarios_combo($usuarios) 
{
	$array = array (OPCAO_ZERO => "Selecione um usuário...");
	return array_replace($array, get_usuarios_multiselect($usuarios));
}

function get_area_atuacao_multiselect($areas) {
	$array = array ();
	$array[SEM_AREA_ATUACAO] = "(Sem área de atuação)";
	foreach ($areas as $area)
		$array[$area['ara_id']] = $area['ara_nome'];

	return $array;
}

function get_area_atuacao_combo($areas) {
	$array = array (OPCAO_ZERO => "Selecione um área de atuação...");
	return array_replace($array, get_area_atuacao_multiselect($areas));
}

function get_areas_atuacao_ids($areas_atuacao) {
	$array = array ();
	foreach ($areas_atuacao as $area_atuacao)
		$array[] = $area_atuacao['ara_id'];

	return $array;
}

function get_area_formacao_multiselect($areas) {
	$array = array ();
	$array[SEM_AREA_FORMACAO] = "(Sem área de formação)";
	foreach ($areas as $area)
		$array[$area['arf_id']] = $area['arf_nome'];

	return $array;
}

function get_area_formacao_combo($areas) {
	$array = array (OPCAO_ZERO => "Selecione um área de formacao...");
	return array_replace($array, get_area_formacao_multiselect($areas));
}

function get_areas_formacao_ids($areas_formacao) {
	$array = array ();
	foreach ($areas_formacao as $area_formacao)
		$array[] = $area_formacao['arf_id'];

	return $array;
}

function get_ano_multiselect($anos) {
	$array = array ();
	foreach ($anos as $ano)
		$array[$ano['pro_ano']] = $ano['pro_ano'];
	
	return $array;
}

function get_ano_combo($anos) {
	$array = array (OPCAO_ZERO => "Selecione um ano...");
	return array_replace($array, get_ano_multiselect($anos));
}

function get_simulado_multiselect($simulados, $todos = false, $todos_ativos = false, $todos_menos_inativos = false) {
	$array = array ();

	if($todos) {
		$array[TODOS_SIMULADOS] = 'Todos';
	}

	if($todos_ativos) {
		$array[TODOS_SIMULADOS_ATIVOS] = 'Todos ativos';
	}

	if($todos_menos_inativos) {
		$array[TODOS_SIMULADOS_MENOS_INATIVOS] = 'Todos menos inativos';
	}

	foreach ($simulados as $simulado)
		$array[$simulado['sim_id']] = $simulado['sim_nome'];

	return $array;
}

function get_simulado_combo($simulados) {
	$array = array (OPCAO_ZERO => "Selecione um simulado...");
	return array_replace($array, get_simulado_multiselect($simulados));
}

function get_status_multiselect() {
	$array = array(
			ATIVO => "Ativo",
			INATIVO => "Inativo",
			INCONSISTENTE => "Inconsistente"
	);
	return $array;
}

function get_status_combo() {
	$array = array (OPCAO_ZERO => "Selecione um status...");
	return array_replace($array, get_status_multiselect());
}

function get_status_questao_multiselect($status = NULL) {
	if($status)
	{
		$selecionadas = array();

		switch ($status) 
		{
			case NORMAL:
				$selecionadas["status"] = "Normal";
				break;
			case ANULADA:
				$selecionadas["status"] = "Anulada";
				break;
			case DESATUALIZADA:
				$selecionadas["status"] = "Desatualizada";
				break;
		}
	return $selecionadas;
	} 
	else
	{
		$array = array(
			NORMAL => "Normal",
			ANULADA => "Anulada",
			DESATUALIZADA => "Desatualizada"
		);
		return $array;
	}
}

function get_exclusoes_ids($exclusoes, $campo_exclusao) {
	$array = array ();
	foreach ($exclusoes as $exclusao)
		if($exclusao['sie_campo'] == $campo_exclusao)
			$array[] = $exclusao['sie_valor'];

	return $array;
}

function get_produto_multiselect($produtos, $destacar_nao_publicados = FALSE) {
	$array = array();
	foreach ($produtos as $produto) {

		if($destacar_nao_publicados) {
			$sufixo = ($produto->post_status == 'pending' || $produto->post_status == 'draft') ? " *" : " ";
		}

		$array[$produto->ID] = $produto->post_title . $sufixo;
	}

	return $array;
}

function get_produto_combo($produtos, $destacar_nao_publicados = FALSE) {
	$array = array (0 => "Selecione um produto...");
	return array_replace($array, get_produto_multiselect($produtos, $destacar_nao_publicados));
}

function get_ordenacao_multiselect() {
	$array = array(
			MAIS_NOVOS => "Ordenar por mais novos",
			MAIS_ANTIGOS => "Ordenar por mais antigos",
			MENOR_PRECO => "Ordenar por menor preço",
			MAIOR_PRECO => "Ordenar por maior preço",
			NOME_ASC => "Ordenar por nome"
	);
	return $array;
}

function get_ordenacao_combo() {
	$array = array (OPCAO_ZERO => "Selecione uma ordenação...");
	return array_replace($array, get_ordenacao_multiselect());
}

function get_ordenacao_simulado_multiselect() {
	$array = array(
			PRIORIDADE => "Prioridade",
			MAIS_NOVOS => "Mais novos",
			MAIS_ANTIGOS => "Mais antigos",
	);
	return $array;
}

function get_ordenacao_simulado_combo() {
	$array = array (OPCAO_ZERO => "Selecione uma ordenação...");
	return array_replace($array, get_ordenacao_simulado_multiselect());
}

function get_combo_numerico($ultimo)
{
	$array = array();

	for( $i = 0; $i <= $ultimo; $i++ ) {
		$array[$i] = $i;
	}
	
	return $array;
}

function get_caderno_combo($cadernos, $default_label = null, $todos = false) {
	$array = array ();
	
	if(!is_null($default_label)) {
		$array[0] = $default_label;
	}

	if($todos) {
		$array[TODOS_CADERNOS] = 'Todos';
	}	

	foreach ($cadernos as $caderno) {
		if(!tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR)) && $caderno['cad_comercializavel']) continue;

		$array[$caderno['cad_id']] = $caderno['cad_nome'];
	}

	// print_r($array);exit;

	return $array;
}

function get_categoria_multiselect($categorias) {
	$array = array ();
	foreach ($categorias as $categoria)
		$array[$categoria['cat_id']] = $categoria['cat_nome'];

	return $array;
}

function get_categoria_cp_multiselect($categorias) {
	$array = array ();
	foreach ($categorias as $categoria)
		$array[$categoria['ccp_id']] = $categoria['ccp_nome'];

	return $array;
}

function get_questoes_relacionadas_combo($questoes) {
	$array = array (0 => "");
	
	foreach ($questoes as $questao)
		$array[$questao['que_id']] = "id: " . $questao['que_id'];

	return $array;
}

function get_meses_combo() 
{
	$meses = array(
			1	=> "Janeiro",
			2	=> "Fevereiro",
			3	=> "Março",
			4	=> "Abril",
			5	=> "Maio",
			6	=> "Junho",
			7	=> "Julho",
			8	=> "Agosto",
			9	=> "Setembro",
			10  => "Outubro",
			11	=> "Novembro",
			12	=> "Dezembro"
	);
	
	$combo_mes_ano = array();
	
	for($i = date('Y'); $i >= 2014; $i--) {
		for($j = 12; $j >= 1; $j--) {
			if($i == date('Y') && $j > date('m')) continue;
	
			$combo_mes_ano[$j . '-' . $i] = $meses[$j] . ' ' . $i;
		}
	}
	
	return $combo_mes_ano;
}

function get_filtro_incluir_combo()
{
	return array(
		TODAS => "Todas",
		NAO_RESOLVIDAS => "Não Resolvidas",
		RESOLVIDAS => "Resolvidas",
		QUE_ACERTEI => "Que Acertei",
		QUE_ERREI => "Que Errei",
		//NOTIFICACAO_ERRO => "",
		FAVORITAS => "Favoritas"
	);
}

function get_filtro_comentarios_combo()
{
	return array(
		TODAS => "Todas",
		COM_COMENTARIOS_DE_PROFESSORES => "Professores",
		COM_COMENTARIOS_DE_ALUNOS => "Alunos",
		COM_COMENTARIOS => "Quaisquer",
		NENHUM_COMENTARIO => "Nenhum",
		MEUS_COMENTARIOS => "Meus",
		COM_COMENTARIOS_EM_VIDEO => "Vídeo"
	);
}

function get_filtro_resposta_combo()
{
	return array(
		TODAS => "Todas",
		COM_RESPOSTA => "Com resposta",
		SEM_RESPOSTA => "Sem resposta"
	);
}

function get_filtro_repetidas_combo(){
	return array(
		TODAS => "Todas",
		REPETIDAS => "Repetidas",
		NAO_REPETIDAS => "Não repetidas"
	);
}

function get_filtro_excluir_combo(){
	$result = array(
		DOS_MEUS_CADERNOS => "Dos meus cadernos",
		ANULADA => "Anuladas",
		DESATUALIZADA => "Desatualizada"
	);

	if(tem_acesso([ADMINISTRADOR, COORDENADOR_SQ])){
		$result[REPETIDAS] = 'Repetidas';
	}

	return $result;
}

function get_modalidades_combo($default_label = null) {
	$array = array ();
	
	if(!is_null($default_label)) {
		$array[0] = $default_label;
	}
	
	$array[1] = "Concursos";
	$array[2] = "OAB";
	$array[3] = "ENEM";
	$array[4] = "Militares";
	$array[5] = "Vestibular";

	return $array;
}

function get_situacoes_combo($default_label = null, $final = 'a') {
	$array = array ();
	
	if(!is_null($default_label)) {
		$array[0] = $default_label;
	}
	
	$array[1] = "Ativ" . $final;
	$array[2] = "Inativ" . $final;

	return $array;
}

function get_excluir_questoes_simulado($default_label = null) 
{
	$array = array ();
	
	if(!is_null($default_label)) {
		$array[0] = $default_label;
	}
	
	$array[1] = "Inativas";
	$array[2] = "Sem comentários de professor";
	$array[3] = "Usadas em outros simulados";

	return $array;
}

function get_grandezas_combo($default_label = NULL)
{
	$array = array ();
	
	if(!is_null($default_label)) {
		$array[0] = $default_label;
	}

	$array[CAMPO_DISCIPLINA] = 'Disciplina';
	$array[CAMPO_BANCA] = 'Banca';
	$array[CAMPO_ANO] = 'Ano';
	$array[CAMPO_ESCOLARIDADE] = 'Escolaridade';
	$array[CAMPO_AREA_ATUACAO] = 'Área de Atuação';
	$array[CAMPO_STATUS] = 'Status';
	$array[CAMPO_TIPO] = 'Tipo';
	$array[CAMPO_COMENTARIOS_PROFESSORES] = 'Comentário de Professores';
	$array[CAMPO_GABARITO] = 'Gabarito';
	$array[CAMPO_SITUACAO] = 'Ativo/Inativo';

	return $array;
}

function get_aprovacao_multiselect() 
{
    $array = [
        QUESTAO_APROVADA => "Aprovada",
        QUESTAO_EM_APROVACAO => "Pendente",
        QUESTAO_REPROVADA => "Rejeitada",
    ];
    
    return $array;
}

function get_procedencia_multiselect()
{
    $array = [
        QUESTAO_INEDITA => "Inédita",
        QUESTAO_ADAPTADA => "Adaptada",
        QUESTAO_CONCURSO => "Concurso",
        QUESTAO_FIXACAO => "Exercício de Fixação",
    ];
    
    return $array;
}

function get_meus_cadernos_ordenacao_multiselect() 
{
	$array = array(
			MEUS_CADERNOS_MAIS_RECENTE => "Data de criação mais recente",
			MEUS_CADERNOS_MENOS_RECENTE => "Data de criação menos recente",
			MEUS_CADERNOS_ACESSO_MAIS_RECENTE => "Último acesso mais recente",
			MEUS_CADERNOS_ACESSO_MENOS_RECENTE => "Último acesso menos recente",
			MEUS_CADERNOS_AZ => "A - Z",
			MEUS_CADERNOS_ZA => "Z - A",
			MEUS_CADERNOS_MAIS_ACERTOS => "Maior % de acertos",
			MEUS_CADERNOS_MENOS_ACERTOS => "Menor % de acertos",
			MEUS_CADERNOS_MAIS_ERROS => "Maior % de erros",
			MEUS_CADERNOS_MENOS_ERROS => "Menor % de erros"
	);
	
	return $array;
}

function get_meus_cadernos_ordenacao_combo() 
{
	$array = array (OPCAO_ZERO => "Selecione uma ordenação...");
	return array_replace($array, get_meus_cadernos_ordenacao_multiselect());
}

function to_select2_format( $array_multiselect )
{
	
	$options = array();

	foreach($array_multiselect as $key => $value)
	{

		$option = array();
		$option = ['id' => $key, 'text' => $value];
		array_push($options, $option);
	}

	return $options;
}