<?php
function grafico_questoes_respondidas_por_banca($qr_bancas)
{
	$dados = [];

	foreach ($qr_bancas as $item) {
		array_push($dados, [$item['ban_nome'], $item['total']]);
	}

	return json_encode($dados);
}

function grafico_questoes_respondidas_por_disciplina($qr_disciplinas)
{
	$dados = [];

	foreach ($qr_disciplinas as $item) {
		array_push($dados, [$item['dis_nome'], $item['total']]);
	}

	return json_encode($dados);
}

function grafico_acertos_questoes_por_banca($qr_bancas, $limit = null)
{
	$totais = ["Respostas"];
	$acertos = ["Acertos"];
	$x = ['x'];

	$i = 0;
	foreach ($qr_bancas as $item) {
		array_push($totais, $item['total']);
		array_push($acertos, $item['acertos']);
		array_push($x, $item['ban_nome']);

		$i++;

		if($limit && $i >= $limit) {
			break;
		}
	}

	return json_encode([$x, $totais, $acertos]);
}

function grafico_desempenho($desempenhos, $limit = null, &$aproveitamento = null)
{
	
	if($desempenhos) {

		$x = ['x'];
		$desempenho = ['Desempenho'];
		$performance = array();
		$i = 0;
		foreach($desempenhos as $item){

			array_push($x, $item['nome']);
			array_push($desempenho, $item['performance']);
			
			if($aproveitamento){
				array_push($performance, $item['performance']."% (".$item['nota']."/".$item['total_questoes'].")");
			}

			$i++;

			if($limit && $i >= $limit) {
				break;
			}
		}

		if($aproveitamento){
			$aproveitamento = json_encode([$performance]);
		}

		return json_encode([$x, $desempenho]);
	}

	return "";
}

function grafico_questao_resposta($qr_acertos, $qr_erros){

	$dados = [];
	$acertos = "Acertaram (".$qr_acertos.")";
	$erros = "Erraram (".$qr_erros.")";

	array_push($dados, [$acertos, $qr_acertos]);
	array_push($dados, [$erros, $qr_erros]);

	return json_encode($dados);
}

function grafico_questao_alternativas($qr_alternativas, $tipo = MULTIPLA_ESCOLHA){

	$dados = [];
	$pre = ($tipo != CERTO_ERRADO?"Letra ":"");
	foreach($qr_alternativas as $item){
		$letra = get_letra_opcao_questao($item['item'], $tipo);
		if($pre){
			$letra = strtoupper($letra);
		}
		array_push($dados, [$pre.$letra." (".$item['total'].")", $item['total']]);
	}

	return json_encode($dados);
}

?>