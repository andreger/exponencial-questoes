<?php
function is_index()
{
	$CI =& get_instance();
	return !$CI->uri->segment(1);
}

function is_ambiente_desenvolvimento()
{
	return $_SERVER['HTTP_HOST'] == 'exponencial' ? true : false;
}

function is_area_administrativa()
{
	$CI =& get_instance();
	return get_controller() == 'admin';
}

function get_controller()
{
	$CI =& get_instance();
	return $CI->uri->segment(1);
}

function get_action()
{
	$CI =& get_instance();
	return $CI->uri->segment(2);
}

function existe_arquivo($caminho_relativo)
{
	return file_exists($_SERVER['DOCUMENT_ROOT'] . '/questoes/' . $caminho_relativo);
}

function get_javascript_da_pagina()
{
	$arquivo = 'assets-admin/js/pagina/' . get_controller() . '/' . get_action() . '.js';
	if(existe_arquivo($arquivo)) {
		$url = base_url($arquivo);
		return "<script src='{$url}'></script>";
	}

	return "";
}

function javascript_da_pagina(){
	echo get_javascript_da_pagina();
}

function set_mensagem_flash($tipo, $mensagem)
{
	$_SESSION['mflash'] = array(
			'tipo' => $tipo,
			'mensagem' => $mensagem
	);
}

function get_mensagem_flash()
{
	$flash = $_SESSION['mflash'];

	if($flash['tipo'] == SUCESSO){
		$tipo = ALERTA_SUCESSO;
	}elseif($flash['tipo'] == ERRO){
		$tipo = ALERTA_ERRO;
	}elseif($flash['tipo'] == AVISO){
		$tipo = ALERTA_ATENCAO;
	}

	$caixa = ui_get_alerta($flash['mensagem'], $tipo);
	$_SESSION['mflash'] = null;
	
	return $caixa;
}

/**
 * Inicia o controle do profiler do CodeIniter. 
 * Se houver um cookie KDEBUG_ENABLE exibe o profiler do codeigniter
 */
function init_profiler()
{
    if($_COOKIE['KDEBUG_ENABLE']) {
        $CI =& get_instance();
		$CI->output->enable_profiler(true);
		if($_COOKIE['KDEBUG_ENABLE'] == 2){
			ini_set('display_errors', 1);
			ini_set('display_startup_errors', 1);
			error_reporting(E_ALL);
		}
    } 
}
