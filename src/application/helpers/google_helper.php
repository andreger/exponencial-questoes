<?php
function get_shorten_url($url)
{
	$api_url = "https://www.googleapis.com/urlshortener/v1/url?key=" . GOOGLE_SHORTEN_URL_KEY;
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);
	curl_setopt($ch, CURLOPT_URL, $api_url);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("longUrl"=>$url)));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
	
	$result = curl_exec($ch);	
	curl_close($ch);
	
	$result = json_decode($result);
	return $result->id;
}