<?php
function get_questoes_caderno_tooltip($caderno)
{
	return "Acertos: {$caderno['questoes_certas']}, Erros: {$caderno['questoes_erradas']}, Não resolvidas: {$caderno['total_questoes_nao_respondidas']}";
}

function pode_editar_caderno($caderno)
{
	if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) return TRUE;

	if(is_compartilhado($caderno)) return FALSE;

	if(is_caderno_comprado($caderno['cad_id'])) return FALSE;

	if(is_comercializavel($caderno))
	{
		return tem_acesso([PROFESSOR]);
	}

	if(is_caderno_coaching($caderno))
	{
		return tem_acesso([PARCEIRO_COACHING]);
	}

	
	if(! is_null($caderno['usu_designador_id']) ){
		if(is_aluno()){
			return FALSE;
		}
		else{
			return FALSE;
		}
	}
	else{
		return TRUE;
	}
}

function pode_excluir_caderno($caderno)
{
	if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) return TRUE;

	if(is_caderno_comprado($caderno['cad_id'])) return FALSE;

	if(is_comercializavel($caderno))
	{
		return tem_acesso([PROFESSOR]);
	}

	if(is_caderno_coaching($caderno))
	{
		if(is_compartilhado($caderno))
		{
			return TRUE;
		}
		else
		{
			return tem_acesso([PARCEIRO_COACHING]);
		}
	}

	return TRUE;
}


function pode_administrar_caderno_designado($caderno)
{
	if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) return TRUE;

	$CI = &get_instance();
	$CI->load->model('caderno_model');
	
	if(is_caderno_comprado($caderno['cad_id'])) return FALSE;

	if(is_comercializavel($caderno)) return FALSE;

	if(is_caderno_coaching($caderno)) return !$caderno['cad_compartilhado'];
	
	if(! is_null($caderno['usu_designador_id']) ){
		if(is_aluno()){
			return FALSE;
		}
		else{
			return FALSE;
		}
	}
	elseif(is_usuario_logado()){
		return TRUE;
	}else{
		return FALSE;
	}
}

function pode_categorizar_caderno($caderno)
{
	if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) return TRUE;

	$CI = &get_instance();
	$CI->load->model('caderno_model');
	
	if(is_comercializavel($caderno))
	{
		return tem_acesso([PROFESSOR]);
	}

	return TRUE;
}

function pode_designar_caderno($caderno)
{
	if(is_compartilhado($caderno)) return FALSE;

	if(is_comercializavel($caderno) && !tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR))) return FALSE;

	if(!is_caderno_comprado($caderno['cad_id']) && tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, CONSULTOR, PROFESSOR, REVISOR))) {
		return TRUE;
	}

	return FALSE;
}

function pode_compartilhar_caderno($caderno)
{
	if($caderno['cad_comprado'] && !tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) return FALSE;

	if($caderno['cad_coaching'] && $caderno['cad_compartilhado'])
	{
		return FALSE;
	}

	if($caderno['cad_coaching'] && !tem_acesso([PARCEIRO_COACHING]))
	{
		return FALSE;
	}

	return TRUE;
}

function foi_designado_por_administrador($caderno)
{
	$CI = &get_instance();
	$CI->load->model('caderno_model');
	
	if(! is_null($caderno['usu_designador_id']) ){
		if(user_can($caderno['usu_designador_id'], 'administrator')){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	else{
		return FALSE;
	}
}

function foi_designado($caderno)
{
	$CI = &get_instance();
	$CI->load->model('caderno_model');

	return !is_null($caderno['usu_designador_id']) ? true : false;
}

function is_comercializavel($caderno)
{
	$CI = &get_instance();
	$CI->load->model('caderno_model');

	return $caderno['cad_comercializavel'] ? true : false;
}

function is_caderno_coaching($caderno)
{
	$CI = &get_instance();
	$CI->load->model('caderno_model');

	return $caderno['cad_coaching'] ? true : false;
}

function encode_id_compartilhado($caderno_id)
{
	return base_convert($caderno_id * 34500, 10, 36);
}

function decode_id_compartilhado($id)
{
	return base_convert($id, 36, 10 ) / 34500;
}

function is_compartilhado($caderno)
{
	return $caderno['cad_compartilhado'] ? true : false;
}

/**
 * Verifica se o usuário é dono do caderno
 * 
 * @param int $caderno_id Id do caderno
 * @param int $usuario_id Id do usuário. Se null, utiliza usuário corrente
 * 
 * @return boolean
 */

function is_dono_do_caderno($caderno_id, $usuario_id = null)
{
	$CI = &get_instance();
	$CI->load->model('caderno_model');
	
	if(is_null($usuario_id)) {
	    $usuario_id = get_current_user_id();
	}

	$caderno = $CI->caderno_model->get_caderno($caderno_id);
	
	return $usuario_id == $caderno['usu_id'];
}

function is_caderno_comprado($caderno_id)
{
	$CI = &get_instance();
	$CI->load->model('caderno_model');

	$caderno = $CI->caderno_model->get_caderno($caderno_id);
	
	return $caderno['cad_comprado'];
}

function formatar_filtro_model(&$filtros, $valores, $config, $chave)
{
	$nome = $config[0] ? $config[0] : null;
	$model = $config[1] ? $config[1] : null;
	$campo = $config[2] ? $config[2] : null;	

	if($chave == "palavra_chave") {
		
		$filtros[$nome] = $valores;
		return;

	}

	elseif($chave == "pro_anos") {
		
		$itens = $valores;

	}

	elseif(in_array($chave, array("esc_ids", "que_statuses", "que_tipos", "que_dificuldades", "professores_ids"))) {

		$itens = array();
		foreach ($valores as $valor) {
			array_push($itens, $model[$valor]);
		}

	}

	elseif(in_array($chave, array("filtro_incluir", "filtro_comentarios", "filtro_resposta", "filtro_repetidas"))) {

		$filtros[$nome] = $model[$valores];

	}

	elseif($chave == "filtro_questoes") {
		$itens = $valores;
	}

	elseif($chave == "neg_cad_ids") {
		$CI = &get_instance();
		$CI->load->model('caderno_model');

		$model = get_caderno_combo($CI->caderno_model->listar_cadernos(get_current_user_id()));

		$itens = array();

		foreach ($valores as $valor) {
			array_push($itens, $model[$valor]);
		}
	}

	else {
		
		$CI = &get_instance();
		$CI->load->model($model);	

		$itens = array();
		foreach ($valores as $valor) {
			$item = $CI->$model->get_by_id($valor);
			array_push($itens, $item[$campo]);
		}
	}

	if($itens) {
		$filtros[$nome] = implode(' | ', $itens);
	}
}


function formatar_filtro_caderno($input) 
{
	if(!$input) return null;

	$filtros = array();
	$input_a = unserialize($input);

	// return $input_a;

	$campos = array(
			'palavra_chave' => array('Palavra Chave'),
			'dis_ids' => array('Disciplinas', 'disciplina_model', 'dis_nome'),
			'ass_ids' => array('Assuntos', 'assunto_model', 'ass_nome'),
			'ban_ids' => array('Bancas', 'banca_model', 'ban_nome'),
			'org_ids' => array('Órgãos', 'orgao_model', 'org_nome'),
			'car_ids' => array('Cargos', 'cargo_model', 'car_nome'),
			'pro_anos' => array('Anos'),
			'esc_ids' => array('Níveis Escolares', get_escolaridade_multiselect()),
			'arf_ids' => array('Áreas de Formação', 'area_formacao_model', 'arf_nome'),
			'ara_ids' => array('Áreas de Atuação', 'area_atuacao_model', 'ara_nome'),
			'que_statuses' => array('Status', get_status_questao_combo()),
			'que_tipos' => array('Tipo de Questão', get_tipo_questao_combo()),
			'que_dificuldades' => array('Dificuldades', get_dificuldade_questao_multiselect()),
			'professores_ids' => array('Comentários dos Professores', get_professores_combo_options("Todos os professores", 0, FALSE)),
			'filtro_incluir' => array('Incluir questões', get_filtro_incluir_combo()),
			'filtro_comentarios' => array('Comentários', get_filtro_comentarios_combo()),
			'filtro_resposta' => array('Respostas', get_filtro_resposta_combo()),
			'filtro_repetidas' => array('Incluir questões', get_filtro_repetidas_combo()),
			'filtro_questoes' => array('Questões adicionadas'),
			'neg_cad_ids' => array('Não presente em cadernos')
		);

	foreach ($input_a as $chave => $valores) {
		foreach ($campos as $campo => $config) {
			if($chave == $campo) {
				formatar_filtro_model($filtros, $valores, $config, $chave);
			}
		}

	}
	
	return $filtros;	
}

function get_caderno_icone($caderno)
{
	if(foi_designado_por_administrador($caderno)) {
		$src = get_tema_image_url('ico-caderno-designado.png');
		$tooltip = "Designado para você";
	}
	elseif(is_compartilhado($caderno)) {
		$src = get_tema_image_url('ico-caderno-compartilhado.png');	
		$tooltip = "Compartilhado com você";
	}
	elseif(is_caderno_comprado($caderno['cad_id'])) {
		$src = get_tema_image_url('ico-caderno-comprado.png');	
		$tooltip = "Comprado por você";	
	}
	else {
		$src = get_tema_image_url('ico-caderno-criado.png');
		$tooltip = "Criado por você";
	}

	return "<img src='{$src}'' alt='{$tooltip}' title='{$tooltip}'>";
}

function get_caderno_por_usuario_caderno_referencia($usuario_id, $caderno_ref_id){
	$CI = &get_instance();
	$CI->load->model('caderno_model');

	return $CI->caderno_model->get_caderno_por_usuario_caderno_referencia($usuario_id, $caderno_ref_id);
}

//Alterado porque não tratava a repetição de questões no assunto pai, apenas somava
/*function completar_indice(&$indice, $item) 
{
    $CI =& get_instance();
    $CI->load->model("assunto_model");
    
    if(array_key_exists($item["ass_id"], $indice)) {
        $indice[$item["ass_id"]]["qtde"] += $item["qtde"];
    }
    else {
        $indice[$item["ass_id"]] = $item;
    }
    
    if($item["ass_pai_id"]) {
        $novo_item = $CI->assunto_model->get_by_id($item["ass_pai_id"]);
        $novo_item["qtde"] = $item["qtde"];
        
        completar_indice($indice, $novo_item);
    }
}*/

function completar_indice(&$indice, $item, $caderno_id, $incluir_inativas)
{
	$CI =& get_instance();
	$CI->load->model("assunto_model");
	$CI->load->model("caderno_model");
    
    if(!array_key_exists($item["ass_id"], $indice)) {
		$indice[$item["ass_id"]] = $item;
	}
	//É necessário saber os filhos desse assunto para saber o total de questões dele
	$assuntos_filhos = $CI->assunto_model->listar_filhos($item['ass_id']);
	$ass_ids = array($item['ass_id']);
	foreach($assuntos_filhos as $ass_filho)
	{
		array_push($ass_ids, $ass_filho['ass_id']);
	}
	$indice[$item["ass_id"]]["qtde"] = $CI->caderno_model->contar_questoes_por_assuntos($caderno_id, $ass_ids, $incluir_inativas);
    
    if($item["ass_pai_id"]) {
        $novo_item = $CI->assunto_model->get_by_id($item["ass_pai_id"]);
        //$novo_item["qtde"] = $item["qtde"];
        
        completar_indice($indice, $novo_item, $caderno_id, $incluir_inativas);
    }
}

function get_arvore_indice($indice, $ass_pai_id, $dis_id)
{
    $arvore = [];
    foreach ($indice as $item) {
        
        if($dis_id == $item["dis_id"] && $ass_pai_id == $item["ass_pai_id"]) {
            
            $arvore_item = $item;
            
            $arvore_item["filhos"] = get_arvore_indice($indice, $arvore_item["ass_id"], $dis_id);
            
            $arvore[] = $arvore_item;
        }
 
    }
    
    return $arvore;
}

function get_arvore_indice_html($arvore, $pai_id, $total, $dis_id, $possui_filtro, $assuntos_selecionados = null)
{
    $html = "";
    
    foreach ($arvore as $item) {
        $id = "ass_" . $item['ass_id'];
        $percentual = valor_percentual($item["qtde"], $total);
        
        $html .= 
        "<tr data-tt-id='{$id}' data-tt-parent-id='{$pai_id}'>";
		
		if($possui_filtro){
			//$html .= "<td><a href='#' data-id='".$item['ass_id']."' data-dis_id='{$dis_id}' class='indice-assunto'>{$item['ass_nome']}</a></td>";
			$html .= "<td>" . form_checkbox('ass_ids', $item['ass_id'], in_array($item['ass_id'], $assuntos_selecionados?:array()), "class='indice-assunto' data-dis_id='{$dis_id}' data-id='{$item['ass_id']}' data-pai_id='{$item['ass_pai_id']}'") . "{$item['ass_nome']}</td>";
		}else{
			$html .= "<td>{$item['ass_nome']}</td>";
		}

		$html .= "<td class='text-right' nowrap>{$item["qtde"]} ({$percentual})</td>
        	</tr>";
        
        if($item["filhos"]) {
            $html .= get_arvore_indice_html($item["filhos"], $id, $total, $dis_id, $possui_filtro, $assuntos_selecionados);
        }

    }
    
    return $html;
}



