<?php
function get_offset_ranking($simulado_id, $usuario_id, $somente_aprovados = 0)
{
	$CI = &get_instance();
	$CI->load->model('simulado_model');

	$ranking_usuario = $CI->simulado_model->get_posicao_ranking($simulado_id, $usuario_id, $somente_aprovados);

	return floor((($ranking_usuario['posicao'] + 1) / LIMIT_RANKING)) * LIMIT_RANKING;
}

function is_simulado_usuario_aprovado($simulado_id, $usuario_id)
{
	$CI = &get_instance();
	$CI->load->model('simulado_model');

	$CI->simulado_model->is_aprovado($usuario_id, $simulado_id);
}

function is_simulado_gratuito($simulado_id)
{
	$CI = &get_instance();
	$CI->load->model('simulado_model');
	
	$simulado = $CI->simulado_model->get_by_id($simulado_id);
	$produto = get_produto_by_id($simulado['prd_id']);
	
	return is_produto_gratis($produto);
}

function is_revisao_ativada($simulado_id)
{
	$CI = &get_instance();
	$CI->load->model('simulado_model');
	
	$simulado = $CI->simulado_model->get_by_id($simulado_id);
	return $simulado['sim_revisao_ativada'] == 1 ? true : false;
}

function get_resultado_da_disciplina($resultados_array, $disciplina_id)
{
	if($resultados_array) {
		foreach ($resultados_array as $resultado) {
			if($resultado['dis_id'] == $disciplina_id) 
				return $resultado['sre_resultado'];
		}
	}
	return null;
}

function get_acertos_da_disciplina($resultados_array, $disciplina_id)
{
	if($resultados_array) {
		foreach ($resultados_array as $resultado) {
			if($resultado['dis_id'] == $disciplina_id) 
				return $resultado['sre_acertos'];
		}
	}
	return null;
}

function get_erros_da_disciplina($resultados_array, $disciplina_id, $peso_erros)
{
	if($resultados_array) {
		foreach ($resultados_array as $resultado) {
			if($resultado['dis_id'] == $disciplina_id) 
				return ($resultado['sre_acertos'] - $resultado['sre_resultado'])/$peso_erros;
		}
	}
	return null;
}

function get_resultado_da_questao($resultados_array, $questao_id, $resposta)
{
	if($resultados_array) {
		foreach ($resultados_array as $resultado) {
			if($resultado['que_id'] == $questao_id) { 
				return $resultado['srd_resultado'] == $resposta ? true : false;
			}
		}
	}
	return null;
}

function get_disciplinas_ordem_str($disciplinas)
{
	$disciplinas_ids = array();
	foreach ($disciplinas as $disciplina) {
		array_push($disciplinas_ids,  $disciplina['dis_id']);
	}
	
	return implode(';', $disciplinas_ids);
}

function tem_acesso_ao_simulado($simulado_id, $usuario_id)
{	
	if(tem_acesso([USUARIO_LOGADO]))
	{
		if(tem_acesso(array(ADMINISTRADOR, PROFESSOR, CONSULTOR, REVISOR))) return true;
		
		$CI = &get_instance();
		$CI->load->model('simulado_model');
		return $CI->simulado_model->comprou_simulado($simulado_id, $usuario_id) || $CI->simulado_model->is_aluno_designado($simulado_id, $usuario_id);
	}
}

function tem_acesso_aos_comentarios_do_simulado($simulado_id, $usuario_id)
{
	if(tem_acesso(array(ADMINISTRADOR, PROFESSOR, CONSULTOR, REVISOR, ASSINANTE_SQ))) return true;
	
	$CI = &get_instance();
	$CI->load->model('simulado_model');

	return $CI->simulado_model->comprou_simulado($simulado_id, $usuario_id, true) || $CI->simulado_model->is_aluno_designado($simulado_id, $usuario_id);
}

function get_questao_ordem($questoes, $questao_id) 
{
	for ($i = 0; $i < count($questoes); $i++) {
		if($questao_id == $questoes[$i]['que_id']) {
			return $i+1;
		}
	}
	return null;
}

function get_respostas_simulado_array($respostas)
{
	$vetor = array();
	
	foreach ($respostas as $item) {
		$vetor[$item['que_id']] = ($item['qre_selecao'] == $item['que_resposta']) ? SIM : NAO;	
	}
		
	return $vetor;
}

function get_resultado_simulado_array($disciplinas, $resultados)
{
	$resultado_array = array();
	foreach ($resultados as $resultado) {
		$resultado_array[$resultado['dis_id']] = [
			'nota' => $resultado['sre_resultado'],
			'acertos' => $resultado['acertos']
		];
	}
	
	$vetor = array();
	
	foreach ($disciplinas as $disciplina) {
		$item = array(
			'nome' => $disciplina['dis_nome'],
			'qtde' => $disciplina['qtde'],
			'acertos' => $resultado_array[$disciplina['dis_id']]['acertos'],
			'nota' => $resultado_array[$disciplina['dis_id']]['nota'],
		);
		
		array_push($vetor, $item);
	}
	return $vetor;
}

function get_simulados_onde_questao_aparece_str($questao_id, $excluir_simulado_id)
{
	$ci = &get_instance();
	$ci->load->model('questao_model');
	
	$simulados = $ci->questao_model->listar_simulados_onde_questao_aparece($questao_id, $excluir_simulado_id);
	
	$simulados_array = array();
	foreach ($simulados as $simulado) {
		if($simulado['sim_status'] == ATIVO) {
			array_push($simulados_array, "<span class='texto-vermelho'>{$simulado['sim_nome']}</span>");
		}
		else {
			array_push($simulados_array, $simulado['sim_nome']);
		}
	}
	
	if($simulados_array) {
		return "<b>Simulados com esta questão:</b> " . implode($simulados_array, ', ');
	}
	return "";
}

function pode_revisar_simulado($simulado_id)
{
	if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) return true;
	
	$ci = &get_instance();
	$ci->load->model('simulado_model');
	
	return $ci->simulado_model->pode_revisar_simulado($simulado_id, get_current_user_id());
}

function pode_revisar_disciplina_simulado($simulado_id, $disciplina_id)
{
	if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) return true;
	
	$ci = &get_instance();
	$ci->load->model('simulado_model');
	
	return $ci->simulado_model->pode_revisar_disciplina_simulado($simulado_id, $disciplina_id, get_current_user_id());
}

function get_listar_simulado_class($simulado)
{
	return "simulado status-{$simulado['sim_status']} banca-{$simulado['ban_id']} orgao-{$simulado['org_id']}";
}

function is_simulado_resolvido($simulado_id, $usuario_id)
{
	$ci = &get_instance();
	$ci->load->model('simulado_model');
	$resultado = $ci->simulado_model->get_simulado_usuario($simulado_id, $usuario_id);

	if($resultado['sim_usu_data_resolucao'] == null ) {
		return false;

	}

	return true;

}

function get_nome_etapa_edicao_simulado($passo){
	if($passo == 1){
		return "Dados do Simulado";
	}
	if($passo == 2){
		return "Filtros de Questões";
	}
	if($passo == 3){
		return "Disciplinas";
	}
	if($passo == 4){
		return "Assuntos";
	}
	if($passo == 5){
		return "Revisores";
	}
	if($passo == 6){
		return "Produto Exponencial";
	}
	return null;
}

/**
 * Recupera a URL do edital. Se não tiver protocolo adiciona https://
 * 
 * @param mixed $simulado Simulado
 * 
 * @return string
 */

function get_url_edital_simulado($simulado) {
    if($url = $simulado['sim_url_edital']) {
        
        $url_a = parse_url($url);
        if($url_a["scheme"]) {
            return $url;
        }
        else {
            return "https://" . $url;
        }
        
    }
    else {
        return "";
    }
}