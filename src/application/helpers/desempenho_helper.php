<?php

/**
 * Returna o estilo de um item de submenu de Meu Desempenho
 * 
 * @param string $menu_item 
 * @param string $menu_ativo
 * 
 * @return string Estilo do menu
 */

function get_desempenho_ativo_estilo($menu_item, $menu_ativo) 
{
	return $menu_item == $menu_ativo ? "desempenho-submenu-ativo" : "";
}

function get_desempenho_icone($valor) 
{   
    $icone_num = 4;
    
    if($valor >= 85.0) {
        $icone_num = 1;
    }
    elseif($valor >= 70.0) {
        $icone_num = 2;
    }
    elseif($valor >= 50.0) {
        $icone_num = 3;
    }
    
    $img_url =  get_tema_image_url("icones-desempenho{$icone_num}.png");
    
    return "<img src='{$img_url}' style='margin: 0 5px; height: 14px; width: 14px; vertical-align: initial;'>";
}