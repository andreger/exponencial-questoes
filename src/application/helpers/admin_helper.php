<?php
function upload_pdf($nome, $local, $post_name, $object) {
	
	$config ['upload_path'] = './uploads/'. $local .'/';
	$config ['allowed_types'] = 'pdf';
	$config ['file_name'] = $nome;
	$config ['overwrite'] = TRUE;
		
	$object->upload->initialize ( $config );
	if (! $object->upload->do_upload ( $post_name )) {
		$object->session->set_flashdata ($post_name, 'Problema ao enviar PDF!' );
		log_sq("ERRO", "Problema ao enviar PDF: " . $object->upload->display_errors('', '') );
		return false;
	}
	
	return true;
}

function upload_img($nome, $local, $post_name, $object) {
	$return = excluir_arquivo($nome, $local);
	$config ['upload_path'] = "./uploads/{$local}/";
	$config ['allowed_types'] = "gif|jpg|png";
	$config ['file_name'] = $nome;
	$config ['overwrite'] = TRUE;

	$object->upload->initialize ( $config );
	if (! $object->upload->do_upload ( $post_name )) {
		$object->session->set_flashdata ($post_name, 'Problema ao enviar imagem!' );
		log_sq("ERRO", "Problema ao enviar imagem: " . $object->upload->display_errors('', '') );
		return false;
	}
	return true;
}

function excluir_arquivo($nome, $local) {
	
	$files = glob("uploads/{$local}/{$nome}.*");
	if(count($files) > 0)
		foreach($files as $file)
			if(is_file($file))
				$return = unlink($file);
	return $return;
}

function get_menu_ativo_inativo($atual)
{
	$action = get_action();

	if(is_array($atual)) {
		foreach ($atual as $item) {
			if($item == $action) return 'active';
		}
	}
	return $atual == $action ? 'active' : '';
}

function menu_ativo_inativo($atual)
{
	echo get_menu_ativo_inativo($atual);
}