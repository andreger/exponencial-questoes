<?php
function imprimir_questoes_html($questoes, $simulado = null, $cargo = null, $caderno = null, $rotulos = false) {
	$tamanho_fonte = get_tamanho_fonte_style();

	$usuario = get_usuario_array(get_current_user_id());

	$conf_usuario = get_config_aluno();
	$ordem_cabecalho = explode(",", $conf_usuario['ordem_cabecalho']);

	if($simulado) {
		$tipo = "Simulado";
		$titulo = $simulado['sim_nome'];
		$texto_cabecalho = "Foco: {$cargo['car_nome']}";
	}
	elseif($caderno) {
		$tipo = "Caderno";
		$titulo = $caderno['cad_nome'];
		$texto_cabecalho = "";
	}
	else {
		$tipo = "";
		$titulo = "";
		$texto_cabecalho = "";
	}
	
//	$texto_introdutorio = count($questoes) == LIMIT_IMPRIMIR_QUESTOES ? "Impressão limitada a ". LIMIT_IMPRIMIR_QUESTOES . " questões " : "";

	$disciplina = "Vazio";
	$anterior = "";
	$nao_disponivel = "Disciplina Não Definida.";
	
	/************************************************************
	 * Capa
	 ************************************************************/
	
	$html =
		"<div style='text-align:center;'>
			<div style='margin:150px 0 50px'><img width='250' src='/questoes/assets-admin/img/capa.jpg'></div>
			<div style='margin:30px 0; color: #0078AB; font-size: 100px; '><b>{$tipo}</b></div>
			<div style='margin:60px 0;color: #0078AB; font-size: 30px;'><b>{$titulo}</b></div>
			<div style='margin:0'><img src='/wp-content/themes/academy/images/logo.png'></div>
		</div>";

//	if($texto_introdutorio) {
//		$html .=
//		"<div style='line-heigth: 14px; margin: 40px; color: #0078AB; font-size: 14px; text-align:center;'>{$texto_introdutorio}</div>";
//	}
	$html .= QUEBRA_IMPRESSAO;

	/************************************************************
	 * Contracapa
	 ************************************************************/
	$html .= get_contracapa($simulado);
	
	/************************************************************
	 * Questões
	 ************************************************************/
	$html .=	"<div class='pdf-content'>
				<div class='pdf-body'>";
	
	foreach ($questoes as $key => $questao) {

		$questao_html = "";

		if($rotulos) {
			$nome_questao = trim($questao['sro_nome']);
		}
		else {
			$nome_questao = trim($questao['dis_nome']);
		}

		$anterior = $disciplina;
		if (!strcmp($disciplina,$nome_questao) == 0)
			if(is_null($nome_questao) || strcmp("",$nome_questao) == 0)
				$disciplina = $nao_disponivel;
			else
				$disciplina = $nome_questao;

			if($simulado && !strcmp($disciplina,$anterior) == 0 )
				$questao_html .= '</br><div class="disciplina-barra">'.$disciplina.'</div>';

			$numero_questao = $key +1;

			$assuntos_str = get_questao_assuntos_str($questao);
			
			if($questao['anos']) {
				$barra_anos = "<b>Ano:</b> " . implode(", ", $questao['anos']);
			}
			if($questao['bancas']) 	{
				$barra_bancas = "<b>Bancas:</b> " . implode(", ", $questao['bancas']);
			}

			if($questao['orgaos']) {
				$barra_orgaos = "<b>Instituições:</b> " . implode(", ", $questao['orgaos']);
			}

			if($questao['provas']){
				$barra_provas = "<b>Provas:</b> " . implode(", ", $questao['provas']);
			}

			$barra_id = "<b>Id:</b> " . $questao['que_id'];

			$barra_disciplina = "<b>Disciplina:</b> ".$disciplina;

			$barra_assuntos = "<b>Assuntos:</b> " .$assuntos_str; 

			$barra_questoes = "";

			foreach($ordem_cabecalho as $opcao) {
				switch ($opcao) {

					case "ID":
						$barra_questoes .= "{$barra_id} ";
						break;

					case "Ano":
						$barra_questoes .= "{$barra_anos} ";
						break;

					case "Bancas":
						$barra_questoes .= "{$barra_bancas} ";
						break;

					case "Órgãos":
						$barra_questoes .= "{$barra_orgaos} ";
						break;

					case "Provas":
						$barra_questoes .= "{$barra_provas} ";
						break;

					case "Disciplina":
						$barra_questoes .= "{$barra_disciplina} ";
						break;

					case "Assuntos":
						$barra_questoes .= "{$barra_assuntos} ";
						break;
				}
			}

			if($caderno && $conf_usuario['exibir_url_imprimir_caderno'] == 1) {				
				$questao_html .=
					"<div style='font-family:verdana;font-size:11pt; margin:20px 0;' class='questao'>
					<div></div>				
					<div style='margin-top:10px'>{$numero_questao}. {$barra_questoes}</div>
					<div style='margin-top:10px;{$tamanho_fonte}'>{$questao['que_texto_base']}</div>
					<div style='{$tamanho_fonte}'>{$questao['que_enunciado']}</div>";
			}elseif($caderno && $conf_usuario['exibir_url_imprimir_caderno'] == 0) {			
				$questao_html .=
					"<div style='font-family:verdana;font-size:11pt; margin:20px 0;' class='questao'><a href=\"{$link}\" target=\"_blank\">{$link}</a>
					<div style='margin-top:10px'>{$numero_questao}. {$barra_questoes}</div>
					<div style='margin-top:10px;{$tamanho_fonte}'>{$questao['que_texto_base']}</div>
					<div style='{$tamanho_fonte}'>{$questao['que_enunciado']}</div>";
			}
			elseif($simulado) {
				$questao_html .=
					"</br> <div>{$numero_questao} - {$barra_id} {$barra_disciplina} {$barra_assuntos}</div>";
				
				
				if(!is_null($questao['que_referencia_id'])) {
					$questao_num = get_questao_ordem($questoes, $questao['que_referencia_id']);
					
					if(!is_null($questao_num)) {
						$questao_html .= "<div style='margin:10px 0;{$tamanho_fonte}'>Utilize o texto da questão {$questao_num} para responder o item abaixo</div>";
					}
					else {
						$questao_html .= "<div style='margin-top:10px;{$tamanho_fonte}'>{$questao['que_texto_base']}</div>";
					}
				}
				else {
					$questao_html .= "<div style='margin-top:10px;{$tamanho_fonte}'>{$questao['que_texto_base']}</div>";
				}
				
				$questao_html .= "<div style='{$tamanho_fonte}'>{$questao['que_enunciado']}</div>";
			}
			else {				
				$questao_html .=
				"<div style='font-family:verdana;font-size:11pt; margin:20px 0;' class='questao'>
				<div>{$numero_questao}.  {$barra_questoes}</div>
				<div style='margin-top:10px;{$tamanho_fonte}'>{$questao['que_texto_base']}</div>
				<div style='{$tamanho_fonte}'>{$questao['que_enunciado']}</div>";
			}
			
			if($questao['que_tipo'] == MULTIPLA_ESCOLHA || $questao['que_tipo'] == MULTIPLA_ESCOLHA_5) {
				$opcoes = array(1 => 'a)', 2 => 'b)', 3 => 'c)', 4=> 'd)', 5 => 'e)');
				foreach ($questao['opcoes'] as $opcao) {
					if(trim($opcao['qop_texto']) != "" && trim($opcao['qop_texto']) != "<p><br></p>") {
						$questao_html .= "<div style='{$tamanho_fonte}'>{$opcoes[$opcao['qop_ordem']]}&nbsp;{$opcao['qop_texto']}</div>";
					}
				}

			} elseif($questao['que_tipo'] == CERTO_ERRADO) {
				$questao_html .= "<div style='{$tamanho_fonte}'>( ) Certo</div><div>( ) Errado</div>";
			}
			$questao_html .= "</div>";
			$html .= $questao_html;
			
			/**
			 * [JOULE][Acerto]- Remover texto padrão para impressão de cadernos
			 *
			 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2065
			 */

			$imprimir_msg_copia_registrada = true;

			if($caderno && tem_acesso(array(ADMINISTRADOR, COORDENADOR, PROFESSOR))) {
				$imprimir_msg_copia_registrada = false;
			}

			if($imprimir_msg_copia_registrada && ($numero_questao % 5 == 0)) {
				$html .= 
					"<div style='text-align:center; margin-top: 20px;color: #999'>Direitos autorais reservados (Lei 9610/98). Proibida a reprodução, venda ou compartilhamento deste arquivo. Uso individual<br>
					Cópia registrada para {$usuario['nome_completo']} (CPF:{$usuario['cpf']})</div>";
			}
			
	}
	$html .= "</div>
			</div>";

	return stripcslashes($html);
}

function get_contracapa($simulado) {
	// se não for simulado não gera contracapa
	if(is_null($simulado)) return "";
	
	$contracapa = str_replace("<!--quebra-->", QUEBRA_IMPRESSAO, $simulado['sim_descricao']);
	$contracapa .= QUEBRA_IMPRESSAO;
	
	return $contracapa;
}
 
function processar_imagens_html($documentTemplate)
{
	$documentTemplate =  stripcslashes($documentTemplate);
	$html = str_get_html($documentTemplate);
	
	$elementos = $html->find('img');
	foreach ($elementos as $elemento) {
		$qcon_nome_imagem = str_replace('"', '', trim($elemento->src));
		$expo_absoluto = $_SERVER['DOCUMENT_ROOT'] . $qcon_nome_imagem;
		$blank = '/questoes/assets-admin/img/blank.png';
		
		if(file_exists($expo_absoluto)) {
			if(!getimagesize($expo_absoluto)) {
				$html = str_replace($elemento->src, $blank, $html);
			}
			else {
				$html = str_replace($elemento->src, trim($elemento->src), $html);
			}
		}
		else {
			$html = str_replace($elemento->src, $blank, $html);
		}
	}
	
	return $html;
}

function imprimir_resultado_simulado($questoes, $simulado, $respostas) {

	$usuario = get_usuario_array(get_current_user_id());

	$conf_usuario = get_config_aluno();
	$ordem_cabecalho = explode(",", $conf_usuario['ordem_cabecalho']);

	if($simulado) {
		$tipo = "Resultado de Simulado";
		$titulo = $simulado['sim_nome'];
		$texto_cabecalho = "Foco: {$cargo['car_nome']}";
	}

// 	$texto_introdutorio = count($questoes) == LIMIT_LISTAR_QUESTOES ? "Foram impressas somente as " . LIMIT_LISTAR_QUESTOES . " primeiras questões" : "";

	$disciplina = "Vazio";
	$anterior = "";
	$nao_disponivel = "Disciplina Não Definida.";

	/************************************************************
	 * Capa
	 ************************************************************/

	$html =
	"<div style='text-align:center;'>
	<div style='margin:150px 0 50px'><img width='250' src='/questoes/assets-admin/img/capa.jpg'></div>
	<div style='margin:30px 0; color: #0078AB; font-size: 40px; '><b>{$tipo}</b></div>
	<div style='margin:60px 0;color: #0078AB; font-size: 30px;'><b>{$titulo}</b></div>
	<div style='margin:0'><img src='/wp-content/themes/academy/images/logo.png'></div>
	</div>";

	$html .= QUEBRA_IMPRESSAO;

	/************************************************************
	 * Questões
	************************************************************/
	$html .=	"<div class='pdf-content'>
				<div class='pdf-body'>";

	foreach ($questoes as $key => $questao) {
		//echo "<pre>";
		//print_r($questao);
		$questao_html = "";

		$nome_questao = trim($questao['dis_nome']);
		$anterior = $disciplina;
		if (!strcmp($disciplina,$nome_questao) == 0)
			if(is_null($nome_questao) || strcmp("",$nome_questao) == 0)
				$disciplina = $nao_disponivel;
			else
				$disciplina = $nome_questao;

			if($simulado && !strcmp($disciplina,$anterior) == 0 )
				$questao_html .= '</br><div class="disciplina-barra">'.$disciplina.'</div>';

			$numero_questao = $key +1;
				
			$link = $questao['que_url_reduzida']; //get_questao_url($questao['que_id']);
			$assuntos_str = get_questao_assuntos_str($questao);
				
			$assunt = "";
			foreach ($questao['assuntos'] as $assunto) 
			{
				$assunt = $assunt . $assunto['ass_nome'];
			}

			$ano = "";
			foreach ($questao['anos'] as $an) 
			{
				$ano = $ano . $an;
			}

			$banca = "";
			foreach ($questao['bancas'] as $ban) 
			{
				$banca = $ban . $ban;
			}

			$orgao = "";
			foreach ($questao['orgaos'] as $org) 
			{
				$orgao = $orgao . $org;
			}

			$prova = "";
			foreach ($questao['provas'] as $pro) 
			{
				$prova = $prova . $pro;
			}

			$barra_id = "<b>Id:</b> " . $questao['que_id'];

			$barra_disciplina = "<b>Disciplina:</b> " . $disciplina;

			$barra_assuntos = "<b>Assuntos:</b> " . $assunt; 

			$barra_anos = "<b>Anos:</b> " . $ano;

			$barra_bancas = "<b>Bancas:</b> " . $banca;

			$barra_orgaos = "<b>Órgãos:</b> " . $orgao;

			$barra_provas = "<b>Provas:</b> " . $prova;

			$barra_questoes = "";

			foreach($ordem_cabecalho as $opcao) {
				switch ($opcao) {

					case "ID":
						$barra_questoes .= "{$barra_id} ";
						break;

					case "Ano":
						$barra_questoes .= "{$barra_anos} ";
						break;

					case "Bancas":
						$barra_questoes .= "{$barra_bancas} ";
						break;

					case "Órgãos":
						$barra_questoes .= "{$barra_orgaos} ";
						break;

					case "Provas":
						$barra_questoes .= "{$barra_provas} ";
						break;

					case "Disciplina":
						$barra_questoes .= "{$barra_disciplina} ";
						break;

					case "Assuntos":
						$barra_questoes .= "{$barra_assuntos} ";
						break;
				}
			}

			$questao_html .=
			"<div style='font-family:verdana;font-size:11pt; margin:20px 0;' class='questao'>
			<div>{$numero_questao}. {$barra_questoes}</div>";


			if(!is_null($questao['que_referencia_id'])) {
				$questao_num = get_questao_ordem($questoes, $questao['que_referencia_id']);
					
				if(!is_null($questao_num)) {
					$questao_html .= "<div style='margin:10px 0;{$tamanho_fonte}'>Utilize o texto da questão {$questao_num} para responder o item abaixo</div>";
				}
				else {
					$questao_html .= $questao_html .= "<div style='margin-top:10px;{$tamanho_fonte}'>{$questao['que_texto_base']}</div>";
				}
			}
			else {
				$questao_html .= "<div style='margin-top:10px;{$tamanho_fonte}'>{$questao['que_texto_base']}</div>";
			}

			$questao_html .= "<div style='{$tamanho_fonte}'>{$questao['que_enunciado']}</div>";
			
			if($questao['que_tipo'] == MULTIPLA_ESCOLHA || $questao['que_tipo'] == MULTIPLA_ESCOLHA_5) {
				$opcoes = array(1 => 'a)', 2 => 'b)', 3 => 'c)', 4=> 'd)', 5 => 'e)');
				$opcoes2 = array(1 => 'A', 2 => 'B', 3 => 'C', 4=> 'D', 5 => 'E');
				$opcoes3 = array(1 => 'Certo', 2 => 'Errado');
				
				foreach ($questao['opcoes'] as $opcao) {
// 					$resposta_style = $opcao['qop_ordem'] == $questao['que_resposta'] ? 'color: #18a689;font-weight: bold;' : '';
					
// 					if(!$resposta_style) {
// 						$resposta_style = $opcao['qop_ordem'] == $respostas[$questao['que_id']] ? 'color: #ff0000;font-weight: bold;' : '';
// 					}
					
					$questao_html .= "<div style='{$resposta_style},{$tamanho_fonte}'>{$opcoes[$opcao['qop_ordem']]}&nbsp;{$opcao['qop_texto']}</div>";
				}
			
				if($respostas[$questao['que_id']] == $questao['que_resposta']) {
					$questao_html .= "<div style='margin: 10px 0; color: #18a689;font-weight: bold;'>Você acertou a questão. Resposta: ". $opcoes2[$questao['que_resposta']] .".</div>";
					
				}
				else {
					$r = $respostas[$questao['que_id']] ? "Sua resposta foi <b>" . $opcoes2[$respostas[$questao['que_id']]] ."</b>." : "Você não respondeu esta questão.";
					$questao_html .= "<div style='margin: 10px 0; color: #ff0000;font-weight: bold;'>Você errou a questão. Resposta correta: " . $opcoes2[$questao['que_resposta']] . ".  " . $r ."</div>";
				}

			} elseif($questao['que_tipo'] == CERTO_ERRADO) {
				$questao_html .= "<div>( ) Certo</div><div>( ) Errado</div>";
				
				if($respostas[$questao['que_id']] == $questao['que_resposta']) {
					$questao_html .= "<div style='margin: 10px 0; color: #18a689;font-weight: bold;'>Você acertou a questão. Resposta: ". $opcoes3[$questao['que_resposta']] .".</div>";
						
				}
				else {
					$r = $respostas[$questao['que_id']] ? "Sua resposta foi <b>" . $opcoes3[$respostas[$questao['que_id']]] ."</b>." : "Você não respondeu esta questão.";
					$questao_html .= "<div style='margin: 10px 0; color: #ff0000;font-weight: bold;'>Você errou a questão. Resposta correta: " . $opcoes3[$questao['que_resposta']] . ".  " . $r ."</div>";
				}
			}
			$questao_html .= "</div>";
			$html .= $questao_html;
				
			if(!is_administrador() && ($numero_questao % 5 == 0)) {
				$html .=
				"<div style='text-align:center; margin-top: 20px;color: #999'>Direitos autorais reservados (Lei 9610/98). Proibida a reprodução, venda ou compartilhamento deste arquivo. Uso individual<br>
				Cópia registrada para {$usuario['nome_completo']} (CPF:{$usuario['cpf']})</div>";
			}
				
	}
	$html .= "</div>
			</div>";

	return stripcslashes($html);
}

function get_tamanho_fonte_style() {
	return "font-size:" . (isset($_SESSION['tamanho-fonte']) ? $_SESSION['tamanho-fonte'] : TAMANHO_FONTE) . 'px !important';
}

function imprimir_comentarios($questoes) {
    $CI =& get_instance();
    $CI->load->model('Comentario_model');
    $CI->load->model('Caderno_model');
    $CI->load->model('Simulado_model');

    $html = "";

    $items = [];
    $questoes_comentarios = [];

    if ($questoes) {
        for($i = 0; $i < count($questoes); $i++) {
            $questoes_comentarios[$i] = $CI->Comentario_model->listar_destaques_por_questao($questoes[$i]['que_id']);
        }

        $html .= formatar_imprimir_comentarios($questoes_comentarios);
    }

    return $html;
}

function formatar_imprimir_comentarios($questoes_comentarios)
{
    $html = '';

    if ($questoes_comentarios) {
        $html .= "<h1 style='margin-top: 50px;'>Comentários</h1>";

        foreach ($questoes_comentarios as $indice_questao => $comentarios) {
            $numero = $indice_questao + 1;
            $html .= "<div style='margin-bottom: 30px;'>";
            $html .= "<div><h2>Questão {$numero}</h2></div>";

            if ($comentarios) {

                foreach ($comentarios as $comentario) {
                    $usuario = get_usuario_array($comentario['user_id']);
                    $texto = purificar_html($comentario['com_texto']);

                    $html .= "<div style='background-color: #f9f9f9; padding: 10px; margin-bottom: 10px'>";

                    if ($usuario['nome_completo']) {
                        $html .= "<div><strong>Por:</strong> {$usuario['nome_completo']}</div>";
                    }

                    if ($comentario['com_data_criacao']) {
                        $data_criacao = converter_para_ddmmyyyy($comentario['com_data_criacao']);
                        $html .= "<div><strong>Criado em:</strong> {$data_criacao}</div>";
                    }

                    if ($comentario['com_data_edicao']) {
                        $data_edicao = converter_para_ddmmyyyy($comentario['com_data_edicao']);
                        $html .= "<div><strong>Editado em:</strong> {$data_edicao}</div>";
                    }

                    $html .= "<div style='margin-top: 10px'>{$texto}</div>";
                    $html .= "</div>";
                }
            }
            else {
                $html .= "Esta questão não possui comentários.";
            }


            $html .= "</div>";
        }
    }

    return $html;
}