<?php

/** TODO: verificar os outros pontos em que essa informação é necessária e centralizar nesse ponto
 * Verifica e retorna qual o peso utilizado para multiplicar a nota de uma disciplina de um ranking
 *  
 * @since L1
 * 
 * @param int $ranking_id ID do ranking
 * @param int $disciplina_id ID da disciplina
 * @param array $disciplina_grupo Disciplina grupo
 * @param array $ranking Ranking
 * 
 * @return float peso utilizado na disciplina
 * 
 */
function get_peso_utilizado($ranking_id, $disciplina_id, $disciplina_grupo = null, $ranking = null)
{
        
    $CI =& get_instance();

    if(is_null($disciplina_grupo) || is_null($ranking)) {

        if(!class_exists('Ranking_model'))
        {
            $CI->load->model('ranking_model');
        }
        
        if(is_null($disciplina_grupo))
        {
            $disciplina_grupo = $CI->ranking_model->get_disciplina_grupo($ranking_id, $disciplina_id);
        }
        
        if(is_null($ranking))
        {
            $ranking = $CI->ranking_model->get_by_id($ranking_id);
        }
        
    }

    if($rag_id = $disciplina_grupo['rag_id']) {
        // disciplina está associada a algum grupo

        $grupo = $CI->ranking_model->get_grupo($rag_id);

        // grupo tem peso associado
        if($grupo['rag_peso'] > 1) {
            $peso = $grupo['rag_peso'];
        }

        // disciplina tem peso associado
        elseif($disciplina_grupo['rad_peso'] > 1){
            $peso = $disciplina_grupo['rad_peso'];
        }

        // grupo não tem peso associado, nesse caso usamos o peso do ranking
        else {
            $peso = ($ranking['ras_peso']?:1);
        }

    }
    else {
        // disciplina não está associada a nenhum grupo
        if($disciplina_grupo['rad_peso'] > 1){
            $peso = $disciplina_grupo['rad_peso'];
        }else{
            $peso = ($ranking['ras_peso']?:1);				
        }
    }

    return $peso;
}