<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Helper para organizar rotinas relacionadas ao filtros do SQ
 *
 * @package SQ/Helpers
 *
 * @author André Gervásio <andre@keydea.com.br>
 *
 * @since A1
 */
function init_combos(&$data, $disciplinas_somente_ativas = TRUE, $simulados_somente_ativos = FALSE)
{
    $CI = &get_instance();
    $CI->load->model('disciplina_model');
    $CI->load->model('banca_model');
    $CI->load->model('orgao_model');
    $CI->load->model('cargo_model');
    $CI->load->model('assunto_model');
    $CI->load->model('prova_model');
    $CI->load->model('questao_model');
    $CI->load->model('area_formacao_model');
    $CI->load->model('area_atuacao_model');
    $CI->load->model('simulado_model');
    $CI->load->model('comentario_model');
    $CI->load->model('caderno_model');

    $data['array_ids'] = array(
        "#ban_ids" => "Banca",
        "#org_ids" => "Orgão",
        "#car_ids" => "Cargo",
        "#pro_anos" => "Ano",
        "#esc_ids" => "Nível Escolar",
        "#arf_ids" => "Área de Formação",
        "#dis_ids" => "Disciplina",
        "#ass_ids" => "Assunto",
        "#ara_ids" => "Area de Atuação",
        "#que_tipos" => "Tipo de Questão",
        "#que_dificuldades" => "Dificuldade",
        "#professores_ids" => "Comentários dos Professores",
        "#que_statuses" => "Status",
        "#neg_cad_ids" => "Não presente em cadernos",
        "#pro_tipo_ids" => "Modalidades",
        "#sim_ids" => "Não usadas nos Simulados",
        "#status_ids" => "Status"
    );
    $data['palavra_chave'] = '';
    $data['bancas'] = array(); //get_banca_multiselect($CI->banca_model->listar_todas());
    $data['bancas_selecionadas'] = array();
    $data['orgaos'] = array(); //get_orgao_multiselect($CI->orgao_model->listar_todas());
    $data['orgaos_selecionados'] = array();
    $data['cargos'] = array(); //get_cargo_multiselect($CI->cargo_model->listar_todas());
    $data['cargos_selecionados'] = array();
    $data['anos'] = get_ano_multiselect($CI->prova_model->listar_anos());
    $data['anos_selecionados'] = array();
    $data['escolaridades'] = get_escolaridade_multiselect();
    $data['escolaridades_selecionadas'] = array();
    $data['statuses'] = get_status_questao_multiselect();
    $data['statuses_selecionadas'] = array();
    $data['formacoes'] = get_area_formacao_multiselect($CI->area_formacao_model->listar_todas());
    $data['formacoes_selecionadas'] = array();
    $data['disciplinas'] = array(); //get_disciplina_multiselect($CI->disciplina_model->listar_todas(FALSE, $disciplinas_somente_ativas), TRUE);
    $data['disciplinas_selecionadas'] = array();
    $data['assuntos'] = array();
    $data['assuntos_selecionados'] = array();
    $data['atuacoes'] = get_area_atuacao_multiselect($CI->area_atuacao_model->listar_todas());
    $data['atuacoes_selecionadas'] = array();
    $data['tipos'] = get_tipo_questao_multiselect();
    $data['tipos_selecionados'] = array();
    $data['dificuldades'] = get_dificuldade_questao_multiselect();
    $data['dificuldades_selecionadas'] = array();
    $data['cadernos'] = get_current_user_id() ? get_caderno_combo($CI->caderno_model->listar_cadernos(get_current_user_id()), null, true) : [];
    $data['professores'] = get_professores_multiselect(get_professores(FALSE));
    $data['simulados'] = get_simulado_multiselect($CI->simulado_model->listar(null, null, $simulados_somente_ativos), true, true, true);
    $data['simulados_selecionados'] = array();
    $data['modalidades'] = get_modalidades_combo();
    $data['situacoes'] = get_situacoes_combo();
    $data['sim_excluir_questoes'] = get_excluir_questoes_simulado();
    $data['filtro_excluir'] = array();
    $data['status_selecionados'] = array();
}

function preencher_formulario_filtro($filtro, &$data)
{
    $posts = get_array_posts();

    foreach ($posts as $post => $selecionados) {
        if (! empty($filtro[$post])) {
            $data[$selecionados] = $filtro[$post];
        }
    }
}

/**
 * Adiciona os assuntos filhos no filtro de questões
 *
 * @since D1
 *
 * @param array $filtro Filtro de questões
 */

function adicionar_assuntos_filhos(&$filtro, $filtro_nome = 'ass_ids', $force = FALSE)
{
    /* === interrompe caso filtro mande ignorar adição de filhos === */
    if(!$force && isset($filtro['apenas_assunto_pai']) && $filtro['apenas_assunto_pai']) {
        //print_r("nao add filhos");
        return;
    }

    /* === lógica principal === */
    $ci = &get_instance();
    $ci->load->model('assunto_model');

    if (isset($filtro[$filtro_nome])) {
        foreach ($filtro[$filtro_nome] as $ass_id) {
            $assuntos = $ci->assunto_model->listar_filhos($ass_id);

            foreach ($assuntos as $assunto) {
                array_push($filtro[$filtro_nome], $assunto['ass_id']);
            }
        }
    }
}

function adicionar_filtro(&$filtro, &$data, $that, $is_post = TRUE)
{
    KLoader::helper("StringHelper");

    $posts = get_array_posts();
    foreach ($posts as $post => $selecionados) {
        if($is_post == TRUE){
            $valor = $that->input->post($post);
        }else{
            $valor = $that->input->get($post);
        }

        if (! empty($valor)) {

            if($is_post == TRUE){
                $filtro[$post] = $that->input->post($post);
            }else{
                $filtro[$post] = $that->input->get($post);
            }

            if ($post == "busca") {
                $filtro[$post] = StringHelper::remover_contrabarras($filtro[$post]);
            }

            if($is_post == TRUE){
                $data[$selecionados] = $that->input->post($post);
            }else{
                $data[$selecionados] = $that->input->get($post);
            }
        }
    }
}

function adicionar_todos_filtros(&$filtro, &$data, $that, $is_post = TRUE)
{

    if($is_post)
    {
        if($that->input->post('status_ids'))
        {
            $filtro['status_ids'] = $that->input->post('status_ids');
        }
        if ($that->input->post('filtro_favoritas')) {
            $filtro['favoritas'] = $that->input->post('filtro_favoritas');
        }
        if ($that->input->post('data_inicio')) {
            $filtro['data_inicio'] = $that->input->post('data_inicio');
        }
        if ($that->input->post('data_fim')) {
            $filtro['data_fim'] = $that->input->post('data_fim');
        }   

        $data['apenas_assunto_pai'] = $that->input->post("apenas_assunto_pai");
    }
    else
    {
        if($that->input->get('status_ids'))
        {
            $filtro['status_ids'] = $that->input->get('status_ids');
        }
        if ($that->input->get('filtro_favoritas')) {
            $filtro['favoritas'] = $that->input->get('filtro_favoritas');
        }
        if ($that->input->get('data_inicio')) {
            $filtro['data_inicio'] = $that->input->get('data_inicio');
        }
        if ($that->input->get('data_fim')) {
            $filtro['data_fim'] = $that->input->get('data_fim');
        }   

        $data['apenas_assunto_pai'] = $that->input->get("apenas_assunto_pai");
    }
    
    adicionar_filtro($filtro, $data, $that, $is_post);

    if(!$data['apenas_assunto_pai'])
    {
        adicionar_assuntos_filhos($filtro);
    }
    else
    {
        $filtro['ass_ids_excluir'] = $filtro['ass_ids'];
        adicionar_assuntos_filhos($filtro, 'ass_ids_excluir', TRUE);
        $filtro['ass_ids_excluir'] = array_diff($filtro['ass_ids_excluir'], $filtro['ass_ids']);
    }

}

function gerar_arvore_array($assuntos, $pai_id = null, $profundidade = 0)
{
    $array_ref = array();
    for ($i = 0, $ni = count($assuntos); $i < $ni; $i ++) {
        if ($assuntos[$i]['ass_pai_id'] == $pai_id) {
            $assuntos[$i]['filhos'] = gerar_arvore_array($assuntos, $assuntos[$i]['ass_id'], $profundidade + 1);
            array_push($array_ref, $assuntos[$i]);
        }
    }
    return $array_ref;
}

function get_array_input_names($place = null)
{
    $array = array(
        "ban_ids",
        "org_ids",
        "car_ids",
        "pro_anos",
        "esc_ids",
        "ara_ids",
        "que_dificuldades",
        "arf_ids",
        "professores_ids",
        "que_statuses",
        "neg_cad_ids",
        "pro_tipo_ids",
        "status_ids"
    );

    if ($place == "listar") {
        array_push($array, "dis_ids", "que_tipos", "sim_ids", "sim_excluir_questoes");
    } else {
        array_push($array, "que_tipos", "sim_excluir_questoes");
    }
    return $array;
}

function get_array_input_labels($place = null)
{
    $array = array(
        "Banca",
        "Orgão",
        "Cargo",
        "Ano",
        "Nível Escolar",
        "Área de Atuação",
        "Dificuldade",
        "Área de Formação",
        "Comentários dos Professores",
        "Status",
        "Não presente em cadernos",
        "Modalidades",
        "Status"
    );

    if ($place == "listar") {
        array_push($array, "Disciplina", "Tipo de Questão", "Não usada nos Simulados", "Excluir questões");
    } else {
        array_push($array, "Tipo de Questão", "Excluir questões");
    }
    return $array;
}

function get_array_table_fields($place = null)
{
    $array = array(
        "ban_id",
        "org_id",
        "car_id",
        "pro_ano",
        "esc_id",
        "ara_id",
        "que_dificuldade",
        "arf_id",
        "",
        "que_status",
        "busca",
        ""
    );

    if ($place == "listar") {
        array_push($array, "dis_id", "que_tipo", "", "sim_excluir_questoes");
    } else {
        array_push($array, "que_tipo", "sim_excluir_questoes");
    }
    return $array;
}

function get_array_nome_selecionados()
{
    return array(
        'bancas_selecionadas',
        'orgaos_selecionados',
        'cargos_selecionados',
        'anos_selecionados',
        'escolaridades_selecionadas',
        'atuacoes_selecionadas',
        'dificuldades_selecionadas',
        'formacoes_selecionadas',
        'tipos_selecionados',
        'statuses_selecionadas',
        'busca'
    );
}

function get_array_posts()
{
    return array(
        'filtro_comentarios' => 'filtro_comentarios',
        'filtro_incluir' => 'filtro_incluir',
        'filtro_resposta' => 'filtro_resposta',
        'filtro_repetidas' => 'filtro_repetidas',
        'filtro_excluir' => 'filtro_excluir',
        'filtro_ativo' => 'filtro_ativo',
        'que_qcon_id' => 'que_qcon_id',
        'que_id' => 'questao_id',
        'palavra_chave' => 'palavra_chave',
        'ban_ids' => 'bancas_selecionadas',
        'org_ids' => 'orgaos_selecionados',
        'car_ids' => 'cargos_selecionados',
        'pro_anos' => 'anos_selecionados',
        'esc_ids' => 'escolaridades_selecionadas',
        'arf_ids' => 'formacoes_selecionadas',
        'dis_ids' => 'disciplinas_selecionadas',
        'ass_ids' => 'assuntos_selecionados',
        'ara_ids' => 'atuacoes_selecionadas',
        'que_tipos' => 'tipos_selecionados',
        'que_dificuldades' => 'dificuldades_selecionadas',
        'sim_ids' => 'simulados_selecionados',
        'professores_ids' => 'professores_selecionados',
        'que_statuses' => 'statuses_selecionadas',
        'neg_cad_ids' => 'neg_cadernos_selecionados',
        'pro_tipo_ids' => 'modalidades_selecionadas',
        'sim_excluir_questoes' => 'sim_excluir_questoes_selecionados',
        'status_ids' => 'status_selecionados',
        'busca' => 'busca',
        'apenas_assunto_pai' => 'apenas_assunto_pai'
    );
}

function get_coluna($array, $coluna)
{
    $retorno = array();
    foreach ($array as $valor)
        array_push($retorno, $valor[$coluna]);

    return $retorno;
}

/**
 * @deprecated O filtro não está mais sendo deixado em sessão
 */
function has_filtro_selecionado()
{
    if (isset($_SESSION['filtro']))
    	return count(unserialize($_SESSION['filtro'])) && unserialize($_SESSION['filtro']) != TODAS ? true : false;
    return false;
}

function get_remover_filtro_url($campo, $valor)
{
    $url = get_url_atual();

    //Remove o conjunto chave e valor da query string da url atual
    $url = str_replace($campo . "=" . $valor . "&", "", $url);
    //$url = str_replace($campo . "[]=" . $valor . "&", "", $url);
    //$url = str_replace($campo . "%5B%5D=" . $valor . "&", "", $url);
    $url = preg_replace('/\b('.$campo.'\[)[0-9]{0,10}(\]='.$valor.'&)\b/', '', $url);
    $url = preg_replace('/\b('.$campo.'%5B)[0-9]{0,10}(%5D='.$valor.'&)\b/', '', $url);
    $url = str_replace($campo . "=" . $valor, "", $url);
    //$url = str_replace($campo . "[]=" . $valor, "", $url);
    //$url = str_replace($campo . "%5B%5D=" . $valor, "", $url);
    $url = preg_replace('/\b('.$campo.'\[)[0-9]{0,10}(\]='.$valor.')\b/', '', $url);
    $url = preg_replace('/\b('.$campo.'%5B)[0-9]{0,10}(%5D='.$valor.')\b/', '', $url);

    return $url;
}

function get_filtro_incluir_by_id($id)
{
    $items = get_filtro_incluir_combo();

    return $items[$id];
}

function get_filtro_comentarios_by_id($id)
{
    $items = get_filtro_comentarios_combo();

    return $items[$id];
}

function get_filtro_respostas_by_id($id)
{
    $items = get_filtro_resposta_combo();

    return $items[$id];
}

function get_filtro_repetidas_by_id($id)
{
    $items = get_filtro_repetidas_combo();

    return $items[$id];
}

function get_filtro_excluir_by_id($id)
{
    $items = get_filtro_excluir_combo();

    return $items[$id];
}

function get_filtro_key($url = null)
{
    if (is_null($url)) {
        $url = get_url_atual();
    }

    if (strpos($url, '/admin/listar_questoes') !== false) {
        return FILTRO_KEY_LISTAR_QUESTOES;
    } elseif (strpos($url, '/admin/revisar_questoes') !== false) {
        return FILTRO_KEY_REVISAR_QUESTOES;
    } else {
        return FILTRO_KEY_RESOLVER_QUESTOES;
    }
}

function is_filtro_key($filtro_key, $url = null)
{
    return get_filtro_key($url) == $filtro_key;
}

// /**
//  * Verifica se os filtros possuem valor aplicado
//  *
//  * @param array $filtros Filtros
//  *
//  * @return bool
//  */

// function tem_filtro_aplicado($filtros)
// {
// 	foreach ($filtros as $filtro) {
// 		if(!is_null($filtro) && $filtro != "") {
// 			return true;
// 		}
// 	}

// 	return false;
// }

/**
 * Verifica os filtros selecionados e monta seu texto de apresentação
 *
 * @since L1
 *
 * @param Array $filtro Filtro utilizado na busca
 *
 * @return Array contendo informações sobre cada filtro selecionado
 */
function get_filtros_selecionados($filtro, $bloqueios = [])
{
    $CI = &get_instance();
    $CI->load->model('simulado_model');
    $CI->load->model('disciplina_model');
    $CI->load->model('assunto_model');
    $CI->load->model('banca_model');
    $CI->load->model('orgao_model');
    $CI->load->model('cargo_model');
    $CI->load->model('area_formacao_model');
    $CI->load->model('area_atuacao_model');
    $CI->load->model('caderno_model');

    $selecionados = [];

    foreach ($filtro as $campo => $valores) {
    	if($valores) {

    		// transforma os campos de entrada única em arrays
    		if(in_array($campo, ['palavra_chave', 'que_id', 'que_qcon_id', 'filtro_incluir', 'filtro_comentarios', 'filtro_resposta', 'filtro_repetidas', 'filtro_ativo', 'favoritas', 'data_inicio', 'data_fim', 'busca', 'cad_id'])) {
    			$valores = [$valores];
    		}

    		foreach($valores as $valor) {

    			if(!in_array($campo, $bloqueios)) {
    				$selecionado['url'] = get_remover_filtro_url($campo, $valor);
    			} else {
    				$selecionado['url'] = '#';
    			}

    			switch ($campo) {
    				case 'dis_ids': {
    					// filtro de disciplinas não aparece no revisar questões
    					if(is_filtro_key(FILTRO_KEY_REVISAR_QUESTOES)) {
    						break 2;
    					}

    					$item = $CI->disciplina_model->get_by_id($valor);
    					if ($valor == -1){
    						$item['dis_nome'] = "(Sem disciplina)";
    					}
    					$selecionado['nome'] = 'Disciplina: ' . $item['dis_nome'];
    					break;
    				}

    				case 'ass_ids': {
    					$item = $CI->assunto_model->get_by_id($valor);
    					$selecionado['nome'] = 'Assunto: ' . $item['ass_nome'];
    					break;
    				}

    				case 'ban_ids': {
    					$item = $CI->banca_model->get_by_id($valor);
    					$selecionado['nome'] = 'Banca: ' . $item['ban_nome'];
    					break;
    				}

    				case 'org_ids': {
    					$item = $CI->orgao_model->get_by_id($valor);
    					$selecionado['nome'] = 'Orgão: ' . $item['org_nome'];
    					break;
    				}

    				case 'car_ids': {
    					$item = $CI->cargo_model->get_by_id($valor);
    					$selecionado['nome'] = 'Cargo: ' . $item['car_nome'];
    					break;
    				}

    				case 'pro_anos': {
    					$selecionado['nome'] = 'Ano: ' . $valor;
    					break;
    				}

    				case 'esc_ids': {
    					$selecionado['nome'] = 'Escolaridade: ' . get_escolaridade_by_id($valor);
    					break;
    				}

    				case 'arf_ids': {
    					$item = $CI->area_formacao_model->get_by_id($valor);
    					$selecionado['nome'] = 'Área de Formação: ' . $item['arf_nome'];
    					break;
    				}

    				case 'ara_ids': {
    					$item = $CI->area_atuacao_model->get_by_id($valor);
    					$selecionado['nome'] = 'Área de Atuação: ' . $item['ara_nome'];
    					break;
    				}

    				case 'que_tipos': {
    					$selecionado['nome'] = 'Tipo de Questão: ' . get_tipo_questao_by_id($valor);
    					break;
    				}

    				case 'que_dificuldades': {
    					$selecionado['nome'] = 'Dificuldade: ' . get_dificuldade_questao_by_id($valor);
    					break;
    				}

    				case 'sim_ids': {
    					if($valor == TODOS_SIMULADOS){
    						$nome = 'Simulado: Todos';
    					}elseif($valor == TODOS_SIMULADOS_ATIVOS){
    						$nome = 'Simulado: Todos ativos';
    					}elseif($valor == TODOS_SIMULADOS_MENOS_INATIVOS){
    						$nome = 'Simulado: Todos menos inativos';
    					}else{
    						$item = $CI->simulado_model->get_by_id($valor);
    						$nome = 'Simulado: ' . $item['sim_nome'];
    					}
    					$selecionado['nome'] = 'Simulado: ' . $nome;
    					break;
    				}

    				case 'professores_ids': {
    					$item = get_usuario_array($valor);
    					$selecionado['nome'] = 'Professor: ' . $item['nome_completo'];
    					break;
    				}

    				case 'que_statuses': {
    					$selecionado['nome'] = 'Status: ' . get_status_questao_by_id($valor);
    					break;
    				}

    				case 'neg_cad_ids': {
    					$item = $CI->caderno_model->get_caderno($valor);
    					$selecionado['nome'] = 'Não presente em caderno: ' . $item['cad_nome'];
    					break;
    				}

    				case 'pro_tipo_ids': {
    					$selecionado['nome'] = 'Modalidade: ' . get_modalidade_by_id($valor);
    					break;
    				}

    				case 'palavra_chave': {
    					$selecionado['nome'] = 'Palavra-chave: ' . $valor;
    					break;
    				}

    				case 'que_id': {
    					$selecionado['nome'] = 'Id da Questão: ' . $valor;
    					break;
    				}

    				case 'que_qcon_id': {
    					$selecionado['nome'] = 'Q: ' . $valor;
    					break;
    				}

    				case 'filtro_incluir': {
    					$selecionado['nome'] = 'Incluir: ' . get_filtro_incluir_by_id($valor);
    					break;
    				}

    				case 'filtro_excluir': {
    					$selecionado['nome'] = 'Excluir questões: ' . get_filtro_excluir_by_id($valor);
    					break;
    				}

    				case 'filtro_comentarios': {
    					$selecionado['nome'] = 'Comentários: ' . get_filtro_comentarios_by_id($valor);
    					break;
    				}

    				case 'filtro_resposta': {
    					$selecionado['nome'] = 'Resposta: ' . get_filtro_respostas_by_id($valor);
    					break;
					}

					case 'filtro_repetidas': {
						$selecionado['nome'] = 'Incluir questões: ' . get_filtro_repetidas_by_id($valor);
						break;
					}

    				case 'filtro_ativo': {
    					$selecionado['nome'] = 'Situação: ' . get_situacao_by_id($valor);
    					break;
    				}

    				case 'status_ids': {
    					$status = get_status_questao_multiselect($valor);
    					$selecionado['nome'] = 'Status: ' . $status["status"];
    					break;
    				}

    				case 'favoritas': {
    					$selecionado['nome'] = 'Favoritas';
    					break;
    				}

    				case 'data_inicio': {
    					$selecionado['nome'] = 'Adicionadas em (De): '. $valor;
    					break;
    				}

    				case 'data_fim': {
    					$selecionado['nome'] = 'Adicionadas em (Até): '. $valor;
    					break;
    				}
    				case 'busca':
    					$selecionado['nome'] = 'Busca por texto: '. $valor;
    					break;

    				default:
    					break 2;
    			}

    			array_push($selecionados, $selecionado);
    		}
    	}
    }

    return $selecionados;
}