<?php

function agrupar_arquivos($arquivos, $path, $tipo, $versao)
{
	$conteudo = "";

	foreach ($arquivos as $arquivo) {
		$file = FCPATH . $arquivo;

		$conteudo = read_file($file);

		switch ($tipo) {
			case 'css': $conteudo = minify_css($conteudo); break;
			case 'js': $conteudo = minify_js($conteudo); break;
		}

		$novo .= $conteudo;
	}

	write_file(FCPATH . $path, $novo);
	return base_url($path) . '?v=' . $versao;
}

/**
 * Grupo de componentes para Cabeçalhos de páginas
 */
function get_admin_cabecalho_pagina($titulo)
{
	return
		"<div class='row wrapper white-bg page-heading'>
			<div class='col-lg-12'>
				<h1>{$titulo}</h1>
			</div>
		</div>";
}

function admin_cabecalho_pagina($titulo)
{
	echo get_admin_cabecalho_pagina($titulo);
}

/**
 * Grupo de componentes para Botões
 */
function get_admin_botao_verde($texto, $link)
{
	return
		"<div><a class='btn btn-primary ' href='{$link}'>{$texto}</a></div>";
}

function admin_botao_verde($texto, $link)
{
	echo get_admin_botao_verde($texto, $link);
}

function get_admin_botao_branco($texto, $link, $attributos = "")
{
	return
		"<div><a class='btn btn-white' href='{$link}' {$attributos}>{$texto}</a></div>";
}
function get_botao_ranking($texto, $link)
{
	return
		"<a class='col-sm-10 col-md-8 col-lg-6 btn btn-azul btn-rounded-expo text-align-center' href='{$link}'><i class='fa fa-trophy'></i> <span class='hidden-xs hidden-sm pull-right'>{$texto}</span></a>";
}
function get_admin_botao_listar($texto, $link, $attributos = "")
{
	return
		"<a class='btn btn-white btn-sm' href='{$link}' {$attributos}>
			<i class='fa fa-list'> </i> {$texto}
		</a>";
}

function admin_botao_branco($texto, $link)
{
	echo get_admin_botao_branco($texto, $link);
}

function get_admin_botao_icone($texto, $link, $icone)
{
	return
		"<a class='btn btn-white btn-sm' title='{$texto}' href='{$link}'>
			<i class='fa {$icone}'> </i> {$texto}
		</a>";
}

function get_admin_botao_excluir($texto, $link)
{
	return
		"<a class='btn btn-white btn-sm' title='{$texto}' href='{$link}' onclick=\"return confirm('Você realmente deseja apagar este item?')\">
			<i class='fa fa-trash-o'> </i> {$texto}
		</a>";
}

function get_admin_botao_questoes($texto, $link)
{
	return
		"<a class='btn btn-white btn-sm' title='{$texto}' href='{$link}' onclick=\"return confirm('Você realmente deseja apagar este item?')\">
			<i class='fa fa-trash-o'> </i> {$texto}
		</a>";
}

function admin_botao_excluir($texto, $link)
{
	echo get_admin_botao_excluir($texto, $link);
}

function get_admin_model_adicionar($questao)
{
	return
	"<a class='btn btn-white btn-sm' data-toggle='modal' data-target='#modal-adicionar-questao' title='Adicionar Questão a Simulado' href='".get_modal_adicionar_questao_url($questao)."'>
	<i class='fa fa fa-puzzle-piece'> </i> Ad. a Simulado
	</a>";
}

function admin_botao_adicionar($texto, $link)
{
	echo get_admin_botao_adicionar($texto, $link);
}

function get_admin_botao_editar($texto, $link)
{
	return
		"<a class='btn btn-white btn-sm' title='{$texto}' href='{$link}'>
			<i class='fa fa-pencil'> </i> {$texto}
		</a>";
}

function get_admin_botao_responsivo($texto, $texto_mobile, $link)
{
	return
	"<a class='btn btn-white btn-sm mobile-show' style='width=100%; margin-top:5px' title='{$texto}' href='{$link}'>
		<i class='fa fa-pencil mobile-hide'> </i>
		<span style='width: 100%;margin: 2px;'>{$texto_mobile}</span>
	</a>
	<a class='btn btn-white btn-sm mobile-hide' title='{$texto}' href='{$link}'>
		<i class='fa fa-pencil'> </i>
		<span class=''>{$texto}</span>
	</a>";
}

function admin_botao_editar($texto, $link)
{
	echo get_admin_botao_editar($texto, $link);
}

function get_admin_botao_submit($texto, $name, $class = '', $atributos = '')
{
	return "<input class='btn btn-primary {$class}' type='submit' name='{$name}' value='{$texto}' {$atributos} />";
}

function admin_botao_submit($texto, $name)
{
	echo get_admin_botao_submit($texto, $name);
}

function get_admin_botoes_salvar_cancelar($cancelar_link, $salvar_name = 'salvar')
{
	$botao_salvar = get_admin_botao_submit('Salvar', $salvar_name);

	return
		"<div class='col-sm-4 col-sm-offset-2'>
	    	<a class='btn btn-white' href='{$cancelar_link}'>Cancelar</a>
	        {$botao_salvar}
      	</div>
	    <div style='clear:both'></div>";
}

function get_admin_botao_cancelar($link, $name = 'Cancelar')
{
	return
		"<div class='col-sm-4 col-sm-offset-2'>
	    	<a class='btn btn-white' href='{$link}'>{$name}</a>
	        {$botao_salvar}
      	</div>
	    <div style='clear:both'></div>";
}

function get_admin_botoes_navegacao_ranking($ranking_id = null, $pagina = 1)
{
	$nome_salvar = $pagina == 4 ? "Finalizar" : "Avançar";
	$botao_salvar = get_admin_botao_submit($nome_salvar, 'salvar', 'btn-ranking-avancar', 'id="avancar"');
	$cancelar_link = get_listar_rankings_url();
	$voltar_link = get_editar_ranking_url($ranking_id, $pagina - 1);
	$avancar_link = get_editar_ranking_url($ranking_id, $pagina + 1);

	$html =
		"<div class='col-sm-4 col-sm-offset-2'>
	    	<a class='btn btn-white btn-ranking-cancelar' href='{$cancelar_link}'>Cancelar</a>";

	if($pagina > 1) {
    	$html .= "<a class='btn btn-white btn-ranking-voltar' href='{$voltar_link}'>Voltar</a>";
    }

    if($pagina == 2) {
		$html .= "<a class='btn btn-primary btn-ranking-avancar' href='{$avancar_link}'>Avançar</a>";
	}
	else {
		$html .= "{$botao_salvar}";
	}

	$html .= "</div><div style='clear:both'></div>";

	return $html;
}

function admin_botoes_salvar_cancelar($cancelar_link, $salvar_name = 'salvar')
{
	echo get_admin_botoes_salvar_cancelar($cancelar_link, $salvar_name);
}

/**
 * Grupo de componentes para Tabelas
 */
function get_data_table($thead_array, $tbody_array, $tfoot_array, $options)
{
	$thead = '<tr>';
	foreach ($thead_array as $item) {
		$thead .= "<th>{$item}</th>";
	}
	$thead .= '</tr>';

	$tbody = '';
	foreach ($tbody_array as $row) {
		$tbody .= "<tr>";
		foreach ($row as $item) {
			$tbody .= "<td>{$item}</td>";
		}
		$tbody .= "</tr>";
	}

	$tfoot = '<tr>';
	foreach ($tfoot_array as $item) {
		$tfoot .= "<th>{$item}</th>";
	}
	$tfoot .= '</tr>';

	$id = $options['id'] ? " id='{$options['id']}'" : "";
	$data_table_class = $options['data_table_class'] ? $options['data_table_class'] : 'dataTables';

	return
		"<div class='wrapper wrapper-content animated fadeInRight'>
			<div class='row'>
				<div class='col-lg-12'>
					<div class='ibox-content' style='padding: 0'>
						<div class='table-responsive'>
							<table class='table table-striped table-bordered table-hover {$data_table_class}' $id >
								<thead>{$thead}</thead>
								<tbody>{$tbody}</tbody>
								<tfoot>{$tfoot}</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>";
}

function data_table($thead_array, $tbody_array, $tfoot_array, $options = array())
{
	echo get_data_table($thead_array, $tbody_array, $tfoot_array, $options);
}

function get_simple_table($thead_array, $tbody_array, $tfoot_array, $pagination, $url)
{
	$thead = '<tr>';
	foreach ($thead_array as $item) {
		$thead .= "<th>{$item}</th>";
	}
	$thead .= '</tr>';

	$tbody = '';
	foreach ($tbody_array as $row) {
		$tbody .= "<tr>";
		foreach ($row as $item) {
			$tbody .= "<td>{$item}</td>";
		}
		$tbody .= "</tr>";
	}

	$tfoot = '<tr>';
	foreach ($tfoot_array as $item) {
		$tfoot .= "<th>{$item}</th>";
	}
	$tfoot .= '</tr>';

	$q = '';

	if(isset($_POST['q'])){
		$q = $_POST['q'];
	}else if( isset($_SESSION['q_assunto'])){
		$q = $_SESSION['q_assunto'];
	}

	$st_a = '';

	if(isset($_POST['st_a'])){
		$st_a = $_POST['st_a'];
	}else if( isset($_SESSION['st_a_assunto'])){
		$st_a = $_SESSION['st_a_assunto'];
	}

	$st_a_chk = ATIVO == $st_a ? "checked" : "";

	$st_i = '';

	if(isset($_POST['st_i'])){
		$st_i = $_POST['st_i'];
	}else if( isset($_SESSION['st_i_assunto'])){
		$st_i = $_SESSION['st_i_assunto'];
	}

	$st_i_chk = INATIVO == $st_i ? "checked" : "";

	$filtro = "";
	if($url) {
		$filtro = "
		<div class='col-lg-2 assunto-status'>
			<input type='checkbox' name='st_a' value='".ATIVO."' {$st_a_chk}><span class='span_checkbox'>Ativo</span>
			<input type='checkbox' name='st_i' value='".INATIVO."' {$st_i_chk}><span class='span_checkbox'>Inativo</span>
		</div>
		<div class='col-lg-3'>
			<input type='textbox' name='q' value='{$q}' class='form-control input-sm'>
		</div>
		<div class='col-lg-1'>
			<input type='submit' name='filtrar' value='Filtrar' class='btn btn-default btn-outline'>
		</div>";
	}
	else {
		$filtro = "<div class='col-lg-6'></div>";
	}

	return
	"<div class='wrapper wrapper-content animated fadeInRight'>
		<div class='row'>
			<div class=''>
				<div class='ibox-content'>
					<div style='padding-bottom: 10px'>
						<form method='post' action='{$url}'>
							<div class='col-lg-6'>{$pagination}</div>
							{$filtro}
						</form>
					</div>
					<div class='row' style='clear:both; padding-top: 10px'>
						<div class='table-responsive'>
							<table class='table table-striped table-bordered table-hover' >
								<thead>{$thead}</thead>
								<tbody>{$tbody}</tbody>
								<tfoot>{$tfoot}</tfoot>
							</table>
							<div>{$pagination}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>";
}

function simple_table($thead_array, $tbody_array, $tfoot_array, $pagination, $url)
{
	echo get_simple_table($thead_array, $tbody_array, $tfoot_array, $pagination, $url);
}

function get_matriz_table($matriz)
{
	$thead = "<tr>
				<th></th>";

	foreach($matriz as $linha) {
		foreach(array_keys($linha) as $coluna_nome) {
			$thead .= "<th>{$coluna_nome}</thd>";
		}
		break;
	}
	$thead .= "</tr>";


	$tbody = '';

	foreach ($matriz as $linha_nome => $linha) {
		$tbody .= "<tr>
					<td>{$linha_nome}</td>";

		foreach ($linha as $coluna_nome => $valor) {
			$tbody .= "<td>{$valor}</td>";
		}

		$tbody .= "</tr>";
	}

	return
		"<div class='wrapper wrapper-content animated fadeInRight'>
			<div class='row'>
				<div class=''>
					<div class='ibox-content'>
						<div class='row' style='clear:both; padding-top: 10px'>
							<div class='table-responsive'>
								<table class='table table-striped table-bordered table-hover' >
									<thead>{$thead}</thead>
									<tbody>{$tbody}</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>";
}

/**
 * Grupo de componentes para Cabeçalhos de páginas da Área do Aluno
 */
function get_main_cabecalho_pagina($titulo)
{
	return
	"<div class='row wrapper white-bg page-heading'>
	<div class='col-lg-12'>
	<h2>{$titulo}</h2>
	</div>
	</div>";
}

function main_cabecalho_pagina($titulo)
{
	echo get_main_cabecalho_pagina($titulo);
}

function get_questoes_por_pagina_array()
{
	return array(1 => 1, 5 => 5, 10 => 10, 20 => 20, 50 => 50);
}

function get_modal_confirmacao($titulo, $mensagem, $url_retorno, $name = 'item_id')
{
	return "<div class='modal-dialog'>
				<div class='modal-content'>
					<div class='modal-header'>
						<button type='button' class='close' data-dismiss='modal'>&times;</button>
						<h4 class='modal-title'>{$titulo}</h4>
					</div>
					<div class='modal-body'>

						<div class='panel blank-panel'>
							<div class='panel-body modal-exclusao'>
								<div class='modal-exclusao-mensagem'>{$mensagem}</div>

								<form action='{$url_retorno}' method='post'>
								<div>
									<div>
										<input type='hidden' id='{$name}' name='{$name}'>
										<input type='submit' value='Sim' name='submit' class='btn btn-primary'>
										<input type='button' value='Não' data-dismiss='modal' class='btn btn-white'>
									</div>
								</div>
								</form>
			                </div>
			            </div>
					</div>
				</div>
			</div>";
}

function set_mensagem($tipo, $mensagem)
{
	$_SESSION[$tipo] = $mensagem;
}

function exibir_mensagem()
{
	if(isset($_SESSION[SUCESSO])) {
		echo "<div class='alert alert-success alert-dismissable'>
              	<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
              	{$_SESSION[SUCESSO]}
             </div>";
        $_SESSION[SUCESSO] = null;
	}
	elseif(isset($_SESSION[ERRO])) {
		echo "<div class='alert alert-danger alert-dismissable'>
              	<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
              	{$_SESSION[ERRO]}
             </div>";
       $_SESSION[ERRO] = null;
	}
	elseif(isset($_SESSION[AVISO])) {
		echo "<div class='alert alert-info alert-dismissable'>
              	<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>
              	{$_SESSION[AVISO]}
             </div>";
       $_SESSION[AVISO] = null;
	}
}

function get_comentario_box($comentario, $comentario_avaliacao = null, $destaque = false, $tem_acesso = true)
{
	KLoader::helper("UrlHelper");

	$usuario = get_usuario_array($comentario['user_id']);
	$nome_exibicao = get_usuario_nome_exibicao($usuario['id']);
	$is_professor_visivel = is_professor($comentario['user_id'], true);
	$is_professor_oculto = is_professor_oculto($comentario['user_id']);//Na opção acima existe um cache sem expiração que não ajuda nessa verificação

	if($is_professor_visivel && !$is_professor_oculto){
		$url = UrlHelper::get_professor_url($usuario['slug']);
		$foto = "<a href='{$url}' class='pull-left'>".
					get_foto_usuario($usuario['id']).
				"</a>";
		$nome_exibicao = "<a href='{$url}'>{$nome_exibicao}</a>";

	}
	else {
		$foto = get_foto_usuario($usuario['id']);
	}

	$estilo = $destaque ? 'comentario-professor' : '';

	if($usuario['id'] == get_current_user_id() || tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) {
		$botoes = "<div class='botoes'><a com_id='{$comentario['com_id']}' class='btn btn-primary excluir-comentario'> Excluir </a>
			<a usu_id='{$comentario['user_id']}' que_id='{$comentario['que_id']}' com_id='{$comentario['com_id']}'
			class='btn btn-primary abrir-editar-comentario' data-loading-text='Salvando...' > Editar </a>
			</div>";

		$botoes_edicao = "<div class='botoes-edicao' style='display:none'><a com_id='{$comentario['com_id']}' class='btn btn-primary editar-comentario' data-loading-text='Salvando...'> Salvar </a>
			<a usu_id='{$comentario['user_id']}' que_id='{$comentario['que_id']}' com_id='{$comentario['com_id']}'
			class='btn btn-primary cancelar-edicao'> Cancelar </a>
				<span id='msg-edit-comentario-{$comentario['com_id']}'></span>
			</div>";
	}

	$html= "<div class='social-feed-box $estilo' com_id='{$comentario['com_id']}'>
					<div class='social-avatar'>
						$foto
						<div class='media-body'>
							<div class='comentario-nome'>$nome_exibicao</div>
							<span>Criado em: <small class='text-muted'>".date('d/m/Y', strtotime($comentario['com_data_criacao']))."</small>";

	if(!empty($comentario['com_data_edicao'])) {
		$html .= 		"<br>Editado em: <small class='text-muted'>".date('d/m/Y', strtotime($comentario['com_data_edicao']))."</small>";
	}
	$html .= "</span>";

	if($destaque) {
		$html .= "<div class='comentario-professor-destaque'><span class='professor-exponencial'>Professor Exponencial</span>" . get_tema_image_tag('estrela-comentario-professor.png', 40, 40, 'Professor Exponencial', 'professor-exponencial-estrela') . "</div>";
	}

	if($tem_acesso) {
		$texto_comentario = purificar_html($comentario['com_texto']);
	}
	else {
		$link_sq = "/sistema-de-questoes";
		$link_minha_conta = is_usuario_logado() ? "/minha-conta" : login_url('/minha-conta');
		$texto = "O acesso aos comentários dos professores é exclusivo para assinantes do Sistema de Questões do Exponencial Concursos. <a href='{$link_sq}'>Clique aqui para mais informações.</a><br><br>";
		$texto .= "<b>Caso você tenha adquirido um Simulado Exponencial</b>: para resolvê-lo online, participar do Ranking e visualizar os comentários, acesse-o por meio da área do aluno clicando em <a href='{$link_minha_conta}'>Área do Aluno</a>.";
		$texto_comentario = ui_get_alerta($texto, ALERTA_ATENCAO);
	}

	if(!is_null($comentario_avaliacao) && $comentario_avaliacao['cav_avaliacao']){
		$estrelas = get_estrelas($comentario['com_id'], $comentario['com_media_avaliacao'], $comentario_avaliacao['cav_avaliacao']);
	}else{
		$estrelas = get_estrelas($comentario['com_id'], $comentario['com_media_avaliacao']);
	}

	$html .= 	"</div>
			</div>
			<section id='comentario-texto-{$comentario['com_id']}' class='social-body comentario-texto'>
				{$texto_comentario}
			</section>
			<div class='summernote-div' style='display:none'>{$texto_comentario}</div>
			<div class='row'><div class='col-md-7'>$botoes $botoes_edicao</div> $estrelas</div>
		</div>";

	return $html;
}

function get_estrelas($id, $media_comentario = 0, $valor = 0, $max_num = 5){

	$usuario_id = get_id_usuario_atual();
	$url = get_ajax_main_avaliar_comentario_url();
	$media = "";

	if(!is_null($media_comentario) && $media_comentario > 0){
		$media = number_format($media_comentario, 2, ',','')." Média Geral";
	}

	return "<div class='col-md-5 text-right'><span class='avaliacao'>Avalie este comentário: </span><span class='estrelas estrelas{$id}'></span><span class='media media{$id}'>{$media}</span></div>
		<script>
			$('.estrelas{$id}').stars({
				stars: {$max_num},"
				.(($valor > 0)?"value: {$valor},":"").
				"emptyIcon: 'fa-star-o',
				filledIcon: 'fa-star',
				click: function(i) {
					$.ajax({
						url: '{$url}',
						method: 'POST',
						cache: false,
						data: {
							id: {$id},
							usuId: {$usuario_id},
							avaliacao : i
						}
					})
					.done(function( msg ) {
						$('.media{$id}').html( msg );
					});
				}
			});
		</script>";
}

function is_questoes_admin()
{
	if(strpos($_SERVER['REQUEST_URI'], '/questoes/admin') === false)
		return false;
	return true;
}

function get_letra_opcao_questao($ordem, $tipo = MULTIPLA_ESCOLHA) {

	if($tipo == MULTIPLA_ESCOLHA || $tipo == MULTIPLA_ESCOLHA_5) {
		switch ($ordem) {
			case 1: return "a";
			case 2: return "b";
			case 3: return "c";
			case 4: return "d";
			case 5: return "e";
		}
	}
	else {
		switch ($ordem) {
			case 1: return "Certo";
			case 2: return "Errado";
		}
	}
}

function get_arvore($item, $max_nome_size = 9999, &$pai = null)
{
	$arvore = "";

	if($item['filhos']) {
		$arvore .= "<ul>";

		foreach ($item['filhos'] as $filho) {

			$arvore_filha = get_arvore($filho, $max_nome_size, $item);

			if($pai){
				//$pai['ass_qtd_questoes'] = $pai['ass_qtd_questoes']?:0;
				//$pai['ass_qtd_questoes'] += $filho['ass_qtd_questoes'];
			}

			$nome = isset($filho['dis_nome']) ? $filho['dis_nome'] : $filho['ass_nome'];
			$id = isset($filho['dis_nome']) ? 'd' . $filho['dis_id'] : $filho['ass_id'];

			//if($nome == SEM_ASSUNTO) continue;

			$qtd = "";
			if(isset($filho['ass_id'])) {
				$qtd = "(".($filho['ass_qtd_questoes'] ? $filho['ass_qtd_questoes'] : 0).")";
			}else{
				$qtd = "(".($filho['dis_qtd_questoes'] ? $filho['dis_qtd_questoes'] : 0).")";
			}

			if(strlen($nome) + strlen($qtd) > $max_nome_size){
				$nome = mb_substr($nome, 0, $max_nome_size -1 -strlen($qtd) ).'...';
			}

			//$nome = strlen($nome) > $max_nome_size ?  mb_substr($nome, 0, $max_nome_size -1) . '...' : $nome;

			$arvore .= "<li id='{$id}'> $nome <b>$qtd</b>";
			$arvore .= $arvore_filha;
			$arvore .= "</li>";

		}

		$arvore .= "</ul>";
	}

	return $arvore;
}

function pagination_config($base_url, $total_rows, $per_page, $url_segment = 3)
{
	$config = array();
	$config["base_url"] = $base_url;
	$config["total_rows"] = $total_rows;
	$config["per_page"] = $per_page;
	$config['uri_segment'] = $url_segment;
	$config['num_links'] = 3;
	$config['full_tag_open'] = '<div class="btn-group btn-pagination">';
	$config['full_tag_close'] = '</div>';
	$config['first_link'] = 'Primeiro';
	$config['first_tag_open'] = '<div class="btn btn-white">';
	$config['first_tag_close'] = '</div>';
	$config['last_link'] = 'Último';
	$config['last_tag_open'] = '<div class="btn btn-white">';
	$config['last_tag_close'] = '</div>';
	$config['next_link'] = '<i class="fa fa-chevron-right"></i>';
	$config['next_tag_open'] = '<div class="btn btn-white">';
	$config['next_tag_close'] = '</div>';
	$config['prev_link'] = '<i class="fa fa-chevron-left"></i>';
	$config['prev_tag_open'] = '<div class="btn btn-white">';
	$config['prev_tag_close'] = '</div>';
	$config['num_tag_open'] = '<div class="btn btn-white">';
	$config['num_tag_close'] = '</div>';
	$config['cur_tag_open'] = '<div class="btn btn-white  active">';
	$config['cur_tag_close'] = '</div>';

	return $config;
}


function get_botao_resposta($texto, $imagem, $estilo)
{
	return "<span class='resposta-mensagem {$estilo}'><img src='/wp-content/themes/academy/images/{$imagem}'> {$texto}</span>";
}

function get_pagina_gabarito($nome, $gabarito)
{
	$html =
		"<div class='row'>
			<img width='100%' src='/wp-content/themes/academy/images/cabecalho-cartao.jpg'>
		</div>";

	if($nome) {
		$html .= "<div class='header-azul'> " . get_admin_cabecalho_pagina ($nome) . "</div>";
	}

	$html .=
		"<div class='ibox' style='margin-top: 20px'>
			<div class='ibox-content'>
				<div class='row'>
					<table style='width:100%; border: 1px solid black; border-collapse: collapse;'> ";



	for($linha = 0; $linha < count($gabarito); $linha++) {

		$html .= "<tr style=border: 1px solid black; border-collapse: collapse;'>";

		for($coluna = 0; $coluna < count($gabarito[$linha]) ; $coluna++) {

			$html .= "<td style='padding:3px 10px; border: 1px solid black; border-collapse: collapse;width: 50px; padding:3px 10px; border: 1px solid black; border-collapse: collapse;'>";

			if(isset($gabarito[$linha][$coluna]['num'])) {
				$html .= $gabarito[$linha][$coluna]['num'] . ' - ' . $gabarito[$linha][$coluna]['resposta'];
			}

			$html .= "</td>";
		}

		$html .= "</tr>";
	}

	$html .= "		</table>
				</div>
			</div>
		</div>";

	return $html;
}

function meus_simulados_resultados($simulado)
{
	$CI =& get_instance();
	$CI->load->model('Simulado_model');

	if($simulado['usu_id']) {

		$ranking = $CI->Simulado_model->get_posicao_ranking($simulado['sim_id'], get_current_user_id());
		$count_simulado = $CI->Simulado_model->count_ranking($simulado['sim_id'], false);
		$href = get_ranking_simulado_url($simulado['sim_id']);
		$button = get_botao_ranking('Ranking', $href);
		$estilo = $simulado['aprovado'] ? "texto-verde" : "";
		$data = date('d/m/Y', strtotime($simulado['sim_usu_data_resolucao']));

		return
			"<tr>
				<td class='text-left'>{$simulado['sim_nome']}</td>
				<td>{$simulado['nota']}</td>
				<td>{$data}</td>
				<td title='{$ranking['posicao']} de $count_simulado' class='texto-verde'>{$ranking['posicao']}</td>
				<td>{$button}</td>
			</tr>";
	}
}

function tabela_deslocamento()
{
	return "<span class='tabela-deslocamento mobile-show'>Deslize para ver mais</span>";
}

function get_filtro_campo_helper($html_answer)
{
	return "<div class='col-sm-1 btn campo-ask'>
				<label style='color: blue; cursor: pointer;'>?</label>
			</div>
			<div class='campo-answer col-sm-12' style='display: none;'>" .
			ui_get_alerta($html_answer, ALERTA_INFO, false) .
			"</div>";
}

function ui_filtro_campo_helper($html_answer)
{
	echo get_filtro_campo_helper($html_answer);
}