<?php
/**
 * Helper para organizar rotinas relacionadas ao Memcached
 * 
 * @package	helpers
 *
 * @author 	João Paulo Ferreira <jpaulofms@gmail.com>
 *
 * @since	M3
 */

/**
 * Busca um cache
 * 
 * @since M3
 * 
 * @param string $cachename Nome do cache
 * 
 * @return object valor ligado ao nome do cache
 */
function get_memcached($cachename)
{
    $memcached = new Memcached();
    $memcached->addServer("127.0.0.1", 11211);
    return $memcached->get($cachename);
}

/**
 * Adiciona/Substitue um cache com o nome informado para o valor informado
 *
 * @param string $cachename Nome do cache
 * @param object $cachevalue Valor do cache
 * @param int $expires Tempo de expiração do cache
 * @since M3
 */
function set_memcached($cachename, $cachevalue, $expires = 0)
{
    $memcached = new Memcached();
    $memcached->addServer("127.0.0.1", 11211);
    $memcached->set($cachename, $cachevalue, $expires);
}

/**
 * Remove o cache com o nome informado
 * 
 * @since M3
 * 
 * @param string $cachename Nome do cache
 */
function remove_memcached($cachename)
{
    $memcached = new Memcached();
    $memcached->addServer("127.0.0.1", 11211);
    $memcached->delete($cachename);
}

/**
 * Remove os caches de questões
 * 
 * @since M3
 * 
 */
function memcached_remover_cache_questoes()
{

    $cacheKeyQuestoesPrefix =  implode(":", [ "expo", "sq"]);

    delete_cache_entries($cacheKeyQuestoesPrefix, [ "questoes", "count" ]);

}
