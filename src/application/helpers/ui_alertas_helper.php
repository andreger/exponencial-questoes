<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Helper para organizar rotinas relacionadas aos alertas da interface
 *
 * @package SQ/Helpers
 *
 * @author André Gervásio <andre@keydea.com.br>
 *
 * @since L1
 */

/**
 * Retorna o HTML de um alerta
 * 
 * @param string $mensagem Texto da mensagem
 * @param int $tipo Tipo da mensagem
 * @param boolean $fechar Exibe ou não botão para fechar
 * 
 * @since L1
 * 
 * @throws InvalidArgumentException
 * 
 * @return string HTML do alerta
 */

function ui_get_alerta($mensagem, $tipo, $fechar = true)
{
    if(!$mensagem) return;// throw new InvalidArgumentException("Argumento 'mensagem' precisa ser informado");
    if(!(filter_var($tipo, FILTER_VALIDATE_INT))) throw new InvalidArgumentException("Argumento 'tipo' precisa ser inteiro");
    
    $CI =& get_instance();
    
    $estilo = '';
    switch ($tipo) {
        case ALERTA_SUCESSO: $estilo = 'alert-success'; break;
        case ALERTA_ERRO: $estilo = 'alert-danger'; break;
        case ALERTA_ATENCAO: $estilo = 'alert-warning'; break;
        case ALERTA_INFO: $estilo = 'alert-info'; break;
    }
    
    $btn_fechar = $fechar ? "<button aria-hidden='true' data-dismiss='alert' class='close' type='button'>×</button>" : "";
    
    $data = [
        "btn_fechar" => $btn_fechar,
        "estilo" => $estilo,
        "mensagem" => $mensagem
    ];
    
    return $CI->load->view("ui/alertas/alerta.php", $data, true);
}

/**
 * Imprime o HTML de um alerta
 *
 * @param string $mensagem Texto da mensagem
 * @param int $tipo Tipo da mensagem
 * @param boolean $fechar Exibe ou não botão para fechar
 *
 * @since L1
 */

function ui_alerta($mensagem, $tipo, $fechar = true) {
    echo ui_get_alerta($mensagem, $tipo, $fechar);
}
