<div class='modal-header'>
	<div class='row resultado-header'>
		<h3 class='pull-left'><?= $simulado['sim_nome'] ?></h3>
		<a id='feedback-detalhado' class='btn btn-primary pull-right' href='#'><strong>Feedback Detalhado</strong></a>
	</div>
</div>
<?= form_open(get_salvar_resultado_url($simulado), "role='form' id='frm-resultado' class='form-horizontal form-resultado'") ?>
	<div class='modal-body'>
		<div class='feedback-resultado'>
			<small>Acertos: </small><span id='feedback-acertos'><?= loading_img() ?></span> /
			<?php if($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']) : ?>
		 		<small>Erros: </small><span id='feedback-erros'><?= loading_img() ?></span> / Total: 
			<?php endif; ?>
			 <?= $total_questoes ?> <span style="margin-right: 20px"></span> 
			 <small>Nota: </small><span id='feedback-nota'><?= loading_img() ?></span>
		</div>
		<div style="overflow-x:auto;">
			<table class='table table-hover table-striped table-responsive'>
				<thead>
					<tr>
						<th>Disciplina</th>
						<th>Quantidade de Questões</th>
						<th>Informe seus acertos</th>
						<?php if($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']) : ?>
							<th>Informe seus erros</th>
						<?php endif; ?>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($disciplinas as $disciplina) : ?>
					<?php
						$selecionado_acertos = get_acertos_da_disciplina($resultados, $disciplina['dis_id']);
						$nome_acertos_id = "sre_acertos_{$disciplina['dis_id']}_{$simulado['sim_id']}";
						if($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']){
							$nome_erros_id = "sre_erros_{$disciplina['dis_id']}_{$simulado['sim_id']}";
							$selecionado_erros = get_erros_da_disciplina($resultados, $disciplina['dis_id'], $simulado['sim_peso_erros']);
						}					
					?>

					<tr>
						<td><?= $disciplina['dis_nome'] ?></td>
						<td><?= $disciplina['qtde'] ?></td>
						<td><?= form_dropdown("{$nome_acertos_id}", get_combo_numerico($disciplina['qtde']), $selecionado_acertos, " class='dis-nota-acertos form-control p-1' ") ?></td>
						<?php if($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']) : ?>
						<td><?= form_dropdown("{$nome_erros_id}", get_combo_numerico($disciplina['qtde']), $selecionado_erros, " class='dis-nota-erros form-control p-1' ") ?></td>
						<?php endif; ?>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class='modal-footer'>
		<button class='btn btn-primary pull-right' type='submit' name='enviar'>Enviar</button>
		<button type='button' class='btn btn-white' data-dismiss='modal'>Fechar</button>
	</div>
</form>

<script>
	function calcular_acertos() {
		var sum = 0;
    
		$('.dis-nota-acertos :selected').each(function () {
			sum += Number($(this).val());
		});

		$('#feedback-acertos').html(sum);
	}

	function calcular_erros(){
		var peso_erros = <?= ($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']) ? $simulado['sim_peso_erros'] : 0  ?>;
		if(peso_erros > 0){
			var sum = 0;
    
			$('.dis-nota-erros :selected').each(function () {
				sum += Number($(this).val());
			});

			$('#feedback-erros').html(sum);
		}
	}

	function calcular_nota() {

		$('#feedback-nota').html("<?= loading_img() ?>");	
		$.post('/questoes/xhr/simulado_nota/<?= $simulado['sim_id'] ?>', $('#frm-resultado').serialize(), function(data) {
			$('#feedback-nota').html(data);
		});
	}

	$('#feedback-detalhado').click(function (event) {
		event.preventDefault();
		var id = <?= $simulado['sim_id'] ?>;
		
		$('#modal-simulados').find('.modal-content').html( "<div class='modal-carregando'><?= loading_img() ?> Carregando...</div>" );

		$.ajax({
			url: '<?= get_ajax_main_resultado_detalhado_url() ?>',
			method: 'POST',
			cache: false,
			data: { id: id }
		})
		.done(function( msg ) {
			$('#modal-simulados').find('.modal-content').html(msg);
			//$('#modal-simulados').show();
		});
	});	

	$('.dis-nota-acertos').change(function () {
		//Comparar o total de questoes
		var erros = $('select[name='+ $(this).attr('name').replace('acertos', 'erros') +']').val();
		var total = $('select[name='+ $(this).attr('name').replace('acertos', 'erros') +'] option:last').val();
		
		if((Number(erros) + Number($(this).val())) > Number(total)){
			alert('O valor excede o número de questões!');
			$(this).val($(this).data('pre'));
			return;
		} 
		$(this).data('pre',	$(this).val());
		calcular_acertos();
		calcular_erros();
		calcular_nota();
	});

	$('.dis-nota-erros').change(function () {
		//Comparar o total de 
		var acertos = $('select[name='+ $(this).attr('name').replace('erros', 'acertos') +']').val();
		var total = $('select[name='+ $(this).attr('name').replace('erros', 'acertos') +'] option:last').val();
		
		if((Number(acertos) + Number($(this).val())) > Number(total)){
			alert('O valor excede o número de questões!');
			$(this).val($(this).data('pre'));
			return;
		} 
		$(this).data('pre',	$(this).val());
		calcular_erros();
		calcular_acertos();
		calcular_nota();
	});

	$(function () {
		$('.dis-nota-acertos').each(function () {
			$(this).data('pre',	$(this).val());
		});
		$('.dis-nota-erros').each(function () {
			$(this).data('pre',	$(this).val());
		});
		calcular_acertos();
		calcular_erros();
		calcular_nota();
	});
					
</script>