<link href='/questoes/assets-admin/css/plugins/c3/c3.min.css' rel='stylesheet'>+
<script src='/questoes/assets-admin/js/plugins/d3/d3.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/c3/c3.min.js'></script>

<div class="col-sm-6">
	<table class="table table-hover no-margins">
		<tr>
			<td>Número total de Questões:</td>
			<td><?= $caderno['total_questoes'] ?></td>
		</tr>
		
		<tr>
			<td>Questões Resolvidas:</td>
			<td><?= $caderno['total_questoes_respondidas'] ?></td>
		</tr>

		<tr>
			<td>Acertos:</td>
			<td><?= $caderno['questoes_certas'] ?></td>
		</tr>

		<tr>
			<td>Erros:</td>
			<td><?= $caderno['questoes_erradas'] ?></td>
		</tr>

		<tr>
			<td>Questões em branco:</td>
			<td><?= $caderno['total_questoes_nao_respondidas'] ?></td>
		</tr>

		<tr>
			<td>Questões anuladas:</td>
			<td><?= $caderno['total_questoes_anuladas'] ?></td>
		</tr>

		<tr>
			<td>Tempo total gasto:</td>
			<td><?= $caderno['tempo_gasto'] ?></td>
		</tr>

		<tr>
			<td>Tempo médio por questão:</td>
			<td><?= $caderno['tempo_medio_questao'] ?></td>
		</tr>

		<tr>
			<td>Tempo médio por questão resolvida:</td>
			<td><?= $caderno['tempo_medio_questao_resolvida'] ?></td>
		</tr>

		<tr>
			<td>Data de Criação:</td>
			<td><?= converter_para_ddmmyyyy($caderno['cad_data_criacao']) ?></td>
		</tr>

	</table>
</div>
<div class="col-sm-6">
	<div id="completo"></div>
	<div id="simples" style="display: none"></div>

	<?php if($caderno['total_questoes_respondidas'] > 0) : ?>
	<div style="margin-top: 20px; text-align: center">
		<input id="resolvidas" type="checkbox"> Apenas questões resolvidas
	</div>
	<?php endif; ?>
	
</div>
<div style="clear: both"></div>

<script>
var completo = c3.generate({
	bindto: '#completo',
    data: {
        // iris data from R
        columns: [
            ['Acertos', <?= $caderno['questoes_certas'] ?>],
            ['Erros', <?= $caderno['questoes_erradas'] ?>],
            ['Em branco', <?= $caderno['total_questoes_nao_respondidas'] ?>],
            ['Anuladas', <?= $caderno['total_questoes_anuladas'] ?>]
        ],
        colors: {
           	'Acertos': '#01a527',
            'Erros': '#ff0000',
            'Em branco': '#eee',
            'Anuladas': '#aaa',
        },
        type : 'pie',
    }
});

var simples = c3.generate({
	bindto: '#simples',
    data: {
        // iris data from R
        columns: [
            ['Acertos', <?= $caderno['questoes_certas'] ?>],
            ['Erros', <?= $caderno['questoes_erradas'] ?>],
        ],
        colors: {
           	'Acertos': '#01a527',
            'Erros': '#ff0000',
        },
        type : 'pie',
    }
});

$(function() {
	$('#resolvidas').click(function() {
		if($(this).is(':checked')) {
			$('#completo').hide();
			$('#simples').show();
		}
		else {
			$('#completo').show();
			$('#simples').hide();
		}
	});
});
</script>