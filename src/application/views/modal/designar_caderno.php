
<div class="panel-heading">
	<div class="panel-options">
		<ul class="nav nav-tabs">
			<li class="active"><a href="#tab-1-<?= $caderno_id ?>" data-toggle="tab">Designar Aluno</a></li>

			<?php if(!$edicao_massa) : ?>
			<li class=""><a id="<?= $caderno_id ?>" href="#tab-2-<?= $caderno_id ?>" data-toggle="tab" class="designados">Alunos Designados</a></li>
			<?php endif ?>
		</ul>
	</div>
</div>

<div class="panel-body">
	<div class="tab-content">
		
		<div class="tab-pane active" id="tab-1-<?= $caderno_id ?>">
			<?= form_open(get_designar_caderno_url($caderno_id), 'role="form" id="designar-form" ') ?>	
			<div>
				<div class='form-group'>
					<label for='usu_ids'>Selecionar alunos para quem designar este caderno</label>		
					<div style="padding-bottom: 8px;">
						<label class="checkbox-inline"><?php echo form_checkbox('filtro_aluno[]', ALUNO_PODIO, in_array(ALUNO_PODIO, $filtro_aluno), 'id="check_podio" class="check-aluno"');?> Coaching Individual </label>
						<label class="checkbox-inline"><?php echo form_checkbox('filtro_aluno[]', ALUNO_COACHING, in_array(ALUNO_COACHING, $filtro_aluno), 'id="check_coaching" class="check-aluno"');?> Turma de Coaching </label>
					</div>
					<div id="select-alunos">
						<?= form_multiselect("usu_ids[]", $alunos, '', "id='usu_ids' class='multiselect'") ?>
					</div>
				</div>

				<?php if(is_administrador()): ?>
				<div class='form-group'>
					<label for='prof_ids'>Selecionar professores para quem designar este caderno</label>
					<?= form_multiselect("prof_ids[]", $professores, '', "id='prof_ids' class='multiselect'") ?>
				</div>		
				<?php endif ?>

			</div>

			<div class='modal-footer'>
				<img src='<?= get_tema_image_url("loader.gif") ?>' style='display:none' id='loader'>
				<button type='submit' class='btn btn-primary btn-designar pull-right'>Designar</button>
				<button type='button' class='btn btn-white pull-left' data-dismiss='modal'>Fechar</button>
			</div>
		</form>	
		</div>

		<div class="tab-pane" id="tab-2-<?= $caderno_id ?>">
			<div id="usuarios_designados-<?= $caderno_id ?>"></div>
		</div>
	</div>
</div>

<script>	

$('.check-aluno').change(function(){

	var filtro_aluno = '';

	$('input[name="filtro_aluno[]"]:checked').each(function() {
		filtro_aluno += this.value + ',';
	});

	if(filtro_aluno){
		filtro_aluno = filtro_aluno.substring(0, filtro_aluno.length-1);
	}

	$("#usu_ids").prop('disabled', true);
			
	$.ajax({
		url: "<?php echo get_ajax_cadernos_alunos_acompanhamento_url(PAGE_CADERNOS);?>",
		method: "POST",
		cache: false,
		data: { 
			filtro_aluno: filtro_aluno
		}
	})
	.done(function( msg ) {
		
		$("#select-alunos").html(msg);
		$('#usu_ids').chosen({
			no_results_text: 'Nenhum aluno com:',
			placeholder_text_multiple: 'Selecione alguns alunos...',
			width: '100%'			
		});
	});

});

$('#usu_ids').chosen({
	no_results_text: 'Nenhum aluno com:',
	placeholder_text_multiple: 'Selecione alguns alunos...',
	width: '100%'			
});
$('#prof_ids').chosen({
	no_results_text: 'Nenhum professor com:',
	placeholder_text_multiple: 'Selecione alguns professores...',
	width: '100%'			
});
$('.btn-designar').click(function (event) {
	$('#loader').show();
});

$(function() {
	$('.designados').click(function() {
		var caderno_id = $(this).attr('id');

		$.get('/questoes/xhr/usuarios_designados_caderno/' + caderno_id, function(data) {
			$('#usuarios_designados-'+caderno_id).html(data);

		});
	});

});
</script>