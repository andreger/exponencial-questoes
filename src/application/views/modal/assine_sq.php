<div id="assine-sq" class="modal-dialog">
	<div class="modal-content modal-form-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
		</div>
		<div class="modal-body">
			<?= get_tema_image_tag('logo-sq-colorido.png', null, null, 'Sistema de Questões - SQ') ?>
			<h2>Você ainda não é assinante!</h2>
			<h3>Quer ter acesso completo?</h3>
			<h5>Por menos de 40 centavos por dia você acessa os comentários,<br>além de muitas outras vantagens.</h5>
			<a href="/sistema-de-questoes#planos"><?= get_tema_image_tag('confira-os-planos.png', '', '', 'Sistema de Questões - Planos', "max-width-100p") ?></a>
			</div>
		</div>
	</div>
</div>