<script>

function mostrar_filtro_p1()
{
	$('#p2').hide();
	$('#p1').show();
	$('#back-btn').hide();
	$('#cancel-btn').show();
}

function mostrar_filtro_p2()
{
	$('#p1').hide();
	$('#p2').show();
	$('#cancel-btn').hide();
	$('#back-btn').show();
}

$(function() {
	$(document).ready(function(){

		$.fn.datepicker.dates['pt'] = {
			format: 'dd/mm/yyyy',
			days: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			daysMin: ['D','S','T','Q','Q','S','S','D'],
			daysShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			months: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthsShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			today: 'Hoje',
			weekStart: 0,
			clear: "Limpar"
		};

		$(document).on('click', '#data_inicio', function () { 
	        var me = $("#data_inicio");   
	        me.datepicker({
	            showOn: 'focus',
	            altFormat: "mm/dd/yy",
	            dateFormat: "mm/dd/yy",
				language: 'pt',
				clearBtn: true
	        }).focus();
	    }).on('focus', '#data_inicio', function () {
			var me = $("#data_inicio");
			me.mask('99/99/9999');
	    });

		$(document).on('click', '#data_fim', function () { 
	        var me = $("#data_fim");   
	        me.datepicker({
	            showOn: 'focus',
	            altFormat: "mm/dd/yy",
	            dateFormat: "mm/dd/yy",
	            language: 'pt',
				clearBtn: true
	        }).focus();
	    }).on('focus', '#data_fim', function () {
	        var me = $("#data_fim");
	        me.mask('99/99/9999');
	    });
	});

	$('#filtro-submit').click(function(e) {
		$('#questoes-principal').hide();
		$('#carregando-questoes').show();
		$('#cancel-btn').click();
	});
	
	$('.modal').on('hidden.bs.modal', function () {
    	$('#p2').hide();
		$('#p1').show('slow');
	});
	
	$('#cancel-btn').click(function(e) {
		e.preventDefault();
		mostrar_filtro_p1();
	});

	$('#back-btn').click(function(e) {
		e.preventDefault();
		mostrar_filtro_p1();
	});

	$("#limpar-botao-modal").click(function() {
		$('#que_id').val("");
		$('#que_qcon_id').val("");
		$('#palavra_chave').val("");
		$('#data_inicio').val("");
		$('#data_fim').val("");
		$('#busca').val("");
		$('.multiselect').select2("val", "");
		$('.i-checks input:radio').iCheck('uncheck');
		$('.i-checks input:checkbox').iCheck('uncheck');
	});

	$('#cancelar').click(function(e) {
		e.preventDefault();
		mostrar_filtro_p1();
	});

	$('#adicionar').click(function(e) {
		e.preventDefault();	

		var ids = $("#p2-corpo").jstree("get_checked",null,true);

		$('#'+ass_ids_name).val(ids).trigger('change');

		mostrar_filtro_p1();
	});

	$('#expand-all').click(function(e){
		$('#p2-corpo').jstree('open_all');
	});
	
	$('#collapse-all').click(function(e){
		$('#p2-corpo').jstree('close_all');
	});

	$('#arvore-btn').click(function(e) {

		e.preventDefault();
		mostrar_filtro_p2();
				
		var ids_a = $("#"+dis_ids_name).val();
		 
		var qs = "q=1";

		if($.isArray(ids_a)) {
			$.each(ids_a, function(i, value) {
				qs += '&dis_ids[]=' + value; 
			});
		}
		else {
			qs += '&dis_ids[]=' + ids_a; 
		}

		if(ids_a == null) {
			$('#p2-corpo').html('É necessário selecionar pelo menos uma disciplina.');
		}
		else {
			
			$('#p2-corpo').jstree('destroy');
			
			$('#p2-corpo').jstree( {
			'core' : {
				  'data' : {
				    'url' : '/questoes/xhr/get_arvore?' + qs,
				    'data' : function (node) {
				      return { 'id' : node.id }
				    }
				  }
				},
			'plugins' : [ 'types', 'dnd', 'checkbox' ]
		
			}).bind('loaded.jstree', function(e, data){ 
				
				var ass_ids = $('#'+ass_ids_name).val();
				
				if(ass_ids){
					$.each(ass_ids,function(index,value){
						$('#p2-corpo').jstree('check_node', '#'+value);
					});
				}
			});		
		}
	});

	$('.campo-ask').click(function(e){
		var answer = $(this).next('.campo-answer');
		if(answer.is(":hidden")){
			answer.show();
		}else{
			answer.hide();
		}
	});

	$('#modal-form-filter').on('shown.bs.modal', function carregar_combo_ajax(e){

		$.get('/questoes/xhr/listar_dados_combo_filtro', function(data){

			var result = JSON.parse(data);

			adicionar_itens($('#dis_ids'), result.dis_ids);
			adicionar_itens($('#ban_ids'), result.ban_ids);
			adicionar_itens($('#car_ids'), result.car_ids);
			adicionar_itens($('#org_ids'), result.org_ids);

			$('#modal-form-filter').off('shown.bs.modal', carregar_combo_ajax);			
		});

	});

	$('#ass_ids').html("<option value='' disabled >Carregando...</option>");
	$('#dis_ids').html("<option value='' disabled >Carregando...</option>");
	$('#car_ids').html("<option value='' disabled >Carregando...</option>");
	$('#ban_ids').html("<option value='' disabled >Carregando...</option>");
	$('#org_ids').html("<option value='' disabled >Carregando...</option>");

});

var combos_info = ['dis_ids', 'ban_ids', 'org_ids', 'car_ids', 'ass_ids'];

var combos_selecionados = [
	[ <?php echo implode(",", $_GET['dis_ids']) ?> ],
	[ <?php echo implode(",", $_GET['ban_ids']) ?> ],
	[ <?php echo implode(",", $_GET['org_ids']) ?> ],
	[ <?php echo implode(",", $_GET['car_ids']) ?> ],
	[ <?php echo implode(",", $_GET['ass_ids']) ?> ]
];

function adicionar_itens(el, itens){
	adicionar_itens(el, itens, null);
}

function adicionar_itens(el, itens, valoresAtuais){
	
	$(el).html('');

	$.each(itens, function(i, val){
		var selected = combos_selecionados[combos_info.indexOf($(el).attr('id'))].includes(val.id) 
			|| ( valoresAtuais && valoresAtuais.includes( val.id.toString() ) );
		var option = new Option(val.text, val.id, false, selected);
    	$(el).append(option);
	});

	$(el).trigger('change');

}

</script>