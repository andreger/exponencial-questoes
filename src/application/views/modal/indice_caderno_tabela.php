<?= ui_alerta("A quantidade de questões apontadas aqui pode ser maior do que o total pois uma questão pode levar mais de uma classificação de assunto", ALERTA_INFO, FALSE) ?>
<a class="btn btn-outline btn-default" href="#" onclick="$('#tabela-indice').treetable('expandAll'); return false;"><i class="fa fa-plus"></i> Expandir Tudo</a>
<a class="btn btn-outline btn-default" href="#" onclick="$('#tabela-indice').treetable('collapseAll'); return false;"><i class="fa fa-minus"></i> Recolher Tudo</a>
<a class="btn btn-outline btn-default" href="#" onclick="selectElementContents( document.getElementById('tabela-indice') );"><i class="fa fa-copy"></i> Copiar</a>

<table id="tabela-indice" class="controller" style="width: 100%">
	<tr>
		<th>Disciplina/Assunto</th>
		<th class="text-right">Quantidade</th>
	</tr>

	<?php foreach ($disciplinas as $disciplina) : ?>
	<?php $id = "dis_" . $disciplina["dis_id"]; ?>
    <tr data-tt-id="<?= $id ?>">
        <?php if($possui_filtro): ?>
            <td><?= form_checkbox('dis_ids', $disciplina['dis_id'], FALSE, "class='indice-disciplina' data-root_dis_id='{$disciplina['dis_id']}'") ?><?= $disciplina["dis_nome"] ?></td>
        <?php else: ?>
            <td><?= $disciplina["dis_nome"] ?></td>
        <?php endif; ?>
		<td class="text-right" nowrap><?= $disciplina["qtde"] ?> (<?= valor_percentual($disciplina["qtde"], $total) ?>)</td>
	</tr>
	
	<?= get_arvore_indice_html($disciplina["assuntos"], $id, $total, $disciplina["dis_id"], $possui_filtro, $assuntos_selecionados) ?>

	<?php endforeach; ?>
</table>

<a class="btn btn-default" href="#" id="filtrar-indice">Filtrar</a>
<a class="btn btn-default" href="#" id="limpar-filtro-indice">Limpar Seleção</a>

<script>
$('#tabela-indice').treetable({ expandable: true });

function selectElementContents(el) {
    var body = document.body, range, sel;
    if (document.createRange && window.getSelection) {
        range = document.createRange();
        sel = window.getSelection();
        sel.removeAllRanges();
        try {
            range.selectNodeContents(el);
            sel.addRange(range);
        } catch (e) {
            range.selectNode(el);
            sel.addRange(range);
        }
        document.execCommand("copy");

    } else if (body.createTextRange) {
        range = body.createTextRange();
        range.moveToElementText(el);
        range.select();
        range.execCommand("Copy");
    }
}

$('.indice-disciplina').change(function(e){
    e.preventDefault();
    var dis_id = $(this).val();
    $('#tabela-indice [data-dis_id="'+ dis_id +'"]').prop('checked', $(this).prop('checked'));
});

$('.indice-assunto').change(function(e){
    e.preventDefault();

    var valor = $(this).prop('checked');
    var pai_id = $(this).val();

    //Marca filhos
    $('#tabela-indice [data-pai_id="'+ pai_id +'"]').prop('checked', valor);
    
    //Marca pai
    if($(this).data('pai_id')){

        var total_filhos = $('#tabela-indice [data-pai_id="'+ $(this).data('pai_id') +'"]').size();
        var total_selecionado = 0;
        $('#tabela-indice [data-pai_id="'+ $(this).data('pai_id') +'"]').each(function(index, elem){
            if($(elem).prop('checked')){
                total_selecionado = total_selecionado + 1;
            }
        });        
        
        $('#tabela-indice [data-id="'+ $(this).data('pai_id') +'"]').prop('checked', (total_selecionado == total_filhos));        
    }

    var total_dis_filhos = $('#tabela-indice [data-dis_id="'+ $(this).data('dis_id') +'"]').size();
    var total_dis_selecionado = 0;
    $('#tabela-indice [data-dis_id="'+ $(this).data('dis_id') +'"]').each(function(index, elem){
        if($(elem).prop('checked')){
            total_dis_selecionado = total_dis_selecionado + 1;
        }
    });
    
    $('#tabela-indice [data-root_dis_id="'+ $(this).data('dis_id') +'"]').prop('checked', (total_dis_filhos == total_dis_selecionado) );

});

$('#filtrar-indice').click(function(e){
    e.preventDefault();

    var dis_ids = [];
    var ass_ids = $("#tabela-indice input:checkbox[name=ass_ids]:checked").map(function(){
        var dis_id = parseInt($(this).data('dis_id'));
        if(dis_ids.indexOf(dis_id) == -1){
            dis_ids.push(dis_id);
        }
        return $(this).val();
    }).get();

    $("#tabela-indice input:checkbox[name=dis_ids]:checked").each(function(index, elem){
        var dis_id = parseInt($(elem).val());
        if(dis_ids.indexOf(dis_id) == -1){
            dis_ids.push(dis_id);
        }
    });

    $('#form-indice-caderno input[name=dis_ids]').val(dis_ids);
    $('#form-indice-caderno input[name=ass_ids]').val(ass_ids);
    //alert($('#form-indice-caderno input[name=dis_ids]').val());
    //alert($('#form-indice-caderno input[name=ass_ids]').val());
    $('#questoes-principal').hide();
	$('#carregando-questoes').show();
	$('#cancel-btn-indice').click();
    $('#form-indice-caderno').submit();
});

$('#limpar-filtro-indice').click(function(e){
    e.preventDefault();
    $('#tabela-indice input[type=checkbox]').prop('checked', false);
});

$(document).ready(function(){

    $("#tabela-indice input:checkbox[name=dis_ids]").each(function(index, elem){
        
        var total_dis_filhos = $('#tabela-indice [data-dis_id="'+ $(elem).val() +'"]').size();
        var total_dis_selecionado = 0;
        $('#tabela-indice [data-dis_id="'+ $(elem).val() +'"]').each(function(i, el){
            if($(el).prop('checked')){
                total_dis_selecionado = total_dis_selecionado + 1;
            }
        });

        if(total_dis_filhos == total_dis_selecionado){
            $(elem).prop('checked', true);
        }

    });

});

</script>