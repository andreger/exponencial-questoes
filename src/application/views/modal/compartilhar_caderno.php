<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Compartilhar Caderno</h4>
		</div>
		<div class="modal-body">
		
			<div class="panel blank-panel">
				<div class="panel-body">
					<div class="panel-carregando">Compartilhe o caderno usando o link abaixo:</div>
					<div class="panel-conteudo">
						<div class="form-group">
							<div class="col-sm-12">
								<?php echo form_input('', '','id="url_caderno_compartilhado" readonly class="form-control m-b"'); ?>
							</div>
						</div>

						<div>
							<input type="button" class="btn btn-primary" onclick="copy_to_clipboard()" value="Copiar link">
							<input type="button" class="btn btn-primary" onclick="$('#emailbox').toggle()" value="Enviar por E-mail">
							<input type="button" class="btn btn-outline btn-default" onclick="$('#modal-compartilhar-caderno').modal('hide')" value="Fechar">
						</div>

						<div id="emailbox" style="display: none">
							<form method="post" id="enviar_email_form">
								<div class="form-group">
									<div class="col-sm-12">
										E-mails:
										<input type="text" name="emails" class="form-control m-b" placeholder="Escreva os e-mails separados por vírgula">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										Título:
										<textarea name="titulo" id="titulo" class="form-control m-b">
											
										</textarea>
										<input type="hidden" name="mensagem_padrao" id="mensagem_padrao">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										Mensagem:
										<textarea name="mensagem" id="mensagem" class="form-control m-b"></textarea>


										<input type="submit" name="enviar_email" value="Enviar" class="btn btn-primary">
									</div>
								</div>								
							</form>
						</div>
					</div>
                </div>
            </div>      
		</div>
	</div>
</div>
