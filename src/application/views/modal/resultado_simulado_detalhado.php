<div class='modal-header'>
	<div class='row resultado-header'>
		<h3 class='pull-left'><?= $simulado['sim_nome'] ?></h3>
		<a id='feedback' class='btn btn-primary pull-right' href='#'><strong>Feedback</strong></a>
	</div>
</div>
	
<?=
form_open(get_salvar_resultado_detalhado_url($simulado), "role='form' class='form-horizontal form-resultado' id='frm-resultado-d'") ?>
	<div class='modal-body'>
		<div class='feedback-resultado'>
			<small>Acertos: </small><span id='feedback-d-acertos'><?= loading_img() ?></span> / 
			<?php if($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']) : ?>
				<small>Erros: </small><span id='feedback-d-erros'><?= loading_img() ?></span> / Total: 
			<?php endif; ?>
			<?= $total_questoes ?> <span style="margin-right: 20px"></span> 
			<small>Nota: </small><span id='feedback-d-nota'><?= loading_img() ?></span>
		</div>
		<table class='table table-hover table-striped table-responsive'>
			<thead>
				<tr>
					<th class='col-sm-6'>Questão</th>
					<th class='col-sm-6'>Opção Selecionada</th>
				</tr>
			</thead>
			<tbody>


			<?php foreach ($questoes as $key => $questao) : ?>
				<?php
				$opcoes = $this->questao_model->listar_opcoes($questao['que_id']);

				if($questao['que_tipo'] == CERTO_ERRADO || (($questao['que_tipo'] == MULTIPLA_ESCOLHA || $questao['que_tipo'] == MULTIPLA_ESCOLHA_5) && count($opcoes) > 0)) :
					$numero_questao = $key +1; ?>
					<tr>
						<td><?= $numero_questao ?></td>
						<td>
							<div class='radio i-checks form-group'>
							<?php $resposta = $this->questao_model->get_resposta_usuario(get_current_user_id(), $questao['que_id'], $simulado_id); ?>

							<?php if($questao['que_tipo'] == MULTIPLA_ESCOLHA || $questao['que_tipo'] == MULTIPLA_ESCOLHA_5) :
							
								
								foreach ($opcoes as $opcao) : 
								$selecionado = ($resposta && $resposta['qre_selecao'] == $opcao['qop_ordem']) ? TRUE : FALSE; ?>
								
									<label for='qop_<?= $questao['que_id'] . $opcoes_label[$opcao['qop_ordem']] ?>' class='radio-inline' style='margin-right:20px'>
										<?= form_radio("opcao_questao[{$questao['que_id']}]", $opcao['qop_ordem'],
										$selecionado, " class='dis-nota' id='qop_{$questao['que_id']}{$opcao['qop_ordem']}'") ?>
										<?= $opcoes_label[$opcao['qop_ordem']] ?></label>

								<?php endforeach ?>
							<?php else : ?>
								<?php if($questao['que_tipo'] == CERTO_ERRADO) : ?>
							
								<label for='qop_<?= $questao['que_id'] ?>_certo' class='radio-inline'>
									<?= form_radio("opcao_questao[{$questao['que_id']}]",
										CERTO, $resposta && $resposta['qre_selecao'] == CERTO,
										'class="dis-nota" id="qop' . $questao['que_id'] . '_certo"') ?>
									<i class='fa fa-check'></i></label>
								<label for='qop_<?= $questao['que_id'] ?>_errado' class='radio-inline'>
									<?= form_radio("opcao_questao[{$questao['que_id']}]", 
										ERRADO, $resposta && $resposta['qre_selecao'] == ERRADO, 
										'class="dis-nota" id="qop' . $questao['que_id'] . '_errado"') ?>
										<i class='fa fa-times'></i></label>
								<?php endif ?>
							<?php endif ?>
							<?php if($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']) : ?>
								 <a href="#" class="que-branco" onclick="questao_em_branco('<?= $questao['que_id'] ?>');" >em branco</a>
							<?php endif; ?>
							</div>
						</td>
					</tr>
				<?php endif ?>
			<?php endforeach ?>
			</tbody>
		</table>
		</div>
		<div class='modal-footer'>
			<button class='btn btn-primary pull-right' type='submit' name='enviar'>Enviar</button>
			<button type='button' class='btn btn-white' data-dismiss='modal'>Fechar</button>
		</div>
	</div>
</form>
	
<script>
	function calcular_acertos() {
		$('#feedback-d-acertos').html("<?= loading_img() ?>");		
    	$.post('/questoes/xhr/contar_acertos/<?= $simulado_id ?>', $('#frm-resultado-d').serialize(), function(data) { 
    		$('#feedback-d-acertos').html(data);
    	});
	}

	function calcular_erros(){
		var peso_erros = <?= ($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']) ? $simulado['sim_peso_erros'] : 0  ?>;
		if(peso_erros > 0){
			$('#feedback-d-erros').html("<?= loading_img() ?>");
			$.post('/questoes/xhr/contar_erros/<?= $simulado_id ?>', $('#frm-resultado-d').serialize(), function(data) { 
				$('#feedback-d-erros').html(data);
			});
		}
	}

	function calcular_nota() {
		$('#feedback-d-nota').html("<?= loading_img() ?>");	
		$.post('/questoes/xhr/simulado_nota_detalhe/<?= $simulado['sim_id'] ?>', $('#frm-resultado-d').serialize(), function(data) {
			$('#feedback-d-nota').html(data);
		});
	}

	function questao_em_branco(que_id){
		$("input[name='opcao_questao["+que_id+"]'").prop('checked', false);
	}

	$('#feedback').click(function (event) {
		event.preventDefault();
		var id = <?= $simulado['sim_id'] ?>;
		$('#modal-simulados').find('.modal-content').html("<div class='modal-carregando'><?= loading_img() ?> Carregando...</div>");
		//$('#modal-simulados').hide();
		$.ajax({
			url: '<?= get_ajax_main_resultado_url($simulado_id) ?>',
			method: 'POST',
			cache: false,
			data: { id: id }
		})
		.done(function( msg ) { console.log(msg);
			$('#modal-simulados').find('.modal-content').html(msg);
			//$('#modal-simulados').show();
		});
	});

	$('.dis-nota').change(function () {
		calcular_acertos();
		calcular_erros()
		calcular_nota();
	});

	$('.que-branco').click(function () {
		calcular_acertos();
		calcular_erros()
		calcular_nota();
	});

	$(function () {
		calcular_acertos();
		calcular_erros()
		calcular_nota();
	});
</script>