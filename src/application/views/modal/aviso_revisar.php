<div id="modal-revisar" class="modal fade" aria-hidden="true" data-sim_id="">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				&nbsp;
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div>
					Você só poderá revisar o simulado após resolvê-lo. Você pode resolvê-lo online clicando no botão <strong>Resolver Agora</strong> ou, caso deseje resolver o simulado offline, pressione o botão <strong>Informar Resultado</strong> e informe seu desempenho na prova. Após isto você poderá revisar o simulado detalhadamente e verificar os comentários de cada questão.
				</div>

				<div class="text-align-center" style="margin-top: 30px;">
					<a id="revisar-resolver-agora" class="btn btn-primary btn-rounded-expo text-align-center" href="#"><i class="fa fa-pencil"></i> Resolver Agora</a>
											
					<a id="revisar-informar" class="btn btn-azul btn-rounded-expo text-align-center" href="#"><i class="fa fa-book"></i> Informar Resultado</a>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(function() {
	$("#revisar-resolver-agora").click(function() {
		var id = $("#modal-revisar").data("sim_id");
		$("#modal-revisar").modal("hide");
		$("#simulado-" + id + " .btn-resolver")[0].click();
	});

	$("#revisar-informar").click(function() {
		var id = $("#modal-revisar").data("sim_id");
		$("#modal-revisar").modal("hide");
		$("#simulado-" + id + " .btn-informar")[0].click();
	});
});
</script>