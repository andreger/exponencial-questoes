<?php
/* === inclusão de classes do kcore === */
KLoader::helper("StringHelper");
?>

<div class="row text-center">
	<?php admin_cabecalho_pagina ("Cadernos Exponencial"); ?>
</div>

<div class="panel-heading">
	<?php $this->load->view('templates/mensagem') ?>
</div>
<div class="listar-cadernos">
	<form method="post" action="<?= get_cadernos_exponencial_url(); ?>">
		<div class="row">
			<div style="margin-bottom: 5px">
				<input id="search" class="search_filter form-control" type="text" placeholder="Digite para buscar nos cadernos..." name="search_filter" value="<?= $search_filter ?>"/>
			</div>
		</div>
		<div class="row filtro-simulado">
			<label class="control-label">Listar </label>
			<label class="radio-inline i-checks"><?php echo form_radio('radio_caderno', TODAS, $radio_caderno == TODAS, 'id="radio_todos"');?> Todos </label>
			<label class="radio-inline i-checks"><?php echo form_radio('radio_caderno', ROTA_EXPONENCIAL, $radio_caderno == ROTA_EXPONENCIAL, 'id="radio_rota_exponencial"');?> Rota Exponencial </label>
			<label class="radio-inline i-checks"><?php echo form_radio('radio_caderno', QUE_ADQUIRI, $radio_caderno == QUE_ADQUIRI, 'id="radio_adquiri"');?> Meus cadernos </label>
			<label class="radio-inline i-checks"><?php echo form_radio('radio_caderno', QUE_NAO_ADQUIRI, $radio_caderno == QUE_NAO_ADQUIRI, 'id="radio_nao_adquiri"');?> Cadernos à venda </label>
			<label class="radio-inline i-checks"><?php echo form_radio('radio_caderno', GRATUITOS, $radio_caderno == GRATUITOS, 'id="radio_gratuito"');?> Grátis </label>
		</div>
		<div class="row">
				<?php echo form_dropdown('ordenacao', $ordenacoes, $ordenacao_selecionada, 'id="ordenacao" class="form-control m-b"'); ?>
		</div>
		<div class="row">
			<?php echo admin_botao_submit('Filtrar', 'filtrar'); ?>
			<?php echo admin_botao_submit('Limpar', 'limpar'); ?>
		</div>
	</form>
	<div id="corpo-cadernos" class="row">
		<div id="lista-cadernos" class="col-md-12" style="margin-top: 20px">
			<div class="row">
				<?= StringHelper::pluralizar($total, "1 caderno encontrado", "{$total} cadernos encontrados", "Nenhum caderno encontrado") ?>
			</div>

			<div id="cadernos">
				<?php foreach ($cadernos as $caderno) :
					//$produto = get_produto_by_id($caderno['produto_id']);
					$produto = wc_get_product($caderno['produto_id']); 

					if(!$produto) {
						log_sq("error", "Cadernos Exponencial: Nao foi possivel achar produto com ID {$caderno['produto_id']} que esta associado ao caderno {$caderno['cad_id']}");
						continue;
					}

					$comprou = $caderno['comprou_produto'];
					$gratuito = ($comprou || $produto->get_price() > 0) ? "" : "caderno-gratis";
				?>
				<div class="col-12 col-sm-6 col-lg-4 caderno-height">
					<div class="caderno-widget <?= $gratuito; ?> col-md-12">
						<div id="caderno-<?= $caderno['cad_id']; ?>" class="col-xs-12 caderno-margin-top">
							<div class="row">
								<div id="cdn-col" class="caderno-img col-xs-12 col-sm-3 col-md-3 col-lg-3">
										<?php echo get_the_post_thumbnail( $produto->post->ID, "thumbnail", "'".substr($caderno['cad_nome'],0,70)."'" ); ?>
								</div>
								<div class="padding-se col-xs-9 col-sm-6 col-md-7 col-lg-6">
									<div class="caderno-info-row">
										<?php if($gratuito) : ?>
											<div class="texto-gratis">CADERNO GRÁTIS</div>
										<?php endif; ?>
										<div class="nome_caderno" title="<?= $caderno['cad_nome']?>">
											<?= $caderno['cad_nome']?>
										</div>

									</div>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-2 col-lg-3 text-right total-questoes-caderno" data-toggle="tooltip" title="Quantidade total de questões, considerando todas as provas.">
									<?= is_null($caderno['total_questoes']) ? 0 : $caderno['total_questoes']; ?>
								</div>
							</div>
							<div class="caderno-botoes col-xs-12">
								<a data-toggle="modal" class="caderno-info" href="#modal-info-caderno" onclick="carrega_modal_info_caderno(<?php echo $caderno['cad_id'] ?>)"><?= get_tema_image_tag('sq-info.png', 22, 22) ?></a>
								<?php if($comprou) : ?>
									<a class="btn btn-primary btn-rounded-expo btn-block btn-caderno text-align-center" style="padding: 17pt;" href="<?= get_resolver_caderno_url(get_caderno_por_usuario_caderno_referencia(get_current_user_id(), $caderno['cad_id'])['cad_id']) . '?zf=1' ?>">ACESSAR</a>
								<?php elseif(is_administrador() || $caderno['usu_id'] == get_current_user_id()): ?>
									<a class="btn btn-primary btn-rounded-expo btn-block btn-caderno text-align-center" style="padding: 17pt;" href="<?= get_resolver_caderno_url($caderno['cad_id']) . '?zf=1' ?>">ACESSAR</a>
								<?php else: ?>

									<?php
									if(is_visitante()){
										$url = "#";
										$modal = "data-toggle='modal' data-target='#login-modal' data-redirect='" . urlencode($produto->get_permalink() . "?add-to-cart=" . $produto->id) ."' ";
									}else{
										$url = $produto->get_permalink() . "?add-to-cart=" . $produto->id;
									}
								?>

									<a class='btn btn-primary btn-rounded-expo btn-block btn-caderno text-align-center' href="<?= $url ?>" <?= $modal ?>>
										<?php $preco = is_pacote($produto) ? $produto->max_price : $produto->get_price(); ?>
										<?php $sem_desconto = $produto->get_regular_price(); ?>

										<div class="pxs55 col-md-12" style="padding: 0 5px">

											<?php if($sem_desconto > $preco) : ?>
											<span class="curso-sem-desconto">De: <?= moeda($sem_desconto) ?></span>
											<?php endif ?>

											<?php if($preco > 0) : ?>
												<div class="curso-preco price-cut">De: <?= moeda($preco) ?></div>
											<?php else : ?>
												<div class="curso-preco">
													<img src="<?= get_tema_image_url('icone-aulagratis.png') ?>" width='20' style="position: relative; left: -6px;"> GRÁTIS</div>
											<?php endif ?>

											<?php if($preco > 0) : ?>
											<div class="curso-parcelas">
												Por: 10x de <?= moeda($preco / 10) ?> sem juros
											</div>
											<?php endif ?>

											<div class="curso-acao">
												<?php if($preco > 0) : ?>
													<img src="<?= get_tema_image_url('icone-compra.png') ?>" width='20' style="position: relative; left: -6px;"> Comprar
												<?php else : ?>
													Adquirir
												<?php endif; ?>
											</div>
										</div>
										<div style="clear:both" class="clear"></div>
									</a>
								<?php endif; ?>
							</div>
						</div>
							<div style="clear:both" class="clear"></div>
					</div>
				</div>
				<?php endforeach; ?>
				<div id="modal-info-caderno" class="modal fade" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class='modal-header'>
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<div class='row resultado-header'>
									<h3 class='pull-left'>Informações</h3>
								</div>
							</div>
							<div class='modal-body'></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row paginacao">
		<?php echo $links; ?>
	</div>
</div>
<script>
	function carrega_modal_info_caderno(caderno_id){
		var elem = $('#modal-info-caderno .modal-body');
		elem.html("Carregando...");

		$.get('/questoes/cadernos/modal_info_caderno_exponencial/'+caderno_id, function(data) {
			elem.html(data);
		});
	}
</script>