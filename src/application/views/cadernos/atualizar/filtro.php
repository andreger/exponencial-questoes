<div class="row">
    <div class="col-md-12"><h3>Filtros</h3></div>
    <div class="col-md-12">
        <div class="ibox-content">
            <?php $this->view("questoes/filtro/filtro_campos", ['is_modal' => true]); ?>
        </div>
        
        <?php $this->view("questoes/filtro/arvore_assuntos", ['is_modal' => true]); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12"><h3>Quantidade de questões</h3></div>
    <div class="col-md-12">
        <div class="ibox-content">
            <div class="col-sm-6">
                <?php 
                    $extra = array( 'type' => 'number', 'name' => 'qtd_questoes', 'value' => isset($qtd_questoes) ? $qtd_questoes : 0, 'id' => 'qtd_questoes', 'class' => 'form-control m-b', 'placeholder' => 'Quantidade de questões do caderno' );
                    echo form_input($extra);
                ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4 col-sm-offset-8">
        <button class="btn btn-white"><a href="<?= get_meus_cadernos_url() ?>">Cancelar</a></button>
        <input id="avancar" class="btn btn-primary" type="submit" name="avancar" value="Avançar">
    </div>
</div>

<script>
    
    dis_ids_name = 'dis_ids';
    ass_ids_name = 'ass_ids';

    <?php if(isset($array_ids)) : ?>
		<?php foreach ($array_ids as $id => $nome) : ?>
       		$("<?php echo $id; ?>").select2({
	           	placeholder: "<?php echo $nome ?>"
       		});
       	<?php endforeach; ?>
   	<?php endif;?>

    $('#dis_ids').change("select2:open", function () { 
        var assIds = $('#ass_ids').val();
        var str = "";
        $('#ass_ids').html("<option value='' disabled >Carregando...</option>");
        
        var ids_a = [];
        $('#dis_ids option:selected').each(function(i, selected){ 
            ids_a[i] = $(selected).val(); 
        });

        var ids = ids_a.join('-');
        $.get("<?= base_url('/main/ajax_get_assuntos/') ?>/" + ids, function(data) {
            $.each(data,function(index,value){
                var sel = ($.inArray(data[index].id, assIds) != -1) ? ' selected="selected"' : '';
                str += '<option value="' + data[index].id+ '" ' + sel + ' >'+data[index].text+'</option>';
            });
            $('#ass_ids').html(str);
        }, 'json');
    });
</script>

<?php $this->view('modal/filtro_scripts'); ?>