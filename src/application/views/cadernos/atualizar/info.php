<div class="row">
    <div class="col-md-12"><h3>Questões atuais</h3></div>
    <div class="col-md-12">
        <div class="ibox-content">
            
            <label class="col-md-12"><?php echo form_checkbox('check_manter_atuais', SIM, TRUE, 'id="check_manter_atuais"');?> Manter questões atuais </label>
            
            <?php if(!empty($questoes_fora_do_filtro)): ?>
                <div class="ibox-content col-md-12 fora-filtro" style="padding-bottom: 0px;">
                    <?= ui_get_alerta("As questões " . implode(', ', $questoes_fora_do_filtro) . " não se enquadram no filtro escolhido.", ALERTA_ATENCAO, FALSE); ?>
                </div>
                <label class="col-md-12 fora-filtro"><?php echo form_checkbox('check_fora_filtro', SIM, TRUE, 'id="check_fora_filtro"');?> Remover questões que não se enquadram no filtro </label>
            <?php endif; ?>

            <div id="alert-quantidade-sobrando" class="col-md-12" style="display: none;">
                <div class="ibox-content col-md-12" style="padding-bottom: 0px;">
                    <?= ui_get_alerta("O número de questões escolhido é menor do que o número de questões do caderno atual. Será necessário remover <span class='sobrando'></span> questões. ", ALERTA_ATENCAO, FALSE); ?>
                </div>
                <label class="col-md-3"><?php echo form_radio('radio_exclusao', CADERNOS_QUESTOES_ALEATORIAS, TRUE, 'id="radio_exclusao_aleatoria" class="radio_exclusao"');?> Excluir aleatoriamente </label>
                <label class="col-md-3"><?php echo form_radio('radio_exclusao', CADERNOS_QUESTOES_ESCOLHIDAS, FALSE, 'id="radio_exclusao_escolher" class="radio_exclusao"');?> Escolher quais excluir </label>
            </div>

            <div id="questoes-excluir" class="col-md-12" style="display: none;">

                <?php foreach ($questoes as $questao) : ?>
					<div id="questao_<?php echo $questao['que_id'];?>" class="col-lg-12 questao">
						<div class="ibox">
							<?php $this->view('questoes/cabecalho/cabecalho', array('questao' => $questao, 'is_revisar' => true)); ?>
							
							<div class="ibox-content">
								
								<?= $this->view('questoes/barra_superior/barra_superior', ['pagina' => PAGE_ATUALIZAR_CADERNO], true); ?>
								
								<?= $this->view('questoes/principal/corpo', null, true); ?>
    
    							<?= $this->view('questoes/principal/alternativas', null, true); ?>

							</div>
							<?php //if(!$is_resultado) :?>
							<div class="ibox-footer">							
								<?= $this->view('questoes/barra_inferior/barra_inferior', array('questao' => $questao), true); ?>	
							</div>
							<?php //endif; ?>
						</div>
					</div>
				<?php endforeach;?>

            </div>

            <div id="alert-quantidade-faltando" class="ibox-content col-md-12" style="padding-bottom: 0px; display: none;">
                <?= ui_get_alerta("O número de questões do filtro atual é menor do que o número de questões escolhido, ficarão faltando <span class='faltando'></span> questões para atingir {$qtd_questoes} questões.", ALERTA_ATENCAO, FALSE); ?>
            </div>            

        </div>
        
        
    </div>
</div>

<div class="row">
    <div class="col-sm-4 col-sm-offset-8">
        <button class="btn btn-white"><a href="<?= get_meus_cadernos_url() ?>">Cancelar</a></button>
        <input id="voltar" class="btn btn-primary" type="submit" name="voltar" value="Voltar">
        <input id="atualizar" class="btn btn-primary" type="submit" name="atualizar" value="Atualizar">
    </div>
</div>

<script>

    function exibe_alertas(){
        
        var qtd_questoes = <?= $qtd_questoes ?>;
        var qtd_questoes_atual = <?= $qtd_questoes_atual ?>;
        var qtd_questoes_filtro = <?= $qtd_questoes_filtro ?>;
        var qtd_questoes_fora_filtro = <?= $qtd_questoes_fora_filtro ?>;
		
        var qtd_nova = 0;


        $('#alert-quantidade-sobrando').hide();
        $('#alert-quantidade-faltando').hide();

        if($('#check_manter_atuais').is(':checked')){
            
            var ficarao;
            if($('#check_fora_filtro').is(':checked')){
                ficarao = qtd_questoes_atual - qtd_questoes_fora_filtro;
            }else{
                ficarao = qtd_questoes_atual;
            }

            if(ficarao >= 0){
                
                qtd_falta = qtd_questoes - ficarao;

                if(qtd_falta > qtd_questoes_filtro){
                    $('.faltando').html(qtd_falta - qtd_questoes_filtro);
                    $('#alert-quantidade-faltando').show();
                }

                if(ficarao > qtd_questoes){
                    $('.sobrando').html(ficarao - qtd_questoes);
                    $('#alert-quantidade-sobrando').show();
                }

            }

        }else{
            
            if(qtd_questoes_filtro < qtd_questoes){
                $('.faltando').html(qtd_questoes - qtd_questoes_filtro);
                $('#alert-quantidade-faltando').show();
            }

        }

    }

    $('#check_manter_atuais').change(function(e){
        exibe_alertas();
        if($('#check_manter_atuais').is(':checked')){
            $('.fora-filtro').show();
        }else{
            $('.fora-filtro').hide();
        }
    });

    $('#check_fora_filtro').change(function(e){
        exibe_alertas();
    });

    $('input[type=radio][name=radio_exclusao]').change(function(e){
        
        if(this.value == <?= CADERNOS_QUESTOES_ESCOLHIDAS ?>){
            $('#questoes-excluir').show();
        }else{
            $('#questoes-excluir').hide();
        }

    });

    $(document).ready(function(){
        exibe_alertas();
    });

</script>