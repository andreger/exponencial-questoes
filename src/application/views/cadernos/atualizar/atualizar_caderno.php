<?php $this->view("principal/comum/titulo_h1", ["titulo" => "Atualizar Caderno de Questões"]) ?>

<div style="margin-top: 10px; padding-left: 3px; padding-right: 7px;">	
<?php exibir_mensagem(); ?>
</div>

<div class="atualizar-caderno">

    <?php if(!$is_info_caderno): ?>
        <?php echo form_open('', 'id="form-atualizar-caderno"'); ?>
            <?php $this->view('cadernos/atualizar/filtro'); ?>
        <?php echo form_close(); ?>
    <?php else: ?>
        <?php echo form_open('', 'id="form-atualizar-caderno"', array('filtro_novo' => $filtro_novo, 'qtd_questoes' => $qtd_questoes)); ?>
            <?php $this->view('cadernos/atualizar/info'); ?>
        <?php echo form_close(); ?>
    <?php endif; ?>
</div>

<br/><br/>
