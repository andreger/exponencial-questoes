<?php $this->view("principal/comum/titulo_h1", ["titulo" => "Cadernos de Questões para Concurso"]) ?>
<?php $this->view("principal/cadernos/meus_cadernos/banner_cadernos_exponencial") ?>

<div style="margin-top: 10px; padding-left: 3px; padding-right: 7px;">
<?php exibir_mensagem(); ?>
</div>

<div class="listar-cadernos">

	<?php $this->view("principal/cadernos/meus_cadernos/filtros/por_texto") ?>

	<div class="row">
		<div class="col-md-12">
			<div class="ibox-content">
				<?php if(!tem_acesso(array(/*ADMINISTRADOR, COORDENADOR, PROFESSOR, CONSULTOR, REVISOR, ASSINANTE_SQ*/ USUARIO_LOGADO))) : ?>
				<div class="alert alert-warning" style="margin-bottom: 10px">
					Olá, fique atento: Não Assinantes do Sistema de Questões acessam apenas cadernos grátis e comprados. Caso deseje criar seus próprios cadernos, <a href="/sistema-de-questoes/#planos"> assine nosso Sistema de Questões</a>
				</div>
				<?php endif; ?>

				<?php $this->view("principal/cadernos/meus_cadernos/filtros/legenda") ?>
				<?php $this->view("principal/cadernos/meus_cadernos/filtros/categorias") ?>
				<?php $this->view("principal/cadernos/meus_cadernos/filtros/acoes_em_massa") ?>

				<form id="cadernos-form" method="post">
				<div id="no-more-tables">
				<table class="table cadernos-tabela">
					<thead>
					<tr>
						<th><input type="checkbox" id="marcar_todas"></th>
						<th></th>
						<th>Nome</th>
						<th>Criação</th>
						<th>Último Acesso</th>
						<th>Questões</th>
						<th>Resultado</th>
						<th>Ações</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($cadernos as $caderno) : ?>
					<tr>
						<td><input class="caderno-checkbox" type="checkbox" name="ids[]" value="<?= $caderno['cad_id'] ?>"></td>
						<td><?= get_caderno_icone($caderno) ?></td>
						<td data-title="Nome" class="nome_caderno"><a href="<?= get_resolver_caderno_url($caderno['cad_id']) . '?zf=1' ?>"><?= $caderno['cad_nome']?></a>

							<?php if(!tem_acesso(array(/*ADMINISTRADOR, COORDENADOR, PROFESSOR, CONSULTOR, REVISOR, ASSINANTE_SQ*/ USUARIO_LOGADO))) :?>

								<a class="caderno-aviso-link" href="#"><?= get_tema_image_tag('warning2.png') ?></a>
							<div class="caderno-aviso" style="display:none">
								<?= ui_get_alerta("Assine o Sistema de Questões do Exponencial e tenha acesso livre<br> a cadernos que você mesmo cria ou amigos compartilham com você!<br> <a href='/sistema-de-questoes#planos'>Conheça nossos planos aqui</a>!", ALERTA_INFO, FALSE);
								?>
							</div>

							<?php endif ?>
						</td>
						<td data-title="Criação"><?= converter_para_ddmmyyyy($caderno['cad_data_criacao']) ?></td>
						<td data-title="Último Acesso"><?php
							if(!is_null($caderno['cad_ultimo_acesso'])) {
								echo converter_para_ddmmyyyy($caderno['cad_ultimo_acesso']);
							}
							?>
						</td>
						<td data-title="Questões"><?= $caderno['total_questoes']?> </td>
						<td data-title="Resultado">
							<div class="progress progress-bar-default" title="<?= get_questoes_caderno_tooltip($caderno) ?>">
                                <div style="width: <?= $caderno['total_questoes'] ? $caderno['questoes_certas']/$caderno['total_questoes']*100 : 0 ?>%" class="progress-bar progress-bar"></div>
                                <div style="width: <?= $caderno['total_questoes'] ? $caderno['questoes_erradas']/$caderno['total_questoes']*100 : 0 ?>%" class="progress-bar progress-bar-danger"></div>
                            </div>
						</td>
						<td data-title="Ações" style="width: 22%;">
							<?php $this->view("principal/cadernos/meus_cadernos/listagem_barra_acoes", ["caderno" => $caderno]) ?>
						</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<div class="paginacao">
					<?= $links ?>
				</div>
				</div>
				</form>

				<?php if(count($cadernos) == 0) : ?>
					<?php if(is_null($categoria_id)) :?>
						<div>Você não possui cadernos.</div>
					<?php else :?>
						<div>Não foram encontrados cadernos na categoria selecionada.</div>
					<?php endif; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<div id="modal-editar-caderno" class="modal fade" aria-hidden="true">
	<?php $this->view(get_main_form_editar_caderno_view_url()); ?>
</div>

<div id="modal-editar-categoria" class="modal fade" aria-hidden="true">
	<?php $this->view(get_main_form_editar_categoria_view_url()); ?>
</div>

<div id="modal-categorizar-caderno" class="modal fade" aria-hidden="true">
	<?php $this->view(get_main_form_categorizar_caderno_view_url()); ?>
</div>

<div id="modal-excluir-caderno" class="modal fade" aria-hidden="true">
	<?= get_modal_confirmacao('Excluir Caderno', 'Deseja realmente EXCLUIR este caderno?', get_excluir_caderno_url()) ?>
</div>

<div id="modal-excluir-categoria" class="modal fade" aria-hidden="true">
	<?= get_modal_confirmacao('Excluir Categoria', 'Deseja realmente EXCLUIR a categoria <strong id="cat_name"></strong>?', get_excluir_categoria_url(), 'del_cat_item_id') ?>
</div>

<div id="modal-zerar-estatisticas" class="modal fade" aria-hidden="true">
	<?= get_modal_confirmacao('Zerar Estatísticas', 'Todas as informações sobre seu resultado neste caderno serão zeradas.<br>Tem certeza disto?', get_zerar_estatisticas_url(), "zer_est_item_id") ?>
</div>

<div id="modal-designar-caderno" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class='modal-header'>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class='row resultado-header'>
					<h3 class='pull-left'>Designar Caderno</h3>
				</div>
			</div>
			<div class='modal-body'></div>
		</div>
	</div>
</div>

<div id="modal-filtros-caderno" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class='modal-header'>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class='row resultado-header'>
					<h3 class='pull-left'>Filtros</h3>
				</div>
			</div>
			<div class='modal-body'></div>
		</div>
	</div>
</div>

<div id="modal-info-caderno" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class='modal-header'>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class='row resultado-header'>
					<h3 class='pull-left'>Informações</h3>
				</div>
			</div>
			<div class='modal-body'></div>
		</div>
	</div>
</div>

<div id="modal-compartilhar-caderno" class="modal fade" aria-hidden="true">
	<?php $this->view(get_modal_compartilhar_caderno_view_url()); ?>
</div>

<div id="modal-indice" class="modal fade" aria-hidden="true">
	<?php $this->view('modal/indice_caderno'); ?>
</div>

<a style="display:none" id="link-modal-assine-sq" data-toggle="modal" href="#modal-assine-sq" class="btn btn-sm btn-primary m-t-n-xs"></a>
<div id="modal-assine-sq" class="modal fade" aria-hidden="true">
	<?php $this->view(get_modal_assine_sq_view_url()); ?>
</div>

<div id="modal-comentarios" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content"></div>
	</div>
</div>

<?php $this->view("principal/cadernos/meus_cadernos/listagem_barra_acoes_scripts"); ?>