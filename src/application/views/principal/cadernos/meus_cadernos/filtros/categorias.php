<div style="margin-bottom: 20px">
	<span class="legenda-celula">
		<span class="legenda-label"><strong>Categorias: </strong></span>
	</span>

	<span class="legenda-label"><a href="<?= listar_todas_categorias_meus_cadernos_url()?>">Listar Todos</a></span>

	<?php foreach ($categorias as $categoria) :?>
		<span class="legenda-celula">
			<span class="legenda-label">
				<a onclick="carrega_modal_edicao_categoria(<?= $categoria['cat_id'] ?>)" data-toggle="modal" href="#modal-editar-categoria">
					<?= get_tema_image_tag('e.png', 12, 12) ?>
				</a>
				<a onclick="carrega_modal_exclusao_categoria(<?= $categoria['cat_id']?>,'<?= $categoria['cat_nome'] ?>')" data-toggle="modal" href="#modal-excluir-categoria">
					<?= get_tema_image_tag('x.png', 12, 12) ?>
				</a>
				<a <?= $categoria['cat_id'] == $categoria_id ?"style='font-weight: bold;font-style: italic;'":""  ?> href="<?= get_categoria_url($categoria['cat_id'], TRUE)?>"><?= $categoria['cat_nome'] ?></a>
			</span>
		</span>
	<?php endforeach;?>
</div>