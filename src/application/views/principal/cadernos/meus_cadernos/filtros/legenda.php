<div style="margin-bottom: 10px">
	<span class="legenda-celula">
		<span class="legenda-label"><strong>Legenda: </strong></span>
	</span>

	<span class="legenda-celula <?= $filtro_legenda == FILTRO_LEGENDA_CRIADOS?"legenda-selecionada" : "" ?>">
		<a href="#" onclick="filtrar_legenda(<?= FILTRO_LEGENDA_CRIADOS ?>);">
			<img src="<?= get_tema_image_url('ico-caderno-criado.png') ?>"> <span class="legenda-label">Criados por você</span>
		</a>
	</span>

	<span class="legenda-celula <?= $filtro_legenda == FILTRO_LEGENDA_COMPARTILHADOS?"legenda-selecionada" : "" ?>">
		<a href="#" onclick="filtrar_legenda(<?= FILTRO_LEGENDA_COMPARTILHADOS ?>);">
			<img src="<?= get_tema_image_url('ico-caderno-compartilhado.png') ?>"> <span class="legenda-label">Compartilhados c/ você</span>
		</a>
	</span>

	<span class="legenda-celula <?= $filtro_legenda == FILTRO_LEGENDA_DESIGNADOS?"legenda-selecionada" : "" ?>">
		<a href="#" onclick="filtrar_legenda(<?= FILTRO_LEGENDA_DESIGNADOS ?>);">
			<img src="<?= get_tema_image_url('ico-caderno-designado.png') ?>"> <span class="legenda-label">Designados p/ você</span>
		</a>
	</span>

	<span class="legenda-celula <?= $filtro_legenda == FILTRO_LEGENDA_COMPRADOS?"legenda-selecionada" : "" ?>">
		<a href="#" onclick="filtrar_legenda(<?= FILTRO_LEGENDA_COMPRADOS ?>);">
			<img src="<?= get_tema_image_url('ico-caderno-comprado.png') ?>"> <span class="legenda-label">Comprados por você</span>
		</a>
	</span>
</div>