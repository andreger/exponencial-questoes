<?php //if($check_filtros == 1) : ?>
<form id="form" method="get" action="<?= get_meus_cadernos_url() ?>">
	<div class="row">
		<div style="margin: 20px 0" class="col-lg-9">
			<input id="search-cadernos" class="search_filter form-control" type="text" placeholder="Digite para buscar nos cadernos..." name="texto" value="<?= $search_filter ?>"/>
		</div>
		<?php $this->view("principal/cadernos/meus_cadernos/filtros/ordenacao") ?>
	</div>
	
	<input id="filtro_legenda" type="hidden" name="legenda" value="<?= $filtro_legenda ?>" />
	<div style="margin-bottom: 20px;" class="row"> 
		<?= get_admin_botao_submit('Filtrar', 'filtrar', "", "id='btn-filtrar'"); ?>
		<?= admin_botao_submit('Limpar', 'limpar'); ?>
	</div>
</form> 
<?php //endif; ?>