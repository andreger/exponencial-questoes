
<div class="btn-group dropdown">
	<a class="btn btn-outline btn-default tooltip2 visible-md visible-lg" title="Ordem Aleatória" href="<?= get_embaralhar_questoes_url($caderno['cad_id']) ?>"><i class="fa fa-random"></i></a>

	<a  class="btn btn-outline btn-default tooltip2 visible-md visible-lg" title="Informações" data-toggle="modal" href="#modal-info-caderno" onclick="carrega_modal_info_caderno(<?= $caderno['cad_id'] ?>)"><i class="fa fa-info"></i></a>

	<a class="sq-mecad-zerar-estatisticas btn btn-outline btn-default tooltip2 visible-md visible-lg" title="Zerar Estatísticas" onclick="carrega_modal_zerar_estatisticas(<?= $caderno['cad_id'] ?>)" data-toggle="modal" href="#modal-zerar-estatisticas"><i class="fa fa-code"></i></a>
<!-- 	<div class="dropdown"> -->
		<button data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle" aria-expanded="false">Ver Mais... <span class="caret"></span></button>
		<ul class="dropdown-menu">
			<li class="hidden-md hidden-lg">
				<a href="<?= get_embaralhar_questoes_url($caderno['cad_id']) ?>">Ordem Aleatória</a>
			</li>
			<li class="hidden-md hidden-lg">
				<a data-toggle="modal" href="#modal-info-caderno" onclick="carrega_modal_info_caderno(<?= $caderno['cad_id'] ?>)">Informações</a>
			</li>
			<li class="hidden-md hidden-lg">
				<a onclick="carrega_modal_zerar_estatisticas(<?= $caderno['cad_id'] ?>)" data-toggle="modal" href="#modal-zerar-estatisticas">Zerar Estatísticas</a>
			</li>

			<?php if(pode_editar_caderno($caderno)):?>
			<li>
				<a onclick="carrega_modal_edicao_caderno(<?= $caderno['cad_id'] ?>)" data-toggle="modal" href="#modal-editar-caderno">Editar</a>
			</li>
			<?php endif;?>

			<?php if(pode_categorizar_caderno($caderno)) : ?>
			<li>
				<a onclick="carrega_modal_categorizacao_caderno(<?= $caderno['cad_id'] ?>)" data-toggle="modal" href="#modal-categorizar-caderno">Categorizar</a>
			</li>
			<?php endif; ?>

			<?php if(pode_excluir_caderno($caderno)) : ?>
			<li>
				<a onclick="carrega_modal_exclusao_caderno(<?= $caderno['cad_id'] ?>)" data-toggle="modal" href="#modal-excluir-caderno">Excluir</a>
			</li>
			<?php endif;?>

			<?php if(pode_designar_caderno($caderno)) : ?>
			<li>
				<a data-toggle="modal" href="#modal-designar-caderno" onclick="carrega_modal_designar_caderno(<?= $caderno['cad_id'] ?>, '<?= $caderno['cad_nome'] ?>')"> Designar Caderno</a>
			</li>
			<?php endif;?>

			<li>
				<a data-toggle="modal" href="#modal-filtros-caderno" onclick="carrega_modal_filtros_caderno(<?= $caderno['cad_id'] ?>)"> Filtros</a>
			</li>

			<?php if(pode_compartilhar_caderno($caderno)) : ?>
			<li>
				<a data-toggle="modal" class="compartilhar-caderno" data-id="<?= $caderno['cad_id'] ?>" data-coaching="<?= $caderno['cad_coaching'] ?>" data-nome="<?= $caderno['cad_nome'] ?>" data-target="#modal-compartilhar-caderno"> Compartilhar</a>
			</li>
			<?php endif;?>

			<?php if(is_administrador() || is_coordenador_sq() || is_coordenador_coaching()) :?>
			<li>
				<a data-toggle="modal" data-target="#modal-comentarios" href="<?= get_modal_comentarios_professores_cadernos_url($caderno['cad_id']); ?>"> Comentário Professores</a>
			</li>
			<?php endif;?>

			<li>
				<a onclick="carrega_modal_indice(<?= $caderno['cad_id'] ?>, 0)" data-toggle="modal" data-target="#modal-indice"> Índice</a>
			</li>

			<?php if(AcessoGrupoHelper::cadernos_duplicar(ACESSO_NEGADO)) : ?>
			<li>
				<a class="sq-mecad-duplicar" href="<?= get_duplicar_caderno_url($caderno['cad_id']) ?>"> Duplicar</a>
			</li>
			<?php endif;?>

			<!--
			<li>
				<a class="sq-mecad-duplicar" href="<?php // echo get_atualizar_caderno_url($caderno['cad_id']) ?>"> Atualizar caderno</a>
			</li>
			-->

		</ul>
<!-- 	</div> -->
</div>
