<div style="padding-left: 19px;padding-right: 22px;">
	<div class="row baner-text ipad-hide mobile-hide" style="margin-top: 10px;">
		<a href="<?= get_cadernos_exponencial_url() ?>">		
			<div class="col-md-8 baner-bg-1">
				<p>Acesse vários cadernos de questões <span style="font-size: 22px;">EXCLUSIVOS!</span></br>
				Muitos deles são <span style="color: #fede67;font-size: 22px;">GRÁTIS</span> e focados no seu concurso.</p>
			</div>
			<div class="col-md-4 baner-bg-2">
				<p>CADERNO DE QUESTÕES <span style="color: #fede67; font-size: 22px; font-style: italic;">Exponencial</span></p>
				<span class="bg-exper">Acesse aqui</span>
			</div>
		</a>
	</div>
	<div class="row baner-text b-mobile ipad-show" style="margin-top: 10px; display: none">
		<a href="<?= get_cadernos_exponencial_url() ?>">		
			<div class="col-md-12 baner-bg-3">
				<p class="margin-baner-mobile">CADERNO DE QUESTÕES <span style="color: #fede67; font-size: 28px; font-style: italic;">Exponencial</span></p>
				<p class="font-baner-mobile">Acesse vários cadernos de questões <span style="font-size: 19px;">EXCLUSIVOS!</span></br>
				Muitos deles são <span style="color: #fede67;font-size: 19px;">GRÁTIS</span> e focados no seu concurso.</p>
				<span class="bg-exper">Acesse aqui</span>
			</div>
		</a>
	</div>
	<div class="row baner-text b-mobile mobile-show" style="margin-top: 10px; display: none">
		<a href="<?= get_cadernos_exponencial_url() ?>">		
			<div class="col-md-12 baner-bg-3">
				<p class="margin-baner-mobile-2">CADERNO DE QUESTÕES <span style="color: #fede67; font-size: 28px; font-style: italic;">Exponencial</span></p>	
				<span class="bg-exper">Acesse aqui</span>
			</div>
		</a>
	</div>
</div>