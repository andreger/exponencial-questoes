<!--<script src="/wp-content/themes/academy/js/jquery.validate.min.js"></script>-->
<script>
function carrega_modal_edicao_categoria(categoria_id)
{
	$('#modal-editar-categoria .panel-body .panel-conteudo').hide();

	$.get('/questoes/main/xhr_get_categoria/'+categoria_id, function(data) {
		$('#id_categoria').val(categoria_id);
		$('#nome_categoria').val(data.cat_nome);
		$('#modal-editar-categoria .panel-body .panel-carregando').hide();
		$('#modal-editar-categoria .panel-body .panel-conteudo').show();
	}, 'json');
}

function carrega_modal_designar_caderno(caderno_id, nome_caderno)
{
	$('#modal-designar-caderno .modal-header h3').html("Designar Caderno - " + nome_caderno);
	$('#modal-designar-caderno .modal-body').html("Carregando...");

	$.get('/questoes/cadernos/modal_designar_caderno/'+caderno_id, function(data) {
		$('#modal-designar-caderno .modal-body').html(data);
	});
}

function carrega_modal_designar_cadernos_selecionados()
{
	$('#modal-designar-caderno .modal-header h3').html("Designar Cadernos");
	$('#modal-designar-caderno .modal-body').html("Carregando...");

	caderno_id = $('.caderno-checkbox:checked').map(function () { return this.value} ).get().join('-');

	$.get('/questoes/cadernos/modal_designar_caderno/'+caderno_id+'/1', function(data) {
		$('#modal-designar-caderno .modal-body').html(data);
	});
}

function carrega_modal_filtros_caderno(caderno_id)
{
	$('#modal-filtros-caderno .modal-body').html("Carregando...");

	$.get('/questoes/cadernos/modal_filtros_caderno/'+caderno_id, function(data) {
		$('#modal-filtros-caderno .modal-body').html(data);
	});
}

function carrega_modal_info_caderno(caderno_id)
{
	var elem = $('#modal-info-caderno .modal-body');
	elem.html("Carregando...");

	$.get('/questoes/cadernos/modal_info_caderno/'+caderno_id, function(data) {
		elem.html(data);
	});
}


function carrega_modal_indice(caderno_id, filtro)
{
	var elem = $('#modal-indice .modal-body');
	elem.html("Carregando...");

	$.get('/questoes/cadernos/modal_indice/'+caderno_id+'/'+filtro + '<?= $query_string ?>', function(data) {
		elem.html(data);
	});
}

function carrega_modal_exclusao_categoria(categoria_id, categoria_nome)
{
	document.getElementById("cat_name").innerHTML = categoria_nome;
	document.getElementById("del_cat_item_id").value = categoria_id;
	//$('#cat_name').html(categoria_nome);
	//$('#del_cat_item_id').val(categoria_id);
}

function copy_to_clipboard()
{
  var aux = document.createElement("input");
  aux.setAttribute("value", $('#url_caderno_compartilhado').val());
  document.body.appendChild(aux);
  aux.select();
  document.execCommand("copy");

  document.body.removeChild(aux);
}

function filtrar_legenda(filtro_legenda){
	$('#filtro_legenda').val(filtro_legenda);
	$('#btn-filtrar').click();
}

$("<?= $id; ?>").select2({
   	placeholder: "<?= $nome ?>"
	});


$(function() {

	$('#modal-comentarios').on('shown.bs.modal', function () {
		$(this).removeData('bs.modal');
	});

	$('#marcar_todas').click(function() {
    	$('.cadernos-tabela input:checkbox').not(this).prop('checked', this.checked);
	});

	$('.caderno-aviso-link').on('click', function (e) {
		e.preventDefault();
		$(this).closest('.nome_caderno').find('.caderno-aviso:first').toggle();
	});

	$('#excluir_selecionados').click(function() {
		if(confirm('Confirma exclusão dos cadernos?')) {
			$('#cadernos-form').prop('action', '/questoes/cadernos/excluir_selecionados');
			$('#cadernos-form').submit();
		}
	});

	$(".compartilhar-caderno").click(function(e) {
		e.preventDefault();
		$('#url_caderno_compartilhado').val("Carregando...");
		var nome = $(this).data('nome');
		nome = nome.trim();
		$.get('/questoes/xhr/caderno_compartilhado_url/' + $(this).data('id') + '/' + $(this).data('coaching'), function(data) {
			<?php $usuario = get_usuario_array(); ?>


			<?php $titulo = "Novo Caderno Exponencial Compartilhado - [[nome]] "; ?>
			var titulo = "<?= $titulo ?>";
			titulo = titulo.replace('[nome]', nome);

			$('#titulo').val(titulo);

			<?php $mensagem = "Olá, {$usuario['nome_completo']} compartilhou o caderno ˮ[nome]ˮ com você:"; ?>
			var mensagem = "<?= $mensagem ?> " + data;
			mensagem = mensagem.replace('[nome]', nome);




			$('#url_caderno_compartilhado').val(data);
			$('#mensagem').val(mensagem);
			$('#mensagem_padrao').val(mensagem);
		})
	});

	$("#enviar_email_form").validate({
		rules: {
			emails: {
				required: true,
			},
			titulo: {
				required: true,
			},
			mensagem: {
				required: true,
			}
		},
		messages: {
			emails: {
				required: "Você precisa preencher esse campo",
			},
			titulo: {
				required: "Você precisa preencher esse campo",
			},
			mensagem: {
				required: "Você precisa preencher esse campo",
			}
		}
	});

	$('.tooltip2').tooltipster();

	$(document).on("shown.bs.dropdown", ".dropdown", function () {
	    // calculate the required sizes, spaces
	    var $ul = $(this).children(".dropdown-menu");
	    var $button = $(this).children(".dropdown-toggle");
	    var ulOffset = $ul.offset();
	    // how much space would be left on the top if the dropdown opened that direction
	    var spaceUp = (ulOffset.top - $button.height() - $ul.height()) - $(window).scrollTop();
	    // how much space is left at the bottom
	    var spaceDown = $(window).scrollTop() + $(window).height() - (ulOffset.top + $ul.height());
	    // switch to dropup only if there is no space at the bottom AND there is space at the top, or there isn't either but it would be still better fit
	    if (spaceDown < 0 && (spaceUp >= 0 || spaceUp > spaceDown))
	      $(this).addClass("dropup");
	}).on("hidden.bs.dropdown", ".dropdown", function() {
	    // always reset after close
	    $(this).removeClass("dropup");
	});

});

</script>