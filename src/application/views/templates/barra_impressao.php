<script src="<?php echo base_url('assets-admin/js/jquery-2.1.1.js'); ?>"></script>
<style>
    .barra-impressao .barra{
        background-color: #000234;
        font-weight: bold;
        color: #FFFFFF;
        border-radius: 8px;
        font-size: 14px;
        padding: 6px;
        margin-right: 8px;
        margin-top: 5px;
    }

    .botao :hover{
        cursor: pointer;
    }
</style>

<div class="barra-impressao">
    <span class="barra">Tamanho da fonte</span>
    <span class="barra botao"><a onclick="alteraTamanho(true); return false;"> + </a></span>
    <span class="barra botao"><a onclick="alteraTamanho(false); return false;"> - </a></span>
    <span class="barra botao"><a onclick="window.print();"> Imprimir </a></span>
</div>

<script>

    $( document ).ready(function() {

        var top = $('.barra-impressao').offset().top;
        
        $(document).scroll(function(){
            $('.barra-impressao').css('position','');
            top = $('.barra-impressao').offset().top;
            $('.barra-impressao').css('position','absolute');   
            $('.barra-impressao').css('top', Math.max(top,$(document).scrollTop()));
            $('.barra-impressao').css('margin-top','5px');
        });

    });

    var min = parseInt('<?php echo $min?$min:5 ?>');
    var max = parseInt('<?php echo $max?$max:30 ?>');

    function alteraTamanho(isAumentar){

        var el = $('.<?php echo $fonte_class ?>');
        var fs = el.css('font-size');
        fs = fs.substring(0, fs.length-2);
        fs = parseInt(fs);
        if(isAumentar && max > fs){
            fs++;
        }else if(!isAumentar && fs > min){
            fs--;
        }
        el.css('font-size', fs+'px');
    }
    
    $( window ).bind( "beforeprint", function(e){
        $('.barra-impressao').hide();
    });
    
    $( window ).bind( "afterprint", function(e){
        $('.barra-impressao').show();
    });

</script>