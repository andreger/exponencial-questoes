<?php 
$selecionados = [];
$config_usuario = get_config_aluno();

if($filtros_selecionados) {
    $selecionados = $filtros_selecionados;
}

if($selecionados && !nenhum_filtro_aplicado($filtro)) {
	
	$contador = count($selecionados);
	if($contador > 1)
	{
		$mensagem_filtro = "filtros selecionados";
	}
	else
	{
		$mensagem_filtro = "filtro selecionado";
	}

	$expanded = "false";
	$collapse = "collapse";
	if($config_usuario['exibir_filtros']){
		$expanded = "true";
		$collapse = "collapse in";
	}

	$html .= "<div class='filtros-selecionados'>
				<div class='panel panel-default'>
				<div class='panel-heading' role='tab' id='questionOne'>
					<h5 class='panel-title'>
						<a id='collapse-id-filtro' data-toggle='collapse' data-parent='#faq' href='#answerOne' aria-expanded='{$expanded}' aria-controls='answerOne'>
						<div class='filtros-selecionados-titulo'>{$contador} {$mensagem_filtro} -  <span class='mostrar-div'>Mostrar</span><span class='esconder-div'>Esconder</span>
						</div>
						</a>
					</h5>		
				</div>
				<div id='answerOne' class='panel-collapse {$collapse}' role='tabpanel' aria-labelledby='questionOne' aria-expanded='{$expanded}'>
				<div class='panel-body'>";
				foreach($selecionados as $item) {
					$filtro = truncar($item['nome'], 30);
						$html .= "<button data-toggle='tooltip' class='btn btn-danger btn-outline' type='button' title='{$item['nome']}'>{$filtro}&nbsp;&nbsp;<a href='{$item['url']}' class='fa fa-times color-x'></a></button> ";

				}
				$html .="</div></div></div></div>";
				echo $html;
}