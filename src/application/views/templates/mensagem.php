<?php $CI = &get_instance(); ?>
<?php if($mensagem = $CI->session->flashdata(ALERTA_SUCESSO)) : ?>
<div class="alert alert-success alert-dismissable">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	<?php echo $mensagem ?> <a class="alert-link" href="#">Alert Link</a>.
</div>

<?php endif; ?>
