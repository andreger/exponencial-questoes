<?php
	admin_cabecalho_pagina("Editar Grupo"); 
?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<?php echo form_open(get_editar_grupo_url($grupo['rag_id']), 'id="form-orgao" class="form-horizontal"'); ?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nome</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="rag_nome" value="<?php echo isset($grupo['rag_nome']) ? $grupo['rag_nome'] : ''; ?>"/>
                    	<span class="help-block m-b-none error"><?php echo form_error('rag_nome'); ?></span>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>
                <?php admin_botoes_salvar_cancelar(get_editar_ranking_url ($grupo['ran_id'], 2)) ?>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>