<?php
	$prefixo = isset($caderno_produto) ? 'Editar' : 'Novo'; 
	admin_cabecalho_pagina($prefixo . " Produto"); 
    $desativado = $foi_vendido ? 'disabled=disabled' : '';
?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
                <?php if($foi_vendido) : ?>
                <div class="alert alert-info alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <strong>ATENÇÃO:</strong> Este produto já foi comercializado e não pode mais ser editado.
                </div>
                <?php endif ?>

				<?php echo form_open(get_editar_caderno_produto_url($caderno_produto['cpr_id']), 'id="form-assunto" class="form-horizontal"'); ?>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Caderno</label>
                    <div class="col-sm-10">
                    	<?php echo form_dropdown('cad_id', $cadernos_combo, isset($caderno_produto) ? $caderno_produto['cad_id'] : null, "id='cad_id' class='form-control m-b' {$desativado}"); ?>
                    	<span class="help-block m-b-none error"><?php echo form_error('cad_id'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Produto WP</label>
                    <div class="col-sm-10">
                    	<?php echo form_dropdown('produto_id', $produtos_combo, isset($caderno_produto) ? $caderno_produto['produto_id'] : null, "id='produto_id' class='form-control m-b'{$desativado}"); ?>
                    	<span class="help-block m-b-none error"><?php echo form_error('produto_id'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                
                <?php 
                if($desativado) {
                    echo get_admin_botao_cancelar(get_listar_cadernos_produtos_url(), $caderno_produto['cpr_id']?"Voltar":"Cancelar");
                }
                else {                   
                    admin_botoes_salvar_cancelar(get_listar_cadernos_produtos_url());
                }
				form_close(); 
                ?>
			</div>
		</div>
	</div>
</div>