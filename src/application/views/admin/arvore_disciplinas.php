<?php admin_cabecalho_pagina("Árvore de Disciplinas") ?>
<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<div id="arvore"><?= get_arvore($arvore); ?></div>
			</div>
		</div>
	</div>
</div>

<script>
$('#arvore').jstree();
</script>