<?php
	$prefixo = isset($area_atuacao['ara_id']) ? 'Editar' : 'Nova'; 
	admin_cabecalho_pagina($prefixo . " Área de Atuação"); 
?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<?php echo form_open(get_editar_area_atuacao_url($area_atuacao), 'id="form-area_atuacao" class="form-horizontal"'); ?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nome</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="ara_nome" value="<?php echo isset($area_atuacao['ara_nome']) ? $area_atuacao['ara_nome'] : ''; ?>"/>
                    	<span class="help-block m-b-none error"><?php echo form_error('ara_nome'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <?php admin_botoes_salvar_cancelar(get_listar_areas_atuacao_url()) ?>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>