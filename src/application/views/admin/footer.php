    	</div>
    </div>

    <script>
	    function contar_simulados() {
			//var simulados = $("#simulados").find('.widget-simulado:visible').length;
			var simulados = <?php echo $total ? $total : 0 ?>;
	
			if(simulados == 0) {
				label = "Nenhum simulado encontrado. Filtre novamente.";
			}
			else if(simulados == 1) {
				label = "1 simulado encontrado";
			}
			else {
				label = simulados + " simulados encontrados";
			}
			
			$('#cont-simulado').html(label);
		}
		
        $(document).ready(function() {
        	contar_simulados();
            
        	<?php if(isset($include_icheck)) : ?>
        	$('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            <?php endif ?>
            
			<?php if(isset($include_data_tables)) : ?>
        	$('.dataTables').dataTable({
				<?php if(isset($data_table_order)) : ?>
				"order": <?= $data_table_order ?>,
				<?php endif; ?>
				 
				<?php if(isset($data_table_length)) : ?>
				"pageLength": <?= $data_table_length ?>,
				<?php endif; ?>            	

        		language: {
        	        url: "<?php echo base_url('assets-admin/js/ext/dataTable-pt_BR.json') ?>"
        	    },
                responsive: true,
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": "<?php echo base_url('assets-admin/js/plugins/dataTables/swf/copy_csv_xls_pdf.swf') ?>",
                    "aButtons": [
                       	{
                       		"sExtends": "copy",
                       		"sButtonText": "Copiar"
                       	},
                        {
                        	"sExtends": "csv",
                        },
                        {
                        	"sExtends": "xls",
                        },
                        {
                        	"sExtends": "pdf",
                        },
                        {
                        	"sExtends": "print",
                        	"sButtonText": "Imprimir"
                        }
                	]
                }
            });
            <?php endif; ?>

            <?php if(isset($include_admin_editar_assunto)) : ?>
				$("#ass_pai_id").remoteChained({
					parents : "#dis_id",
					url : "<?= get_ajax_get_ass_id_url(false, true, 'dis_id', false, $assunto_selecionado?:0); ?>"
				});
				$('#dis_id').trigger('change');
            <?php endif; ?>
            
            <?php if(isset($include_admin_editar_prova)) : ?>
				<?php $array = array('arf_ids' => 'áreas de formação', 'ara_ids' => 'áreas de atuação', 'car_ids' => 'cargos'); ?>
				<?php foreach ($array as $key => $name) : ?>
	            	$("#<?php echo $key; ?>").chosen({
	            		disable_search_threshold: 10,
	            		width: '750px',
	            		no_results_text: "Nenhuma opção com:",
	            		placeholder_text_multiple: "Selecione <?php echo $name; ?>..."
	                });
				<?php endforeach; ?>
            <?php endif;?>
            
			<?php if(isset($include_admin_listar_questoes)) : ?>

				$('#modal-adicionar-questao').on('hidden.bs.modal', function () {
				  $(this).removeData('bs.modal').find(".modal-content").empty();
				});
	           	<?php $arra_inputs = get_array_input_names("listar"); ?>
                <?php $array_labels = get_array_input_labels("listar"); ?>
				<?php foreach ($arra_inputs as $key => $input): ?>
		           		$('<?php echo "#{$input}"; ?>').select2({
				           	placeholder: "<?php echo $array_labels[$key]; ?>"
		           		});
				<?php endforeach; ?>
            <?php endif; ?>

           $('.note-view button[data-event=fullscreen]').remove();           
        });
    </script>
    <?php javascript_da_pagina() ?>
    
    <?php if(isset($include_data_tables)) : ?>
    <style>
    body.DTTT_Print {
        background: #fff;

    }
    .DTTT_Print #page-wrapper {
        margin: 0;
        background:#fff;
    }

    button.DTTT_button, div.DTTT_button, a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }
    button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }
	</style>
	<?php endif; ?>
	<?php if( SHOW_ACTIVE_PLUGINS && is_administrador() ): ?>
		<div class="text-center p-3 text-info font-weight-bold">
			<?php 
		      FiltroPluginsConstantes::plugins_ativos();
		    ?>
		 </div>
	<?php endif; ?>
	<?php KLoader::view("login/modal_expiracao"); ?>

</body>