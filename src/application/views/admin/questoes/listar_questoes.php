<?php admin_cabecalho_pagina("Questões"); ?>
<?php ui_alerta($this->session->flashdata('sucesso'), ALERTA_SUCESSO); ?>
<nav id="nav-questoes" class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0; z-index: 999;">
	<div class="row">
		<div class="col-lg-8">					
			<div style="margin-top: 10px;">
				<a id="filtrar-questoes" class="btn btn-outline btn-default" data-toggle="modal" href="#modal-form-filter"><i class="fa fa-filter"></i> Filtrar </a>
				<a class="btn btn-outline btn-default" href="<?= get_listar_questoes_url() ?>" id="limpar-botao" >Limpar Filtros</a>
				<a class='btn btn-primary ' href='<?php echo get_editar_questao_url();?>'>Nova Questão</a>
			</div>
			<div id="modal-form-filter" class="modal fade" aria-hidden="true"><?php $this->view(get_admin_questoes_filtro_modal_view_url()); ?></div>
		</div>
		<div class="col-lg-4" style="text-align: right; margin-top: 20px;">
			<?php if(isset($total)) :?>
			<div class="">
				Questões: <strong><?php echo numero_inteiro($total) ?></strong>
			</div>
			<?php endif; ?>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-12">
			<?php $this->view('templates/filtros_selecionados') ?>
		</div>		
	</div>
</nav>
<?php if(isset($erro)): ?>
	<div class="row col-md-12">
		<?php ui_alerta($erro, ALERTA_ERRO) ?>
	</div>
<?php endif; ?>
<?php if(isset($thead_array)) : ?>
	<div><?php data_table($thead_array, $tbody_array, $tfoot_array); ?></div>
<?php endif; ?>
<div><?php admin_botao_verde('Nova Questão', get_editar_questao_url()); ?></div>
<div id='modal-adicionar-questao' class='modal fade' aria-hidden='true' tabindex='-1' >
	<div class='modal-dialog modal-sm'>
		<div class='modal-content modal-form-content'>
		</div>
	</div>
</div>

<script>

	$(function() {
		
		$("#limpar-botao").click(function() {
			$('#que_id').val("");
			$('#que_qcon_id').val("");
			$('#palavra_chave').val("");
			$('#data_inicio').val("");
			$('#data_fim').val("");
			$('.multiselect').select2("val", "");
			$('.i-checks input:radio').iCheck('uncheck');
			$('.i-checks input:checkbox').iCheck('uncheck');
			$('#filtro-submit').click();
		}); 

		$('#filtrar-questoes').click(function(){
			dis_ids_name = 'dis_ids';
			ass_ids_name = 'ass_ids';
		});

		$('#ass_ids').select2({
			placeholder: "Assuntos"
		});

		$('#dis_ids').change("select2:open", function () {
			var assIds = $('#ass_ids').val();
			var str = "";
			$('#ass_ids').html("<option value='' disabled >Carregando...</option>");

			var ids_a = [];
			$('#dis_ids option:selected').each(function(i, selected){
				ids_a[i] = $(selected).val();
			});

			var ids = ids_a.join('-');
			if(ids){
				$.get("<?= base_url('/main/ajax_get_assuntos/') ?>/" + ids, function(data) {
					/*$.each(data,function(index,value){
					var sel = ($.inArray(data[index].id, assIds) != -1) ? ' selected="selected"' : '';
					str += '<option value="' + data[index].id+ '" ' + sel + ' >'+data[index].text+'</option>';
					});
					$('#ass_ids').html(str);*/
					adicionar_itens($('#ass_ids'), data, assIds);
				}, 'json');
			}
		});

	});

</script>