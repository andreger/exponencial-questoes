<div class="modal-dialog">
	<div class="modal-content modal-form-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Filtrar Questões</h4>
		</div>
		<div class="modal-body">
			<div id="p1">
				<?php echo form_open(get_listar_questoes_url(), 'id="form-listar-questao" class="form-horizontal" method="get"'); ?>
			
					<div class="filtro-incluir"> 
						<div class="row">
							<label class="col-md-2 control-label">Incluir questões</label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_incluir', TODAS, $filtro_incluir == TODAS, 'id="radio_todas"');?> Todas </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_incluir', NOTIFICACAO_ERRO, $filtro_incluir == NOTIFICACAO_ERRO, 'id="radio_notificacao_erro"');?> Notificação de erro </label>
							<!-- <label class="col-md-2 radio-inline i-checks"><?php echo form_radio('filtro_incluir', FAVORITAS, $filtro_incluir == FAVORITAS, 'id="radio_favoritas"');?> Favoritas </label> -->
						</div>
					</div>
					
					<div style="clear:both"></div>
					<div class="filtro-incluir">
						<div class="row">
							<label class="col-md-2 control-label">Comentários </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', TODAS, $filtro_comentarios == TODAS, 'id="radio_todas"');?> Todas </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', COM_COMENTARIOS_DE_PROFESSORES, $filtro_comentarios == COM_COMENTARIOS_DE_PROFESSORES, 'id="radio_com_comentarios_de_professores"');?> Professores </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', COM_COMENTARIOS_DE_ALUNOS, $filtro_comentarios == COM_COMENTARIOS_DE_ALUNOS, 'id="radio_com_comentarios_de_alunos"');?> Alunos </label>
						</div>

						<div class="row">
							<label class="col-md-offset-2 col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', COM_COMENTARIOS, $filtro_comentarios == COM_COMENTARIOS, 'id="radio_com_comentarios"');?> Quaisquer </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', NENHUM_COMENTARIO, $filtro_comentarios == NENHUM_COMENTARIO, 'id="radio_nenhum_comentario"');?> Nenhum </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', MEUS_COMENTARIOS, $filtro_comentarios == MEUS_COMENTARIOS, 'id="radio_meus_comentarios"');?> Meus </label>
						</div>

						<div class="row">
							<label class="col-md-offset-2 col-md-9 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', COM_COMENTARIOS_EM_VIDEO, $filtro_comentarios == COM_COMENTARIOS_EM_VIDEO, 'id="radio_com_comentarios_em_video"');?> Vídeos </label>
						</div>
					</div>
					
					<?php if(tem_acesso(array(ADMINISTRADOR, CONSULTOR, PROFESSOR, REVISOR))) :?>
					<div style="clear:both"></div>
					<div class="filtro-incluir">
						<div class="row">
							<label class="col-md-2 control-label">Incluir questões </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_resposta', TODAS, $filtro_resposta == TODAS, 'id="radio_todas"');?> Todas </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_resposta', COM_RESPOSTA, $filtro_resposta == COM_RESPOSTA, 'id="radio_com_resposta"');?> Com Resposta </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_resposta', SEM_RESPOSTA, $filtro_resposta == SEM_RESPOSTA, 'id="radio_sem_resposta"');?> Sem Resposta </label>
						</div>
					</div>
					<div style="clear:both"></div>
					<div class="filtro-incluir">
						<div class="row">
							<label class="col-md-2 control-label">Incluir questões </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_ativo', TODAS, $filtro_ativo == TODAS, 'id="radio_todas"');?> Todas </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_ativo', ATIVO, $filtro_ativo == ATIVO, 'id="radio_ativas"');?> Ativas </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_ativo', INATIVO, $filtro_ativo == INATIVO, 'id="radio_inativas"');?> Inativas </label>
						</div>
					</div>
					<?php endif; ?>
					<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) :?>
					<div class="filtro-incluir">
						<div class="row">
							<label class="col-md-2 control-label">Incluir questões </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_repetidas', TODAS, $filtro_repetidas == TODAS, 'id="radio_repetidas_nao_repetidas_todas"');?> Todas </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_repetidas', REPETIDAS, $filtro_repetidas == REPETIDAS, 'id="radio_repetidas"');?> Repetidas </label>
							<label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_repetidas', NAO_REPETIDAS, $filtro_repetidas == NAO_REPETIDAS, 'id="radio_nao_repetidas"');?> Não repetidas </label>
						</div>
					</div>
					<?php endif ?>
					<div class="filtro-incluir">
						<div class="row">
							<label class="col-md-2 control-label">Excluir questões </label>
							<?php if(!$is_caderno) :?>
								<label class="col-md-3 checkbox-inline i-checks"><?php echo form_checkbox('filtro_excluir[]', DOS_MEUS_CADERNOS, in_array(DOS_MEUS_CADERNOS, $filtro_excluir), 'id="check_cadernos"');?> Dos meus cadernos </label>
							<?php endif ?>
							<label class="col-md-3 checkbox-inline i-checks"><?php echo form_checkbox('filtro_excluir[]', ANULADA, in_array(ANULADA, $filtro_excluir), 'id="check_anuladas"');?> Anuladas </label>
							<label class="col-md-3 checkbox-inline i-checks"><?php echo form_checkbox('filtro_excluir[]', DESATUALIZADA, in_array(DESATUALIZADA, $filtro_excluir), 'id="check_desatualizadas"');?> Desatualizadas </label>
						</div>
						<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) :?>
							<div class="row">
								<label class="col-md-offset-2 col-md-3 checkbox-inline i-checks"><?php echo form_checkbox('filtro_excluir[]', REPETIDAS, in_array(REPETIDAS, $filtro_excluir), 'id="check_repetidas"');?> Repetidas </label>
							</div>
						<?php endif ?>
					</div>

					<div class="filtro-incluir">
						<div class="row">
							<label class="col-md-2 control-label">Apenas favoritas </label>
							<label class="col-md-3 checkbox-inline i-checks"><?php echo form_checkbox('filtro_favoritas', FAVORITAS, $filtro_favoritas, 'id="check_favoritas"');?></label>
						</div>
					</div>
				
					<div class="form-group">
						<div class="col-sm-11">
							<?php echo form_input('busca', $busca, 'id="busca" autocomplete=off placeholder="Busca por texto" class="form-control m-b"'); ?>
						</div>
						<?php ui_filtro_campo_helper("Veja como funciona a busca por texto livre:<br/>
							- organizações modernas -> busca ambos os termos, independente da ordem em que aparecem.<br/>
							- \"organizações moderna\" -> busca texto exato.<br/>
							- organizações | modernas -> busca um ou outro termo.<br/>
							A busca é feita no enunciado e alternativas das questões. Imagens são ignoradas.") ?>
						
						<?php foreach($array_multiselect as $multiselect) : ?>

							<?php if ($multiselect[0] == "ass_ids"): ?>
								<div class="col-sm-12">
									<label class="i-checks lbl-normal"><?php echo form_checkbox('apenas_assunto_pai', 1, $apenas_assunto_pai, 'id="check_assunto_pai"');?> Apenas assunto-pai</label>
								</div>
							<?php endif ?>
							
						<div class="<?= $multiselect[0] == "que_tipos" || 
										$multiselect[0] == "que_dificuldades" ||
										$multiselect[0] == "ara_ids" ||
										$multiselect[0] == "arf_ids"
											? "col-sm-6": ($multiselect[0] == "sim_ids"?"col-sm-11":"col-sm-12") ?>">
														
							<?php echo form_multiselect("{$multiselect[0]}[]", $multiselect[1], $multiselect[2], "id='{$multiselect[0]}' class='multiselect form-control m-b'"); ?>
						
							<?php if ($multiselect[0] == "sim_ids"): ?>
								</div>
								<?php ui_filtro_campo_helper("Todos: exclui questões presentes em quaisquer simulados<br/>
									Ativos: exclui questões presentes em simulados ativos (simulados disponíveis para alunos)<br/>
									Todos menos inativos: exclui questões presentes em todos os simulados, exceto inativos. Ou seja, mostra questões presentes em simulados inativos (normalmente são simulados muito antigos ou não comercializados).<br/>
									Escolha simulados específicos caso queira excluir questões usadas em determinados simulados.<br/>
									Você pode aplicar várias opções neste filtro.") ?>
							<?php elseif($multiselect[0] == "ass_ids"): ?>
								<a style="position:relative;top: -15px" id="arvore-btn" href="#">Árvore de assuntos</a>
									<?php if(tem_acesso(array(ADMINISTRADOR, PROFESSOR, REVISOR))) : ?>
									</div>
									<div class="col-sm-12">
										<?php echo form_multiselect('professores_ids[]', $professores, $professores_selecionados, 'id="professores_ids" class="multiselect form-control m-b"'); ?>
									<?php endif; ?>
								</div>
							<?php else: ?>
								</div>
							<?php endif; ?>
						
						<?php endforeach; ?>					

						<div class="col-sm-12">
							<?php echo form_multiselect('neg_cad_ids[]', $cadernos, $neg_cadernos_selecionados, 'id="neg_cad_ids" class="multiselect form-control m-b"'); ?>
						</div>

						<div class="col-sm-12">
							<?php echo form_multiselect('pro_tipo_ids[]', $modalidades, $modalidades_selecionadas, 'id="pro_tipo_ids" class="multiselect form-control m-b"'); ?>
						</div>

						</a><div class="col-sm-6">
							<?php echo form_input('que_id', $questao_id,'id="que_id" placeholder="Id da Questão" class="form-control m-b"'); ?>
						</div>
						
						<div class="col-sm-6">
							<?php echo form_input('que_qcon_id', $que_qcon_id,'id="que_qcon_id" placeholder="Q" class="form-control m-b"'); ?>
						</div>
						
						
						<?php if(tem_acesso([ADMINISTRADOR, COORDENADOR, PROFESSOR, CONSULTOR])) : ?>
						<label class="col-sm-12">Questões adicionadas em:</label>

						<div class="col-sm-6">
							<?php echo form_input('data_inicio', $data_inicio,'id="data_inicio" placeholder="De" class="form-control m-b" autocomplete=off'); ?>
						</div>

						<div class="col-sm-6">
							<?php echo form_input('data_fim', $data_fim,'id="data_fim" placeholder="Até" class="form-control m-b" autocomplete=off'); ?>
						</div>
						<?php endif ?>
						
						
					</div>
				<div>
					<?php echo form_submit('buscar', 'Buscar!', 'class="btn btn-primary" id="filtro-submit"'); ?>
					<a class="btn btn-outline btn-default" id="limpar-botao-modal">Limpar Filtros</a>
				</div>

				<?php echo form_close(); ?>

			</div>

			<?php $this->view("questoes/filtro/arvore_assuntos"); ?>

		</div>
	</div>
</div>

<?php $this->view('modal/filtro_scripts') ?>

<script>
	$(document).ready(function(){

		$.fn.datepicker.dates['pt'] = {
			format: 'dd/mm/yyyy',
			days: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			daysMin: ['D','S','T','Q','Q','S','S','D'],
			daysShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			months: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthsShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			today: 'Hoje',
			weekStart: 0,
			clear: "Limpar"
		};

		$(document).on('click', '#data_inicio', function () { 
	        var me = $("#data_inicio");   
	        me.datepicker({
	            showOn: 'focus',
	            altFormat: "mm/dd/yy",
	            dateFormat: "mm/dd/yy",
	            language: 'pt',
				clearBtn: true
	        }).focus();
	    }).on('focus', '#data_inicio', function () {
	        var me = $("#data_inicio");
	        me.mask('99/99/9999');
	    });

		$(document).on('click', '#data_fim', function () { 
	        var me = $("#data_fim");   
	        me.datepicker({
	            showOn: 'focus',
	            altFormat: "mm/dd/yy",
	            dateFormat: "mm/dd/yy",
	            language: 'pt',
				clearBtn: true
	        }).focus();
	    }).on('focus', '#data_fim', function () {
	        var me = $("#data_fim");
	        me.mask('99/99/9999');
	    });
	});
</script>
<script>
$(function() {
	$("#limpar-botao-modal").click(function() {
		$('#que_id').val("");
		$('#que_qcon_id').val("");
		$('#palavra_chave').val("");
		$('#data_inicio').val("");
		$('#data_fim').val("");
		$('#busca').val("");
		$('.multiselect').select2("val", "");
		$('.i-checks input:radio').iCheck('uncheck');
		$('.i-checks input:checkbox').iCheck('uncheck');
	});
}); 	
</script>
