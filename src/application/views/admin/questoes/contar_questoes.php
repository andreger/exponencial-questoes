<?php admin_cabecalho_pagina("Contagem de Questões"); ?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<?= form_open('/admin/contar_questoes', 'id="form-contar-questoes" class="form-horizontal"'); ?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nome do Arquivo</label>
                    <div class="col-sm-10">
                    	<input type="textbox" name="nome" value="<?= $nome ?>" id="nome" class="form-control m-b">
                    	<span class="help-block m-b-none error"><?= form_error('nome'); ?></span>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label">Grandeza das Linhas</label>
                    <div class="col-sm-10">
                    	<?= form_dropdown('linha', $linhas_combo, $linha_selecionada ?: NULL, 'id="linha" class="form-control m-b"'); ?>
                    	<span class="help-block m-b-none error"><?= form_error('linha'); ?></span>
                    </div>
                </div>
                
                <div class="hr-line-dashed"></div>
                
                <div class="form-group">
					<label class="col-sm-2 control-label">Grandeza das Colunas</label>
                    <div class="col-sm-10">
                    	<?= form_dropdown('coluna', $colunas_combo, $coluna_selecionada ?: NULL, 'id="coluna" class="form-control m-b"'); ?>
                    	<span class="help-block m-b-none error"><?= form_error('coluna'); ?></span>
                    </div>
                </div>

				<div class="form-group">
	                <div class="col-sm-offset-2">
	                	<h2>Filtros</h2>

	                	<div><a class='btn btn-white' href='#' id="limpar-filtro">Limpar Filtros</a></div>
	                </div>
	            </div>

                <div class="form-group">
                	<label class="col-sm-2 control-label">Disciplinas</label>
					<div class="col-sm-10">
						<?= form_multiselect('dis_ids[]', $disciplinas, $filtro['dis_ids'] ?: NULL, 'id="dis_ids" class="multiselect form-control m-b"'); ?>
					</div>
				</div>

				<div class="hr-line-dashed"></div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Bancas</label>
					<div class="col-sm-10">
						<?= form_multiselect('ban_ids[]', $bancas, $filtro['ban_ids'] ?: NULL, 'id="ban_ids" class="multiselect form-control m-b"'); ?>
					</div>
				</div>

				<div class="hr-line-dashed"></div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Anos</label>
					<div class="col-sm-10">
						<?= form_multiselect('pro_anos[]', $anos, $filtro['pro_anos'] ?: NULL, 'id="pro_anos" class="multiselect form-control m-b"'); ?>
					</div>
				</div>

				<div class="hr-line-dashed"></div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Escolaridades</label>
					<div class="col-sm-10">
						<?= form_multiselect('esc_ids[]', $escolaridades, $filtro['esc_ids'] ?: NULL, 'id="esc_ids" class="multiselect form-control m-b"'); ?>
					</div>
				</div>

				<div class="hr-line-dashed"></div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Áreas de Atuação</label>
					<div class="col-sm-10">
						<?= form_multiselect('ara_ids[]', $atuacoes, $filtro['ara_ids'] ?: NULL, 'id="ara_ids" class="multiselect form-control m-b"'); ?>
					</div>
				</div>

				<div class="hr-line-dashed"></div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Status</label>
					<div class="col-sm-10">
						<?= form_multiselect('que_statuses[]', $statuses, $filtro['que_statuses'] ?: NULL, 'id="que_statuses" class="multiselect form-control m-b"'); ?>
					</div>
				</div>

				<div class="hr-line-dashed"></div>

				<div class="form-group">
					<label class="col-sm-2 control-label">Tipos</label>
					<div class="col-sm-10">
						<?= form_multiselect('que_tipos[]', $tipos, $filtro['que_tipos'] ?: NULL, 'id="que_tipos" class="multiselect form-control m-b"'); ?>
					</div>
				</div>

				<div class="hr-line-dashed"></div>

				<div class="form-group">
	                <div class="col-sm-offset-2">
                		<?= get_admin_botao_submit('Gerar XLS', 'gerar'); ?>
                	</div>
                </div>

				<?= form_close(); ?>
			</div>
		</div>
	</div>
</div>

<script>

$(function() {
	<?php $campos = ['dis_ids', 'ban_ids', 'pro_anos', 'esc_ids', 'ara_ids', 'que_statuses', 'que_tipos']; ?>

	<?php foreach ($campos as $ids) : ?>
		$("#<?= $ids ?>").select2();		
	<?php endforeach ?>

	$("#limpar-filtro").click(function(e) {
		e.preventDefault();
		$('.multiselect').select2("val", "");
	});
});

</script>