<?php

$prefixo = isset ( $questao ['que_id'] ) ? 'Editar' : 'Nova';
admin_cabecalho_pagina ( $prefixo . " Questão" );
?>
<?php echo get_mensagem_flash() ;?>

<div class="row margin-top-10">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<?php if(isset ( $questao ['que_id'] )) :?>
				<div style="text-align:right">
					<a class="btn btn-primary" target="_blank" href="<?= get_questao_url($questao['que_id']) ?>"><i class="fa fa-eye"></i> Ver no SQ </a>
					<a class="btn btn-primary" data-toggle="modal" href="#modal-crop"><i class="fa fa-crop"></i> Tratar Imagens </a>
					<a class="btn btn-primary" href="<?= get_historico_questao_url($questao['que_id']) ?>"><i class="fa fa-eye"></i> Histórico </a>
					<form action="<?= get_editar_questao_url() ?>" method="post" style="display: inline;">
						<input type="hidden" name="que_id" value="<?= $questao ['que_id'] ?>" />
						<button class='btn btn-primary' type='submit' name='replicar' value='replicar'><i class="fa fa-copy"></i> Replicar </button>
					</form>
				</div>
				<?php endif ?>

				<?php echo form_open(get_editar_questao_url($questao), 'id="form-questao" class="form-horizontal"'); ?>
				
				<div class="form-group">
					<label class="col-sm-2 control-label"><i class="fa fa-search"></i> </label>
					<div class="col-sm-10">
						<?php echo isset($questao['que_qcon_id']) ? $questao['que_qcon_id'] : 'N/A'; ?>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
			
				<div class="form-group">
					<label class="col-sm-2 control-label">Informe os dados da questão</label>
					<div class="col-sm-10">
						<textarea id="que_texto_base" class="wysisyng" name="que_texto_base">
						<?php 
						if(isset($_POST['que_texto_base'])) {
							echo stripcslashes($_POST['que_texto_base']);
						}
						else if(isset($questao['que_texto_base'])) {
							echo stripcslashes($questao['que_texto_base']);
						}elseif(isset($que_texto_base)){
							echo stripcslashes($que_texto_base);
						}
						?>
						</textarea>
						<span class="help-block m-b-none error"><?php echo form_error('que_texto_base'); ?></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Enunciado</label>
					<div class="col-sm-10">
						<textarea id="que_enunciado" class="wysisyng" name="que_enunciado">
						<?php 
						if(isset($_POST['que_enunciado'])) {
							echo stripcslashes($_POST['que_enunciado']);
						}
						else if(isset($questao['que_enunciado'])) {
							echo stripcslashes($questao['que_enunciado']);
						}
						?>
						</textarea>
						<span class="help-block m-b-none error"><?php echo form_error('que_enunciado'); ?></span>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<!--<div class="form-group">
					<label class="col-sm-2 control-label">Dificuldade</label>
					<div class="col-sm-10">
						<?php //$dificuldade_selecionada = isset($_POST['que_dificuldade']) ? $_POST['que_dificuldade'] : $dificuldade_selecionada ?>
						<?php //echo form_dropdown('que_dificuldade', $dificuldades, $dificuldade_selecionada, 'id="que_dificuldade" class="form-control m-b"'); ?>
					</div>
				</div>
				<div class="hr-line-dashed"></div>-->
				<div class="form-group">
					<label class="col-sm-2 control-label">Status</label>
					<div class="col-sm-10">
						<?php $status_selecionado = isset($_POST['que_status']) ? $_POST['que_status'] : $status_selecionado; ?>
						<?php echo form_dropdown('que_status', $statuses, $status_selecionado, 'id="que_status" class="form-control m-b"'); ?>
						<span class="help-block m-b-none error"><?php echo form_error('que_status'); ?></span>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Tipo</label>
					<div class="col-sm-10">
						<?php $tipo_selecionado = isset($_POST['que_tipo']) ? $_POST['que_tipo'] : $tipo_selecionado ?>
						<?php echo form_dropdown('que_tipo', $tipos, $tipo_selecionado, 'id="que_tipo" class="form-control m-b"'); ?>
						<span class="help-block m-b-none error"><?php echo form_error('que_tipo'); ?></span>
					</div>
				</div>
				<div class="grupo_opcoes">
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Opção A</label>
					<div class="col-sm-10">
						<textarea class="wysisyng opcoes" id="qop_01" name="qop_01">
						<?php 
						if(isset($_POST['qop_01'])) {
							echo stripcslashes($_POST['qop_01']);
						}
						else if(isset($qop_01)) {
							echo stripcslashes($qop_01);
						}
						?>
						</textarea>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Opção B</label>
					<div class="col-sm-10">
						<textarea class="wysisyng opcoes" id="qop_02" name="qop_02">
						<?php 
						if(isset($_POST['qop_02'])) {
							echo stripcslashes($_POST['qop_02']);
						}
						else if(isset($qop_02)) {
							echo stripcslashes($qop_02);
						}
						?>
						</textarea>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Opção C</label>
					<div class="col-sm-10">
						<textarea class="wysisyng opcoes" id="qop_03" name="qop_03">
						<?php 
						if(isset($_POST['qop_03'])) {
							echo stripcslashes($_POST['qop_03']);
						}
						else if(isset($qop_03)) {
							echo stripcslashes($qop_03);
						}
						?>
						</textarea>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Opção D</label>
					<div class="col-sm-10">
						<textarea class="wysisyng opcoes" id="qop_04" name="qop_04">
						<?php 
						if(isset($_POST['qop_04'])) {
							echo stripcslashes($_POST['qop_04']);
						}
						else if(isset($qop_04)) {
							echo stripcslashes($qop_04);
						}
						?>
						</textarea>
					</div>
				</div>
				<div class="hr-line-dashed opcao_5"></div>
				<div class="form-group opcao_5">
					<label class="col-sm-2 control-label">Opção E</label>
					<div class="col-sm-10">
						<textarea class="wysisyng opcoes" id="qop_05" name="qop_05">
						<?php 
						if(isset($_POST['qop_05'])) {
							echo stripcslashes($_POST['qop_05']);
						}
						else if(isset($qop_05)) {
							echo stripcslashes($qop_05);
						}
						?>
						</textarea>
					</div>
				</div>
				</div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Resposta</label>
					<div class="col-sm-10">
						<?php $resposta_selecionada = isset($_POST['que_resposta']) ? $_POST['que_resposta'] : $resposta_selecionada ?>
						<?php echo form_dropdown('que_resposta', $respostas, $resposta_selecionada, 'id="que_resposta" class="form-control m-b"'); ?>
						<span class="help-block m-b-none error"><?php echo form_error('que_resposta'); ?></span>
					</div>
				</div>
				<div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Disciplina</label>
                    <div class="col-sm-10">
                    	<?php $disciplina_selecionada = isset($_POST['dis_id']) ? $_POST['dis_id'] : $disciplina_selecionada ?>
                    	<?php echo form_dropdown('dis_id', $disciplinas, $disciplina_selecionada, 'id="dis_id" class="form-control m-b"'); ?>
                    	<span class="help-block m-b-none error"><?php echo form_error('dis_id'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Assuntos</label>
                    <div class="col-sm-10">
                    	<?php $assuntos_selecionados = isset($_POST['ass_ids']) ? $_POST['ass_ids'] : $assuntos_selecionados ?>
						<?php echo form_multiselect('ass_ids[]', $assuntos, $assuntos_selecionados, 'id="ass_ids" class="multiselect form-control m-b"'); ?>
						<div>
							<a id="arvore-btn" data-toggle="modal" href="#modal-arvore-assuntos">Árvore de assuntos</a>
						</div>
                    </div>
                </div>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Provas</label>
					<div class="col-sm-10">
						<?php $provas_selecionadas = isset($_POST['pro_ids']) ? $_POST['pro_ids'] : $provas_selecionadas ?>
						<?php echo form_multiselect('pro_ids[]', $provas, $provas_selecionadas, 'id="pro_id" class="multiselect form-control m-b"'); ?>
						<span class="help-block m-b-none error"><?php echo form_error('pro_ids'); ?></span>
					</div>
				</div>
				
				<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, REVISOR))): ?>
				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Situação</label>
					<div class="col-sm-10">
						<?php $situacao_selecionada = isset($_POST['que_ativo']) ? $_POST['que_ativo'] : $situacao_selecionada ?>
						<?php echo form_dropdown('que_ativo', $situacoes, $situacao_selecionada, 'id="que_ativo" class="form-control m-b"'); ?>
						<span class="help-block m-b-none error"><?php echo form_error('que_ativo'); ?></span>
					</div>
				</div>
				<?php endif; ?>

				<div class="hr-line-dashed"></div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Procedência</label>
					<div class="col-sm-10">
						<?php $procedencia_selecionada = isset($_POST['que_procedencia']) ? $_POST['que_procedencia'] : $procedencia_selecionada ?>
						<?php echo form_dropdown('que_procedencia', $procedencias, $procedencia_selecionada, 'id="que_procedencia" class="form-control m-b"'); ?>
						<span class="help-block m-b-none error"><?php echo form_error('que_procedencia'); ?></span>
					</div>
				</div>


				<div class="hr-line-dashed"></div>
				
				<div class="form-group">
				<h3 class="text-center">Comentário do professor</h3>
					<label class="col-sm-2 control-label">Comentário</label>
					<div class="col-sm-10">
						<div style="margin-bottom: 10px">
						<?php 
							if(isset($professor_que_comentou) && !empty($professor_que_comentou)) {
								$excluir_link = get_excluir_comentario_questao_admin($questao['que_id'], $destaque['com_id']);
								$botao_excluir = "<a style='margin: 0 20px' class='btn btn-primary' href='$excluir_link' onclick='return confirm(\"Confirma exclusão de comentário?\")'>Excluir comentário</a>";

								ui_alerta("<strong>Questão comentada por " . $professor_que_comentou['nome_completo'] . "</strong>$botao_excluir", ALERTA_INFO, FALSE);
								
							}
						?>
						</div>
						<div style="margin-bottom: 10px">
						<?php $professor_selecionado = isset($_POST['user_id']) ? $_POST['user_id'] : $professor_selecionado ?>
							<?php echo form_dropdown('user_id', $professores, $professor_selecionado, 'id="user_id" class="form-control"'); ?>
						</div>
						<textarea class="wysisyng" name="com_texto" id="com_texto">
						<?php 
						if(isset($_POST['com_texto'])) {
							echo stripcslashes($_POST['com_texto']);
						}
						else if(isset($com_texto)) {
							echo stripcslashes($com_texto);
						}
						?>
						</textarea>
						<span class="help-block m-b-none error"><?php echo form_error('com_texto'); ?></span>
					</div>
				</div>
				<div class="hr-line-dashed"></div>

				<?php if(AcessoGrupoHelper::adicionar_comentarios_videos()): ?>

					<div class="form-group">
						<h3 class="text-center">Comentário em Vídeo</h3>
						<label class="col-sm-2 control-label">Professor</label>
						<div class="col-sm-10">
							<div style="margin-bottom: 10px">
							<?php $cvi_user_id = isset($_POST['cvi_user_id']) ? $_POST['cvi_user_id'] : $cvi_user_id ?>
								<?php echo form_dropdown('cvi_user_id', $professores, $cvi_user_id, 'id="cvi_user_id" class="form-control"'); ?>
								<span class="help-block m-b-none error"><?php echo form_error('cvi_user_id'); ?></span>
							</div>
							
						</div>

						<label class="col-sm-2 control-label">URL do Vídeo</label>
						<div class="col-sm-10">
							<?php $cvi_url = isset($_POST['cvi_url']) ? $_POST['cvi_url'] : $cvi_url ?>
							<?php echo form_input('cvi_url', isset($cvi_url) ? $cvi_url : '','id="cvi_url" placeholder="Url do Vimeo" class="form-control m-b"'); ?>
							<span class="help-block m-b-none error"><?php echo form_error('cvi_url'); ?></span>
						</div>
					</div>
					<div class="hr-line-dashed"></div>

				<?php endif; ?>
				
				<div class='col-sm-4 col-sm-offset-2'>
					<button class='btn btn-white'><a href='<?php echo get_listar_questoes_url() ?>'>Cancelar</a></button>
					<input id="salvar" class='btn btn-primary' type='submit' name='salvar' value='Salvar'>
				</div>


		    	<div style='clear:both'></div>
	    		
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

<div id="modal-crop"  class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class='modal-content modal-form-content'>
			<div class='modal-header'>
				<button type='button' class='close' data-dismiss='modal'>&times;</button>
				<h4 class='modal-title'>Tratar Imagens da Questão</h4>
			</div>
			<div class='modal-body'>
			
				<div class='panel blank-panel'>
					<div class="row">
						<?php 
						$hidden = false;
						foreach ($imagens_nao_tratadas as $imagem) :?>
						<div class="imagem-questao" id="<?= $imagem['qim_id'] ?>-im" style="display: <?= $hidden ? 'none' : 'block' ?>">
		                    <div class="col-md-12">
		                        <div class="image-crop">
		                            <img src="<?= $imagem['qim_url'] ?>">
		                        </div>
		                   	<!-- </div>
		                    <div class="col-md-6">
		                        <h4>Pré-visualização</h4>
		                        <div class="img-preview img-preview-sm"></div>
		                    -->
		                        <div style="margin-top: 20px" class="btn-group">
		                        <form id="<?= $imagem['qim_id'] ?>-form">
								    <input type="hidden" id="<?= $imagem['qim_id'] ?>-x" name="x">                     
								    <input type="hidden" id="<?= $imagem['qim_id'] ?>-y" name="y">
									<input type="hidden" id="<?= $imagem['qim_id'] ?>-width" name="width">
									<input type="hidden" id="<?= $imagem['qim_id'] ?>-height" name="height">
		                            <a href="#" onclick="salvar(<?= $imagem['qim_id'] ?>)" class="btn btn-primary">Cortar e Marcar como Tratada</a>
		                            <input type='button' value='Fechar' data-dismiss='modal' class='btn btn-white'>
		                             <span id="<?= $imagem['qim_id'] ?>-msg"></span>
		                        </form>
		                        </div>
		                        
		                    </div>
		                </div>
	                    <?php $hidden = true; ?>
	                   	<?php endforeach; ?>
	                   	<div class="imagem-questao" style="display: <?= $hidden ? 'none' : 'block' ?>">
	                   		<div style="text-align:center">
	                   		
	                   			<div style="margin: 40px;">Não há imagens para tratar nessa questão.</div>
	                   	
			                    <div class="col-md-12">
		                             <input type='button' value='Fechar' data-dismiss='modal' class='btn btn-white'>
			                    </div>
		                    
		                    </div>
		                </div>
	                </div>
	            </div>      
			</div>
		</div>
	</div>
</div>
<div id="modal-arvore-assuntos" class="modal fade" aria-hidden="true">
	<div id="p2" class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Selecionar Assuntos</h4>
			</div>
			<div class="modal-body">
				<div class="alert alert-info">
	                A quantidade de questões mostrada para cada assunto leva em conta o total de questões cadastradas em nossa base, sem filtros aplicados
	            </div>
				<div class="arvore-control">
					<a id="expand-all">Expandir tudo</a>
					<a id="collapse-all">Recolher tudo</a>
				</div>
				<div id="p2-corpo"></div>
				<div class="botoes" style="margin-top: 20px">
					<a class="btn primary btn-default" data-dismiss="modal" id="adicionar">Adicionar</a>
					<a class="btn primary btn-default" data-dismiss="modal" id="cancelar">Cancelar</a>
				</div>
			</div>		
		</div>
	</div>
</div>
<script>
	dis_ids_name = 'dis_id';
	ass_ids_name = 'ass_ids';
</script>
<?php $this->view('modal/filtro_scripts') ?>

<script>
function salvar(id) 
{
	$("#"+id+"-msg").text("Aguarde. Salvando imagem...");
	
	var data = $("#"+id+"-form").serializeArray();

	data.push({name: 'imagem_id', value: id});
	
	$.post("/questoes/admin/xhr_tratar_imagem", data)
		.done(function() {
			proxima();
		});
}

function proxima()
{
	$('.imagem-questao[style*="display: block"]').remove();
	$('.imagem-questao:first').css('display','block');
}

$(function() {
	<?php foreach ($imagens_nao_tratadas as $imagem) : ?>
		$("#<?= $imagem['qim_id'] ?>-im .image-crop > img").cropper({
			done: function(data) {
				$('#<?= $imagem['qim_id'] ?>-x').val(data.x);
				$('#<?= $imagem['qim_id'] ?>-y').val(data.y);
				$('#<?= $imagem['qim_id'] ?>-width').val(data.width);
				$('#<?= $imagem['qim_id'] ?>-height').val(data.height);
			}
		});
	<?php endforeach; ?>
	
	$('#salvar').click(function() {
		$('#que_texto_base').val($('#que_texto_base').code());
		$('#que_enunciado').val($('#que_enunciado').code());
		$('#qop_01').val($('#qop_01').code());
		$('#qop_02').val($('#qop_02').code());
		$('#qop_03').val($('#qop_03').code());
		$('#qop_04').val($('#qop_04').code());
		$('#qop_05').val($('#qop_05').code());
	});
	
	if($("#que_tipo").val() == <?php echo CERTO_ERRADO ?>) {
		$(".grupo_opcoes").slideUp();
	}
	
	$('.wysisyng').summernote({
         onImageUpload: function(files) {
             sendFile(files[0], $(this));
         }
	});
	$("#que_resposta").remoteChained({
	    parents : "#que_tipo",
	    url : "<?php echo get_ajax_get_que_resposta_url(); ?>"
	});
	
	/*$("#ass_ids").remoteChained({
	    parents : "#dis_id",
	    url : "<?php echo get_ajax_get_ass_id_url(1); ?>"
	});*/

	$('#dis_id').change(function () {
		var assIds = $('#ass_ids').val();
   		var str = "";
   		$('#ass_ids').html("<option value='' disabled >Carregando...</option>");

		var ids_a = [];
       	$('#dis_id option:selected').each(function(i, selected){
       		ids_a[i] = $(selected).val();
       	});

       	var ids = ids_a.join('-');
		if(ids){
			$.get("<?= base_url('/main/ajax_get_assuntos/') ?>/" + ids, function(data) {
				adicionar_itens($('#ass_ids'), data, assIds);
			}, 'json');
		}
    });

	$(".multiselect").chosen({
		disable_search_threshold: 10,
		no_results_text: "Nenhum item com:",
		placeholder_text_multiple: "Selecione alguns itens..."
	});
	$(".multiselect").on("change", function(event, data) {
	    $(this).trigger("chosen:updated");
	});
	$("#que_tipo").on("change", function() {
	    if($("#que_tipo").val() == <?php echo CERTO_ERRADO ?>) {
	    	$(".grupo_opcoes").slideUp();
	    }else if($("#que_tipo").val() == <?php echo MULTIPLA_ESCOLHA ?>) {
	 		$(".grupo_opcoes").slideDown();
			 $(".opcao_5").hide();
		}else if($("#que_tipo").val() == <?php echo MULTIPLA_ESCOLHA_5 ?>) {
			$(".grupo_opcoes").slideDown();
			$(".opcao_5").show();
		}

	});

	$("#que_tipo").trigger("change");
	
	$('#pro_id').chosen({
		search_contains: true
	});

	combos_info = ['ass_ids'];

	combos_selecionados = [
		[ <?php echo implode(",", $_POST['ass_ids']?:$assuntos_selecionados) ?> ]
	];

	$("#dis_id").trigger("change");
});
</script>