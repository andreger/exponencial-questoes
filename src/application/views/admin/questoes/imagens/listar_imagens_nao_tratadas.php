<?php 
$texto = $total == 1 ? "Imagem não tratada" : "Imagens não tratadas"; 
admin_cabecalho_pagina($total . " " . $texto); 
exibir_mensagem();
?>

<div class="row">
		<div class="col-lg-12" style="margin: 30px 0">
			<div class="form-group">
				<form method="post">
					<label class="col-sm-3 control-label">Filtrar imagens por questão:</label>
                	<div class="col-sm-3">
                    	<input type="text" class="form-control" name="que_id" value="<?php echo isset($questao_id) ? $questao_id : '' ?>"/>
                    </div>
	                
	                <?php if($imagens) : ?>
	                	<?php foreach ($imagens as $imagem) : ?>
	                		<input type="hidden" name="imagens_a_tratar[]" value="<?php echo $imagem['qim_id']?> ">
	                	<?php endforeach; ?>
	                <?php endif ?>

                    <div class="col-sm-2">
                    	<button type="submit" name="submit" value="true" class="btn btn-primary">Filtrar</button>&nbsp;
                    </div>
                    <div class="col-sm-4" style="text-align:right">
                    	<button type="submit" name="submit-tratar-todas" value="true" class="btn btn-primary">Marcar Todas como Tratadas</button>
                    </div>
	                
	            </form>
	        </div>
     </div>
</div>
	

<div class="row">
	<div class="col-lg-12">
		<div class="ibox-content">
			<?php if(!$imagens) : ?>

				<div style="margin: 30px">Não foram encontradas imagens que necessitam tratamento.</div>

			<?php else : ?>

				<div class="ibox">
					<?php echo $links; ?>
				</div>

				<?php $i = 1; foreach ($imagens as $imagem) : ?>
				<div class="" style="margin-bottom: 30px;">
					<?php $num = $i + $offset ?>
					<h3><?php echo "$num - Questão " . $imagem['que_id'] ?></h3>
					<div><img src="<?php echo $imagem['qim_url'] . '?' . time() ?>"></div>
					<div style="margin: 10px 0">
						<a class="btn btn-primary " href="<?php echo get_marcar_imagem_como_tratada_url($imagem['qim_id'], $offset) ?>">Marcar como Tratada</a>
						<a class="btn btn-primary " href="<?php echo get_tratar_imagem_url($imagem['qim_id'], $offset) ?>">Tratar Imagem</a>
					</div>	
				</div>
				<div class="hr-line-dashed"></div>
				<?php $i++; endforeach ?>

				<div class="ibox" style="padding-bottom: 10px">
					<?php echo $links; ?>
				</div>

			<?php endif; ?>
		</div>
	</div>
</div>
