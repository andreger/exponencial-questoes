<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title  back-change">
                <h5>Confirmar Imagem</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="image-crop">
                            <img src="<?php echo $pre_imagem ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="btn-group">
                        <form action=" <?= get_confirmar_tratar_imagem_url($imagem['qim_id'], $offset); ?> " method="post">
                            <button type="submit" name="confirmar" class="btn btn-primary">Marcar como tratada</button>&nbsp;
                            <a href="<?php echo get_tratar_imagem_url($imagem['qim_id'], $offset); ?>" class="btn btn-primary">Voltar</a>
                        </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>