<?php admin_cabecalho_pagina("Imagens Tratadas") ?>
<div>
    <div class='wrapper wrapper-content animated fadeInRight'>
        <div class='row'>
            <div class=''>
                <div class='ibox-content'>
                    <div style='padding-bottom: 10px'>
                        <form method='post'>
                            <div class='col-lg-3'>
                                Período:
                                <input type="text" id="daterange" name="daterange" class="form-control m-b" style="height: 37px">
                                <input type="hidden" name="inicio" id="inicio">
                                <input type="hidden" name="fim" id="fim">
                            </div>
                            <div class='col-lg-3'>
                                Usuário:
                                <?php echo form_dropdown('usuario_id', $usuarios, $usuario_id ?: null, "id='aluno_id' class='form-control m-b'"); ?>
                            </div>

                            <div class='col-lg-1'>
                                <br>
                                <input type='submit' name='filtrar' value='Filtrar' class='btn btn-primary'>
                            </div>
                        </form>
                    </div>
                    <div class='row' style='clear:both; padding-top: 10px'>
                        <h3>Imagens tratadas no período: <?= count($imagens) ?></h3>
                    </div>
                    <div class='row' style='clear:both; padding-top: 10px'>
                        <div class='table-responsive'>
                            <table class='table table-striped table-bordered table-hover' >
                                <thead>
                                    <tr>
                                        <th>Imagem ID</th>
                                        <th>Questão ID</th>
                                        <th>Data/Hora Tratamento</th>
                                        <th>Usuário</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($imagens as $imagem) : ?>
                                    <tr>
                                        <td><?= $imagem['qim_id'] ?></td>
                                        <td><?= $imagem['que_id'] ?></td>
                                        <td><?= $imagem['qim_data_tratamento'] ?></td>
                                        <td><?= get_usuario_nome_completo($imagem['qim_usu_id_tratamento']) ?></td>
                                    </tr>
                                    <?php endforeach ?>
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() { 
    $("#daterange").daterangepicker({
        "locale": 
        {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Filtrar",
            "cancelLabel": "Cancelar",
            "fromLabel": "De",
            "toLabel": "Até",
            "customRangeLabel": "Personalizado",
            "weekLabel": "S",
            "daysOfWeek": [
                "Dom",
                "Seg",
                "Ter",
                "Qua",
                "Qui",
                "Sex",
                "Sáb"
            ],
            "monthNames": [
                "Janeiro",
                "Fevereiro",
                "Março",
                "Abril",
                "Maio",
                "Junho",
                "Julho",
                "Agosto",
                "Setembro",
                "Outubro",
                "Novembro",
                "Dezembro"
            ],
            "firstDay": 1,
        },
        "startDate": '<?= $inicio ?>',
        "endDate": '<?= $fim ?>',
        "alwaysShowCalendars": true,
        ranges: {
           'Hoje': [moment(), moment()],
           'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
           'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
           'Este Mês': [moment().startOf('month'), moment().endOf('month')],
           'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'Este Ano': [moment().startOf('year'), moment().endOf('year')]
        }
    });

    $('#daterange').on('apply.daterangepicker', function(ev, picker) {
        $('#inicio').val(picker.startDate.format('YYYY-MM-DD'));
        $('#fim').val(picker.endDate.format('YYYY-MM-DD'));
    });
});
</script>
