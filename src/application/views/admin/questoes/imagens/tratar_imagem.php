<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title  back-change">
                <h5>Imagem</h5>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="image-crop">
                            <img src="<?php echo $imagem['qim_url'] ?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4>Pré-visualização</h4>
                        <div class="img-preview img-preview-sm"></div>
                       
                        <div class="btn-group">
                        <form action="" method="post">
						    <input type="hidden" id="x" name="x">                     
						    <input type="hidden" id="y" name="y">
							<input type="hidden" id="width" name="width">
							<input type="hidden" id="height" name="height">
                            <button type="submit" name="submit" class="btn btn-primary">Cortar</button>&nbsp;
                            <a href="<?php echo get_listar_imagens_nao_tratadas_url($offset)?>" class="btn btn-primary">Voltar</a>
                        </form>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){

	var $image = $(".image-crop > img")
	$($image).cropper({
		preview: ".img-preview",
		done: function(data) {
			$('#x').val(data.x);
			$('#y').val(data.y);
			$('#width').val(data.width);
			$('#height').val(data.height);
		}
	});

});

</script>