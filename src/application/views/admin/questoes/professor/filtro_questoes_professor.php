<div class="row">
	
    <div class="ibox">
        <div class="col-md-12" style="margin: 10px 0">
            <div class="ibox-content">
                <div class='row'>
                    <div class='col-lg-6'>
                        <div>
                            <a href="#" class="btn btn-white toggle-filtros-btn"><i class="fa fa-filter"></i> Filtrar</a>
                            
                            <?php if(tem_acesso([ADMINISTRADOR, COORDENADOR])) : ?>
                                <a href="#" class="btn btn-white" id="btn-aprovar-todas"><i class="fa fa-thumbs-up"></i> Aprovar Selecionadas</a>
                                <a href="#" class="btn btn-white" id="btn-reprovar-todas"><i class="fa fa-thumbs-down"></i> Rejeitar Selecionadas</a>
                                <a href="#" class="btn btn-white" id="btn-selecionar-todas"><i class="fa fa-check"></i> Selecionar Todas</a>
                                <a href="#" class="btn btn-white" id="btn-deselecionar-todas" style="display:none"><i class="fa fa-times"></i> Desmarcar Todas</a>
                            <?php endif ?>
                            <?php if(AcessoGrupoHelper::copiar_ids_questoes()): ?>
                                <a href="#" class="btn btn-white tooltipster" id="btn-copiar-ids-questoes" title="Copia os ids de todas as questões presentes na busca"><i class="fa fa-copy"></i></a>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                </div>
                
                <div class='row admin-filtro' id="filtro" style="display: none">
                    <form method='post' action="/questoes/admin/listar_questoes_professor" class="form-horizontal" id="frm-filtros">
        
                        <div class='col-md-6'>		
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Disciplinas</label>

                                <div class="col-sm-10">
                                    <?= form_multiselect('q_disciplinas[]', $disciplinas_combo, $filtros['disciplinas'], 'id="q_disciplinas" class="multiselect form-control m-b"'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Assuntos</label>

                                <div class="col-sm-10">
                                    <?php echo form_multiselect('q_assuntos[]', $assuntos_combo, $filtros['assuntos'], 'id="q_assuntos" class="multiselect form-control m-b"'); ?>                            
                                    <a style="position:relative;top: -15px" id="arvore-btn" data-toggle="modal" href="#modal-arvore-assuntos">Árvore de assuntos</a>
                                </div>
                            </div>
                        </div>

                        <?php if(tem_acesso([ADMINISTRADOR, COORDENADOR])) : ?>
                        
                            <div class='col-md-6'>		
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Professores</label>

                                    <div class="col-sm-10">
                                        <?= form_multiselect('q_professores[]', $professores_combo, $filtros['professores'], 'id="q_professores" class="multiselect form-control m-b"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row"></div>

                        <?php endif ?>
                        
                        <div class='col-md-6'>		
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Aprovação</label>

                                <div class="col-sm-10">
                                    <?= form_multiselect('q_aprovacao[]', $aprovacao_combo, $filtros['aprovacao'], 'id="q_aprovacao" class="multiselect form-control m-b"'); ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class='col-md-6'>		
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Procedência</label>

                                <div class="col-sm-10">
                                    <?= form_multiselect('q_procedencia[]', $procedencia_combo, $filtros['procedencia'], 'id="q_procedencia" class="multiselect form-control m-b"'); ?>
                                </div>
                            </div>
                        </div>

                        <div class='col-md-12'>
                            <div class="form-group">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-11">
                                    <button type='submit' id="sumit-btn" class='btn btn-primary' name='filtrar' value='1'>
                                        <i class="fa fa-filter"></i> Filtrar
                                    </button>
                                    <a href="#" id="limpar-filtros-btn" class='btn btn-white limpar-filtros-btn'><i class="fa fa-eraser"></i> Limpar Filtros</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    dis_ids_name = "q_disciplinas";
    ass_ids_name = 'q_assuntos';

    <?php if(isset($array_ids)) : ?>
		<?php foreach ($array_ids as $id => $nome) : ?>
       		$("<?php echo $id; ?>").select2({
	           	placeholder: "<?php echo $nome ?>"
       		});
       	<?php endforeach; ?>
   	<?php endif;?>

    $('#q_disciplinas').change("select2:open", function () { 
        var assIds = $('#q_assuntos').val();
        var str = "";
        $('#q_assuntos').html("<option value='' disabled >Carregando...</option>");
        
        var ids_a = [];
        $('#q_disciplinas option:selected').each(function(i, selected){ 
            ids_a[i] = $(selected).val(); 
        });

        var ids = ids_a.join('-');
        $.get("<?= base_url('/main/ajax_get_assuntos/') ?>/" + ids, function(data) {
            $.each(data,function(index,value){
                var sel = ($.inArray(data[index].id, assIds) != -1) ? ' selected="selected"' : '';
                str += '<option value="' + data[index].id+ '" ' + sel + ' >'+data[index].text+'</option>';
            });
            $('#q_assuntos').html(str);
        }, 'json');
    });

</script>
<?php $this->view('modal/filtro_scripts') ?>