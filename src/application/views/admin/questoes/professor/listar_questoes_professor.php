<?php admin_cabecalho_pagina("Questões de Professores"); ?>
<?php exibir_mensagem() ?>
<?php echo get_mensagem_flash() ;?>

<div class="questoes">
	<div id="msgbox"><?php if(isset($erro)) { ui_alerta($erro, ALERTA_ERRO); }?></div>
	
	<?php $this->view('admin/questoes/professor/filtro_questoes_professor'); ?>
	
	<?php if(isset($questoes)) : ?>
	
		<form method="post" id="frm-massa" action="/questoes/admin/aprovar_rejeitar_questoes_selecionadas">
		<input type="hidden" id="hid-tipo" name="hid-tipo">
		<div class="row">
			
			<?php if(!$questoes) : ?>
				<div style="margin: 20px;text-align: center">Não foram encontradas questões de professores.</div>
			<?php else : ?>
				<div style="margin-top: 10px">
					<div class='col-lg-6'>
						Registros encontrados: <strong><?= $total ?></strong>
					</div>
					<div class='col-lg-6 text-right'><?= $links ?></div>
				</div>
			
				<?php foreach ($questoes as $questao) : ?>
					<div id="questao_<?php echo $questao['que_id'];?>" class="col-lg-12 questao">
						<div class="ibox">
							<?php $this->view('questoes/cabecalho/cabecalho', array('questao' => $questao, 'is_revisar' => tem_acesso([ADMINISTRADOR, COORDENADOR]))); ?>
							
							<div class="ibox-content">
								<div style="margin-bottom: 20px;text-align: left;">
								</div>
								<div style="margin-bottom: 20px;text-align: right;">
									<?php if($questao['que_procedencia'] == QUESTAO_INEDITA): ?>
										<span style="float: left;color: #005fab;">- Questão Inédita - Professor <?= get_usuario_nome_completo($questao['usu_id']) ?>, <?= converter_para_yyyy($questao['que_data_criacao']); ?> </span>
									<?php elseif($questao['que_procedencia'] == QUESTAO_ADAPTADA): ?>
										<span style="float: left;color: #005fab;">- Questão Adaptada - Professor <?= get_usuario_nome_completo($questao['usu_id']) ?>, <?= converter_para_yyyy($questao['que_data_criacao']); ?> </span>
									<?php elseif($questao['que_procedencia'] == QUESTAO_FIXACAO): ?>
										<span style="float: left;color: #005fab;">- Exercício de Fixação - Professor <?= get_usuario_nome_completo($questao['usu_id']) ?>, <?= converter_para_yyyy($questao['que_data_criacao']); ?> </span>
									<?php else: ?>
										<span style="float: left;color: #005fab;">- Professor <?= get_usuario_nome_completo($questao['usu_id']) ?></span>
									<?php endif; ?>

									<?php if(is_administrador()): ?>
										<a class="btn btn-outline btn-default" href="<?php echo get_aprovar_questao_url($questao['que_id']) ?>" ><i class="fa fa-thumbs-up"></i> Aprovar </a>
										<a class="btn btn-outline btn-default" href="<?php echo get_reprovar_questao_url($questao['que_id']) ?>"><i class="fa fa-thumbs-down"></i> Rejeitar </a>
									<?php endif; ?>

									<?php if($questao['que_status'] == CANCELADA) : ?>
									<span style="margin: 0 20px; color: red"><i class="fa fa-times"></i> Questão Anulada</span>
									<?php endif; ?>
									
									<?php if($questao['que_status'] == DESATUALIZADA) : ?>
									<?= get_botao_resposta('Questão Desatualizada', 'resposta-desatualizada.png', 'resposta-desatualizada'); ?>
									<?php endif; ?>
									
									<a class="btn btn-outline btn-default btn-edicao-rapida" href="#" data-id="<?= $questao['que_id'] ?>"><i class="fa fa-pencil"></i> Edição Rápida </a>
									
									<?php $this->view('questoes/barra_superior/limpar_formatacao') ?>

									<a class="btn btn-outline btn-default" href="<?php echo get_editar_questao_url($questao) ?>"><i class="fa fa-pencil"></i> Editar Questão </a>
									
								</div>
								
								<div id="edicao-rapida-<?= $questao['que_id']?>" style="display:none; background-color: #eee; padding: 20px"></div>
								
								<div class="questao-texto-base texto-questao"><?= formatar_texto_de_questao($questao['que_texto_base']); ?></div>
								<div class="questao-enunciado texto-questao"><?= formatar_texto_de_questao($questao['que_enunciado']); ?></div>
								
								<ul class="questao-opcoes texto-questao">
									<?php if($questao['que_tipo'] == MULTIPLA_ESCOLHA || $questao['que_tipo'] == MULTIPLA_ESCOLHA_5) :?>
										<?php $i = 0; foreach ($questao['opcoes'] as $opcao) : $i++; ?>
											<?php if(trim($opcao['qop_texto']) != "" && trim($opcao['qop_texto']) != "<p><br></p>"): ?>
												<li>
													<div class="radio i-checks">
														<label <?php echo $i == $questao['que_resposta'] ? ' class="revisar-resposta-certa"' : '' ?> for="qop_<?php echo $questao['que_id'] . $opcao['qop_ordem']; ?>">
															<?php echo form_radio('opcao_questao_' . $questao['que_id'], 
																$opcao['qop_ordem'], 
																false, 'disabled=disabled id="qop_' . $questao['que_id'] . $opcao['qop_ordem'] . '"');?>
															<?php echo get_letra_opcao_questao($opcao['qop_ordem']) . ') ' . stripcslashes($opcao['qop_texto']); ?>
														</label>
													</div>
												</li>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php elseif($questao['que_tipo'] == CERTO_ERRADO) :?>
										<li>
											<div class="radio i-checks">
												<label <?php echo CERTO == $questao['que_resposta'] ? ' class="revisar-resposta-certa"' : '' ?> for="qop_<?php echo $questao['que_id']; ?>_certo">
													<?php echo form_radio('opcao_questao_' . $questao['que_id'], 
														CERTO, 
														false, 
														'disabled=disabled id="qop' . $questao['que_id'] . '_certo"');
													?>
													Certo
												</label>
											</div>
										</li>
										<li>
											<div class="radio i-checks">
												<label <?php echo ERRADO == $questao['que_resposta'] ? ' class="revisar-resposta-certa"' : '' ?> for="qop_<?php echo $questao['que_id']; ?>_errado">
													<?php echo form_radio('opcao_questao_' . $questao['que_id'], 
														ERRADO, false, 
														'disabled=disabled id="qop' . $questao['que_id'] . '_errado"');
													?>
													Errado
												</label>
											</div>
										</li>
									<?php endif; ?>
								</ul>
							</div>
							
							<div class="ibox-footer">

							</div>
                            
						</div>
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
		</form>
		
		<div class="ibox text-right" style="padding-bottom: 10px">
			<?php echo $links; ?>
		</div>
	<?php endif; ?>
</div>
<?php $this->view("questoes/filtro/arvore_assuntos", ['is_modal' => true]); ?>
<script>

$(function() {

	$('.btn-edicao-rapida').click(function(e) {
		e.preventDefault();
		var questao_id = '#edicao-rapida-' + $(this).data('id');
		$(questao_id).toggle();
		
		$(questao_id).html("Carregando...");
		
		$.get('/questoes/questoes_xhr/carregar_edicao_rapida/'+$(this).data('id'), function(data) {
			$(questao_id).html(data);
		});
	});

	$('.btn-remover-formatacao').click(function(e){
		e.preventDefault();
		var id = $(this).data('id');

		$.ajax({
			url: "<?php echo get_ajax_main_remover_formatacao_alternativas_url(); ?>",
			method: "POST",
			cache: false,
			data: { id: id }
		})
		.done(function( msg ) {
			alert(msg);
		});
	});

	$("#btn-aprovar-todas").click(function () {
		$("#hid-tipo").val(1);
		$("#frm-massa").submit();
	});

	$("#btn-reprovar-todas").click(function () {
		$("#hid-tipo").val(3);
		$("#frm-massa").submit();
	});

	$("#btn-selecionar-todas").click(function () {
		$('.questao-opcao').prop('checked', true);
		$(this).hide();
		$('#btn-deselecionar-todas').show();
	});

	$("#btn-deselecionar-todas").click(function () {
		$('.questao-opcao').prop('checked', false);
		$(this).hide();
		$('#btn-selecionar-todas').show();
	});

	$('.favorito').click(function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		var classe = $(this).find('i',0).attr('class');

		if(classe == "fa fa-star") {
			$(this).find('i',0).attr('class', 'fa fa-star-o');
		}
		else {
			$(this).find('i',0).attr('class', 'fa fa-star');
			
			//Favoritar também faz acompanhar
			var bell_class = $('#acompanhar-'+id).attr('class');
			if(bell_class == 'fa fa-bell-slash-o'){
				$('#acompanhar-'+id).attr('class', 'fa fa-bell');
			}
		}

		$.get('/questoes/questoes_xhr/marcar_desmarcar_favorito/'+id+'/<?= QUESTAO_FAVORITA ?>');
	});

	$('.acompanhar').click(function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		var classe = $(this).find('i',0).attr('class');

		if(classe == "fa fa-bell-slash-o") {
			$(this).find('i',0).attr('class', 'fa fa-bell');
		}
		else {
			$(this).find('i',0).attr('class', 'fa fa-bell-slash-o');
		}

		$.get('/questoes/questoes_xhr/marcar_desmarcar_favorito/'+id+'/<?= QUESTAO_ACOMPANHADA ?>');
	});
});

</script>