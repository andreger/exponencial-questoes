<?php
	$prefixo = isset($disciplina['dis_id']) ? 'Editar' : 'Nova'; 
	admin_cabecalho_pagina($prefixo . " Disciplina"); 
?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<?php echo form_open(get_editar_disciplina_url($disciplina), 'id="form-disciplina" class="form-horizontal"'); ?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nome</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="dis_nome" value="<?php echo isset($disciplina['dis_nome']) ? $disciplina['dis_nome'] : ''; ?>"/>
                    	<span class="help-block m-b-none error"><?php echo form_error('dis_nome'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-10">
                        <?= form_dropdown('dis_ativo', $statuses, isset($disciplina['dis_ativo']) ? $disciplina['dis_ativo'] : NULL , 'id="dis_ativo" class="form-control m-b"'); ?>
                        <span class="help-block m-b-none error"><?php echo form_error('dis_ativo'); ?></span>

                        <?php if($simulados_atrelados) : ?>
                            <div class="alert alert-warning">
                                <i class="fa fa-exclamation-triangle"></i>
                                <strong>ATENÇÃO: </strong> Esta disciplina está associada a um ou mais simulados. Caso seja inativada todas as questões desta disciplina serão removidas dos simulados.
                                <?php
                                /****************************************************************************
                                 * Permitir inativar disciplinas
                                 * https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1898
                                 ***************************************************************************/
                                ?>
                                <ul>
                                <?php foreach ($simulados_atrelados as $simulado) : ?>
                                    <li><?= $simulado['sim_nome'] ?></li>
                                <?php endforeach ?>
                                </ul>
                            </div>

                        <?php endif ?>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <?php admin_botoes_salvar_cancelar(get_listar_disciplinas_url())?>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>