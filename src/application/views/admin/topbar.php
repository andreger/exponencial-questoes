<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
	<div class="navbar-header">
		<div class="navbar-bemvindo">
			<a title="Retrair/Voltar o menu lateral" class="navbar-minimalize minimalize-styl-1 btn btn-primary" href="#"><i class="fa fa-bars"></i></a>
			<?php 
			if(is_visitante()) {
				echo "Bem-vindo(a). Bons estudos!";
			}
			else {
				echo "Bem-vindo(a), " . get_usuario_nome_exibicao(get_current_user_id()) . ". Bons estudos!";
			}
			?>
		</div>
	</div>
	
	<ul class="nav navbar-top-links navbar-right">

		<?php if(!tem_acesso(array(ADMINISTRADOR, COORDENADOR, PROFESSOR, CONSULTOR, REVISOR, ASSINANTE_SQ))) : ?>
		<li>
			<a href="/sistema-de-questoes#planos">
				<?= get_tema_image_tag('plano-assinatura1.png', null, null, null, 'vertical-bottom') ?> <strong>Planos de Assinatura</strong>
			</a>
		</li>
		<?php endif ?>

		<?php if(is_usuario_logado()): ?>
			<li>
				<?php KLoader::view('header/barra_superior/notificacoes/notificacoes') ?>
			</li>
		<?php endif; ?>

		<li>
			<a href="/">
				<?= get_tema_image_tag('sq-ir.png', null, null, null, 'vertical-bottom') ?> Ir para site
			</a>
		</li>
		
		<?php if(tem_acesso(array(
				ADMINISTRADOR, PROFESSOR, CONSULTOR, REVISOR
				))) : ?>
		
			<?php if(!is_questoes_admin()) : ?>
			<li>
				<a href="<?php echo base_url('admin') ?>">
					<?= get_tema_image_tag('sq-admin.png') ?> Administração
				</a>
			</li>
			<?php endif; ?>
			
			<?php if(is_questoes_admin()) : ?>
			<li>
				<a href="<?php echo base_url('main/resolver_questoes') ?>">
					<i class="fa fa-dot-circle-o"></i> Sistema de Questões
				</a>
			</li>
			<?php endif; ?>
		
		<?php endif;?>
		
		<?php if(is_usuario_logado()) : ?>		
		<li>
			<a href="<?php echo base_url('login/logout') ?>">
				<?= get_tema_image_tag('sq-sair.png') ?> Sair
			</a>
		</li>
		<?php else : ?>
		<li>
			<a href="/cadastro-login?ref=<?= base_url(uri_string()); ?>">
				Login
			</a>
		</li>
		<?php endif ?>
	</ul>
</nav>

<script>
$(function () {
	$('.navbar-minimalize').click(function () {
		if(sessionStorage.miniNavbar && sessionStorage.miniNavbar == 1) {
			sessionStorage.miniNavbar = 0;
		}
		else {
			sessionStorage.miniNavbar = 1;
		}
	});
});

if(sessionStorage.miniNavbar && sessionStorage.miniNavbar == 1) {
	$('body').addClass('mini-navbar');
}
else {
	$('body').removeClass('mini-navbar');	
}
</script>