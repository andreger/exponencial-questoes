<div class="row">
    <div class="col-lg-6">
        <div class="ibox ">
            <div>{caixa_azul}</div>

            <div class="ibox-content">

            <?php 
            /* Exibe nestable se disciplina possuir assuntos raízes ativos */
            if($assuntos) : ?>
            
                <div class="dd" id="nestable">
                    <ol class="dd-list">
                        {assuntos}
                        <li class="dd-item" data-ass_id="{ass_id}">
                            <div class="dd-handle">{ass_nome}</div>
                        </li>
                        {/assuntos}
                    </ol>
                </div>
            
            <?php
            /* Exibe mensagem de erro se se disciplina não possuir assuntos raízes ativos */
            else : ?>

                <div>Esta disciplina não possui assuntos ativos cadastrados.</div>

            <?php endif ?>

            </div>
        </div>
    </div>
</div>

<script>
$(function () {
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');

        if (window.JSON) {
            var assuntos = window.JSON.stringify(list.nestable('serialize'));
            $.post('/questoes/xhr/atualizar_ordem_taxonomia', {'assuntos' : assuntos, disciplina_id: $('#dis_id').val() });
        }
    };

    $('#nestable').nestable({
        group: 1
    }).on('change', updateOutput);
});
</script>   