<?php
	$prefixo = isset($assunto['ass_id']) ? 'Editar' : 'Novo'; 
	admin_cabecalho_pagina($prefixo . " Assunto"); 
?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<?php echo form_open(get_editar_assunto_url($assunto), 'id="form-assunto" class="form-horizontal"'); ?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nome</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="ass_nome" value="<?php echo isset($assunto['ass_nome']) ? $assunto['ass_nome'] : ''; ?>"/>
                    	<span class="help-block m-b-none error"><?php echo form_error('ass_nome'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Disciplina</label>
                    <div class="col-sm-10">
                    	<?php echo form_dropdown('dis_id', $disciplinas, $disciplina_selecionada, 'id="dis_id" class="form-control m-b"'); ?>
                    	<span class="help-block m-b-none error"><?php echo form_error('dis_id'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Assunto Pai</label>
                    <div class="col-sm-10">
                    	<?php echo form_dropdown('ass_pai_id', $assuntos_pai, $assunto_selecionado, 'id="ass_pai_id" class="form-control m-b"'); ?>
                    	<span class="help-block m-b-none error"><?php echo form_error('ass_pai_id'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-10">
                        <?= form_dropdown('ass_ativo', $statuses, isset($assunto['ass_ativo']) ? $assunto['ass_ativo'] : NULL , 'id="ass_ativo" class="form-control m-b"'); ?>
                        <span class="help-block m-b-none error"><?php echo form_error('ass_ativo'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <?php admin_botoes_salvar_cancelar(get_listar_assuntos_url()) ?>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>