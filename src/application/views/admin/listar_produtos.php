<?php admin_cabecalho_pagina("Produtos") ?>

<?php echo get_mensagem_flash(); ?>

<div class="listar-produtos">
	<div class="row">
		<div class="col-md-10">
			<div class="ibox-content">
				<div id="produto" class="row">
					<div class="col-md-12">
						<input type="text" class="form-control m-b" id="filtro" placeholder="Filtrar pelo nome">
					</div>
					<div class="col-md-12 filtro-simulado" style="margin-top: -20px">
						<label class="control-label">Listar </label> 
						<label class="radio-inline i-checks"><input class="status-radio" type="radio" name="status" value="0" id="todos" checked=checked> Todos </label>
						<label class="radio-inline i-checks"><input class="status-radio" type="radio" name="status" value="1" id="ativos"> Ativos </label>
						<label class="radio-inline i-checks"><input class="status-radio" type="radio" name="status" value="2" id="inativos"> Inativos </label>
					</div>
				</div>
				
				<table class="table cadernos-tabela" id="tabela">
					<thead>
					<tr>
						<th>Nome</th>
						<th>Questões</th>
						<th>Produto Associado</th>
						<th>Ações</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($cadernos_produtos as $caderno_produto) : ?>
					<?php 
						try {
							$produto_wp = get_produto_by_id($caderno_produto['produto_id']);
						}
						catch(Exception $e) {
							continue;
						}

						$status = "";
						
						if($caderno_produto['cpr_status'] == 1) {
							$status = "status-ativo";
						}
						else if($caderno_produto['cpr_status'] == 0) {
							$status = "status-inativo";
						}
					?>
					<tr class="<?= $status ?>">
						<td><span class="produto-nome"><?= $caderno_produto['cad_nome'] ?></span></td>
						<td><span class="produto-nome"><?= $caderno_produto['total_questoes']?></span></td>
						<td><span class="produto-nome"><?= $produto_wp->post->post_title ?></span></td>
						<td>
							<a href="<?= get_editar_caderno_produto_url($caderno_produto['cpr_id']) ?>">Editar</a> | 
							<a onclick="carrega_modal_categorizacao_caderno(<?= $caderno_produto['cpr_id'] ?>)" data-toggle="modal" href="#modal-categorizar-caderno">Categorizar</a> | 
							<?= get_ativar_desativar_caderno_produto_link($caderno_produto) ?>
						</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-2">
			<div class="ibox-content">
				<div>Categorias</div>
				
				<div><a href="<?= get_listar_cadernos_produtos_url() ?>">Listar Todos</a></div>
				<?php foreach ($categorias as $categoria) :?>
					<div>
						<a onclick="carrega_modal_edicao_categoria(<?php echo $categoria['ccp_id'] ?>)" data-toggle="modal" href="#modal-editar-categoria">
							<?= get_tema_image_tag('e.png', 12, 12) ?>
						</a>
						<a onclick="carrega_modal_exclusao_categoria(<?php echo $categoria['ccp_id'] ?>)" data-toggle="modal" href="#modal-excluir-categoria">
							<?= get_tema_image_tag('x.png', 12, 12) ?>
						</a>
						<a href="<?php echo get_categoria_produto_url($categoria['ccp_id'])?>"><?php echo $categoria['ccp_nome'] ?></a>
					</div>
				<?php endforeach;?>
			</div>	
		</div>
	</div>
</div>

<div id="modal-categorizar-caderno" class="modal fade" aria-hidden="true">
	<?php $this->view(get_main_form_categorizar_caderno_produto_view_url()); ?>
</div>

<div id="modal-editar-categoria" class="modal fade" aria-hidden="true">
	<?php $this->view(get_admin_form_editar_categoria_produto_view_url()); ?>
</div>

<div id="modal-excluir-categoria" class="modal fade" aria-hidden="true">
	<?php echo get_modal_confirmacao('Excluir Categoria', 'Deseja realmente EXCLUIR esta categoria?', get_excluir_categoria_produto_url(), 'del_cat_item_id') ?>
</div>

<script>
function carrega_modal_categorizacao_caderno(produto_id) {
	$('#modal-categorizar-caderno .modal-body .panel-conteudo').hide();
	$('.hidden-caderno-id').val(produto_id);
	
	$.get('/questoes/admin/xhr_get_categorias_de_caderno_produto/' + produto_id, function(data) {
		$('#categorias_ids').val(data).trigger('change');
		$('#modal-categorizar-caderno .modal-body .panel-carregando').hide();
		$('#modal-categorizar-caderno .modal-body .panel-conteudo').show();
	}, 'json');
}

function carrega_modal_edicao_categoria(categoria_id) {
	$('#modal-editar-categoria .panel-body .panel-conteudo').hide();
	
	$.get('/questoes/admin/xhr_get_categoria_produto/'+categoria_id, function(data) {

		console.log(data);
		
		$('#id_categoria').val(categoria_id);
		$('#nome_categoria').val(data.ccp_nome);
		$('#modal-editar-categoria .panel-body .panel-carregando').hide();
		$('#modal-editar-categoria .panel-body .panel-conteudo').show();
	}, 'json');
} 

function carrega_modal_exclusao_categoria(categoria_id) {
	$('#del_cat_item_id').val(categoria_id);
} 

function parseFiltro() {
	var filter = $("#filtro").val(); // get the value of the input, which we filter on

	if (filter) {
		$("#tabela").find(".produto-nome:not(:Contains(" + filter + "))").closest("tr").hide();
		$("#tabela").find(".produto-nome:Contains(" + filter + ")").closest("tr").show();
	} else {
		$("#tabela").find("tr").show();
	}
}

function parseRadio() {
	$radio = parseInt($("input[type='radio'][name='status']:checked").val());

	if($radio == 1) {
		$("#tabela").find(".status-inativo").hide();
	}
	if($radio == 2) {
		$("#tabela").find(".status-ativo").hide();
	}
}

$(function() {
	$("#categorias_ids").select2();

	$(".status-radio").change( function () {
		parseFiltro();
		parseRadio();
	});

	$("#filtro").change( function () {
		parseFiltro();
		//parseRadio();
	}).keyup( function () {
		// fire the above change event after every letter
		parseFiltro();
		//parseRadio();
	});

	jQuery.expr[':'].Contains = function(a,i,m){
	    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
	};
});
</script>