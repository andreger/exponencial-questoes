<div class='row'>
    <div class='col-lg-6'>
        <div>
            <a class='btn btn-primary ' href='<?= get_editar_prova_url() ?>'>Nova Prova</a>
            <a href="#" class="btn btn-white toggle-filtros-btn"><i class="fa fa-filter"></i> Filtrar</a>
        </div>
    </div>
    
</div>

<div class='row admin-filtro' id="filtro" style="display: none">
    <form method='post' action="<?= get_listar_provas_url(); ?>" class="form-horizontal" id="frm-filtros">
        <div class='col-md-6'>
            <div class="form-group">
                <label class="col-sm-2 control-label">Nome</label>
                <div class="col-sm-10"><input name="q" type="text" class="form-control" value="<?= $filtros['nome'] ?>"></div>
            </div>
        </div>

        <div class='col-md-6'>		
            <div class="form-group">
                <label class="col-sm-2 control-label">Ano</label>

                <div class="col-sm-10">
                    <?= form_multiselect('q_anos[]', $combo_anos, $filtros['anos'], 'id="q_anos" class="multiselect form-control m-b"'); ?>
                </div>
            </div>
        </div>

        <div class='col-md-6'>		
            <div class="form-group">
                <label class="col-sm-2 control-label">Instituição</label>

                <div class="col-sm-10">
                    <?= form_multiselect('q_orgaos[]', $combo_orgaos, $filtros['orgaos'], 'id="q_orgaos" class="multiselect form-control m-b"'); ?>
                </div>
            </div>
        </div>

        <div class='col-md-6'>		
            <div class="form-group">
                <label class="col-sm-2 control-label">Banca</label>

                <div class="col-sm-10">
                    <?= form_multiselect('q_bancas[]', $combo_bancas, $filtros['bancas'], 'id="q_bancas" class="multiselect form-control m-b"'); ?>
                </div>
            </div>
        </div>

        <div class='col-md-6'>		
            <div class="form-group">
                <label class="col-sm-2 control-label">Área de Atuação</label>

                <div class="col-sm-10">
                    <?= form_multiselect('q_areas_atuacao[]', $combo_areas_atuacao, $filtros['areas_atuacao'], 'id="q_areas_atuacao" class="multiselect form-control m-b"'); ?>
                </div>
            </div>
        </div>

        <div class='col-md-12'>
            <div class="form-group">
                <div class="col-sm-1"></div>
                <div class="col-sm-11">
                    <button type='submit' id="sumit-btn" class='btn btn-primary' name='filtrar' value='1'>
                        <i class="fa fa-filter"></i> Filtrar
                    </button>
                    <a href="#" id="limpar-filtros-btn" class='btn btn-white limpar-filtros-btn'><i class="fa fa-eraser"></i> Limpar Filtros</a>
                    <a href="#" class='btn btn-white toggle-filtros-btn'><i class="fa fa-times"></i> Fechar</a>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="row" style="margin-top: 30px">
    <div class='col-lg-6'>
        Registros encontrados: <strong><?= $total ?></strong>

        <?php if($tem_filtro_aplicado) :?>
            <i>(Filtro aplicado)</i>
        <?php endif ?>
    </div>
    <div class='col-lg-6 text-right'><?= $links ?></div>
</div>