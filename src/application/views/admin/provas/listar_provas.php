<?php admin_cabecalho_pagina("Provas") ?>

<?php ui_alerta($this->session->flashdata('sucesso'), ALERTA_SUCESSO); ?>

<div class='wrapper wrapper-content animated fadeInRight'>
	<div class='row'>
		<div class=''>
			<div class='ibox-content'>
				
				<?php $this->view('admin/provas/filtro_provas'); ?>

				<div class='row' style='clear:both; padding-top: 10px'>
					<div class='table-responsive'>
						<table class='table table-striped table-bordered table-hover' >
							<thead>
								<tr>
									<?php foreach ($thead_array as $item) : ?>
										<th><?= $item ?></th>
									<?php endforeach ?>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($tbody_array as $row) : ?>
									<tr>
										<?php foreach ($row as $item) : ?>
											<td><?= $item ?></td>
										<?php endforeach ?>
									</tr>
								<?php endforeach ?>
							</tbody>
							<tfoot>
								<tr>
									<?php foreach ($tfoot_array as $item) : ?>
										<th><?= $item ?></th>
									<?php endforeach ?>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<div class='row'>
					<div class='col-lg-6'></div>
					<div class='col-lg-6 text-right'><?= $links ?></div>
				</div>
			</div>
		</div>
	</div>
</div>