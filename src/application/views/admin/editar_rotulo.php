<?php
	$prefixo = isset($rotulo['sro_id']) ? 'Editar' : 'Novo'; 
	admin_cabecalho_pagina($prefixo . " Rótulo"); 
?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<form method="post" class="form-horizontal">
    				<div class="form-group">
    					<label class="col-sm-2 control-label">Nome</label>
                        <div class="col-sm-10">
                        	<input type="text" class="form-control" name="sro_nome" value="<?php echo isset($rotulo['sro_nome']) ? $rotulo['sro_nome'] : ''; ?>"/>
                        	<span class="help-block m-b-none error"><?php echo form_error('sro_nome'); ?></span>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <?php admin_botoes_salvar_cancelar("/questoes/admin/rotulos_materias/" . $simulado_id) ?>
				</form>
			</div>
		</div>
	</div>
</div>