<table class="table table-striped table-hover">
<thead>
	<tr>
		<th style="width:300px">Disciplina</th>
		<th>Revisores</th>
	</tr>
</thead>
<tbody>
	<?php foreach ($disciplinas as $disciplina) : ?>
	<?php $selecionados = $revisores_ids[$disciplina['dis_id']]; ?>
	<?php $completa = is_disciplina_simulado_completa($simulado_id, $disciplina['dis_id']) ? "" : " *"; ?>
	<tr class="odd gradeX">
		<td><?= $disciplina['dis_nome'] ?> <?= $completa ?></td>
		<td class="">
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<?php echo form_multiselect("revisores_ids[{$disciplina['dis_id']}][]", $professores, $selecionados, "id='revisores_ids-{$disciplina['dis_id']}' class='multiselect form-control m-b'"); ?>
					</div>
				</div>
			</div>
		</td>
	
	</tr>
	<?php endforeach; ?>
</tbody>
</table>

<script>
$(function() {
  	$("#revisores .multiselect").on("change", function(event, data) { 
  		$(this).trigger("chosen:updated");
  	});
  	$("#revisores .multiselect").chosen({
  		no_results_text: "Nenhum resultado com:",
  		placeholder_text_multiple: "Escolha os revisores para esta disciplina",
  		disable_search_threshold: 10
  	});
});
</script>
<style>
 .chosen-container {
 	width: 600px !important;
</style>