<?php
	$prefixo = isset($prova['pro_id']) ? 'Editar' : 'Nova'; 
	admin_cabecalho_pagina($prefixo . " Prova");
	exibir_mensagem();
?>
<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<?php echo form_open_multipart(get_editar_prova_url($prova), 'id="form-prova" class="form-horizontal"'); ?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nome *</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="pro_nome" value="<?php echo isset($prova['pro_nome']) ? $prova['pro_nome'] : ''; ?>"/>
                    	<span class="help-block m-b-none error"><?php echo form_error('pro_nome'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group">
					<label class="col-sm-2 control-label">Ano *</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="pro_ano" value="<?php echo isset($prova['pro_ano']) ? $prova['pro_ano'] : ''; ?>"/>
                    	<span class="help-block m-b-none error"><?php echo form_error('pro_ano'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Instituição *</label>
                    <div class="col-sm-10">
                    	<?php echo form_dropdown('org_id', $orgaos, $orgao_selecionado, 'id="org_id" class="form-control m-b"'); ?>
                    	<span class="help-block m-b-none error"><?php echo form_error('org_id'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Banca *</label>
                    <div class="col-sm-10">
                    	<?php echo form_dropdown('ban_id', $bancas, $banca_selecionada, 'id="ban_id" class="form-control m-b"'); ?>
                    	<span class="help-block m-b-none error"><?php echo form_error('ban_id'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Escolaridade</label>
                    <div class="col-sm-10">
                    	<?php echo form_dropdown('esc_id', $escolaridades, $escolaridade_selecionada, 'id="esc_id" class="form-control m-b"'); ?>
                    	<span class="help-block m-b-none error"><?php echo form_error('esc_id'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Modalidades</label>
                    <div class="col-sm-10">
                        <?php echo form_dropdown('pro_tipo', $modalidades, $modalidade_selecionada, 'id="pro_tipo" class=" form-control m-b"'); ?>
                        <span class="help-block m-b-none error"><?php echo form_error('pro_tipo'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Área de Formação</label>
                    <div class="col-sm-10">
                    	<?php echo form_multiselect('arf_ids[]', $areas_formacao, $formacoes_selecionadas, 'id="arf_ids" class="multiselect form-control m-b"'); ?>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Área de Atuação</label>
                    <div class="col-sm-10">
                    	<?php echo form_multiselect('ara_ids[]', $areas_atuacao, $atuacoes_selecionadas, 'id="ara_ids" class="multiselect form-control m-b"'); ?>
                    </div>
                </div>
 				<div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Cargos</label>
                    <div class="col-sm-10">
                    	<?php echo form_multiselect('car_ids[]', $cargos, $cargos_selecionados, 'id="car_ids" class="multiselect form-control m-b"'); ?>
                    </div>
                </div>
                <div class="hr-line-dashed"></div> 
                <?php if(isset($prova['pro_id'])) :?>
	                <div class="form-group">
						<label class="col-sm-2 control-label">PDF da Prova</label>
	                    <div class="col-sm-10">  
		                   	<?php if($has_prova_pdf) :?>
								<div><a target="_blank" href="<?php echo get_prova_pdf_link( get_nome_arquivo_prova( "prova", $prova ) );?>">Link da prova atual</a></div>
								<div><a href="<?php echo get_exluir_prova_pdf_link($prova['pro_id']);?>">Apagar Prova</a></div>
							<?php endif;?>
							<div class="margin-top-10"><?php echo form_upload('pdf_prova'); ?></div>
							<div class="margin-top-10"><?php admin_botao_submit('Enviar Prova', 'enviarProva')?></div>
							<h4><?php echo $this->session->flashdata('pdf_prova');?></h4>
	                    </div>
	                </div>    
	                <div class="hr-line-dashed"></div>
	               	<div class="form-group">
						<label class="col-sm-2 control-label">PDF do Gabarito</label>
	                    <div class="col-sm-10">
		                   	<?php if($has_gabarito_pdf) :?>
								<div><a target="_blank" href="<?php echo get_gabarito_pdf_link( get_nome_arquivo_prova( "gabarito", $prova ) );?>">Link do gabarito atual</a>
								<div><a href="<?php echo get_exluir_gabarito_pdf_link($prova['pro_id']);?>">Apagar Gabarito</a></div></div>
							<?php endif;?>
							<div class="margin-top-10"><?php echo form_upload('pdf_gabarito'); ?></div>
							<div class="margin-top-10"><?php admin_botao_submit('Enviar Gabarito', 'enviarGabarito')?></div>
							<h4><?php echo $this->session->flashdata('pdf_gabarito');?></h4>
	                    </div>
	                </div>   
					<div class="hr-line-dashed"></div>
	               	<div class="form-group">
						<label class="col-sm-2 control-label">PDF do Edital</label>
	                    <div class="col-sm-10">
		                   	<?php if($has_edital_pdf) :?>
								<div><a target="_blank" href="<?php echo get_edital_pdf_link( get_nome_arquivo_prova( "edital", $prova ) );?>">Link do edital atual</a>
								<div><a href="<?php echo get_exluir_edital_pdf_link($prova['pro_id']);?>">Apagar Edital</a></div></div>
							<?php endif;?>
							<div class="margin-top-10"><?php echo form_upload('pdf_edital'); ?></div>
							<div class="margin-top-10"><?php admin_botao_submit('Enviar Edital', 'enviarEdital')?></div>
							<h4><?php echo $this->session->flashdata('pdf_edital');?></h4>
	                    </div>
	                </div> 
                <?php endif;?>
                <div class="hr-line-dashed"></div>
                <div><small>(*) Campos obrigatórios</small></div>

                <?php admin_botoes_salvar_cancelar(get_listar_provas_url()) ?>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>