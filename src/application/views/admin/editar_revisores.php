<?php
	admin_cabecalho_pagina("Editar Revisores"); 

	echo get_mensagem_flash();
?>

<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<div class="panel-body">
					<?php echo form_open(get_editar_revisores_simulado_url($simulado_id), 'id="form-revisores" class="form-horizontal"'); ?>
					<div class="panel-group" id="revisores">
						<div class="form-group m-label">
							<label>Nome do Simulado:</label>
							<div class="nome-simulado">
								<?= $simulado_nome ?>
							</div>	
						</div>
						<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th style="width:300px">Disciplina</th>
								<th style="text-align: center">Qtde Questões</th>
								<th style="text-align: center">Percentual</th>
								<th>Revisores</th>
							</tr>
						</thead>
						<tbody>
							<?php $total = array_sum(array_column($disciplinas, 'qtde')); ?>
							<?php foreach ($disciplinas as $disciplina) : ?>
							<?php $selecionados = $revisores_ids[$disciplina['dis_id']]; ?>
							<?php $completa = is_disciplina_simulado_completa($simulado_id, $disciplina['dis_id']) ? "" : " *"; ?>
							<tr class="odd gradeX">
								<td><?= $disciplina['dis_nome'] ?> <?= $completa ?></td>
								<td style="text-align: center"><?= $disciplina['qtde']	?></td> 
								<td style="text-align: center"><?= porcentagem($disciplina['qtde'] * 100 / $total) ?></td>
								<td class="">
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<?php echo form_multiselect("revisores_ids[{$disciplina['dis_id']}][]", $professores, $selecionados, "id='revisores_ids-{$disciplina['dis_id']}' class='multiselect form-control m-b'"); ?>
											</div>
										</div>
									</div>
								</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
						</table>
					</div>
					
					<div>
						<?php admin_botoes_salvar_cancelar(get_listar_simulados_url(TRUE)) ?>
					</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(function() {
  	$("#revisores .multiselect").on("change", function(event, data) { 
  		$(this).trigger("chosen:updated");
  	});
  	$("#revisores .multiselect").chosen({
  		no_results_text: "Nenhum resultado com:",
  		placeholder_text_multiple: "Escolha os revisores para esta disciplina",
  		disable_search_threshold: 10
  	});
});
</script>
<style>
 .chosen-container {
 	width: 600px !important;
</style>