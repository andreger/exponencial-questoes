<?php admin_cabecalho_pagina("Ordem da Taxonomia") ?>

<div class="row">
    <div class="col-lg-6">
        <div class="ibox ">
            <div class="form-group">
                <label class="col-sm-2 control-label">Disciplina</label>
                <div class="col-sm-10">
                    <?= form_dropdown('dis_id', $disciplinas, $disciplina_selecionada, 'id="dis_id" class="form-control m-b"'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {

    $('#dis_id').on('change', function() {
        var dis_id = $(this).val();

        window.location.replace('/questoes/admin/ordem_taxonomia/' + dis_id);
    });
    
});
</script>