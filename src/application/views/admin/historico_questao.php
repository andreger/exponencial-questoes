<?php admin_cabecalho_pagina ( "Histórico - Questão " . $questao['que_id'] ); ?>

<div class="row margin-top-10">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<div style="text-align:right">
					<a class="btn btn-primary" href="javascript:history.back()"><i class="fa fa-arrow-left"></i> Voltar </a>
					<!-- <a class="btn btn-primary" href="<?= get_editar_questao_url($questao) ?>"><i class="fa fa-eye"></i> Editar </a> -->
				</div>
				<div><?php data_table($thead_array, $tbody_array, $tfoot_array) ?></div>
			</div>
		</div>
	</div>
</div>

