<?php
	$prefixo = isset($area_formacao['arf_id']) ? 'Editar' : 'Nova'; 
	admin_cabecalho_pagina($prefixo . " Área de Formação"); 
?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<?php echo form_open(get_editar_area_formacao_url($area_formacao), 'id="form-area_formacao" class="form-horizontal"'); ?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nome</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="arf_nome" value="<?php echo isset($area_formacao['arf_nome']) ? $area_formacao['arf_nome'] : ''; ?>"/>
                    	<span class="help-block m-b-none error"><?php echo form_error('arf_nome'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <?php admin_botoes_salvar_cancelar(get_listar_areas_formacao_url()) ?>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>