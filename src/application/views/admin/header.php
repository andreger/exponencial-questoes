<!DOCTYPE html>
<html lang="pt-BR" prefix="og: http://ogp.me/ns#" class="wf-opensans-n4-active wf-opensans-i4-active wf-opensans-n6-active wf-active">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php if(!is_producao()) : ?>
        <meta name="robots" content="noindex, nofollow" />
    <?php endif ?>  

    <title>Sistema de Questões - Administração | Exponencial Concursos </title>
    <link rel="shortcut icon" href="<?= base_url('/favicon.ico') ?>" />

    <?php 
        $arquivos_css = [
            'assets-admin/css/bootstrap.min.css',
            'assets-admin/font-awesome/css/font-awesome.css',
            'assets-admin/css/plugins/toastr/toastr.min.css',
            'assets-admin/js/plugins/gritter/jquery.gritter.css',
            /*'assets-admin/css/plugins/jQueryUI/jquery-ui.css',
            'assets-admin/css/plugins/dataTables/dataTables.bootstrap.css',
            'assets-admin/css/plugins/dataTables/dataTables.responsive.css',
            'assets-admin/css/plugins/dataTables/dataTables.tableTools.min.css',
            'assets-admin/css/plugins/summernote/summernote.css',
            'assets-admin/css/plugins/summernote/summernote-bs3.css',
            'assets-admin/css/plugins/chosen/chosen.css',
            'assets-admin/js/plugins/select2/select2.css',
            'assets-admin/js/plugins/select2/select2-bootstrap.css',
            'assets-admin/css/plugins/iCheck/custom.css',
            'assets-admin/css/plugins/c3/c3.min.css',
            'assets-admin/css/plugins/daterangepicker2/daterangepicker.css',
            'assets-admin/css/bootstrap-4.0.0-alpha.6.css',
            'assets-admin/css/animate.css',
            'assets-admin/css/style.css',
            'assets-admin/css/custom.css',
            'assets-admin/css/mobile.css',
            'assets-admin/css/print.css',
            'assets-admin/css/plugins/jsTree/style.min.css' */
        ];

        $css = agrupar_arquivos($arquivos_css, 'assets-admin/css/all.css', 'css', VERSAO)
    ?>

    <link href="<?= $css ?>" rel="stylesheet">

    <!--<link href="<?= base_url('assets-admin/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets-admin/font-awesome/css/font-awesome.css') ?>" rel="stylesheet">

    <!-- Toastr style --
    <link href="<?= base_url('assets-admin/css/plugins/toastr/toastr.min.css') ?>" rel="stylesheet">

    <!-- Gritter --
    <link href="<?= base_url('assets-admin/js/plugins/gritter/jquery.gritter.css') ?>" rel="stylesheet">-->
    
    <?php if(isset($include_data_tables)) : ?>
    <!-- Data Tables -->
    <link href="<?= base_url('assets-admin/css/plugins/dataTables/dataTables.bootstrap.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets-admin/css/plugins/dataTables/dataTables.responsive.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets-admin/css/plugins/dataTables/dataTables.tableTools.min.css') ?>" rel="stylesheet">
	<?php endif ?>
	
	<?php if(isset($include_icheck)) : ?>
    <link href="<?= base_url('assets-admin/css/plugins/iCheck/custom.css') ?>" rel="stylesheet">
    <?php endif; ?>
	
	<?php if(isset($include_summernote)) : ?>
	<link href="<?= base_url('assets-admin/css/plugins/summernote/summernote.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets-admin/css/plugins/summernote/summernote-bs3.css') ?>" rel="stylesheet">
	<?php endif ?>
	 <?php if(isset($include_datepicker)) : ?>
    <link href="<?= base_url('assets-admin/css/plugins/datapicker/datepicker3.css') ?>" rel="stylesheet">
    <?php endif; ?>
	<?php if(isset($include_chosen)) : ?>
	<link href="<?= base_url('assets-admin/css/plugins/chosen/chosen.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets-admin/js/plugins/select2/select2.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets-admin/js/plugins/select2/select2-bootstrap.css') ?>" rel="stylesheet">
	<?php endif ?>

    <?php if(isset($include_stars)) : ?>
    <link href="<?php echo base_url('assets-admin/css/plugins/jquery-stars/stars.css') ?>" rel="stylesheet">
    <?php endif; ?>

    <?php if(isset($include_c3)) : ?>
    <link href="<?php echo base_url('assets-admin/css/plugins/c3/c3.min.css') ?>" rel="stylesheet">
    <?php endif; ?>

	<?php if(isset($include_daterangepicker)) : ?>
    <link href="<?= base_url('assets-admin/css/plugins/daterangepicker2/daterangepicker.css') ?>" rel="stylesheet">
    <!--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/latest/css/bootstrap.css" />-->

    <?php endif; ?>
	<?php if(isset($include_steps)) : ?>
	<link href="<?= base_url('assets-admin/css/plugins/steps/jquery.steps.css') ?>" rel="stylesheet">
	<?php endif ?>
	
	<?php if(isset($include_jstree)) : ?>
	<link href="<?= base_url('assets-admin/css/plugins/jsTree/style.min.css') ?>" rel="stylesheet">
	<style>
    .jstree-open > .jstree-anchor > .fa-folder:before {
        content: "\f07c";
    }

    .jstree-default .jstree-icon.none {
        width: 0;
    }
	</style>
	<?php endif ?>
	
    <link href="<?= base_url('assets-admin/css/animate.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets-admin/css/style.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets-admin/css/custom.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets-admin/css/mobile.css') ?>" rel="stylesheet">

    <!-- Mainly scripts -->
    <script src="<?= base_url('assets-admin/js/jquery-2.1.1.js') ?>"></script>

    <?php if(isset($include_jquery_ui)) : ?>
    <!-- jQuery UI -->
    <script src="<?= base_url('assets-admin/js/plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
    <?php endif ?>
    
    <script src="<?= base_url('assets-admin/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets-admin/js/plugins/metisMenu/jquery.metisMenu.js') ?>"></script>
    <script src="<?= base_url('assets-admin/js/plugins/slimscroll/jquery.slimscroll.min.js') ?>"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?= base_url('assets-admin/js/inspinia.js') ?>"></script>
    <!--<script src="<?= base_url('assets-admin/js/plugins/pace/pace.min.js') ?>"></script>-->

	<?php if(isset($include_jquery_form)) : ?>
    <!-- jQuery Form -->
    <script src="<?= base_url('assets-admin/js/jquery.form.js') ?>"></script>
	<?php endif ?>

    <!-- Toastr -->
    <script src="<?= base_url('assets-admin/js/plugins/toastr/toastr.min.js') ?>"></script>

    <?php if(isset($include_data_tables)) : ?>
    <!-- Data Tables -->
    <script src="<?= base_url('assets-admin/js/plugins/dataTables/jquery.dataTables.js') ?>"></script>
    <script src="<?= base_url('assets-admin/js/plugins/dataTables/dataTables.bootstrap.js') ?>"></script>
    <script src="<?= base_url('assets-admin/js/plugins/dataTables/dataTables.responsive.js') ?>"></script>
    <script src="<?= base_url('assets-admin/js/plugins/dataTables/dataTables.tableTools.min.js') ?>"></script>
    <?php endif ?>
    
    <?php if(isset($include_steps)) : ?>
    <script src="<?= base_url('assets-admin/js/plugins/staps/jquery.steps.min.js') ?>"></script>
    <?php endif; ?>
    
	<?php if(isset($include_validade)) : ?>
    <script src="<?= base_url('assets-admin/js/plugins/validate/jquery.validate.min.js') ?>"></script>
    <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
    <script src="<?= base_url('assets-admin/js/validate.translate.js') ?>"></script>
    <?php endif; ?>
    
    <?php if(isset($include_datepicker)) : ?>
    <script src="<?= base_url('assets-admin/js/plugins/datapicker/bootstrap-datepicker.js') ?>"></script>
    <?php endif; ?>
    
    <?php if(isset($include_jquery_chained)) : ?>
    <script src="<?= base_url('assets-admin/js/jquery.chained.remote.min.js') ?>"></script>
    <?php endif ?>
    
    <?php if(isset($include_summernote)) : ?>
    <script src="<?= base_url('assets-admin/js/plugins/summernote/summernote.js') ?>"></script>
    <script src="<?= base_url('assets-admin/js/plugins/summernote/lang/summernote-pt-BR.js') ?>"></script>
    <script src="<?= base_url('assets-admin/js/ext/summernote-ext.js') ?>"></script>
    <script src="<?= base_url('assets-admin/js/ext/summernote-ext-specialchars.js') ?>"></script>
    <script src="<?= base_url('assets-admin/js/ext/summernote-ext-myenter.js') ?>"></script>
    <?php endif ?>
    
    <?php if(isset($include_chosen)) : ?>
    <script src="<?= base_url('assets-admin/js/plugins/chosen/chosen.jquery.js') ?>"></script>
    <script src="<?= base_url('assets-admin/js/plugins/select2/select2.js'); ?>"></script>
    <?php endif ?>
    
    <?php if(isset($include_jstree)) : ?>
	<script src="<?= base_url('assets-admin/js/plugins/jsTree/jstree.min.js') ?>"></script>
	<?php endif ?>
	
	<?php if(isset($include_icheck)) : ?>
    <script src="<?= base_url('assets-admin/js/plugins/iCheck/icheck.min.js'); ?>"></script>
    <?php endif; ?>
    
    <?php if(isset($include_cropper)) : ?>
    <link href="<?= base_url('assets-admin/css/plugins/cropper/cropper.min.css'); ?>" rel="stylesheet">
	<script src="<?= base_url('assets-admin/js/plugins/cropper/cropper.min.js'); ?>"></script>
    <?php endif; ?>

    <?php if(isset($include_stars)) : ?>
    <script src="<?php echo base_url('assets-admin/js/plugins/jquery-stars/stars.min.js') ?>"></script>
    <?php endif; ?>

    <?php if(isset($include_c3)) : ?>
    <script src="<?php echo base_url('assets-admin/js/plugins/c3/c3.min.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/plugins/d3/d3.min.js') ?>"></script>
    <?php endif; ?>

    <?php if(isset($include_daterangepicker)) : ?>
    <script src="<?= base_url('assets-admin/js/plugins/daterangepicker2/moment.min.js') ?>"></script>
    <script src="<?= base_url('assets-admin/js/plugins/daterangepicker2/daterangepicker.js') ?>"></script>
    <?php endif; ?>

    <?php if(isset($include_nestable)) : ?>
    <script src="<?= base_url('assets-admin/js/plugins/nestable/jquery.nestable.js') ?>"></script>
    <?php endif; ?>

    <?php if(isset($include_mask)) : ?>
    <script src="/wp-content/themes/academy/js/jquery.maskedinput.min.js"></script>
    <?php endif ?>

    <?php if(isset($include_mask_plugin)) : ?>
    <script src="<?= base_url('assets-admin/js/plugins/jquery-mask-plugin/jquery.mask.min.js') ?>"></script>
    <?php endif ?>

    <?php if($include_vimeo): ?>
        <link href="/wp-content/themes/academy/css/lity.min.css" rel="stylesheet">
        <script src="/wp-content/themes/academy/js/lity.min.js"></script>
        <style>
            .lity, .lity-wrap {
                z-index: 99999 !important;
            }
        </style>
    <?php endif; ?>
    
</head>

<body class="bg-darkblue">
    <div id="wrapper">
    	<?php $this->load->view(get_admin_menu_view_url()) ?>
    	 <div id="page-wrapper" class="margin-wrapper gray-bg dashbard-1">
    	 	<?php $this->load->view(get_admin_topbar_view_url()) ?>