<?php
	admin_cabecalho_pagina("Editar Ranking <small>(Passo 4 - Definir Pesos)</small>"); 
?>

<?php ui_alerta($this->session->flashdata('sucesso'), ALERTA_SUCESSO); ?>

<div class="row margin-top-10">
	<div class="col-lg-12">
		<div class="ibox ">
	        <div class="ibox-content">
				<form method="post" class="form-horizontal">

				<h2>Ranking</h2>
				<div class="form-group">
					<label class="col-sm-3 control-label"></label>
					<label class="col-sm-3">Peso erro</label>
					<label class="col-sm-3">Corte</label>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label"><?= $ranking['ran_nome'] ?></label>
					<label class="col-sm-3"><input type="text" class="form-control peso" name="ran_peso_erros" value="<?= $ranking['ran_peso_erros'] ? numero_brasileiro($ranking['ran_peso_erros']) : '' ?>" /></label>
					<label class="col-sm-3"><input type="text" class="form-control corte" name="ran_corte" value="<?= $ranking['ran_corte'] ?  numero_brasileiro($ranking['ran_corte']) : '' ?>" /></label>
                </div>

				<h2>Simulados</h2>
				<div class="form-group">
					<label class="col-sm-3 control-label"></label>
					<label class="col-sm-3">Peso</label>
					<label class="col-sm-3">Corte</label>
                </div>

				<?php foreach ($simulados as $simulado) : ?>
				<div class="form-group">
					<label class="col-sm-3 control-label"><?= $simulado['sim_nome'] ?></label>
					<label class="col-sm-3"><input type="text" class="form-control peso" name="simulados[<?= $simulado['sim_id'] ?>][peso]" value="<?= $simulado['ras_peso'] ? numero_brasileiro($simulado['ras_peso']) : '' ?>" /></label>
					<label class="col-sm-3"><input type="text" class="form-control corte" name="simulados[<?= $simulado['sim_id'] ?>][corte]" value="<?= $simulado['ras_corte'] ? numero_brasileiro($simulado['ras_corte']) : '' ?>" /></label>
                </div>
                <div class="hr-line-dashed"></div>
            	<?php endforeach ?>

            	<?php if($grupos) : ?>
            	<h2>Grupos</h2>
				<div class="form-group">
					<label class="col-sm-3 control-label"></label>
					<label class="col-sm-3">Peso</label>
					<label class="col-sm-3">Corte</label>
                </div>
            	<?php endif; ?>

				<?php foreach ($grupos as $grupo) : ?>
				<div class="form-group">
					<label class="col-sm-3 control-label"><?= $grupo['rag_nome'] ?></label>
					<label class="col-sm-3"><input type="text" class="form-control peso" name="grupos[<?= $grupo['rag_id'] ?>][peso]" value="<?= $grupo['rag_peso'] ? numero_brasileiro($grupo['rag_peso']) : '' ?>" /></label>
					<label class="col-sm-3"><input type="text" class="form-control corte" name="grupos[<?= $grupo['rag_id'] ?>][corte]" value="<?= $grupo['rag_corte'] ? numero_brasileiro($grupo['rag_corte']) : '' ?>" /></label>
                </div>
                <div class="hr-line-dashed"></div>
            	<?php endforeach ?>
   				
   				<h2>Disciplinas</h2>
				<div class="form-group">
					<label class="col-sm-3 control-label"></label>
					<label class="col-sm-3">Peso</label>
					<label class="col-sm-3">Corte</label>
                </div>

				<?php foreach ($disciplinas as $disciplina) : ?>
				<div class="form-group">
					<label class="col-sm-3 control-label"><?= $disciplina['dis_nome'] ?>
						<span class="ranking-disciplina-subtitle">(<?= $disciplina['rag_id'] ? $disciplina['rag_nome'] : 'Sem grupo associado' ?>)</span>
					</label>
					<label class="col-sm-3"><input type="text" class="form-control peso" name="disciplinas[<?= $disciplina['dis_id'] ?>][peso]" value="<?= $disciplina['rad_peso'] ? numero_brasileiro($disciplina['rad_peso']) : '' ?>" /></label>
					<label class="col-sm-3"><input type="text" class="form-control corte" name="disciplinas[<?= $disciplina['dis_id'] ?>][corte]" value="<?= $disciplina['rad_corte'] ? numero_brasileiro($disciplina['rad_corte']) : '' ?>" /></label>
					<input type="hidden" name="disciplinas[<?= $disciplina['dis_id'] ?>][rag_id]" value="<?= $disciplina['rag_id'] ? $disciplina['rag_id'] : '' ?>" />
                </div>
                <div class="hr-line-dashed"></div>
            	<?php endforeach ?>

               	<?= get_admin_botoes_navegacao_ranking($id, $pagina) ?>
				<?= form_close(); ?>
	        </div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		
		$('.peso').mask('0,09', {reverse: false});
		$('.corte').mask('#.##0,09', {reverse: true});

	});
</script>