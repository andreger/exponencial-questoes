<?php admin_cabecalho_pagina("Histórico de Execuções - #{$execucao_id}") ?>

<?php echo get_mensagem_flash() ?>

<div class="row margin-top-10">
	<div class="col-lg-12">
		<a href="<?= get_questoes_repetidas_excecucoes_url() ?>" class="btn btn-white"><i class="fa fa-arrow-left"></i> Voltar para listagem</a>
	</div>
</div>

<div class="row margin-top-10">
	<div class="col-lg-12">
		<div class='ibox float-e-margins'>
			<div class="col-md-6">
				<div class='ibox float-e-margins'>
					<div class='ibox-title'>
		                <h5>Executadas</h5>
		            </div>
		            <div class='ibox-content'>
						<?php if($num_questoes_executadas > 0) : ?>
						<div>
							<?php foreach ($questoes_executadas as $questao) : ?>
							<span class="qrx_nome"><a href="<?= get_questao_url($questao['que_id']) ?>"><?= $questao['que_id'] ?></a></span>
							<?php endforeach ?>
						</div>
						<?php else : ?>
						<div>
							Nenhuma questão foi executada
						</div>
						<?php endif ?>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class='ibox float-e-margins'>
					<div class='ibox-title'>
		                <h5>Repetidas</h5>
		            </div>
		            <div class='ibox-content'>
						<?php if($num_questoes_repetidas > 0) : ?>
						<div>
							<?php foreach ($questoes_repetidas as $questao) : ?>
							<span class="qrx_nome"><a href="<?= get_comparar_questoes_url($questao['que_id_master'], $questao['que_id_repetida']) ?>"><?= $questao['que_id_master'] ?></a></span>
							<?php endforeach ?>
						</div>
						<?php else : ?>
						<div>
							Nenhuma questão estava repetida
						</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>