<?php
	admin_cabecalho_pagina("Editar Ranking <small>(Passo 2 - Selecionar Grupos)</small>"); 
?>

<?php ui_alerta($this->session->flashdata('sucesso'), ALERTA_SUCESSO); ?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<form method="post" class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-2 control-label">Novo Grupo</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="rag_nome" />
                    	<span class="help-block m-b-none error"><?php echo form_error('rag_nome'); ?></span>
                    </div>
                </div>
   				<div class="form-group">
	   				<label class="col-sm-2 control-label"></label>
    	            <div class="col-sm-10">
                		<?= get_admin_botao_submit('Salvar Grupo', 'salvar') ?>
                	</div>
                </div>
				<?php echo form_close(); ?>
			</div>
		</div>

		<div style="margin-top: -30px">
		<?php if($grupos) : ?>
			<?php simple_table($thead_array, $tbody_array, $tfoot_array,'','') ?>
		<?php endif; ?>

			<?= get_admin_botoes_navegacao_ranking($id, $pagina) ?>
		</div>
	</div>
</div>