<?php
	admin_cabecalho_pagina("Conversão de Assuntos"); 

    echo get_mensagem_flash();
?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
                <div>
                    <a class='btn btn-primary' id="nova-conversao-btn" href='#'>Nova Conversão</a>
                </div>

                <div id="nova-conversao-frm" style="<?= validation_errors()?'':'display:none'; ?> ">
    				<?php echo form_open(get_conversao_assuntos_url($assunto), 'id="form-assunto" class="form-horizontal"'); ?>
       				<div class="form-group">
    					<label class="col-sm-2 control-label">Disciplina Antiga</label>
                        <div class="col-sm-10">
                        	<?php echo form_dropdown('dis_antigo', $disciplinas, set_value('dis_antigo'), 'id="dis_antigo" class="combo form-control m-b"'); ?>
                            <span class="help-block m-b-none error"><?php echo form_error('dis_antigo'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <input id="conversao_default" type="checkbox" name="conversao_default" value="1" <?php echo set_value('conversao_default')?"checked":"" ?> > Conversão Default
                        </div>
                    </div>
       				<div class="form-group">
    					<label class="col-sm-2 control-label">Assunto Antigo</label>
                        <div class="col-sm-10">
                        	<?php echo form_dropdown('ass_antigo', $assuntos_antigos, set_value('ass_antigo'), 'id="ass_antigo" class="combo form-control m-b"'); ?>
                        	<span class="help-block m-b-none error"><?php echo form_error('ass_antigo'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-10">
                            <input type="checkbox" id="inativar_assunto" name="inativar_assunto" value="1" <?php echo set_value('inativar_assunto')?"checked":"" ?>> Inativar Assunto
                            <span class="help-block m-b-none error"><?php echo form_error('inativar_assunto'); ?></span>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Disciplina Nova</label>
                        <div class="col-sm-10">
                            <?php echo form_dropdown('dis_novo', $disciplinas, set_value('dis_novo'), 'id="dis_novo" class="form-control m-b"'); ?>
                            <span class="help-block m-b-none error"><?php echo form_error('dis_novo'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Assunto Novo</label>
                        <div class="col-sm-10">
                            <?php echo form_dropdown('ass_novo', $assuntos_novos, set_value('ass_novo'), 'id="ass_novo" class="combo form-control m-b"'); ?>
                            <span class="help-block m-b-none error"><?php echo form_error('ass_novo'); ?></span>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <input type="hidden" name="nova_conversao" value="1">
                    <?php admin_botoes_salvar_cancelar(get_conversao_assuntos_url()) ?>
    				<?php echo form_close(); ?>
                </div>

                <div id="tabela">
                    
                    <div class="" style="margin: 40px 0 20px">
                        <form method="post">
                            <div style="margin:5px 0">Filtrar por disciplinas:</div>
                            <div>
                                <?php echo form_multiselect('disciplinas_ids[]', $disciplinas_multi, $disciplinas_selecionadas_ids, 'id="disciplinas" class="multiselect form-control"') ?>
                            </div>
                            <div style="margin:5px 0">
                                <input class="btn btn-primary" type="submit" name="filtrar" value="Filtrar">
                            </div>
                        </form>
                    </div>
                    
                    <?php if($conversoes) : ?>
                        <div>
                            <?php simple_table($thead_array, $tbody_array, $tfoot_array, NULL, NULL) ?>
                        </div>
                    <?php else : ?>
                        Nenhuma conversão de assunto cadastrada.
                    <?php endif; ?>
                </div>
			</div>
		</div>
	</div>
</div>


<script>
$(function() {
    $('#nova-conversao-btn').click(function() {
        $('#nova-conversao-frm').toggle();
        $('#tabela').toggle();
    });

    $("#ass_antigo").remoteChained({
        parents : "#dis_antigo",
        url : "<?= get_ajax_get_ass_id_url(0,0,'dis_antigo',NAO); ?>"
    });
    $("#ass_novo").remoteChained({
        parents : "#dis_novo",
        url : "<?= get_ajax_get_ass_id_url(0,0,'dis_novo',NAO); ?>"
    });

    $(".multiselect").chosen({
        disable_search_threshold: 10,
        no_results_text: "Nenhuma disciplina com:",
        placeholder_text_multiple: "Selecione algumas disciplinas..."
    });

    $(".combo").select2();

    $('#conversao_default').change(function(){
        $('#ass_antigo').prop( "disabled", $(this).is(':checked') );
        $('#inativar_assunto').prop( "disabled", $(this).is(':checked') );
    });

    $('#conversao_default').trigger('change');

});
</script>