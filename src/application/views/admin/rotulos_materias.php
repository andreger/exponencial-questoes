<?php admin_cabecalho_pagina("Rótulos de Matérias") ?>

<?php echo get_mensagem_flash(); ?>

<div class="row margin-top-10">
	<div class="ibox-content">
		<div class="col-lg-12 rotulos">
			
			<div style="margin-bottom: 30px"><?php admin_botao_verde('Novo Rótulo', get_editar_rotulo_url($simulado_id))?></div>

			<table class="table table-bordered">
				<tr>
					<th>Ações</th>
					<th>Rótulo</th>
					<th>Questões</th>
				</tr>

				<?php foreach ($rotulos as $rotulo) : ?>
				<tr>
					<td>
						<?= get_admin_botao_editar("Editar Rótulo", get_editar_rotulo_url($rotulo['sim_id'], $rotulo['sro_id'])) ?>
						<?= get_admin_botao_excluir("Excluir Rótulo", get_excluir_rotulo_url($rotulo['sim_id'], $rotulo['sro_id'])) ?>
					</td>
					<td><?= $rotulo['sro_nome'] ?></td>
					<td>
						<div class="dd sortable">
			         	    <ol class="dd-list" data-rotulo-id="<?= $rotulo['sro_id'] ?>">
			         	    	<?php if($rotulo['questoes']) : ?>
			         	    		<?php foreach ($rotulo['questoes'] as $questao_id) : ?>
					                    <li class="dd-item" id="<?= $questao_id ?>">
				    	                    <div class="dd-handle"><a href="<?= get_questao_url($questao_id) ?>"><?= $questao_id ?></a></div>
				        	            </li>
				        	        <?php endforeach ?>
			                	<?php endif ?>
			                </ol>
		            	</div>
		        	</td>
				</tr>
				<?php endforeach ?>
				
			</table>

			<form method="post" id="form">
				<?= get_admin_botao_submit("Salvar Rótulos", "salvar", "", "id='salvar'") ?>
			</form>

		</div>
		<div style="clear: both;"></div>
	</div>
</div>

<script>
$(function() {
	$(".dd-list").sortable({ connectWith: '.dd-list' });
	$(".dd-list").disableSelection();

	$('#salvar').click(function(e) {
		$('#form input[type=hidden]').remove();

		$(".dd-list").each(function() {
			var rotulo_id = $(this).data('rotulo-id');
			
			if(rotulo_id == undefined) {

			}

			var que_ids = JSON.stringify($(this).sortable('toArray', {attribute: 'id'}));

			$('<input>').attr({
			    type: 'hidden',
			    name: "rotulos["+ rotulo_id + "]",
			    value: que_ids
			}).appendTo('#form');
		});
		
	});
});
</script>