<?php KLoader::helper("AcessoGrupoHelper") ?>

<nav class="navbar-default navbar-static-side menu-admin" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav metismenu" id="side-menu">
			<?php if(AcessoGrupoHelper::coaching_metas()) : ?>
			<li>
				<a href="/painel-coaching/admin"><i class="fa fa-folder-o"></i> <span class="nav-label">Painel do Coaching</span> </a>
			</li>
			<?php endif; ?>	
			
			<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR))) : ?>
			<li class="<?php menu_ativo_inativo(array('listar_disciplinas','editar_disciplina')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Disciplinas</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_disciplinas') ?>"><a href="<?php echo get_listar_disciplinas_url() ?>">Listar Todas</a></li>
					<li class="<?php menu_ativo_inativo('editar_disciplina') ?>"><a href="<?php echo get_editar_disciplina_url() ?>">Nova Disciplina</a></li>
					<li class="<?php menu_ativo_inativo('arvore_disciplinas') ?>"><a href="<?php echo get_arvore_disciplinas_url() ?>">Árvore de Disciplinas</a></li>
				</ul>
			</li>
			<li class="<?php menu_ativo_inativo(array('listar_bancas','editar_banca')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Bancas</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_bancas') ?>"><a href="<?php echo get_listar_bancas_url() ?>">Listar Todas</a></li>
					<li class="<?php menu_ativo_inativo('editar_banca') ?>"><a href="<?php echo get_editar_banca_url() ?>">Nova Banca</a></li>
				</ul>
			</li>
			<li class="<?php menu_ativo_inativo(array('listar_orgaos','editar_orgao')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Instituições</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_orgaos') ?>"><a href="<?php echo get_listar_orgaos_url() ?>">Listar Todas</a></li>
					<li class="<?php menu_ativo_inativo('editar_orgao') ?>"><a href="<?php echo get_editar_orgao_url() ?>">Nova Instituição</a></li>
				</ul>
			</li>
			<li class="<?php menu_ativo_inativo(array('listar_cargos','editar_cargo')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Cargos</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_cargos') ?>"><a href="<?php echo get_listar_cargos_url() ?>">Listar Todos</a></li>
					<li class="<?php menu_ativo_inativo('editar_cargo') ?>"><a href="<?php echo get_editar_cargo_url() ?>">Novo Cargo</a></li>
				</ul>
			</li>
			<li class="<?php menu_ativo_inativo(array('listar_assuntos','editar_assunto', 'conversao_assuntos')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Assuntos</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_assuntos') ?>"><a href="<?= get_listar_assuntos_url() ?>">Listar Todos</a></li>
					<li class="<?php menu_ativo_inativo('editar_assunto') ?>"><a href="<?= get_editar_assunto_url() ?>">Novo Assunto</a></li>
					<li class="<?php menu_ativo_inativo('conversao_assuntos') ?>"><a href="<?= get_conversao_assuntos_url() ?>">Conversão de Assuntos</a></li>
					<li class="<?php menu_ativo_inativo('ordem_taxonomia') ?>"><a href="<?= get_ordem_taxonomia_url() ?>">Ordem da Taxonomia</a></li>
				</ul>
			</li>
			<li class="<?php menu_ativo_inativo(array('listar_provas','editar_prova')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Provas</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_provas') ?>"><a href="<?php echo get_listar_provas_url() ?>">Listar Todas</a></li>
					<li class="<?php menu_ativo_inativo('editar_prova') ?>"><a href="<?php echo get_editar_prova_url() ?>">Nova Prova</a></li>
				</ul>
			</li>
			<?php endif; ?>
			<li class="<?php menu_ativo_inativo(array('listar_questoes','editar_questao','listar_imagens_nao_tratadas', 'listar_imagens_tratadas', 'contagem_questoes', 'correcao_assuntos')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Questões</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR))) : ?>
						<li class="<?php menu_ativo_inativo('listar_questoes') ?>"><a href="<?php echo get_listar_questoes_url() ?>">Listar Todas</a></li>
					<?php endif; ?>
					
					<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR, PROFESSOR))) : ?>
					<li class="<?php menu_ativo_inativo('listar_questoes_professor') ?>"><a href="<?php echo get_listar_questoes_professor_url() ?>">Questões de Professores</a></li>
					<li class="<?php menu_ativo_inativo('editar_questao') ?>"><a href="<?php echo get_editar_questao_url() ?>">Nova Questão</a></li>
					<?php endif; ?>
					
					<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR))) : ?>
						<li class="<?php menu_ativo_inativo('listar_imagens_nao_tratadas') ?>"><a href="<?php echo get_listar_imagens_nao_tratadas_url() ?>">Imagens Não Tratadas</a></li>
						<li class="<?php menu_ativo_inativo('listar_imagens_tratadas') ?>"><a href="<?= get_listar_imagens_tratadas_url() ?>">Imagens Tratadas</a></li>
						<li class="<?php menu_ativo_inativo('contagem_questoes') ?>"><a href="<?= get_contar_questoes_url() ?>">Contagem de Questões</a></li>
						<li class="<?php menu_ativo_inativo('correcao_assuntos') ?>"><a href="<?= get_correcao_assuntos_url() ?>">Correção de Assuntos</a></li>
					<?php endif; ?>
				</ul>
			</li>
			<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR))) : ?>
			<li class="<?php menu_ativo_inativo(array('listar_areas_formacao','editar_area_formacao')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Áreas de Formação</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_areas_formacao') ?>"><a href="<?php echo get_listar_areas_formacao_url() ?>">Listar Todas</a></li>
					<li class="<?php menu_ativo_inativo('editar_area_formacao') ?>"><a href="<?php echo get_editar_area_formacao_url() ?>">Nova Área</a></li>
				</ul>
			</li>
			<li class="<?php menu_ativo_inativo(array('listar_areas_atuacao','editar_area_atuacao')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Áreas de Atuação</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_areas_atuacao') ?>"><a href="<?php echo get_listar_areas_atuacao_url() ?>">Listar Todas</a></li>
					<li class="<?php menu_ativo_inativo('editar_area_atuacao') ?>"><a href="<?php echo get_editar_area_atuacao_url() ?>">Nova Área</a></li>
				</ul>
			</li>
			<?php endif; ?>
			<li class="<?php menu_ativo_inativo(array('listar_simulados','editar_simulado', 'prioridade_simulados')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Simulados</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_simulados') ?>"><a href="<?php echo get_listar_simulados_url() ?>">Listar Todos</a></li>
					
					<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR))) : ?>
					<li class="<?php menu_ativo_inativo('editar_simulado') ?>"><a href="<?php echo get_editar_simulado_url() ?>">Novo Simulado</a></li>
					<?php endif; ?>

					<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING, REVISOR))) : ?>
					<li class="<?php menu_ativo_inativo('prioridade_simulados') ?>"><a href="<?= get_prioridade_simulados_url() ?>">Prioridade de Simulados</a></li>
					<?php endif; ?>
				</ul>
			</li>
			<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING))) : ?> 
			<li class="<?php menu_ativo_inativo(array('listar_produtos','editar_produto')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Produtos</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_produtos') ?>"><a href="<?php echo get_listar_cadernos_produtos_url() ?>">Listar Todos</a></li>
					<li class="<?php menu_ativo_inativo('editar_produto') ?>"><a href="<?php echo get_editar_caderno_produto_url() ?>">Novo Produto</a></li>
				</ul>
			</li>
			<?php endif; ?>
			<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING))) : ?> 
			<li class="<?php menu_ativo_inativo(array('listar_rankings','editar_ranking')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Rankings</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_rankings') ?>"><a href="<?= get_listar_rankings_url() ?>">Listar Todos</a></li>
					<?php if(is_administrador() || is_revisor_sq() || is_coordenador_sq() || is_coordenador_coaching()) : ?>
					<li class="<?php menu_ativo_inativo('editar_ranking') ?>"><a href="<?= get_editar_ranking_url() ?>">Novo Ranking</a></li>
					<?php endif; ?>
				</ul>
			</li>
			<?php endif; ?>
			<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, COORDENADOR_COACHING))) : ?> 
			<li class="<?php menu_ativo_inativo(array('listar_questoes_repetidas','editar_configs_questoes_repetidas','questoes_repetidas_execucoes')) ?>">
				<a href="#"><i class="fa fa-folder-o"></i> <span class="nav-label">Comparar Questões</span> <span class="fa arrow"></span></a>
				<ul class="nav nav-second-level collapse">
					<li class="<?php menu_ativo_inativo('listar_questoes_repetidas') ?>"><a href="<?= get_listar_questoes_repetidas_url() ?>">Listar Repetidas Pendentes</a></li>
					<li class="<?php menu_ativo_inativo('editar_configs_questoes_repetidas') ?>"><a href="<?= get_questoes_repetidas_configuracoes_url() ?>">Configurações</a></li>
					<li class="<?php menu_ativo_inativo('questoes_repetidas_execucoes') ?>"><a href="<?= get_questoes_repetidas_excecucoes_url() ?>">Execuções</a></li>
				</ul>
			</li>
			<?php endif; ?>
		</ul>
	</div>
</nav>