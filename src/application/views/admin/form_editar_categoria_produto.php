<div class="modal-dialog">
	<div class="modal-content modal-form-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Editar Categoria</h4>
		</div>
		<div class="modal-body">
		
			<div class="panel blank-panel">
				<div class="panel-body">
					<div class="panel-carregando">Carregando...</div>
					<div class="panel-conteudo">
						<?php echo form_open(get_editar_categoria_produto_url(), 'id="form-editar-categoria" class="form-horizontal"'); ?>
						<div class="form-group">
							<div class="col-sm-12">
								<input type="hidden" id="id_categoria" name="ccp_id">
								<?php echo form_input('ccp_nome', null,'id="nome_categoria" class="form-control m-b"'); ?>
							</div>
						</div>
						<div><?php echo form_submit('salvar', 'Salvar', 'class="btn btn-primary"'); ?></div>
						<?php echo form_close(); ?>
					</div>
                </div>
            </div>      
		</div>
	</div>
</div>