<?php
	admin_cabecalho_pagina("Editar Ranking <small>(Passo 3 - Definir Grupos)</small>"); 
?>

<?php ui_alerta($this->session->flashdata('sucesso'), ALERTA_SUCESSO); ?>

<div class="row margin-top-10">
	<div class="col-lg-3">
		<div class="ibox ">
	        <div class="ibox-title">
	            <h5>Sem Grupos</h5>
	        </div>
	        <div class="ibox-content">
				<div class="dd sortable">
	         	    <ol class="dd-list">
	         	    	<?php foreach ($sem_grupos as $item) : ?>
	                    <li class="dd-item" id="<?= $item['dis_id'] ?>">
	                        <div class="dd-handle"><?= $item['dis_nome'] ?></div>
	                    </li>
	                	<?php endforeach ?>
	                </ol>
	            </div>
	        </div>
		</div>
	</div>

	<div class="col-lg-9">
		<?php foreach ($com_grupos as $chave => $grupo) : ?>
			<div class="col-lg-3">
				<div class="ibox ">
			        <div class="ibox-title">
			            <h5><?= $grupo['nome'] ?></h5>
			        </div>
			        <div class="ibox-content">
						<div class="dd sortable">
			         	    <ol class="dd-list" data-ran-id="<?= $chave ?>">
			         	    	<?php foreach ($grupo['itens'] as $item) : ?>
			                    <li class="dd-item" id="<?= $item['dis_id'] ?>">
			                        <div class="dd-handle"><?= $item['dis_nome'] ?></div>
			                    </li>
			                	<?php endforeach ?>
			                </ol>
			            </div>
			        </div>
				</div>
			</div>
		<?php endforeach ?>

	</div>
</div>

<div>
	<div>
		<div style="margin-top: -30px">
			<form id="form" method="post">
				<?= get_admin_botoes_navegacao_ranking($id, $pagina) ?>
			</form>
		</div>
	</div>
</div>

<script>
$(function() {
	$(".dd-list").sortable({ connectWith: '.dd-list' });
	$(".dd-list").disableSelection();

	$('#avancar').click(function(e) {
		$('#form input[type=hidden]').remove();

		$(".dd-list").each(function() {
			var ran_id = $(this).data('ran-id');

			if(ran_id == undefined) {

			}

			var dis_ids = JSON.stringify($(this).sortable('toArray', {attribute: 'id'}));


			$('<input>').attr({
			    type: 'hidden',
			    name: "grupos["+ ran_id + "]",
			    value: dis_ids
			}).appendTo('#form');
		});

	});
});
</script>