<?php
	$prefixo = isset($cargo['car_id']) ? 'Editar' : 'Novo'; 
	admin_cabecalho_pagina($prefixo . " Cargo"); 
?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<?php echo form_open(get_editar_cargo_url($cargo), 'id="form-cargo" class="form-horizontal"'); ?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nome</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="car_nome" value="<?php echo isset($cargo['car_nome']) ? $cargo['car_nome'] : ''; ?>"/>
                    	<span class="help-block m-b-none error"><?php echo form_error('car_nome'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <?php admin_botoes_salvar_cancelar(get_listar_cargos_url())?>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>