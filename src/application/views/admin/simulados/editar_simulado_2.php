<?php
	$titulo = isset($simulado['sim_id']) ? 'Editar Simulado - ' . $simulado['sim_nome'] : 'Novo Simulado';
?>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row" >
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5><?php echo $titulo; ?></h5>
				</div>
				<div class="ibox-content">
					<?php echo form_open_multipart(get_editar_simulado_url($simulado, $passo), 'id="form-simulado" class="wizard-big"'); ?>
						<input type="hidden" name="passo" value="<?php echo $passo; ?>" />
						<div class="row">
						<ul style="list-style: none !important;">
							<li title="<?= get_nome_etapa_edicao_simulado(1) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 1); ?>">Passo 1 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(2) ?>" class="simulado-passo-atual"><a href="#">Passo 2 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(3) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 3); ?>">Passo 3 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(4) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 4); ?>">Passo 4 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(5) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 5); ?>">Passo 5 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(6) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 6); ?>">Passo 6 de 6</a></li>
						</ul>
						</div>
						<br/>
						<fieldset class="simulado-box">
							<h2><?= get_nome_etapa_edicao_simulado(2) ?></h2>
							<div class="row" style="margin-bottom: 30px;">
								<?php foreach($array_multiselect as $multiselect) : ?>
									<div class="col-lg-8">
										<div class="form-group">
											<?php echo form_multiselect("{$multiselect[0]}[]", $multiselect[1], $multiselect[2], "id='{$multiselect[0]}' class='multiselect form-control m-b'"); ?>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						</fieldset>
						<br/>
						<div class="col-lg-12">
							<div class="form-group">
								<div style="float: right;">									
									<?php echo get_admin_botao_submit('Salvar', 'simulado_salvar', '', 'title="Salva o passo atual e redireciona para listagem de simulados."')?>
									<a class='btn btn-primary' title='Volta para o passo anterior sem salvar.' href="<?php echo get_editar_simulado_url($simulado, 1); ?>">Voltar</a>
									<?php echo get_admin_botao_submit('Próximo', 'simulado_passo', '', 'title="Salva o passo atual e redireciona para o próximo passo."')?>
									<a class='btn btn-white' title="Cancela as alterações que não foram salvas da etapa atual e redireciona para listagem de simulados." onclick="cancelar();" href='#'>Cancelar</a>
								</div>
							</div>
						</div>
						<div style="clear:both"></div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function cancelar(){
		var r = confirm('Deseja realmente cancelar a criação/edição do simulado?');
		if(r == true) {
			window.location.href='/questoes/admin/listar_simulados/0';
		}
	}

	$.validator.addMethod('require-one', function(value) {
		return $('.require-one:checked').size() > 0;
	},'Selecione pelo menos uma das opções.');

	<?php $arra_inputs = get_array_input_names(); ?>
	<?php $array_labels = get_array_input_labels(); ?>
	<?php foreach ($arra_inputs as $key => $input): ?>
		$("<?php echo "#{$input}"; ?>").on("change", function(event, data) { 
			$(this).trigger("chosen:updated");
		});
		$("<?php echo "#{$input}"; ?>").chosen({
			width: '750px',
			no_results_text: "Nenhum resultado com:",
			placeholder_text_multiple: "<?php echo $array_labels[$key]; ?>",
			disable_search_threshold: 10
		});
	<?php endforeach; ?>
</script>