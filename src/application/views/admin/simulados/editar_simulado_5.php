<?php
	$titulo = isset($simulado['sim_id']) ? 'Editar Simulado - ' . $simulado['sim_nome'] : 'Novo Simulado';
?>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row" >
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5><?php echo $titulo; ?></h5>
				</div>
				<div class="ibox-content">
					<?php echo form_open_multipart(get_editar_simulado_url($simulado, $passo), 'id="form-simulado" class="wizard-big"'); ?>
						<input type="hidden" name="passo" value="<?php echo $passo; ?>" />
						<div class="row">
						<ul style="list-style: none !important;">
							<li title="<?= get_nome_etapa_edicao_simulado(1) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 1); ?>">Passo 1 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(2) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 2); ?>">Passo 2 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(3) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 3); ?>">Passo 3 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(4) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 4); ?>">Passo 4 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(5) ?>" class="simulado-passo-atual"><a href="#">Passo 5 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(6) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 6); ?>">Passo 6 de 6</a></li>
						</ul>
						</div>
						<br/>
						<fieldset class="simulado-box">
							<h2><?= get_nome_etapa_edicao_simulado(5) ?></h2>
							<div class="row">
								<div class="col-lg-12">
                    				<div class="ibox float-e-margins">
                    					<div class="ibox-content">
											<div class="panel-body">
												<div class="panel-group" id="revisores"><?php echo $simulado_revisores; ?></div>
											</div>
	                    				</div>
                    				</div>
                    			</div>
                    			<div class="col-lg-12">
									<div class="form-group">
										<div class="form-group">
											<label>
												<input id="revisao-ativada" type="checkbox" name="sim_revisao_ativada" value="1" <?php echo isset($simulado['sim_revisao_ativada']) && $simulado['sim_revisao_ativada'] ? 'checked' : '' ?>>  Permitir revisão de questões pelos professores
											</label>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
						<br/>
						<div class="col-lg-12">
							<div class="form-group">
								<div style="float: right;">
									<?php echo get_admin_botao_submit('Salvar', 'simulado_salvar', '', 'title="Salva o passo atual e redireciona para listagem de simulados."')?>
									<a class='btn btn-primary' title='Volta para o passo anterior sem salvar.' href="<?php echo get_editar_simulado_url($simulado, 4); ?>">Voltar</a>
									<?php echo get_admin_botao_submit('Próximo', 'simulado_passo', '', 'title="Salva o passo atual e redireciona para o próximo passo."')?>
									<a class='btn btn-white' title="Cancela as alterações que não foram salvas da etapa atual e redireciona para listagem de simulados." onclick="cancelar();" href='#'>Cancelar</a>
								</div>
							</div>
						</div>
						<div style="clear:both"></div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function cancelar(){
		var r = confirm('Deseja realmente cancelar a criação/edição do simulado?');
		if(r == true) {
			window.location.href='/questoes/admin/listar_simulados/0';
		}
	}

	$.validator.addMethod('require-one', function(value) {
		return $('.require-one:checked').size() > 0;
	},'Selecione pelo menos uma das opções.');

</script>