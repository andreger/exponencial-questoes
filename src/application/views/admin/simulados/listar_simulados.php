<?php 
	exibir_mensagem();
?>
<div class="listar-simulados">
	<form method="post" action="<?php echo get_listar_simulados_url(); ?>">
    <div class="row">
    	<div class="col-md-2">Buscar por:</div>
		<div class="col-md-10">
			<input id="search" class="search_filter form-control" type="text" placeholder="Digite parte do nome do simulado..." name="search_filter" value="<?= $texto ?>">
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">Status:</div>
		<div class="col-md-10">
			<span style="margin-right: 20px">
				<?= form_radio('check_simulado', TODAS, $status_selecionado == TODAS) ?> Todos
			</span>

			<span style="margin-right: 20px">
				<?= form_radio('check_simulado', ATIVO, $status_selecionado == ATIVO) ?> Ativo
			</span>

			<span style="margin-right: 20px">
				<?= form_radio('check_simulado', INATIVO, $status_selecionado == INATIVO) ?> Inativo
			</span>

			<span style="margin-right: 20px">
				<?= form_radio('check_simulado', INCONSISTENTE, $status_selecionado == INCONSISTENTE) ?> Inconsistente
			</span>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">Bancas:</div>
		<div class="col-md-10">
			<?php echo form_multiselect('ban_ids[]', $bancas, $banca_selecionada, 'id="ban_id" class="form-control"'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">Instituições:</div>
		<div class="col-md-10">
			<?php echo form_multiselect('org_ids[]', $orgaos, $orgao_selecionado, 'id="org_id" class="form-control"'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">Disciplinas:</div>
		<div class="col-md-10">
			<?php echo form_multiselect('dis_ids[]', $disciplinas, $disciplina_selecionada, 'id="dis_id" class="form-control"'); ?>
		</div>
	</div>

	<?php if(is_administrador() || is_coordenador_sq() || is_coordenador_coaching()) : ?>
		<div class="row">
			<div class="col-md-2">Professores:</div>
			<div class="col-md-10">
				<?php echo form_multiselect('prof_ids[]', $professores, $professor_selecionado, 'id="prof_id" class="form-control"'); ?>
			</div>
		</div>
	<?php endif; ?>

	<div class="row">
		<div class="col-md-2">Situação:</div>
		<div class="col-md-10">
			<span style="margin-right: 20px">
				<?= form_radio('check_situacao', TODAS, $situacao_selecionada == TODAS) ?> Todos
			</span>

			<span style="margin-right: 20px">
				<?= form_radio('check_situacao', REVISADO, $situacao_selecionada == REVISADO) ?> Revisados
			</span>
			
			<span style="margin-right: 20px">
				<?= form_radio('check_situacao', PENDENTE, $situacao_selecionada == PENDENTE) ?> Pendentes
			</span>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">Prazo:</div>
		<div class="col-md-10">
			<span style="margin-right: 20px">
				<?= form_checkbox('check_prazo[]', PRAZO_SIMULADO_ATRASADOS, in_array(PRAZO_SIMULADO_ATRASADOS, $prazos_selecionados)) ?> Atrasado
			</span>
			
			<span style="margin-right: 20px">
				<?= form_checkbox('check_prazo[]', PRAZO_SIMULADO_FUTURO, in_array(PRAZO_SIMULADO_FUTURO, $prazos_selecionados)) ?> Futuro
			</span>

			<span style="margin-right: 20px">
				<?= form_checkbox('check_prazo[]', PRAZO_SIMULADO_PRESENTE, in_array(PRAZO_SIMULADO_PRESENTE, $prazos_selecionados)) ?> Presente
			</span>

			<span style="margin-right: 20px">
				<?= form_checkbox('check_prazo[]', PRAZO_SIMULADO_SEM_PREVISAO, in_array(PRAZO_SIMULADO_SEM_PREVISAO, $prazos_selecionados)) ?> Sem previsão
			</span>
		</div>
	</div>

	<?php
	/* Adicionar um combo para possibilitar a ordenação de simulados
	 * 
	 * [JOULE][J1] UPGRADE - Controle de prioridade dos simulados
	 * https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/340
	 */
	?>
	<div class="row">
		<div class="col-md-2"><strong>Ordenar por:</strong></div>
		<div class="col-md-10">
			<?= form_dropdown('ordenacao', $ordenacoes, $ordenacao_selecionada, 'id="dis_id" class="form-control"'); ?>
		</div>
	</div>


	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-10">
			<input  class="btn btn-primary" type="submit" name="filtrar" value="Filtrar">
		</div>
	</div>
	</form>

	<div class="row">
		<div class="row" id="cont-simulado" style="font-size: 14px; margin: 10px 0 10px 15px"></div>
		
		<?php if($simulados) : ?>
		
		<div class="row"><?php echo $links ?></div>

		<div id="simulados">
			<?php foreach ($simulados as $simulado) :?>
				<?php 
					$classe = "";
					$botao = "btn-azul";
					switch ($simulado['sim_status']) {
						case INATIVO :
							$classe = "yellow-bg";
// 							$botao = "btn-warning";
							$botao_label = "Ativar";
							$icone = "fa-toggle-on";
							break;
						case ATIVO :
							$classe = "navy-bg";
// 							$botao = "btn-primary";
							$botao_label = "Inativar";
							$icone = "fa-toggle-off";
							break;
						case INCONSISTENTE :
							$classe = "red-bg";
// 							$botao = "btn-danger";
							break;
					}
				?>
				<div class="widget-simulado col-xs-4">
					<div id="modal-produto-<?php echo $simulado['sim_id']; ?>" class="modal fade modal-produto" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h3 class="m-t-none m-b text-center">
										<?= get_tema_image_tag('sq-info.png', 22, 22) . ' ' . get_nome_produto($simulado['prd_id']); ?>
									</h3>
								</div>
								<div class="modal-body">
									<?php echo get_informacao_produto($simulado['prd_id']); ?>
								</div>
							</div>
						</div>
					</div>
				
					<div id="modal-form-<?php echo $simulado['sim_id']; ?>" class="modal fade" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Fechar</span></button>
									<h3 class="m-t-none m-b">Designar a Aluno</h3>
								</div>
								
								<div class="modal-body">
									<div class="panel blank-panel">
										<div class="panel-heading">
											<div class="panel-options">
												<ul class="nav nav-tabs">
													<li class="active"><a href="#tab-1-<?php echo $simulado['sim_id']; ?>" data-toggle="tab">Designar Aluno</a></li>
													<li class=""><a href="#tab-2-<?php echo $simulado['sim_id']; ?>" data-toggle="tab">Alunos Designados</a></li>
												</ul>
											</div>
										</div>
										
										<div class="panel-body">
											<div class="tab-content">
												<div class="tab-pane active" id="tab-1-<?php echo $simulado['sim_id']; ?>">
												<?php echo form_open(get_designar_alunos_url($simulado), 'role="form" id="designar-form" class="form-inline form-resultado"'); ?>
													<div class="form-group col-md-12" style="margin-bottom: 20px">
														<label for="usu_ids">Selecionar alunos para quem designar este simulado</label>
														<div style="padding-bottom: 8px;">
															<label class="checkbox-inline"><?php echo form_checkbox('filtro_aluno[]', ALUNO_PODIO, in_array(ALUNO_PODIO, $filtro_aluno), 'id="check_podio" class="check-aluno"');?> Coaching Individual </label>
															<label class="checkbox-inline"><?php echo form_checkbox('filtro_aluno[]', ALUNO_COACHING, in_array(ALUNO_COACHING, $filtro_aluno), 'id="check_coaching" class="check-aluno"');?> Turma de Coaching </label>
														</div>
														<div class="select-alunos">
															<?php echo form_multiselect("usu_ids[]", $alunos, $aluno_selecionado, "id='usu_ids' class='multiselect'"); ?>
														</div>
														<div class="carregando-alunos" style="display:none; line-height: 33px">
															<img width="20" src='/wp-content/themes/academy/images/carregando.gif'> Carregando lista de usuários...
														</div>
													</div>
													<div class="form-group col-md-12" style="margin-bottom: 20px">
														<label>Simulado deve ser feito até: </label>
														<div class="input-group date"  style="width: 100%">
															<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
															<input name="data_conclusao" id="data_conclusao" type="text" class="form-control" value="">
														</div>
													</div>
													<div class="form-group col-md-12">
														<button class="btn btn-sm <?php echo $botao; ?>" id="designar-submit" type="submit" value="enviar"><strong>Enviar</strong></button>
														<button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
													</div>
												<?php echo form_close(); ?>
												</div>
												
												<div class="tab-pane" id="tab-2-<?php echo $simulado['sim_id']; ?>">
													<div id="usuarios_designados-<?php echo $simulado['sim_id']; ?>"></div>
												</div>
											</div>
										</div>
									</div>
			
								</div>
							</div>
						</div>
					</div>
					<div id="simulado-<?= $simulado['sim_id']; ?>" 
						class="<?= get_listar_simulado_class($simulado) ?>"
						data-sort="<?= $simulado['sim_id']; ?>">
						<div class="widget widget-admin <?= $classe; ?>">
							<div class="row">
								<div class="col-md-3" style="padding: 0 3px">
									<div class="simulado-img" style="min-height: 100px">
										<img src="<?= get_simulado_img_link($simulado['sim_id']); ?>" alt="<?= $simulado['sim_nome']?>"/>
									</div>
									<div>
										<a data-toggle="modal" href="#modal-produto-<?= $simulado['sim_id']; ?>">
											<?= get_tema_image_tag('sq-info.png', 22, 22) ?>
										</a>
									</div>	
								</div>
								
								<div class="col-md-7">
									<div style="min-height: 120px">
									 	<div class="nome_simulado">
											<div>
												<?= $simulado['sim_nome']?>
											</div>
											
											<div style="margin-top: 10px">
												<div class="nome-cargo text-left"><b>Foco:</b> <?= $simulado['car_nome']; ?></div>
											</div>
											
											<?php if($url_edital = get_url_edital_simulado($simulado)) : ?>
											<div style="margin-top: 10px">
												<a href="<?= $url_edital?>" target="_blank">Análise do Edital do Concurso</a>
											</div>
											<?php endif; ?>
										</div>
									</div>
									<div>
										<a class="btn <?= $botao; ?> btn-rounded-expo btn-block" href="<?= get_ranking_simulado_url($simulado['sim_id']) ?>"><i class="fa fa-trophy"></i> Ranking</a>

										<?php if(is_administrador() || is_coordenador_sq() || is_revisor_sq() || is_coordenador_coaching()) : ?>
											<a class="btn <?= $botao; ?> btn-rounded-expo btn-block" href="<?= get_rotulos_materias_simulado_url($simulado['sim_id']); ?>"><i class="fa fa-tag"></i> Rótulos de Matérias</a>
										<?php endif; ?>
										
										<?php if(pode_revisar_simulado($simulado['sim_id'])) : ?>
										<a class="btn <?= $botao; ?> btn-rounded-expo btn-block" href="<?= get_revisar_questoes_url($simulado['sim_id']) ?>"><i class="fa fa-search"></i> Revisar Questões</a>
										<?php endif; ?>
										
										<?php if(is_administrador() || is_coordenador_sq() || is_revisor_sq()  || is_coordenador_coaching()) : ?>
											<a class="btn <?= $botao; ?> btn-rounded-expo btn-block" href="<?= get_editar_simulado_url($simulado); ?>"><i class="fa fa-pencil"></i> Editar</a>
										<?php endif; ?>

										<?php if(is_administrador() || is_coordenador_sq() || is_revisor_sq() || is_coordenador_coaching()) : ?>
											<a class="btn <?= $botao; ?> btn-rounded-expo btn-block" href="<?= get_editar_revisores_simulado_url($simulado['sim_id']); ?>"><i class="fa fa-users"></i> Editar Revisores</a>
										<?php endif; ?>
									
										<?php if(is_administrador() || is_coordenador_sq() || is_consultor() || is_revisor_sq() || is_coordenador_coaching()) :?>
										<a data-toggle="modal" data-id="<?= $simulado['sim_id']; ?>" class="btn <?= $botao; ?> btn-rounded-expo btn-block botao-designar-aluno" href="#modal-form-<?= $simulado['sim_id']; ?>"><i class="fa fa-user"></i> Designar a Aluno</a>
										<?php endif; ?>
		
										<a class="btn <?php echo $botao; ?> btn-rounded-expo btn-block" href="<?= get_imprimir_meus_simulados_html_url($simulado); ?>" target="_blank"><i class="fa fa-print"></i> Imprimir</a>
										<a class="btn btn-azul btn-rounded-expo btn-block" href="<?= get_imprimir_gabarito_simulado_url($simulado); ?>" target="_blank"><i class="fa fa-print"></i> Gabarito</a>
										
										<?php if(is_administrador() || is_coordenador_sq() || is_revisor_sq() || is_coordenador_coaching()) :?>
										<a data-toggle="modal" data-target="#modal-duplicar" class="btn <?php echo $botao; ?> btn-rounded-expo btn-block" href="<?php echo get_modal_duplicar_simulados_url($simulado); ?>"><i class="fa fa-files-o"></i> Duplicar Simulado</a>
										<?php endif; ?>
										
										<?php if(is_administrador() || is_coordenador_sq() || is_revisor_sq() || is_coordenador_coaching()) :?>
											<?php if($simulado['sim_status'] == INCONSISTENTE) : ?>
												<a class="btn <?php echo $botao; ?> btn-rounded-expo btn-block" href="<?php echo get_alterar_status_simulado_url($simulado['sim_id']); ?>"><i class="fa fa-toggle-off"></i> Inativar Simulado</a>
											<?php elseif($simulado['sim_status'] == INATIVO) : ?>
												<a class="btn <?php echo $botao; ?> btn-rounded-expo btn-block" href="<?php echo get_alterar_status_simulado_url($simulado['sim_id']); ?>"><i class="fa fa-toggle-on"></i> Ativar Simulado</a>
											<?php elseif($simulado['sim_status'] == ATIVO) : ?>
												<a class="btn <?php echo $botao; ?> btn-rounded-expo btn-block" href="<?php echo get_alterar_status_simulado_url($simulado['sim_id']); ?>"><i class="fa fa-toggle-off"></i> Tornar Inconsistente</a>
											<?php endif; ?>
										<?php endif; ?>
										
										<?php if(is_administrador() || is_coordenador_sq() || is_revisor_sq() || is_coordenador_coaching()) :?>
											<?php if(is_revisao_ativada($simulado['sim_id'])) : ?>
												<a class="btn <?php echo $botao; ?> btn-rounded-expo btn-block" href="<?php echo get_inativar_revisao_url($simulado['sim_id']); ?>"><i class="fa fa-toggle-off"></i> Desativar Revisão</a>
											<?php else : ?>
												<a class="btn <?php echo $botao; ?> btn-rounded-expo btn-block" href="<?php echo get_ativar_revisao_url($simulado['sim_id']); ?>"><i class="fa fa-toggle-on"></i> Ativar Revisão</a>
											<?php endif; ?>
										<?php endif; ?>

										<?php if(is_administrador() || is_coordenador_sq() || is_revisor_sq() || is_coordenador_coaching()) :?>
											<a class="btn <?= $botao; ?> btn-rounded-expo btn-block" href="<?= get_atualizar_notas_url($simulado['sim_id']); ?>"><i class="fa fa-refresh"></i> Atualizar Notas</a>
										<?php endif; ?>

										<?php if(is_administrador() || is_coordenador_sq() || is_coordenador_coaching()) :?>
											<a data-toggle="modal" data-target="#modal-comentarios" class="btn <?php echo $botao; ?> btn-rounded-expo btn-block" href="<?php echo get_modal_comentarios_professores_simulados_url($simulado['sim_id']); ?>"><i class="fa fa-users"></i> Comentário Professores</a>
										<?php endif; ?>

										<?php if(is_administrador() || is_coordenador_sq()) :?>
											<span class="btn <?= $botao; ?> btn-rounded-expo btn-block" style="cursor: default;"><i class="fa fa-clock-o"></i> Data entrega: <?= $simulado['sim_data_entrega']?converter_para_ddmmyyyy($simulado['sim_data_entrega']):"\t--" ?></span>
										<?php endif; ?>

									</div>
								</div>
								<div class="col-md-2 text-right total-questoes mobile-hide" data-toggle="tooltip" title="Quantidade total de questões, considerando todas as provas.">
									<?php echo is_null($simulado['total_questoes']) ? 0 : $simulado['total_questoes']; ?>
								</div>
							</div>
							
						</div>
					</div><div class="clearfix"></div>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="clearfix"></div>
		<?php endif ?>
	</div>
	<div class="row"><?php echo $links ?></div>
</div>
<div id="modal-duplicar" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content"></div>
	</div>
</div>
<div id="modal-comentarios" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content"></div>
	</div>
</div>


<script>
$(document).ready(function() {

	$('#modal-comentarios').on('shown.bs.modal', function () {
		$(this).removeData('bs.modal');
	});
	
	$('.check-aluno').change(function(){
		$(".select-alunos").hide();
		$(".carregando-alunos").show();
		
		var filtro_aluno = '';
		
		$('input[name="filtro_aluno[]"]:checked').each(function() {
			filtro_aluno += this.value + ',';
		});

		if(filtro_aluno){
			filtro_aluno = filtro_aluno.substring(0, filtro_aluno.length-1);
		}
		
		$("#usu_ids").prop('disabled', true);

		$.ajax({
			url: "<?php echo get_ajax_main_alunos_acompanhamento_url(PAGE_SIMULADOS);?>",
			method: "POST",
			cache: false,
			data: { 
				filtro_aluno: filtro_aluno
			}
		})
		.done(function( msg ) {
			$(".select-alunos").html(msg);
			$(".multiselect").chosen({
				disable_search_threshold: 10,
				width: '100%',
				no_results_text: "Nenhum aluno com:",
				placeholder_text_multiple: "Selecione alguns alunos..."
			});
			$(".multiselect").trigger("chosen:updated");

			$(".carregando-alunos").hide();
			$(".select-alunos").show();	
		});

	});

	$('#ban_id').chosen({
		disable_search_threshold: 10,
		no_results_text: "Nenhuma banca com:",
		placeholder_text_multiple: "Selecione algumas bancas..."
	});

	$('#org_id').chosen({
		disable_search_threshold: 10,
		no_results_text: "Nenhuma instituição com:",
		placeholder_text_multiple: "Selecione algumas instituições..."
	});

	$('#dis_id').chosen({
		disable_search_threshold: 10,
		no_results_text: "Nenhuma disciplina com:",
		placeholder_text_multiple: "Selecione algumas disciplinas..."
	});

	$('#prof_id').chosen({
		disable_search_threshold: 10,
		no_results_text: "Nenhum professor com:",
		placeholder_text_multiple: "Selecione alguns professores..."
	});
    
	// $('#ban_id').change(function() {
	// 	filtrar_divs();
 //    });
	
	// $('#org_id').change(function() {
	// 	filtrar_divs();
 //    });
    
 //    $('#dis_id').change(function() {
	// 	filtrar_divs();
 //    });

 //    $('input[type=radio][name=check_simulado]').change(function() {
 //    	filtrar_divs();
	// });

	// $("#search").change( function () {
	// 	filtrar_divs();
	// }).keyup( function () {
	// 	// fire the above change event after every letter
	// 	$(this).change();
	// });

	function filtrar_divs() {
		$("#simulados").find("div").show();
		var filter = $("#search").val(); // get the value of the input, which we filter on

		if (filter) {
			$("#simulados").find(".nome_simulado:not(:Contains(" + filter + "))").closest('.widget-simulado').hide();
		}
		var banId = $('#ban_id').val();
		console.log(banId);
		if(banId != null) {
			var elementos = [];
			for(i = 0; i < banId.length; i++) {
				elementos[i] = '.banca-' + banId[i];
			}
			$("#simulados").find("div.simulado:not('" + elementos.join() + "')").closest('.widget-simulado').hide();
		}
		
		var orgId = $('#org_id').val();
		if(orgId != null) {
			mostrar_tudo = false;

			var elementos = [];
			for(i = 0; i < orgId.length; i++) {
				elementos[i] = '.orgao-' + orgId[i];
			}
			$("#simulados").find("div.simulado:not('" + elementos.join() + "')").closest('.widget-simulado').hide();
		}

		var disId = $('#dis_id').val();
		if(disId != null) {
			mostrar_tudo = false;

			var elementos = [];
			for(i = 0; i < disId.length; i++) {
				elementos[i] = '.disciplina-' + disId[i];
			}
			$("#simulados").find("div.simulado:not('" + elementos.join() + "')").closest('.widget-simulado').hide();
		}
		
		var radio = $('input[type=radio][name=check_simulado]:checked').val();
		if (radio == <?php echo ATIVO; ?>) {
			$("#simulados").find("div.simulado:not(.status-<?php echo ATIVO; ?>)").closest('.widget-simulado').hide();
		} else if (radio == <?php echo INATIVO; ?>) {
			$("#simulados").find("div.simulado:not(.status-<?php echo INATIVO; ?>)").closest('.widget-simulado').hide();
		} else if (radio == <?php echo INCONSISTENTE; ?>) {
			$("#simulados").find("div.simulado:not(.status-<?php echo INCONSISTENTE; ?>)").closest('.widget-simulado').hide();
		}

		var situacao = $('input[type=radio][name=check_situacao]:checked').val();
		if (situacao == <?= PENDENTE ?>) {
			$("#simulados").find("div.simulado:not(.situacao-<?= PENDENTE ?>)").closest('.widget-simulado').hide();
		} else if (situacao == <?= REVISADO ?>) {
			$("#simulados").find("div.simulado:not(.situacao-<?= REVISADO ?>)").closest('.widget-simulado').hide();
		}

		//contar_simulados();
	}
	// Case-Insensitive Contains
	$.expr[':'].Contains = function(a,i,m){
	    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
	};
    $('.input-group.date').datepicker({
    	format: "dd/mm/yyyy",
    	language: 'pt-BR',
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    }).datepicker("setDate", new Date());
    $(".multiselect").chosen({
    	disable_search_threshold: 10,
    	width: '100%',
		no_results_text: "Nenhum aluno com:",
		placeholder_text_multiple: "Selecione alguns alunos..."
    });
   	$(".multiselect").on("change", function(event, data) {
   	    $(this).trigger("chosen:updated");
   	});

});
</script>