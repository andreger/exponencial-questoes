<?php
	$titulo = isset($simulado['sim_id']) ? 'Editar Simulado - ' . $simulado['sim_nome'] : 'Novo Simulado';
?>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row" >
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5><?= $titulo; ?></h5>
				</div>
				<div class="ibox-content">
					<?= form_open_multipart(get_editar_simulado_url($simulado, $passo), 'id="form-simulado" class="wizard-big"'); ?>
						<input type="hidden" name="img_submit" id="img_submit" value="" />
						<input type="hidden" name="pdf_submit" id="pdf_submit" value="" />
						<input type="hidden" name="passo" value="<?= $passo; ?>" />
						<div class="row">
						<ul style="list-style: none !important;">
							<li title="<?= get_nome_etapa_edicao_simulado(1) ?>" class="simulado-passo-atual"><a href="#">Passo 1 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(2) ?>" class="simulado-passo"><a href="<?= is_null($simulado['sim_id'])?'#': get_editar_simulado_url($simulado, 2); ?>">Passo 2 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(3) ?>" class="simulado-passo"><a href="<?= is_null($simulado['sim_id'])?'#': get_editar_simulado_url($simulado, 3); ?>">Passo 3 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(4) ?>" class="simulado-passo"><a href="<?= is_null($simulado['sim_id'])?'#': get_editar_simulado_url($simulado, 4); ?>">Passo 4 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(5) ?>" class="simulado-passo"><a href="<?= is_null($simulado['sim_id'])?'#': get_editar_simulado_url($simulado, 5); ?>">Passo 5 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(6) ?>" class="simulado-passo"><a href="<?= is_null($simulado['sim_id'])?'#': get_editar_simulado_url($simulado, 6); ?>">Passo 6 de 6</a></li>
						</ul>
						</div>
						<br/>
						<fieldset class="simulado-box">
							<h2><?= get_nome_etapa_edicao_simulado(1) ?></h2>
							<div class="row" >
								<div class="col-lg-8">
									<div class="form-group">
										<label>Nome do Simulado</label>
										<input type="text" class="required form-control" name="sim_nome" value="<?= isset($simulado['sim_nome']) ? $simulado['sim_nome'] : ''; ?>"/>
									</div>
								</div>
								<?php foreach ($array_combo as $combo) : ?>
								<div class="col-lg-8">
									<div class="form-group">
										<label><?= $combo[0]; ?></label>
										<?= form_dropdown($combo[1], $combo[2], $combo[3], "id='{$combo[1]}' class='required form-control m-b'"); ?>
									</div>
								</div>
								<?php endforeach; ?>

								<div class="col-lg-8">
									<div class="form-group">
										<label>Simulados Associados</label>
										<?= form_multiselect('simulados_associados[]', $simulados_combo, $simulados_associados_selecionados, "id='simulados_associados' class='multiselect form-control m-b'"); ?>
									</div>
								</div>
								<div class="col-lg-8">
									<div class="form-group">
										<label>Carregar imagem</label><img src="<?= get_img_loading(); ?>" style="display: none;" id="loading_image">
										<?php if($has_simulado_img) :?>
											<div><img style="max-width: 400px" src="<?= get_simulado_img_link($simulado['sim_id']); ?>?t=<?= time() ?>" alt="<?= $simulado['sim_nome']?>"/></div>
											<div><a id="remover_img" href="<?= get_exluir_simulado_img_link($simulado['sim_id']);?>">Remover imagem</a></div>
										<?php endif;?>
										<?= form_upload('img_simulado', "class='form-control'"); ?>
										<h4><?= $this->session->flashdata('img_simulado');?></h4>
									</div>
								</div>

								<div class="col-lg-8">
									<div class="form-group">
										<div class="form-group">
											<label>
												<input id="sortear-questoes" type="checkbox" name="sim_sortear_questoes" value="1">  Sortear questões ao finalizar edição do simulado
											</label>
										</div>
									</div>
								</div>
								<div class="col-lg-8">
									<div class="form-group">
										<div class="form-group">
											<label>
												<input id="pontos-negativos" type="checkbox" name="sim_pontos_negativos" value="1" <?= $simulado['sim_pontos_negativos']?"checked":""; ?> />  Pontos negativos
											</label>
										</div>
									</div>
								</div>
								<div class="col-lg-8">
									<div class="form-group">
										<div class="form-group">
											<label>Peso dos erros</label>
											<input id="peso-erros" type="text" class="form-control" name="sim_peso_erros" value="<?= isset($simulado['sim_peso_erros']) ? numero_brasileiro($simulado['sim_peso_erros']) : '1'; ?>" <?= $simulado['sim_pontos_negativos']?"":"disabled"; ?>/>
										</div>
									</div>
								</div>
								<div class="col-lg-8">
									<div class="form-group">
										<div class="form-group">
											<label>
												<input id="media-ponderada" type="checkbox" name="sim_media_ponderada" value="1" <?= $simulado['sim_media_ponderada']?"checked":""; ?> />  Média ponderada
											</label>
										</div>
									</div>
								</div>
								<div class="col-lg-8">
									<div class="form-group">
										<div class="form-group">
											<label>Data de entrega</label>
											<input id="data-entrega" type="text" class="form-control" name="sim_data_entrega" value="<?= isset($simulado['sim_data_entrega']) ? converter_para_ddmmyyyy($simulado['sim_data_entrega']) : ''; ?>"/>
										</div>
									</div>
								</div>

								<div class="col-lg-8">
									<div class="form-group">
										<div class="form-group">
											<label>URL do Edital</label>
											<input placeholder="https://" type="text" class="form-control" name="sim_url_edital" value="<?= $simulado['sim_url_edital'] ? $simulado['sim_url_edital'] : ''; ?>"/>
										</div>
									</div>
								</div>

							</div>
						</fieldset>
						<br/>
						<div class="col-lg-12">
							<div class="form-group">
								<div style="float: right;">
									<?= get_admin_botao_submit('Salvar', 'simulado_salvar', '', 'title="Salva o passo atual e redireciona para listagem de simulados."')?>
									<?= get_admin_botao_submit('Próximo', 'simulado_passo', '', 'title="Salva o passo atual e redireciona para o próximo passo."')?>
									<a class='btn btn-white' title="Cancela as alterações que não foram salvas da etapa atual e redireciona para listagem de simulados." onclick="cancelar();" href='#'>Cancelar</a>
								</div>
							</div>
						</div>
						<div style="clear:both"></div>
					<?= form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>

	function cancelar(){
		var r = confirm('Deseja realmente cancelar a criação/edição do simulado?');
		if(r == true) {
			window.location.href='/questoes/admin/listar_simulados/0';
		}
	}

	$(document).ready(function(){
		//$('#peso-erros').mask('9,9');
		$('#peso-erros').mask('0,00', {reverse: false});

		$.fn.datepicker.dates['pt'] = {
			format: 'dd/mm/yyyy',
			days: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			daysMin: ['D','S','T','Q','Q','S','S','D'],
			daysShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			months: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthsShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			today: 'Hoje',
			weekStart: 0
		};

		$("#data-entrega").datepicker({language: 'pt'});
	});

	$.validator.addMethod('require-one', function(value) {
		return $('.require-one:checked').size() > 0;
	},'Selecione pelo menos uma das opções.');

	$("#simulados_associados").on("change", function(event, data) {
		$(this).trigger("chosen:updated");
	});

	$("#simulados_associados").chosen({
		no_results_text: "Nenhum resultado com:",
		placeholder_text_multiple: "Escolha os simulados a serem associados",
		disable_search_threshold: 10
	});

	<?php $arra_inputs = get_array_input_names(); ?>
	<?php $array_labels = get_array_input_labels(); ?>
	<?php foreach ($arra_inputs as $key => $input): ?>
		$("<?= "#{$input}"; ?>").on("change", function(event, data) {
			$(this).trigger("chosen:updated");
		});
		$("<?= "#{$input}"; ?>").chosen({
			width: '750px',
			no_results_text: "Nenhum resultado com:",
			placeholder_text_multiple: "<?= $array_labels[$key]; ?>",
			disable_search_threshold: 10
		});
	<?php endforeach; ?>

	$("#form-simulado").on("submit", function(event, data){
		return $(this).valid();
	});
	//img
	$("#form-simulado").validate({
		invalidHandler: function(event, validator) {
			var img_submit = $('#img_submit').val();

			if (img_submit == "submit") {
				$('#img_submit').val("");
				$('input[name=img_simulado]').val('');
			}
		},
		errorPlacement: function (error, element) { element.before(error); }
	});

	$('input[name=img_simulado]').change(function() {
		$('#loading_image').show();
		$('#img_submit').val("submit");
		$("#form-simulado").submit();
	});

	$("#remover_img").click(function() {
		$('#loading_image').show();
	});

	$('#pontos-negativos').change(function(e){
		if(this.checked){
			$('#peso-erros').prop('disabled', false);
		}else{
			$('#peso-erros').prop('disabled', true);
		}
	});

</script>