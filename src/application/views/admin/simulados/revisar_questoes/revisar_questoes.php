<?php admin_cabecalho_pagina("Revisar Questões - " . $simulado['sim_nome']); ?>
<?php exibir_mensagem() ?>
<div class="row ibox-title legenda-revisar-questoes"><h5>Sinalizações de simulados</h5>
	<div class="ibox-content">
		<div class="col-lg-3"><span>*</span>Contém questão não comentada por professor</div>
		<div class="col-lg-3"><span>!</span>Contém questão sem assunto/sem disciplina</div>
		<div class="col-lg-3"><span>#</span>Contém questão anulada ou desatualizada</div>
		<div class="col-lg-3"><span>&</span>Contém questão presente em outro simulado ativo</div>
	</div>
</div>
<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
	<div class="btn-tools">
		<div class="col-lg-8">
			<?php if(isset($disciplina_selecionada)) :?>
				<?php if($pode_adicionar) : ?>
				<a id="selecionar-questoes" class="btn btn-outline btn-default" data-toggle="modal" href="#modal-form-filter"><i class="fa fa-plus"></i> Selecionar Questões </a>
				<?php endif; ?>
				<?php if(isset($is_resultado)) : ?>
				<a id="salvar"  data-permitidas="<?php echo isset($permitidas_questoes) ? $permitidas_questoes : 0 ?>" class="btn btn-outline btn-default" data-toggle="modal" href="#"><i class="fa fa-check"></i> Incluir selecionadas </a>
				<a class="btn btn-outline btn-default" data-toggle="modal" href="<?php echo get_revisar_questoes_url($simulado_id, $disciplina_selecionada) . $busca_query_string ?>"><i class="fa fa-undo"></i> Voltar </a>
				<?php else : ?>
				<a class="btn btn-outline btn-default" data-toggle="modal" href="#modal-confirm"><i class="fa fa-times"></i> Remover Questões </a>
				<?php endif; ?>
							
			<div id="modal-form-filter" class="modal fade" aria-hidden="true">
				<?php $this->view(get_admin_simulados_revisar_questoes_filtro_modal_view_url()); ?>
			</div>
			
			
			<div id="modal-confirm"  class="modal fade" aria-hidden="true">
				<div class="modal-dialog">
					<div class='modal-content modal-form-content'>
						<div class='modal-header'>
							<button type='button' class='close' data-dismiss='modal'>&times;</button>
							<h4 class='modal-title'>Remover Questões</h4>
						</div>
						<div class='modal-body'>
						
							<div class='panel blank-panel'>
								<div class='panel-body modal-exclusao'>
									<div class='modal-exclusao-mensagem'>Deseja remover as questões selecionadas do simulado atual?</div>
									
									<div>
										<div>
											<input type='hidden' id='item_id' name='item_id'>
											<input type='button' id="botao-sim" value='Sim' name='submit' class='btn btn-primary'>
											<input type='button' value='Não' data-dismiss='modal' class='btn btn-white'>
										</div>
									</div>
				                </div>
				            </div>      
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<a href="<?php echo get_listar_simulados_url(TRUE) ?>" class="btn btn-outline btn-default"><i class="fa fa-undo"></i> Sair do Simulado</a>
		</div>
		<!-- <div class="col-lg-3">
		<?php if(isset($disciplina_selecionada)) :?>
		Questões: <?php echo $atual_questoes ?> / <?php echo $max_questoes ?>
		<?php endif ?>
		</div>-->
		<div class="col-lg-4">
			<?php echo form_dropdown('disciplina_id', $disciplina_combo, $disciplina_selecionada, "id='disciplina' data-sim-id='{$simulado_id}' data-busca-query-string='{$busca_query_string}' class='form-control m-b'") ?>
		</div>
	</div>

	<div class="btn-tools">
		<?php if(isset($_GET['buscar'])): ?>
		<div class="col-lg-12">
			<?php $this->view('templates/filtros_selecionados') ?>
		</div>
		<?php endif; ?>
	</div>
</nav>

<?php if(isset($disciplina_selecionada)) : ?>
<div class="row">
	<?php if(!$pode_adicionar) : ?>
		<?php ui_alerta('Não é possível adicionar uma nova questão desta matéria.', ALERTA_INFO) ?>
	<?php endif; ?>
	
	<?php 
		if(isset($is_resultado) && $total) {
			$msg = $total > 200 ? 
				"Encontramos {$total} questões, mas exibiremos apenas as 200 primeiras. Para um melhor resultado refine sua busca." :
				"Encontramos {$total} questões com esse filtro.";
			ui_alerta($msg, ALERTA_INFO);
		}
	?>
</div>
<?php endif; ?>
<div class="questoes">
	<div id="msgbox"><?php if(isset($erro)) { ui_alerta($erro, ALERTA_ERRO); }?></div>
	<?php if(isset($questoes)) : ?>
		<div class="ibox">
			<input type="checkbox" id="selecionar-todas"> <label for="selecionar-todas">Marcar / Desmarcar Todas</label>
		</div>
		<div class="row">
			<?php if(isset($is_resultado)) : ?>
			<form method="post" id="acao-questoes" action="<?php echo get_adicionar_questoes_ao_simulado_url($simulado_id, $disciplina_selecionada) . $busca_query_string ?>">
			<?php else : ?>
			<form method="post" id="acao-questoes" action="<?php echo get_excluir_questoes_de_simulado_url($simulado_id, $disciplina_selecionada) . $busca_query_string ?>">
			<?php endif;?>
			
			<?php if($is_resultado && !$questoes) : ?>
				<div style="margin: 20px;text-align: center">Não foram encontradas questões com o filtro escolhido.</div>
			<?php else : ?>
				<?php foreach ($questoes as $questao) : ?>
					<div id="questao_<?php echo $questao['que_id'];?>" class="col-lg-12 questao">
						<div class="ibox">
							<?php $this->view('questoes/cabecalho/cabecalho', array('questao' => $questao)); ?>
							
							<div class="ibox-content">
								
								<?= $this->view('questoes/barra_superior/barra_superior', ['pagina' => PAGE_REVISAR_QUESTOES]); ?>
								
								<?= $this->view('questoes/principal/corpo'); ?>
    
    							<?= $this->view('questoes/principal/alternativas'); ?>

							</div>
							<?php //if(!$is_resultado) :?>
							<div class="ibox-footer">							
								<?= $this->view('questoes/barra_inferior/barra_inferior', array('questao' => $questao), true); ?>	
							</div>
							<?php //endif; ?>
						</div>
					</div>
				<?php endforeach;?>
			<?php endif; ?>
		</form>
		</div>
		<div class="ibox" style="padding-bottom: 10px">
			<?php echo $links; ?>
		</div>
	<?php endif; ?>
</div>

<?php if($disciplina_id) : ?>
<script>
$(function() {

	$('#selecionar-questoes').click(function(){
		dis_ids_name = 'dis_ids';
		ass_ids_name = 'ass_ids';
	});

	$('#selecionar-todas').click(function() {
		$('.questao-opcao').prop('checked', this.checked);   
	});

	$('.favorito').click(function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		var classe = $(this).find('i',0).attr('class');

		if(classe == "fa fa-star") {
			$(this).find('i',0).attr('class', 'fa fa-star-o');
		}
		else {
			$(this).find('i',0).attr('class', 'fa fa-star');
			
			//Favoritar também faz acompanhar
			var bell_class = $('#acompanhar-'+id).attr('class');
			if(bell_class == 'fa fa-bell-slash-o'){
				$('#acompanhar-'+id).attr('class', 'fa fa-bell');
			}
		}

		$.get('/questoes/questoes_xhr/marcar_desmarcar_favorito/'+id+'/<?= QUESTAO_FAVORITA ?>');
	});

	$('.acompanhar').click(function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		var classe = $(this).find('i',0).attr('class');

		if(classe == "fa fa-bell-slash-o") {
			$(this).find('i',0).attr('class', 'fa fa-bell');
		}
		else {
			$(this).find('i',0).attr('class', 'fa fa-bell-slash-o');
		}

		$.get('/questoes/questoes_xhr/marcar_desmarcar_favorito/'+id+'/<?= QUESTAO_ACOMPANHADA ?>');
	});
				
	$('.btn-edicao-rapida').click(function(e) {
		e.preventDefault();
		var questao_id = '#edicao-rapida-' + $(this).data('id');
		$(questao_id).toggle();
		
		$(questao_id).html("Carregando...");
		
		$.get('/questoes/questoes_xhr/carregar_edicao_rapida/'+$(this).data('id'), function(data) {
			$(questao_id).html(data);
		});
	});

	$('.btn-remover-formatacao').click(function(e){
		e.preventDefault();
		var id = $(this).data('id');

		$.ajax({
			url: "<?php echo get_ajax_main_remover_formatacao_alternativas_url(); ?>",
			method: "POST",
			cache: false,
			data: { id: id }
		})
		.done(function( msg ) {
			alert(msg);
		});
	});

	$('#ass_ids').select2({
       	placeholder: "Assuntos"
	});

	$('.questao-relacionada-combo').change(function() {
		$.get('/questoes/admin/xhr_atualizar_questao_relacionada/'+$(this).data('id')+'/'+$(this).val());
	});

	<?php 
	$array_inputs = ["ban_ids", "org_ids", "car_ids", "pro_anos", "esc_ids", "ara_ids", "que_dificuldades", "arf_ids", "professores_ids", "que_statuses", "neg_cad_ids", "pro_tipo_ids", "status_ids", "dis_ids", "que_tipos", "sim_ids", "sim_excluir_questoes"];
    $array_labels = ["Banca", "Orgão", "Cargo", "Ano", "Nível Escolar", "Área de Atuação", "Dificuldade", "Área de Formação", "Comentários dos Professores", "Status", "Não presente em cadernos", "Modalidades", "Status", "Disciplina", "Tipo de Questão", "Não usada nos Simulados", "Excluir questões"]; 

	foreach($array_inputs as $key => $input) :
		if($input == "dis_ids") continue;
	?>
   		$('<?php echo "#{$input}"; ?>').select2({
           	placeholder: "<?php echo $array_labels[$key]; ?>"
   		});
	<?php endforeach; ?>
	
});
</script>
<?php $this->view('questoes/barra_inferior/barra_inferior_scripts') ?>
<?php endif ?>