<?php
	$titulo = isset($simulado['sim_id']) ? 'Editar Simulado - ' . $simulado['sim_nome'] : 'Novo Simulado';
?>
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row" >
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5><?php echo $titulo; ?></h5>
				</div>
				<div class="ibox-content">
					<?php echo form_open_multipart(get_editar_simulado_url($simulado, $passo), 'id="form-simulado" class="wizard-big"'); ?>
						<input type="hidden" name="passo" value="<?php echo $passo; ?>" />
						<div class="row">
						<ul style="list-style: none !important;">
							<li title="<?= get_nome_etapa_edicao_simulado(1) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 1); ?>">Passo 1 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(2) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 2); ?>">Passo 2 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(3) ?>" class="simulado-passo-atual"><a href="#">Passo 3 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(4) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 4); ?>">Passo 4 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(5) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 5); ?>">Passo 5 de 6</a></li>
							<li title="<?= get_nome_etapa_edicao_simulado(6) ?>" class="simulado-passo"><a href="<?php echo get_editar_simulado_url($simulado, 6); ?>">Passo 6 de 6</a></li>
						</ul>
						</div>
						<br/>
						<fieldset class="simulado-box">
							<h2><?= get_nome_etapa_edicao_simulado(3) ?></h2>
							<div class="row">
								<span id="check_message"></span>
								<div class="col-lg-12">
                    				<div class="ibox float-e-margins">
                    					<div class="table-responsive">
                    					<span class="btn default totalQuestoesSelecionadas easy-pie-chart-reload">Questões Selecionadas (<?php echo $total_questoes_disciplina?>)</span>
                    						<table class="table table-striped table-hover" id="disciplina_table">
			                                    <thead>
				                                    <tr>
				                                    	<th style="width:400px">Disciplina</th>
				                                        <th>Quantidade de Questões</th>
				                                        <th style="width:200px">Questões Disponíveis</th>
				                                    </tr>
			                                    </thead>
			                                    <tbody>
			                                    	<?php foreach ($disciplinas as $disciplina) : ?>
				                                    	<tr class="odd gradeX">
					                                        <td><?php echo $disciplina['dis_nome']; ?></td>
					                                        <td class="">
																<div class="col-md-6">
																	<div class="input-group input-small">
																		<span class="input-group-addon">
																			<span class=""><?php echo form_checkbox("dis_check[]", $disciplina['dis_id'], $disciplina['selected'], "class='check_disciplinas require-one'"); ?></span>
																		</span>
																		<?php echo form_input($disciplina['dis_input'], $disciplina['valor_input'], "id='{$disciplina['dis_input']}' class='form-control valor_input'"); ?>
																	</div>
																</div>
					                                        </td>
					                                        <td class=""><span id="numero-questoes-<?php echo $disciplina['dis_id'] ?>"></span> <span id="numero-total-<?php echo $disciplina['dis_id'] ?>"><?php echo $disciplina['total']; ?></span></td>
														</tr>
													<?php endforeach; ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</fieldset>
						<br/>
						<div class="col-lg-12">
							<div class="form-group">
								<div style="float: right;">									
									<?php echo get_admin_botao_submit('Salvar', 'simulado_salvar', '', 'title="Salva o passo atual e redireciona para listagem de simulados."')?>
									<a class='btn btn-primary' title='Volta para o passo anterior sem salvar.' href="<?php echo get_editar_simulado_url($simulado, 2); ?>">Voltar</a>
									<?php echo get_admin_botao_submit('Próximo', 'simulado_passo', '', 'title="Salva o passo atual e redireciona para o próximo passo."')?>
									<a class='btn btn-white' title="Cancela as alterações que não foram salvas da etapa atual e redireciona para listagem de simulados." onclick="cancelar();" href='#'>Cancelar</a>
								</div>
							</div>
						</div>
						<div style="clear:both"></div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function cancelar(){
		var r = confirm('Deseja realmente cancelar a criação/edição do simulado?');
		if(r == true) {
			window.location.href='/questoes/admin/listar_simulados/0';
		}
	}
	
	$.validator.addMethod('require-one', function(value) {
		return $('.require-one:checked').size() > 0;
	},'Selecione pelo menos uma das opções.');

	$('.valor_input').on('change', function () {
		if($(this).val() == "" || $(this).val() == 0) {
			$(this).val(10);
			$(this).parent().find('input:checkbox').prop( "checked", false );
		}
		var total_questoes = 0;
		var ids = 
			$("#disciplina_table input:checkbox:checked").map(function(){
				var id = $(this).val();
				var input = "#dis_input_" + id;
				total_questoes += parseInt($(input).val());
				return id;
			}).get();
		var text = "Questões Selecionadas(" + total_questoes + ")";
		$(".totalQuestoesSelecionadas").text(text);
	});

	$('#disciplina_table input:checkbox:checked').each(function(index) {
		var id = $(this).val();
		atualizar_numero_questoes(id);
	});	

	var edit_dis_ids = [];

	$('.check_disciplinas').click(function() {
		if($(this).prop('checked')) {
			edit_dis_ids.push($(this).val());
			
			var id = $(this).val();
			atualizar_numero_questoes(id);	
		}
		else {
			var index = edit_dis_ids.indexOf($(this).val());
			if(index > -1 ) {
				edit_dis_ids.splice( index , 1);
			}
		}
	});

	function atualizar_numero_questoes(id) {
		$('#numero-total-'+id).hide();
		$('#numero-questoes-'+id).text("Atualizando...");
		$.get('/questoes/admin/contar_questoes_por_filtro_e_disciplina/'+id, function(data) {
			$('#numero-questoes-'+id).text(data + " de ");
			$('#numero-total-'+id).show();
		});
	}
</script>