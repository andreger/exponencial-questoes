<?php admin_cabecalho_pagina("Prioridade de Simulados") ?>

<div class="row">
    <div class="col-lg-6">
        <div class="ibox ">
            <div>{caixa_azul}</div>               

            <div class="ibox-content">
                <div class="dd" id="nestable">
                    <ol class="dd-list">
                        {simulados}
                        <li class="dd-item" data-sim_id="{sim_id}">
                            <div class="dd-handle">{sim_nome}</div>
                        </li>
                        {/simulados}
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function () {
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');

        if (window.JSON) {
            var simulados = window.JSON.stringify(list.nestable('serialize'));
            $.post('/questoes/xhr/atualizar_prioridade_simulados', {'simulados' : simulados});
        }
    };

    $('#nestable').nestable({
        group: 1
    }).on('change', updateOutput);
});
</script>   