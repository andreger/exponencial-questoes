<?php
	$prefixo = isset($ranking['ran_id']) ? 'Editar' : 'Novo'; 
	admin_cabecalho_pagina($prefixo . " Ranking <small>(Passo 1 - Selecionar Simulados)</small>"); 
?>

<?php ui_alerta($this->session->flashdata('sucesso'), ALERTA_SUCESSO); ?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<?php echo form_open(get_editar_ranking_url($id), 'id="form-banca" class="form-horizontal"'); ?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nome</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="ran_nome" value="<?php echo isset($ranking['ran_nome']) ? $ranking['ran_nome'] : ''; ?>"/>
                    	<span class="help-block m-b-none error"><?= form_error('ran_nome'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Simulados</label>
                    <div class="col-sm-10">
                       <?php echo form_multiselect('simulados_ids[]', $simulados, $simulados_selecionados, 'id="sim_ids" class="multiselect form-control m-b"'); ?>
                        <span class="help-block m-b-none error"><?= form_error('simulados_ids[]'); ?></span>
                    </div>
				</div>
				<div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Média ponderada</label>
                    <div class="col-sm-10">
                       <?php echo form_checkbox('ran_media_ponderada', 1, ($ranking['ran_media_ponderada'] == 1), 'id="ran_media_ponderada" class="m-b"'); ?>
                        <span class="help-block m-b-none error"><?= form_error('ran_media_ponderada'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <?= get_admin_botoes_navegacao_ranking($id, 1) ?>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){

		$("#sim_ids").chosen({
			no_results_text: "Nenhum resultado com:",
			placeholder_text_multiple: "Escolha os simulados",
			disable_search_threshold: 10
		});

	});
</script>