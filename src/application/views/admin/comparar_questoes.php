<?php admin_cabecalho_pagina("Comparar Questões") ?>

<?php echo get_mensagem_flash() ?>

<div class="row margin-top-10">
	<div class="col-lg-12">
		<form method="post">
			<input type="submit" class="btn btn-danger" name="ignorar" value="Ignorar Caso">
			<input type="submit" class="btn btn-primary" name="master" value="Marcar <?= $master_id ?> como Master" >
	    	<input type="submit" class="btn btn-primary" name="repetida" value="Marcar <?= $repetida_id ?> como Master">
	    	
	    	<input type="hidden" name="master_id" value="<?= $master_id ?>">
	    	<input type="hidden" name="repetida_id" value="<?= $repetida_id ?>">
	    	<label for="inativar">
	    		<input type="checkbox" id="inativar" name="inativar" checked="checkbox">
	    		Inativar questão repetida
	    	</label>
	    </form>
    </div>
</div>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">	
			
			<div class="col-md-4">	
				<?= $master  ?>

				<div class='ibox float-e-margins'>
					<div class='ibox-title'>
		                <h5>Comentários</h5>
		            </div>
		            <div class='ibox-content'>
						<?php foreach ($comentarios_professores_master as $item) : ?>
							<div class="qr_destaque qr_comentario">
								<?php $usuario = get_usuario_array($item['user_id']) ?>
								<div class="qr_usuario"><?= $usuario['nome_completo'] ?>, em <?= converter_para_ddmmyyyy($item['com_data_criacao']) ?> </div>
								<div><?= $item['com_texto'] ?></div>					
							</div>
						<?php endforeach ?>

							<?php foreach ($comentarios_alunos_master as $item) : ?>
							<div class="qr_comentario">
								<?php $usuario = get_usuario_array($item['user_id']) ?>
								<div class="qr_usuario"><?= $usuario['nome_completo'] ?>, em <?= converter_para_ddmmyyyy($item['com_data_criacao']) ?> </div>
								<div><?= $item['com_texto'] ?></div>					
							</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>

			<div class="col-md-4">
				<?= $comparacao ?>				
			</div>

			<div class="col-md-4">
				<?= $repetida ?>
				<div class='ibox float-e-margins'>
					<div class='ibox-title'>
		                <h5>Comentários</h5>		              
		            </div>
		            <div class='ibox-content'>
						<?php foreach ($comentarios_professores_repetida as $item) : ?>
							<div class="qr_destaque qr_comentario">
								<?php $usuario = get_usuario_array($item['user_id']) ?>
								<div class="qr_usuario"><?= $usuario['nome_completo'] ?>, em <?= converter_para_ddmmyyyy($item['com_data_criacao']) ?> </div>
								<div><?= $item['com_texto'] ?></div>					
							</div>
						<?php endforeach ?>

							<?php foreach ($comentarios_alunos_repetida as $item) : ?>
							<div class="qr_comentario">
								<?php $usuario = get_usuario_array($item['user_id']) ?>
								<div class="qr_usuario"><?= $usuario['nome_completo'] ?>, em <?= converter_para_ddmmyyyy($item['com_data_criacao']) ?> </div>
								<div><?= $item['com_texto'] ?></div>					
							</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
		<script>

		$('.btn-edicao-rapida').click(function(e) {
		e.preventDefault();
		var questao_id = '#edicao-rapida-' + $(this).data('id');
		$(questao_id).toggle();
		
		$(questao_id).html("Carregando...");
		
		$.get('/questoes/questoes_xhr/carregar_edicao_rapida/'+$(this).data('id'), function(data) {
			$(questao_id).html(data);
		});
		});
		
		</script>
