<?php
	admin_cabecalho_pagina("Editar Configurações"); 
	echo get_mensagem_flash();
?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<form method="post" class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-2 control-label">Percentual de Similaridade</label>
                    <div class="col-sm-10">
                    	<input id="config_percentual_similaridade" class="form-control" type="textbox" name="config_percentual_similaridade" value="<?= isset($config_percentual_similaridade) ? $config_percentual_similaridade : '' ?>" />
                    	<span class="help-block m-b-none error"><?php echo form_error('config_percentual_similaridade'); ?></span>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>

                <div class="form-group">
					<label class="col-sm-2 control-label">Buscar em Texto Base</label>
                    <div class="col-sm-10">
                    	<input id="config_base_texto" class="i-checks" type="checkbox" name="config_base_texto" value="1" <?= isset($config_base_texto) && $config_base_texto == SIM ? 'checked' : '' ?>>
                    	<span class="help-block m-b-none error"><?php echo form_error('config_base_texto'); ?></span>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>

                <div class="form-group">
					<label class="col-sm-2 control-label">Buscar em Enunciado</label>
                    <div class="col-sm-10">
                    	<input id="config_enunciado" class="i-checks" type="checkbox" name="config_enunciado" value="1" <?= isset($config_enunciado) && $config_enunciado == SIM  ? 'checked' : '' ?>>
                    	<span class="help-block m-b-none error"><?php echo form_error('config_enunciado'); ?></span>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>

                <div class="form-group">
					<label class="col-sm-2 control-label">Ignorar HTML</label>
                    <div class="col-sm-10">
                    	<input id="config_ignorar_html" class="i-checks" type="checkbox" name="config_ignorar_html" value="1" <?= isset($config_ignorar_html) && $config_ignorar_html == SIM ? 'checked' : '' ?>>
                    	<span class="help-block m-b-none error"><?php echo form_error('config_ignorar_html'); ?></span>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>

                <div class="form-group">
					<label class="col-sm-2 control-label">Somente na mesma Disciplina</label>
                    <div class="col-sm-10">
                    	<input id="config_mesma_disciplina" class="i-checks" type="checkbox" name="config_mesma_disciplina" value="1" <?= isset($config_mesma_disciplina) && $config_mesma_disciplina == SIM ? 'checked' : '' ?>>
                    	<span class="help-block m-b-none error"><?php echo form_error('config_mesma_disciplina'); ?></span>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Somente na mesma Banca</label>
                    <div class="col-sm-10">
                        <input id="config_mesma_banca" class="i-checks" type="checkbox" name="config_mesma_banca" value="1" <?= isset($config_mesma_banca) && $config_mesma_banca == SIM ? 'checked' : '' ?>>
                        <span class="help-block m-b-none error"><?php echo form_error('config_mesma_banca'); ?></span>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Disciplinas</label>
                    <div class="col-sm-10">
                        <?= form_multiselect('config_disciplinas[]', $disciplinas, $config_disciplinas, 'id="config_disciplinas" class="multiselect form-control m-b"') ?>
                        <span class="help-block m-b-none error"><?php echo form_error('config_disciplinas'); ?></span>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Anos</label>
                    <div class="col-sm-10">
                        <?= form_multiselect('config_anos[]', $anos, $config_anos, 'id="config_anos" class="multiselect form-control m-b"') ?>
                        <span class="help-block m-b-none error"><?php echo form_error('config_anos'); ?></span>
                    </div>
                </div>

                <div class="hr-line-dashed"></div>

                <div class="form-group">
					<label class="col-sm-2 control-label">Questões/Execução</label>
                    <div class="col-sm-10">
                    	<input id="config_qtde_questoes" class="form-control" type="textbox" name="config_qtde_questoes" value="<?= isset($config_qtde_questoes) ? $config_qtde_questoes : '' ?>">
                    	<span class="help-block m-b-none error"><?php echo form_error('config_qtde_questoes'); ?></span>
                    </div>
                </div>
   				
                <?php admin_botoes_salvar_cancelar(get_listar_questoes_repetidas_url()) ?>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
$(function() {
	$('.i-checks').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});

    $(".multiselect").chosen({
        disable_search_threshold: 10,
        no_results_text: "Nenhum item com:",
        placeholder_text_multiple: "Selecione alguns itens..."
    });
    
    $(".multiselect").on("change", function(event, data) {
        $(this).trigger("chosen:updated");
    });
});
</script>