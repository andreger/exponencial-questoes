<?php
	$prefixo = isset($banca['ban_id']) ? 'Editar' : 'Nova'; 
	admin_cabecalho_pagina($prefixo . " Banca"); 
?>

<?php ui_alerta($this->session->flashdata('sucesso'), ALERTA_SUCESSO); ?>

<div class="row margin-top-10">
	<div class="col-lg-12">
    	<div class="ibox float-e-margins">
    		<div class="ibox-content">
				<?php echo form_open(get_editar_banca_url($banca), 'id="form-banca" class="form-horizontal"'); ?>
				<div class="form-group">
					<label class="col-sm-2 control-label">Nome Abreviado</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="ban_nome" value="<?php echo isset($banca['ban_nome']) ? $banca['ban_nome'] : ''; ?>"/>
                    	<span class="help-block m-b-none error"><?php echo form_error('ban_nome'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
   				<div class="form-group">
					<label class="col-sm-2 control-label">Nome Completo</label>
                    <div class="col-sm-10">
                    	<input type="text" class="form-control" name="ban_nome_completo" value="<?php echo isset($banca['ban_nome_completo']) ? $banca['ban_nome_completo'] : ''; ?>"/>
                    	<span class="help-block m-b-none error"><?php echo form_error('ban_nome_completo'); ?></span>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <?php admin_botoes_salvar_cancelar(get_listar_bancas_url()) ?>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>