
<?php $this->load->view("desempenho/header"); ?>

<div class="row">
	<?php $this->load->view("desempenho/filtro_alunos"); ?>
	<?php $this->load->view("desempenho/filtro_periodo"); ?>
	<?php $this->load->view("desempenho/filtro_bancas"); ?>
	<?php $this->load->view("desempenho/filtro_disciplinas"); ?>
<?php $this->load->view("desempenho/filtro_ordem_mobile", array('pagina' => 'bancas')); ?>
</div>

<div class="row">
	<div class="col-lg-12">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Desempenho por Banca</h5>
			</div>
			<div class="ibox-content table-disciplina">
				<?php
				       $options = array('id' => 'dt_banca', 'data_table_class' => 'dt_banca');
					data_table($thead_array_bancas, $tbody_array_bancas, $tfoot_array_bancas, $options);
				?>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-6">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Questões respondidas por Banca</h5>
			</div>
			<div class="ibox-content">
				<div id="grafico-qr-banca">Não existem dados suficientes</div>
				<?php if($tem_gr_banca) : ?>
					<a id="ver-mais-qr-banca" class="btn btn-primary btn-rounded btn-block"> Ver mais</a>
				<?php endif; ?>
				<div id="modal-chart-qr-banca" class="modal fade">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Questoes respondidas por Banca</h4>
							</div>
							<div class="modal-body">
								<div id="grafico-qr-banca-modal"></div>
								<div style="padding: 30px"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-6">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Acertos por Banca</h5>
			</div>
			<div class="ibox-content">
				<div id="grafico-acertos-banca">Não existem dados suficientes</div>

				<div style="margin-top: 20px">
					<?php if($tem_gr_banca) : ?>
						<a id="ver-mais-acertos-banca" class="btn btn-primary btn-rounded btn-block"> Ver mais</a>
					<?php endif; ?>
				</div>
				<div id="modal-chart-acertos-banca" class="modal fade">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Acertos por Banca</h4>
							</div>
							<div class="modal-body">
								<div id="grafico-acertos-banca-modal"></div>
								<div style="padding: 30px"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
	const is_mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

	let config = {
			language: {
		        url: "<?= base_url('assets-admin/js/ext/dataTable-pt_BR.json'); ?>"
		    },
	        responsive: false,
// 	        scrollX: true,
	        searching: false,
	        paging: false,
	        "order": [[1, 'asc']],
// 	        ordering: !is_mobile,
	        "columnDefs": [
	        	{
	                "targets": 0,
// 	                visible: !is_mobile
	                orderable: !is_mobile
	            },
	            {
	                "targets": 1,
// 	                visible: !is_mobile
	                orderable: !is_mobile
	            },
	            {
	                "targets": 2,
 	                visible: !is_mobile,
	                orderable: !is_mobile
	            },
	            {
	                // The `data` parameter refers to the data for the cell (defined by the
	                // `data` option, which defaults to the column being worked with, in
	                // this case `data: 0`.
	                "render": function ( data, type, row ) {
	                    return data.replace('.',',')+'%';
	                },
	                "targets": 3,
	                visible: !is_mobile,
	                orderable: !is_mobile
	            },
	            {
	                "targets": 4,
 	                visible: true,
	                orderable: false
	            }
	        ]
	    };

	var dt_disciplina = $('#dt_banca').DataTable(config);

	if(is_mobile) {
		$("#ordenacao").select2();
		$(document).on("change", "#ordenacao", function() {
			order = this.value.split('-');
			dt_disciplina.order( [ order[0], order[1] ] ).draw();
		});
	}
    /*var dt_banca = $('.dt_banca').DataTable({
		language: {
	        url: "<?= base_url('assets-admin/js/ext/dataTable-pt_BR.json'); ?>"
	    },
		scrollX: true,
        responsive: true,
        "initComplete": function(settings, json) {
			$('#dt_banca_filter').empty();
        	$('#dt_banca_filter').css('text-align', 'right');
        	$('#dt_banca_filter').append($('#filtros-por-banca-div'));
        	$('#filtros-por-banca-div').show();

        	$('#fb-por-banca, #fb-por-disciplina').on('change', function () {
		    	$.post('/questoes/xhr/listar_desempenho_por_banca', $('#form-fb').serialize(), function (data) {
					dt_banca.clear();

		    		$.each(data, function(i) {
						dt_banca.row.add(data[i]);
		    		});

		    		dt_banca.draw();

		    	}, 'json');
			} );

			$('#fb-por-banca').select2({
	           	placeholder: "Selecione uma Banca",
       		});
       		$('#fb-por-disciplina').select2({
	           	placeholder: "Selecione uma disciplina",
	           	allowClear: true
       		});

  		}
    });*/

	<?php if($tem_gr_banca) : ?>

		c3.generate({
			bindto: '#grafico-acertos-banca',
			data: {
				x: 'x',
				columns: <?= $acertos_banca_10 ?>,
				type: 'bar'
			},
			legend: {
				show: false
			},
			axis: {
				x: {
					type: 'category', // this needed to load string x value
					tick: {
						rotate: 90,
						multiline: false
					},
				}
			}
		});

		c3.generate({
			bindto: '#grafico-qr-banca',
			data: {
			columns: <?= $qr_banca ?>,
			type: 'pie'
			},
			legend: {
				show: false
			}
		});

	<?php endif; ?>

	$('#ver-mais-qr-banca').off('click');
	$('#ver-mais-qr-banca').on('click', function() {
		$('#modal-chart-qr-banca').modal('show');
		$('#modal-chart-qr-banca').css("display", "block");

		c3.generate({
		    bindto: '#grafico-qr-banca-modal',
		    data: {
		      columns: <?= $qr_banca ?>,
		      type: 'pie'
		    },
		    legend: {
	        	show: false
	    	}
		});
	});

	$('#ver-mais-acertos-banca').off('click');
	$('#ver-mais-acertos-banca').on('click', function() {
		$('#modal-chart-acertos-banca').modal('show');
		$('#modal-chart-acertos-banca').css("display", "block");

		c3.generate({
		    bindto: '#grafico-acertos-banca-modal',
		    data: {
			    x: 'x',
			    columns: <?= $acertos_banca_30 ?>,
			    type: 'bar'
		    },
		    legend: {
	        	show: false,
	    	},
	    	axis: {
		        x: {
		            type: 'category', // this needed to load string x value
		            tick: {
		                rotate: 90,
		                multiline: false
		            },
		        }
		    }
		});
	});

});
</script>