<?php $this->load->view("desempenho/header"); ?>

<div class="row">
	<?php $this->load->view("desempenho/filtro_alunos"); ?>
	<?php $this->load->view("desempenho/filtro_periodo"); ?>
	<?php $this->load->view("desempenho/filtro_titulo"); ?>
	<?php $this->load->view("desempenho/filtro_ordem_mobile", array('pagina' => 'simulados')); ?>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Desempenho por Simulado</h5>
			</div>
			<div class="ibox-content table-disciplina">
				<?php
				    $options = array('id' => 'dt_simulado', 'data_table_class' => 'dt_simulado');
					data_table($thead_array_simulados, $tbody_array_simulados, $tfoot_array_simulados, $options);
				?>
			</div>
				<div class="flot-chart">
					<div class="flot-chart-content" id="grafico-simulado-10">Não existem dados suficientes</div>
				</div>
				<?php if($tem_gr_simulado) : ?>
					<a id="ver-mais-simulado" class="btn btn-primary btn-rounded btn-block"> Ver mais</a>
				<?php endif; ?>
				<div id="modal-chart-simulado" class="modal fade">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Simulados Exponencial</h4>
							</div>
							<div class="modal-body">
								<div class="flot-chart-content" id="grafico-simulado-30"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<script>
	$(document).ready(function() {
		const is_mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

		let config = {
				language: {
			        url: "<?= base_url('assets-admin/js/ext/dataTable-pt_BR.json'); ?>"
			    },
		        responsive: false,
//	 	        scrollX: true,
		        searching: false,
		        paging: false,
		        "order": [[0, 'asc']],
//	 	        ordering: !is_mobile,
		        "columnDefs": [
		        	{
		                "targets": 0,
//	 	                visible: !is_mobile
		                orderable: !is_mobile
		            },
		            {
		                "targets": 1,
//	 	                visible: !is_mobile
		                orderable: !is_mobile
		            },
		            {
		                "targets": 2,
// 	 	                visible: !is_mobile,
		                orderable: !is_mobile
		            },
		            {
		                // The `data` parameter refers to the data for the cell (defined by the
		                // `data` option, which defaults to the column being worked with, in
		                // this case `data: 0`.

		                "targets": 3,
// 		                visible: !is_mobile,
		                orderable: !is_mobile
		            },
		            {
		                "targets": 4,
	 	                visible: true,
		                orderable: false
		            }
		        ]
		    };

		var dt_disciplina = $('#dt_simulado').DataTable(config);

		if(is_mobile) {
			$("#ordenacao").select2();
			$(document).on("change", "#ordenacao", function() {
				order = this.value.split('-');
				dt_disciplina.order( [ order[0], order[1] ] ).draw();
			});
		}

	<?php if($tem_gr_simulado) : ?>

		var gr_simulado_notas = <?php echo $gr_simulado_notas; ?>;
		var gr_simulado_10 = <?php echo $gr_simulado_10; ?>;
		var gr_simulado_30 = <?php echo $gr_simulado_30; ?>;

		c3.generate({
			bindto: '#grafico-simulado-10',
			axis: {
				x: {
					type: 'category', // this needed to load string x value
					tick: {
						rotate: 90,
						multiline: false
					},
					height: 150
				},
				y: {
					tick: {
						format: function (d) { return d+'%'; }
					},
					max: 100,
					min: 0,
					padding: {
						top: 0,
						bottom: 0
					}
				}
			},
			data: {
				x: 'x',
				columns: gr_simulado_10,
				type: 'bar'
			},
			bar: {
				width: 10
			},
			legend: {
				show: false
			},
			tooltip: {
				grouped: false,
				format: {
					value: function (name, ratio, id, index) { return gr_simulado_notas[0][index]; }
				}

			}
		});

	<?php endif; ?>

	$('#ver-mais-simulado').off('click');
	$('#ver-mais-simulado').on('click', function() {
		$('#modal-chart-simulado').modal('show');
		$('#modal-chart-simulado').css("display", "block");
	});

	$('#modal-chart-simulado').on('shown.bs.modal', function(e){

		c3.generate({
			bindto: '#grafico-simulado-30',
			axis: {
				x: {
					type: 'category', // this needed to load string x value
					tick: {
						rotate: 90,
						multiline: false
					}
				},
				y: {
					tick: {
						format: function (d) { return d+'%'; }
					},
					max: 100,
					min: 0,
					padding: {
						top: 0,
						bottom: 0
					}
				}
			},
			data: {
				x: 'x',
				columns: gr_simulado_30,
				type: 'bar'
			},
			bar: {
				width: 10
			},
			legend: {
				show: false
			},
			tooltip: {
				grouped: false,
				format: {
					value: function (name, ratio, id, index) { return gr_simulado_notas[0][index]; }
				}

			}
		});

	});

});
</script>