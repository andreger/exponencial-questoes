<div class="col-lg-3">
	<div class="ibox">
		<form id="filtro" method="get">
			Período: <input type="text" id="daterange" name="daterange" class="form-control m-b">
			<input type="hidden" name="aluno_id" id="aluno" value="<?= isset($aluno_selecionado) ? $aluno_selecionado : null ?>">
			<input type="hidden" name="filtro_aluno" id="filtro_aluno">
			<input type="hidden" name="inicio" id="inicio" value="<?= $inicio ?>">
			<input type="hidden" name="fim" id="fim" value="<?= $fim ?>">
			<input type="hidden" name="bancas_ids" id="bancas_ids" value="<?= implode(',', $bancas) ?>">
			<input type="hidden" name="disciplinas_ids" id="disciplinas_ids" value="<?= implode(',', $disciplinas) ?>">
			<input type="hidden" name="assuntos_ids" id="assuntos_ids" value="<?= implode(',', $assuntos) ?>">
			<input type="hidden" name="sim_nome" id="sim_nome" value="<?= $simulado_nome; ?>">
			<input type="hidden" name="visualizar" id="filtro-submit">
			<input type="hidden" name="minha_visao" id="minha-visao-submit">
		</form>
	</div>
</div>