<?php $this->load->view("desempenho/header"); ?>

<div class="row">
	<?php $this->load->view("desempenho/filtro_alunos"); ?>
	<?php $this->load->view("desempenho/filtro_periodo"); ?>
	<?php $this->load->view("desempenho/filtro_disciplinas"); ?>
	<?php $this->load->view("desempenho/filtro_assuntos"); ?>
	<?php $this->load->view("desempenho/filtro_ordem_mobile", array('pagina' => 'assuntos')); ?>
</div>

<div class="row">
	<div class="col-lg-12">

		<div class="ibox">
			<div class="ibox-title">
				<h5>Desempenho por Assunto</h5>
			</div>
			<div class="ibox-content table-disciplina">
				<?php
				    $options = array('id' => 'dt_assunto', 'data_table_class' => 'dt_assunto');
					data_table($thead_array_assuntos, $tbody_array_assuntos, $tfoot_array_assuntos, $options);
				?>
			</div>
		</div>

	</div>
</div>
<script>
$(document).ready(function() {
	const is_mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

	let config = {
			language: {
		        url: "<?= base_url('assets-admin/js/ext/dataTable-pt_BR.json'); ?>"
		    },
	        responsive: false,
// 	        scrollX: true,
	        searching: false,
	        paging: false,
	        "order": [[1, 'asc']],
// 	        ordering: !is_mobile,
	        "columnDefs": [
	        	{
	                "targets": 0,
// 	                visible: !is_mobile
	                orderable: !is_mobile
	            },
	            {
	                "targets": 1,
// 	                visible: !is_mobile
	                orderable: !is_mobile
	            },
	            {
	                "targets": 2,
 	                visible: !is_mobile,
	                orderable: !is_mobile
	            },
	            {
	                // The `data` parameter refers to the data for the cell (defined by the
	                // `data` option, which defaults to the column being worked with, in
	                // this case `data: 0`.
	                "render": function ( data, type, row ) {
	                    return data.replace('.',',')+'%';
	                },
	                "targets": 3,
	                visible: !is_mobile,
	                orderable: !is_mobile
	            },
	            {
	                "targets": 4,
 	                visible: true,
	                orderable: false
	            }
	        ]
	    };

	var dt_disciplina = $('#dt_assunto').DataTable(config);

	if(is_mobile) {
		$("#ordenacao").select2();
		$(document).on("change", "#ordenacao", function() {
			order = this.value.split('-');
			dt_disciplina.order( [ order[0], order[1] ] ).draw();
		});
	}
});
</script>