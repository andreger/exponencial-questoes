<div class="col-lg-3 visible-xs visible-sm">
	<form>
		<div class="ibox">
        Ordenação
        <select id="ordenacao" class="form-control input-sm" style='margin-bottom: 5px' placeholder="Escolher Ordenação da tabela">
	<?php if($pagina != 'simulados'): ?>

			<option value="0-asc" <?= $pagina == 'disciplinas' ? 'selected' : ''; ?>>Disciplinas A-Z</option>
			<option value="0-desc">Disciplinas Z-A</option>

		<?php if($pagina == 'disciplinas'): ?>
			<option value="1-desc">Mais Questões Resolvidas</option>
			<option value="1-asc">Menos Questões Resolvidas</option>
			<option value="2-desc">Mais Acertos</option>
			<option value="2-asc">Menos Acertos</option>
			<option value="3-desc">Mais Erros</option>
			<option value="3-asc">Menos Erros</option>
			<option value="4-desc">Maior % de Acerto</option>
			<option value="4-asc">Menor % de Acerto</option>

		<?php else: ?>
			<?php if($pagina == 'bancas'): ?>
			<option value="1-asc" selected>Banca A-Z</option>
			<option value="1-desc">Banca Z-A</option>

			<?php elseif($pagina == 'assuntos'): ?>
			<option value="1-asc" selected>Assuntos A-Z</option>
			<option value="1-desc">Assuntos Z-A</option>
			<?php endif; ?>
			<option value="2-desc">Mais Questões Resolvidas</option>
			<option value="2-asc">Menos Questões Resolvidas</option>
			<option value="3-desc">Maior % de Acerto</option>
			<option value="3-asc">Menor % de Acerto</option>

		<?php endif; ?>
	<?php else: ?>
			<option value="0-asc" <?= $pagina == 'disciplinas' ? 'selected' : ''; ?>>Nome A-Z</option>
			<option value="0-desc">Nome Z-A</option>
			<option value="2-desc">Mais Recente</option>
			<option value="1-desc">Maior Nota</option>
			<option value="1-asc">Menor Nota</option>
			<option value="3-desc">Melhor Ranking</option>
			<option value="3-asc">Pior Ranking</option>
	<?php endif; ?>
        </select>
    </div>
	</form>
</div>