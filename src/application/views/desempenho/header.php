<?php
$this->view('desempenho/estilo.html.php');
$this->view('desempenho/scripts.html.php');
?>

<div class="row text-center">
	<?php admin_cabecalho_pagina("Desempenho de Questões para Concurso"); ?>
</div>

<div class="row text-center desempenho-submenu">
	Por: <a href="/questoes/desempenho/disciplinas<?= $query_string ?>" class="<?= get_desempenho_ativo_estilo('disciplinas', $desempenho_menu) ?>">Disciplina</a> | 
	<a href="/questoes/desempenho/assuntos<?= $query_string ?>" class="<?= get_desempenho_ativo_estilo('assuntos', $desempenho_menu) ?>">Assunto</a> | 
	<a href="/questoes/desempenho/bancas<?= $query_string ?>" class="<?= get_desempenho_ativo_estilo('bancas', $desempenho_menu) ?>">Banca</a> | 
	<a href="/questoes/desempenho/simulados<?= $query_string ?>" class="<?= get_desempenho_ativo_estilo('simulados', $desempenho_menu) ?>">Simulado</a>
</div>
