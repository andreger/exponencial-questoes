<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, CONSULTOR))) : ?>
<div class="row">
	<div class="col-lg-12">
		<?php if($aluno_selecionado) : ?>
		<?php ui_alerta('<b>ATENÇÃO</b>: Você está visualizando os dados de um aluno.', ALERTA_INFO); ?>
		<?php endif; ?>
	</div>
</div>

<div class="col-lg-12">
	<div class="ibox">
		<div class="ibox-content">
			<form method="post">
			
				<div id="select-alunos" class="col-lg-3">
					<?= form_dropdown('aluno_id', $alunos, $aluno_selecionado ?: null, "id='aluno_id' class='form-control m-b'"); ?>
				</div>
				<div class="col-lg-4">
					<label class="col-md-5 checkbox-inline i-checks"><?= form_checkbox('filtro_aluno[]', ALUNO_PODIO, in_array(ALUNO_PODIO, $filtro_aluno), 'id="check_podio" class="check-aluno"');?> Coaching Individual </label>
					<label class="col-md-5 checkbox-inline i-checks"><?= form_checkbox('filtro_aluno[]', ALUNO_COACHING, in_array(ALUNO_COACHING, $filtro_aluno), 'id="check_coaching" class="check-aluno"');?> Turma de Coaching </label>
				</div>
				<div class="col-lg-5">
					<input type="hidden" id="periodo-inicio" value="<?= $inicio ?: '' ?>">
					<input type="hidden" id="periodo-fim" value="<?= $fim ?: '' ?>">
					<input type="button" name="visualizar" value="Visualizar tela como aluno" class="btn btn-primary" id="filtro-submit-btn">
					<input type="button" name="minha_visao" value="Minha visão" class="btn btn-outline btn-default" id="minha-visao-submit-btn">
				</div>
			</form>
			<div style="clear: both"></div>
		</div>
	</div>
</div>
<?php endif; ?>