<?php $this->load->view("desempenho/header"); ?>

<div class="row">
	<?php $this->load->view("desempenho/filtro_alunos"); ?>
	<?php $this->load->view("desempenho/filtro_periodo"); ?>
	<?php $this->load->view("desempenho/filtro_bancas"); ?>
	<?php $this->load->view("desempenho/filtro_disciplinas"); ?>
	<?php $this->load->view("desempenho/filtro_ordem_mobile", array('pagina' => 'disciplinas')); ?>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Desempenho por Disciplina</h5>
			</div>
			<div class="ibox-content table-disciplina" id="desempenho-por-disciplina">
				<?php
				if($tbody_array_disciplina) {
				    $options = array('id' => 'dt_disciplina', 'data_table_class' => 'dt_disciplina');
					data_table($thead_array_disciplina, $tbody_array_disciplina, $tfoot_array_disciplina, $options);
				}
				else {
				    echo "<div style='margin: 15px 15px 30px'>Não existem dados suficientes</div>";
				}
				?>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-6">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Questões respondidas por Disciplina</h5>
			</div>
			<div class="ibox-content">
				<div id="grafico-qr-disciplina">Não existem dados suficientes</div>
				<?php if($tem_gr_disciplina): ?>
					<a id="ver-mais-qr-disciplina" class="btn btn-primary btn-rounded btn-block"> Ver mais</a>
				<?php endif; ?>
				<div id="modal-chart-qr-disciplina" class="modal fade">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Questoes respondidas por Disciplina</h4>
							</div>
							<div class="modal-body">
								<div id="grafico-qr-disciplina-modal"></div>
								<div style="padding: 30px"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-6">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Histórico de Questões</h5>
			</div>
			<div class="ibox-content">
				<div class="flot-chart">
					<div class="flot-chart-content" id="grafico-disciplina-10">Não existem dados suficientes</div>
				</div>
				<?php if($tem_gr_hist_disciplina) : ?>
					<a id="ver-mais-disciplina" class="btn btn-primary btn-rounded btn-block"> Ver mais</a>
				<?php endif; ?>
				<div id="modal-chart-disciplina" class="modal fade">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Histórico de Questões</h4>
							</div>
							<div class="modal-body">
								<div class="flot-chart-content" id="grafico-disciplina-30"></div>
								<div style="padding: 30px"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {

	const is_mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

	let config = {
			language: {
		        url: "<?= base_url('assets-admin/js/ext/dataTable-pt_BR.json'); ?>"
		    },
	        responsive: false,
// 	        scrollX: true,
	        searching: false,
	        paging: false,
// 	        ordering: !is_mobile,
	        "columnDefs": [
	        	{
	                "targets": 0,
// 	                visible: !is_mobile
	                orderable: !is_mobile
	            },
	            {
	                "targets": 1,
// 	                visible: !is_mobile
	                orderable: !is_mobile
	            },
	            {
	                "targets": 2,
// 	                visible: !is_mobile
	                orderable: !is_mobile
	            },
	        	{
	                "targets": 3,
// 	                visible: !is_mobile
	                orderable: !is_mobile
	            },
	            {
	                // The `data` parameter refers to the data for the cell (defined by the
	                // `data` option, which defaults to the column being worked with, in
	                // this case `data: 0`.
	                "render": function ( data, type, row ) {
	                    return data.replace('.',',')+'%';
	                },
	                "targets": 4,
	                visible: !is_mobile,
	                orderable: !is_mobile
	            },
	        ]
	    };

	var dt_disciplina = $('#dt_disciplina').DataTable(config);

	if(is_mobile) {
		$("#ordenacao").select2();
		$(document).on("change", "#ordenacao", function() {
			order = this.value.split('-');
			dt_disciplina.order( [ order[0], order[1] ] ).draw();
		});
	}

// 	var dt_disciplina = $('.dt_disciplina').DataTable({
// 		language: {
//	        url: "<?= base_url('assets-admin/js/ext/dataTable-pt_BR.json'); ?>"
// 	    },
//         responsive: true,
//         scrollX: true,
//         searching: false,
//         paging: false,
//         pageLength: 25,
//         "initComplete": function(settings, json) {
//          	$('#dt_disciplina_filter').empty();
//         	$('#dt_disciplina_filter').css('text-align', 'right');
//         	$('#dt_disciplina_filter').append($('#filtros-por-disciplina-div'));
//         	$('#filtros-por-disciplina-div').show();

//         	$('#fd-por-banca, #fd-por-disciplina').on('change', function () {
// 		    	$.post('/questoes/xhr/listar_desempenho_por_disciplina', $('#form-fd').serialize(), function (data) {

// 					dt_disciplina.clear();

// 		    		$.each(data, function(i) {
// 						dt_disciplina.row.add(data[i]);
// 		    		});

// 		    		dt_disciplina.draw();

// 		    		var qtde = dt_disciplina.column(1).data().length;
// 		    		var resolvidas = qtde == 0 ? 0 : dt_disciplina.column(1).data().reduce(function(a,b) {return parseInt(a, 10)+parseInt(b,10)});
// 		    		var acertos = qtde == 0 ? 0 : dt_disciplina.column(2).data().reduce(function(a,b) {return parseInt(a, 10)+parseInt(b,10)});
// 		    		var erros = qtde == 0 ? 0 : dt_disciplina.column(3).data().reduce(function(a,b) {return parseInt(a, 10)+parseInt(b,10)});
// 					var aproveitamento = qtde == 0 ? 0 : dt_disciplina.column(4).data().reduce(function(a,b) {return parseFloat(a)+parseFloat(b)});
// 					var media = qtde == 0 ? "0" : (aproveitamento/qtde).toFixed(1);

// 					$(dt_disciplina.column(1).footer()).html(resolvidas);
// 					$(dt_disciplina.column(2).footer()).html(acertos);
// 					$(dt_disciplina.column(3).footer()).html(erros);
// 					$(dt_disciplina.column(4).footer()).html(media.replace('.',',')+'%');

// 		    	}, 'json');
// 			} );

// 			$('#fd-por-banca').select2({
// 	           	placeholder: "Selecione uma banca",
//        		});
//        		$('#fd-por-disciplina').select2({
// 	           	placeholder: "Selecione uma disciplina",
// 	           	allowClear: true
//        		});

//   		},
//   		"columnDefs": [
//             {
//                 // The `data` parameter refers to the data for the cell (defined by the
//                 // `data` option, which defaults to the column being worked with, in
//                 // this case `data: 0`.
//                 "render": function ( data, type, row ) {
//                     return data.replace('.',',')+'%';
//                 },
//                 "targets": 4
//             },
//         ]
//     });

	<?php if($tem_gr_disciplina) : ?>

		c3.generate({
			bindto: '#grafico-qr-disciplina',
			data: {
			columns: <?= $qr_disciplina ?>,
			type: 'pie'
			},
			legend: {
				show: false
			}
		});

	<?php endif; ?>

	<?php if($tem_gr_simulado) : ?>

		var gr_simulado_notas = <?php echo $gr_simulado_notas; ?>;
		var gr_simulado_10 = <?php echo $gr_simulado_10; ?>;
		var gr_simulado_30 = <?php echo $gr_simulado_30; ?>;

		c3.generate({
			bindto: '#grafico-simulado-10',
			axis: {
				x: {
					type: 'category', // this needed to load string x value
					tick: {
						rotate: 90,
						multiline: false
					},
					height: 150
				},
				y: {
					tick: {
						format: function (d) { return d+'%'; }
					},
					max: 100,
					min: 0,
					padding: {
						top: 0,
						bottom: 0
					}
				}
			},
			data: {
				x: 'x',
				columns: gr_simulado_10,
				type: 'bar'
			},
			bar: {
				width: 10
			},
			legend: {
				show: false
			},
			tooltip: {
				grouped: false,
				format: {
					value: function (name, ratio, id, index) { return gr_simulado_notas[0][index]; }
				}

			}
		});

	<?php endif; ?>

	<?php if($tem_gr_hist_disciplina) : ?>

		var gr_disciplina_notas = <?php echo $gr_disciplina_notas; ?>;
		var gr_disciplina_10 = <?php echo $gr_disciplina_10; ?>;
		var gr_disciplina_30 = <?php echo $gr_disciplina_30; ?>;

		c3.generate({
			bindto: '#grafico-disciplina-10',
			axis: {
				x: {
					type: 'category', // this needed to load string x value
					tick: {
						rotate: 90,
						multiline: false,
					},
					height: 150
				},
				y: {
					tick: {
						format: function (d) { return d+'%'; }
					},
					max: 100,
					min: 0,
					padding: {
						top: 0,
						bottom: 0
					}
				}
			},
			data: {
				x: 'x',
				columns: gr_disciplina_10,
				type: 'bar'
			},
			bar: {
				width: 10
			},
			legend: {
				show: false
			},
			tooltip: {
				grouped: false,
				format: {
					value: function (name, ratio, id, index) { return gr_disciplina_notas[0][index]; }
				}

			}
		});

	<?php endif; ?>

	$('#ver-mais-disciplina').off('click');

	$('#ver-mais-disciplina').on('click', function() {
		$('#modal-chart-disciplina').modal('show');
	});

	$('#modal-chart-disciplina').on('shown.bs.modal', function (e) {
		c3.generate({
			bindto: '#grafico-disciplina-30',
			axis: {
				x: {
					type: 'category', // this needed to load string x value
					tick: {
						rotate: 90,
						multiline: false
					}
				},
				y: {
					tick: {
						format: function (d) { return d+'%'; }
					},
					max: 100,
					min: 0,
					padding: {
						top: 0,
						bottom: 0
					}
				}
			},
			data: {
				x: 'x',
				columns: gr_disciplina_30,
				type: 'bar'
			},
			bar: {
				width: 10
			},
			legend: {
				show: false
			},
			tooltip: {
				grouped: false,
				format: {
					value: function (name, ratio, id, index) { return gr_disciplina_notas[0][index]; }
				}

			}
		});
	});

	$('#ver-mais-simulado').off('click');
	$('#ver-mais-simulado').on('click', function() {
		$('#modal-chart-simulado').modal('show');
		$('#modal-chart-simulado').css("display", "block");
	});

	$('#modal-chart-simulado').on('shown.bs.modal', function(e){

		c3.generate({
			bindto: '#grafico-simulado-30',
			axis: {
				x: {
					type: 'category', // this needed to load string x value
					tick: {
						rotate: 90,
						multiline: false
					}
				},
				y: {
					tick: {
						format: function (d) { return d+'%'; }
					},
					max: 100,
					min: 0,
					padding: {
						top: 0,
						bottom: 0
					}
				}
			},
			data: {
				x: 'x',
				columns: gr_simulado_30,
				type: 'bar'
			},
			bar: {
				width: 10
			},
			legend: {
				show: false
			},
			tooltip: {
				grouped: false,
				format: {
					value: function (name, ratio, id, index) { return gr_simulado_notas[0][index]; }
				}

			}
		});

	});

	$('#ver-mais-qr-disciplina').off('click');
	$('#ver-mais-qr-disciplina').on('click', function() {
		$('#modal-chart-qr-disciplina').modal('show');
		$('#modal-chart-qr-disciplina').css("display", "block");

		c3.generate({
		    bindto: '#grafico-qr-disciplina-modal',
		    data: {
		      columns: <?= $qr_disciplina ?>,
		      type: 'pie'
		    },
		    legend: {
	        	show: false
	    	}
		});
	});

});
</script>