<!-- build:js /questoes/assets-admin/js/pagina/desempenho/sq-desempenho.scripts.min.js -->
<script src='/questoes/assets-admin/js/plugins/dataTables/jquery.dataTables.js'></script>
<script src='/questoes/assets-admin/js/plugins/dataTables/dataTables.bootstrap.js'></script>
<script src='/questoes/assets-admin/js/plugins/dataTables/dataTables.responsive.js'></script>
<script src='/questoes/assets-admin/js/plugins/dataTables/dataTables.tableTools.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/d3/d3.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/c3/c3.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/daterangepicker2/moment.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/daterangepicker2/daterangepicker.js'></script>
<script src='/questoes/assets-admin/js/plugins/basictable/jquery.basictable.min.js'></script>
<!-- endbuild -->