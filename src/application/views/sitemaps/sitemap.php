<?php
header('Content-type: text/xml');
header('Pragma: public');
header('Cache-control: private');
header('Expires: -1');
?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<?php foreach ($links as $link) :?>
   <url>
      <loc><?= $link['loc'] ?></loc>
      <changefreq><?= $link['changefreq'] ?></changefreq>
      <priority><?= $link['priority'] ?></priority>
   </url>
   <?php endforeach; ?>
</urlset>