<?php 
get_header();
header('Content-Type: text/html; charset=utf-8');
?>

<div class="container-fluid pt-1 pt-md-5">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
        <h1>Relatório de questões comentadas</h1>
    </div>
</div>  

<div class="container">	
	<form action="<?php echo base_url("relatorios/questoes_comentadas") ?>" id="form-relatorio" method="post">
		<div class="row">
	  	<?php echo form_dropdown('mes-ano', $combo_mes_ano, str_replace('/','-',$mes_ano), 'class="form-control col-2 ml-2 mr-2" id="mes-ano"') ?> até 
	  	<?php echo form_dropdown('mes-ano-f', $combo_mes_ano, str_replace('/','-',$mes_ano_f), 'class="form-control col-2 ml-2" id="mes-ano-f"') ?>
	  	
	  	<?php if(tem_acesso(array(ADMINISTRADOR))) : ?>
	  	<?php echo form_dropdown('professor_id', $combo_professores, $professor_id, 'class="form-control col-2 ml-2" id="professor_id"') ?>
	  	<?php endif; ?>

	  	<input type="submit" name="submit" class="btn u-btn-blue ml-2" value="Filtrar">
		<input type="submit" name="exportar-excel" class="btn u-btn-blue ml-2" value="Exportar XLS">

		<?php if(tem_acesso(array(ADMINISTRADOR))) : ?>
		<input type="submit" name="reprocessar" class="btn u-btn-blue ml-2" value="Reprocessar Relatório">
		</div>	
		<?php endif ?>		
		<div class="mt-3">
			Última atualização: <?= $ultima_atualizacao ?>
		</div>			
	</form>

	<div><strong><?= $num_participantes ?> participantes do SQ</strong></div>
	</div>

	<?php echo $html_tabela;?>
</div>
</div>
<style>
.ui-datepicker-calendar {
    display: none;
    }
</style>

<?php get_footer() ?>