<?php get_header();
header('Content-Type: text/html; charset=utf-8');
?>
<div class="container-fluid pt-5">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
        <h1>Relatório de questões comentadas resumo</h1>
    </div>
</div>   
<div class="container pt-1 pt-3 pb-5">
	
	<form action="<?php echo base_url("relatorios/questoes_comentadas") ?>" id="form-relatorio" method="post">
		<div class="row">
		<?php echo form_hidden('professor_id', $professor_id) ?>
		<div class="col-3 col-lg-2">
	  	<?php echo form_dropdown('mes-ano', $combo_mes_ano, str_replace('/','-',$mes_ano), 'class="form-control date-picker" id="mes-ano"') ?>
	  	</div> até 
	  	<div class="col-4 col-lg-3">
	  	<?php echo form_dropdown('mes-ano-f', $combo_mes_ano, str_replace('/','-',$mes_ano_f), 'class="form-control date-picker" id="mes-ano-f"') ?>
	  	</div>
	  	<div class="col-4 col-lg-3">
	  	<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) : ?>
	  	<?php echo form_dropdown('professor_id', $combo_professores, $professor_id, 'class="form-control date-picker" id="professor_id"') ?>
	  	<?php endif; ?>
	    </div>
	    <div class="mt-2 mt-lg-0 col-12 col-lg-3">
	  	<input type="submit" name="submit" class="btn u-btn-blue" value="Filtrar">
		<input type="submit" name="exportar-excel" class="ml-2 btn u-btn-blue" value="Exportar XLS">
		</div>
		<div class="mt-3 ml-2">
			Última atualização: <?= $ultima_atualizacao ?>
		</div>
		</div>		
	</form>

	<div><strong><?= $num_participantes ?> participantes do SQ</strong></div>	
</div>
<div class="container-fluid"><?php echo $html_tabela;?></div>

<?php get_footer() ?>