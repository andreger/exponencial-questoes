<div class="mt-4">
	<div class="font-pop"><?php echo $professor['nome_completo'] ?></div>
</div>

<table border="0" cellspacing="0" cellpadding="0" class="mt-3 table table-bordered table-striped table-hover">
	<thead>
		<tr class="font-10 bg-blue text-white line-height-1-5">
			<th>Posição</th>
			<th>Mês</th>
			<th>Receita Mês&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			<th>Pagseguro</th>
			<th>Imposto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			<th>Cancelamento</th>
			<th>Receita Liquida Mês</th>
			<th>Cotas Professores</th>
			<th>Qtde total comentadas</th>
			<?php if(!is_null($professor)) :?>								
			<th class='relatorio-destaque'>Qtde comentadas pelo professor</th>
			<th>Qtd-p&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			<th>R$ prof mês corrente</th>
			<th>R$ acumulado não pago (meses anteriores)</th>
			<th>Valor acumulado p/ próximo mês</th>
			<th>Valor a receber</th>
			<?php endif; ?>
		</tr>
	</thead>
	
	<tbody>
	<?php if($questoes_comentadas):?>
		<?php foreach ($questoes_comentadas as $dados) : ?>
		<?php
			$valor_acumulado = get_valor_acumulado_prof($dados['user_id'], $dados['mes_ano']);
			$percentual_sem_format = get_percentual_sem_format($dados['qcp_qtde_comentada'], $total_coments[$dados['mes']][$dados['ano']]);
			$pg_professor_mes = calc_pg_professor_mes($totais_pagseguro[$dados['mes']][$dados['ano']], $percentual_sem_format);
			$min_pagamento = get_minimo_valor_acumulado_por_data($dados['ano'] . " - " . ($dados['mes'] < 10 ? "0" . $dados['mes'] : $dados['mes']) - "-01", VMA_SQ);

			if(!is_administrador() && is_professor()) {
				if($dados['mes'] == date('m') && $dados['ano'] == date('y')) continue; 
			}
			?>
				<tr>
					<td><?= $dados['posicao'] ?>
					<td><?php echo $dados['mes_ano'] ?></td>
					<td>R$ <?php echo number_format($totais_pagseguro[$dados['mes']][$dados['ano']]['receita'], 2, ',', '.') ?></td>
					<td>R$ <?php echo number_format($totais_pagseguro[$dados['mes']][$dados['ano']]['pagseguro'], 2, ',', '.') ?></td>
					<td>R$ <?php echo number_format($totais_pagseguro[$dados['mes']][$dados['ano']]['imposto'], 2, ',', '.') ?></td>
					<td>R$ <?php echo number_format($totais_pagseguro[$dados['mes']][$dados['ano']]['cancelamento'], 2, ',', '.') ?></td>
					<td>R$ <?php echo number_format($totais_pagseguro[$dados['mes']][$dados['ano']]['receita_liquida'], 2, ',', '.') ?></td>
					<td>50%</td>
					<td><?php echo numero_inteiro($total_coments[$dados['mes']][$dados['ano']]) ?></td>
					
					<?php if(!is_null($professor)) :?>
					<td class='relatorio-destaque'><?php echo numero_inteiro($dados['qcp_qtde_comentada']) ?></td>
					<td><?= get_percentual($dados['qcp_qtde_comentada'], $total_coments[$dados['mes']][$dados['ano']])?> %</td>
					<td><?= $pg_professor_mes ?></td>
					<td><?= number_format($valor_acumulado, 2, ',', '.') ?></td>
					<td><?= get_valor_acumulado_prox_mes($pg_professor_mes, $valor_acumulado, $min_pagamento) ?></td>
					<td><?= get_valor_a_receber($pg_professor_mes, $valor_acumulado, $min_pagamento) ?></td>
					<?php endif; ?>
				</tr>
		<?php endforeach; ?>
	<?php endif;?>
	</tbody>
</table>