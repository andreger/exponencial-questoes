<table border="0" cellspacing="0" cellpadding="0" class="table table-bordered table-striped table-hover mt-3">
	<thead>
		<tr class="font-10 bg-blue text-white line-height-1-5">
			<th></th>
			<th>Professor</th>
			<th>Mês</th>
			<th>Receita Mês&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
			<th>Pagseguro</th>
			<th>&nbsp;&nbsp;&nbsp;Imposto&nbsp;&nbsp;&nbsp;</th>
			<th>Cancelamento</th>
			<th>Receita Liquida Mês</th>
			<th>Cotas Professores</th>
			<th>Qtde total comentadas</th>
			<th class='relatorio-destaque'>Qtd total cmtdas<br> pelo professor</th>
			<th>Qtd-porcetagem</th>

			<?php if($tem_dados_travados): ?>
				<th class='relatorio-destaque'>Qtd cmtdas<br> pelo professor até para cálculo %</th>
				<th>Qtd-porcetagem para pgto</th>
			<?php endif; ?>

			<th>R$ prof mês corrente</th>
			<th>R$ acumulado não pago (meses anteriores)</th>
			<th>Valor acumulado p/ próximo mês</th>
			<th>Valor a receber</th>
		</tr>
	</thead>
	
	<tbody>
	<?php 
		// $min_pagamento = 300;
			
		// if($yyyymmdd < '201704') {
		// 	$min_pagamento = 100;
		// }

		$soma_receita = 0;
		$soma_pagseguro = 0;
		$soma_imposto = 0;
		$soma_cancelamento = 0;
		$soma_receita_liquida = 0;
		$soma_qtde_comentada = 0;
		$soma_percentual = 0;
		$soma_percentual_anterior = 0;
		$soma_percentual_sem_formatacao = 0;
		$soma_percentual_anterior_sem_formatacao = 0;
	?>
	
	<?php if($questoes_comentadas):

		$soma_pg_professor_mes = 0;

		?>
		<?php foreach ($questoes_comentadas as $dados) : ?>
			<?php 

				if($tem_dados_travados)
				{
					$percentual = get_percentual($dados['qct_total'], $total_coments);
					$percentual_sem_formatacao = get_percentual_sem_format($dados['qct_total'], $total_coments);

					$percentual_anterior = get_percentual($dados['qcp_qtde_comentada'], $total_coments);
					$percentual_anterior_sem_formatacao = get_percentual_sem_format($dados['qcp_qtde_comentada'], $total_coments);

					$soma_percentual_anterior += numero_americano($percentual_anterior);
					$soma_percentual_anterior_sem_formatacao += ($percentual_anterior_sem_formatacao * 100);
				}
				else
				{
					$percentual = get_percentual($dados['qcp_qtde_comentada'], $total_coments);
					$percentual_sem_formatacao = get_percentual_sem_format($dados['qcp_qtde_comentada'], $total_coments);
				}

				$pg_professor_mes = calc_pg_professor_mes($totais_pagseguro, $percentual_sem_formatacao);
				$acumulado_prof = get_valor_acumulado_prof($dados['user_id'], $mes_ano);
				$acumulado_prox_mes = get_valor_acumulado_prox_mes(calc_pg_professor_mes($totais_pagseguro, $percentual_sem_formatacao), get_valor_acumulado_prof($dados['user_id'], $mes_ano),$min_pagamento);
				$a_receber = numero_americano($pg_professor_mes) + $acumulado_prof >= $min_pagamento ? numero_americano($pg_professor_mes) + $acumulado_prof : 0;

				$soma_receita = $totais_pagseguro['receita'];
				$soma_pagseguro = $totais_pagseguro['pagseguro'];
				$soma_imposto = $totais_pagseguro['imposto'];
				$soma_cancelamento = $totais_pagseguro['cancelamento'];
				$soma_receita_liquida = $totais_pagseguro['receita_liquida'];
				$soma_qtde_comentada += $dados['qcp_qtde_comentada'];
				
				$soma_percentual += numero_americano($percentual);
				$soma_percentual_sem_formatacao += ($percentual_sem_formatacao * 100);
				$soma_pg_professor_mes += numero_americano($pg_professor_mes);
				$soma_acumulado_prof += $acumulado_prof;
				$soma_acumulado_prox_mes += numero_americano($acumulado_prox_mes);
				$soma_a_receber += $a_receber;
			?>
		<?php endforeach; ?>
		
		<?php if($tem_dados_travados): ?>

		<?php
			
			$percentual_pos = get_percentual($total_comments_pos_travamento, $total_coments);
			
			$percentual_pos_sem_formatacao = get_percentual_sem_format($total_comments_pos_travamento, $total_coments);
			$pg_professor_mes_pos = calc_pg_professor_mes($totais_pagseguro, $percentual_pos_sem_formatacao);

			$soma_percentual += numero_americano($percentual_pos);
			$soma_percentual_sem_formatacao += ($percentual_pos_sem_formatacao * 100);
			$soma_pg_professor_mes += numero_americano($pg_professor_mes_pos);

		?>
		
		<?php endif; ?>

		<tr class="bg-secondary text-white font-11">
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>

			<?php if($tem_dados_travados): ?>
				<td class='relatorio-destaque'><?= numero_inteiro($soma_qtde_comentada) ?></td>
				<td><?= round($soma_percentual_anterior) ?> % <!--<br> <?= number_format($soma_percentual_anterior_sem_formatacao, 2, ',', '.') ?> %--></td>
			<?php endif; ?>
			
			<td class='relatorio-destaque'><?= numero_inteiro($soma_qtde_comentada) ?></td>
			<td><?= round($soma_percentual) ?> % <!--<br> <?= number_format($soma_percentual_sem_formatacao, 2, ',', '.') ?> %--></td>
			<td>R$ <?= number_format($soma_pg_professor_mes, 2, ',', '.') ?></td>
			<td>R$ <?= number_format($soma_acumulado_prof, 2, ',', '.') ?></td>
			<td>R$ <?= number_format($soma_acumulado_prox_mes, 2, ',', '.') ?></td>
			<td>R$ <?= number_format($soma_a_receber, 2, ',', '.') ?></td>
		</tr>

		<?php if($tem_dados_travados): ?>

			<tr class="bg-gray font-weight-bold">
				<td></td>
				<td>Questões Exponencial (comentários pós <?= converter_para_ddmmyyyy(REL_QUESTOES_COMENTADAS_DATA_FIXADA) ?> )</td>
				<td><?php echo $mes_ano ?></td>
				<td>R$ <?php echo number_format($totais_pagseguro['receita'], 2, ',', '.') ?></td>
				<td>R$ <?php echo number_format($totais_pagseguro['pagseguro'], 2, ',', '.') ?></td>
				<td>R$ <?php echo number_format($totais_pagseguro['imposto'], 2, ',', '.') ?></td>
				<td>R$ <?php echo number_format($totais_pagseguro['cancelamento'], 2, ',', '.') ?></td>
				<td>R$ <?php echo number_format($totais_pagseguro['receita_liquida'], 2, ',', '.') ?></td>
				<td>50%</td>
				<td><?= numero_inteiro($total_coments) ?></td>
				<td class='relatorio-destaque'></td>
				<td></td>
				<td><?= numero_inteiro($total_comments_pos_travamento) ?></td>
				<td><?= $percentual_pos ?> %</td>
				<td>R$ <?= number_format($pg_professor_mes_pos, 2, ',', '.') ?></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>

		<?php endif; ?>
		
		<?php	
		foreach ($questoes_comentadas as $dados) : 

			$nome = get_usuario($dados['user_id']);
			
			if($tem_dados_travados)
			{
				$percentual = get_percentual($dados['qct_total'], $total_coments);
				$percentual_sem_formatacao = get_percentual_sem_format($dados['qct_total'], $total_coments);

				$percentual_antigo = get_percentual($dados['qcp_qtde_comentada'], $total_coments);
			}
			else
			{
				$percentual = get_percentual($dados['qcp_qtde_comentada'], $total_coments);
				$percentual_sem_formatacao = get_percentual_sem_format($dados['qcp_qtde_comentada'], $total_coments);
			}

			$pg_professor_mes = calc_pg_professor_mes($totais_pagseguro, $percentual_sem_formatacao);
			$acumulado_prof = get_valor_acumulado_prof($dados['user_id'], $mes_ano);
			$acumulado_prox_mes = get_valor_acumulado_prox_mes(calc_pg_professor_mes($totais_pagseguro, $percentual_sem_formatacao), get_valor_acumulado_prof($dados['user_id'], $mes_ano), $min_pagamento);
			$a_receber = numero_americano($pg_professor_mes) + $acumulado_prof >= $min_pagamento ? number_format(numero_americano($pg_professor_mes) + $acumulado_prof, 2, ',', '.') : '';
			?>
				<tr>
					<td><?= $dados['posicao'] ?></td>
					<td><a href="/questoes/relatorios/questoes_comentadas_resumo/<?= $dados['user_id']?>"><?php echo $nome->user_firstname . " " . $nome->user_lastname ?></a></td>
					<td><?php echo $mes_ano ?></td>
					<td>R$ <?php echo number_format($totais_pagseguro['receita'], 2, ',', '.') ?></td>
					<td>R$ <?php echo number_format($totais_pagseguro['pagseguro'], 2, ',', '.') ?></td>
					<td>R$ <?php echo number_format($totais_pagseguro['imposto'], 2, ',', '.') ?></td>
					<td>R$ <?php echo number_format($totais_pagseguro['cancelamento'], 2, ',', '.') ?></td>
					<td>R$ <?php echo number_format($totais_pagseguro['receita_liquida'], 2, ',', '.') ?></td>
					<td>50%</td>
					<td><?= numero_inteiro($total_coments) ?></td>
					<td class='relatorio-destaque'><?= numero_inteiro($dados['qcp_qtde_comentada']) ?></td>

					<?php if($tem_dados_travados): ?>
						<td><?= $percentual_antigo ?> %</td>
						<td><?= !is_null($dados['qct_total']) ? numero_inteiro($dados['qct_total']) : null ?></td>
					<?php endif; ?>

					<td><?= $percentual ?> %</td>
					<td><?= $pg_professor_mes ?></td>
					<td><?= number_format($acumulado_prof, 2, ',', '.') ?></td>
					<td><?= $acumulado_prox_mes ?></td>
					<td><?= $a_receber ?></td>
				</tr>
		<?php endforeach; ?>
	<?php endif;?>
	</tbody>
</table>