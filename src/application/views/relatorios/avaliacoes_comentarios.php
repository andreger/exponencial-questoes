<?php 
get_header();
header('Content-Type: text/html; charset=utf-8');
?>
<div class="container-fluid pt-1 pt-md-4">
    <div class="mb-5 col-12 text-center text-md-left text-blue">
        <h1>Relatório de avaliações de comentários</h1>
    </div>
</div>
<div class="container">

	<form action="<?php echo base_url("relatorios/avaliacoes_comentarios") ?>" id="form-relatorio" method="post">
        
        <?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) : ?>
            <div class="row">
                <label class="col-2">Professor </label>
                <div class="col-4">
                    <?php echo form_dropdown('professor_id', $combo_professores, $professor_id, 'class="form-control" id="professor_id"') ?>
                </div>
            </div>
        <?php endif; ?>
        
        <div class="row mt-3">
            <label class="col-2">Média </label> 
            <div class="col-2">
                <?php echo form_dropdown('media_avaliacao_comparador', $media_avaliacao_comparador_combo, $media_avaliacao_comparador, 'class="form-control"'); ?> 
           </div>
           <div class="col-2">
                <?php echo form_input('media_avaliacao', $media_avaliacao, 'class="form-control"'); ?>
            </div>
        </div>

        <div class="row mt-3">
            <label class="col-2">Com pelo menos </label> 
            <div class="col-3">
                <?php echo form_input('quantidade_avaliacoes', $quantidade_avaliacoes, 'class="form-control"'); ?> 
            </div>
            <div class="col-2 font-10">
                avaliações
            </div>
            
        </div>

        <div class="row mt-3">
            <label class="col-2">Ordenar por </label>
            <div class="col-2">
                <?php echo form_dropdown('ordem', $avaliacao_comentario_ordem_combo, $ordem?$ordem:'Id Questão', 'class="form-control"'); ?> 
            </div>    
            <div class="col-2">    
                <?php echo form_dropdown('ordem_dir', $avaliacao_comentario_sentido_combo, $ordem_dir?$ordem_dir:'Crescente', 'class="form-control"'); ?>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-2">
            <input id="btn-filtrar" type="submit" name="submit" class="col-12 btn u-btn-blue" value="Filtrar">
            </div>
        </div>

	</form>
    
    <div class="font-10 mt-3">
	    <div class="row">
	        <label class="col-md-4 control-label">Total de comentários: </label>
	        <div class="col-md-2"> <?php echo $total_avaliacoes ?></div>
	    </div>
	    <div class="row">
	        <label class="col-md-4 control-label">Média das avaliações: </label>
	        <div class="col-md-2"> <?php echo $total_avaliacoes_media_simples ?></div>
	    </div>
	    <div class="row">
	        <label class="col-md-4 control-label">Média das avaliações válidas: </label>
	        <div class="col-md-2"> <?php echo $total_avaliacoes_media ?></div>
	    </div>
    </div>

    <div class="row">
        <?php echo $html_tabela;?>
    </div>

   
    <div class="row mb-5">
        <?= $links ?>
    </div>
    


</div>

<script>
    
    $('document').ready(function() {
        $(".txt-media").mask("9,99");
    });

</script>

<?php get_footer() ?>