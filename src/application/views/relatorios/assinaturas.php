<?php
get_header();
header('Content-Type: text/html; charset=utf-8');
?>
<div class="container-fluid pt-1 pt-md-5">
	<div class="mb-5 col-12 text-center text-md-left text-blue">
        <h1>Relatório de Assinaturas do SQ</h1>
    </div>
<div class="container">
<div class="row pb-5">
	<form method="post" id="form-relatorio" action="/questoes/relatorios/assinaturas">
		<div class="row ml-auto mr-autor">
			<div class="col-4 p-0">
			<input type="checkbox" <?= isset($_POST['data_expiracao']) && $_POST['data_expiracao'] ? "checked=checked" : "" ?>> A expirar até
			</div>
			<div class="col-8 p-0">
			<input type="text" class="form-control date-picker relatorio-assinatura-data-expiracao" name="data_expiracao" value="<?= isset($_POST['data_expiracao']) && $_POST['data_expiracao'] ?  $_POST['data_expiracao'] : '' ?>">
			</div>
		</div>

		<div class="mt-3">
			<span class="mr-1">Exibir: </span>
			<span class="mr-1"><input class="relatorio-assinatura-filtro" type="radio" name="filtro" value="<?= FILTRO_ASSINATURAS_TODOS ?>" <?= $filtro_selecionado == FILTRO_ASSINATURAS_TODOS ? 'checked=checked' : '' ?>> Todos</span>
			<span class="mr-1"><input class="relatorio-assinatura-filtro" type="radio" name="filtro" value="<?= FILTRO_ASSINATURAS_SOMENTE_ATIVOS ?>" <?= $filtro_selecionado == FILTRO_ASSINATURAS_SOMENTE_ATIVOS ? 'checked=checked' : '' ?>> Somente ativos</span>
			<span class="mr-1"><input class="relatorio-assinatura-filtro" type="radio" name="filtro" value="<?= FILTRO_ASSINATURAS_A_EXPIRAR_EM_30_DIAS ?>" <?= $filtro_selecionado == FILTRO_ASSINATURAS_A_EXPIRAR_EM_30_DIAS ? 'checked=checked' : '' ?>> A expirar em 30 dias</span>
			<span class="mr-1"><input class="relatorio-assinatura-filtro" type="radio" name="filtro" value="<?= FILTRO_ASSINATURAS_SOMENTE_EXPIRADOS ?>" <?= $filtro_selecionado == FILTRO_ASSINATURAS_SOMENTE_EXPIRADOS ? 'checked=checked' : '' ?>> Somente expirados</span>
		</div>

		<div class="mt-3">
			<input type="submit" name="filtrar" value="Filtrar" class="btn u-btn-blue">
			<input type="submit" name="exportar" value="Exportar XLS" class="btn u-btn-blue">
		</div>
	</form>

	<div class="w-100 d-flex justify-content-between align-items-end mt-3">
		<span><?= $total ?></span>
		<?= $links ?>
	</div>
	<?= $tabela ?>
	<div class="w-100 d-flex justify-content-between align-items-end mt-3">
		<span><?= $total ?></span>
		<?= $links ?>
	</div>
	</div>
</div>
</div>

<?= get_date_picker(".date-picker") ?>

<script>
jQuery(function () {
	jQuery('.relatorio-assinatura-filtro').click(function () {
		jQuery('.relatorio-assinatura-check').prop('checked', false);
		jQuery('.relatorio-assinatura-data-expiracao').val("");
	});

	jQuery('.relatorio-assinatura-check').click(function () {
		jQuery('.relatorio-assinatura-filtro').prop('checked', false);
	});

	jQuery('.relatorio-assinatura-data-expiracao').change(function () {
		jQuery('.relatorio-assinatura-filtro').prop('checked', false);
	});
});
</script>
<?php get_footer() ?>