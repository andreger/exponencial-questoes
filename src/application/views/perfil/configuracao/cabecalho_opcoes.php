<ul class='col-lg-3' id='sortable'>
<?php 
    $opcoes = explode(",", $cabecalho_ordem);
    foreach ($opcoes as $opcao => $value):
?>        
    <li class="sortable-item" id="<?= $value ?>">
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?= $opcao ?>"><?= $value ?><i class="icon-angle-down"></i>
                        </a>
                    </h4>
                </div>
                <div id="collapse_<?= $opcao ?>" class="panel-collapse collapse">
                    <div class="panel-body">
                    </div>
                </div>
            </div>
        </div>
    </li>
<?php endforeach; ?>
</ul>
<script>
    $('#sortable').sortable({
        update: function(event, ui) {

            var order = [];

            $('.sortable-item').each( function(e) {
                order.push( $(this).attr('id'));
                var positions = order.join(',');
                $('#cabecalho_ordem').val(positions);
            });
        }
    });
</script>