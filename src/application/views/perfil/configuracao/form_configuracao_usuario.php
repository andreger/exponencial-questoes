<div class="container">
	<div class="col-12" style="margin-top: 40px; text-align: center;">
		<h1>Configurações</h1>
		<br />
		<?php exibir_mensagem(); ?>
	</div>

	<div class="col-12">
    	<?php echo form_open("perfil/configuracoes"); ?>
			<div class="row">
			<?php echo $user_id; ?>
				<div class="col-lg-12">
					<div class="checkbox">
						<label class="checkbox-inline i-checks">
							<?php echo form_checkbox('check-acertos', ATIVO, $check_acertos ? TRUE : FALSE); ?> Mostrar meus acertos e erros
						</label>
						<label title="Se desmarcado, ao acessar a questão, não será mostrada a informação se o aluno acertou ou errou tal questão." style="color: blue;"><b>?</b></label>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="checkbox">
						<label class="checkbox-inline i-checks">
							<?php echo form_checkbox('check-filtros', ATIVO, $check_filtros ? TRUE : FALSE); ?> Exibir filtros selecionados
						</label>
						<label title="Se desmarcado, ao filtrar as questões, a informação com os filtros selecionados não será mostrada." style="color: blue;"><b>?</b></label>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="checkbox">
						<label class="checkbox-inline i-checks">
							<?php echo form_checkbox('check-historico', ATIVO, $check_historico ? TRUE : FALSE); ?> Manter histórico de marcações no caderno
						</label>
						<label title="Se desmarcado, ao acessar a questão, não serão mostradas as marcações realizadas no caderno em sua última resolução." style="color: blue;"><b>?</b></label>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="checkbox">
						<label class="checkbox-inline i-checks">
							<?php echo form_checkbox('check-url-cad', ATIVO, $check_url_cad ? TRUE : FALSE); ?> Esconder links das questões ao imprimir cadernos
						</label>
						<label title="Se desmarcado, ao imprimir um caderno, será mostrada a url das questões." style="color: blue;"><b>?</b></label>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="checkbox">
						<label class="checkbox-inline i-checks">
							<?php echo form_checkbox('check-assuntos', ATIVO, $check_assuntos ? TRUE : FALSE); ?> Exibir os assuntos das questões
						</label>
						<label title="Se desmarcado, na hora de listar ou imprimir questões, os assuntos serão omitido." style="color: blue;"><b>?</b></label>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="ibox float-e-margins">
						<div class="ibox-content">
							<h3>Ordem cabeçalho questões <label title="Ordem de exibição dos dados das questões ao resolver questões." style="color: blue;"><b>?</b></label></h3>
							<div class="panel-body">
								<input type='hidden' name='cabecalho_ordem' id='cabecalho_ordem' value='<?php echo isset($cabecalho_ordem) ? $cabecalho_ordem : '' ?>'/>
								<div class="panel-group" id="accordion">
									<?php $this->view('perfil/configuracao/cabecalho_opcoes') ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="ibox-content"></div>
			<input type="submit" id="check-salvar" name="check-salvar" class="btn btn-primary" value="Salvar">
		<?php echo form_close(); ?>
    </div>
</div>
<script type="text/javascript">
	$('.i-checks').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});
</script>