<div class="col-12 col-md-9 mt-4">
    
    <div class="mb-2"><strong>Apresentação:</strong> <?php echo $professor['mini_cv'] ?></div>

    <div class="mb-2"><strong>Descrição Detalhada:</strong> <?php echo purificar_html($professor['descricao']) ?></div>

    <div class="mb-2"><strong>Facebook:</strong> <?php echo $professor['facebook'] ?></div>

    <div class="mb-2"><strong>Instagram:</strong> <?php echo $professor['instagram'] ?></div>

    <div class="mb-2"><strong>Youtube:</strong> <?php echo $professor['youtube'] ?></div>

</div>