<div class="col-12 col-md-9">

    <div class="font-14 mb-2 text-center text-md-left"><?php echo $usuario['nome_completo'] ?></div>

    <?php if($item = $usuario['cpf']) :?>
    <div class="mb-2"><strong>CPF:</strong> <?php echo $item ?></div>
    <?php endif; ?>

    <?php if($item = $usuario['apelido']) :?>
    <div class="mb-2"><strong>Apelido:</strong> <?php echo $item ?></div>
    <?php endif; ?>

    <?php if($item = $usuario['email']) :?>
    <div class="mb-2"><strong>E-mail:</strong> <?php echo $item ?></div>
    <?php endif; ?>
            
    <?php if($item = $usuario['data_nascimento']) :?>
    <div class="mb-2"><strong>Data Nascimento:</strong> <?php echo $item ?></div>
    <?php endif; ?>
    
    <?php if($item = $usuario['endereco']) :?>
    <div class="mb-2"><strong>Endereço:</strong> <?php echo $item ?></div>
    <?php endif; ?>
    
    <?php if($item = $usuario['bairro']) :?>
    <div class="mb-2"><strong>Bairro:</strong> <?php echo $item ?></div>
    <?php endif; ?>
    
    <?php if($item = get_cidade_usuario($usuario)) :?>
    <div class="mb-2"><strong>Cidade:</strong> <?php echo $item ?></div>
    <?php endif; ?>
    
    <?php if($item = $usuario['estado']) :?>
    <div class="mb-2"><strong>Estado:</strong> <?php echo $item ?></div>
    <?php endif; ?>
    
    <?php if($item = $usuario['cep']) :?>
    <div class="mb-2"><strong>CEP:</strong> <?php echo $item ?></div>
    <?php endif; ?>
    
    <?php if($item = $usuario['telefone']) :?>
    <div class="mb-2"><strong>Telefone:</strong> <?php echo $item ?></div>
    <?php endif; ?>

    <?php if($item = $usuario['codigo_desconto']) :?>
    <div class="mb-2"><strong>Código de desconto:</strong> <?php echo $item ?></div>
    <?php endif; ?>
    
    <?php if(is_assinante_sq()) : ?>
    <div class="mb-2">
        <div class="font-14 mb-2">Assinante do Sistema de Questões</div>
        <div class="mb-2"><strong>Validade:</strong> <?php echo converter_para_ddmmyyyy(get_user_meta($usuario['id'], 'assinante-sq-validade', true)) ?></div>
    </div>
    <?php endif; ?>
    
</div>