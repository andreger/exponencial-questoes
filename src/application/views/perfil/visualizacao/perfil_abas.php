<div class="tab-v1 panel-options col-9">
    
    <?php exibir_mensagem() ?>	

    <ul class="nav nav-tabs" id="tabs">
        
        <li class="active"><a class="aba-perfil" data-toggle="tab" href="#tab-dados-basicos">Dados Básicos</a></li>
        
        <?php if($is_professor): ?>
            <li class=""><a class="aba-perfil" data-toggle="tab" href="#tab-professor">Dados do Professor</a></li>
        <?php endif; ?>

    </ul>

    <div class="panel-body">
        <div class="tab-content">

            <div id="tab-dados-basicos" class="tab-pane active">
                <?php $this->view('perfil/visualizacao/aba_dados_basicos') ?>
            </div>
            
            <?php if($is_professor): ?>
                <div id="tab-professor" class="tab-pane">
                    <?php $this->view('perfil/visualizacao/aba_professor') ?>
                </div>
            <?php endif; ?>
            
        </div>
    </div>
        
    <?php if(is_usuario_logado($usuario['id'])) : ?>
        <div class="col-12 col-md-8 col-lg-10 mt-5">
            <a href="/questoes/perfil/editar" class='btn u-btn-primary'>Editar</a>
            <a href="/alteracao-de-senha" class='btn u-btn-primary'>Alterar senha</a>	
                        
            <div class="mt-4">
                <a class="t-d-none text-blue" href="/questoes/perfil/recuperar_senha">
                    Faço login via Facebook, não tenho a senha atual. Envie-me uma nova senha por e-mail.
                </a>
            </div>
            
            <?php echo form_open_multipart('/perfil/alterar_foto', 'id="alterar-foto-form"') ?>
                <input style="visibility:hidden" name="userfile" type="file" id="alterar-foto-file" />
            </form>
        </div>
    <?php endif; ?>

</div>
<script>
    //TODO: Não deveria precisar disso aqui para funcionar
    jQuery(".aba-perfil").click(function(){
        jQuery("#tabs > li.active").removeClass("active");
        jQuery(this).parent().addClass("active");
    });
</script>