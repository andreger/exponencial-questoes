<?php 
redirecionar_se_nao_estiver_logado();
get_header() ?>

<?php $titulo = is_usuario_logado($usuario['id']) ? 'Meu Perfil' : 'Perfil' ?>

<div class="container pt-2">
	<?php echo get_voltar_link('&lt;&lt; Área do Aluno', '/minha-conta', is_usuario_logado($usuario['id'])) ?>
</div>

<div class="container pt-1 pt-md-4">
	<h1><?= $titulo ?></h1>
</div>

<div class="container pt-1 pt-md-3">
	
<div class="row mt-3">
	<div class="col-12 col-md-3 col-lg-2 usuario-foto text-center text-md-left">		
		<?php echo get_foto_usuario($usuario['id']) ?>
		
		<?php if(is_usuario_logado($usuario['id']) && !is_professor($usuario['id'])) : ?>
			<div class="text-center">
			<a href="#" id='alterar-foto-btn'>Alterar foto</a></div>
		<?php endif; ?>
	</div>

	<?php $this->view('perfil/visualizacao/perfil_abas') ?>
	
</div>
</div>
<script type="text/javascript">
jQuery('#alterar-foto-btn').click(function() {
	jQuery('#alterar-foto-file').click();	
});

jQuery('#alterar-foto-file').change(function() {
	jQuery('#alterar-foto-form').submit();
});
</script>
<?php get_footer() ?>
