<div class="tab-v1 panel-options col-9">

    <form id="perfil-form" action="<?php echo get_editar_perfil_url() . ($from_checkout ? '?from=checkout' : '') . ($perfil_basico ? '&perfil_basico=1' : '') ?>" method="post">

        <ul class="nav nav-tabs" id="tabs">
            
            <li class="active"><a class="aba-perfil" data-toggle="tab" href="#tab-dados-basicos">Dados Básicos</a></li>
            
            <?php if($is_professor): ?>
                <li class=""><a class="aba-perfil" data-toggle="tab" href="#tab-professor">Dados do Professor</a></li>
            <?php endif; ?>

        </ul>

        <div class="panel-body">
            <div class="tab-content">

                <div id="tab-dados-basicos" class="tab-pane active">
                    <?php $this->view('perfil/edicao/aba_dados_basicos') ?>
                </div>
                
                <?php if($is_professor): ?>
                    <div id="tab-professor" class="tab-pane">
                        <?php $this->view('perfil/edicao/aba_professor') ?>
                    </div>
                <?php endif; ?>
                
            </div>
        </div>
            
        <div class="col-12 col-md-8 col-lg-10 text-center mt-5">
            <button class="btn u-btn-primary" type="submit" name="professor">Salvar</button>
        </div>

    </form>

</div>
<script>
    //TODO: Não deveria precisar disso aqui para funcionar
    jQuery(".aba-perfil").click(function(){
        jQuery("#tabs > li.active").removeClass("active");
        jQuery(this).parent().addClass("active");
    });
</script>