<div class="col-12 col-md-9 col-lg-10 usuario-editar-painel">
    
    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">Nome: <span>*</span></label></div>
        <div class="col-12 col-md-8"><input class="form-control" type="text" name="nome" value="<?php echo $usuario['nome'] ?>"></div>
    </div>

    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">Sobrenome: <span>*</span></label></div>
        <div class="col-12 col-md-8"><input class="form-control" type="text" name="sobrenome" value="<?php echo $usuario['sobrenome'] ?>"></div>
    </div>

    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">CPF: <?php echo !$perfil_basico ? "<span>*</span>" : ""?></label></div>
        <div class="col-12 col-md-8" title="Solicitamos o seu CPF para emissão da sua nota fiscal">
            <input id="cpf" class="form-control" type="text" name="cpf" value="<?php echo $usuario['cpf'] ?>">
            <?php echo !$perfil_basico ? '<div style="margin-bottom: 15px"><a href="#" title="O CPF é utilizado para correta identificação do usuário, além de ser necessário para o cumprimento das obrigações tributárias como emissão de Nota Fiscal de Serviço.||Fique tranquilo que o nosso cadastro é seguro e o seus dados são mantidos em sigilo.">Por que o meu CPF é necessário?</a></div>' : '' ?>
        </div>
    </div>
    
    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">E-mail: <span>*</span></label></div>
        <div class="col-12 col-md-8"><input class="form-control" type="text" id="email" name="email" value="<?php echo $usuario['email'] ?>"></div>
    </div>
    
    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">Confirmar E-mail: <span>*</span></label></div>
        <div class="col-12 col-md-8"><input class="form-control" type="text" id="email_confirm" name="email_confirm" value="<?php echo $usuario['email'] ?>"></div>
    </div>
    
    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">Apelido:</label></div>
        <div class="col-12 col-md-8">
            <input class="form-control" type="text" name="apelido" value="<?php echo $usuario['apelido'] ?>">
            <?php echo form_checkbox('mostrar_apelido', '1', $mostrar_apelido_selecionado, 'style="margin-bottom: 20px"'); ?> Mostrar apelido<br>
            <span style="color:#ff0000; display:block; margin-bottom: 10px;"><i>O apelido será mostrado em todas as áreas públicas ou compartilhadas do site. Use-o, se preferir não divulgar seu nome verdadeiro.</i></span>
        </div>
    </div>
                
    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">Data Nascimento: <span>*</span></label></div>
        <div class="col-12 col-md-8"><input id="data_nascimento" class="form-control" type="text" name="data_nascimento" value="<?php echo $usuario['data_nascimento'] ?>"></div>
    </div>

    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">CEP: <?= !$perfil_basico ? "<span>*</span>" : ""?></label></div>
        <div class="col-12 col-md-8"><input id="cep" class="form-control" type="text" name="cep" value="<?php echo $usuario['cep'] ?>"></div>
    </div>
    
    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">Endereço: <?php echo !$perfil_basico ? "<span>*</span>" : ""?></label></div>
        <div class="col-12 col-md-8"><input class="form-control" type="text" name="endereco" id="endereco" value="<?php echo $usuario['endereco'] ?>"></div>
    </div>
    
    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">Bairro: </label></div>
        <div class="col-12 col-md-8"><input class="form-control" type="text" id="bairro" name="bairro" value="<?php echo $usuario['bairro'] ?>"></div>
    </div>
    
    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">Estado: <?php echo !$perfil_basico ? "<span>*</span>" : ""?></label></div>
        <div class="col-12 col-md-8"><?php echo form_dropdown('estado', $estados_combo, $estado_selecionado, 'class="form-control" id="estado">') ?></div>
    </div>

    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">Cidade: <?php echo !$perfil_basico ? "<span>*</span>" : ""?></label></div>
        <div class="col-12 col-md-8"><?php echo form_dropdown('cidade', $cidades_combo, $cidade_selecionada, 'class="form-control" id="cidade">') ?></div>
    </div>

    <div class="row mt-2">
        <div class="col-12 col-md-3 text-left text-md-right"><label class="text-left text-md-right">Telefone: <?= !$perfil_basico ? "<span>*</span>" : ""?></label></div>
        <div class="col-12 col-md-8"><input id="telefone" class="form-control" type="text" name="telefone" value="<?php echo $usuario['telefone'] ?>"></div>
    </div>

    <!--<div class="row">
        <div class="threecol column"><label class="text-left text-md-right">Código de Desconto:</label></div>
        <div class="col-12 col-md-8"><input id="telefone" class="form-control" type="text" name="codigo_desconto" value="<?php echo $usuario['codigo_desconto'] ?>"></div>
    </div>-->
    
</div>