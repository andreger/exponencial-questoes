<?php get_header() ?>

<div class="container pt-1">
	<?php echo get_voltar_link() ?>		
</div>

<div class="container pt-1 pt-md-4">
	<h1>Meu Perfil</h1>
</div>

<?php if($from_checkout) : ?>
<div class="container pt-2 pb-2">
	<div class="text-14 text-center">
		<h2>Você precisa completar seus dados antes de finalizar a compra!</h2>
	</div>
</div>
<?php endif; ?>

<div class="container pt-3 pb-5">

<div class="row mt-3">
	<div class="col-12 col-md-3 col-lg-2 usuario-foto text-center text-md-left">		
		<div><?php echo get_foto_usuario($usuario['id']) ?></div>
	</div>

	<?php $this->view('perfil/edicao/perfil_abas') ?>
	
</div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<script src="/wp-content/themes/academy/js/jquery.validate.min.js"></script>
<script src="/wp-content/themes/academy/js/jquery.mask.min.js"></script>
<script src="<?php echo base_url('assets-admin/js/jquery.chained.remote.min.js') ?>"></script>
<script>
jQuery.validator.addMethod("birthdate", function(value, element) {
	  return this.optional(element) || /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/.test(value);
	}, "Data de nascimento inválida");

jQuery.validator.addMethod("brphone", function(value, element) {
	  value = value.replace("_","");
	  return this.optional(element) || /^([\(]{1}[1-9]{2}[\)]{1}[ ][0-9]{5}[\-]{1}[0-9]{3,4})$/.test(value);
	}, "Telefone inválido");

jQuery.validator.addMethod("onlychar", function(value, element) {
	  value = value.trim();
	  return this.optional(element) || /^([a-zA-ZãõáéíóúâêôçÃÕÁÉÍÓÚÂÊÔÇ]+\s)*[a-zA-ZãõáéíóúâêôçÃÕÁÉÍÓÚÂÊÔÇ]+$/.test(value);
	}, "Nome inválido");

jQuery.validator.addMethod("cpf", function (cpf, element) {
		cpf = cpf.replace(".","");
		cpf = cpf.replace(".","");
		cpf = cpf.replace("-","");

		if(cpf == 00000000000 || 
		   cpf == 11111111111 ||
		   cpf == 22222222222 ||
		   cpf == 33333333333 ||
		   cpf == 44444444444 ||
		   cpf == 55555555555 ||
		   cpf == 66666666666 ||
		   cpf == 77777777777 ||
		   cpf == 88888888888 ||
		   cpf == 99999999999) {
			return false;
		}
		
	    while (cpf.length < 11) cpf = "0" + cpf;
	    var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
	    var a = [];
	    var b = new Number;
	    var c = 11;
	    for (i = 0; i < 11; i++) {
	        a[i] = cpf.charAt(i);
	        if (i < 9) b += (a[i] * --c);
	    }
	    if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11 - x }
	    b = 0;
	    c = 11;
	    for (y = 0; y < 10; y++) b += (a[y] * c--);
	    if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11 - x; }
	    if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) return this.optional(element) || false;
	    return this.optional(element) || true;
	}, "Informe um CPF válido.");	

$().ready(function() {
	$(document).tooltip({
		 content: function(callback) { 
		     callback($(this).prop('title').replace(/\|/g, '<br />')); 
		  }
	});

	$('#email_confirm').bind("cut copy paste contextmenu",function(e) {
        e.preventDefault();
    });
	
	$("#cidade").remoteChained({
	    parents : "#estado",
	    url : "/questoes/xhr/get_cidades_por_estado",
	    loading : "Carregando cidades..."
	});

	$("#cep").on('keyup', function() {
		var cep = $(this).val();
		cep = cep.replace('-','');
		cep = cep.replace('_','');

		if(cep.length != 8) {
			$("#endereco").prop("disabled", true);
			$("#bairro").prop("disabled", true);
			$("#cidade").prop("disabled", true);
			$("#estado").prop("disabled", true);

			$("#endereco").addClass("campo-desativado");
			$("#bairro").addClass("campo-desativado");
			$("#cidade").addClass("campo-desativado");
			$("#estado").addClass("campo-desativado");
		}
		else {
			$("#endereco").prop("disabled", false);
			$("#bairro").prop("disabled", false);
			$("#cidade").prop("disabled", false);
			$("#estado").prop("disabled", false);

			$("#endereco").removeClass("campo-desativado");
			$("#bairro").removeClass("campo-desativado");
			$("#cidade").removeClass("campo-desativado");
			$("#estado").removeClass("campo-desativado");

			$.get("https://viacep.com.br/ws/"+cep+"/json/", function(data) {
				$("#estado").val(data.uf);
				$("#endereco").val(data.logradouro);
				$("#bairro").val(data.bairro);

				$.get('/questoes/xhr/get_cidades_por_estado?estado='+data.uf, function(cidades) {
					$("#cidade").find("option").remove();
					$.each(cidades, function(key, value) {
	 					$("#cidade").append('<option value=' + value[0] + '>' + value[1] + '</option>');
					});

					$("#cidade").val(data.ibge);
				}, 'json');			
			}, 'json');
		}
	});

	$("#cep").trigger("blur");
	
	// field masks
	$("#cpf").mask("000.000.000-00");
	$("#data_nascimento").mask("00/00/0000");
	$("#telefone").mask("(00) 00000-0000");
	$("#cep").mask("00000-000");
	 
	// validate signup form on keyup and submit
	$("#perfil-form").validate({
		rules: {
			
			email: {
				required: true,
				email: true,
      			remote: {
        			url: "/wp-content/themes/academy/ajax/checar_email.php",
        			type: "post",
        			data: {
          				email: function() {
            				return $( "#email" ).val();
          				}
        			}
        		}
      		},

			email_confirm : {
				required: true,
                equalTo : "#email"
            },
            nome: {
				required: true,
				onlychar: true
			},
			sobrenome: {
				required: true,
				onlychar: true
			},
            data_nascimento: "required"
			
			<?php if(!$perfil_basico) : ?>
			,
			cidade: "required",
			estado: "required",
			cpf: {
				required: true,
				cpf: true
			},
			cep: "required",
			endereco: "required",
			telefone: {
				required: true,
				brphone: true
			}
			<?php endif; ?>
		},
		messages: {
			
			email: {
				required: "E-mail é obrigatório",
				email: "Formato do e-mail inválido",
      			remote: "Este e-mail já está sendo usado. Escolha outro, por favor."
      		},
			email_confirm : {
				required: "Confirmação de e-mail é obrigatória",
                equalTo : "A confirmação do e-mail está incorreta"
            },
            nome: {
				required: "Nome é obrigatório",
				onlychar: "O nome deve conter apenas letras e espaços"
			},
			sobrenome: {
				required: "Sobrenome é obrigatório",
				onlychar: "O sobrenome deve conter apenas letras e espaços"
			},
			data_nascimento: "Data de nascimento é obrigatória"

			<?php if(!$perfil_basico) : ?>
			,
			cidade: "Cidade é obrigatória",
			estado: "Estado é obrigatório",
			cpf: {
				required: "CPF é obrigatório",
				cpf: "CPF inválido"
			},
			cep: "CEP é obrigatório",
			endereco: "Endereço é obrigatório",
			telefone: {
				required: "Telefone é obrigatório",
				brphone: "Telefone inválido"
			}
			<?php endif; ?>
		},
	  	errorPlacement: function(error, element) {
		    if (element.attr("name") == "terms") {
		      error.insertAfter("#terms");
		    } else {
		      error.insertAfter(element);
		    }
		},
		submitHandler: function(form) {
		    form.submit();
		}
	});
});
</script>
<?php get_footer() ?>

