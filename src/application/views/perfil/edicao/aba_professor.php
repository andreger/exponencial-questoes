<div class="col-12 usuario-editar-painel">
        
    <div class="row mt-2">
        <div class="col-md-3 text-left text-md-right"><label class="text-left text-md-right">Apresentação: </label></div>
        <div class="col-md-8">
            <textarea maxlength="200" rows="4" class="form-control" type="text" name="mini_cv"><?php echo isset($_POST['mini_cv'])?$_POST['mini_cv']:$professor['mini_cv'] ?></textarea>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-3 text-left text-md-right"><label class="text-left text-md-right">Descrição Detalhada: </label></div>
        <div class="col-md-8">
            <!--<textarea id="descricao" class="wysisyng" name="descricao"></textarea>-->
            <?php 
                if(isset($_POST['mini_cv'])) {
                    $content = stripcslashes($_POST['descricao']);
                }else{
                    $content = stripcslashes($professor['descricao']);
                }
            ?>
            <?php wp_editor( $content, 'descricao', array( 'media_buttons' => false ) ); ?>
        </div>
    </div>
    
    <div class="row mt-2">
        <div class="col-md-3 text-left text-md-right"><label class="text-left text-md-right">Facebook: </label></div>
        <div class="col-md-8"><input class="form-control" type="text" id="facebook" name="facebook" value="<?php echo isset($_POST['facebook'])?$_POST['facebook']:$professor['facebook'] ?>"></div>
    </div>
    
    <div class="row mt-2">
        <div class="col-md-3 text-left text-md-right"><label class="text-left text-md-right">Instagram: </label></div>
        <div class="col-md-8"><input class="form-control" type="text" id="instagram" name="instagram" value="<?php echo isset($_POST['instagram'])?$_POST['instagram']:$professor['instagram'] ?>"></div>
    </div>
    
    <div class="row mt-2">
        <div class="col-md-3 text-left text-md-right"><label class="text-left text-md-right">Youtube: </label></div>
        <div class="col-md-8"><input class="form-control" type="text" name="youtube" id="youtube" value="<?php echo isset($_POST['youtube'])?$_POST['youtube']:$professor['youtube'] ?>"></div>
    </div>

</div>
<script>
    
    jQuery( document ).ready(function() {

        jQuery('.wysisyng').summernote({
            onImageUpload: function(files) {
                sendFile(files[0], $(this));
            }
        });

    });

</script>