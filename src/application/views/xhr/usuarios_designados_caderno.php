<?php if($usuarios_designados) :?>
	<table class="table table-hover no-margins">
		<thead>
			<tr>
				<th>Aluno</th>
				<th>E-mail</th>
				<th>Perfil</th>
				<th>Data de Designação</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$usuarios = array();
			foreach ($usuarios_designados as $item) {
				$usuario = get_usuario_array($item['usu_id']);
				$usuario['cad_id'] = $item['cad_id'];
				$usuario['cde_data'] = $item['cde_data'];
				array_push($usuarios, $usuario);
			}
			
			$usuarios = ordenar_usuarios_arrays($usuarios);
		?>
		<?php foreach ($usuarios as $usuario) : ?>
		<tr>
			<td><?= $usuario['nome_completo']?></td>
			<td><?= $usuario['email']?></td>
			<td><?= is_professor($usuario['id']) ? "Professor" : "Aluno" ?></td>
			<td><?= $usuario['cde_data'] ? converter_para_ddmmyyyy($usuario['cde_data']) : '' ?></td>
		</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php else : ?>
	<div>Nenhum aluno foi designado para esse caderno.</div>
<?php endif; ?>