<?php if($filtros) :?>
	<table class="table table-hover no-margins">
		<thead>
			<tr>
				<th>Campo</th>
				<th>Valores</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($filtros as $campo => $valores) : ?>
			<tr>
				<td><?= $campo ?></td>
				<td><?= $valores ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<div style="text-align: center; margin: 10px"><a href="<?= gerar_novo_caderno_url($caderno_id) ?>" class="btn btn-primary">Gerar Novo</a></div>
<?php else : ?>
	<div>Nenhum filtro foi aplicado para geração desse caderno.</div>
<?php endif; ?>