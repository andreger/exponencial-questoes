<?php if($usuarios_designados) :?>
	<table class="table table-hover no-margins">
		<thead>
			<tr>
				<th>Aluno</th>
				<th>Data Limite</th>
				<th>Ação</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$usuarios = array();
			foreach ($usuarios_designados as $item) {
				$usuario = get_usuario_array($item['usu_id']);
				$usuario['sim_id'] = $item['sim_id'];
				$usuario['siu_id'] = $item['siu_id'];
				$usuario['sim_data_limite'] = $item['sim_data_limite'];
				array_push($usuarios, $usuario);
			}
			
			$usuarios = ordenar_usuarios_arrays($usuarios);
		?>
		<?php foreach ($usuarios as $usuario) : ?>
		<tr id="siu-<?php echo $usuario['siu_id'] ?>">
			<td><?php echo $usuario['nome_completo']?></td>
			<td><?php echo !is_null($usuario['sim_data_limite']) ? converter_para_ddmmyyyy($usuario['sim_data_limite']) : '' ?></td>
			<td><a onclick="cancelar_designacao('<?php echo $usuario['siu_id'] ?>','<?php echo $usuario['sim_id'] ?>')" href="#" data-id="<?php echo $usuario['siu_id'] ?>">Cancelar Designação</a></td>
		</tr>
		<?php endforeach; ?>
		</tbody>																																																																																																																						</tbody>
	</table>
<?php else : ?>
	<div>Nenhum aluno foi designado para esse simulado.</div>
<?php endif; ?>