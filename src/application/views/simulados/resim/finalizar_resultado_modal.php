<div class="modal fade" id="confirma-finalizacao" tabindex="-1" role="dialog" aria-labelledby="confirma-finalizacaoModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      	<div style="clear: both;"></div>
      </div>
      <div class="modal-body text-center">
        <p id="modal_finalizar_simulado"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-white" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary confirmar-finalizar-simulado">Finalizar</button>
      </div>
    </div>
  </div>
</div>