<div class="row">
    <div class="col-lg-3">
        <div class="widget style1 verde-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-cubes fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <span> Ranking </span>
                    <h2 class="font-bold"><?= $posicao['posicao'] ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget style1 verde-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-bar-chart  fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <span> Nota </span>
                    <h2 class="font-bold"><?= $nota ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget style1 verde-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa fa-clock-o fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <span> Tempo Total </span>
                    <h2 class="font-bold"><?= $tempo_total ?></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget style1 verde-bg">
            <div class="row">
                <div class="col-xs-4">
                    <i class="fa <?= $aprovado ? "fa-thumbs-up" : "fa-thumbs-down" ?> fa-5x"></i>
                </div>
                <div class="col-xs-8 text-right">
                    <span> Resultado </span>
                    <h2 class="font-bold"><?= $aprovado ? "Aprovado" : "Reprovado" ?></h2>
                </div>
            </div>
        </div>
    </div>
</div>