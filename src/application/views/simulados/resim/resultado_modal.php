<div class="modal-dialog modal-form-content">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Resultado do Simulado</h4>
		</div>
		<div class="modal-body">
			<div class="row">
				<div class="col-lg-2">
                    <div class="widget meu-resultado-azul-bg text-center">
                        <div class="">
                            <h1 class="m-xs font-size-20"><?= $total ?></h1>
                            <span class="font-bold no-margins">
                                Questões
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="widget meu-resultado-azul-bg text-center">
                        <div class="">
                            <h1 class="m-xs font-size-20"><?= numero_brasileiro($nota) ?></h1>
                            <span class="font-bold no-margins">
                                Nota
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="widget meu-resultado-azul-bg text-center">
                        <div class="">
                            <h1 class="m-xs font-size-20"><?= $tempo_total ?></h1>
                            <span class="font-bold no-margins">
                                Tempo total
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget meu-resultado-azul-bg text-center">
                        <div class="">
                            <h1 class="m-xs font-size-20"><?= $ranking['resultado_ranking'] ?></h1>
                            <span class="font-bold no-margins">
                                Resultado
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="widget meu-resultado-azul-bg text-center">
                        <div class="">
                            <h1 class="m-xs font-size-20"><?= $ranking['posicao_ranking'] ?>º Lugar</h1>
                            <a class="font-bold no-margins" href="<?= get_ranking_simulado_url($simulado['sim_id'])?>" target="_blank">
                                Visualizar Ranking
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="widget meu-resultado-verde-bg text-center">
                        <h1 class="m-xs font-size-20">Sua nota no simulado: <?= numero_brasileiro($ranking['total_ranking']) ?> pontos</h1>
                    </div>
                </div>
            </div>
			<div class="row">
	         	<div class="table-responsive">
	                <table class="table table-striped" id="detalhe-resultado">
	                    <thead>
	                    <tr>
	                        <th>Disciplina</th>
	                        <th class="text-align-center">Questões</th>
	                        <th class="text-align-center">Acertos</th>
                            <?php if(($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']) || $ranking['ran_peso_erros']) : ?>
                                <th class="text-align-center">Erros</th>
                            <?php endif; ?>
                            <th class="text-align-center">Nota</th>
	                        <th class="text-align-center">Aproveitamento</th>
	                    </tr>
	                    </thead>
	                    <tbody>
	                    <?php foreach ($detalhes as $resultado) : ?>
	                    <tr>
	                        <td><?= $resultado['nome'] ?></td>
	                        <td class="text-align-center"><?= $resultado['qtde'] ?></td>
	                        <td class="text-align-center"><?= $resultado['acertos'] ?></td>
                            <?php if(($simulado['sim_pontos_negativos'] && $simulado['sim_peso_erros']) || $ranking['ran_peso_erros']) : ?>
                                <td class="text-align-center"><?= $resultado['erros'] ?></td>
                            <?php endif; ?>
                            <td class="text-align-center"><?= numero_brasileiro($resultado['nota']) ?></td>
	                        <td class="text-align-center"><?= porcentagem($resultado['acertos'] / $resultado['qtde'] * 100) ?></td>
	                    </tr>
	                    <?php endforeach; ?>
	                    </tbody>
	                </table>
	            </div>
	    	</div>
       	</div>
	</div>
</div>

<script>
$(function () {
    $('#detalhe-resultado').basictable();
});
</script>