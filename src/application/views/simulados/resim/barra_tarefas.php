<?php if($exibir_resultado) : ?>
    <div class="col-lg-12">
        <div id="detalhe" class="modal fade" aria-hidden="true">
            <?php $this->view(get_modal_detalhe_resultado_simulado_view_url()); ?>
        </div>
        <a class="btn btn-default" href="<?php echo get_imprimir_resultado_simulado_url($simulado, $tipo_resultado) ?>" target="_blank"><i class="fa fa-print"></i> Imprimir </a>
        <a class="btn btn-default" id="detalhe-btn" href="#detalhe" data-toggle="modal">Resultado</a>
        <a class="btn btn-default" id="filtro-btn"><i class="fa fa-filter"></i> Filtros</a>
        <div class="btn-group dropdown">
            <a data-toggle="dropdown" class="btn btn-default dropdown-toggle tooltipster" title="Alterar modo de visualização" aria-expanded="false"><i class="fa fa-eye"></i>  <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a class="tooltipster-right mrq" data-modo="<?= MODO_QUESTOES_PADRAO ?>" href="#" title="Modo Normal"><i class="fa fa-book"></i></a></li>
                <li><a class="tooltipster-right mrq" data-modo="<?= MODO_QUESTOES_COCKPIT ?>" href="#" title="Modo Cockpit"><i class="fa fa-exchange"></i></a></li>
            </ul>
        </div>
        <?php if($is_exibe_modal_resultado) : ?>
            <script>$(function() {$('#detalhe-btn').click();});</script>
        <?php endif; ?>
    </div>
    <div class="col-lg-12">
    <?php //if($check_filtros == 1) : ?>	
        <?php $this->load->view("simulados/resim/resim_filtro", ["tipo_resultado" => $tipo_resultado] ) ?>
    <?php //endif; ?>
    </div>
<?php else : ?>
    <div class="col-lg-12">
        <div class="col-lg-4">
            <a id="imprimir" class="btn btn-default" href="<?php echo get_imprimir_meus_simulados_html_url($simulado); ?>" target="_blank"><i class="fa fa-print"></i> Imprimir </a>
            <a id="aumentar-fonte" class="btn btn-default"><i class="fa fa-font"></i> + </a>
            <a id="diminuir-fonte" class="btn btn-default"><i class="fa fa-font"></i> - </a>
            <div class="btn-group dropdown">
                <a data-toggle="dropdown" class="btn btn-default dropdown-toggle tooltipster" title="Alterar modo de visualização" aria-expanded="false"><i class="fa fa-eye"></i>  <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a class="tooltipster-right mrq" data-modo="<?= MODO_QUESTOES_PADRAO ?>" href="#" title="Modo Normal"><i class="fa fa-book"></i></a></li>
                    <li><a class="tooltipster-right mrq" data-modo="<?= MODO_QUESTOES_COCKPIT ?>" href="#" title="Modo Cockpit"><i class="fa fa-exchange"></i></a></li>
                </ul>
            </div>
        </div>
        <?php if($is_usa_cockpit): ?>
            <div class="col-lg-6 text-left">
                Questão <strong><?php echo numero_inteiro($questao['numero']) ?></strong> de <strong><?php echo numero_inteiro($total) ?></strong>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>