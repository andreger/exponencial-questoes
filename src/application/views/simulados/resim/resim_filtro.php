<form method="post" action="<?= get_resolver_simulado_url($simulado_id) ?>">
	<div class="resim-filtro" style="display: none" id="resim-filtro">
		<?= form_radio("tipo_resultado", SIMULADO_QUESTAO_TODAS, $tipo_resultado && $tipo_resultado == SIMULADO_QUESTAO_TODAS, "id='simulado_questao_todas' class='tipo_resultado'"); ?> <label for="simulado_questao_todas"> Todas</label>
		<?= form_radio("tipo_resultado", SIMULADO_QUESTAO_NAO_RESOLVIDAS, $tipo_resultado && $tipo_resultado == SIMULADO_QUESTAO_NAO_RESOLVIDAS, "id='simulado_questao_nao_resolvidas' class='tipo_resultado'"); ?>  <label for="simulado_questao_nao_resolvidas"> Não resolvidas</label>
        <?= form_radio("tipo_resultado", SIMULADO_QUESTAO_RESOLVIDAS, $tipo_resultado && $tipo_resultado == SIMULADO_QUESTAO_RESOLVIDAS, "id='simulado_questao_resolvidas' class='tipo_resultado'"); ?>  <label for="simulado_questao_resolvidas"> Resolvidas</label>
        <?= form_radio("tipo_resultado", SIMULADO_QUESTAO_QUE_ACERTEI, $tipo_resultado && $tipo_resultado == SIMULADO_QUESTAO_QUE_ACERTEI, "id='simulado_questao_acertei' class='tipo_resultado'"); ?>  <label for="simulado_questao_acertei"> Que acertei</label>
        <?= form_radio("tipo_resultado", SIMULADO_QUESTAO_QUE_ERREI, $tipo_resultado && $tipo_resultado == SIMULADO_QUESTAO_QUE_ERREI, "id='simulado_questao_errei' class='tipo_resultado'"); ?>  <label for="simulado_questao_errei"> Que errei</label>
        <button type="submit" class="btn btn-default btn-outline" name="tipo_resultado_filtrar" value="1"><i class="fa fa-filter"></i> Filtrar</button>
    </div>
</form>