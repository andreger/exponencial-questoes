<link href='/questoes/assets-admin/css/plugins/daterangepicker2/daterangepicker.css' rel='stylesheet'>
<script src='/questoes/assets-admin/js/plugins/daterangepicker2/moment.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/daterangepicker2/daterangepicker.js'></script>

<div class="row text-center">
	<?php admin_cabecalho_pagina ("Simulados para Concurso"); ?>
</div>
	
<div id="ultimos-resultados" class="mtop-5 col-md-12">
	<div>
		<div class="ibox" >
			<div id="ultimos-resultados-header" class="ibox-title">
				<a id='collapse-id-filtro' data-toggle='collapse' data-parent='#faq' href='#ultimos-resultados-box' aria-expanded='true' aria-controls='ultimos-resultados-box'>
					<div style="font-size: 25px;font-weight: bold;">Meus últimos resultados<span style="font-size: 14px; vertical-align: middle;"> - <span class='mostrar-div'>Mostrar</span><span class='esconder-div'>Esconder</span></span></div>
				</a>
			</div>

			<div id="ultimos-resultados-box" class="ibox-content panel-collapse collapse in" role='tabpanel' aria-labelledby='ultimos-resultados-header'>
				<div>
					<form id="filtro-resultados">
						<div class="form-group">
							<label class="col-sm-2 control-label">Título</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="sim_nome" id="filtro-titulo" />
							</div>
							<div style="clear: both"></div>
						</div>

						<div class="form-group">
							<label class="col-sm-2 control-label">Período</label>
							<div class="col-sm-10">
								<input type="text" id="daterange" name="daterange" class="form-control m-b">
							</div>
							<div style="clear: both"></div>
						</div>
					</form>
				</div>

				<table class="table table-striped" id="tabela-simulados-resultados">
					<thead>
						<tr>
							<th>Simulado</th>
							<th>Nota</th>
							<th>Data</th>
							<th>Ranking</th>
							<th>Ranking Completo</th>
						</tr>
					</thead>
					<tbody>								
					</tbody>
				</table>
				<div id="tabela-simulados-resultados-footer" class="center"></div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<div class="listar-simulados mtop-5 col-md-12" style="padding: 0px 0px 20px 0px;">
<?php //if($check_filtros == 1) : ?>
<div>
	<div class="ibox" >
		<div style="font-size: 25px;font-weight: bold;" class="ibox-title">Simulados</div>

		<div class="ibox-content">
			<form method='post'>
				<div class="row">
					<div style="margin-bottom: 5px">
						<input id="search" class="search_filter form-control" type="text" placeholder="Digite para buscar nos simulados..." name="search_filter" value="<?= $search_filter ?>"/>
					</div>

					<div style="margin-bottom: 5px">
						<?php echo form_multiselect('ban_id', $bancas, $banca_selecionada, 'id="ban_id" class="form-control"'); ?>
					</div>

					<div style="margin-bottom: 5px">
						<?php echo form_multiselect('org_id', $orgaos, $orgao_selecionado, 'id="org_id" class="form-control"'); ?>
					</div>

				</div>
				<div class="row filtro-simulado">
					<label class="control-label">Listar </label> 
					<label class="radio-inline i-checks"><?php echo form_radio('check_simulado', TODAS, $check_simulado == TODAS, 'id="radio_todos"');?> Todos </label>
					<label class="radio-inline i-checks"><?php echo form_radio('check_simulado', QUE_ADQUIRI, $check_simulado == QUE_ADQUIRI, 'id="radio_adquiri"');?> Meus simulados </label>
					<label class="radio-inline i-checks"><?php echo form_radio('check_simulado', QUE_NAO_ADQUIRI, $check_simulado == QUE_NAO_ADQUIRI, 'id="radio_nao_adquiri"');?> Simulados à venda </label>
					<label class="radio-inline i-checks"><?php echo form_radio('check_simulado', GRATUITOS, $check_simulado == GRATUITOS, 'id="radio_gratuito"');?> Grátis </label>
				</div>
				<div class="row"> 
						<?php echo form_dropdown('ordenacao', $ordenacoes, $ordenacao_selecionada, 'id="ordenacao" class="form-control m-b"'); ?>
				</div>
				<div class="row" style="text-align: right;"> 
					<?php echo admin_botao_submit('Filtrar', 'filtrar'); ?>
					<?php echo admin_botao_submit('Limpar', 'limpar'); ?>
				</div>

			</form>
		</div>
	</div>
</div>
<?php //endif; ?>
	<div id="corpo-meus-simulados" class="row">
				
		<?php if($produto_id): ?>
			<div class="row">
				<?php ui_alerta("Estamos mostrando apenas os simulados vinculados ao produto acessado. Desejar procurar mais? <b><a href='".get_meus_simulados_url()."'>Clique aqui</a></b> para fazer uma nova pesquisa.", ALERTA_INFO, false); ?>
			</div>
		<?php endif; ?>

		<div id="lista-meus-simulados" class="col-md-12">
			<div class="row" style="font-size: 14px; margin: 10px 0 10px 15px"><?php echo "{$total} simulados encontrados" ?></div>
			
			<div id="simulados">
				<?php foreach ($simulados as $simulado) :
						if($simulado['sim_status'] != ATIVO) continue;	
				
						$produto = wc_get_product($simulado['prd_id']); 

						if(!$produto) {
							log_sq("error", "Meus Simulados:Nao foi possivel achar produto com ID {$simulado['prd_id']} que esta associado ao simulado {$simulado['sim_id']}");
							continue;
						}

						$has_simulado = tem_acesso_ao_simulado($simulado['sim_id'], get_current_user_id());
						$tem = $has_simulado ? "sim" : "nao";
						$gratuito = $produto->get_price() > 0 ? "gratuito-nao" : "gratuito-sim";
					?>
					<div class="widget-simulado col-xs-12 col-sm-6 col-md-4" data-sort="<?= $simulado['sim_id']; ?>" data-sim_id="<?= $simulado['sim_id']; ?>" data-id="<?= $produto->get_price(); ?>"  data-nome="<?= $simulado['sim_nome'] ?>">
						<div id="modal-produto-<?= $simulado['sim_id']; ?>" class="modal fade modal-produto" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h3 class="m-t-none m-b text-center">
											<?= get_tema_image_tag('sq-info.png', 22, 22, 'Informações') . ' ' . $produto->post->post_title ?>
										</h3>
									</div>
									<div class="modal-body">
										<?= $purifier->purify($produto->post->post_content); ?>
									</div>
								</div>
							</div>
						</div>
						<div id="simulado-<?= $simulado['sim_id']; ?>" class="col-xs-12 simulado <?= $tem; ?> <?= $gratuito ?> banca-<?= $simulado['ban_id']; ?> orgao-<?= $simulado['org_id']; ?>" style="padding: 0px">
							<div class="<?= is_visitante() ? 'widget-free' : ''?> widget bg-white col-md-12">
								<div class="row height-widget">
									<div class="col-xs-12 col-sm-2 col-md-3 padding-img-sim-md">
										<div class="simulado-img height-img-mobile">
											<a class="simulado-img-link-info" data-toggle="modal" href="#modal-produto-<?= $simulado['sim_id']; ?>">
												<?= get_tema_image_tag('sq-info.png', 22, 22, 'Informações') ?>
											</a>
											<img class="simulado-img-concurso" src="<?= get_simulado_img_link($simulado['sim_id']); ?>" alt="
											<?php echo substr($simulado['sim_nome'],0,70)?>"/>												
										</div>
									</div>
									<div class="col-xs-9 col-sm-7 col-md-6">
										<div class="height-espaco-sim" style="margin-bottom: 20px;">
											<div class="nome_simulado">
												<?= $simulado['sim_nome']?>
											</div>
											<div style="margin-top: 10px">
												<div class="nome-cargo text-left" style="font-size: 11px;"><b>Foco:</b> <?= $simulado['car_nome']; ?></div>
											</div>
											
											<?php if($url_edital = get_url_edital_simulado($simulado)) : ?>
											<div style="margin-top: 10px">
												<a href="<?= $url_edital?>" target="_blank">Análise do Edital do Concurso</a>
											</div>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-xs-3 col-sm-2 col-md-3 text-right total-questoes" data-toggle="tooltip" title="Quantidade total de questões, considerando todas as provas.">
										<?= is_null($simulado['total_questoes']) ? 0 : $simulado['total_questoes']; ?>
									</div>
								</div>

								<div class="row">
									<div class="col-xs-12">
										<?php if($has_simulado) : ?>
											<?php if(is_simulado_resolvido($simulado['sim_id'], get_current_user_id())) : ?>
												<a class="btn btn-resolver btn-resolver-novamente btn-rounded-expo btn-block text-align-center" data-sim_id="<?= $simulado['sim_id'] ?>" href="<?= get_resolver_simulado_url($simulado['sim_id'], false); ?>"><i class="fa fa-pencil"></i> Resolver Novamente</a>
											<?php else : ?>
												<a class="btn btn-resolver btn-primary btn-rounded-expo btn-block text-align-center" data-sim_id="<?= $simulado['sim_id'] ?>" href="<?= get_resolver_simulado_url($simulado['sim_id'], false); ?>"><i class="fa fa-pencil"></i> Resolver Agora</a>
											<?php endif ?>
											<a class="btn btn-azul btn-rounded-expo btn-block text-align-center" href="<?= get_ranking_simulado_url($simulado['sim_id'])?>"><i class="fa fa-trophy"></i> Ranking</a>
											<a class="btn btn-azul btn-rounded-expo btn-block text-align-center" href="<?= get_imprimir_meus_simulados_html_url($simulado); ?>" target="_blank"><i class="fa fa-print"></i> Imprimir</a>
											<a class="btn btn-azul btn-rounded-expo btn-block text-align-center" href="<?= get_imprimir_gabarito_simulado_url($simulado); ?>" target="_blank"><i class="fa fa-print"></i> Gabarito</a>
											<a class="btn btn-azul btn-rounded-expo btn-block text-align-center" href="<?= get_imprimir_cartao_resposta_simulado_url($simulado); ?>" target="_blank"><i class="fa fa-print"></i> Cartão Resposta</a>
											<a data-toggle="modal" data-target="#modal-simulados" class="btn btn-azul btn-informar btn-rounded-expo btn-block text-align-center sim-informar-resultado" href="#"><i class="fa fa-book"></i> Informar Resultado</a>
											<?php if(is_simulado_resolvido($simulado['sim_id'], get_current_user_id())) : ?>
												<a class="btn btn-azul btn-rounded-expo btn-block text-align-center" href="<?= get_revisar_simulado_url($simulado['sim_id']); ?>"><i class="fa fa-book"></i> Revisar Simulado</a>
											<?php else : ?>
												<a class="btn btn-azul btn-revisar btn-rounded-expo btn-block text-align-center" data-toggle="modal" href="#modal-revisar"><i class="fa fa-book"></i> Revisar Simulado</a>
											<?php endif ?>
										<?php else :?>
											
											<?php if(is_visitante()): ?>
												<a class="btn btn-azul btn-rounded-expo btn-block text-align-center" data-toggle="modal" data-target="#login-modal" data-redirect="<?= get_ranking_simulado_url($simulado['sim_id'], 0, 0, 0, FALSE)?>" href="#"><i class="fa fa-trophy"></i> Ranking</a>
											<?php else: ?>
												<a class="btn btn-azul btn-rounded-expo btn-block text-align-center" href="<?= get_ranking_simulado_url($simulado['sim_id'])?>"><i class="fa fa-trophy"></i> Ranking</a>
											<?php endif; ?>

											<?php if(tem_acesso(array(ADMINISTRADOR, PROFESSOR))) : ?>
											<a class="btn btn-azul btn-rounded-expo btn-block text-align-center" href="<?= get_imprimir_gabarito_simulado_url($simulado); ?>" target="_blank"><i class="fa fa-print"></i> Gabarito</a>
											<?php endif; ?>
											
											<?php 

												if(is_visitante()){
													$url = "#";
													$modal = "data-toggle='modal' data-target='#login-modal' data-redirect='" . urlencode($produto->get_permalink() . "?add-to-cart=" . $produto->id) ."' ";
												}else{
													$url = $produto->get_permalink() . "?add-to-cart=" . $produto->id; 
												}
											
											?>

											<a class='button-gratis-azul btn btn-azul btn-rounded-expo btn-block btn-comprar-simulado text-align-center' <?= $modal ?> href="<?= $url ?>" style="padding: 8px 0;">
												<?php $preco = is_pacote($produto) ? $produto->max_price : $produto->get_price(); ?>
												<?php $sem_desconto = $produto->get_regular_price(); ?>

												<div class="col-md-12" style="padding: 0 5px">
													
													<?php if($sem_desconto > $preco) : ?>
													<div class="curso-sem-desconto">De: <?= moeda($sem_desconto) ?></div>
													<?php endif ?>

													<?php if($preco > 0) : ?>
														<div class="curso-preco"><?= moeda($preco) ?></div>
													<?php else : ?>
														<div class="curso-preco">
														 	<img src="<?= get_tema_image_url('icone-aulagratis.png') ?>" width='20' alt='Aula Grátis' style="position: relative; left: -6px; margin-top: 10px;"> GRÁTIS</div>
													<?php endif ?>

													<?php if($preco > 0) : ?>
													<div class="curso-parcelas">ou 10x de <?= moeda($preco / 10) ?> sem juros</div>
													<?php endif ?>

													<div class="curso-acao">
														<?php if($preco > 0) : ?>
															<img src="<?= get_tema_image_url('icone-compra.png') ?>" width='20' alt='Comprar' style="position: relative; left: -6px;"> Comprar
														<?php else : ?>
															Adquirir
														<?php endif; ?>
													</div>
												</div>
												<div style="clear:both" class="clear"></div>
											
												
											</a>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
			<?php echo $links; ?>
		</div>	
	
</div>
<div id="modal-simulados" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content"><div class="modal-carregando"><?= loading_img() ?> Carregando...</div></div>
	</div>
</div>

<?php $this->view('modal/aviso_revisar'); ?>

<a style="display:none" id="link-modal-assine-sq" data-toggle="modal" href="#modal-assine-sq" class="btn btn-sm btn-primary m-t-n-xs"></a>
<div id="modal-assine-sq" class="modal fade" aria-hidden="true">
	<?php $this->view(get_modal_assine_sq_view_url()); ?>
</div>

</div>

<script>

$(function() {

	var inf_res_ajax = null;
	var is_waiting = false;
	
	$('.sim-informar-resultado').click(function(){

		var id = $(this).closest('.widget-simulado').data('sim_id');
		if(is_waiting == true && inf_res_ajax != null){
			inf_res_ajax.abort();
		}
		is_waiting = true;
		inf_res_ajax = $.ajax({
			url: '<?= get_modal_resultados_simulados_url(); ?>/'+id,
			method: 'GET',
			cache: false
		}).done(function(html) {
			$('#modal-simulados .modal-content').html( html );
			is_waiting = false;
		});

	});

	$('#modal-simulados').on('hidden.bs.modal', function () {
		if(is_waiting == true && inf_res_ajax != null){
			inf_res_ajax.abort();
		}
	 	$(this).removeData('bs.modal').find(".modal-content").empty();
		$('#modal-simulados .modal-content').html( "<div class='modal-carregando'><?= loading_img() ?> Carregando...</div>" );
	});

	$('.btn-revisar').click(function() {
		var id = $(this).closest('.widget-simulado').data('sim_id');
		$('#modal-revisar').data("sim_id", id);
	});

	$('#ban_id').chosen({
		disable_search_threshold: 10,
		no_results_text: "Nenhuma banca com:",
		placeholder_text_multiple: "Selecione algumas bancas..."
	});
	
	$('#org_id').chosen({
		disable_search_threshold: 10,
		no_results_text: "Nenhuma instituição com:",
		placeholder_text_multiple: "Selecione algumas instituições..."
	});

	$("#daterange").daterangepicker({
	 	"locale": 
	 	{
	        "format": "DD/MM/YYYY",
	        "separator": " - ",
	        "applyLabel": "Filtrar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "De",
	        "toLabel": "Até",
	        "customRangeLabel": "Personalizado",
	        "weekLabel": "S",
	        "daysOfWeek": ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],
	        "monthNames": ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],
	        "firstDay": 1,
	    },
	    "startDate": '<?= $inicio ?>',
		"endDate": '<?= $fim ?>',
	    "alwaysShowCalendars": true,
	    ranges: {
           'Hoje': [moment(), moment()],
           'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Últimos 7 Dias': [moment().subtract(6, 'days'), moment()],
           'Últimos 30 Dias': [moment().subtract(29, 'days'), moment()],
           'Este Mês': [moment().startOf('month'), moment().endOf('month')],
           'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
           'Este Ano': [moment().startOf('year'), moment().endOf('year')]
        }
	});

	$('#daterange').on('apply.daterangepicker', function(ev, picker) {
		filtrar_resultados(0, 4);
	});

	$('#filtro-titulo').keyup(function() {
		filtrar_resultados(0, 4);
	});

	$('body').on('click', '.simulado-resultado-mais', function(e) {
		e.preventDefault();
		$(this).closest('tr').remove();
		offset = $(this).data('offset');
		filtrar_resultados(offset, 10);
	});

	$(".btn-resolver").click(function(e){
		var sim_id = $(this).data('sim_id');
		Cookies.remove('cron_sim_' + sim_id);
		Cookies.remove('resp_sim_' + sim_id);
	});

	filtrar_resultados(0, 4);

});

</script>