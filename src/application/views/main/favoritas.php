<div class="row text-center">
	<?php admin_cabecalho_pagina ("Questões Favoritas para Concurso"); ?>
</div>

<?php exibir_mensagem(); ?>

<div class="listar-favoritas">
    <div class="row">
    	<div class="col-md-12">
			<div class="ibox-content">
				<div style="margin: 20px 0">
					<input id="search-favoritas" class="search_filter form-control" type="text" placeholder="Digite para buscar nas disciplinas..." name="search_filter">
				</div>

				<table class="table favoritas-tabela">
					<thead>
					<tr>
						<th>Disciplina</th>
						<th class="mobile-hide">Questões</th>
						<th class="mobile-hide">Resultado</th>
					</tr>
					</thead>
					<tbody>
					<?php foreach ($disciplinas as $disciplina) : ?>
					<tr class="">
						<td class="nome_disciplina"><a href="<?php echo get_resolver_favoritas_url($disciplina['dis_id']) ?>"><?php echo $disciplina['dis_nome']?></a></td>
						<td class="mobile-hide"><?php echo $disciplina['total_questoes']?> </td>
						<td class="mobile-hide">
							<div class="progress progress-bar-default" title="<?php echo get_questoes_favoritas_tooltip($disciplina) ?>">
                                <div style="width: <?php echo $disciplina['total_questoes'] ? $disciplina['questoes_certas']/$disciplina['total_questoes']*100 : 0 ?>%" class="progress-bar progress-bar"></div>
                                <div style="width: <?php echo $disciplina['total_questoes'] ? $disciplina['questoes_erradas']/$disciplina['total_questoes']*100 : 0 ?>%" class="progress-bar progress-bar-danger"></div>
                            </div>
						</td>
					</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
				<?php if(count($disciplinas) == 0) : ?>
					<div>Você não possui questões favoritas.</div>
				<?php endif; ?>
			</div>
		</div>	
	</div>
</div>

<script>
$(document).ready(function() {
	
	$("#search-favoritas").change( function () {
		var filter = $(this).val(); // get the value of the input, which we filter on
		if (filter) {
			$(".favoritas-tabela").find(".nome_disciplina:not(:Contains(" + filter + "))").parent().hide();
			$(".favoritas-tabela").find(".nome_disciplina:Contains(" + filter + ")").parent().show();
		} else {
			$(".favoritas-tabela").find("tr").show();
		}
	}).keyup( function () {
		// fire the above change event after every letter
		$(this).change();
	});
	
	// Case-Insensitive Contains
	$.expr[':'].Contains = function(a,i,m){
	    return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
	};
});
</script>