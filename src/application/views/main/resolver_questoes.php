<?php
$titulo = "Resolver Questões";
//echo implode(', ', array_column($questoes, 'que_id'));
if(isset($is_simulado)) $titulo = $simulado['sim_nome'];
if(isset($is_caderno)) $titulo = $caderno['cad_nome'];
if(isset($is_favoritas)) $titulo = "Resolver Favoritas";
if(isset($is_tipo_questao)) $titulo = $title;
//$has_filtro = has_filtro_selecionado();
?>

<div class="row text-center">
	<?php admin_cabecalho_pagina ( $titulo ); ?>
</div>

<div id="questoes-principal">
    <?php exibir_mensagem(); ?>

    <?php if($questoes && ($has_filtro || isset($caderno_id)) && AcessoGrupoHelper::copiar_ids_questoes()) :?>
    	<input type="text" value="" id="clipboard_input" style="position: relative;top: -1000px;">
    <?php endif ?>

	<?php if($is_usa_barra_tec): ?>
		<?php $this->view('questoes/resolver/barra_tec'); ?>
	<?php else: ?>
		<?php $this->view('questoes/resolver/barra_padrao'); ?>
	<?php endif; ?>

    <div class="questoes">
    	<?php if(isset($caderno)) { echo get_produtos_associados_a_caderno_msg($caderno['cad_id']); } ?>
    	<?php if(isset($erro)) { ui_alerta($erro, ALERTA_ERRO); }?>
    	<?php if(isset($successo)) { ui_alerta($successo, ALERTA_SUCESSO); }?>
		
		<?php if($is_usa_cockpit): ?>
			<?php $this->view('questoes/principal/questoes_cockpit'); ?>
		<?php else: ?>
			<?php $this->view('questoes/principal/questoes_padrao'); ?>
		<?php endif; ?>

    </div>
</div>

<?php $this->view("questoes/filtro/filtro_carregando") ?>

<?php if(!$is_usa_cockpit): ?>
	<?php $this->view('questoes/barra_tarefas/anotacoes/modal') ?>
	<?php $this->view('questoes/barra_tarefas/questao/compartilhar_modal') ?>
<?php endif; ?>

<a style="display:none" id="link-modal-assine-sq" data-toggle="modal" href="#modal-assine-sq" class="btn btn-sm btn-primary m-t-n-xs"></a>
<div id="modal-assine-sq" class="modal fade" aria-hidden="true">
	<?php $this->view(get_modal_assine_sq_view_url()); ?>
</div>


<div id="modal-form-filter" class="modal fade" aria-hidden="true">
	<?php if(!$is_filtro_bloqueado): ?>
		<?php $this->view("questoes/filtro/filtro_modal"); ?>
	<?php else: ?>
        <?php $this->view("questoes/filtro/filtro_bloqueado") ?>
	<?php endif; ?>
</div>

<div id="modal-delete-from-filter" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div>Você deseja remover <b>TODAS</b> as questões listadas do caderno?</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary" id="confirmar-remover-selecao">Confirmar</button>
			</div>
		</div>
	</div>
</div>

<?php if($questoes && ($has_filtro || isset($caderno_id)) && AcessoGrupoHelper::copiar_ids_questoes()) :?>
	<?php $this->view('questoes/barra_tarefas/questao/modal_copiar_ids') ?>
<?php endif; ?>

<?php $data = ['is_modal' => TRUE, 'is_ajax' => FALSE, 'has_filtro' => $has_filtro]?>
<?php $this->view(get_questoes_barra_tarefas_adicionar_a_caderno_view_url(), $data); ?>

<script>
$(function() {

	$(document).on("click", "#remover-selecao", function() {
		$("#modal-delete-from-filter").modal('show');
	});

	$(document).on("click", "#confirmar-remover-selecao", function() {
		$("#remove-questoes-caderno").val('S');
		$('#filtro-submit').click();
		$("#modal-delete-from-filter").modal('hide');
	});

	$("#limpar-botao").click(function() {
		$('#que_id').val("");
		$('#que_qcon_id').val("");
		$('#palavra_chave').val("");
		$('#data_inicio').val("");
		$('#data_fim').val("");
		$('.multiselect').select2("val", "");
		$('.i-checks input:radio').iCheck('uncheck');
		$('.i-checks input:checkbox').iCheck('uncheck');
		$('#filtro-submit').click();
	});

	var tamanho_fonte = <?= get_tamanho_fonte() ?>

	$('#aumentar-fonte').click(function () {
		if(tamanho_fonte <= 40) {
			tamanho_fonte++;
		}

		alterar_tamanho_fonte(tamanho_fonte);
	});

	$('#diminuir-fonte').click(function () {
		if(tamanho_fonte >= 10) {
			tamanho_fonte--;
		}

		alterar_tamanho_fonte(tamanho_fonte);
	});

	alterar_tamanho_fonte(tamanho_fonte);

	$('#filtrar').click(function(){
		dis_ids_name = 'dis_ids';
		ass_ids_name = 'ass_ids';
	});

	$('.alternativa').click(function() {
		$(this).toggleClass('opcao-tachada');
	});

	$('#adicionar-a-caderno-btn').click(function() {
		atualizar_categorias('#caderno_id');
	});

	$('#timer-pausar').click(function() {
		$('#timer-pausar').hide();
		$('#timer-continuar').show();

		pausado = true;
	});

	$('#timer-continuar').click(function() {
		$('#timer-continuar').hide();
		$('#timer-pausar').show();

		pausado = false;
	});

	$('.favorito').click(function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		var classe = $(this).find('i',0).attr('class');

		if(classe.includes("fa fa-star-o")) {
			$(this).find('i',0).attr('class', 'fa fa-star');
			
			//Favoritar também faz acompanhar
			var bell_class = $('#acompanhar-'+id).attr('class');
			if(bell_class.includes('fa fa-bell-slash-o')){
				$('#acompanhar-'+id).attr('class', 'fa fa-bell');
			}
		}
		else {
			$(this).find('i',0).attr('class', 'fa fa-star-o');
		}

		$.get('/questoes/questoes_xhr/marcar_desmarcar_favorito/'+id+'/<?= QUESTAO_FAVORITA ?>');
	});

	$('.acompanhar').click(function(e) {
		e.preventDefault();
		var id = $(this).data('id');
		var classe = $(this).find('i',0).attr('class');

		if(classe.includes("fa fa-bell-slash-o")) {
			$(this).find('i',0).attr('class', 'fa fa-bell');
		}
		else {
			$(this).find('i',0).attr('class', 'fa fa-bell-slash-o');
		}

		$.get('/questoes/questoes_xhr/marcar_desmarcar_favorito/'+id+'/<?= QUESTAO_ACOMPANHADA ?>');
	});

	$('.btn-edicao-rapida').click(function(e) {
		e.preventDefault();
		var questao_id = '#edicao-rapida-' + $(this).data('id');
		$(questao_id).toggle();

		$(questao_id).html("Carregando...");

		$.get('/questoes/questoes_xhr/carregar_edicao_rapida/'+$(this).data('id'), function(data) {
			$(questao_id).html(data);
		});
	});

	$('.btn-remover-formatacao').click(function(e){
		e.preventDefault();
		var id = $(this).data('id');

		$.ajax({
			url: "<?php echo get_ajax_main_remover_formatacao_alternativas_url(); ?>",
			method: "POST",
			cache: false,
			data: { id: id }
		})
		.done(function( msg ) {
			alert(msg);
		});
	});

	$("#limpar-botao").click(function() {
		$('#que_id').val("");
		$('#que_qcon_id').val("");
		$('#palavra_chave').val("");
		$('#data_inicio').val("");
		$('#data_fim').val("");
		$('.multiselect').select2("val", "");
		$('.i-checks input:radio').iCheck('uncheck');
		$('.i-checks input:checkbox').iCheck('uncheck');
		$('#filtro-submit').click();
	});

	$('#qpp').change(function () {
		<?php if(isset($is_caderno)) : ?>
		url = "<?= get_alterar_questoes_por_pagina_caderno_url() ?>" + $('#qpp option:selected').text() + "/<?= $caderno['cad_id'] ?>/<?= $offset ?><?= $query_string?:"" ?>";
		<?php elseif(isset($is_favoritas)) : ?>
		url = "<?= get_alterar_questoes_por_pagina_url() ?>/" + $('#qpp option:selected').text() + "/<?= $offset ?>/<?= $is_favoritas ?><?= $query_string?:"" ?>";
		<?php else: ?>
		url = "<?= get_alterar_questoes_por_pagina_url() ?>/" + $('#qpp option:selected').text() + "/<?= $offset ?>/0/<?= urlencode( urlencode( $url ) ) ?>/<?= $query_string?:"" ?>";
		<?php endif; ?>
		location.href = url
	});

	$('.mrq').click(function (e) {
		e.preventDefault();
		var modo = $(this).data('modo');
		<?php if(isset($is_caderno)) : ?>
			url = "/questoes/cadernos/alterar_modo_resolver_questoes/" + modo + "/<?= $caderno['cad_id'] ?>/<?= $page - 1 ?><?= $_SERVER['QUERY_STRING'] ? ('?'.$_SERVER['QUERY_STRING']) : '' ?>";	
		<?php elseif(isset($is_simulado)) : ?>
			url = "/questoes/simulados/alterar_modo_resolver_questoes/" + modo + "/<?= $simulado['sim_id'] ?>/<?= $page - 1 ?><?= $_SERVER['QUERY_STRING'] ? ('?'.$_SERVER['QUERY_STRING']) : '' ?>";
		<?php elseif($is_favoritas): ?>
			url = "/questoes/main/alterar_modo_resolver_questoes/" + modo + "/<?= $page - 1 ?>/<?= $is_favoritas ?><?= $_SERVER['QUERY_STRING'] ? ('?'.$_SERVER['QUERY_STRING']) : '' ?>";
		<?php else : ?>
			url = "/questoes/main/alterar_modo_resolver_questoes/" + modo + "/<?= $page - 1 ?>/0/<?= urlencode( urlencode( $url ) ) ?>/<?= $_SERVER['QUERY_STRING'] ? ('?'.$_SERVER['QUERY_STRING']) : '' ?>";
		<?php endif; ?>	
		location.href = url;
	});

	$('#modal-comentario-questao').on('hidden.bs.modal', function () {
		$(this).removeData('bs.modal').find(".modal-content").empty();
	});
	$('.i-checks').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});

	$('.responder').click(function (event) {
		event.preventDefault();

		ga('send', 'event', 'SQ', 'click', 'respostaSQ');

		var id = $(this).data('id');
		$("#questao_" + id + " .msg-resposta").html("<span class='resposta-mensagem resposta-carregando'><img src='/wp-content/themes/academy/images/mensagem-conferindo.png'>Conferindo resposta...</span>");
		var texto = 'input[name=opcao_questao_' + id + ']:checked';
		var selecao = $(texto).val();
		var caderno_id = $(this).data('caderno-id');
		var simulado_id = $(this).data('simulado-id');

		if(selecao) {
			$.ajax({
				url: "<?php echo get_ajax_main_responder_url();?>",
				method: "POST",
				cache: false,
				data: {
					id: id,
					selecao : selecao,
					caderno_id: caderno_id,
					simulado_id: simulado_id
				}
			})
			.done(function( msg ) {
				$("#questao_" + id + " .msg-resposta").html(msg);
				ver_dificuldade(id);
			});
		}
		else {
			$("#questao_" + id + " .msg-resposta").html("<span class='resposta-mensagem resposta-aviso'><img src='/wp-content/themes/academy/images/mensagem-escolha.png'>Você precisa escolher uma opção</span>");
		}

	});

   	<?php if(isset($array_ids)) : ?>
		<?php foreach ($array_ids as $id => $nome) : ?>
       		$("<?php echo $id; ?>").select2({
	           	placeholder: "<?php echo $nome ?>"
       		});
       	<?php endforeach; ?>
   	<?php endif;?>

   	$('#dis_ids').change("select2:open", function () {
		var assIds = $('#ass_ids').val();
   		var str = "";
   		$('#ass_ids').html("<option value='' disabled >Carregando...</option>");

		var ids_a = [];
       	$('#dis_ids option:selected').each(function(i, selected){
       		ids_a[i] = $(selected).val();
       	});

       	var ids = ids_a.join('-');
		if(ids){
			$.get("<?= base_url('/main/ajax_get_assuntos/') ?>/" + ids, function(data) {
				/*$.each(data,function(index,value){
					var sel = ($.inArray(data[index].id, assIds) != -1) ? ' selected="selected"' : '';
					str += '<option value="' + data[index].id+ '" ' + sel + ' >'+data[index].text+'</option>';
				});
				$('#ass_ids').html(str);*/
				adicionar_itens($('#ass_ids'), data, assIds);
			}, 'json');
		}
    });

    $('.questao-opcoes input[type=radio]').on('ifClicked', function(event) {
    	var id = event.target.name.split('_').pop();
    	var value = event.target.value;

	   	$.get('/questoes/xhr/salvar_resposta_marcada/' + id + '/' + value);
    });

	$(".link-anotacoes-listagem").click(function(e){

		var questao_id = $(this).data("id");

		$('#modal-anotacoes-questao .modal-body').html("Carregando...");

		$.get('/questoes/questoes_xhr/anotacoes_questao/'+questao_id, function(data) {
			if(data){
				$('#modal-anotacoes-questao').html(data);
			}else{
				$('#modal-anotacoes-questao .modal-body').html("Erro no servidor. Tente novamente.");
			}
		});

	});

});

<?php if(isset($caderno_id)) : ?>

var pausado = false;
var tempo = <?= $caderno['cad_tempo_gasto'] ? $caderno['cad_tempo_gasto'] : 0 ?>;
var cron_cad_key = 'cron_cad_<?= $caderno_id ?>';
if(Cookies.get(cron_cad_key) == undefined){
	Cookies.set(cron_cad_key, '0');
}
tempo += parseInt(Cookies.get(cron_cad_key));
$('#timer').html(formatar_tempo(tempo));


setInterval(function() {
	incrementar_tempo_gasto(false);
}, <?php echo TEMPO_GASTO_DEFAULT ?> * 1000);

var timerId = setInterval(function() {
	if(!pausado) {
		tempo++;
		Cookies.set(cron_cad_key, parseInt(Cookies.get(cron_cad_key)) + 1);
		$('#tempo').val(tempo);
		$('#timer').html(formatar_tempo(tempo));
	}
}, 1000);

function formatar_tempo(segundos) {
	return new Date(segundos * 1000).toISOString().substr(11, 8);
}

function incrementar_tempo_gasto(isPadrao)
{
	if(!pausado) {
		if(isPadrao){
	    	$.get("/questoes/main/xhr_incrementar_tempo_gasto_caderno/<?php echo $caderno_id ?>");
		}else{
			$.get("/questoes/main/xhr_incrementar_tempo_gasto_caderno/<?php echo $caderno_id ?>/"+Cookies.get(cron_cad_key));
		}
		Cookies.set(cron_cad_key, '0');
	}
}

/*$('.incrementar_tempo').click(function(e){
	if(!pausado) {
		$(this).attr("href", "/questoes/cadernos/trocar_pagina/<?= $caderno_id ?>/" + tempoGasto + '?redirect=' + encodeURI($(this).attr("href")));
	}
	//incrementar_tempo_gasto(false);
});*/

<?php endif; ?>

function ver_dificuldade(id) {
	$.ajax({
		url: "<?php echo get_ajax_main_questao_ver_dificuldade_url(); ?>" + id,
		method: "POST",
		cache: false,
		data: {
			id: id,
		}
	})
	.done(function( msg ) {
		$("#questao_" + id + " .dificuldade").html(msg);
	});
}

function dificuldade(id) {
	$("#questao_" + id + " .questao-dificuldade").show();
	$("#questao_" + id + " .questao-ver-dificuldade").hide();
}

function alterar_tamanho_fonte(tamanho_fonte) {
	$('.texto-questao').css('font-size', tamanho_fonte + 'px');
	$('#aumentar-fonte').prop('title', 'Aumentar tamanho da fonte para ' + (tamanho_fonte + 1) + ' px');
	$('#diminuir-fonte').prop('title', 'Diminuir tamanho da fonte para ' + (tamanho_fonte - 1) + ' px');
	$.get('/questoes/xhr/salvar_tamanho_fonte/'+tamanho_fonte);
	gerar_tooltipster();
}

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
<?php $this->view('questoes/barra_inferior/barra_inferior_scripts'); ?>