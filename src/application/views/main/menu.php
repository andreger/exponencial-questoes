<div id="login-modal-container"></div>

<script>
$(function() {
    var modalCarregada = false;

	$('#side-menu a').click(function() {

		$.get('/questoes/xhr/zerar_marcadas');
	});

	$('a[data-target="#login-modal"]').click(function() {
        var disparador = $(this);

	    if(!modalCarregada) {
            $.get('/questoes/login/get_login_box_html', function (data) {
                $("#login-modal-container").html(data);
                modalCarregada = true;
                disparador.click();
            });
        }
    });
});
</script>
