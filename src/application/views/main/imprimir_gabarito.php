<?= get_pagina_gabarito("Gabarito - " . $simulado['sim_nome'], $gabarito) ?>

<style>
@media only print {
	.navbar-static-side { display:none }
	.p-nav-sq { display:none }
	.mb-main { margin-top: 0; }
	#page-wrapper { margin:0; }
}
</style>

<script>window.print()</script>