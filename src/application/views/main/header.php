<?php
KLoader::helper("HeadHelper");
?>
<!DOCTYPE html>
<html lang="pt-BR" prefix="og: http://ogp.me/ns#" class="wf-opensans-n4-active wf-opensans-i4-active wf-opensans-n6-active wf-active">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php if(!is_producao()) : ?>
        <meta name="robots" content="noindex, nofollow" />
    <?php endif ?>

	<?php $meta_title = (isset($title) ? $title : "Sistema de Questões")  . " | Exponencial Concursos" ?>

    <title><?= $meta_title ?></title>
	<!--<META http-equiv="refresh" content="<?= (MINUTE_IN_SECONDS * 10) + 1; ?>;URL=<?= base_url('main/resolver_questoes'); ?>">-->
    <meta property="fb:app_id" content="842077389244844" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?= $meta_title ?>" />
    <meta property="og:url" content="<?= current_url() ?>" />
    <meta property="og:site_name" content="Exponencial Concursos" />
    <meta property="og:image" content="https://www.exponencialconcursos.com.br/wp-content/themes/academy/images/generico-sq.jpg" />

	<link rel="shortcut icon" href="<?= base_url('/favicon.ico') ?>" />

    <?= HeadHelper::get_canonical() ?>

    <?php $this->view('main/comum/estilo.html.php') ?>
    <?php $this->view('main/comum/scripts.html.php') ?>

    <?php KLoader::view("plugins/google_analytics/ga") ?>

    <?= gtm_pagina() ?>

    <?php KLoader::view("plugins/google_tag_manager/gtm_head") ?>
    <?php KLoader::view("plugins/google_ads/google_ads") ?>
    <?php KLoader::view("plugins/facebook_pixel/facebook_pixel") ?>

    <?php
    /**
     * Essa TAG vazia é necessária porque o plugin BIOEP busca essa tag no head para inserir um novo 'style' antes.
     */
    ?>
    <style></style>

</head>
<body class="mb-main">
	<?php KLoader::view("plugins/facebook_sdk/facebook_sdk") ?>
    <?php KLoader::view("plugins/google_tag_manager/gtm_body") ?>

    <div id="wrapper">
        <?php $this->load->view(get_main_topbar_view_url()) ?>
    	<?php $this->load->view(get_main_menu_view_url()) ?>
    	 <div id="page-wrapper" class="container white-bg dashbard-1">





