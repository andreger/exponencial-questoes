<div class='row'>
	<img width='100%' src='/wp-content/themes/academy/images/cabecalho-cartao.jpg'>
</div>

<div class='header-azul'>
	<?php admin_cabecalho_pagina ("Cartão de Resposta - " .  $simulado['sim_nome']); ?>
</div>

<div class="ibox" style="margin-top: 20px">
	<div class="ibox-content">
		<div class="row">
			<table style="width:100%; border: 1px solid black; border-collapse: collapse;" >
			<?php for($i = 0; $i < count($linhas); $i++) : ?>
				<tr style="border: 1px solid black; border-collapse: collapse;">
					<td style="width: 50px; padding:3px 10px; border: 1px solid black; border-collapse: collapse;">
						<?= $linhas[$i]['num_esq'] ?> 
					</td>	
					
					<td style="width: 250px; padding:3px 10px; border: 1px solid black; border-collapse: collapse;">
						<?= $linhas[$i]['opcoes_esq'] ?> 
					</td>
					
					<td style="width: 50px; padding:3px 10px; border: 1px solid black; border-collapse: collapse;">
						<?= $linhas[$i]['num_cen'] ?> 
					</td>	

					<td style="width: 250px; padding:3px 10px; border: 1px solid black; border-collapse: collapse;">
						<?= $linhas[$i]['opcoes_cen'] ?> 
					</td>		
					
					<td style="width: 50px; padding:3px 10px; border: 1px solid black; border-collapse: collapse;">
						<?= $linhas[$i]['num_dir'] ?> 
					</td>	

					<td style="width: 250px; padding:3px 10px; border: 1px solid black; border-collapse: collapse;">
						<?= $linhas[$i]['opcoes_dir'] ?> 
					</td>			
				</tr>
			<?php endfor; ?>
			</table>
		</div>
	</div>
</div>

<style>
@media only print {
	.navbar-static-side { display:none }
	.p-nav-sq { display:none }
	.mb-main { margin-top: 0; }
	#page-wrapper { margin:0; }
}
</style>

<script>window.print()</script>