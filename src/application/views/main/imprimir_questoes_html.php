<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
 
 	<style>   
 	.disciplina-barra {
		text-align:center; 
		font-weight: bold; 
		background-color: rgb(255, 0, 0); 
		color: rgb(255, 255, 255);
	}
 	
 	
    @media print {

	.disciplina-barra {
		text-align:center; 
		font-weight: bold; 
		background-color: rgb(255, 0, 0); 
		color: rgb(255, 255, 255);
		-webkit-print-color-adjust: exact; 
	}

	.pdf-body img {
		max-height: 800px !important;
	}


	}
	</style>
</head>

<body>

<script>
window.print();
</script>

<?= $conteudo ?>

<?= get_pagina_gabarito("Gabarito", $gabarito) ?>

<?= $comentarios ?? "" ?>
</body>

</html>