<link href='/questoes/assets-admin/css/plugins/c3/c3.min.css' rel='stylesheet'>+
<script src='/questoes/assets-admin/js/plugins/d3/d3.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/c3/c3.min.js'></script>

<div style="text-align: center;">
    <h4>Essa questão foi respondida <?= $total_respostas ?> vezes</h4>
    <div class="row">
        <div class="col-md-6">
            <div id="grafico-estatisticas-questao-acertos-erros"></div>
            <h5>Quantidade de acertos e erros</h5>
        </div>
        <div class="col-md-6">
            <div id="grafico-estatisticas-questao-alternativas"></div>
            <h5>Alternativas mais respondidas</h5>
        </div>
    </div>
</div>
<script>
    
    c3.generate({
        bindto: '#notificar-<?= $questao_id ?> #grafico-estatisticas-questao-acertos-erros',
        size: {
            height: 240,
            width: 240
        },
        data: {
            columns: <?= $que_respostas ?>
            , type: 'pie'
        },
        legend: {
            show: false
        }
    });
    
    c3.generate({
        bindto: '#notificar-<?= $questao_id ?> #grafico-estatisticas-questao-alternativas',
        size: {
            height: 240,
            width: 240
        },
        data: {
            columns: <?= $que_alternativas ?>
            , type: 'pie'
        },
        legend: {
            show: false
        }
    });

</script>