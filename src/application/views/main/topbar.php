<div class="p-nav-sq bg-white">
<div class="bg-topbar">
<div class="container">
	<div class="row">
	
	<div id="topbar-col1" class="mtop-1 col-xs-7 col-sm-4 col-md-4 col-lg-3 pl-topbar">
		<a href="/">
		    <img src="/wp-content/themes/academy/images/logo2018.png" height="40" alt="Exponencial Concursos">
		</a>
		<a href="/questoes/main/resolver_questoes">
		 <img src="/wp-content/themes/academy/images/logo-sq-colorido.png" height="38" alt="Sistema de Questões - SQ" class="img-logo-margin">
		</a>	
	</div>
	<div id="topbar-col2" class="mtop-2 col-xs-5 col-sm-8 col-md-8 col-lg-9 topbar-right">
		<span class="mobile-hide ipad-hide navbar-bemvindo">			
			<?php 
			if(is_visitante()) {
				echo "Bem-vindo(a). Bons estudos!";
			}
			else {
				echo "Bem-vindo(a), " . get_usuario_nome_exibicao(get_current_user_id()) . ". Bons estudos!";
			}
			?>
		</span>

		<?php if(is_usuario_logado()): ?>
		<span class="mgl-1 mgr-1">
			<?php KLoader::view('header/barra_superior/notificacoes/notificacoes') ?>
		</span>
		<?php endif; ?>
		
		<!-- 
		<?php if(!tem_acesso(array(ADMINISTRADOR, COORDENADOR, PROFESSOR, CONSULTOR, REVISOR, ASSINANTE_SQ))) : ?>
		<span class="mgl-1 mgr-1">
			<a href="/sistema-de-questoes#planos">
				<?= get_tema_image_tag('plano-assinatura1.png', null, null, 'Planos de Assinatura', 'vertical-bottom') ?> <strong class="mobile-hide">Planos de Assinatura</strong>
			</a>
		</span>
		<?php endif ?>
		 -->

		<span class="mgl-1 mgr-1">
			<a href="/">
				<?= get_tema_image_tag('sq-ir.png', null, null, 'Ir para site', 'vertical-bottom') ?> <span class="mobile-hide"><strong>Ir para site</strong></span>
			</a>
		</span>
		
		<?php if(tem_acesso(array(
				ADMINISTRADOR, PROFESSOR, CONSULTOR, REVISOR
				))) : ?>
		
			<?php if(!is_questoes_admin()) : ?>
			<span class="mgl-1 mgr-1">
				<a href="<?php echo base_url('admin') ?>">
					<?= get_tema_image_tag('sq-admin.png', null, null, 'Administração') ?> <span class="mobile-hide"><strong>Administração</strong></span>
				</a>
			</span>
			<?php endif; ?>
			
			<?php if(is_questoes_admin()) : ?>
			<span class="mgl-1 mgr-1">
				<a href="<?php echo base_url('main/resolver_questoes') ?>">
					<i class="fa fa-dot-circle-o"></i> <span class="mobile-hide"><strong>Sistema de Questões</strong></span>
				</a>
			</span>
			<?php endif; ?>
		
		<?php endif;?>
		
		<?php if(is_usuario_logado()) : ?>		
		<span class="mgl-1 mgr-1">
			<a href="<?php echo base_url('login/logout') ?>">
				<?= get_tema_image_tag('sq-sair.png', null, null, 'Sair') ?> <span class="mobile-hide"><strong>Sair</strong></span>
			</a>
		</span>
		<?php else : ?>
		<span class="mgl-1 mgr-1">
			<a data-toggle="modal" data-target="#login-modal" data-redirect="<?= get_url_atual()?>"><?= get_tema_image_tag('sq-entrar.png', null, null, 'Entrar') ?>
				<span class="mobile-hide"><strong>Entrar</strong></span>
			</a>
		</span>
		<?php endif ?>
	</div>
</div>
</div>
</div>
<!--<div class="container-fluid bg-white">	
<div class="container">	
	<div class="nav-header bg-white">
                <div class="mtop-3 dropdown profile-element"> 
	                <img src="/wp-content/themes/academy/images/logo-sq-colorido.png" height="60">
	            </div>
    </div>
</div>
</div> -->
<nav id="menu-nav" class="bg-blue navbar rounded-0">
<div class="container">
  <div class="container-fluid" style="width: max-content;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-expand-xl">        
			<li>
				<a href="https://www.youtube.com/playlist?list=PLva2C7MtgiEfK_cpxisXhjQOj40rIfm_6">
					<i><?= get_tema_image_tag('plano-assinatura2.png', null, null, 'Como usar') ?></i> <span><strong>Como usar</strong></span>
				</a>
			</li>

			<li class="<?php menu_ativo_inativo('resolver_questoes') ?>">
				<a href="<?= get_resolver_questoes_url() . "?zf=1" ?>"><i><?= get_tema_image_tag('ico-questoes.png', null, null, 'Resolver Questões') ?></i> <span>Resolver Questões</span></a>
			</li>	
				
			<li class="<?php menu_ativo_inativo('meus_cadernos') ?>">
				<?php if(is_visitante()) : ?>
					<a data-toggle="modal" data-target="#login-modal" data-redirect="<?php echo get_meus_cadernos_url(FALSE); ?>" href="#"><i><?= get_tema_image_tag('ico-cadernos.png', null, null, 'Meus Cadernos') ?></i> <span>Meus Cadernos</span></a>
				<?php else: ?>
					<a data-toggle="modal" href="<?= get_acesso_meus_cadernos_url() ?>"><i><?= get_tema_image_tag('ico-cadernos.png', null, null, 'Meus Cadernos') ?></i> <span>Meus Cadernos</span></a>
				<?php endif; ?>
			</li>
			
			<li class="<?php menu_ativo_inativo('caderno_exponencial') ?>">
				<a href="<?= get_cadernos_exponencial_url() ?>"><i><?= get_tema_image_tag('ico-cadernos.png', null, null, 'Cadernos Exponencial') ?></i> <span>Cadernos Exponencial</span></a>
			</li>
			
			<li class="<?php menu_ativo_inativo('meus_simulados') ?>">
				<a href="<?= get_meus_simulados_url() ?>"><i><?= get_tema_image_tag('ico-simulados.png', null, null, 'Simulados') ?></i> <span>Simulados</span></a>
			</li>
			
			<li class="<?php menu_ativo_inativo('painel_aluno') ?>">
				<?php if(is_visitante()) : ?>
					<a data-toggle="modal" data-target="#login-modal" data-redirect="<?php echo get_painel_aluno_url(FALSE); ?>" href="#"><i><?= get_tema_image_tag('ico-desempenho.png', null, null, 'Desempenho') ?></i> <span>Desempenho</span></a>
				<?php else: ?>
					<a data-toggle="modal" href="<?= get_painel_aluno_url() ?>"><i><?= get_tema_image_tag('ico-desempenho.png', null, null, 'Desempenho') ?></i> <span>Desempenho</span></a>
				<?php endif; ?>
			</li>
			
			<li class="<?php menu_ativo_inativo(array('favoritas','resolver_favoritas')) ?>">
				<?php if(is_visitante()) : ?>
					<a data-toggle="modal" data-target="#login-modal" data-redirect="<?php echo get_favoritas_url(FALSE); ?>" href="#"><i><?= get_tema_image_tag('ico-favoritas.png', null, null, 'Favoritas') ?></i> <span>Favoritas</span></a>
				<?php else: ?>
					<a data-toggle="modal" href="<?= get_favoritas_url() ?>"><i><?= get_tema_image_tag('ico-favoritas.png', null, null, 'Favoritas') ?></i> <span>Favoritas</span></a>
				<?php endif; ?>	
			</li>
			
			<?php if(tem_acesso(array(/*ADMINISTRADOR, COORDENADOR, PROFESSOR, CONSULTOR, REVISOR, ASSINANTE_SQ, ALUNO_COACH*/ USUARIO_LOGADO))) : ?>
				<li class="<?php menu_ativo_inativo('configuracoes') ?>">
					<a href="<?= get_configuracoes_url() ?>"><i class="fa fa-cog" style="font-size: 18px;"></i> <span>Configurações</span></a>
				</li>
			<?php endif; ?>	
        
      </ul>
      
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</div>
</nav>
</div>
<div id="espaco-ipadpro"></div>
<script>
$(function () {
	$('.navbar-minimalize').click(function () {
		if(sessionStorage.miniNavbar && sessionStorage.miniNavbar == 1) {
			sessionStorage.miniNavbar = 0;
		}
		else {
			sessionStorage.miniNavbar = 1;
		}
	});
});

if(sessionStorage.miniNavbar && sessionStorage.miniNavbar == 1) {
	$('body').addClass('mini-navbar');
}
else {
	$('body').removeClass('mini-navbar');	
}
</script>