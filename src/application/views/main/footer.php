    </div>
	<?php javascript_da_pagina() ?>

    <?php if(isset($js) && $js) : ?>
        <?php foreach ($js as $src) : ?>
            <script src="<?= $src ?>"></script>
        <?php endforeach ?>
    <?php endif ?>    

	<?php if( SHOW_ACTIVE_PLUGINS && is_administrador() ): ?>
		<div class="text-center p-3 text-info font-weight-bold">
			<?php 
		      FiltroPluginsConstantes::plugins_ativos();
		    ?>
		 </div>
	<?php endif; ?>
	<?php KLoader::view("login/modal_expiracao"); ?>
</body>
</html>