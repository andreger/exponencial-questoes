<?php $titulo = $simulado['sim_nome']; ?>

<div class="row text-center">
	<?php admin_cabecalho_pagina ( $titulo ); ?>
</div>

<?php exibir_mensagem(); ?>

<?php if(!isset($respostas)) : ?>
	<?php $this->view("simulados/resim/cronometro") ?>
<?php endif ?>

<nav class="navbar navbar-static-top resultado" role="navigation" style="margin-bottom: 0">
	<div class="btn-tools">
		
		<?php $this->view("simulados/resim/barra_tarefas") ?>
		
		<div class="col-lg-12 text-align-right">
		<?php if(!$exibir_resultado) : ?>
			<?php $this->view("simulados/resim/finalizar_resultado_botao") ?>
		<?php else : ?>
			<?php $this->view("simulados/resim/barra_resultado") ?>
        <?php endif; ?>
		</div>
	</div>
</nav>

<form method="post" id="form-simulado" action="<?= get_resolver_simulado_url($simulado_id) ?>">
	<input type="hidden" id="tempo" value="0" name="tempo">
	<input type="hidden" id="exibir_resultado" value="1" name="exibir_resultado">
	<div class="questoes">
		<?php if(isset($erro)) { ui_alerta($erro, ALERTA_ERRO); }?>
		<?php if(isset($questoes) && !empty($questoes)) : ?>
			<div class="row">
				<?php foreach ($questoes as $questao) : ?>
					<div id="questao_<?php echo $questao['que_id'];?>" class="col-lg-12 questao">						
						<div class="ibox">
							<?php $this->view('questoes/cabecalho/cabecalho', array('questao' => $questao, 'assuntos' => $check_assuntos, 'simplificado' => !$exibir_resultado, 'cabecalho' => $cabecalho_ordem, 'esconder_favorita' => TRUE)); ?>
							
							<div class="ibox-content">

								<?php $this->view('questoes/barra_superior/barra_superior', ['pagina' => PAGE_RESOLVER_SIMULADO]); ?>
								
								<?php $this->view('questoes/principal/corpo'); ?>
								
								<?php $this->view('questoes/principal/alternativas', ['pagina' => PAGE_RESOLVER_SIMULADO]); ?>
								
								<div class="row">

									<div class="col-lg-5">
										<?php 
											if($exibir_resultado) {
												$opcoes_letras = array(1 => 'A', 2 => 'B', 3 => 'C', 4=> 'D', 5 => 'E');
												$opcao_me = $opcoes_letras[$questao['que_resposta']];
												
												if($questao['que_tipo'] == MULTIPLA_ESCOLHA || $questao['que_tipo'] == MULTIPLA_ESCOLHA_5) {
													$opcao_correta = "A resposta é <b>{$opcao_me}</b>." ; 
												}
												else {
													$opcao_ce = $questao['que_resposta'] == CERTO ? "Certo" : "Errado";
													$opcao_correta = "A resposta é <b>{$opcao_ce}</b>." ;
												}
													
												if($exibir_info_resposta) {
													$erro_msg = "<div class='alert alert-danger resposta-simulado'>Resposta errada! {$opcao_correta}</div>";
													$anulada_msg = "<div class='alert alert-warning resposta-simulado'>Questão anulada, você ganhará a pontuação correspondente a ela.</div>";
													
													echo get_resposta_questao($questao, $respostas[$questao['que_id']], SIMULADO_RESPOSTA_CERTA, $erro_msg, $anulada_msg); 
												}
												else {
													echo "<div class='alert alert-info resposta-simulado'>{$opcao_correta}</div>";
												}
											}
										?>
									</div>

									<div class="col-lg-3"></div>
									<?php if($is_usa_cockpit): ?>
										<div class="col-lg-4 text-align-right cockpit-navegacao">
											<?= $links ?>
										</div>
									<?php endif; ?>

								</div>
							</div>
							
							<?php if($is_usa_cockpit): ?>

								<div id="info-<?= $questao['que_id'] ?>" class="panel panel-default notificacao">
									<div class="panel-heading">
										<h5 class="panel-title"></h5>
									</div>
									<div id="notificar-<?php echo $questao['que_id'];?>" style="display: none;">
										<div class="panel-body"></div>
									</div>
								</div>
							
							<?php else: ?>
								<?php $this->view('questoes/barra_inferior/barra_inferior', array('questao' => $questao, 'is_resolver_simulado' => $is_resolver_simulado, 'simulado_id' => $simulado_id)); ?>
							<?php endif; ?>

						</div>
					</div>
			<?php endforeach;?>

			<?php if(!$exibir_resultado) : ?>
			<div class="col-lg-12 text-align-right simulado-footer">
				<?php $this->view("simulados/resim/finalizar_resultado_botao") ?>
			</div>
			<?php endif ?>
			</div>
		<?php else: ?>
			<?php ui_alerta("Nenhuma questão foi encontrada com esse filtro", ALERTA_INFO, false); ?></div>
		<?php endif; ?>
	</div>
</form>


<a style="display:none" id="link-modal-assine-sq" data-toggle="modal" href="#modal-assine-sq" class="btn btn-sm btn-primary m-t-n-xs"></a>
<div id="modal-assine-sq" class="modal fade" aria-hidden="true">
	<?php $this->view(get_modal_assine_sq_view_url()); ?>
</div>
			
<!-- Modal -->
<?php $this->view("simulados/resim/finalizar_resultado_modal") ?>

<!-- Cockpit - Início -->
<?php $this->view('questoes/principal/questoes_cockpit_scripts') ?>

<?php $this->view('questoes/barra_tarefas/anotacoes/modal') ?>

<?php $this->view('questoes/barra_tarefas/questao/compartilhar_modal') ?>

<?php $this->view('questoes/barra_tarefas/questao/atalhos_modal') ?>
<!-- Cockpit - Fim -->


<script>
$(document).ready(function () {

   $(".finalizar-simulado").click(function () {
	var sim_id = $(this).data("sim_id");
	if(Cookies.get('resp_sim_' + sim_id) && Object.keys(JSON.parse(Cookies.get('resp_sim_' + sim_id))).length == <?= numero_inteiro($total) ?>){
    	$('#modal_finalizar_simulado').html("<p>Deseja finalizar o <strong>Simulado</strong>?</p>"); 
	} else {
		$('#modal_finalizar_simulado').html("<p>O <strong>Simulado</strong> contém questões não respondidas por você. Deseja realmente finalizá-lo?</p>");
	}
  
   });

   $("#resim-filtro-fechar").click(function() {
		$("#resim-filtro").hide();
		localStorage.setItem('resim.exibir_filtro', $("#resim-filtro").is(':visible'));
   });

   $("#filtro-btn").click(function() {
		$("#resim-filtro").toggle();
		localStorage.setItem('resim.exibir_filtro', $("#resim-filtro").is(':visible'));
   });

   var exibir_filtro = localStorage.getItem('resim.exibir_filtro');

   if(exibir_filtro == "true") {
       $("#resim-filtro").show();
   }
});

$(function() {
	var tamanho_fonte = <?= get_tamanho_fonte() ?>

	$('#aumentar-fonte').click(function () {
		if(tamanho_fonte <= 40) {
			tamanho_fonte++;
		}
		
		alterar_tamanho_fonte(tamanho_fonte);		
	});

	$('#diminuir-fonte').click(function () {
		if(tamanho_fonte >= 10) {
			tamanho_fonte--;
		}

		alterar_tamanho_fonte(tamanho_fonte);
	});

	alterar_tamanho_fonte(tamanho_fonte);
	
	$('.alternativa').click(function() {
		$(this).toggleClass('opcao-tachada');	
	});

	$('#timer-icon').mouseover(function() {
		$('#timer').show();
	});

	$('#timer').mouseover(function() {
		$('#timer').hide();
	});

	$('#timer-box').mouseleave(function() {
		$('#timer').hide();
	});

	$('.btn-edicao-rapida').click(function(e) {
		e.preventDefault();
		var questao_id = '#edicao-rapida-' + $(this).data('id');
		$(questao_id).toggle();
		
		$(questao_id).html("Carregando...");
		
		$.get('/questoes/questoes_xhr/carregar_edicao_rapida/'+$(this).data('id'), function(data) {
			$(questao_id).html(data);
		});
	});

	$('.confirmar-finalizar-simulado').click(function() {
		clearInterval(timerId);
		$('#form-simulado').submit();
	});	
	
	$('.i-checks').iCheck({
		checkboxClass: 'icheckbox_square-green',
		radioClass: 'iradio_square-green',
	});

	$(".i-checks").on("ifClicked", function() {
		ga('send', 'respostaSimulado');
	});

	/*$('.tipo_resultado').change(function(event){
		if($('[name="tipo_resultado[]"]:checked').length > 1 && $('#simulado_questao_todas').is(':checked')){
			$('#simulado_questao_todas').prop('checked', false);
		}
	});*/

	$('.mrq').click(function (e) {
		e.preventDefault();
		var modo = $(this).data('modo');		
		<?php if(isset($simulado_id)) : ?>
			url = "/questoes/simulados/alterar_modo_resolver_questoes/" + modo + "/<?= $simulado_id ?>/<?= $exibir_resultado ?>";
		<?php endif; ?>	
		location.href = url;
	});

	$(".link-anotacoes-listagem").click(function(e){

		var questao_id = $(this).data("id");

		$('#modal-anotacoes-questao .modal-body').html("Carregando...");

		$.get('/questoes/questoes_xhr/anotacoes_questao/'+questao_id, function(data) {
			if(data){
				$('#modal-anotacoes-questao').html(data);
			}else{
				$('#modal-anotacoes-questao .modal-body').html("Erro no servidor. Tente novamente.");
			}
		});

	});

	$('.questao-opcoes .iCheck-helper').click(function(e){
		var que_id = $(this).closest('.questao-opcoes').data("que_id");
		//console.log(que_id);
		var radioValue = $("input[name='opcao_questao["+que_id+"]']:checked").val();
		var respostas = Cookies.getJSON('resp_sim_<?= $simulado_id ?>');
		if(respostas == undefined){
			respostas = {};
		}
		respostas[que_id] = radioValue;
		Cookies.set('resp_sim_<?= $simulado_id ?>', respostas);
		//console.log(Cookies.getJSON('resp_sim_<?= $simulado_id ?>'));
	});

});

function alterar_tamanho_fonte(tamanho_fonte) {
	$('.texto-questao').css('font-size', tamanho_fonte + 'px');
	$('#aumentar-fonte').prop('title', 'Aumentar tamanho da fonte para ' + (tamanho_fonte + 1) + ' px');
	$('#diminuir-fonte').prop('title', 'Diminuir tamanho da fonte para ' + (tamanho_fonte - 1) + ' px');
	$.get('/questoes/xhr/salvar_tamanho_fonte/'+tamanho_fonte);
}

<?php if(!$exibir_resultado) : ?>

var tempo = 0;
var cron_sim_key = 'cron_sim_<?= $simulado_id ?>';
if(Cookies.get(cron_sim_key) == undefined){
	Cookies.set(cron_sim_key, '0');
}
tempo += parseInt(Cookies.get(cron_sim_key));

$('#timer').html(formatar_tempo(tempo));
var timerId = setInterval(function() {
  tempo++;
  $('#tempo').val(tempo);
  Cookies.set(cron_sim_key, parseInt(Cookies.get(cron_sim_key)) + 1);
  $('#timer').html(formatar_tempo(tempo));
}, 1000);

function formatar_tempo(segundos) {
	return new Date(segundos * 1000).toISOString().substr(11, 8);
}

setInterval(
	function() {
		$.get("/questoes/main/xhr_incrementar_tempo_gasto_simulado/<?php echo $simulado_id ?>/"+Cookies.get(cron_sim_key))
	}, <?php echo TEMPO_GASTO_DEFAULT ?> * 1000
);

<?php endif ?>
</script>
<?php $this->view('questoes/barra_inferior/barra_inferior_scripts'); ?>