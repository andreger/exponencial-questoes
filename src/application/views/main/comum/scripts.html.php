<!-- build:js /questoes/assets-admin/js/sq-scripts.min.js -->
<script src='/questoes/assets-admin/js/jquery-2.1.1.js'></script>
<script src='/questoes/assets-admin/js/plugins/jquery-ui/jquery-ui.min.js'></script>
<script src='/questoes/assets-admin/js/bootstrap.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/metisMenu/jquery.metisMenu.js'></script>
<script src='/questoes/assets-admin/js/plugins/slimscroll/jquery.slimscroll.min.js'></script>
<script src='/questoes/assets-admin/js/inspinia.js'></script>
<script src='/questoes/assets-admin/js/plugins/toastr/toastr.min.js'></script>
<script src='/questoes/assets-admin/js/jquery.chained.remote.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/chosen/chosen.jquery.js'></script>
<script src='/questoes/assets-admin/js/plugins/select2/select2.js'></script>
<script src='/questoes/assets-admin/js/plugins/validate/jquery.validate.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/validate/additional-methods.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/iCheck/icheck.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/jsTree/jstree.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/jquery-stars/stars.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/treetable/jquery.treetable.js'></script>
<script src='/questoes/assets-admin/js/plugins/datapicker/bootstrap-datepicker.js'></script>
<script src='/questoes/assets-admin/js/plugins/hotkeys/hotkeys.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/jquery-maskedinput/jquery.maskedinput.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/lity/lity.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/tooltipster/tooltipster.bundle.min.js'></script>
<script src='/questoes/assets-admin/js/plugins/tooltipster/tooltipster-ext.js'></script>
<script src='/questoes/assets-admin/js/plugins/js-cookie/js-cookie.js'></script>
<script src='/questoes/assets-admin/js/plugins/basictable/jquery.basictable.min.js'></script>
<!-- endbuild -->