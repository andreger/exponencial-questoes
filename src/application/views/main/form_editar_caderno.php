<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Editar Caderno</h4>
		</div>
		<div class="modal-body">
		
			<div class="panel blank-panel">
				<div class="panel-body">
					<div class="panel-carregando">Carregando...</div>
					<div class="panel-conteudo">
						<?php echo form_open(get_editar_caderno_url(), 'id="form-editar-caderno" class="form-horizontal"'); ?>
						<div class="form-group">
							<div class="col-sm-12">
								Nome:
								<input type="hidden" id="id_caderno" name="cad_id">
								<?php echo form_input('cad_nome', null,'id="nome_caderno" class="form-control m-b"'); ?>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								Categorias:
								<?php echo form_multiselect('categorias_ids[]', $categorias_combo, NULL, 'id="categorias_caderno" placeholder="Categorias do caderno" class="form-control m-b categorias-multiselect"'); ?>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<a href="#" class="novas-categorias-btn">Criar novas categorias</a>
								<div class="novas-categorias-div" style="display: none">
									<?php echo form_input('novas_categorias', null, 'placeholder="Crie novas categorias, separadas por vírgula. (Ex: TJ 2016)" class="form-control m-b"'); ?>
								</div>
							</div>
						</div>
						
						<?php if(tem_acesso(array(ADMINISTRADOR, PROFESSOR, REVISOR))) :?>
						<div class="form-group">
							<div class="col-sm-12">
								<label>
									<input type="checkbox" id="comercializavel" name="cad_comercializavel" value="1">  Caderno Comercializável
								</label>
							</div>
						</div>
						<?php endif; ?>

						<?php if(tem_acesso(array(PARCEIRO_COACHING))) :?>
						<div class="form-group">
							<div class="col-sm-12">
								<label>
									<input type="checkbox" id="coaching" name="cad_coaching" value="1">  Caderno Coaching
								</label>
							</div>
						</div>
						<?php endif; ?>
						
						<div><?php echo form_submit('salvar', 'Salvar', 'class="btn btn-primary"'); ?></div>
						<?php echo form_close(); ?>
					</div>
                </div>
            </div>      
		</div>
	</div>
</div>

<script>
function atualizar_categorias()
{
	var caderno_id = $('#id_caderno').val();

	$.get('/questoes/cadernos_xhr/listar_categorias_caderno/' + caderno_id, function(data) {
		var ids = $.trim(data).split(',');
		$('.categorias-multiselect').val(ids).trigger('chosen:updated');
	});
}

$(function() {
	$('.categorias-multiselect').chosen({
  		disable_search_threshold: 10,
  		placeholder_text_multiple: "Escolha as categorias do caderno",
  		width: '100%'
  	});

  	$('.novas-categorias-btn').click(function(e) {
  		e.preventDefault();
  		$(this).closest('div').find('.novas-categorias-div:first').toggle();
  	});
});
</script>