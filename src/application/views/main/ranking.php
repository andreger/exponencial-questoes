<?php 
admin_cabecalho_pagina ( 'Ranking - ' . $simulado['sim_nome'] ); 
?>

<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-content">

				<div style="margin: 20px">
					<input type="checkbox" id="aprovados" <?= $somente_aprovados ? "checked" : "" ?>> <label for="aprovados">Exibir somente aprovados</label>
				</div>

				<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_COACHING, COORDENADOR_SQ))) :?>
					<div style="margin: 20px">
						<a class="btn btn-primary" href="<?= get_ranking_simulado_url($simulado['sim_id'], $somente_aprovados, $offset, 1) ?>">Exportar Ranking</a>
					</div>
				<?php endif ?>

				<div style="margin: 20px">
					<?php if($total) : ?>
						Este ranking possui <?= $total ?> <?= $total == 1 ? "nota" : "notas" ?> de alunos
					<?php else : ?>
						Este ranking ainda não possui notas de alunos
					<?php endif; ?>
				</div>

				<div style="margin: 20px">
					<?php if($ranking_usuario['posicao']) :?>
					<a class="btn btn-primary" href="<?= get_minha_colocacao_url($simulado['sim_id'], get_current_user_id(), $somente_aprovados) ?>">Ver minha colocação</a>
					</a>
					<?php endif ?>
				</div>

				<?php if($total) : ?>
					<div class="row" style="margin: 20px 0">
						<div class="col-md-4">
							<div class="stat-list">
	                            <h2 class="no-margins"><?= $nota_5 ?></h2>
	                            <small>Média total dos <?= $qtde_5 ?> primeiros colocados</small>
	                            <div class="stat-percent"><?= $percentual_5 ?> </div>
	                            <div class="progress progress-mini" style="margin-bottom: 40px">
	                                <div style="width: <?= $percentual_a_5 ?>%;" class="progress-bar"></div>
	                            </div>
	                        </div>
	                    </div>

	                    <div class="col-md-4">
							<div class="stat-list">
	                            <h2 class="no-margins"><?= $nota_10 ?></h2>
	                            <small>Média total dos <?= $qtde_10 ?> primeiros colocados</small>
	                            <div class="stat-percent"><?= $percentual_10 ?> </div>
	                            <div class="progress progress-mini" style="margin-bottom: 40px">
	                                <div style="width: <?= $percentual_a_10 ?>%;" class="progress-bar"></div>
	                            </div>
	                        </div>
	                    </div>

	 					<div class="col-md-4">
							<div class="stat-list">
	                            <h2 class="no-margins"><?= $nota_20 ?></h2>
	                            <small>Média total dos <?= $qtde_20 ?> primeiros colocados</small>
	                            <div class="stat-percent"><?= $percentual_20 ?> </div>
	                            <div class="progress progress-mini">
	                                <div style="width: <?= $percentual_a_20 ?>%;" class="progress-bar"></div>
	                            </div>
	                        </div>
	                    </div>	                    
					</div>
					<div class="row" style="margin-bottom: 25px;">
					<div class="col-12 col-md-12">
	                    <p>&nbsp;&nbsp;Os valores apresentados já consideram o peso previsto no Edital do Concurso. Em caso de dúvida, não deixe de entrar em contato <a href="/fale-conosco/">conosco</a>!</p>
	                </div>	
	           		</div>

					<div class="row">
						<div class="col-lg-12">
							<?= tabela_deslocamento() ?>
							<div id="div-table-disciplina" class="table-responsive">
								<table class="table">
									<tr>
										<th>Disciplina</th>
										<th style="text-align: center">Nota Média</th>

										<?php if($ranking_usuario['posicao']) :?>
											<th style="text-align: center">Minha Nota</th>
										<?php endif ?>

										<th style="text-align: center">Aproveitamento Médio</th>

										<?php if($ranking_usuario['posicao']) :?>
											<th style="text-align: center">Meu Aproveitamento</th>
										<?php endif ?>
									</tr>
									
									<?php foreach ($media_disciplinas as $media_disciplina) : ?>
									<tr>
										<td><?= $media_disciplina['dis_nome'] ?></td>
										<td style="text-align: center"><?= $media_disciplina['media'] ?></td>

										<?php if($ranking_usuario['posicao']) :?>
											<td style="text-align: center" class="<?= $media_disciplina['estilo'] ?>"><?= $media_disciplina['minha_nota'] ?></td>
										<?php endif ?>
										
										<td style="text-align: center"><?= $media_disciplina['percentual'] ?></td>

										<?php if($ranking_usuario['posicao']) :?>
											<td style="text-align: center"><?= $media_disciplina['meu_percentual'] ?></td>
										<?php endif ?>
									</tr>
									<?php endforeach ?>
								</table>
							</div>
						</div>
					</div>

					<?= tabela_deslocamento() ?>
					<div id="div-table" class="table-responsive">
						<table class="table ranking-tabela table-striped table-bordered">
							<thead>
							<tr class="bg-blue text-white">
								<th>#</th>
								<th style="text-align: left">Nome</th>

								<?php foreach ($ranking[0]['colunas'] as $coluna) : ?>
									<th class="<?= $coluna['destaque'] ? 'ranking-destaque' : '' ?>"><?= $coluna['nome'] ?></th>
								<?php endforeach ?>

								<th>Situação</th>
								<th>Data</th>
							</tr>
							</thead>
							<tbody>
							<?php $i = 0; foreach ($ranking as $item) : $i++; ?>
							<?php $usuario = get_usuario_array($item['usu_id']) ?>
							<tr class="<?= $item['aprovado'] == 'Aprovado' ? 'aprovado' : 'reprovado' ?> <?= get_current_user_id() == $usuario['id'] ? 'usuario-corrente' : '' ?>">
								<td><?= $i + (LIMIT_RANKING * floor(($offset + 1) / LIMIT_RANKING)) ?></td>
								<td style="text-align: left"><?= get_foto_usuario($usuario['id']) . ' ' . get_usuario_nome_exibicao($item['usu_id']) ?></td>

								<?php foreach ($item['colunas'] as $coluna) : ?>
									<td class="<?= $coluna['destaque'] ? 'ranking-destaque' : '' ?>
									<?= $item['aprovado'] == 'Aprovado' ? '' : 'reprovadoo' ?>
									<?= $coluna['valor'] > '0' ? '' : 'reprovadoo' ?>
									"><?= numero_brasileiro($coluna['valor']) ?></td>
								<?php endforeach ?>

								<td class="<?= $item['aprovado'] == 'Aprovado' ? 'aprovadoo' : 'reprovadoo' ?>"><?= $item['aprovado'] ?></td>
								<td><?= converter_para_ddmmyyyy($item['sim_usu_data_resolucao'])?></td>
							</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>

					<div class="ibox">
						<?php echo $links; ?>
					</div>

				<?php endif ?>
			</div>
		</div>
	</div>
</div>

<script src='/questoes/assets-admin/js/plugins/floatThead/jquery.floatThead.min.js'></script>
<script>
$(document).ready(function() {
	$("#aprovados").click( function () {
		if($("#aprovados").is(':checked')) {
			window.location = "<?= get_ranking_simulado_url($simulado['sim_id'], 1); ?>";
			//$(".ranking-tabela").find(".reprovado").hide();
		}
		else {
			//$(".ranking-tabela").find(".reprovado").show();
			window.location = "<?= get_ranking_simulado_url($simulado['sim_id'], 0); ?>";
		}
	});

	var $table = $('.ranking-tabela');
	$table.floatThead(
		{
	    responsiveContainer: function($table){
	        return $table.closest('.table-responsive');
	    }
	});

	
});
</script>