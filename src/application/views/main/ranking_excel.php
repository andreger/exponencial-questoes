<?php 


    tem_acesso(array(
        ADMINISTRADOR, COORDENADOR_COACHING, COORDENADOR_SQ
    ), ACESSO_NEGADO_SQ);	

    use Box\Spout\Reader\ReaderFactory;
    use Box\Spout\Writer\WriterFactory;
    use Box\Spout\Common\Type;

    $newFileName = time() . '-ranking.xlsx';
    $newFilePath = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/temp/' . $newFileName;

    $writer = WriterFactory::create(Type::XLSX);
    $writer->openToFile($newFilePath);

    $row = array();

    array_push($row, '#');
    array_push($row, 'Nome');
    array_push($row, 'E-mail');

    foreach ($ranking[0]['colunas_exportar'] as $coluna){
        array_push($row, $coluna['nome']);
    }

    array_push($row, 'Situação');
    array_push($row, 'Data');
    
    $writer->addRow($row);

    $i = 0; 

    foreach ($ranking as $item){
        
        $i++;
        
        $row = array();

        array_push($row, $i);
        array_push($row, get_usuario_nome_exibicao($item['usu_id']));
        array_push($row, get_usuario_email($item['usu_id']));

        foreach ($item['colunas_exportar'] as $coluna){
            array_push($row, $coluna['valor']);
        }

        array_push($row, $item['aprovado']);
        array_push($row, converter_para_ddmmyyyy($item['sim_usu_data_resolucao']));
        
        $writer->addRow($row);
    }

    $writer->close();
	
	header('Content-Description: File Transfer');
	header('Content-Disposition: attachment; filename="ranking_exportar.xlsx"');
	header('Content-Transfer-Encoding: binary');
	header('Content-Length: ' . filesize($newFilePath));
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Pragma: public');
	header('Expires: 0');
	
	//clean all levels of output buffering
	while (ob_get_level()) {
	    ob_end_clean();
	}

	readfile($newFilePath);
	exit;
?>