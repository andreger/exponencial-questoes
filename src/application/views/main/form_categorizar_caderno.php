<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Categorizar Caderno</h4>
		</div>
		<div class="modal-body">
			<div class="panel-carregando">Carregando...</div>
			<div class="panel-conteudo">
				<div class="panel blank-panel">
					<div class="panel-heading">
						<?php if($categorias) :?>
						<div class="panel-options">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab-1" data-toggle="tab">Categorias Existentes</a></li>
								<li class=""><a href="#tab-2" data-toggle="tab">Nova Categoria</a></li>
							</ul>
						</div>
						<?php endif; ?>
					</div>
					
					<div class="panel-body">
				
						<div class="tab-content">
							<?php if($categorias) :?>
							<div class="tab-pane active" id="tab-1">
								<?php echo form_open(get_adicionar_a_categoria_existente_url(), 'id="form-caderno-existente" class="form-horizontal"'); ?>
								<div class="form-group">
									<div class="col-sm-12">
										<input type="hidden" class="hidden-caderno-id" name="caderno_id">
										<input type="hidden" class="hidden-massa" name="massa">
										<?php echo form_multiselect('categorias_ids[]', $categorias_combo, null, 'id="categorias_ids" class="multiselect form-control m-b"'); ?>
									</div>
								</div>
								<div><?php echo form_submit('salvar', 'Salvar', 'class="btn btn-primary"'); ?></div>
								<?php echo form_close(); ?>
							</div>
							<?php endif; ?>
							<div class="tab-pane <?php echo $categorias ? '' : 'active' ?>" id="tab-2">
								<?php echo form_open(get_adicionar_a_nova_categoria_url(), 'id="form-nova-categoria" class="form-horizontal"'); ?>
								<div class="form-group">
									<div class="col-sm-12">
										<input type="hidden" class="hidden-caderno-id" name="caderno_id">
										<input type="hidden" class="hidden-massa" name="massa">
										<?php echo form_input('nome', null,'id="nome_caderno" placeholder="Nome da categoria" class="form-control m-b"'); ?>
									</div>
								</div>
								<div><?php echo form_submit('salvar', 'Salvar', 'class="btn btn-primary"'); ?></div>
								<?php echo form_close(); ?>
							</div>
						</div>
	                </div>
	            </div>      
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
	$("#form-nova-categoria").validate({
		rules: {
			nome: {
				required: true,
			}
		},
		messages: {
			nome: {
				required: "Nome da Categoria é obrigatório",
			}
		}
	})
});
</script>