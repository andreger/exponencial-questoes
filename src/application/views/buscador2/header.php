<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Exponencial Concursos | Sistema de Questões</title>
    <link rel="shortcut icon" href="<?php echo base_url('/favicon.ico') ?>" />

    <link href="<?php echo base_url('assets-admin/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets-admin/font-awesome/css/font-awesome.css') ?>" rel="stylesheet">

    <!-- Toastr style -->
    <link href="<?php echo base_url('assets-admin/css/plugins/toastr/toastr.min.css') ?>" rel="stylesheet">

    <!-- Gritter -->
    <link href="<?php echo base_url('assets-admin/js/plugins/gritter/jquery.gritter.css') ?>" rel="stylesheet">
    
    <?php if(isset($include_data_tables)) : ?>
    <!-- Data Tables -->
    <link href="<?php echo base_url('assets-admin/css/plugins/dataTables/dataTables.bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets-admin/css/plugins/dataTables/dataTables.responsive.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets-admin/css/plugins/dataTables/dataTables.tableTools.min.css') ?>" rel="stylesheet">
	<?php endif ?>
	
	<?php if(isset($include_icheck)) : ?>
    <link href="<?php echo base_url('assets-admin/css/plugins/iCheck/custom.css') ?>" rel="stylesheet">
    <?php endif; ?>
	
	<?php if(isset($include_summernote)) : ?>
	<link href="<?php echo base_url('assets-admin/css/plugins/summernote/summernote.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets-admin/css/plugins/summernote/summernote-bs3.css') ?>" rel="stylesheet">
	<?php endif ?>
	 <?php if(isset($include_datepicker)) : ?>
    <link href="<?php echo base_url('assets-admin/css/plugins/datapicker/datepicker3.css') ?>" rel="stylesheet">
    <?php endif; ?>
	<?php if(isset($include_chosen)) : ?>
	<link href="<?php echo base_url('assets-admin/css/plugins/chosen/chosen.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets-admin/js/plugins/select2/select2.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets-admin/js/plugins/select2/select2-bootstrap.css') ?>" rel="stylesheet">
	<?php endif ?>
	
	<?php if(isset($include_steps)) : ?>
	<link href="<?php echo base_url('assets-admin/css/plugins/steps/jquery.steps.css') ?>" rel="stylesheet">
	<?php endif ?>
	
	<?php if(isset($include_jstree)) : ?>
	<link href="<?php echo base_url('assets-admin/css/plugins/jsTree/style.min.css') ?>" rel="stylesheet">
	<style>
    .jstree-open > .jstree-anchor > .fa-folder:before {
        content: "\f07c";
    }

    .jstree-default .jstree-icon.none {
        width: 0;
    }
	</style>
	<?php endif ?>
	
    <link href="<?php echo base_url('assets-admin/css/animate.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets-admin/css/style.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets-admin/css/custom.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets-admin/css/mobile.css') ?>" rel="stylesheet">

    <!-- Mainly scripts -->
    <script src="<?php echo base_url('assets-admin/js/jquery-2.1.1.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/plugins/metisMenu/jquery.metisMenu.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/plugins/slimscroll/jquery.slimscroll.min.js') ?>"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url('assets-admin/js/inspinia.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/plugins/pace/pace.min.js') ?>"></script>

    <?php if(isset($include_jquery_ui)) : ?>
    <!-- jQuery UI -->
    <script src="<?php echo base_url('assets-admin/js/plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
	<?php endif ?>
	
	<?php if(isset($include_jquery_form)) : ?>
    <!-- jQuery Form -->
    <script src="<?php echo base_url('assets-admin/js/jquery.form.js') ?>"></script>
	<?php endif ?>

    <!-- Toastr -->
    <script src="<?php echo base_url('assets-admin/js/plugins/toastr/toastr.min.js') ?>"></script>

    <?php if(isset($include_data_tables)) : ?>
    <!-- Data Tables -->
    <script src="<?php echo base_url('assets-admin/js/plugins/dataTables/jquery.dataTables.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/plugins/dataTables/dataTables.bootstrap.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/plugins/dataTables/dataTables.responsive.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/plugins/dataTables/dataTables.tableTools.min.js') ?>"></script>
    <?php endif ?>
    
    <?php if(isset($include_steps)) : ?>
    <script src="<?php echo base_url('assets-admin/js/plugins/staps/jquery.steps.min.js') ?>"></script>
    <?php endif; ?>
    
	<?php if(isset($include_validade)) : ?>
    <script src="<?php echo base_url('assets-admin/js/plugins/validate/jquery.validate.min.js') ?>"></script>
    <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
    <script src="<?php echo base_url('assets-admin/js/validate.translate.js') ?>"></script>
    <?php endif; ?>
    
    <?php if(isset($include_datepicker)) : ?>
    <script src="<?php echo base_url('assets-admin/js/plugins/datapicker/bootstrap-datepicker.js') ?>"></script>
    <?php endif; ?>
    
    <?php if(isset($include_jquery_chained)) : ?>
    <script src="<?php echo base_url('assets-admin/js/jquery.chained.remote.min.js') ?>"></script>
    <?php endif ?>
    
    <?php if(isset($include_summernote)) : ?>
    <script src="<?php echo base_url('assets-admin/js/plugins/summernote/summernote.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/plugins/summernote/lang/summernote-pt-BR.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/ext/summernote-ext.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/ext/summernote-ext-specialchars.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/ext/summernote-ext-myenter.js') ?>"></script>
    <?php endif ?>
    
    <?php if(isset($include_chosen)) : ?>
    <script src="<?php echo base_url('assets-admin/js/plugins/chosen/chosen.jquery.js') ?>"></script>
    <script src="<?php echo base_url('assets-admin/js/plugins/select2/select2.js'); ?>"></script>
    <?php endif ?>
    
    <?php if(isset($include_jstree)) : ?>
	<script src="<?php echo base_url('assets-admin/js/plugins/jsTree/jstree.min.js') ?>"></script>
	<?php endif ?>
	
	<?php if(isset($include_icheck)) : ?>
    <script src="<?php echo base_url('assets-admin/js/plugins/iCheck/icheck.min.js'); ?>"></script>
    <?php endif; ?>
    
    <?php if(isset($include_cropper)) : ?>
    <link href="<?php echo base_url('assets-admin/css/plugins/cropper/cropper.min.css'); ?>" rel="stylesheet">
	<script src="<?php echo base_url('assets-admin/js/plugins/cropper/cropper.min.js'); ?>"></script>
    <?php endif; ?>
</head>

<body>
    <div id="wrapper">
    	<?php $this->load->view('buscador2/menu') ?>
    	 <div id="page-wrapper" class="gray-bg dashbard-1">