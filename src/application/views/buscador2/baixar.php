<div class="row">
	<div class="col-lg-12">
		<div class="ibox-content">
			<div style="margin-bottom: 20px">
				<h2>Status</h2>
				<h3>Baixar Páginas</h3>
				<div><?= ($status) ? "Baixando..." : "Parado." ?></div>
				<div><?= $proxima ?> de <?= number_format($ultima, 0, ',', '.') ?> (<?= number_format($concluido, 2, ',', '.') ?> %)</div>
				<div>Páginas com erro: <?= $erros ?></div>
			</div>

			<div style="margin-bottom: 20px">
				<h3>Processar Páginas</h3>
				<div><?= $p_proxima ?> de <?= number_format($p_ultima, 0, ',', '.') ?> (<?= number_format($p_concluido, 2, ',', '.') ?> %)</div>
			</div>

			<div style="margin-bottom: 20px">
				<h3>Baixar e Processar Provas</h3>
				<div>Provas restantes: <?= $p_restantes ?></div>
			</div>
		</div>
	</div>

	<div class="col-lg-12">
		<div class="ibox-content">
			<h2>Configurações</h2>
			
			<form method="post">
			<div class="form-group">
				<label class="col-sm-2 control-label">URL Base</label>
                <div class="col-sm-10">
					<input type="text" class="form-control" name="url_base" value="<?= $url_base ?>" />
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Páginas por minuto</label>
                <div class="col-sm-10">
					<input type="text" class="form-control" name="ppm" value="<?= $ppm ?>" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-10">
					<input type="submit" class="btn btn-primary" name="submit" value="Salvar"/>
				</div>
			</div>
			</form>

			<div style="clear: both"></div>
		</div>
	</div>

	<div class="col-lg-12">
		<div class="ibox-content">
			<h2>Ações</h2>
			
			<form method="post">

			<div class="form-group">
				<div class="col-sm-10">
					<?php if($status) : ?>
						<input type="submit" class="btn btn-primary" name="parar" value="Parar de Baixar"/>
					<?php else : ?>
						<input type="submit" class="btn btn-primary" name="baixar" value="Baixar Páginas"/>
					<?php endif; ?>
					<input type="submit" class="btn btn-primary" name="resetar" value="Resetar"/>
				</div>
			</div>
			</form>

			<div style="clear: both"></div>
		</div>
	</div>

</div>

