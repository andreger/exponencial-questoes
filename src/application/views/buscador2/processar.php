<div class="row">
	<div class="col-lg-12">
		<div class="ibox-content">
			<h2>Status</h2>
			<div><?= ($status) ? "Processando..." : "Parado." ?></div>
			<div><?= $proxima ?> de <?= number_format($ultima, 0, ',', '.') ?> (<?= number_format($concluido, 2, ',', '.') ?> %)</div>
		</div>
	</div>

	<div class="col-lg-12">
		<div class="ibox-content">
			<h2>Configurações</h2>
			
			<form method="post">
			<div class="form-group">
				<label class="col-sm-2 control-label">Páginas por minuto</label>
                <div class="col-sm-10">
					<input type="text" class="form-control" name="ppm" value="<?= $ppm ?>" />
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-10">
					<input type="submit" class="btn btn-primary" name="submit" value="Salvar"/>
				</div>
			</div>
			</form>

			<div style="clear: both"></div>
		</div>
	</div>

	<div class="col-lg-12">
		<div class="ibox-content">
			<h2>Ações</h2>
			
			<form method="post">

			<div class="form-group">
				<div class="col-sm-10">
					<?php if($status) : ?>
						<input type="submit" class="btn btn-primary" name="parar" value="Parar de Processar"/>
					<?php else : ?>
						<input type="submit" class="btn btn-primary" name="processar" value="Processar Páginas"/>
					<?php endif; ?>
					<input type="submit" class="btn btn-primary" name="resetar" value="Resetar"/>
				</div>
			</div>
			</form>

			<div style="clear: both"></div>
		</div>
	</div>

</div>

