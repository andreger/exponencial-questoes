<div class="modal-dialog modal-filtro">
	<div class="modal-content modal-form-content">
		<div class="modal-header">
			<button type="button" id="cancel-btn" class="close" data-dismiss="modal">&times;</button>
			<button type="button" id="back-btn" class="close" style="display: none">&times;</button>
			<h4 class="modal-title">Filtrar Questões</h4>
		</div>
		<div class="modal-body">
			<div id="p1">
				<?php echo form_open($filtro_url, 'id="form-resolver-questao" class="form-horizontal" method="get"'); ?>
					
					<?php if($is_caderno && $has_filtro_caderno && !$is_caderno_comprado): ?>
						<input type="hidden" id="remove-questoes-caderno" name="remove_questoes_caderno" value="N">
					<?php endif; ?>
					
					<?php $this->view("questoes/filtro/filtro_campos"); ?>
					
					<div>
						<?php echo form_submit('buscar', 'Buscar!', 'class="btn btn-primary" id="filtro-submit"'); ?>
						<a class="btn btn-outline btn-default" id="limpar-botao-modal">Limpar Filtros</a>
					</div>
					
				<?php echo form_close(); ?>
			</div>

			<?php $this->view("questoes/filtro/arvore_assuntos"); ?>

		</div>
	</div>
</div>
<?php $this->view('modal/filtro_scripts') ?>