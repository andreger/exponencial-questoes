
<?php if($is_modal): ?>

    <div id="modal-arvore-assuntos" class="modal fade" aria-hidden="true">
        <div id="p2" style="display:none" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Selecionar Assuntos</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info">
                        A quantidade de questões mostrada para cada assunto leva em conta o total de questões cadastradas em nossa base, sem filtros aplicados
                    </div>
                    <div class="arvore-control">
                        <a id="expand-all">Expandir tudo</a>
                        <a id="collapse-all">Recolher tudo</a>
                    </div>
                <div id="p2-corpo"></div>
                <div class="botoes" style="margin-top: 20px">
                    <a class="btn primary btn-default" data-dismiss="modal" id="adicionar">Adicionar</a>
                    <a class="btn primary btn-default" data-dismiss="modal" id="cancelar">Cancelar</a>
                </div>				
            </div>
        </div>
    </div>

<?php else: ?>

    <div id="p2" style="display:none">
        <div class="alert alert-info">
            A quantidade de questões mostrada para cada assunto leva em conta o total de questões cadastradas em nossa base, sem filtros aplicados
        </div>
        <div class="arvore-control">
            <a id="expand-all">Expandir tudo</a>
            <a id="collapse-all">Recolher tudo</a>
        </div>
        <div id="p2-corpo"></div>
        <div class="botoes" style="margin-top: 20px">
            <a class="btn primary btn-default" id="adicionar">Adicionar</a>
            <a class="btn primary btn-default" id="cancelar">Cancelar</a>
        </div>
    </div>

<?php endif; ?>