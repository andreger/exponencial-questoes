<div class="filtro-incluir">
    <div class="row">
        <label class="col-md-2 control-label">Incluir questões </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_incluir', TODAS, $filtro_incluir == TODAS, 'id="radio_incluir_todas"');?> Todas </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_incluir', NAO_RESOLVIDAS,  $filtro_incluir == NAO_RESOLVIDAS, 'id="radio_nao_resolvidas"');?> Não resolvidas </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_incluir', RESOLVIDAS, $filtro_incluir == RESOLVIDAS, 'id="radio_resolvidas"');?> Resolvidas </label>
    </div>

    <div class="row">
        <label class="col-md-offset-2 col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_incluir', QUE_ACERTEI, $filtro_incluir == QUE_ACERTEI, 'id="radio_acertei"');?> Que eu acertei </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_incluir', QUE_ERREI, $filtro_incluir == QUE_ERREI, 'id="radio_errei"');?> Que eu errei </label>
        <!-- <label class="col-md-2 radio-inline i-checks"><?php echo form_radio('filtro_incluir', FAVORITAS, $filtro_incluir == FAVORITAS, 'id="radio_favoritas"');?> Favoritas </label> -->
    </div>
</div>			

<div class="filtro-incluir">
    <div class="row">
        <label class="col-md-2 control-label">Comentários </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', TODAS, $filtro_comentarios == TODAS, 'id="radio_comentarios_todas"');?> Todas </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', COM_COMENTARIOS_DE_PROFESSORES, $filtro_comentarios == COM_COMENTARIOS_DE_PROFESSORES, 'id="radio_com_comentarios_de_professores"');?> Professores </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', COM_COMENTARIOS_DE_ALUNOS, $filtro_comentarios == COM_COMENTARIOS_DE_ALUNOS, 'id="radio_com_comentarios_de_alunos"');?> Alunos </label>
    </div>

    <div class="row">
        <label class="col-md-offset-2 col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', COM_COMENTARIOS, $filtro_comentarios == COM_COMENTARIOS, 'id="radio_com_comentarios"');?> Quaisquer </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', NENHUM_COMENTARIO, $filtro_comentarios == NENHUM_COMENTARIO, 'id="radio_nenhum_comentario"');?> Nenhum </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', MEUS_COMENTARIOS, $filtro_comentarios == MEUS_COMENTARIOS, 'id="radio_meus_comentarios"');?> Meus </label>
    </div>

    <div class="row">
        <label class="col-md-offset-2 col-md-9 radio-inline i-checks"><?php echo form_radio('filtro_comentarios', COM_COMENTARIOS_EM_VIDEO, $filtro_comentarios == COM_COMENTARIOS_EM_VIDEO, 'id="radio_com_comentarios_em_video"');?> Vídeos </label>
    </div>
</div>

<?php if(tem_acesso(array(ADMINISTRADOR, CONSULTOR, PROFESSOR, REVISOR))) :?>
<div class="filtro-incluir">
    <div class="row">
        <label class="col-md-2 control-label">Incluir questões </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_resposta', TODAS, $filtro_resposta == TODAS, 'id="radio_respostas_todas"');?> Todas </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_resposta', COM_RESPOSTA, $filtro_resposta == COM_RESPOSTA, 'id="radio_com_resposta"');?> Com Resposta </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_resposta', SEM_RESPOSTA, $filtro_resposta == SEM_RESPOSTA, 'id="radio_sem_resposta"');?> Sem Resposta </label>
    </div>
</div>
<div class="filtro-incluir">
    <div class="row">
        <label class="col-md-2 control-label">Incluir questões </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_ativo', TODAS, $filtro_ativo == TODAS, 'id="radio_ativas_inativas_todas"');?> Todas </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_ativo', ATIVO, $filtro_ativo == ATIVO, 'id="radio_ativas"');?> Ativas </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_ativo', INATIVO, $filtro_ativo == INATIVO, 'id="radio_inativas"');?> Inativas </label>
    </div>
</div>
<?php endif ?>
<?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) :?>
<div class="filtro-incluir">
    <div class="row">
        <label class="col-md-2 control-label">Incluir questões </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_repetidas', TODAS, $filtro_repetidas == TODAS, 'id="radio_repetidas_nao_repetidas_todas"');?> Todas </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_repetidas', REPETIDAS, $filtro_repetidas == REPETIDAS, 'id="radio_repetidas"');?> Repetidas </label>
        <label class="col-md-3 radio-inline i-checks"><?php echo form_radio('filtro_repetidas', NAO_REPETIDAS, $filtro_repetidas == NAO_REPETIDAS, 'id="radio_nao_repetidas"');?> Não repetidas </label>
    </div>
</div>
<?php endif ?>
<div class="filtro-incluir">
    <div class="row">
        <label class="col-md-2 control-label">Excluir questões </label>
        <?php if(!$is_caderno) :?>
            <label class="col-md-3 checkbox-inline i-checks"><?php echo form_checkbox('filtro_excluir[]', DOS_MEUS_CADERNOS, in_array(DOS_MEUS_CADERNOS, $filtro_excluir), 'id="check_cadernos"');?> Dos meus cadernos </label>
        <?php endif ?>
        <label class="col-md-3 checkbox-inline i-checks"><?php echo form_checkbox('filtro_excluir[]', ANULADA, in_array(ANULADA, $filtro_excluir), 'id="check_anuladas"');?> Anuladas </label>
        <label class="col-md-3 checkbox-inline i-checks"><?php echo form_checkbox('filtro_excluir[]', DESATUALIZADA, in_array(DESATUALIZADA, $filtro_excluir), 'id="check_desatualizadas"');?> Desatualizadas </label>
    </div>
    <?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ))) :?>
        <div class="row">
            <label class="col-md-offset-2 col-md-3 checkbox-inline i-checks"><?php echo form_checkbox('filtro_excluir[]', REPETIDAS, in_array(REPETIDAS, $filtro_excluir), 'id="check_repetidas"');?> Repetidas </label>
        </div>
    <?php endif ?>    
</div>

<div class="filtro-incluir">
    <div class="row">
        <label class="col-md-2 control-label">Apenas favoritas </label>
        <label class="col-md-3 checkbox-inline i-checks"><?php echo form_checkbox('filtro_favoritas', FAVORITAS, $filtro_favoritas, 'id="check_favoritas"');?></label>
    </div>
</div>

<div class="form-group">

    <div class="col-sm-11">
        <?php echo form_input('busca', $busca, 'id="busca" autocomplete=off placeholder="Busca por texto" class="form-control m-b"'); ?>
    </div>    
    <?php ui_filtro_campo_helper("Veja como funciona a busca por texto livre:<br/>
        - organizações modernas -> busca ambos os termos, independente da ordem em que aparecem.<br/>
        - \"organizações modernas\" -> busca texto exato.<br/>
        - organizações | modernas -> busca um ou outro termo.<br/>
        A busca é feita no enunciado e alternativas das questões. Imagens são ignoradas.") ?>

    <div class="col-sm-12">
        <?php echo form_multiselect('dis_ids[]', $disciplinas, array()/*$disciplinas_selecionadas*/, 'id="dis_ids" class="multiselect form-control m-b"'); ?>
    </div>
    
    <div class="col-sm-12">
        <label class="i-checks lbl-normal"><?php echo form_checkbox('apenas_assunto_pai', 1, $apenas_assunto_pai, 'id="check_assunto_pai"');?> Apenas assunto-pai</label>
    </div>
    
    <div class="col-sm-12">
        <?php echo form_multiselect('ass_ids[]', $assuntos, $assuntos_selecionados, 'id="ass_ids" class="multiselect form-control m-b"'); ?>
        <?php if($is_modal): ?>
            <a style="position:relative;top: -15px" id="arvore-btn" data-toggle="modal" href="#modal-arvore-assuntos">Árvore de assuntos</a>
        <?php else: ?>
            <a style="position:relative;top: -15px" id="arvore-btn" href="#">Árvore de assuntos</a>
        <?php endif; ?>
        
    </div>
    <?php if(tem_acesso(array(ADMINISTRADOR, PROFESSOR, REVISOR))) : ?>
    <div class="col-sm-12">
        <?php echo form_multiselect('professores_ids[]', $professores, $professores_selecionados, 'id="professores_ids" class="multiselect form-control m-b"'); ?>
    </div>
    <?php endif; ?>
    
    <div class="col-sm-12">
        <?php echo form_multiselect('ban_ids[]', $bancas, $bancas_selecionadas, 'id="ban_ids" class="multiselect form-control m-b"'); ?>
    </div>
    <div class="col-sm-12">
        <?php echo form_multiselect('org_ids[]', $orgaos, $orgaos_selecionados, 'id="org_ids" class="multiselect form-control m-b"'); ?>
    </div>
    <div class="col-sm-12">
        <?php echo form_multiselect('car_ids[]', $cargos, $cargos_selecionados, 'id="car_ids" class="multiselect form-control m-b"'); ?>
    </div>
    <div class="col-sm-12">
        <?php echo form_multiselect('pro_anos[]', $anos, $anos_selecionados, 'id="pro_anos" class="multiselect form-control m-b"'); ?>
    </div>
    <div class="col-sm-12">
        <?php echo form_multiselect('esc_ids[]', $escolaridades, $escolaridades_selecionadas, 'id="esc_ids" class="multiselect form-control m-b"'); ?>
    </div>
    <div class="col-sm-6">
        <?php echo form_multiselect('arf_ids[]', $formacoes, $formacoes_selecionadas, 'id="arf_ids" class="multiselect form-control m-b"'); ?>
    </div>
    <div class="col-sm-6">
        <?php echo form_multiselect('ara_ids[]', $atuacoes, $atuacoes_selecionadas, 'id="ara_ids" class="multiselect form-control m-b"'); ?>
    </div>
    <div class="col-sm-6">
        <?php echo form_multiselect('que_tipos[]', $tipos, $tipos_selecionados, 'id="que_tipos" class="multiselect form-control m-b"'); ?>
    </div>
    <div class="col-sm-6">
        <?php echo form_multiselect('que_dificuldades[]', $dificuldades, $dificuldades_selecionadas, 'id="que_dificuldades" class="multiselect form-control m-b"'); ?>
    </div>

    <?php if(tem_acesso(array(ADMINISTRADOR, PROFESSOR, REVISOR))) : ?>
    <div class="col-sm-11">
        <?php echo form_multiselect('sim_ids[]', $simulados, $simulados_selecionados, 'id="sim_ids" class="multiselect form-control m-b"'); ?>
    </div>    
    <?php ui_filtro_campo_helper("Todos: exclui questões presentes em quaisquer simulados<br/>
        Ativos: exclui questões presentes em simulados ativos (simulados disponíveis para alunos)<br/>
        Todos menos inativos: exclui questões presentes em todos os simulados, exceto inativos. Ou seja, mostra questões presentes em simulados inativos (normalmente são simulados muito antigos ou não comercializados).<br/>
        Escolha simulados específicos caso queira excluir questões usadas em determinados simulados.<br/>
        Você pode aplicar várias opções neste filtro.") ?>
    <?php endif; ?>

    <div class="col-sm-12">
        <?php echo form_multiselect('neg_cad_ids[]', $cadernos, $neg_cadernos_selecionados, 'id="neg_cad_ids" class="multiselect form-control m-b"'); ?>
    </div>

    <?php if(is_administrador()) :?>
    <div class="col-sm-12">
        <?php echo form_multiselect('status_ids[]', $status, $status_selecionados, 'id="status_ids" placeholder="Status da questão" class="multiselect form-control m-b"'); ?>
    </div>
    <?php endif; ?> 

    <div class="col-sm-12">
        <?php echo form_multiselect('pro_tipo_ids[]', $modalidades, $modalidades_selecionadas, 'id="pro_tipo_ids" class="multiselect form-control m-b"'); ?>
    </div>

    <div class="col-sm-6">
        <?php echo form_input('que_id', isset($questao_id) ? $questao_id : '','id="que_id" placeholder="Id da Questão" class="form-control m-b"'); ?>
    </div>
    <?php if(is_administrador()) :?>
    <div class="col-sm-6">
        <?php echo form_input('que_qcon_id', $que_qcon_id,'id="que_qcon_id" placeholder="Q" class="form-control m-b"'); ?>
    </div>
    <?php endif; ?>
    
    
    <?php if(tem_acesso([ADMINISTRADOR, COORDENADOR, PROFESSOR, CONSULTOR])) : ?>
    <label class="col-sm-12">Questões adicionadas em:</label>

        <div class="col-sm-6">
            <?php echo form_input('data_inicio', $data_inicio,'id="data_inicio" placeholder="De" class="form-control m-b" autocomplete=off'); ?>
        </div>
        <div class="col-sm-6">
            <?php echo form_input('data_fim', $data_fim,'id="data_fim" placeholder="Até" class="form-control m-b" autocomplete=off'); ?>
        </div>
    <?php endif; ?>
    
</div>