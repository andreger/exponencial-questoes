<?php $this->view('questoes/barra_inferior/editar_comentario_estilo.html.php'); ?>

<div id='modal-editar-comentario' class='modal fade' role="dialog">
	<div class='modal-dialog modal-1000'>
		<div class='modal-content'>
			<div class='modal-header'>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class='m-t-none m-b text-center'>Editar comentário</h3>
			</div>
			<div class='modal-body'>
				<input type="hidden" id="comentario-id"/>
				<input type="hidden" id="usuario-id"/>
				<input type="hidden" id="questao-id"/>
				<div class='form-group'>
					<div id='summernote-comentario'></div>
				</div>
				<div class=''>
					<a class='btn btn-primary editar-comentario' data-loading-text="Enviando..."><i class='fa fa-check'></i>&nbsp;Enviar</a> <span id="msg-edit-comentario-<?= $questao_id ?>"></span>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->view('questoes/barra_inferior/editar_comentario_scripts.html.php'); ?>

<script>
$(document).ready(function() {
	$('.comentario-texto').bind("cut copy paste contextmenu",function(e) {
        e.preventDefault();
    });
	
	$('#summernote-<?php echo $questao_id ?>').summernote({
		 onImageUpload: function(files) {
             sendFile(files[0], $(this));
         }
	});
	
	$('#notificar-<?php echo $questao_id ?> .note-editable').css('font-size','14px');
	$('#notificar-<?php echo $questao_id ?> .note-current-fontsize').text('14');

	$('.note-view button[data-event=fullscreen]').remove();

	$('.comentar').off('click');
	$('.comentar').on('click', function (event) {
		event.preventDefault();

		var id = $(this).data('id');
		var usuId = <?php echo get_id_usuario_atual();?>;
		//var texto = $('#summernote-<?php echo $questao_id; ?>').code();
		var texto = $('#summernote-'+id).code();

		if(texto == "<br>") {
			texto = "";
		}

		$('#msg-add-comentario-' + id).text('');

		//if(texto.length >= <?= is_professor() ? 50 : 1 ?>) {
			var $button = $(this);
			$button.button("loading");

			$.ajax({
				url: '<?php echo get_adicionar_comentario_url(); ?>',
				method: 'POST',
				cache: false,
				data: { id: id, texto : texto, usuId: usuId}
			})
			.done(function( msg ) {
				$button.button("reset");
				if(msg.includes('erro:')){
					$('#msg-add-comentario-' + id).html(msg.trim().substring(5));
				}else{
					$('#msg-add-comentario-' + id).html(msg);
					var total = parseInt($('#comentario-questao-' + id + ' > span').text());
					total++;
					$('#comentario-questao-' + id + ' > span').text(total);
								
					setTimeout(function () {
						$.ajax({
							url: '<?php echo get_ajax_main_comentarios_questao_url(); ?>',
							method: 'POST',
							cache: false,
							data: { id: id }
						})
						.done(function( msg ) {
							$( '#notificar-' + id + ' .panel-body' ).empty();
							$( '#notificar-' + id + ' .panel-body' ).append( msg );
							$('#notificar-' + id ).collapse('show');
						});
					} , 2000);
				}

			});
		/*} else {
			msg = '<?= is_professor() ? "O comentário deve ter, no mínimo, 50 caracteres!" : "É necessário digitar um comentário para enviar!" ?>';
			$('#msg-add-comentario-' + id).text(msg);
		}*/
	});

	
	$('.abrir-editar-comentario').off('click');
	$('.abrir-editar-comentario').on('click', function(event) {
		var feedbox = $(this).closest('.social-feed-box');
		var id = feedbox.attr('com_id');
		$('#msg-edit-comentario-' + id).text('');
		$(this).closest('.social-feed-box').find('.comentario-texto:first').hide();
		$(this).closest('.social-feed-box').find('.summernote-div:first').summernote();

		$(this).closest('.social-feed-box').find('.note-editable:first').css('font-size','14px');
		$(this).closest('.social-feed-box').find('.note-current-fontsize').text('14');

		$(this).closest('.social-feed-box').find('.botoes:first').hide();
		$(this).closest('.social-feed-box').find('.botoes-edicao:first').show();
	});

	$('.cancelar-edicao').off('click');
	$('.cancelar-edicao').on('click', function(event) {
		$(this).closest('.social-feed-box').find('.comentario-texto:first').show();
		$(this).closest('.social-feed-box').find('.summernote-div:first').destroy();
		$(this).closest('.social-feed-box').find('.summernote-div:first').hide();
		$(this).closest('.social-feed-box').find('.botoes:first').show();
		$(this).closest('.social-feed-box').find('.botoes-edicao:first').hide();
	});

	$('.editar-comentario').off('click');
	$('.editar-comentario').on('click', function(event) { 
		event.preventDefault();

		var feedbox = $(this).closest('.social-feed-box');
		var id = feedbox.attr('com_id');
		var texto = feedbox.find('.summernote-div:first').code();
		
		if(texto == "<br>") {
			texto = "";
		}

		$('#msg-edit-comentario-' + id).text('');

		/* if(texto.length >= <?= is_professor() ? 50 : 1 ?>) { */
			var que_id = <?= $questao_id ?>;
			var $button = $(this);
			$button.button("loading");
			$.ajax({
				url: '<?php echo get_adicionar_comentario_url(); ?>' + id,
				method: 'POST',
				cache: false,
				data: { texto : texto, id: que_id }
			})
			.done(function( msg ) {
				$button.button('reset');
				
				if(msg.includes('erro:')){
					$('#msg-edit-comentario-' + id).html(msg.trim().substring(5));	
				}else{

					$('#msg-edit-comentario-' + id).html(msg);

					setTimeout(function () {
						feedbox.find('.comentario-texto:first').html(texto);
						feedbox.find('.comentario-texto:first').show();
						feedbox.find('.summernote-div:first').destroy();
						feedbox.find('.summernote-div:first').hide();
						feedbox.find('.botoes:first').show();
						feedbox.find('.botoes-edicao:first').hide();

						$.ajax({
							url: '<?php echo get_ajax_main_comentarios_questao_url(); ?>',
							method: 'POST',
							cache: false,
							data: { id: id }
						})
						.done(function( msg ) {
							$('#notificar-' + id + ' .panel-body').empty();
							$('#notificar-' + id + ' .panel-body').append( msg );
							$('#notificar-' + id ).collapse('show');
							return false;
						});
					}, 2000);
				}
			});
		/* } else {
			msg = '<?= is_professor() ? "O comentário deve ter, no mínimo, 50 caracteres!" : "É necessário digitar um comentário para enviar!" ?>';
			$('#msg-edit-comentario-' + id).text(msg);
		} */
	});
	$('.excluir-comentario').off('click');
	$('.excluir-comentario').on('click', function(event) {
		var confirmacao = window.confirm('Deseja excluir o comentário?');

		if(confirmacao == true) {
			var idComentario = $(this).attr('com_id');
			$.ajax({
				url: '<?php echo get_excluir_comentario_url(); ?>' + idComentario,
				method: 'POST',
				cache: false,
			})
			.done(function( msg ) {
				var id = <?php echo $questao_id; ?>;
				$.ajax({
					url: "<?php echo get_ajax_main_comentarios_questao_url();?>",
					method: "POST",
					cache: false,
					data: { id: id }
				})
				.done(function( msg ) {
					$( '#notificar-' + id + ' .panel-body' ).empty();
					var total = parseInt($('#total-comentario-' + id).text());
					total--;
					$('#comentario-questao-' + id + ' > span').text(total);
					$( '#notificar-' + id + ' .panel-body' ).append( msg );
					$('#notificar-' + id ).collapse("show");
				});
			});
		}
	});
});
</script>