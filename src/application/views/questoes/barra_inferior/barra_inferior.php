<div class="ibox-footer">
	<?php $this->view('questoes/rodape/rodape', array('questao' => $questao)); ?>
	
	<div id="info-<?= $questao['que_id'] ?>" class="panel panel-default notificacao">
		<div class="panel-heading">
			<h5 class="panel-title">
				<?php if(is_visitante()) : ?>
					<span class="questao-acao-link"> 
						<a data-toggle="modal" data-target="#login-modal" href="#" class="link-questao-barra-inferior pull-left" style="margin-right: 20px">
							<i class="fa fa-flag"></i> Notificar Erro</a>
					</span>

					<?php if(!$is_resolver_simulado) : ?>
						<span class="questao-acao-link">
							<a data-toggle="modal" data-target="#login-modal" href="#" class="link-questao-barra-inferior pull-left" style="margin-right: 20px">
								<i class="fa fa-book"></i> Adicionar a Caderno</a>
						</span>
						
						<span class="questao-acao-link">
							<a data-toggle="modal" data-target="#login-modal" href="#" class="link-questao-barra-inferior pull-left" style="margin-right: 20px">
								<i class="fa fa-comment"></i>
								Comentários (<span id="total-comentario-<?php echo $questao['que_id']; ?>"><?php echo $questao['total_comentarios']?></span>)
							</a>
						</span>

						<span class="questao-acao-link">
							<a data-toggle="modal" data-target="#login-modal" href="#" class="link-questao-barra-inferior link-estatisticas pull-left">
								<i class="fa fa-pie-chart"></i> Estatísticas
							</a>
						</span>

						<span class="questao-acao-link">
							<a data-toggle="modal" data-target="#login-modal" href="#" class="link-questao-barra-inferior link-estatisticas pull-left">
								<i class="fa fa-video-camera"></i> Comentário em Vídeo
							</a>
						</span>

						<span class="questao-acao-link">
							<a data-toggle="modal" data-target="#login-modal" href="#" class="link-questao-barra-inferior link-anotacoes pull-left">
								<i class="fa fa-list-ol"></i> Anotações
							</a>
						</span>

					<?php endif ?>

				<?php else : ?>
					<span class="questao-acao-link">
						<a data-id="<?php echo $questao['que_id']; ?>" class="link-questao-barra-inferior link-notificacao pull-left">
							<i class="fa fa-flag"></i> Notificar Erro</a>
					</span>
					
					<?php if(!$is_resolver_simulado || $exibir_resultado || tem_acesso(array(ADMINISTRADOR, PROFESSOR, CONSULTOR))) : ?>
						
						<?php if(tem_acesso(array(/*ADMINISTRADOR, PROFESSOR, CONSULTOR, ASSINANTE_SQ*/ USUARIO_LOGADO))) : ?>
						
								<span class="questao-acao-link">
									<a data-id="<?php echo $questao['que_id']; ?>" class="link-questao-barra-inferior link-adicionar-caderno pull-left" >
										<i class="fa fa-book"></i> Adicionar a Caderno</a>
								</span>
							<?php else : ?>
						
								<span class="questao-acao-link">
									<a data-toggle="modal" href="#login-modal" class="link-questao-barra-inferior link-adicionar-caderno pull-left" >
									<i class="fa fa-book"></i> Adicionar a Caderno</a>
								</span>
						
						<?php endif; ?>

						<span class="questao-acao-link">
							<a data-id="<?php echo $questao['que_id']; ?>" class="link-questao-barra-inferior link-comentario pull-left"
								id="comentario-questao-<?php echo $questao['que_id']; ?>">
								<i class="fa fa-comment"></i>
								Comentários (<span id="total-comentario-<?php echo $questao['que_id']; ?>"><?php echo $questao['total_comentarios']?></span>)
							</a>
						</span>

						<span class="questao-acao-link">
							<a data-id="<?php echo $questao['que_id']; ?>" class="link-questao-barra-inferior link-estatisticas pull-left"
							id="estatisticas-questao-<?php echo $questao['que_id']; ?>">
							<i class="fa fa-pie-chart"></i> Estatísticas</a>
						</span>

						<span class="questao-acao-link">

							<?php if($questao['comentario_video']): ?>
							
								<?php if(AcessoGrupoHelper::comentarios_acessar($simulado_id, $questao_id)): ?>
									<a data-id="<?php echo $questao['que_id']; ?>" class="link-questao-barra-inferior link-comentario-video pull-left" href="<?= $questao['comentario_video']['cvi_url'] ?>"
									id="comentario-video-<?php echo $questao['que_id']; ?>" data-lity="">
									<i class="fa fa-video-camera"></i> Comentário em Vídeo</a>
								<?php else: ?>
									<a data-toggle="modal" href="#modal-assine-sq" class="link-questao-barra-inferior link-comentario-video pull-left">
									<i class="fa fa-video-camera"></i> Comentário em Vídeo</a>
								<?php endif; ?>
							
							<?php else: ?>

								<span class="link-questao-barra-inferior link-comentario-video-desativado pull-left"
								id="comentario-video-<?php echo $questao['que_id']; ?>">
								<i class="fa fa-video-camera"></i> Comentário em Vídeo</span>

							<?php endif; ?>
						</span>

						<span class="questao-acao-link">
							<a data-id="<?php echo $questao['que_id']; ?>" class="link-questao-barra-inferior link-anotacoes-listagem pull-left"
							href="#modal-anotacoes-questao" data-id="<?= $questao['que_id'] ?>" data-toggle="modal">
							<i class="fa fa-list-ol"></i> Anotações</a>
						</span>
					
					<?php endif ?>

				<?php endif; ?>

			</h5>
		</div>
		
		
		<div id="notificar-<?php echo $questao['que_id'];?>" style="display: none;">
			<div class="panel-body"></div>
		</div>
	</div>
</div>