<?php 

KLoader::helper("AcessoGrupoHelper");

$tem_acesso = AcessoGrupoHelper::comentarios_acessar($simulado_id, $questao_id);

// necessário pq professores tem acesso, mas não podem comentar se outro professor já tiver comentado	
$pode_comentar = true;
?>
<div>
	<h2>Comentários:</h2>
	<?php
	if($comentarios_professores) {
	    $primeiro_destacado = true;
		foreach ($comentarios_professores as $comentario) {
			echo get_comentario_box($comentario, $comentarios_avaliacoes[$comentario['com_id']], $primeiro_destacado, $tem_acesso);
			$primeiro_destacado = false;
		}
	}

	if($comentarios_alunos)	{
		foreach ($comentarios_alunos as $comentario) {
			echo get_comentario_box($comentario, $comentarios_avaliacoes[$comentario['com_id']]);
		}
	}

	if(is_professor() && !$professor_pode_comentar) {
		$texto = "Você não pode adicionar comentários, pois esta questão já foi comentada por outro professor.";
		ui_alerta($texto, ALERTA_INFO);
		$pode_comentar = false;
	}
	?>
</div>

<?php if($pode_comentar) : ?>
<div id="summernote-<?php echo $questao_id ?>"></div>

<div>
	<a href='' data-id="<?php echo $questao_id ?>" class='btn btn-primary comentar' data-loading-text="Enviando..."><i class='fa fa-check'></i>&nbsp;Enviar</a> <span id="msg-add-comentario-<?= $questao_id ?>"></span>
</div>
<?php endif; ?>

<?php 

    $this->load->view(get_editar_comentario_url(), $data);

?>