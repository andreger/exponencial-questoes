<script>

function atualizar_categorias_notificacao(elem)
{
	var caderno_id = $(elem  + ' .caderno-existente .notificacao-caderno').val();

	$.get('/questoes/cadernos_xhr/listar_categorias_caderno/' + caderno_id, function(data) {
		var ids = $.trim(data).split(',');
		$(elem + ' .caderno-existente .categorias-multiselect').val(ids).trigger('chosen:updated');
	});
}

$(function() {

	notificacao_visivel = [];
	cadernos_visivel = [];
	comentario_visivel = [];
	estatisticas_visivel = [];

	$('.link-notificacao').click(function (event) {
		event.preventDefault();

		var id = $(this).data('id');
		var item_id = "#notificar-" + id; 

		if(notificacao_visivel[id] == undefined) {
			notificacao_visivel[id] = false;
		}
		
		cadernos_visivel[id] = false;
		comentario_visivel[id] = false;
		estatisticas_visivel[id] = false;

		if(notificacao_visivel[id]) {	
			$(item_id).hide();
			notificacao_visivel[id] = false;
		}
		else {
			notificacao_visivel[id] = true;
			
			$(item_id + ' .panel-body').html("Carregando...");
			$(item_id).show();
			$.ajax({
				url: "<?php echo get_ajax_main_form_notificacao_url();?>",
				method: "POST",
				cache: false,
				data: { id: id }
			})
			.done(function( msg ) {
				$(item_id + ' .panel-body').html(msg);
				$('html, body').animate({
					scrollTop: $( '#info-' + id ).offset().top - 100
				}, 500);
			});
		}
	});

	$('.link-adicionar-caderno').click(function (event) {
		event.preventDefault();
		
		var id = $(this).data('id');
		var item_id = "#notificar-" + id; 

		if(cadernos_visivel[id] == undefined) {
			cadernos_visivel[id] = false;
		}

		notificacao_visivel[id] = false;
		comentario_visivel[id] = false;
		estatisticas_visivel[id] = false;
		
		if(cadernos_visivel[id]) {
			$(item_id).hide();
			cadernos_visivel[id] = false;
		}
		else {
			cadernos_visivel[id] = true;
			
			$(item_id + ' .panel-body').html("Carregando...");
			$(item_id).show();
			$.ajax({
				url: "<?php echo get_ajax_cadernos_adicionar_caderno_questao_url();?>",
				method: "POST",
				cache: false,
				data: { id: id }
			})
			.done(function( msg ) {
				$(item_id + ' .panel-body').html(msg);
				atualizar_categorias_notificacao(item_id);
				$('html, body').animate({
					scrollTop: $( '#info-' + id ).offset().top - 100
				}, 500);
			});
		}
	});

	$('.link-comentario').click(function(event) {
		event.preventDefault();

		var id = $(this).data('id');
		var item_id = "#notificar-" + id; 

		if(comentario_visivel[id] == undefined) {
			comentario_visivel[id] = false;
		}
		
		notificacao_visivel[id] = false;
		cadernos_visivel[id] = false;
		estatisticas_visivel[id] = false;
		
		if(comentario_visivel[id]) {
			$(item_id).hide();
			comentario_visivel[id] = false;
		}
		else {
			comentario_visivel[id] = true;
			
			$(item_id + ' .panel-body').html("Carregando...");
			$(item_id).show();
			$.ajax({
				url: "<?php echo get_ajax_main_comentarios_questao_url($simulado_id);?>",
				method: "POST",
				cache: false,
				data: { id: id }
			})
			.done(function( msg ) {
				$(item_id + ' .panel-body').html(msg);
				$('html, body').animate({
					scrollTop: $( '#info-' + id ).offset().top - 100
				}, 500);
			});
		}
	});
	
	$('.link-estatisticas').click(function(event) {
		event.preventDefault();

		var id = $(this).data('id');
		var item_id = "#notificar-" + id; 

		if(estatisticas_visivel[id] == undefined) {
			estatisticas_visivel[id] = false;
		}
		
		notificacao_visivel[id] = false;
		cadernos_visivel[id] = false;
		comentario_visivel[id] = false;
		
		if(estatisticas_visivel[id]) {
			$(item_id).hide();
			estatisticas_visivel[id] = false;
		}
		else {
			estatisticas_visivel[id] = true;
			
			$(item_id + ' .panel-body').html("Carregando...");
			$(item_id).show();
			$.ajax({
				url: "<?php echo get_ajax_main_estatisticas_questao_url();?>",
				method: "POST",
				cache: false,
				data: { id: id }
			})
			.done(function( msg ) {
				$(item_id + ' .panel-body').html(msg);
				$('html, body').animate({
					scrollTop: $( '#info-' + id ).offset().top - 100
				}, 500);
			});
		}
	});

});

</script>