<?php
/**
 * Gera o botão de limpar a formatação dos textos das alternativas de uma questão, caso possua as permissões necessárias
 * Para funcionar é necessário incluir a ação de remover, ver em listar_questoes_professores, revisar_questoes, resolver_questoes
 *  @since J5
 */ 
?>
<?php if(AcessoGrupoHelper::limpar_formatacao()) : ?>
    <a class="btn btn-outline btn-default btn-remover-formatacao" href="#" data-id="<?= $questao['que_id'] ?>" title="Remove a formatação dos textos das alternativas"><i class="fa fa-eraser"></i> Limpar Formatação</a>
<?php endif; ?>