<!-- Somente resolver_questoes -->
<?php if($pagina == PAGE_RESOLVER_QUESTOES) : ?>
    
    <?= get_questao_reprovada($questao); ?>

    <?php if(tem_acesso([ADMINISTRADOR, COORDENADOR_SQ])) : ?>
        <?php if($master_repetidas_div = get_questao_master_ou_repetida($questao['que_id'])) : ?>
        <div style="margin-bottom: 20px; text-align: right;">
            <span class="repetida">
                <?= $master_repetidas_div ?>
            </span>
        </div>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>

<div style="margin-bottom: 20px; text-align: right;">

    <!-- Somente resolver_questoes -->
    <?php if($pagina == PAGE_RESOLVER_QUESTOES) : ?>
        <?php if($questao['que_procedencia'] == QUESTAO_INEDITA): ?>
            <span style="float: left;color: #005fab;">- Questão Inédita, <?= converter_para_yyyy($questao['que_data_criacao']); ?> </span>
        <?php elseif($questao['que_procedencia'] == QUESTAO_ADAPTADA): ?>
            <span style="float: left;color: #005fab;">- Questão Adaptada, <?= converter_para_yyyy($questao['que_data_criacao']); ?> </span>
        <?php elseif($questao['que_procedencia'] == QUESTAO_FIXACAO): ?>
            <span style="float: left;color: #005fab;">- Exercício de Fixação, <?= converter_para_yyyy($questao['que_data_criacao']); ?> </span>
        <?php endif; ?>
    <?php endif; ?>

    <!-- Somente revisar -->
    <?php if($pagina == PAGE_REVISAR_QUESTOES || $pagina == PAGE_ATUALIZAR_CADERNO) : ?>
        <?php if($questao['que_procedencia'] == QUESTAO_INEDITA): ?>
            <span style="float: left;color: #005fab;">- Questão Inédita - Professor <?= get_usuario_nome_completo($questao['usu_id']) ?>, <?= converter_para_yyyy($questao['que_data_criacao']); ?> </span>
        <?php elseif($questao['que_procedencia'] == QUESTAO_ADAPTADA): ?>
            <span style="float: left;color: #005fab;">- Questão Adaptada - Professor <?= get_usuario_nome_completo($questao['usu_id']) ?>, <?= converter_para_yyyy($questao['que_data_criacao']); ?> </span>
        <?php elseif($questao['que_procedencia'] == QUESTAO_FIXACAO): ?>
            <span style="float: left;color: #005fab;">- Exercício de Fixação - Professor <?= get_usuario_nome_completo($questao['usu_id']) ?>, <?= converter_para_yyyy($questao['que_data_criacao']); ?> </span>
        <?php endif; ?>
    <?php endif; ?>
    
    <!-- Somente resolver_questoes -->
    <?php if($pagina == PAGE_RESOLVER_QUESTOES) : ?>
        <span class="dificuldade">
            <?= get_questao_ver_dificuldade($questao); ?>
        </span>
    <?php endif; ?>

    <!-- Somente resolver_questoes -->
    <?php if($pagina == PAGE_RESOLVER_QUESTOES) : ?>
        <?php if(isset($is_caderno) && pode_administrar_caderno_designado($caderno))  : ?>
            <a class="btn btn-outline btn-default" href="<?= get_excluir_questao_de_caderno_url($caderno['cad_id'], $questao['que_id'], $page) ?>" title="Remove questão do caderno atual" onclick="return confirm('Confirma a remoção da questão do caderno atual?')"><i class="fa fa-times"></i> Remover do Caderno</a>
        <?php endif; ?> 
    <?php endif; ?> 

    <!-- resolver questões e simulado e revisar -->
    <?php if($pagina == PAGE_RESOLVER_QUESTOES || $pagina == PAGE_REVISAR_QUESTOES || $pagina == PAGE_RESOLVER_SIMULADO || $pagina == PAGE_ATUALIZAR_CADERNO) : ?>
        <?php if(tem_acesso(array(ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, REVISOR)) && $questao['que_ativo'] == INATIVO) : ?>
        <span class="resposta-mensagem resposta-inativa" id="situacao-<?= $questao['que_id'] ?>" style="margin: 0 20px; color: orange"><i class="fa fa-exclamation-triangle"></i> Questão Inativa</span>
        <?php endif; ?>
    <?php endif; ?>

    <!-- resolver e revisar -->
    <?php if($pagina == PAGE_RESOLVER_QUESTOES || $pagina == PAGE_REVISAR_QUESTOES || $pagina == PAGE_ATUALIZAR_CADERNO) : ?>
        <?php if($questao['que_status'] == CANCELADA) : ?>
        <span class="questao-anulada"><img src="/wp-content/themes/academy/images/mensagem-anulada.png"> Questão Anulada</span>
        <?php endif; ?>
    <?php endif; ?>
    
    <!-- resolver e revisar -->
    <?php if($pagina == PAGE_RESOLVER_QUESTOES || $pagina == PAGE_REVISAR_QUESTOES || $pagina == PAGE_ATUALIZAR_CADERNO) : ?>
        <?php if($questao['que_status'] == DESATUALIZADA) : ?>
        <?= get_botao_resposta('Questão Desatualizada', 'resposta-desatualizada.png', 'resposta-desatualizada'); ?>
        <?php endif; ?>
    <?php endif; ?>
    
    <!-- revisar -->
    <?php if($pagina == PAGE_REVISAR_QUESTOES) : ?>
        <?php if(!$is_resultado) : ?>
            <?php if(is_administrador() || is_coordenador_sq() || is_revisor_sq() || is_coordenador_coaching()) : ?>
            <span>Questão Relacionada : <?= form_dropdown('que_referencia_id', $questoes_combo, isset($questao['que_referencia_id']) ? $questao['que_referencia_id'] : null, 'class="form-control m-b questao-relacionada-combo" data-id="' . $questao['que_id']  . '"')?></span>										
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>

    <!-- resolver questões e simulado e revisar -->
    <?php if($pagina == PAGE_RESOLVER_QUESTOES || $pagina == PAGE_REVISAR_QUESTOES || $pagina == PAGE_RESOLVER_SIMULADO || $pagina == PAGE_ATUALIZAR_CADERNO) : ?>
        <?php if(AcessoGrupoHelper::editar_questao($questao['que_id'])) : ?>
            <a class="btn btn-outline btn-default btn-edicao-rapida" href="#" data-id="<?= $questao['que_id'] ?>"><i class="fa fa-pencil"></i> Edição Rápida </a>
        <?php endif; ?>
    <?php endif; ?>

    <!-- resolver e revisar -->
    <?php if($pagina == PAGE_RESOLVER_QUESTOES || $pagina == PAGE_REVISAR_QUESTOES || $pagina == PAGE_ATUALIZAR_CADERNO) : ?>
        <?php $this->view('questoes/barra_superior/limpar_formatacao') ?>
    <?php endif; ?>
    
    <!-- resolver e revisar -->
    <?php if($pagina == PAGE_RESOLVER_QUESTOES || $pagina == PAGE_REVISAR_QUESTOES || $pagina == PAGE_RESOLVER_SIMULADO || $pagina == PAGE_ATUALIZAR_CADERNO) : ?>
        <?php if(is_administrador() || is_coordenador_sq() || is_revisor_sq() || is_coordenador_coaching()) : ?>
            
            <?php if(AcessoGrupoHelper::editar_questao($questao['que_id'])) : ?>
                <a class="btn btn-outline btn-default" href="<?php echo get_editar_questao_url($questao) ?>"><i class="fa fa-pencil"></i> Editar Questão </a>
            <?php endif; ?>
        
            <?php if(isset($questao['que_qcon_id']) && $questao['que_qcon_id']) : ?>
                <a target="_blank" class="btn btn-outline btn-default" href="<?php echo get_qc_questao_url($questao['que_qcon_id']) ?>"><i class="fa fa-search"></i></a>
            <?php endif; ?>
            
            <a class="btn btn-outline btn-default" href="<?= get_historico_questao_url($questao['que_id']) ?>"><i class="fa fa-eye"></i> Histórico </a>

        <?php endif; ?>
    <?php endif; ?>
</div>

<div id="edicao-rapida-<?= $questao['que_id']?>" style="display:none; background-color: #eee; padding: 20px"></div>

<!-- resolver -->
<?php if($pagina == PAGE_RESOLVER_QUESTOES) : ?>
    <?php if($acertou_errou = get_questao_acertou_errou_str(get_current_user_id(), $questao['que_id'])) : ?>
    <div style="margin-bottom: 20px; text-align: right;">

        <?php if($check_acertos == 1) : ?>
        <?= $acertou_errou; ?>
        <?php endif; ?>
        
    </div>
    <?php endif; ?>
<?php endif; ?>