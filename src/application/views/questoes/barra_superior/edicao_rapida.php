<?php if($is_associada_simulado_ativo) : ?>
	<div id="form-alerta-<?= $questao['que_id'] ?>" class="col-sm-12">
		<?php 
			$texto = "A disciplina não pode ser alterada, pois já está associada a um simulado ativo ou inconsistente.";
			ui_alerta($texto, ALERTA_INFO);
		?>
	</div>
<?php endif; ?>
<div id="form-resp-<?= $questao['que_id'] ?>" class="col-sm-12" style="display: none;"></div>
<div style="clear:both"></div>
<form id="form-<?= $questao['que_id'] ?>">
<?php $respostas = get_resposta_combo($questao['que_tipo']) ?>
<input type="hidden" name="que_id" value="<?= $questao['que_id'] ?>">
<div class="form-group">
	<div class="col-sm-3">
		Disciplina: <br>
		<?php $desativado = $is_associada_simulado_ativo ? 'disabled=disabled' : '' ?>
		<?php echo form_dropdown('dis_id', $disciplinas, $questao['dis_id'] ? $questao['dis_id'] : null, 'id="dis_id_'. $questao['que_id'] .'" class="form-control m-b" ' . $desativado); ?>
	</div>
	<!--<div class="col-sm-2">
		Dificuldade: <br>
		<?php //echo form_dropdown('que_dificuldade', $dificuldades, $questao['que_dificuldade'] ? $questao['que_dificuldade'] : null, 'id="que_dificuldade_'. $questao['que_id'] .'" class="form-control m-b"'); ?>
	</div>-->
	<div class="col-sm-3">
		Status: <br>
		<?php echo form_dropdown('que_status', $statuses, $questao['que_status'] ? $questao['que_status'] : null, 'id="que_status_'. $questao['que_id'] .'" class="form-control m-b"'); ?>
	</div>
	<div class="col-sm-3">
		Gabarito: <br>
		<?php echo form_dropdown('que_resposta', $respostas, $questao['que_resposta'] ? $questao['que_resposta'] : null, 'id="que_resposta_'. $questao['que_id'] .'" class="form-control m-b"'); ?>
	</div>
	<div class="col-sm-3">
		Situação: <br>
		<?php echo form_dropdown('que_ativo', $situacoes, $questao['que_ativo'] ? $questao['que_ativo'] : null, 'id="que_situacao_'. $questao['que_id'] .'" class="form-control m-b"'); ?>
	</div>
	<div class="col-sm-6">
		Assuntos: <br>
		<?php echo form_multiselect('ass_ids[]', $assuntos, $assuntos_selecionados, 'id="ass_ids_'. $questao['que_id'] .'" class="multiselect form-control m-b"'); ?>
		<div>
			<a id="arvore-btn" data-toggle="modal" href="#modal-arvore-assuntos">Árvore de assuntos</a>
		</div>	
	</div>

	<?php if(AcessoGrupoHelper::adicionar_comentarios_videos()): ?>

		<div class="col-sm-3">
			Professor Comentário em Vídeo: <br/>
			<?php echo form_dropdown('cvi_user_id', $professores, $comentario_video['usu_id'], 'id="cvi_user_id" class="form-control"'); ?>
		</div>

		<div class="col-sm-3">
			URL Comentário em Vídeo: <br/>
			<?php echo form_input('cvi_url', $comentario_video['cvi_url'],'id="cvi_url" placeholder="Url do Vimeo" class="form-control m-b"'); ?>
		</div>

	<?php endif; ?>

	<div class="col-sm-12 text-right">
		<br>
		<a class='btn btn-primary' href="#" onclick="event.preventDefault();salvar_questao(<?= $questao['que_id'] ?>)">Salvar</a>
		<a class='btn btn-white' href="#" onclick="event.preventDefault();cancelar_edicao(<?= $questao['que_id'] ?>)">Cancelar</a>
	</div>
	</div>
	<div style="clear:both"></div>
</div>
</form>

<script>
$("#ass_ids_<?= $questao['que_id'] ?>").remoteChained({
    parents : "#dis_id_<?= $questao['que_id'] ?>",
    url : "<?php echo get_ajax_get_ass_id_url(true); ?>"
});

$("#ass_ids_<?= $questao['que_id'] ?>").chosen({
	disable_search_threshold: 10,
	no_results_text: "Nenhum assunto com:",
	placeholder_text_multiple: "Selecione alguns assuntos..."
});
$("#ass_ids_<?= $questao['que_id'] ?>").on("change", function(event, data) {
    $(this).trigger("chosen:updated");
});

function salvar_questao(id) {
	var situacao = $('#form-' + id + ' select[name=que_ativo]').val();
	var er_id = '#edicao-rapida-'+id;
	var data = $('#form-' + id).serialize();
	
	$('#form-' + id).hide();
	if($('#form-alerta-' + id)){
		$('#form-alerta-' + id).hide();
	}
	//$(er_id).html('Salvando...');
	$('#form-resp-' + id).html('<div class="col-sm-12">Salvando...</div>');
	$('#form-resp-' + id).show();


	$.post( "/questoes/questoes_xhr/edicao_rapida_questao", data, function(data) {

		if(data.trim() == "OK"){

			$(er_id).html('Questão editada com sucesso!');

			console.log(situacao + "id: " + id);

			if(situacao == 1) {
				$('#situacao-' + id).hide();
			}
			else {
				$('#situacao-' + id).show();
			}

			setTimeout(function(){ $(er_id).hide() }, 5000);

		}else{
			$('#form-resp-' + id).html(data);
			$('#form-' + id).show();
			if($('#form-alerta-' + id)){
				$('#form-alerta-' + id).show();
			}

		}

	});
}

function cancelar_edicao(id) {
	var er_id = '#edicao-rapida-'+id;
	$(er_id).hide();
}
</script>
<div id="modal-arvore-assuntos" class="modal fade" aria-hidden="true">
	<div id="p2" style="display:none" class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Selecionar Assuntos</h4>
			</div>
			<div class="modal-body">
				<div class="alert alert-info">
                	A quantidade de questões mostrada para cada assunto leva em conta o total de questões cadastradas em nossa base, sem filtros aplicados
            	</div>
				<div class="arvore-control">
					<a id="expand-all">Expandir tudo</a>
					<a id="collapse-all">Recolher tudo</a>
				</div>
			<div id="p2-corpo"></div>
			<div class="botoes" style="margin-top: 20px">
				<a class="btn primary btn-default" data-dismiss="modal" id="adicionar">Adicionar</a>
				<a class="btn primary btn-default" data-dismiss="modal" id="cancelar">Cancelar</a>
			</div>				
		</div>
	</div>
</div>
<script>
	dis_ids_name = '<?= $questao['que_id'] ? 'dis_id_'.$questao['que_id'] : '' ?>';
	ass_ids_name = '<?= $questao['que_id'] ? 'ass_ids_'.$questao['que_id'] : '' ?>';
</script>
<?php $this->view('modal/filtro_scripts') ?>