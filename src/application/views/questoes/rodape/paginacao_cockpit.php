<?php
    if(isset($caderno_id))
    {
        $url_ir_aleatoria = "/questoes/cadernos/ir_para_questao_aleatoria/{$caderno_id}/{$total}{$query_string}";
    }
    elseif(isset($simulado_id))
    {
        $uri_string = "";
        if($is_exibir_resultado)
        {
            $uri_string = "/1";
        }
        if($tipo_resultado)
        {
            $uri_string = ($uri_string?:"/0") . "/{$tipo_resultado}";
        }

        $query_string = $uri_string . $query_string;

        $url_ir_aleatoria = "/questoes/simulados/ir_para_questao_aleatoria/{$simulado_id}/{$total}{$query_string}";
    }
    elseif(isset($is_favoritas))
    {
        $url_ir_aleatoria = "/questoes/main/ir_para_questao_aleatoria/{$total}//{$is_favoritas}{$query_string}";
    }
    else
    {        
        $url_ir_aleatoria = "/questoes/main/ir_para_questao_aleatoria/{$total}/". urlencode( urlencode($uri) ) . "{$query_string}";
    }
?>
        
<?php if($pagina_atual > 0): ?>
    <a href="<?= $url . "/" . ($pagina_atual -1) . $query_string ?>" title="Ir para a questão anterior" class="btn-anterior px-4 tooltipster">
        <i class="fa fa-arrow-left fa-2x"></i>
    </a>
<?php endif; ?>

<?php if($pagina_atual < ($total -1)): ?>
    <a id="btn-proximo" href="<?= $url . "/" . ($pagina_atual +1) . $query_string ?>" title="Ir para a próxima questão" class="btn-proximo px-4 tooltipster">
        <i class="fa fa-arrow-right fa-2x"></i>
    </a>
<?php endif; ?>

<?php if($total > 1): ?>
    <a href="<?= $url_ir_aleatoria ?>" title="Ir para uma questão aleatória" class="btn-aleatorio px-4 tooltipster">
        <i class="fa fa-random fa-2x"></i>
    </a>
<?php endif; ?>

    <a data-toggle="modal" href="#modal-atalhos-navegacao" title="Botões de atalho" class="btn-atalhos px-4 tooltipster">
        <i class="fa fa-keyboard-o fa-2x"></i>
    </a>

