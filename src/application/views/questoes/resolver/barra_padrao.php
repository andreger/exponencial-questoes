<nav class="navbar navbar-static-top" style="margin-bottom: 0">
    <div class="btn-tools">
        <div class="col-lg-5">
            <?php if(isset($is_caderno)) : ?>
                <a class="btn btn-default" href="<?= get_embaralhar_questoes_url($caderno_id, TRUE) ?>" id="embaralhar_questoes" title="Ordem aleatória">Ordem Aleatória</a>
            <?php endif ?>

            <?php //if(!isset($is_simulado) && $check_filtros == 1) : ?>
            <a id="filtrar" class="btn btn-default" data-toggle="modal" href="#modal-form-filter" title="Selecionar questões de concursos"><i class="fa fa-filter"></i> Filtrar </a>
            <a class="btn btn-default"  id="limpar-botao" href="<?= $filtro_url ?>" title="Listar questões sem aplicação de filtros">Limpar Filtros</a>
            	<?php if($is_caderno && $has_filtro_caderno && !$is_caderno_comprado): ?>
				<a class="btn btn-default" id="remover-selecao" title="Remover questões listadas do caderno">Remover Seleção</a>
				<?php endif; ?>
            <?php //endif; ?>

            <?php if(tem_acesso(array(/*ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, CONSULTOR, REVISOR, ASSINANTE_SQ*/ USUARIO_LOGADO))) :?>
                <?php if(!isset($is_simulado) && !isset($is_caderno)) : ?>
                <a id="adicionar-a-caderno-btn" class="btn btn-default" data-toggle="modal" href="#modal-form-caderno" title="Adicionar todas as questões selecionadas a um caderno (novo ou existente)"><i class="fa fa-book"></i> Adicionar a Caderno </a>
                <?php endif; ?>
            <?php endif ?>

            <?php if(!isset($is_simulado) && !isset($is_caderno)) : ?>
            <a class="btn btn-default mobile-hide imprimir-btn" href="<?php echo get_imprimir_questoes_html_url(); ?>" title="Imprimir" target="_blank"><i class="fa fa-print"></i></a>
            <?php endif; ?>

            <?php if(isset($is_simulado)) : ?>
            <a class="btn btn-default mobile-hide imprimir-btn" href="<?php echo get_imprimir_meus_simulados_html_url($simulado); ?>" title="Imprimir" target="_blank"><i class="fa fa-print"></i></a>
            <?php endif; ?>

            <?php if(isset($is_caderno)) : ?>
            <a class="btn btn-default mobile-hide imprimir-btn" href="<?php echo get_imprimir_meus_cadernos_html_url($caderno); ?>" target="_blank"><i class="fa fa-print"></i></a>
            <?php endif; ?>
            <a id="aumentar-fonte" class="btn btn-default"><i class="fa fa-font"></i> + </a>
            <a id="diminuir-fonte" class="btn btn-default"><i class="fa fa-font"></i> - </a>

            <?php if($questoes && ($has_filtro || isset($caderno_id))  && AcessoGrupoHelper::copiar_ids_questoes()) :?>
                <a id="copiar_ids" name="copiar_ids" class="btn btn-default tooltipster" title="Copia os ids de todas as questões presentes na busca"><i class="fa fa-copy"></i></a>
            <?php endif ?>

            <div class="btn-group dropdown">
                <a data-toggle="dropdown" class="btn btn-default dropdown-toggle tooltipster" title="Alterar modo de visualização" aria-expanded="false"><i class="fa fa-eye"></i>  <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a class="tooltipster-right mrq" data-modo="<?= MODO_QUESTOES_PADRAO ?>" href="#" title="Modo Normal"><i class="fa fa-book"></i></a></li>
                    <li><a class="tooltipster-right mrq" data-modo="<?= MODO_QUESTOES_COCKPIT ?>" href="#" title="Modo Cockpit"><i class="fa fa-exchange"></i></a></li>
                </ul>
            </div>

        </div>

        <div class="col-lg-2 mobile-margin-20-t-b">
            <?php if($caderno_id) : ?>
                <span><?= get_tema_image_tag('cronometro1.png') ?></span>
                <span class="timer" id="timer"></span>
                <span><a href="#" id="timer-pausar"><?= get_tema_image_tag('cronometro2.png') ?></a></span>
                <span><a href="#" id="timer-continuar" style="display: none"><?= get_tema_image_tag('cronometro3.png') ?></a></span>
            <?php endif ?>
        </div>

        <?php if($is_usa_cockpit): ?>

            <div class="col-lg-5">
                Questão <strong><?php echo numero_inteiro($questao['numero']) ?></strong> de <strong><?php echo numero_inteiro($total) ?></strong>
            </div>

        <?php else: ?>

            <div class="col-lg-3">
                <span>Questões por página:</span>
                <?php echo form_dropdown('limite', $questoes_por_pagina_combo, $limite, "id='qpp' class='form-control m-b' style='width:70px;display:initial'") ?>
            </div>

            <?php if(isset($total)) :?>
                <div class="col-lg-2">
                    Questões: <strong><?php echo numero_inteiro($total) ?></strong>
                </div>
            <?php endif; ?>

        <?php endif; ?>
    </div>

    <div class="aa btn-tools">
        <div class="col-lg-12">
            <?php $this->view('templates/filtros_selecionados') ?>
        </div>
    </div>
</nav>