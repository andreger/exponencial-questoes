<nav class="navbar navbar-static-top" style="margin-bottom: 0">
    <div class="btn-tools">

        <?php if($is_usa_cockpit): ?>

            <div class="col-lg-5">
                Questão <strong><?php echo numero_inteiro($questao['numero']) ?></strong> de <strong><?php echo numero_inteiro($total) ?></strong>
            </div>

        <?php else: ?>

            <div class="col-lg-3">
                <span>Questões por página:</span>
                <?php echo form_dropdown('limite', $questoes_por_pagina_combo, $limite, "id='qpp' class='form-control m-b' style='width:70px;display:initial'") ?>
            </div>

            <?php if(isset($total)) :?>
                <div class="col-lg-2">
                    Questões: <strong><?php echo numero_inteiro($total) ?></strong>
                </div>
            <?php endif; ?>

        <?php endif; ?>

        

        <div class="col-lg-2 mobile-margin-20-t-b">
            <?php if($caderno_id) : ?>
                <span><?= get_tema_image_tag('cronometro1.png') ?></span>
                <span class="timer" id="timer"></span>
                <span><a href="#" id="timer-pausar"><?= get_tema_image_tag('cronometro2.png') ?></a></span>
                <span><a href="#" id="timer-continuar" style="display: none"><?= get_tema_image_tag('cronometro3.png') ?></a></span>
            <?php endif ?>
        </div>

        <div class="col-lg-5 text-right">

            
            <div class="btn-group dropdown">
                <a data-toggle="dropdown" class="btn btn-default dropdown-toggle tooltipster" title="Alterar modo de visualização" aria-expanded="false"><i class="fa fa-eye"></i>  <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a class="tooltipster-right mrq" data-modo="<?= MODO_QUESTOES_PADRAO ?>" href="#" title="Modo Normal"><i class="fa fa-book"></i></a></li>
                    <li><a class="tooltipster-right mrq" data-modo="<?= MODO_QUESTOES_COCKPIT ?>" href="#" title="Modo Cockpit"><i class="fa fa-exchange"></i></a></li>
                </ul>
            </div>
            

            <?php if(isset($is_caderno)) : ?>
                <a onclick="carrega_modal_indice(<?= $caderno['cad_id'] ?>, 1)" data-toggle="modal" class="btn btn-default tooltipster link-indice" title="Índice" data-target="#modal-indice"><i class="fa fa-list"></i></a>
            <?php endif; ?>

            <?php //if(!isset($is_simulado) && $check_filtros == 1) : ?>
                <div class="btn-group dropdown">
                    <a data-toggle="dropdown" class="btn btn-default dropdown-toggle tooltipster" title="Filtro" aria-expanded="false"><i class="fa fa-filter"></i>  <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a id="filtrar" class="tooltipster-right" data-toggle="modal" href="#modal-form-filter" title="Filtrar - Selecionar questões de concursos"><i class="fa fa-filter"></i></a></li>
                        <li><a class="tooltipster-right" id="limpar-botao" href="<?= $filtro_url ?>" title="Limpar Filtros - Listar questões sem aplicação de filtros"><i class="fa fa-eraser"></i></a></li>
                        <li>
                        	<?php if($is_caderno && $has_filtro_caderno && !$is_caderno_comprado): ?>
		    				<a class="tooltipster-right" id="remover-selecao" title="Remover questões listadas do caderno"><i class="fa fa-trash"></i></a>
		    				<?php endif; ?>
    					</li>
                    </ul>
                </div>
            <?php //endif; ?>

            <div class="btn-group dropdown">
                <a data-toggle="dropdown" class="btn btn-default dropdown-toggle tooltipster" title="Alterar tamanho da fonte" aria-expanded="false"><i class="fa fa-font"></i>  <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a id="aumentar-fonte" class="tooltipster-right" title="Aumentar o tamanho da fonte"><i class="fa fa-font"></i> + </a></li>
                    <li><a id="diminuir-fonte" class="tooltipster-right" title="Diminuir o tamanho da fonte"><i class="fa fa-font"></i> - </a></li>
                </ul>
            </div>

            <div class="btn-group dropdown">
                <a data-toggle="dropdown" class="btn btn-default dropdown-toggle tooltipster" title="Mais ..." aria-expanded="false"><i class="fa fa-ellipsis-h"></i>  <span class="caret"></span></a>
                <ul class="dropdown-menu">

                    <?php if(isset($is_caderno)) : ?>
                        <li><a class="tooltipster-right" href="<?= get_embaralhar_questoes_url($caderno_id, TRUE) ?>" id="embaralhar_questoes" title="Ordem aleatória"><i class="fa fa-random"></i></a></li>
                    <?php endif ?>

                    <?php if(tem_acesso(array(/*ADMINISTRADOR, COORDENADOR_SQ, PROFESSOR, CONSULTOR, REVISOR, ASSINANTE_SQ*/ USUARIO_LOGADO))) :?>
                        <?php if(!isset($is_simulado) && !isset($is_caderno)) : ?>
                            <li><a id="adicionar-a-caderno-btn" class="tooltipster-right" data-toggle="modal" href="#modal-form-caderno" title="Adicionar todas as questões selecionadas a um caderno (novo ou existente)"><i class="fa fa-book"></i></a></li>
                        <?php endif; ?>
                    <?php endif ?>

                    <?php if(!isset($is_simulado) && !isset($is_caderno)) : ?>
                        <li><a class="imprimir-btn tooltipster-right" href="<?php echo get_imprimir_questoes_html_url(); ?>" title="Imprimir" target="_blank"><i class="fa fa-print"></i></a></li>
                    <?php endif; ?>

                    <?php if(isset($is_simulado)) : ?>
                        <li><a class="imprimir-btn tooltipster-right" href="<?php echo get_imprimir_meus_simulados_html_url($simulado); ?>" title="Imprimir" target="_blank"><i class="fa fa-print"></i></a></li>
                    <?php endif; ?>

                    <?php if(isset($is_caderno)) : ?>
                        <li><a class="imprimir-btn tooltipster-right" href="<?php echo get_imprimir_meus_cadernos_html_url($caderno); ?>" title="Imprimir" target="_blank"><i class="fa fa-print"></i></a></li>
                    <?php endif; ?>

                    <?php if($questoes && ($has_filtro || isset($caderno_id)) && AcessoGrupoHelper::copiar_ids_questoes()) :?>
                        <li><a id="copiar_ids" href="#" class="tooltipster-right" title="Copia os ids de todas as questões presentes na busca"><i class="fa fa-copy"></i></a></li>
                    <?php endif ?>

                    <?php if(isset($is_caderno)) : ?>     
                        <li><a data-toggle="modal" href="#modal-info-caderno" class="tooltipster-right btn-info-caderno" title="Informações" onclick="carrega_modal_info_caderno(<?= $caderno['cad_id'] ?>)"><i class="fa fa-pie-chart"></i></a></li>

                        <?php if(pode_compartilhar_caderno($caderno)) : ?>
                            <li><a data-toggle="modal" class="compartilhar-caderno tooltipster-right" title="Compartilhar" data-id="<?= $caderno['cad_id'] ?>" data-nome="<?= $caderno['cad_nome'] ?>" data-target="#modal-compartilhar-caderno"><i class="fa fa-share-alt"></i></a></li>
                        <?php endif ?>

                        <li><a data-toggle="modal" href="#modal-filtros-caderno" class="filtros-caderno tooltipster-right" title="Filtros aplicados" onclick="carrega_modal_filtros_caderno(<?= $caderno['cad_id'] ?>)"><i class="fa fa-search"></i></a></li>
                        
						<?php if(pode_editar_caderno($caderno)) : ?>
                        	<li><a class="sq-mecad-duplicar tooltipster-right" title="Atualizar Caderno" href="<?= get_atualizar_caderno_url($caderno['cad_id']) ?>"><i class="fa fa-edit"></i></a></li>
                        <?php endif; ?>
                        
                    <?php endif ?>

                </ul>
            </div>

        </div>

    </div>

    <div class="aa btn-tools">
        <div class="col-lg-12">
            <?php $this->view('templates/filtros_selecionados') ?>
        </div>
    </div>
</nav>
<div id="modal-filtros-caderno" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class='modal-header'>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class='row resultado-header'>
					<h3 class='pull-left'>Filtros</h3>
				</div>
			</div>
			<div class='modal-body'></div>
		</div>
	</div>
</div>

<div id="modal-info-caderno" class="modal fade" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class='modal-header'>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<div class='row resultado-header'>
					<h3 class='pull-left'>Informações</h3>
				</div>
			</div>
			<div class='modal-body'></div>
		</div>
	</div>
</div>

<div id="modal-compartilhar-caderno" class="modal fade" aria-hidden="true">
	<?php $this->view(get_modal_compartilhar_caderno_view_url()); ?>
</div>

<?php if(isset($is_caderno)) : ?>
<?php echo form_open($filtro_url, "id='form-indice-caderno' method='get'"); ?>
<?php echo form_hidden('dis_ids'); ?>
<?php echo form_hidden('ass_ids'); ?>
<?php echo form_hidden('buscar', 'buscar'); ?>
<?php echo form_close(); ?>
<?php endif; ?>

<div id="modal-indice" class="modal fade" aria-hidden="true">
	<?php $this->view('modal/indice_caderno'); ?>
</div>
<?php $this->view("principal/cadernos/meus_cadernos/listagem_barra_acoes_scripts"); ?>
<script>
$('.dropdown-toggle').on('show.bs.dropdown', function () {
    gerar_tooltipster();
});
</script>