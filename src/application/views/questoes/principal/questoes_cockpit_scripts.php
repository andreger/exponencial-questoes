<script>
    $(document).ready(function(){

        adicionar_atalhos_teclado();

        /* Como em alguns modal's é possível digitar e usar atalhos padrões
         * então eles são removidos quando os modal's são exibidos e adicionados
         * de volta quando são escondidos
         */
        $(document).on('show.bs.modal', function() {
            remover_atalhos_teclado();
        });

        $(document).on('hide.bs.modal', function(){
            adicionar_atalhos_teclado();
        });

    });

    function adicionar_atalhos_teclado(){
        //Hotkeys do WP: $.hotkeys.add('atalho', funcao);
        //Hotkeys Atualizada:  $(document).bind('keyup', 'atalho', funcao);
        //Hotkeys: hotkeys('f5', function(event, handler){});

        //$.hotkeys.add('Ctrl+c', function(){
        //$(document).bind('keyup', 'Ctrl+c', function(e){
        hotkeys('Ctrl+h', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('.link-comentario').first().click();
        });

        //$.hotkeys.add('Ctrl+e', function(){
        //$(document).bind('keyup', 'Ctrl+e', function(e){
        hotkeys('Ctrl+e', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('.link-estatisticas').first().click();
        });

        //$.hotkeys.add('Ctrl+f', function(){
        //$(document).bind('keyup', 'Ctrl+f', function(e){
        hotkeys('Ctrl+u', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('.favorito').click();
        });

        //$.hotkeys.add('Ctrl+a', function(){
        //$(document).bind('keyup', 'Ctrl+a', function(e){
        hotkeys('Ctrl+a', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('.link-anotacoes').click();
        });

        //$.hotkeys.add('Ctrl+i', function(){
        //$(document).bind('keyup', 'Ctrl+i', function(e){
        hotkeys('Ctrl+i', function(event, handler){event.preventDefault(); event.stopPropagation();
            //$('.link-indice').click();
            $('.btn-info-caderno').click();
        });

        //$.hotkeys.add('Ctrl+right', function(){
        //$(document).bind('keyup', 'Ctrl+right', function(e){
        hotkeys('Ctrl+right', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('#btn-proximo')[0].click();
        });

        //$.hotkeys.add('Ctrl+left', function(){
        //$(document).bind('keyup', 'Ctrl+left', function(e){
        hotkeys('Ctrl+left', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('.btn-anterior')[0].click();
        });
        
        //$.hotkeys.add('Ctrl+up', function(){
        //$(document).bind('keyup', 'Ctrl+up', function(e){
        hotkeys('Ctrl+up', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('.btn-aleatorio')[0].click();
        });

        //$.hotkeys.add('Ctrl+p', function(){
        //$(document).bind('keyup', 'Ctrl+p', function(e){
        hotkeys('Ctrl+p', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('.imprimir-btn')[0].click();
        });

        //$.hotkeys.add('Ctrl+k', function(){
        //$(document).bind('keyup', 'Ctrl+k', function(e){
        hotkeys('Ctrl+k', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('.compartilhar-caderno').click();
        });

        //$.hotkeys.add('Ctrl+q', function(){
        //$(document).bind('keyup', 'Ctrl+q', function(e){
        hotkeys('Ctrl+q', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('.link-compartilhar-questao').click();
        });

        //$.hotkeys.add('Ctrl+y', function(){
        //$(document).bind('keyup', 'Ctrl+y', function(e){
        hotkeys('Ctrl+y', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('.filtros-caderno').click();
        });

        //$.hotkeys.add('Ctrl+2', function(e){
        //$(document).bind('keyup', 'Ctrl+2', function(e){
        hotkeys('Ctrl+2, Ctrl+98', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('#aumentar-fonte').click();
        });

        //$.hotkeys.add('Ctrl+1', function(e){
        //$(document).bind('keyup', 'Ctrl+1', function(e){
        hotkeys('Ctrl+1, Ctrl+97', function(event, handler){event.preventDefault(); event.stopPropagation();
            $('#diminuir-fonte').click();
        });
        hotkeys('*', function(event, handler){
            if(hotkeys.ctrl && hotkeys.isPressed(97)){
                event.preventDefault(); 
                event.stopPropagation();
                $('#diminuir-fonte').click();
            }else if(hotkeys.ctrl && hotkeys.isPressed(98)){
                event.preventDefault(); 
                event.stopPropagation();
                $('#aumentar-fonte').click();
            }
        });
    }

    function remover_atalhos_teclado(){
        //Hotkeys do WP: $.hotkeys.remove('atalho');
        //Hotkeys Atualizado: $(document).unbind('keydown', 'atalho');
        //Hotkeys: hotkeys.unbind('a');

        //$.hotkeys.remove('Ctrl+c');
        //$(document).unbind('keydown', 'Ctrl+c');
        hotkeys.unbind('Ctrl+h');

        //$.hotkeys.remove('Ctrl+e');
        //$(document).unbind('keydown', 'Ctrl+e');
        hotkeys.unbind('Ctrl+e');

        //$.hotkeys.remove('Ctrl+f');
        //$(document).unbind('keydown', 'Ctrl+f');
        hotkeys.unbind('Ctrl+u');

        //$.hotkeys.remove('Ctrl+a');
        //$(document).unbind('keydown', 'Ctrl+a');
        hotkeys.unbind('Ctrl+a');

        //$.hotkeys.remove('Ctrl+i');
        //$(document).unbind('keydown', 'Ctrl+i');
        hotkeys.unbind('Ctrl+i');

        //$.hotkeys.remove('Ctrl+right');
        //$(document).unbind('keydown', 'Ctrl+right');
        hotkeys.unbind('right');

        //$.hotkeys.remove('Ctrl+left');        
        //$(document).unbind('keydown', 'Ctrl+left');
        hotkeys.unbind('Ctrl+left');

        //$.hotkeys.remove('Ctrl+up');
        //$(document).unbind('keydown', 'Ctrl+up');
        hotkeys.unbind('Ctrl+up');

        //$.hotkeys.remove('Ctrl+p');
        //$(document).unbind('keydown', 'Ctrl+p');
        hotkeys.unbind('Ctrl+p');

        //$.hotkeys.remove('Ctrl+k');
        //$(document).unbind('keydown', 'Ctrl+k');
        hotkeys.unbind('Ctrl+k');
        
        //$.hotkeys.remove('Ctrl+q');
        //$(document).unbind('keydown', 'Ctrl+q');
        hotkeys.unbind('Ctrl+q');

        //$.hotkeys.remove('Ctrl+y');
        //$(document).unbind('keydown', 'Ctrl+y');
        hotkeys.unbind('Ctrl+y');

        //$.hotkeys.remove('Ctrl+2');
        //$(document).unbind('keydown', 'Ctrl+2');
        hotkeys.unbind('Ctrl+2');

        //$.hotkeys.remove('Ctrl+1');
        //$(document).unbind('keydown', 'Ctrl+1');
        hotkeys.unbind('Ctrl+1');
        hotkeys.unbind('*');//Talvez essa daqui já fosse suficiente
    }
</script>