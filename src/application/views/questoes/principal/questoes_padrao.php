<?php if(isset($questoes)) : ?>
    <div class="ibox">
        <?php echo $links; ?>
    </div>
    <div class="row">
        <?php foreach ($questoes as $questao) : ?>
            <div id="questao_<?php echo $questao['que_id'];?>" class="col-lg-12 questao">
                <div class="ibox">
                    <?= $this->view('questoes/cabecalho/cabecalho', array('questao' => $questao), true); ?>

                    <div class="ibox-content">

                        <?= $this->view('questoes/barra_superior/barra_superior', ['pagina' => PAGE_RESOLVER_QUESTOES]); ?>

                        <?= $this->view('questoes/principal/corpo'); ?>

                        <?= $this->view('questoes/principal/alternativas', ['pagina' => PAGE_RESOLVER_QUESTOES]); ?>

                        <?php if(is_visitante()) : ?>
                        <a data-toggle="modal" data-target="#login-modal" href="#" class="btn btn-sm btn-primary m-t-n-xs">
                            Responder
                        </a>
                        <?php else : ?>

                            <?php if($questao['que_status'] != CANCELADA) :?>

                            <a href="#" data-id="<?php echo $questao['que_id']; ?>"
                                <?php echo isset($caderno_id) ? "data-caderno-id={$caderno_id}" : "" ?>
                                <?php echo isset($simulado_id) ? "data-simulado-id={$simulado_id}" : "" ?>
                                class="responder btn btn-sm btn-primary m-t-n-xs">
                                Responder
                            </a>

                            <?php //if($check_acertos == 1) :?>
                            <span class="msg-resposta"></span>
                            <?php //endif; ?>

                            <?php endif; ?>

                        <?php endif; ?>
                    </div>

                    <?= $this->view('questoes/barra_inferior/barra_inferior', array('questao' => $questao), true); ?>
                </div>
            </div>
    <?php endforeach;?>
    </div>
    <div class="ibox" style="padding-bottom: 10px">
        <?php echo $links; ?>
    </div>
<?php endif; ?>