<ul class="questao-opcoes texto-questao" data-que_id="<?= $questao['que_id'] ?>">
    <?php $disabled = $questao['que_status'] == CANCELADA ? "disabled" : "" ?>
    
    <?php if($questao['que_tipo'] == MULTIPLA_ESCOLHA || $questao['que_tipo'] == MULTIPLA_ESCOLHA_5 ) :?>
        <?php foreach ($questao['opcoes'] as $opcao) :?>

            <?php   
                    if($pagina == PAGE_RESOLVER_QUESTOES){

                        //Se for resolver cadernos verifica a configuração do usuário para exibir ou não o histórico de marcações
                        if(isset($caderno_id) && !$check_historico)
                        {
                            $checked = false;
                        }
                        elseif(isset($caderno_id) && $check_historico)
                        {
                            $checked = ( get_resposta_usuario($questao['que_id'], null, $caderno_id) == $opcao['qop_ordem'] );
                        }
                        else//Se não for cadernos usa as marcações em sessão
                        {
                            $checked = is_opcao_resposta_marcada($questao['que_id'], $opcao['qop_ordem']) ;
                        }


                    }elseif($pagina == PAGE_RESOLVER_SIMULADO){

                        $checked = false;

                        if($exibir_info_resposta && isset($respostas[$questao['que_id']])) {
                                $checked = ($respostas[$questao['que_id']] == $opcao['qop_ordem']);
                        }
                        //Pega do cookie porque na exibição Cockpit só é exibida uma questão por vez mas é tudo submetido somente no 'finalizar simulado'
                        elseif($_COOKIE['resp_sim_' . $simulado_id])
                        {
                            $resp_cookie = json_decode(stripslashes($_COOKIE['resp_sim_' . $simulado_id]), true);
                            $checked = ($resp_cookie[$questao['que_id']] == $opcao['qop_ordem']);
                        }

                    }

            ?>
            <?php if(trim($opcao['qop_texto']) != "" && trim($opcao['qop_texto']) != "<p><br></p>"): ?>
                <li>
                    <div class="radio i-checks">
                        <label for="qop_<?php echo $questao['que_id'] . $opcao['qop_ordem']; ?>">
                            <?php 
                            
                                $opcao_questao = $pagina == PAGE_RESOLVER_SIMULADO ? 'opcao_questao[' . $questao['que_id'] . ']' : 'opcao_questao_' . $questao['que_id'];

                                echo form_radio($opcao_questao, 
                                $opcao['qop_ordem'], 
                                $checked, $disabled . " id='qop_{$questao['que_id']}{$opcao['qop_ordem']}' class='questao_alternativa' data-que_id='{$questao['que_id']}'");?>
                        </label>
                        <span class="alternativa">	

                        <?= get_letra_opcao_questao($opcao['qop_ordem']) . ") " . formatar_opcao_questao($opcao['qop_texto']); ?>
                        </span>
                        
                    </div>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    <?php elseif($questao['que_tipo'] == CERTO_ERRADO) :?>
    	<?php   
    	    $opcao_questao = $pagina == PAGE_RESOLVER_SIMULADO ? 'opcao_questao[' . $questao['que_id'] . ']' : 'opcao_questao_' . $questao['que_id']; 
    	?>
        <li>
            <div class="radio i-checks">
                <label for="qop_<?php echo $questao['que_id']; ?>_certo">
                    <?php 
                        if($pagina == PAGE_RESOLVER_QUESTOES){
                            $checked = is_opcao_resposta_marcada($questao['que_id'], CERTO) ;
                        }elseif($pagina == PAGE_RESOLVER_SIMULADO){
                            
                            $checked = false;
                            
                            if($exibir_info_resposta && isset($respostas[$questao['que_id']]) &&
                                $respostas[$questao['que_id']] == CERTO) {
                                    $checked = true;
                                }
                                
                        }
                        
                        echo form_radio($opcao_questao, 
                            CERTO, 
                            $checked, 
                            $disabled . " id='qop_{$questao['que_id']}_certo' class='questao_alternativa' data-que_id='{$questao['que_id']}'");
                    ?>
                </label>
                <span class="alternativa">Certo</span>
            </div>
        </li>
        <li>
            <div class="radio i-checks">
                <label for="qop_<?php echo $questao['que_id']; ?>_errado">
                    <?php 
                        if($pagina == PAGE_RESOLVER_QUESTOES){
                            $checked = is_opcao_resposta_marcada($questao['que_id'], ERRADO) ;
                        }elseif($pagina == PAGE_RESOLVER_SIMULADO){
                            
                            $checked = false;
                            
                            if($exibir_info_resposta && isset($respostas[$questao['que_id']]) &&
                                $respostas[$questao['que_id']] == ERRADO) {
                                    $checked = true;
                                }
                                
                        }
                        
                        echo form_radio($opcao_questao, 
                            ERRADO, 
                            $checked, 
                            "id='qop_{$questao['que_id']}_errado' class='questao_alternativa' data-que_id='{$questao['que_id']}'");
                    ?>
                </label>
                <span class="alternativa">Errado</span>
            </div>
        </li>
    <?php endif; ?>
</ul>