<?php if(isset($questao)) : ?>
    <div class="row">
        
        <div id="questao_<?php echo $questao['que_id'];?>" class="col-lg-12 questao">
            <div class="ibox">
                
                <?= $this->view('questoes/cabecalho/cabecalho', array('questao' => $questao), true); ?>

                <div class="ibox-content">

                    <?php $this->view('questoes/barra_superior/barra_superior', ['pagina' => PAGE_RESOLVER_QUESTOES]); ?>

                    <?php $this->view('questoes/principal/corpo'); ?>

                    <?php $this->view('questoes/principal/alternativas', ['pagina' => PAGE_RESOLVER_QUESTOES]); ?>


                    <div class="row">

                        <div class="col-lg-5">

                            <?php if(is_visitante()) : ?>
                            <a data-toggle="modal" data-target="#login-modal" href="#" class="btn btn-sm btn-primary m-t-n-xs">
                                Responder
                            </a>
                            <?php else : ?>

                                <?php if($questao['que_status'] != CANCELADA) :?>

                                <a href="#" data-id="<?php echo $questao['que_id']; ?>"
                                    <?php echo isset($caderno_id) ? "data-caderno-id={$caderno_id}" : "" ?>
                                    <?php echo isset($simulado_id) ? "data-simulado-id={$simulado_id}" : "" ?>
                                    class="responder btn btn-sm btn-primary m-t-n-xs">
                                    Responder
                                </a>

                                <?php //if($check_acertos == 1) :?>
                                <span class="msg-resposta"></span>
                                <?php //endif; ?>

                                <?php endif; ?>

                            <?php endif; ?>

                        </div>

                        <div class="col-lg-3"></div>
                        
                        <div class="col-lg-4 text-align-right cockpit-navegacao">
                            <?= $links ?>
                        </div>

                    </div>

                </div>

                <?= $this->view('questoes/barra_inferior/barra_inferior', array('questao' => $questao), true); ?>
                
            </div>
        </div>
    
    </div>
<?php endif; ?>

<?php $this->view('questoes/principal/questoes_cockpit_scripts') ?>

<?php $this->view('questoes/barra_tarefas/anotacoes/modal') ?>

<?php $this->view('questoes/barra_tarefas/questao/compartilhar_modal') ?>

<?php $this->view('questoes/barra_tarefas/questao/atalhos_modal') ?>