<?php 
$config_usuario = get_config_aluno();
$ordem_cabecalho = explode(",", $config_usuario['ordem_cabecalho']);
?>

<div class="questao-cabecalho ibox-title">
	<div class="row">
		<div class="<?= 'col-lg-8'// $is_usa_cockpit?'col-lg-8':'col-lg-10' ?>">			
			<?php
			if(isset($is_revisar)) {
				echo form_checkbox('que_ids[]', $questao['que_id'], null, 'class="questao-opcao"');
			}
			?>			
			<b><?php echo $questao['numero'] ?> - </b>
			<?php if(!$simplificado) : ?>					
				<?php 
				/**
				 * Segundo Leonardo não haverá casos onde a mesma questão estará em anos diferentes
				 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2447
				 */
				?>
				<?php foreach($ordem_cabecalho as $opcao) {
					switch ($opcao) {
						
						case "ID":
							echo "<b>id:</b><wbr/> {$questao['que_id']}<wbr/>";
							break;

						case "Ano":
							if($questao['anos']){
								echo "<b> Ano:</b> {$questao['anos'][0]} ";
							}
							elseif ($questao['que_procedencia'] == QUESTAO_INEDITA || $questao['que_procedencia'] == QUESTAO_ADAPTADA) {
								echo "<b> Ano:</b> ". converter_para_yyyy($questao['que_data_criacao']);
							}
							break;
						
						case "Bancas":
							if($questao['bancas']){
								echo "<b> Bancas:</b> " . implode(", ", $questao['bancas']);
							}
							break;

						case "Órgãos":
							if($questao['orgaos']){
								echo "<b> Órgãos:</b> " . implode(", ", $questao['orgaos']);
							}
							break;

						case "Provas":
							if($questao['provas']){
								echo "<b> Provas:</b> " . implode(", ", $questao['provas']);
							}
							break;

						case "Disciplina":
							if ($questao['dis_ativo']) {
								$nome = $questao['sro_nome'] ? $questao['sro_nome'] : $questao['dis_nome'];
								echo "<b> Disciplina:</b><wbr/> {$nome}<wbr/>";
							}
							else{
								echo "<b> Disciplina:</b><wbr/> (Sem disciplina) <wbr/>";
							}
							break;
							
						case "Assuntos":
						    if($config_usuario['exibir_assuntos']){
								if($questao['dis_ativo'] == SIM) {
									$assuntos_a = [];
									foreach ($questao['assuntos'] as $assunto) {
										array_push($assuntos_a, $assunto['ass_nome']);
									}

									echo " <b>Assuntos:</b> " . implode(" - ", $assuntos_a);
								}
								else {
									echo " <b>Assuntos:</b> (Sem assunto)";
								}
							}
							break;
					}}?>
					<wbr/>
			<?php else: ?>
				<b>id:</b><wbr/> <?php echo "{$questao['que_id']}"; ?><wbr/>
			<?php endif; ?>
		</div>


		<div class="col-lg-4 text-align-right cockpit-acoes">
			<?php $this->view('questoes/cabecalho/barra_cockpit') ?>
		</div>
		<?php /*if($is_usa_cockpit): ?>
			<div class="col-lg-4 text-align-right cockpit-acoes">
				<?php $this->view('questoes/cabecalho/barra_cockpit') ?>
			</div>
		<?php else: ?>

			<div class="col-lg-2 text-align-right cabecalho-acoes">

				<div class="col-lg-3">
					<a class="tooltipster px-1 link-anotacoes link-anotacoes-listagem" href="#modal-anotacoes-questao" data-id="<?= $questao['que_id'] ?>" data-toggle="modal" title="Anotações">
						<i class="fa fa-list-ol fa-2x"></i>
					</a>
				</div>

				<div class="col-lg-3">
					<a class="tooltipster px-1 link-compartilhar-questao link-compartilhar-questao-listagem" href="#modal-compartilhar-questao" data-id="<?= $questao['que_id'] ?>" data-url="<?= $questao['que_url_reduzida']?:get_questao_url($questao['que_id']) ?>" data-toggle="modal" title="Compartihar questão">
						<i class="fa fa-share-alt fa-2x"></i>
					</a>
				</div>

				<?php if(!$esconder_favorita && is_usuario_logado()) : ?>
				<?php $style = is_favorita($questao['que_id'], get_current_user_id()) ? "fa-star" : "fa-star-o" ?>
					<div class="col-lg-3"><a class="favorito tooltipster" href="#" data-id="<?= $questao['que_id'] ?>" title="Favoritar questão"><i class="fa <?= $style ?>"></i></a></div>
				<?php endif; ?>
				
				<?php if(is_usuario_logado()) : ?>			
					<?php $style = is_questao_acompanhada($questao['que_id'], get_current_user_id()) ? "fa-bell" : "fa-bell-slash-o" ?>
					<div class="col-lg-3"><a class="acompanhar tooltipster" href="#" data-id="<?= $questao['que_id'] ?>" title="Acompanhar questão"><i id="acompanhar-<?= $questao['que_id'] ?>" class="fa <?= $style ?>"></i></a></div>
				<?php endif; ?>

			</div>

		<?php endif;*/ ?>
		
	</div>
</div>