<?php if(is_visitante()) : ?>
    
        <a class="tooltipster px-1" data-toggle="modal" data-target="#login-modal" href="#" title="Notificar erro">
            <i class="fa fa-flag fa-2x"></i>
        </a>    

    <?php if(!$is_resolver_simulado) : ?>
        
            <a class="tooltipster px-1" data-toggle="modal" data-target="#login-modal" href="#" title="Adicionar a caderno">
                <i class="fa fa-book fa-2x"></i>
            </a>
        
            <a class="tooltipster px-1" data-toggle="modal" data-target="#login-modal" href="#" title="Comentários">
                <i class="fa fa-comment fa-2x"></i>
            </a>
        
            <a class="tooltipster px-1" data-toggle="modal" data-target="#login-modal" href="#" title="Estatísticas">
                <i class="fa fa-pie-chart fa-2x"></i>
            </a>
                
            <a class="tooltipster px-1" data-toggle="modal" data-target="#login-modal" href="#" title="Comentário em Vídeo">
                <i class="fa fa-video-camera fa-2x"></i>
            </a>
            
            <a class="tooltipster px-1" data-toggle="modal" data-target="#login-modal" href="#" title="Anotações">
                <i class="fa fa-list-ol fa-2x"></i>
            </a>

            <a class="tooltipster px-1" data-toggle="modal" data-target="#login-modal" href="#" title="Compartilhar questão">
                <i class="fa fa-share-alt fa-2x"></i>
            </a>

            <a class="tooltipster px-1" data-toggle="modal" data-target="#login-modal" href="#" title="Favoritar questão">
                <i class="fa fa-star-o fa-2x"></i>
            </a>

            <a class="tooltipster px-1" data-toggle="modal" data-target="#login-modal" href="#" title="Acompanhar questão">
                <i class="fa fa-bell-slash-o fa-2x"></i>
            </a>

    <?php endif; ?>

<?php else: ?>
    
        <a class="tooltipster px-1 link-notificacao" href="#" data-id="<?= $questao['que_id'] ?>" title="Notificar erro">
            <i class="fa fa-flag fa-2x"></i>
        </a>

    <?php if(!$is_resolver_simulado || $exibir_resultado || tem_acesso(array(ADMINISTRADOR, PROFESSOR, CONSULTOR))) : ?>

        <?php if(tem_acesso(array(/*ADMINISTRADOR, PROFESSOR, CONSULTOR, ASSINANTE_SQ*/USUARIO_LOGADO))) : ?>
            
            <a class="tooltipster px-1 link-adicionar-caderno" href="#" data-id="<?= $questao['que_id'] ?>" title="Adicionar a caderno">
                <i class="fa fa-book fa-2x"></i>
            </a> 
        
        <?php else: ?>
            
            <a class="tooltipster px-1" data-toggle="modal" href="#modal-assine-sq" title="Adicionar a caderno">
                <i class="fa fa-book fa-2x"></i>
            </a>
            
        <?php endif; ?>
        
            <a class="tooltipster px-1 link-comentario" href="#info-<?= $questao['que_id'] ?>" data-id="<?= $questao['que_id'] ?>" title="Comentários">
                <i class="fa fa-comment fa-2x"></i>
            </a>
                
            <a class="tooltipster px-1 link-estatisticas" href="#info-<?= $questao['que_id'] ?>" data-id="<?= $questao['que_id'] ?>" title="Estatísticas">
                <i class="fa fa-pie-chart fa-2x"></i>
            </a>
        <?php if($questao['comentario_video']): ?>

            <?php if(AcessoGrupoHelper::comentarios_acessar($simulado_id, $questao['que_id'])): ?>
                                
                <a class="tooltipster px-1 link-comentario-video" href="<?= $questao['comentario_video']['cvi_url'] ?>" data-id="<?= $questao['que_id'] ?>" title="Comentário em Vídeo"
                    id="comentario-video-<?php echo $questao['que_id']; ?>" data-lity="">
                    <i class="fa fa-video-camera fa-2x"></i>
                </a>                

            <?php else: ?>
                
                <a class="tooltipster px-1 link-comentario-video" data-toggle="modal" href="#modal-assine-sq" title="Comentário em Vídeo">
                    <i class="fa fa-video-camera fa-2x"></i>
                </a>                

            <?php endif; ?>

        <?php else: ?>
            
            <span class="tooltipster px-1 link-comentario-video-desativado" title="Comentário em Vídeo" id="comentario-video-<?php echo $questao['que_id']; ?>">
                <i class="fa fa-video-camera fa-2x"></i>
            </span>            

        <?php endif; ?>

        <a class="tooltipster px-1 link-anotacoes" href="#modal-anotacoes-questao" data-id="<?= $questao['que_id'] ?>" data-toggle="modal" title="Anotações">
            <i class="fa fa-list-ol fa-2x"></i>
        </a>

        <a class="tooltipster px-1 link-compartilhar-questao" href="#modal-compartilhar-questao" data-id="<?= $questao['que_id'] ?>" data-url="<?= /*$questao['que_url_reduzida']?:*/get_questao_url($questao['que_id']) ?>" data-toggle="modal" title="Compartihar questão">
            <i class="fa fa-share-alt fa-2x"></i>
        </a>

        <?php if(!$esconder_favorita && tem_acesso(array(/*ADMINISTRADOR, PROFESSOR, CONSULTOR, REVISOR, ASSINANTE_SQ*/ USUARIO_LOGADO ))) : ?>
            
            <?php $style = is_favorita($questao['que_id'], get_current_user_id()) ? "fa-star" : "fa-star-o" ?>
            <a class="tooltipster px-1 favorito" href="#" data-id="<?= $questao['que_id'] ?>" title="Favoritar questão">
                <i class="fa <?= $style ?> fa-2x"></i>
            </a>

        <?php endif; ?>
        
        <?php $style = is_questao_acompanhada($questao['que_id'], get_current_user_id()) ? "fa-bell" : "fa-bell-slash-o" ?>
        <a class="tooltipster px-1 acompanhar" href="#" data-id="<?= $questao['que_id'] ?>" title="Acompanhar questão">
            <i id="acompanhar-<?= $questao['que_id'] ?>" class="fa <?= $style ?> fa-2x"></i>
        </a>

    <?php endif; ?>

<?php endif; ?>