<?php
    /**
     * Modal's relacionados as ações de anotações: modal com a listagem de anotações e modal de confirmação de remoção
     * @since L1
     */
?>
<div id="modal-anotacoes-questao" class="modal fade" aria-hidden="true">
    
    <?php $this->view('questoes/barra_tarefas/anotacoes/modal_dialog') ?>

</div>

<div id="modal-remover-anotacao" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                Tem certeza que deseja remover essa anotação?
            </div>
            <div class="modal-footer">
                <button id="confirma-remover-anotacao" type="button" class="btn btn-primary" data-id="" data-que_id="">Sim</button>
                <button type="button" data-dismiss="modal" class="btn btn-secondary">Não</button>
            </div>
        </div>
    </div>
</div>
<script>

    $(document).on("click", '.link-anotacoes', function(e){
        $("#msg-erro-anotacao").hide();
        $("#msg-sucesso-remocao-anotacao").hide();
        $("#msg-sucesso-anotacao").hide();
    });

    $(document).on("click", '.btn-cancelar-anotacao', function(e){
        e.preventDefault();
        $(".btn-salvar-anotacao").data("id", null);
        $("#qan_anotacao").val(null);
        $(".btn-salvar-anotacao").html('Salvar');
    });

    $(document).on("click", '.btn-salvar-anotacao', function(e){
        e.preventDefault();
        
        $('#form-anotacao').hide();
        $('#form-anotacao-salvando').show();

        var qan_anotacao = $("#qan_anotacao").val();
        if(!qan_anotacao || qan_anotacao.trim().length < 1){
            $("#msg-erro-anotacao").show();
            $("#msg-sucesso-anotacao").hide();
            $("#msg-sucesso-remocao-anotacao").hide();
            $('#form-anotacao').show();
            $('#form-anotacao-salvando').hide();
            return;
        }

        var id = $(this).data("id");
        var que_id = $(this).data("que_id");
        $.ajax({
            url: "/questoes/questoes_xhr/salvar_questao_anotacao",
            method: "POST",
            cache: false,
            data: { 
                    id: id,
                    que_id: que_id, 
                    qan_anotacao: qan_anotacao 
                }
        })
        .done(function( msg ) {
            $('#form-anotacao-anotacoes').html(msg);
            $(".btn-salvar-anotacao").data("id", null);
            $(".btn-salvar-anotacao").html('Salvar');
            $("#qan_anotacao").val(null);
            $("#msg-erro-anotacao").hide();
            $("#msg-sucesso-remocao-anotacao").hide();
            $("#msg-sucesso-anotacao").show();
            $('#form-anotacao').show();
            $('#form-anotacao-salvando').hide();
        });
    });

    $(document).on("click", ".editar-anotacao", function(e){
        e.preventDefault();
        $(".btn-salvar-anotacao").data("id", $(this).data("id"));
        $("#qan_anotacao").focus().val($(this).data("anotacao"));
        $(".btn-salvar-anotacao").html('Alterar');
    });

    $(document).on("click", ".remover-anotacao", function(e){
        e.preventDefault();
        $("#modal-remover-anotacao").modal("show");
        $("#confirma-remover-anotacao").attr("data-id", $(this).data("id"));
        $("#confirma-remover-anotacao").attr("data-que_id", $(this).data("que_id"));
    });
    
    $(document).on("click", "#confirma-remover-anotacao", function(e){
        var id = $(this).data("id");
        var que_id = $(this).data("que_id");

        $.ajax({
            url: "/questoes/questoes_xhr/remover_questao_anotacao",
            method: "POST",
            cache: false,
            data: { 
                    id: id,
                    que_id: que_id
                }
        })
        .done(function( msg ) {
            $('#form-anotacao-anotacoes').html(msg);
            $("#msg-sucesso-remocao-anotacao").show();
            $("#msg-sucesso-anotacao").hide();
            $("#msg-erro-anotacao").hide();
            $("#modal-remover-anotacao").modal("hide");
        });

    });

</script>