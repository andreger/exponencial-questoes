<?php if(isset($anotacoes) && count($anotacoes) > 0): ?>
    <?php $count = 1; ?>
    <?php foreach($anotacoes as $anotacao): ?>
        <div class="row">
            <div class="col-sm-10 text-align-left text-justify">
                <b><?= converter_para_ddmmyyyy_HHiiss($anotacao['qan_data_criacao']) ?></b>
                <br/>
                <?= nl2br($anotacao['qan_anotacao']) ?>
                <br/>
            </div>
            <div class="col-sm-2 acao-anotacao">
                <a class="editar-anotacao tooltipster" href="#" data-id="<?= $anotacao['qan_id'] ?>" data-anotacao="<?= $anotacao['qan_anotacao'] ?>" title="Editar anotação"><i class="fa fa-edit"></i></a>
                <a class="remover-anotacao tooltipster" href="#" data-id="<?= $anotacao['qan_id'] ?>" data-que_id="<?= $anotacao['que_id'] ?>" title="Remover anotação"><i class="fa fa-trash"></i></a>
            </div>
        </div>
    <?php endforeach; ?>
<?php else: ?>
    <div class="row text-align-center">Você não possui anotações sobre essa questão.</div>
<?php endif; ?>