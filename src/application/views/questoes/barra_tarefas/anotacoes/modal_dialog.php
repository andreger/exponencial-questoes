<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Anotações sobre a questão</h4>
        </div>
        <div class="modal-body">
            <div id="msg-sucesso-anotacao" class="row" style="display: none;">
                <?= ui_alerta("Anotação salva com sucesso.", ALERTA_SUCESSO, true) ?>
            </div>
            <div id="msg-sucesso-remocao-anotacao" class="row" style="display: none;">
                <?= ui_alerta("Anotação removida com sucesso.", ALERTA_SUCESSO, true) ?>
            </div>
            <div id="msg-erro-anotacao" class="row" style="display: none;">
                <?= ui_alerta("A anotação não pode ser vazia.", ALERTA_ERRO, true) ?>
            </div>
            <div id="form-anotacao-salvando" class="i-box" style="display: none;">Salvando dados ...</div>
            <div id="form-anotacao" class="i-box">
                <div class="form-group">
                    <div class="col-sm-12">
                        <?php echo form_textarea('qan_anotacao', '','id="qan_anotacao" placeholder="Digite aqui a sua anotação" class="form-control m-b"'); ?>
                    </div>
                </div>
                <?php echo form_hidden('qan_id', null); ?>
                <div class="form-group text-align-right">
                    <a href="#" class="btn btn-default btn-primary btn-salvar-anotacao" data-que_id="<?= $cockpit_questao_id ?>" data-id="">Salvar</a>    
                    <a href="#" class="btn btn-default btn-secondary btn-cancelar-anotacao" data-id="">Cancelar</a>
                </div>
            </div>
            <div id="form-anotacao-anotacoes" class="content">
                <?php $this->view('questoes/barra_tarefas/anotacoes/anotacoes') ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
    </div>
</div>