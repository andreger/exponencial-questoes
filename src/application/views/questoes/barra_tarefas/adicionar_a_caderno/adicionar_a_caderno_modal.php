<?php if($is_modal): ?>
<div id="modal-form-caderno" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content modal-form-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Adicionar ao Caderno</h4>
			</div>

<?php endif; ?>

		<div class="modal-body">

			<div class="panel blank-panel">
				<div class="panel-heading">
			<?php if($has_filtro || !$is_modal): ?>
				<?php
					if($total > MAX_QUESTOES_ADICIONADAS_CADERNO) {
						ui_alerta("<b>Atenção:</b> Serão adicionadas ao caderno as primeiras questões listadas, até o limite informado abaixo ou " . number_format(MAX_QUESTOES_ADICIONADAS_CADERNO, 0, ',', '.') . " (o que for menor).", ALERTA_INFO);
					}
				?>

				<?php //if($cadernos) :?>
                        <?php $this->view("questoes/barra_tarefas/adicionar_a_caderno/abas") ?>
				<?php //endif; ?>

				</div>

				<div class="panel-body">

					<div class="tab-content">
				<?php if($cadernos) :?>
                            <?php $this->view("questoes/barra_tarefas/adicionar_a_caderno/caderno_existente", ['is_modal' => $is_modal, 'is_ajax' => $is_ajax]) ?>
				<?php endif; ?>
				<?php $this->view("questoes/barra_tarefas/adicionar_a_caderno/novo_caderno", ['is_modal' => $is_modal, 'is_ajax' => $is_ajax]) ?>
					</div>
			<?php else: ?>
				<?php ui_alerta("<b>Atenção:</b> Para adicionar ao caderno, você precisa aplicar o filtro de pesquisa desejado e então clicar no botão \"Adicionar a Caderno\".", ALERTA_ERRO); ?>
			<?php endif; ?>

                </div>
            </div>
		</div>

<?php if($is_modal): ?>
		</div>
	</div>
</div>
<?php endif; ?>

<script>
function atualizar_categorias(elem)
{
	var caderno_id = $(elem).val();

	$.get('/questoes/cadernos_xhr/listar_categorias_caderno/' + caderno_id, function(data) {
		var ids = $.trim(data).split(',');
		$('#form-caderno-existente .categorias-multiselect').val(ids).trigger('chosen:updated');
	});
}

$(function() {
	$('.categorias-multiselect').chosen({
  		disable_search_threshold: 10,
  		placeholder_text_multiple: "Escolha as categorias do caderno",
  		width: '100%'
  	});

  	$('.novas-categorias-btn').click(function(e) {
  		e.preventDefault();
  		$(this).closest('div').find('.novas-categorias-div:first').toggle();
  	});

  	$('#caderno_id').click(function() {
		atualizar_categorias('#caderno_id');
  	});
});

<?php if($is_ajax): ?>

	$('#novo-caderno').click(function (event) {
		event.preventDefault();
		var que_id = $('#questao_id_n').val();
		var nome_caderno = $('#nome_caderno').val();
		var item_id = "#notificar-" + que_id;
		var categorias_ids = $(item_id + ' .novo-caderno .categorias-multiselect').val();
		var novas_categorias = $(item_id + ' .novo-caderno .novas-categorias').val();
		var comercializavel = 0;
		<?php if(tem_acesso([PROFESSOR])): ?>
			comercializavel = $(item_id + ' .novo-caderno .comercializavel').val();
		<?php endif; ?>

		var coaching = 0;
		<?php if(tem_acesso([PARCEIRO_COACHING])): ?>
			coaching = $(item_id + ' .novo-caderno .coaching').val();
		<?php endif; ?>

		if(!nome_caderno) {
			$('#error-msg').text('O nome do caderno é obrigatório');
			$('#error-msg').show();
		}
		else {
			$('#error-msg').hide();
			$(item_id + ' .panel-body').html("Carregando...");
			$.ajax({
				url: '<?php echo get_xhr_cadernos_adicionar_novo_caderno_url();?>',
				method: 'POST',
				cache: false,
				data: { 'que_id': que_id, 'nome_caderno': nome_caderno, 'categorias_ids': categorias_ids, 'novas_categorias': novas_categorias, 'comercializavel': comercializavel, 'coaching': coaching }
			})
			.done(function( msg ) {
				$(item_id + ' .panel-body').html(msg);
			});
		}
	});

	$('#salvar-existente').click(function (event) {
		event.preventDefault();
		var que_id = $('#questao_id_n').val();
		var cad_id = $('#caderno_id').val();
		var item_id = "#notificar-" + que_id;
		var categorias_ids = $(item_id + ' .caderno-existente .categorias-multiselect').val();
		var novas_categorias = $(item_id + ' .caderno-existente .novas-categorias').val();

		$(item_id + ' .panel-body').html("Carregando...");
		$.ajax({
			url: '<?php echo get_xhr_cadernos_adicionar_caderno_existente_url();?>',
			method: 'POST',
			cache: false,
			data: { 'que_id': que_id, 'cad_id': cad_id, 'categorias_ids': categorias_ids, 'novas_categorias': novas_categorias }
		})
		.done(function( msg ) {
			$(item_id + ' .panel-body').html(msg);
		});
	});

	$('.fechar-caderno').click(function (event) {

		event.preventDefault();

		var id = $(this).data('id');

		var item_id = "#notificar-" + id;

		if(cadernos_visivel[id] == undefined) {
			cadernos_visivel[id] = false;
		}

		notificacao_visivel[id] = false;
		comentario_visivel[id] = false;

		if(cadernos_visivel[id]) {
			$(item_id).hide();
			cadernos_visivel[id] = false;
		}
	});

<?php endif; ?>

</script>