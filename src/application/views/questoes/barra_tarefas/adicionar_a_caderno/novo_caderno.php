<div class="novo-caderno tab-pane <?php echo $cadernos ? '' : 'active' ?>" id="tab-2-<?= $questao_id ?>">
    
    <?php if(!$is_ajax): ?>
        <?php echo form_open(get_adicionar_a_novo_caderno_url() . "?" . $_SERVER['QUERY_STRING'] , 'id="form-novo-caderno" class="form-horizontal"'); ?>
    <?php endif; ?>
    
    <div class="row form-group">
        <div class="col-sm-11">
            Nome do caderno:
            <?php echo form_input('nome', null,'id="nome_caderno" placeholder="Escolha o nome do caderno" class="form-control m-b"'); ?>
        </div>
    </div>

    <?php if($is_modal): ?>
        <div class="row form-group">
            <div class="col-sm-11">
                Número de questões:
                <?php echo form_input('limite', null,'id="limite-caderno-novo" placeholder="Escolha o número de questões a serem adicionadas" class="form-control m-b"'); ?>
                <div id="erro-novo"></div>
            </div>
        </div>
    <?php else: ?>
        <input type="hidden" id="questao_id_n" name="que_id" value="<?php echo $questao_id ?>">
    <?php endif; ?>

    <div class="row form-group">
        <div class="col-sm-11">
            Categorias:
            <?php echo form_multiselect('categorias_ids[]', $categorias_cadernos, NULL, 'id="categorias_caderno" class="form-control m-b categorias-multiselect"'); ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-sm-11">
            <a href="#" class="novas-categorias-btn">Criar novas categorias</a>
            <div class="novas-categorias-div" style="display: none">
                <?php echo form_input('novas_categorias', null, 'placeholder="Crie novas categorias, separadas por vírgula. (Ex: TJ 2016, Direito Civil FCC)" class="novas-categorias form-control m-b"'); ?>
            </div>
        </div>
    </div>

    <?php if(tem_acesso([PROFESSOR])): ?>

    <div class="row form-group">
        <div class="col-sm-11">
            Comercializável?
            <?php echo form_checkbox('comercializavel', SIM, FALSE, 'id="comercializavel" class="m-b comercializavel"'); ?>
        </div>
    </div>

    <?php endif; ?>
    
    <?php if(tem_acesso([PARCEIRO_COACHING])): ?>

    <div class="row form-group">
        <div class="col-sm-11">
            Caderno Coaching?
            <?php echo form_checkbox('coaching', SIM, FALSE, 'id="coaching" class="m-b coaching"'); ?>
        </div>
    </div>

    <?php endif; ?>

    <?php echo form_hidden('filtro_caderno', base64_encode(serialize($filtro))) ?>

    <?php if($is_ajax): ?>
        <?php echo form_button('salvar', 'Salvar', 'id="novo-caderno" class="btn btn-primary"'); ?>
        <?php echo form_button('fechar', 'Fechar', 'id="" class="fechar-caderno btn btn-danger" data-id="' . $questao_id . '"'); ?>
    <?php else: ?>
        <div><?php echo form_submit('salvar', 'Salvar', 'id="salvar-novo" class="btn btn-primary"'); ?></div>
        <?php echo form_close(); ?>
    <?php endif; ?>

</div>