<div class="caderno-existente tab-pane active" id="tab-1-<?= $questao_id ?>">
    
    <?php if(!$is_ajax): ?>
        <?php echo form_open(get_adicionar_a_caderno_existente_url() . "?" . $_SERVER['QUERY_STRING'], 'id="form-caderno-existente" class="form-horizontal"'); ?>
    <?php endif; ?>

    <div class="row form-group">
        <div class="col-sm-11">
            Nome:
            <?php echo form_dropdown('caderno_id', $cadernos, null,'id="caderno_id" class="notificacao-caderno form-control m-b"'); ?>
        </div>
    </div>
    
    <?php if($is_modal): ?>
        <div class="row form-group">
            <div class="col-sm-11">
                Número de questões:
                <?php echo form_input('limite', null,'id="limite-caderno-existente" placeholder="Escolha o número de questões a serem adicionadas" class="form-control m-b"'); ?>
            </div>
        </div>
    <?php else: ?>
        <input type="hidden" id="questao_id_n" name="que_id" value="<?php echo $questao_id ?>">
    <?php endif; ?>

    <div class="row form-group">
        <div class="col-sm-11">
            Categorias:
            <?php echo form_multiselect('categorias_ids[]', $categorias_cadernos, NULL, ' class="categorias-multiselect form-control m-b"'); ?>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-sm-11">
            <a href="#" class="novas-categorias-btn">Criar novas categorias</a>
            <div class="novas-categorias-div" style="display: none">
                <?php echo form_input('novas_categorias', null, 'placeholder="Crie novas categorias, separadas por vírgula. (Ex: TJ 2016, Direito Civil FCC)" class="novas-categorias form-control m-b"'); ?>
            </div>
        </div>
    </div>
    
    <?php echo form_hidden('filtro_caderno', base64_encode(serialize($filtro))) ?>
    
    <?php if($is_ajax): ?>
        <div>
            <?php echo form_button('salvar', 'Salvar', 'id="salvar-existente" class="btn btn-primary"'); ?>
            <?php echo form_button('fechar', 'Cancelar', 'id="" class="fechar-caderno btn btn-danger" data-id="' . $questao_id . '"'); ?>
        </div>
    <?php else: ?>
        <div><?php echo form_submit('salvar', 'Salvar', 'id="salvar-existente" class="btn btn-primary"'); ?></div>
        <?php echo form_close(); ?>
    <?php endif; ?>

</div>