<div class="panel-options">
    <ul class="nav nav-tabs">
        <?php if($cadernos) :?>
            <li class="active"><a href="#tab-1-<?= $questao_id ?>" data-toggle="tab">Caderno Existente</a></li>
        <?php endif; ?>
        <li class=""><a href="#tab-2-<?= $questao_id ?>" id="novo-tab" data-toggle="tab">Novo Caderno</a></li>
    </ul>
</div>