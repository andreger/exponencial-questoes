<div id="modal-atalhos-navegacao" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Botões de atalho</h4>
			</div>
			<div class="modal-body">
            <table id="tabela-atalhos" class="tabela-atalhos" style="width: 100%">
                <tr>
                    <th>Ação</th>
                    <th>Atalho</th>
                </tr>
                <tr>
                    <td>Estatísticas da questão</td>                    
                    <td><code>CTRL</code> + <code>E</code></td>
                </tr>
                <tr>
                    <td>Comentários da questão</td>                    
                    <td><code>CTRL</code> + <code>H</code></td>
                </tr> 
                <tr>
                    <td>Marcar/desmarcar questão como favorita</td>                    
                    <td><code>CTRL</code> + <code>U</code></td>
                </tr>
                <tr>
                    <td>Anotações sobre a questão</td>                    
                    <td><code>CTRL</code> + <code>A</code></td>
                </tr>
                <tr>
                    <td>Ir para a próxima questão</td>                    
                    <td><code>CTRL</code> + <code style="font-size: 18px;"><?= json_decode('"'.'\u2192'.'"') ?></code></td>
                </tr>
                <tr>
                    <td>Ir para a questão anterior</td>                    
                    <td><code>CTRL</code> + <code style="font-size: 18px;"><?= json_decode('"'.'\u2190'.'"') ?></code></td>
                </tr>
                <tr>
                    <td>Ir para uma questão aleatória</td>                    
                    <td><code>CTRL</code> + <code style="font-size: 18px;"><?= json_decode('"'.'\u2191'.'"') ?></code></td>
                </tr>
                <tr>
                    <td>Compartilhar da questão</td>                    
                    <td><code>CTRL</code> + <code>Q</code></td>
                </tr>
                <?php if(isset($caderno)): ?>
                    <tr>
                        <td>Informações sobre o caderno</td>                    
                        <td><code>CTRL</code> + <code>I</code></td>
                    </tr>
                    <tr>
                        <td>Filtros do caderno</td>                    
                        <td><code>CTRL</code> + <code>Y</code></td>
                    </tr>
                    <tr>
                        <td>Compartilhar caderno</td>                    
                        <td><code>CTRL</code> + <code>K</code></td>
                    </tr>
                <?php endif; ?>
                <?php 
                    $imprimir = "Imprimir questões";
                    if(isset($caderno))
                    {
                        $imprimir = "Imprimir caderno";
                    }
                    elseif(isset($simulado))
                    {
                        $imprimir = "Imprimir simulado";
                    }
                ?>
                <tr>
                    <td><?= $imprimir ?></td>                    
                    <td><code>CTRL</code> + <code>P</code></td>
                </tr>
                <tr>
                    <td>Diminuir tamanho da fonte</td>                    
                    <td><code>CTRL</code> + <code>1</code></td>
                </tr>
                <tr>
                    <td>Aumentar tamanho da fonte</td>                    
                    <td><code>CTRL</code> + <code>2</code></td>
                </tr>         
            </table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>