<?php
    if(isset($questao))
    {
        $url = /*$questao['que_url_reduzida']?:*/get_questao_url($questao['que_id']);
    }
    else
    {
        $url = "Carregando...";
    }
?>
<div id="modal-compartilhar-questao" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Compartilhar link da questão</h4>
			</div>
			<div class="modal-body">
                <div id="msg-sucesso-caderno" class="row" style="display: none;">
                  <?= ui_alerta("URL da questão copiada para a área de transferência.", ALERTA_SUCESSO, true) ?>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <?php echo form_input('', $url,'id="url_questao" readonly class="form-control m-b"'); ?>
                    </div>
                    <div class="col-sm-2">
                        <a href="#" class="btn btn-default btn-clipboard"><i class="fa fa-clipboard"></i></a>
                    </div>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>
<script>
    
    $(".link-compartilhar-questao-listagem").click(function(e){
        $("#url_questao").val($(this).data("url"));
    });

    $('.link-compartilhar-questao').click(function(e){
        $("#url_questao").val($(this).data("url"));
        $("#url_questao").select();
    });

    $(".btn-clipboard").click(function(e){
        $("#url_questao").select();
        document.execCommand("copy");
        $("#msg-sucesso-caderno").show();
    });

    $('#modal-compartilhar-questao').on('shown.bs.modal', function (e) {
        $("#msg-sucesso-caderno").hide();
        $("#url_questao").select();
    });

</script>