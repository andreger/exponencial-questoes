<div id="modal-copiar-ids" class="modal fade" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
            <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Copiar IDs das questões</h4>
			</div>
			<div class="modal-body">
                <div id="msg-sucesso-copia" class="row" style="display: none;">
                  <?= ui_alerta("Os IDs das questões foram copiados para a área de transferência.", ALERTA_SUCESSO, true) ?>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <?php echo form_input('', '','id="ids_questoes" readonly class="form-control m-b"'); ?>
                    </div>
                    <div class="col-sm-2">
                        <a href="#" class="btn btn-default btn-clipboard btn-copiar-ids"><i class="fa fa-clipboard"></i></a>
                    </div>
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>
<script>

    $('#copiar_ids').click(function(e){

        $('#modal-copiar-ids').modal("show");

        if($("#ids_questoes").val()){

            $("#ids_questoes").select();

        }else{
            
            $("#ids_questoes").val("Carregando ...");

            $.get('/questoes/questoes_xhr/buscar_ids_questoes?<?= (isset($caderno_id)?"cad_id={$caderno_id}&":"") . $_SERVER['QUERY_STRING'] ?>', function(data) {
                $("#ids_questoes").val(data);
                $("#ids_questoes").select();
            });
            
        }

    });

    $(".btn-copiar-ids").click(function(e){
        $("#ids_questoes").select();
        document.execCommand("copy");
        $("#msg-sucesso-copia").show();
    });

    $('#modal-copiar-ids').on('shown.bs.modal', function (e) {
        $("#msg-sucesso-copia").hide();
        $("#ids_questoes").select();
    });

</script>