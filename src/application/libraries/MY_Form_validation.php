<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MY_Form_validation Class
 *
 * Extends Form_Validation library
 *
*/
class MY_Form_validation extends CI_Form_validation {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function edit_unique($value, $params)
	{
		$CI =& get_instance();
		$CI->load->database();
	
		list($table, $field, $current_id) = explode(".", $params);
	
		$query = $CI->db->select()->from($table)->where($field, $value)->limit(1)->get();
	
		if ($query->row() && $query->row()->id != $current_id)
		{
			return FALSE;
		}
	}
}
?>