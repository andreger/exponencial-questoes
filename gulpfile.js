var gulp = require('gulp'),
    { symlink } = require('gulp'),
    del = require('del'),
    clean = require('gulp-clean'),
    cssmin = require('gulp-cssmin'),
    htmlmin = require('gulp-htmlmin'),
    rev = require('gulp-rev'),
    uglify = require('gulp-uglify-es').default,
    usemin = require('gulp-usemin');

gulp.task('clean', function() {
    return gulp.src('dist',  {allowEmpty: true})
        .pipe(clean());
});

gulp.task('copy', function() {
    return gulp.src(['src/**/*', '!src/uploads/**'],  {allowEmpty: true, dot: true, buffer:false})
        .pipe(gulp.dest('dist'));
});

gulp.task('symlink', function() {
    return gulp.src('dist/')
        .pipe(symlink('dist/questoes'));
});

gulp.task('usemin', function() {
    return gulp.src('dist/**/*.html.php')
        .pipe(usemin({
            'path': 'dist',
            'assetsDir': 'dist',
            'outputRelativePath': '/',
            'js' : [function() { return uglify() }, function() { return rev() }],
            'css' : [function() { return cssmin() }, function() { return rev() }],
            'html': [ function() { return htmlmin({ collapseWhitespace: true }) }]
        }))
        .pipe(gulp.dest('dist'))
});

gulp.task('removesymlink', function() {
    return del('dist/questoes',  {force: true});
});

gulp.task('default', gulp.series('clean', 'copy', 'symlink', 'usemin', 'removesymlink'));

